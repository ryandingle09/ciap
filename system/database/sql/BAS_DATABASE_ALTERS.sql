-- SAMPLE FORMAT
-- RODEL SATUITO
-- 2016.05.30
-- ALTER STATEMENTS;

-- RYAN DINGLE
-- 2016.06.02 08:44AM
ALTER TABLE param_source_types ADD active_flag tinyint(1);

INSERT INTO `bas.param_source_types` (`source_type_num`,`source_type_name`,`active_flag`) VALUES (1,'Current Appropriation',1);
INSERT INTO `bas.param_source_types` (`source_type_num`,`source_type_name`,`active_flag`) VALUES (2,'Continuing Appropriation',1);
INSERT INTO `bas.param_source_types` (`source_type_num`,`source_type_name`,`active_flag`) VALUES (3,'Special Appropriation',1);

-- RYAN DINGLE
-- 2016.06.02 03:53PM
ALTER TABLE line_items MODIFY
    line_item_code VARCHAR(10)
    CHARACTER SET latin1
    COLLATE latin1_general_cs;

-- RYAN DINGLE
-- 2016.06.09 04:26PM
ALTER TABLE line_item_releases CHANGE rerference_date reference_date DATE;
ALTER TABLE  `line_item_releases` ADD  `total_release_amount` FLOAT NOT NULL AFTER  `reference_date` ;
ALTER TABLE  `line_item_releases` ADD  `total_release_amount` DECIMAL( 25, 2 ) NOT NULL AFTER  `reference_date` ;

-- RODE SATUITO
-- 2016.06.30 10:07AM
-- FORGOT TO PUT THIS STATEMENT
ALTER TABLE obligation_charges ADD office_num INT(10) unsigned NOT NULL AFTER account_code;
ALTER TABLE obligation_charges ADD FOREIGN KEY (office_num) REFERENCES param_offices(office_num);


-- RYAN DINGLE
-- 2016.06.29 9.31AM
ALTER TABLE obligation_charges DROP PRIMARY KEY;
ALTER TABLE obligation_charges ADD PRIMARY KEY (obligation_id, line_item_id, account_code, office_num);

-- RYAN DINGLE
-- 2016.06.30 10.00AM
INSERT INTO `bas`.`param_disbursement_funds` (`disbursement_fund_id`, `fund_name`) VALUES (NULL, 'GAA');

-- RYAN DINGLE
-- 2016.07.30 11.27AM
--
-- Dumping data for table `param_account_codes`

INSERT INTO `param_account_codes` (`account_code`, `account_name`, `parent_account_code`, `active_flag`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
('501', 'Personal Services', NULL, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-12 18:25:05'),
('50101', 'Salaries and Wages', '501', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-12 17:38:21'),
('50101010', 'Salaries and Wages - Regular', '50101', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-12 17:38:26'),
('50101020', 'Salaries and Wages - Casual/Contractual', '50101', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-12 17:36:55'),
('50102', 'Other Compensation', '501', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-12 17:36:44'),
('50102010', 'Personal Economic Relief Allowance ( PERA )', '50102', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-12 17:37:15'),
('50102020', 'Representation Allowance ( RA )', '50102', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('502', 'Maintainance and Other Operating Expenses', NULL, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-02 09:00:47'),
('50201', 'Travelling Expenses', '502', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-02 16:36:20'),
('50201010', 'Training Expenses', '50201', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-02 16:36:33'),
('50201020', 'Supplies and Materials', '50201', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-02 16:37:19'),
('50202', 'Utility Exepenses', '502', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-02 16:37:51'),
('50202010', 'Communication Services', '50202', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-02 16:38:15'),
('50202020', 'Advertising Expenses', '50202', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-02 16:38:35'),
('503', 'CO', NULL, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-02 09:02:08'),
('50301', 'CO Salaries and Wages', '503', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-12 09:37:03'),
('50301010', 'CO Salaries and Wages - Regular', '50301', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-12 09:37:11'),
('50301020', 'CO Salaries and Wages - Casual/Contractual', '50301', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-12 09:37:18'),
('50302', 'CO Other Compensation', '503', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-12 09:37:25'),
('50302010', 'CO Personal Economic Relief Allowance ( PERA )', '50302', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-12 09:37:32'),
('50302020', 'CO Representation Allowance ( RA )', '50302', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-12 09:37:38');


-- RODEL SATUITO
-- 2016.06.30 02:57PM
ALTER TABLE line_item_realigns ADD posted_flag TINYINT(1) NOT NULL DEFAULT 1 AFTER reference_no;

-- RYAN DINGLE
-- 2016.07.11 10.53AM
ALTER TABLE `param_account_codes` ADD `created_by` DATETIME NOT NULL AFTER `active_flag` ,
ADD `created_date` DATETIME NOT NULL AFTER `created_by` ,
ADD `modified_by` DATETIME NOT NULL AFTER `created_date` ,
ADD `modified_date` DATETIME NOT NULL AFTER `modified_by` ;

-- RYAN DINGLE
-- 2016.07.11 2.54PM
ALTER TABLE `line_item_accounts` ADD `realigned_amount` DECIMAL( 25, 2 ) NOT NULL AFTER `allotment_amount` ,
ADD `obligated_amount` DECIMAL( 25, 2 ) NOT NULL AFTER `realigned_amount` ;


-- RYAN DINGLE
-- 2016.07.11 4.45PM
ALTER TABLE `line_item_offices` ADD `total_obligated_amount` DECIMAL( 25, 2 ) NOT NULL AFTER `co_allotment` ;

-- RYAN DINGLE
-- 2016.07.18 5.11PM
ALTER TABLE `param_account_codes` CHANGE `created_by` `created_by` INT( 11 ) NOT NULL ;

-- RYAN DINGLE
-- 2016.07.22 10.35AM
ALTER TABLE `line_item_accounts` ADD `disbursed_amount` DECIMAL( 25, 2 ) NOT NULL AFTER `obligated_amount` ;

-- RYAN DINGLE
-- 2016.07.25 1.39PM
ALTER TABLE `checks` ADD `remarks` TEXT NULL AFTER `check_status_id` ;


-- RYAN DINGLE
-- 2016.07.22 5.36PM
CREATE TABLE IF NOT EXISTS `param_check_mode_payments` (
  `mode_payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `mode_type` varchar(50) NOT NULL,
  PRIMARY KEY (`mode_payment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

INSERT INTO `bas`.`param_check_mode_payments` (
`mode_payment_id` ,
`mode_type`
)
VALUES (
NULL , 'MDS Check'
), (
NULL , 'Commercial Check'
), (
NULL , 'ADA'
);

-- RYAN DINGLE
-- 2016.07.22 5:48PM
ALTER TABLE `checks` ADD `mode_of_payment` INT NOT NULL AFTER `disbursement_id` ;

-- RYAN DINGLE
-- 2016.08.16 1.30PM
CREATE TABLE `param_deduction_types` (
  `deduction_id` int(11) NOT NULL AUTO_INCREMENT,
  `deduction_name` varchar(50) NOT NULL,
  `deduction_code` varchar(20) NOT NULL,
  PRIMARY KEY (`dudection_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- RYAN DINGLE
-- 2016.08.16 02:01pm
INSERT INTO `param_deduction_types` (`deduction_id`, `deduction_name`, `deduction_code`) VALUES (NULL, 'PICAM', 'PICAM'), (NULL, 'DTA', 'Due to Absences');

-- RYAN DINGLE
-- 2016.08.17 11:16AM
ALTER TABLE `disbursements` CHANGE `payee_id` `payee_id` INT(10) UNSIGNED NULL;


-- RYAN DINGLE
-- 2016.08.19 3:42PM
ALTER TABLE `line_item_offices` CHANGE `total_allotment` `total_allotment` DECIMAL(25,2) NOT NULL DEFAULT '0.00';
ALTER TABLE `line_item_offices` CHANGE `total_appropriation` `total_appropriation` DECIMAL(25,2) NOT NULL DEFAULT '0.00';
ALTER TABLE `line_items` CHANGE `total_appropriation` `total_appropriation` DECIMAL(25,2) NOT NULL DEFAULT '0.00', CHANGE `total_allotment` `total_allotment` DECIMAL(25,2) NOT NULL DEFAULT '0.00';


-- RYAN DINGLE
-- 2016.08.26 1:29PM
ALTER TABLE `arbitrators` CHANGE `arbitrator_id` `arbitrator_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `cases` CHANGE `case_id` `case_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `case_arbitrator_payments` CHANGE `arbitrator_payment_id` `arbitrator_payment_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `param_proceeding_stages` CHANGE `proceeding_stage_id` `proceeding_stage_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE `param_proceeding_stage_activities` CHANGE `proceeding_stage_activity_id` `proceeding_stage_activity_id` INT(10) UNSIGNED NOT NULL;

    
/***** PLEASE PUT CONTENTS ABOVE THIS COMMENT. *****

THIS ALTER FILE IS FOR BAS SCHEMA ONLY!!!!

REMINDER:

1. No DROPPING of table please.
2. When using 'DELIMITER', reset the delimiter to ';' afterwards.
3. Please remove 'DEFINER=`root`@`localhost`' when creating a trigger.
4. Always check if the statement ends with a DELIMITER (;).
5. When inserting, use the format: INSERT INTO <table_name> (<field_1>, <field_2>, ...) VALUES (<value_1>, <value_2>, ...);
6. NAME FORMAT: REJ MEDIODIA 2015.01.23 11:43AM 
*********** NO STATEMENTS BELOW THIS. **************/