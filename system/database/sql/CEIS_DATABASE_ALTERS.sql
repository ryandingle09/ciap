-- RODEL SATUITO
-- 2016.05.30
ALTER TABLE contractor DROP FOREIGN KEY fk_request_contractor_requests10;

-- CHRISTIAN AQUINO 2016.05.30 11:30AM 

/*
-- Query: select * from param_countries
LIMIT 0, 1000

-- Date: 2016-05-30 11:25
*/
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ABW','Aruba','AWG');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('AFG','Afghanistan','AFN');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('AGO','Angola','AOA');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('AIA','Anguilla','XCD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ALB','Albania','ALL');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('AND','Andorra','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ARE','United Arab Emirates','AED');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ARG','Argentina','ARS');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ARM','Armenia','AMD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ASM','American Samoa','USD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ATA','Antarctica','XCD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ATG','Antigua and Barbuda','XCD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('AUS','Australia','AUD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('AUT','Austria','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('AZE','Azerbaijan','AZN');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BDI','Burundi','BIF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BEL','Belgium','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BEN','Benin','XOF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BFA','Burkina Faso','XOF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BGD','Bangladesh','BDT');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BGR','Bulgaria','BGN');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BHR','Bahrain','BHD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BHS','Bahamas','BSD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BIH','Bosnia-Herzegovina','BAM');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BLR','Belarus','BYR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BLZ','Belize','BZD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BMU','Bermuda','BMD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BOL','Bolivia','BOB');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BRA','Brazil','BRL');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BRB','Barbados','BBD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BRN','Brunei Darussalam','BND');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BTN','Bhutan','BTN');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BVT','Bouvet Island','NOK');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('BWA','Botswana','BWP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('CAF','Central African Republic','XAF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('CAN','Canada','CAD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('CCK','Cocos (Keeling) Islands','AUD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('CHE','Switzerland','CHF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('CHL','Chile','CLP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('CHN','China','CNY');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('CMR','Cameroon','XAF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('COD','Congo, Dem. Republic','CDF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('COG','Congo','XAF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('COK','Cook Islands','NZD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('COL','Colombia','COP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('COM','Comoros','KMF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('CPV','Cape Verde','CVE');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('CRI','Costa Rica','CRC');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('CUB','Cuba','CUP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('CXR','Christmas Island','AUD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('CYM','Cayman Islands','KYD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('CYP','Cyprus','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('CZE','Czech Rep.','CZK');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('DEU','Germany','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('DJI','Djibouti','DJF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('DMA','Dominica','XCD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('DNK','Denmark','DKK');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('DOM','Dominican Republic','DOP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('DZA','Algeria','DZD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ECU','Ecuador','ECS');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('EGY','Egypt','EGP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ERI','Eritrea','ERN');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ESH','Western Sahara','MAD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ESP','Spain','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('EST','Estonia','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ETH','Ethiopia','ETB');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('FIN','Finland','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('FJI','Fiji','FJD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('FLK','Falkland Islands (Malvinas)','FKP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('FRA','France','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('FRO','Faroe Islands','DKK');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('FSM','Micronesia','USD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GAB','Gabon','XAF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GBR','U.K.','GBP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GEO','Georgia','GEL');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GGY','Guernsey','GGP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GHA','Ghana','GHS');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GIB','Gibraltar','GIP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GIN','Guinea','GNF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GLP','Guadeloupe (French)','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GMB','Gambia','GMD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GNB','Guinea Bissau','GWP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GNQ','Equatorial Guinea','XAF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GRC','Greece','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GRD','Grenada','XCD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GRL','Greenland','DKK');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GTM','Guatemala','QTQ');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GUF','French Guiana','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GUM','Guam (USA)','USD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('GUY','Guyana','GYD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('HKG','Hong Kong','HKD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('HMD','Heard Island and McDonald Islands','AUD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('HND','Honduras','HNL');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('HRV','Croatia','HRK');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('HTI','Haiti','HTG');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('HUN','Hungary','HUF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('IDN','Indonesia','IDR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('IMN','Isle of Man','GBP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('IND','India','INR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('IOT','British Indian Ocean Territory','USD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('IRL','Ireland','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('IRN','Iran','IRR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('IRQ','Iraq','IQD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ISL','Iceland','ISK');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ISR','Israel','ILS');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ITA','Italy','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('JAM','Jamaica','JMD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('JEY','Jersey','GBP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('JOR','Jordan','JOD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('JPN','Japan','JPY');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('KAZ','Kazakhstan','KZT');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('KEN','Kenya','KES');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('KGZ','Kyrgyzstan','KGS');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('KHM','Cambodia','KHR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('KIR','Kiribati','AUD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('KNA','Saint Kitts & Nevis Anguilla','XCD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('KOR','Korea-South','KRW');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('KWT','Kuwait','KWD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('LAO','Laos','LAK');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('LBN','Lebanon','LBP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('LBR','Liberia','LRD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('LBY','Libya','LYD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('LCA','Saint Lucia','XCD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('LIE','Liechtenstein','CHF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('LKA','Sri Lanka','LKR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('LSO','Lesotho','LSL');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('LTU','Lithuania','LTL');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('LUX','Luxembourg','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('LVA','Latvia','LVL');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MAC','Macau','MOP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MAR','Morocco','MAD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MCO','Monaco','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MDA','Moldova','MDL');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MDG','Madagascar','MGF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MDV','Maldives','MVR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MEX','Mexico','MXN');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MHL','Marshall Islands','USD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MKD','Macedonia','MKD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MLI','Mali','XOF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MLT','Malta','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MMR ','Myanmar','MMK');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MNE','Montenegro','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MNG','Mongolia','MNT');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MNP','Northern Mariana Islands','USD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MOZ','Mozambique','MZN');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MRT','Mauritania','MRO');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MSR','Montserrat','XCD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MTQ','Martinique (French)','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MUS','Mauritius','MUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MWI','Malawi','MWK');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MYS','Malaysia','MYR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('MYT','Mayotte','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('NAM','Namibia','NAD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('NCL','New Caledonia (French)','XPF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('NER','Niger','XOF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('NFK','Norfolk Island','AUD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('NGA','Nigeria','NGN');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('NIC','Nicaragua','NIO');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('NIU','Niue','NZD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('NLD','Netherlands','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('NOR','Norway','NOK');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('NPL','Nepal','NPR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('NRU','Nauru','AUD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('NZL','New Zealand','NZD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('OMN','Oman','OMR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('PAK','Pakistan','PKR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('PAN','Panama','PAB');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('PCN','Pitcairn Island','NZD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('PER','Peru','PEN');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('PHL','Philippines','PHP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('PLW','Palau','USD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('PNG','Papua New Guinea','PGK');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('POL','Poland','PLN');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('PRI','Puerto Rico','USD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('PRK','Korea-North','KPW');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('PRT','Portugal','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('PRY','Paraguay','PYG');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('PYF','French Southern Territories','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('QAT','Qatar','QAR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('REU','Reunion (French)','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ROU','Romania','RON');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('RUS','Russia','RUB');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('RWA','Rwanda','RWF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SAU','Saudi Arabia','SAR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SDN','Sudan','SDG');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SEN','Senegal','XOF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SGP','Singapore','SGD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SGS','South Georgia & South Sandwich Islands','GBP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SHN','Saint Helena','SHP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SJM','Svalbard and Jan Mayen Islands','NOK');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SLB','Solomon Islands','SBD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SLE','Sierra Leone','SLL');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SLV','El Salvador','SVC');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SMR','San Marino','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SOM','Somalia','SOS');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SPM','Saint Pierre and Miquelon','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SRB','Serbia','RSD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SSD','South Sudan','SSP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('STP','Sao Tome and Principe','STD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SUR','Suriname','SRD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SVK','Slovakia','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SVN','Slovenia','EUR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SWE','Sweden','SEK');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SWZ','Swaziland','SZL');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SYC','Seychelles','SCR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('SYR','Syria','SYP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('TCA','Turks and Caicos Islands','USD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('TCD','Chad','XAF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('TGO','Togo','XOF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('THA','Thailand','THB');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('TJK','Tajikistan','TJS');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('TKL','Tokelau','NZD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('TKM','Turkmenistan','TMT');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('TON','Tonga','TOP');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('TTO','Trinidad and Tobago','TTD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('TUN','Tunisia','TND');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('TUR','Turkey','TRY');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('TUV','Tuvalu','AUD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('TWN','Taiwan','TWD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('TZA','Tanzania','TZS');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('UGA','Uganda','UGX');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('UKR','Ukraine','UAH');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('UMI','USA Minor Outlying Islands','USD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('URY','Uruguay','UYU');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('USA','USA','USD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('UZB','Uzbekistan','UZS');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('VCT','Saint Vincent & Grenadines','XCD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('VEN','Venezuela','VEF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('VGB','Virgin Islands (British)','USD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('VIR','Virgin Islands (USA)','USD');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('VNM','Vietnam','VND');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('VUT','Vanuatu','VUV');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('WLF','Wallis and Futuna Islands','XPF');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('WSM','Samoa','WST');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('YEM','Yemen','YER');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ZAF','South Africa','ZAR');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ZMB','Zambia','ZMW');
INSERT INTO `param_countries` (`country_code`,`country_name`,`currency_code`) VALUES ('ZWE','Zimbabwe','ZWD');

-- RODEL SATUITO
-- 2016.05.30 12:00MN
ALTER TABLE param_contract_involvement MODIFY involvement_code varchar(5) NOT NULL;


-- CHRISTIAN GIL COCJIN AQUINO
-- 2016.06.06 01:13PM
INSERT INTO `param_request_types` (`request_type_id`, `request_type_name`) VALUES ('1', 'Application of Registration');
INSERT INTO `param_request_types` (`request_type_id`, `request_type_name`) VALUES ('2', 'Application for Renewal of Registration');
INSERT INTO `param_request_types` (`request_type_id`, `request_type_name`) VALUES ('3', 'Authority to bid/undertake project contracts');
INSERT INTO `param_request_types` (`request_type_id`, `request_type_name`) VALUES ('4', 'Authority to bid/undertake manpower service contracts');
INSERT INTO `param_request_types` (`request_type_id`, `request_type_name`) VALUES ('5', 'Application for Registration of Equipment');


-- CHRISTIAN GIL AQUINO 
-- 2016.06.08 9:35AM 
ALTER TABLE `request_statements` ADD `exist_in_pcab` TINYINT(1) NULL DEFAULT '0' AFTER `modified_date`;


-- KEBS VILLAROJO
-- JUNE 28,2016
CREATE TABLE `param_license_category` (
  `license_category_code` varchar(10) NOT NULL DEFAULT '',
  `license_category_name` varchar(100) NOT NULL DEFAULT '',
  `sort_order` smallint(6) NOT NULL DEFAULT '1',
  `fin_cap_networth` decimal(17,2) NOT NULL,
  `fin_cap_req_credit_pts` decimal(7,2) NOT NULL,
  `fin_cap_no_pt` decimal(5,2) NOT NULL,
  `fin_cap_no_pt_per_value` decimal(10,2) NOT NULL,
  `tech_cap_ste_individual_yr` decimal(4,2) NOT NULL,
  `tech_cap_ste_aggregate_yr` decimal(5,2) NOT NULL,
  `tech_cap_req_credit_pt` decimal(7,2) NOT NULL,
  `tech_cap_no_pt` decimal(5,2) NOT NULL,
  `tech_cap_no_pt_per_value` decimal(10,2) NOT NULL,
  `default_size_range_code` varchar(10) NOT NULL DEFAULT '',
  `deleted_flag` smallint(6) NOT NULL DEFAULT '0',
  `created_by` varchar(10) NOT NULL DEFAULT '',
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` varchar(10) DEFAULT '',
  `last_modified_date` datetime DEFAULT NULL,
  `synchronized_flag` smallint(6) NOT NULL DEFAULT '0',
  `trade_flag` tinyint(3) DEFAULT '0',
  `synch_date` datetime DEFAULT NULL,
  PRIMARY KEY (`license_category_code`),
  KEY `fk_ceis_param_license_category_ceis_param_reg_size_range1_idx` (`default_size_range_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `users` (
  `employee_no` varchar(10) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- CHRISTIAN GIL AQUINO
-- 2016.July.12 1:05PM

ALTER TABLE `contractor_staff_experiences` ADD `designation` VARCHAR(100) NOT NULL AFTER `company_address`;
ALTER TABLE `contractor_staff_projects` ADD `project_cost` DECIMAL(25,2) NOT NULL AFTER `project_location`;

-- CHRISTIAN GIL AQUINO
-- 2016.July.12 4:14PM
ALTER TABLE `contractor_equipment` ADD `for_registration_flag` TINYINT(1) NOT NULL AFTER `active_flag`;

-- CHRISTIAN GIL AQUINO
-- 2016.July.12 5:31PM
ALTER TABLE registration RENAME contractor_registration;

-- CHRISTIAN GIL AQUINO
-- 2016.July.13 11:53AM
ALTER TABLE contractor_registration 
DROP PRIMARY KEY;

-- CHRISTIAN GIL AQUINO
-- 2016.July.18 1:11PM
ALTER TABLE `contractor_statements` ADD `statement_id` INT(10) NOT NULL FIRST;
ALTER TABLE `progress_report_manpower` ADD `report_manpower_id` INT(10) NOT NULL FIRST;

-- CURRENT END FOR BENCHMARK

-- KEBS VILLAROJO
-- JULY 07,2016
ALTER TABLE request_payment_collections DROP PRIMARY KEY, ADD PRIMARY KEY(rqst_payment_id, coll_nature_code);

-- KEBS VILLAROJO
-- JULY 11,2016
UPDATE `param_request_status` SET `request_status`='APPROVED' WHERE  `request_status_id`=3;
UPDATE `param_request_status` SET `request_status`='DISAPPROVED' WHERE  `request_status_id`=4;
UPDATE `param_request_status` SET `request_status`='IN PROCESSING' WHERE  `request_status_id`=2;

UPDATE `param_request_status` SET `request_status`='IN PROCESSING' WHERE  `request_status_id`=1;
DELETE FROM `param_request_status` WHERE  `request_status_id`=2;

INSERT INTO param_request_tasks
(request_type_id, sequence_no, task_name, role_code)
VALUES
(1, 1, 'Receive Request', 'CEIS-RECEIVING-STAFF'),
(1, 2, 'Pre-evaluation', 'CEIS-EVALUATOR'),
(1, 3, 'Prepare Order of Payment', 'CEIS-RECEIVING-STAFF'),
(1, 4, 'Receive and Encode Official Receipt', 'CEIS-RECEIVING-STAFF'),
(1, 5, 'Submit for Evaluation', 'CEIS-RECEIVING-STAFF'),
(1, 6, 'Evaluate Request', 'CEIS-EVALUATOR'),
(1, 7, 'Print Memorandum', 'CEIS-EVALUATOR'),
(1, 8, 'Submit for Review', 'CEIS-EVALUATOR'),
(1, 9, 'Review Request', 'CEIS-REVIEWER'),
(1, 10, 'Submit for Endorsement', 'CEIS-REVIEWER'),
(1, 11, 'Endorse Request', 'CEIS-ENDORSER'),
(1, 12, 'Submit for Board Action', 'CEIS-ENDORSER'),
(1, 13, 'Receive Board Action', 'CEIS-EVALUATOR'),
(1, 14, 'Release License', 'CEIS-RELEASING-STAFF');

INSERT INTO `param_request_task_status` (`task_status_id`, `task_status`) VALUES (1, 'Not yet started');
INSERT INTO `param_request_task_status` (`task_status_id`, `task_status`) VALUES (2, 'Ongoing');
INSERT INTO `param_request_task_status` (`task_status_id`, `task_status`) VALUES (3, 'Done');
INSERT INTO `param_request_task_status` (`task_status_id`, `task_status`) VALUES (4, 'Skip');

CREATE TABLE `param_role` (
  `role_code` varchar(100) NOT NULL DEFAULT '',
  `role_name` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`role_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


INSERT INTO param_role
(role_code, role_name)
VALUES
('CEIS-RECEIVING-STAFF', 'Receiving Staff'),
('CEIS-EVALUATOR', 'Evaluator'),
('CEIS-REVIEWER', 'Reviewer'),
('CEIS-ENDORSER', 'Endorser'),
('CEIS-RELEASING-STAFF', 'Releasing Staff');


-- CHRISTIAN GIL AQUINO
-- 2016.July.20 11:58PM
CREATE TABLE `contractor_payments` (
  `contractor_payment_id` int(11) NOT NULL,
  `contractor_id` int(10) unsigned NOT NULL,
  `op_date` date NOT NULL,
  `op_amount` decimal(25,2) NOT NULL,
  `or_number` int(11) NOT NULL,
  `op_type` varchar(100) NOT NULL,
  `or_date` date NOT NULL,
  `or_amount` decimal(25,2) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`contractor_payment_id`,`contractor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

-- CHRISTIAN GIL AQUINO
-- 2016.July.20 1:13PM
CREATE TABLE `contractor_payment_collections` (
  `contractor_payment_id` int(11) NOT NULL,
  `coll_nature_code` varchar(100) NOT NULL,
  `amount` varchar(45) NOT NULL,
  PRIMARY KEY (`contractor_payment_id`,`coll_nature_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1

-- CHRISTIAN GIL AQUINO
-- 2016.July.21 5:21PM
ALTER TABLE `contractor_payments` CHANGE `op_type` `request_type_id` INT(10) NOT NULL;
ALTER TABLE `contractor_payments` CHANGE `request_type_id` `request_type_id` INT(10) UNSIGNED NOT NULL;
ALTER TABLE contractor_payments ADD FOREIGN KEY (request_type_id) REFERENCES param_request_types (request_type_id);

-- KEBS VILLAROJO
-- 2016.Aug.19 11:59AM
DROP TABLE `contractor_license_classifications` ;
CREATE TABLE `contractor_license_classifications` (
  `contractor_id` int(10) unsigned NOT NULL COMMENT 'Refers to request',
  `license_classification_code` varchar(15) NOT NULL COMMENT 'Refers to license classification',
  `specialty_flag` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`contractor_id`,`license_classification_code`,`specialty_flag`),
  KEY `fk_contractor_license_classifications_contractor1_idx` (`contractor_id`),
  KEY `fk_request_license_classifications_param_license_classifica_idx` (`license_classification_code`),
  CONSTRAINT `fk_contractor_license_classifications_contractor1` FOREIGN KEY (`contractor_id`) REFERENCES `contractor` (`contractor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_contractor_license_classifications_param_lic_classificati1` FOREIGN KEY (`license_classification_code`) REFERENCES `param_license_classifications` (`license_classification_code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the license classifications per contractor';

-- KEBS VILLAROJO
-- 2016.Aug.19 11:59AM
ALTER TABLE contractor_projects DROP COLUMN remarks;
ALTER TABLE contractor_services DROP COLUMN remarks;

/***** PLEASE PUT CONTENTS ABOVE THIS COMMENT. *****

THIS ALTER FILE IS FOR CEIS SCHEMA ONLY!!!!

REMINDER:

1. No DROPPING of table please.
2. When using 'DELIMITER', reset the delimiter to ';' afterwards.
3. Please remove 'DEFINER=`root`@`localhost`' when creating a trigger.
4. Always check if the statement ends with a DELIMITER (;).
5. When inserting, use the format: INSERT INTO <table_name> (<field_1>, <field_2>, ...) VALUES (<value_1>, <value_2>, ...);
6. NAME FORMAT: REJ MEDIODIA 2015.01.23 11:43AM 
*********** NO STATEMENTS BELOW THIS. **************/

