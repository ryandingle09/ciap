-- RYAN DINGLE 2016.08.31 11:59AM

CREATE DATABASE  IF NOT EXISTS `bas` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `bas`;
-- MySQL dump 10.13  Distrib 5.7.13, for Linux (x86_64)
--
-- Host: localhost    Database: bas
-- ------------------------------------------------------
-- Server version	5.7.13-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `arbitrators`
--

DROP TABLE IF EXISTS `arbitrators`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arbitrators` (
  `arbitrator_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`arbitrator_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `billings`
--

DROP TABLE IF EXISTS `billings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `billings` (
  `billing_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `obligation_id` int(10) unsigned NOT NULL,
  `bill_no` varchar(100) NOT NULL,
  `bill_date` date NOT NULL,
  `due_date` date DEFAULT NULL,
  `amount_due` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`billing_id`),
  KEY `fk_billings_obligations1_idx` (`obligation_id`),
  CONSTRAINT `fk_billings_obligations1` FOREIGN KEY (`obligation_id`) REFERENCES `obligations` (`obligation_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_arbitrator_deductions`
--

DROP TABLE IF EXISTS `case_arbitrator_deductions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_arbitrator_deductions` (
  `arbitrator_payment_id` int(10) unsigned NOT NULL,
  `arbitrator_id` int(10) unsigned NOT NULL,
  `deduction_type_id` int(10) unsigned NOT NULL,
  `amount` decimal(25,2) NOT NULL,
  PRIMARY KEY (`arbitrator_payment_id`,`arbitrator_id`,`deduction_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_arbitrator_fee_shares`
--

DROP TABLE IF EXISTS `case_arbitrator_fee_shares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_arbitrator_fee_shares` (
  `arbitrator_payment_id` int(10) unsigned NOT NULL,
  `arbitrator_id` int(10) unsigned NOT NULL,
  `stage_activity_id` int(10) unsigned NOT NULL,
  `percent` decimal(5,2) NOT NULL DEFAULT '0.00',
  `total_amount` decimal(25,2) DEFAULT NULL,
  PRIMARY KEY (`arbitrator_payment_id`,`arbitrator_id`,`stage_activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `case_arbitrator_payments`
--

DROP TABLE IF EXISTS `case_arbitrator_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `case_arbitrator_payments` (
  `arbitrator_payment_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `case_id` int(10) unsigned NOT NULL,
  `proceeding_stage_id` int(10) unsigned NOT NULL,
  `final_flag` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`arbitrator_payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `cases`
--

DROP TABLE IF EXISTS `cases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cases` (
  `case_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `case_no` char(7) NOT NULL,
  `case_title` text NOT NULL,
  `created_by` int(10) unsigned DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` int(10) unsigned DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL,
  PRIMARY KEY (`case_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `checks`
--

DROP TABLE IF EXISTS `checks`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `checks` (
  `check_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `disbursement_id` int(10) unsigned NOT NULL,
  `mode_of_payment` int(11) NOT NULL,
  `check_date` date NOT NULL,
  `check_no` varchar(100) NOT NULL,
  `check_amount` decimal(25,2) unsigned NOT NULL,
  `check_status_id` int(10) unsigned NOT NULL,
  `remarks` text,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` date NOT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`check_id`),
  KEY `fk_checks_disbursements1_idx` (`disbursement_id`),
  KEY `fk_checks_param_check_status1_idx` (`check_status_id`),
  CONSTRAINT `fk_checks_disbursements1` FOREIGN KEY (`disbursement_id`) REFERENCES `disbursements` (`disbursement_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_checks_param_check_status1` FOREIGN KEY (`check_status_id`) REFERENCES `param_check_status` (`check_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `disbursement_charges`
--

DROP TABLE IF EXISTS `disbursement_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disbursement_charges` (
  `disbursement_id` int(10) unsigned NOT NULL,
  `line_item_id` int(10) unsigned DEFAULT NULL,
  `account_code` varchar(100) NOT NULL,
  `obligated_amount` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `disbursed_amount` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `base_tax_amount` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `ewt_rate` decimal(5,2) unsigned NOT NULL DEFAULT '0.00',
  `ewt_amount` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `vat_rate` decimal(5,2) unsigned NOT NULL DEFAULT '0.00',
  `vat_amount` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  KEY `fk_disbursement_charges_disbursements1_idx` (`disbursement_id`),
  KEY `fk_disbursement_charges_line_item_accounts1_idx` (`line_item_id`,`account_code`),
  CONSTRAINT `fk_disbursement_charges_disbursements1` FOREIGN KEY (`disbursement_id`) REFERENCES `disbursements` (`disbursement_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_disbursement_charges_line_item_accounts1` FOREIGN KEY (`line_item_id`, `account_code`) REFERENCES `line_item_accounts` (`line_item_id`, `account_code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `disbursement_deductions`
--

DROP TABLE IF EXISTS `disbursement_deductions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disbursement_deductions` (
  `deduction_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `disbursement_id` int(10) unsigned NOT NULL,
  `deduction` varchar(100) NOT NULL,
  `deduction_amount` decimal(25,2) NOT NULL,
  PRIMARY KEY (`deduction_id`),
  KEY `fk_disbursement_deductions_disbursements1_idx` (`disbursement_id`),
  CONSTRAINT `fk_disbursement_deductions_disbursements1` FOREIGN KEY (`disbursement_id`) REFERENCES `disbursements` (`disbursement_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `disbursements`
--

DROP TABLE IF EXISTS `disbursements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `disbursements` (
  `disbursement_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dv_no` varchar(100) DEFAULT NULL,
  `dv_date` date DEFAULT NULL,
  `disbursement_fund_id` int(10) unsigned NOT NULL,
  `obligation_id` int(10) unsigned DEFAULT NULL,
  `billing_id` int(10) unsigned DEFAULT NULL,
  `payee_id` int(10) unsigned DEFAULT NULL,
  `case_no` varchar(100) DEFAULT NULL,
  `proceeding_stage_id` int(10) unsigned DEFAULT NULL,
  `arbitrator_id` int(10) unsigned DEFAULT NULL,
  `disbursed_amount` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `vat_rate` decimal(5,2) unsigned NOT NULL DEFAULT '0.00',
  `vat_amount` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `ewt_rate` decimal(5,2) unsigned NOT NULL DEFAULT '0.00',
  `wht_amount` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `disbursement_status_id` int(10) unsigned NOT NULL DEFAULT '0',
  `payee_address` text,
  `particulars` text,
  `remarks` text,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`disbursement_id`),
  KEY `fk_disbursements_obligations1_idx` (`obligation_id`),
  KEY `fk_disbursements_billings1_idx` (`billing_id`),
  KEY `fk_disbursements_param_disbursement_status1_idx` (`disbursement_status_id`),
  KEY `fk_disbursements_param_disbursement_funds1_idx` (`disbursement_fund_id`),
  KEY `fk_disbursements_param_payees1_idx` (`payee_id`),
  KEY `fk_disbursements_param_proceeding_stages1_idx` (`proceeding_stage_id`),
  KEY `fk_disbursements_arbitrators1_idx` (`arbitrator_id`),
  CONSTRAINT `fk_disbursements_arbitrators1` FOREIGN KEY (`arbitrator_id`) REFERENCES `arbitrators` (`arbitrator_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_disbursements_billings1` FOREIGN KEY (`billing_id`) REFERENCES `billings` (`billing_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_disbursements_obligations1` FOREIGN KEY (`obligation_id`) REFERENCES `obligations` (`obligation_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_disbursements_param_disbursement_funds1` FOREIGN KEY (`disbursement_fund_id`) REFERENCES `param_disbursement_funds` (`disbursement_fund_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_disbursements_param_disbursement_status1` FOREIGN KEY (`disbursement_status_id`) REFERENCES `param_disbursement_status` (`disbursement_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_disbursements_param_payees1` FOREIGN KEY (`payee_id`) REFERENCES `param_payees` (`payee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_disbursements_param_proceeding_stages1` FOREIGN KEY (`proceeding_stage_id`) REFERENCES `param_proceeding_stages` (`proceeding_stage_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `fund_sources`
--

DROP TABLE IF EXISTS `fund_sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fund_sources` (
  `year` int(4) unsigned NOT NULL,
  `source_type_num` int(10) unsigned NOT NULL,
  `final_flag` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`year`,`source_type_num`),
  KEY `fk_fund_sources_param_source_types1_idx` (`source_type_num`),
  CONSTRAINT `fk_fund_sources_param_source_types1` FOREIGN KEY (`source_type_num`) REFERENCES `param_source_types` (`source_type_num`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `line_item_accounts`
--

DROP TABLE IF EXISTS `line_item_accounts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `line_item_accounts` (
  `line_item_id` int(10) unsigned NOT NULL,
  `account_code` varchar(100) NOT NULL,
  `appropriation_amount` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `allotment_amount` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `realigned_amount` decimal(25,2) NOT NULL,
  `obligated_amount` decimal(25,2) NOT NULL,
  `disbursed_amount` decimal(25,2) NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`line_item_id`,`account_code`),
  KEY `fk_line_item_accounts_line_items1_idx` (`line_item_id`),
  KEY `fk_line_item_accounts_param_account_codes1_idx` (`account_code`),
  CONSTRAINT `fk_line_item_accounts_line_items1` FOREIGN KEY (`line_item_id`) REFERENCES `line_items` (`line_item_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_line_item_accounts_param_account_codes1` FOREIGN KEY (`account_code`) REFERENCES `param_account_codes` (`account_code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `line_item_offices`
--

DROP TABLE IF EXISTS `line_item_offices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `line_item_offices` (
  `line_item_id` int(10) unsigned NOT NULL,
  `office_num` int(10) unsigned NOT NULL,
  `total_appropriation` decimal(25,2) NOT NULL DEFAULT '0.00',
  `ps_appropriation` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `mooe_appropriation` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `co_appropriation` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `total_allotment` decimal(25,2) NOT NULL DEFAULT '0.00',
  `ps_allotment` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `mooe_allotment` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `co_allotment` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `total_obligated_amount` decimal(25,2) NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`line_item_id`,`office_num`),
  KEY `fk_line_item_offices_param_offices1_idx` (`office_num`),
  KEY `fk_line_item_offices_line_items1_idx` (`line_item_id`),
  CONSTRAINT `fk_line_item_offices_line_items1` FOREIGN KEY (`line_item_id`) REFERENCES `line_items` (`line_item_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_line_item_offices_param_offices1` FOREIGN KEY (`office_num`) REFERENCES `param_offices` (`office_num`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `line_item_realign_details`
--

DROP TABLE IF EXISTS `line_item_realign_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `line_item_realign_details` (
  `realign_id` int(10) unsigned NOT NULL,
  `realigned_amount` decimal(25,2) NOT NULL,
  `source_line_item_id` int(10) unsigned NOT NULL,
  `source_account_code` varchar(100) NOT NULL,
  `recipient_line_item_id` int(10) unsigned NOT NULL,
  `recipient_account_code` varchar(100) NOT NULL,
  KEY `fk_line_item_realign_details_line_item_realigns1_idx` (`realign_id`),
  KEY `fk_line_item_realign_details_line_item_accounts1_idx` (`source_line_item_id`,`source_account_code`),
  KEY `fk_line_item_realign_details_line_item_accounts2_idx` (`recipient_line_item_id`,`recipient_account_code`),
  CONSTRAINT `fk_line_item_realign_details_line_item_accounts1` FOREIGN KEY (`source_line_item_id`, `source_account_code`) REFERENCES `line_item_accounts` (`line_item_id`, `account_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_line_item_realign_details_line_item_accounts2` FOREIGN KEY (`recipient_line_item_id`, `recipient_account_code`) REFERENCES `line_item_accounts` (`line_item_id`, `account_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_line_item_realign_details_line_item_realigns1` FOREIGN KEY (`realign_id`) REFERENCES `line_item_realigns` (`realign_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `line_item_realigns`
--

DROP TABLE IF EXISTS `line_item_realigns`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `line_item_realigns` (
  `realign_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `year` int(4) unsigned NOT NULL,
  `source_type_num` int(10) unsigned NOT NULL,
  `release_date` date NOT NULL,
  `reference_no` varchar(100) NOT NULL,
  `posted_flag` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`realign_id`),
  KEY `fk_line_item_realigns_fund_sources1_idx` (`year`,`source_type_num`),
  CONSTRAINT `fk_line_item_realigns_fund_sources1` FOREIGN KEY (`year`, `source_type_num`) REFERENCES `fund_sources` (`year`, `source_type_num`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `line_item_release_details`
--

DROP TABLE IF EXISTS `line_item_release_details`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `line_item_release_details` (
  `release_id` int(10) unsigned NOT NULL,
  `account_code` varchar(100) NOT NULL,
  `allotment_amount` decimal(25,2) NOT NULL,
  PRIMARY KEY (`release_id`,`account_code`),
  KEY `fk_line_item_release_details_line_item_releases1_idx` (`release_id`),
  KEY `fk_line_item_release_details_param_account_codes1_idx` (`account_code`),
  CONSTRAINT `fk_line_item_release_details_line_item_releases1` FOREIGN KEY (`release_id`) REFERENCES `line_item_releases` (`release_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_line_item_release_details_param_account_codes1` FOREIGN KEY (`account_code`) REFERENCES `param_account_codes` (`account_code`) ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `line_item_releases`
--

DROP TABLE IF EXISTS `line_item_releases`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `line_item_releases` (
  `release_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `line_item_id` int(10) unsigned NOT NULL,
  `reference_no` varchar(100) NOT NULL,
  `reference_date` date DEFAULT NULL,
  `total_release_amount` decimal(25,2) NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`release_id`),
  KEY `fk_line_item_releases_line_items1_idx` (`line_item_id`),
  CONSTRAINT `fk_line_item_releases_line_items1` FOREIGN KEY (`line_item_id`) REFERENCES `line_items` (`line_item_id`) ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `line_items`
--

DROP TABLE IF EXISTS `line_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `line_items` (
  `line_item_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `year` int(4) unsigned NOT NULL,
  `source_type_num` int(10) unsigned NOT NULL,
  `parent_line_item_id` int(10) unsigned DEFAULT NULL,
  `line_item_code` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_cs DEFAULT NULL,
  `line_item_name` varchar(255) NOT NULL,
  `total_appropriation` decimal(25,2) NOT NULL DEFAULT '0.00',
  `ps_appropriation` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `mooe_appropriation` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `co_appropriation` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `total_allotment` decimal(25,2) NOT NULL DEFAULT '0.00',
  `ps_allotment` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `mooe_allotment` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `co_allotment` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `ps_appropriation_realigned` decimal(25,2) NOT NULL DEFAULT '0.00',
  `mooe_appropriation_realigned` decimal(25,2) NOT NULL DEFAULT '0.00',
  `co_appropriation_realigned` decimal(25,2) NOT NULL DEFAULT '0.00',
  `ps_allotment_realigned` decimal(25,2) NOT NULL DEFAULT '0.00',
  `mooe_allotment_realigned` decimal(25,2) NOT NULL DEFAULT '0.00',
  `co_allotment_realigned` decimal(25,2) NOT NULL DEFAULT '0.00',
  `ps_obligation` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `mooe_obligation` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `co_obligation` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `ps_disbursement` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `mooe_disbursement` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `co_disbursement` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `created_by` int(10) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(10) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`line_item_id`),
  KEY `fk_line_items_line_items1_idx` (`parent_line_item_id`),
  KEY `fk_line_items_fund_sources1_idx` (`year`,`source_type_num`),
  CONSTRAINT `fk_line_items_fund_sources1` FOREIGN KEY (`year`, `source_type_num`) REFERENCES `fund_sources` (`year`, `source_type_num`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_line_items_line_items1` FOREIGN KEY (`parent_line_item_id`) REFERENCES `line_items` (`line_item_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `obligation_charges`
--

DROP TABLE IF EXISTS `obligation_charges`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obligation_charges` (
  `obligation_id` int(11) unsigned NOT NULL,
  `line_item_id` int(10) unsigned NOT NULL,
  `account_code` varchar(100) NOT NULL,
  `office_num` varchar(100) NOT NULL,
  `requested_amount` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `obligated_amount` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `disbursed_amount` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`obligation_id`,`line_item_id`,`account_code`,`office_num`),
  KEY `fk_obligation_charges_obligations1_idx` (`obligation_id`),
  KEY `fk_obligation_charges_line_item_accounts1_idx` (`line_item_id`,`account_code`),
  CONSTRAINT `fk_obligation_charges_line_item_accounts1` FOREIGN KEY (`line_item_id`, `account_code`) REFERENCES `line_item_accounts` (`line_item_id`, `account_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_obligation_charges_obligations1` FOREIGN KEY (`obligation_id`) REFERENCES `obligations` (`obligation_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `obligation_signatories`
--

DROP TABLE IF EXISTS `obligation_signatories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obligation_signatories` (
  `signatory_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `obligation_id` int(10) unsigned NOT NULL,
  `role` enum('R','C') NOT NULL DEFAULT 'R' COMMENT 'R - Requested By; C - Certify By',
  `name` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL,
  PRIMARY KEY (`signatory_id`),
  KEY `fk_obligation_signatories_obligations1_idx` (`obligation_id`),
  CONSTRAINT `fk_obligation_signatories_obligations1` FOREIGN KEY (`obligation_id`) REFERENCES `obligations` (`obligation_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `obligations`
--

DROP TABLE IF EXISTS `obligations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `obligations` (
  `obligation_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `manual_flag` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `os_no` varchar(100) DEFAULT NULL,
  `os_date` date DEFAULT NULL,
  `requested_date` date NOT NULL,
  `payee_id` int(10) unsigned NOT NULL,
  `address` text,
  `particulars` text NOT NULL,
  `year` int(4) unsigned NOT NULL,
  `source_type_num` int(10) unsigned NOT NULL,
  `requested_amount` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `obligated_amount` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `disbursed_amount` decimal(25,2) unsigned NOT NULL DEFAULT '0.00',
  `obligation_status_id` int(10) unsigned NOT NULL,
  `remarks` text,
  PRIMARY KEY (`obligation_id`),
  KEY `fk_obligations_fund_sources1_idx` (`year`,`source_type_num`),
  KEY `fk_obligations_param_obligation_status1_idx` (`obligation_status_id`),
  KEY `fk_obligations_param_payees1_idx` (`payee_id`),
  CONSTRAINT `fk_obligations_fund_sources1` FOREIGN KEY (`year`, `source_type_num`) REFERENCES `fund_sources` (`year`, `source_type_num`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_obligations_param_obligation_status1` FOREIGN KEY (`obligation_status_id`) REFERENCES `param_obligation_status` (`obligation_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_obligations_param_payees1` FOREIGN KEY (`payee_id`) REFERENCES `param_payees` (`payee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=111 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `param_account_codes`
--

DROP TABLE IF EXISTS `param_account_codes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_account_codes` (
  `account_code` varchar(100) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `parent_account_code` varchar(100) DEFAULT NULL,
  `active_flag` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  PRIMARY KEY (`account_code`),
  UNIQUE KEY `account_code` (`account_code`),
  KEY `fk_param_account_codes_param_account_codes1_idx` (`parent_account_code`),
  CONSTRAINT `fk_param_account_codes_param_account_codes1` FOREIGN KEY (`parent_account_code`) REFERENCES `param_account_codes` (`account_code`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `param_check_mode_payments`
--

DROP TABLE IF EXISTS `param_check_mode_payments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_check_mode_payments` (
  `mode_payment_id` int(11) NOT NULL AUTO_INCREMENT,
  `mode_type` varchar(50) NOT NULL,
  PRIMARY KEY (`mode_payment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `param_check_status`
--

DROP TABLE IF EXISTS `param_check_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_check_status` (
  `check_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `status_name` varchar(50) NOT NULL,
  PRIMARY KEY (`check_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `param_deduction_types`
--

DROP TABLE IF EXISTS `param_deduction_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_deduction_types` (
  `deduction_id` int(11) NOT NULL AUTO_INCREMENT,
  `deduction_name` varchar(20) NOT NULL,
  `deduction_code` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`deduction_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `param_disbursement_funds`
--

DROP TABLE IF EXISTS `param_disbursement_funds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_disbursement_funds` (
  `disbursement_fund_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `fund_name` varchar(50) NOT NULL COMMENT 'GAA, CMDF, CIAC',
  PRIMARY KEY (`disbursement_fund_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `param_disbursement_status`
--

DROP TABLE IF EXISTS `param_disbursement_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_disbursement_status` (
  `disbursement_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `disbursement_status` varchar(100) NOT NULL,
  PRIMARY KEY (`disbursement_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `param_obligation_status`
--

DROP TABLE IF EXISTS `param_obligation_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_obligation_status` (
  `obligation_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `obligation_status` varchar(100) NOT NULL,
  PRIMARY KEY (`obligation_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `param_offices`
--

DROP TABLE IF EXISTS `param_offices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_offices` (
  `office_num` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_office_num` int(10) unsigned DEFAULT NULL,
  `office_name` varchar(255) NOT NULL,
  PRIMARY KEY (`office_num`),
  KEY `fk_param_offices_param_offices1_idx` (`parent_office_num`),
  CONSTRAINT `fk_param_offices_param_offices1` FOREIGN KEY (`parent_office_num`) REFERENCES `param_offices` (`office_num`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=180 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `param_payees`
--

DROP TABLE IF EXISTS `param_payees`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_payees` (
  `payee_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `payee_name` varchar(255) NOT NULL,
  `payee_address` text,
  `payee_tin` varchar(100) DEFAULT NULL,
  `vatable_flag` tinyint(1) NOT NULL DEFAULT '1',
  `active_flag` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`payee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `param_proceeding_stage_activities`
--

DROP TABLE IF EXISTS `param_proceeding_stage_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_proceeding_stage_activities` (
  `proceeding_stage_activity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `proceeding_stage_id` int(10) unsigned NOT NULL,
  `stage_activity` varchar(255) NOT NULL,
  PRIMARY KEY (`proceeding_stage_activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `param_proceeding_stages`
--

DROP TABLE IF EXISTS `param_proceeding_stages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_proceeding_stages` (
  `proceeding_stage_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `proceeding_stage` varchar(100) NOT NULL,
  PRIMARY KEY (`proceeding_stage_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `param_source_types`
--

DROP TABLE IF EXISTS `param_source_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `param_source_types` (
  `source_type_num` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `source_type_name` varchar(100) NOT NULL,
  `active_flag` tinyint(1) NOT NULL,
  PRIMARY KEY (`source_type_num`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-31 11:35:14
