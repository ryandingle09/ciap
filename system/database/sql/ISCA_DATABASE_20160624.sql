SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

DROP SCHEMA IF EXISTS `isca` ;
CREATE SCHEMA IF NOT EXISTS `isca` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci ;
USE `isca` ;

-- -----------------------------------------------------
-- Table `isca`.`param_case_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_case_status` (
  `case_status_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `case_status_name` VARCHAR(45) NULL,
  `case_status_desc` VARCHAR(255) NULL,
  PRIMARY KEY (`case_status_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_case_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_case_types` (
  `case_type_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `case_type_name` VARCHAR(45) NULL,
  PRIMARY KEY (`case_type_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_areas`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_areas` (
  `area_id` INT NOT NULL,
  `area_name` VARCHAR(45) NULL,
  PRIMARY KEY (`area_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_regions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_regions` (
  `region_id` INT NOT NULL AUTO_INCREMENT,
  `area_id` INT NOT NULL,
  `region_name` VARCHAR(100) NULL,
  PRIMARY KEY (`region_id`),
  INDEX `fk_param_regions_param_areas -1_idx` (`area_id` ASC),
  CONSTRAINT `fk_param_regions_param_areas -1`
    FOREIGN KEY (`area_id`)
    REFERENCES `isca`.`param_areas` (`area_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_arbitrator_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_arbitrator_status` (
  `arbitrator_status_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `arbitrator_status_name` VARCHAR(45) NULL,
  PRIMARY KEY (`arbitrator_status_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`arbitrators`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`arbitrators` (
  `arbitrator_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NULL,
  `last_name` VARCHAR(45) NULL,
  `middle_initial` VARCHAR(45) NULL,
  `region_id` INT NOT NULL,
  `arbitrator_status_id` INT UNSIGNED NOT NULL,
  `accreditation_year` YEAR NULL,
  `mace_pts` INT NULL,
  `sole_flag` TINYINT(1) NULL,
  `mace_exempted_flag` TINYINT(1) NULL,
  `resume_attachment` VARCHAR(255) NULL,
  `created_by` INT UNSIGNED NULL,
  `created_date` DATETIME NULL,
  `last_modified_by` INT UNSIGNED NULL DEFAULT NULL,
  `last_modified_date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`arbitrator_id`),
  INDEX `fk_arbitrators -_param_regions -1_idx` (`region_id` ASC),
  INDEX `fk_arbitrators -_param_arbitrator_status1_idx` (`arbitrator_status_id` ASC),
  CONSTRAINT `fk_arbitrators -_param_regions -1`
    FOREIGN KEY (`region_id`)
    REFERENCES `isca`.`param_regions` (`region_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_arbitrators -_param_arbitrator_status1`
    FOREIGN KEY (`arbitrator_status_id`)
    REFERENCES `isca`.`param_arbitrator_status` (`arbitrator_status_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`cases`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`cases` (
  `case_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `case_no` CHAR(7) NULL,
  `case_title` VARCHAR(100) NULL,
  `short_title` VARCHAR(45) NULL,
  `case_staff` INT UNSIGNED NULL,
  `case_status` INT UNSIGNED NULL,
  `appeal_flag` TINYINT(1) NULL,
  `case_nature` ENUM('G', 'P') NULL COMMENT 'G - Government Contract, P - Private Contract',
  `case_type` INT UNSIGNED NULL,
  `sheriff_date` DATE NULL,
  `sid_final` DECIMAL(12,5) NULL,
  `target_award_date` DATE NULL,
  `gross_award` DECIMAL(12,5) NULL,
  `award_date` DATE NULL,
  `net_award` DECIMAL(12,5) NULL,
  `settlement_mode` ENUM('A', 'M') NULL COMMENT 'A - Arbitration, M - Mediation',
  `closed_flag` TINYINT(1) NULL DEFAULT 0,
  `mediator_id` INT UNSIGNED NULL,
  `created_by` INT UNSIGNED NULL,
  `created_date` DATETIME NULL,
  `last_modified_by` INT UNSIGNED NULL DEFAULT NULL,
  `last_modified_date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`case_id`),
  INDEX `fk_case_param_case_status1_idx` (`case_status` ASC),
  INDEX `fk_case_param_case_type1_idx` (`case_type` ASC),
  INDEX `fk_cases -_arbitrators -1_idx` (`mediator_id` ASC),
  CONSTRAINT `fk_case_param_case_status1`
    FOREIGN KEY (`case_status`)
    REFERENCES `isca`.`param_case_status` (`case_status_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_case_param_case_type1`
    FOREIGN KEY (`case_type`)
    REFERENCES `isca`.`param_case_types` (`case_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_cases -_arbitrators -1`
    FOREIGN KEY (`mediator_id`)
    REFERENCES `isca`.`arbitrators` (`arbitrator_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`case_nature`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`case_nature` (
  `case_id` INT UNSIGNED NOT NULL,
  `case_nature_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`case_id`, `case_nature_id`),
  CONSTRAINT `fk_case_natures1`
    FOREIGN KEY (`case_id`)
    REFERENCES `isca`.`cases` (`case_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_appeal_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_appeal_types` (
  `appeal_type_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `appeal_type_name` VARCHAR(45) NULL,
  PRIMARY KEY (`appeal_type_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_appeal_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_appeal_status` (
  `appeal_status_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `appeal_status_name` VARCHAR(45) NULL,
  `appeal_status_desc` VARCHAR(255) NULL,
  PRIMARY KEY (`appeal_status_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`case_appeals`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`case_appeals` (
  `case_appeal_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `case_id` INT UNSIGNED NOT NULL,
  `appeal_date` DATE NULL,
  `appeal_type` INT UNSIGNED NOT NULL,
  `appeal_status` INT UNSIGNED NOT NULL,
  `gr_no` VARCHAR(45) NULL COMMENT 'G.R. No. from Court',
  PRIMARY KEY (`case_appeal_id`),
  INDEX `fk_case_appeals1_idx` (`case_id` ASC),
  INDEX `fk_case_appeals -_param_appeal_types -1_idx` (`appeal_type` ASC),
  INDEX `fk_case_appeals -_param_appeal_status -1_idx` (`appeal_status` ASC),
  CONSTRAINT `fk_case_appeals1`
    FOREIGN KEY (`case_id`)
    REFERENCES `isca`.`cases` (`case_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_case_appeals -_param_appeal_types -1`
    FOREIGN KEY (`appeal_type`)
    REFERENCES `isca`.`param_appeal_types` (`appeal_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_case_appeals -_param_appeal_status -1`
    FOREIGN KEY (`appeal_status`)
    REFERENCES `isca`.`param_appeal_status` (`appeal_status_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`case_party`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`case_party` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `case_id` INT UNSIGNED NOT NULL,
  `party_id` INT UNSIGNED NOT NULL,
  `name` VARCHAR(100) NOT NULL,
  `address` TEXT NULL,
  `contact_no` VARCHAR(100) NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `fk_case_parties1`
    FOREIGN KEY (`case_id`)
    REFERENCES `isca`.`cases` (`case_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_case_activity`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_case_activity` (
  `case_activity_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `case_activity` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`case_activity_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`case_arbitration_details`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`case_arbitration_details` (
  `case_id` INT UNSIGNED NOT NULL,
  `arbitration_mode` ENUM('S', 'T') NULL COMMENT 'S - Sole Arbitrator, T - Arbitral Tribunal',
  `number_disputants` INT NULL,
  `case_activity_id` INT UNSIGNED NULL,
  `arbitration_clause_flag` TINYINT(1) NULL,
  INDEX `fk_case_arbitration_details_param_case_activities1_idx` (`case_activity_id` ASC),
  PRIMARY KEY (`case_id`),
  CONSTRAINT `fk_case_arbitration_details_param_case_activities1`
    FOREIGN KEY (`case_activity_id`)
    REFERENCES `isca`.`param_case_activity` (`case_activity_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_case_arbitration_detail -_case -1`
    FOREIGN KEY (`case_id`)
    REFERENCES `isca`.`cases` (`case_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`case_arbitrators`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`case_arbitrators` (
  `case_id` INT UNSIGNED NOT NULL,
  `arbitrator_id` INT UNSIGNED NOT NULL,
  `position` ENUM('S', 'C', 'M1', 'M2') NOT NULL DEFAULT 'S' COMMENT 'S - Sole Arbitrator, C - Chairman, M1 - Member 1,  M2 - Member2',
  `sharing_fees` DECIMAL(5,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`case_id`, `arbitrator_id`),
  INDEX `fk_case_arbitrators_arbitrators1_idx` (`arbitrator_id` ASC),
  CONSTRAINT `fk_case_arbitrators1`
    FOREIGN KEY (`case_id`)
    REFERENCES `isca`.`cases` (`case_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_case_arbitrators_arbitrators1`
    FOREIGN KEY (`arbitrator_id`)
    REFERENCES `isca`.`arbitrators` (`arbitrator_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_task_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_task_status` (
  `task_status_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `task_status_name` VARCHAR(45) NULL,
  PRIMARY KEY (`task_status_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`case_tasks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`case_tasks` (
  `task_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `case_id` INT UNSIGNED NOT NULL,
  `task_name` VARCHAR(45) NULL,
  `turnaround_time` INT NULL,
  `task_status_id` INT UNSIGNED NOT NULL,
  `assigned_by` INT NULL,
  `assigned_to` INT NULL,
  `processed_by` INT NULL,
  `processed_date` DATE NULL,
  `created_by` INT UNSIGNED NULL,
  `created_date` DATETIME NULL,
  `last_modified_by` INT UNSIGNED NULL DEFAULT NULL,
  `last_modified_date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`task_id`),
  INDEX `fk_case_tasks -_cases -1_idx` (`case_id` ASC),
  INDEX `fk_case_tasks -_param_task_status1_idx` (`task_status_id` ASC),
  CONSTRAINT `fk_case_tasks -_cases -1`
    FOREIGN KEY (`case_id`)
    REFERENCES `isca`.`cases` (`case_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_case_tasks -_param_task_status1`
    FOREIGN KEY (`task_status_id`)
    REFERENCES `isca`.`param_task_status` (`task_status_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`case_task_remarks`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`case_task_remarks` (
  `remarks_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `task_id` INT UNSIGNED NOT NULL,
  `remarks` TEXT NULL,
  `created_by` INT UNSIGNED NULL,
  `created_date` DATETIME NULL,
  `last_modified_by` INT UNSIGNED NULL DEFAULT NULL,
  `last_modified_date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`remarks_id`),
  INDEX `fk_case_task_remarks_case_tasks -1_idx` (`task_id` ASC),
  CONSTRAINT `fk_case_task_remarks_case_tasks -1`
    FOREIGN KEY (`task_id`)
    REFERENCES `isca`.`case_tasks` (`task_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_document_types`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_document_types` (
  `document_type_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `document_type` VARCHAR(100) NOT NULL,
  `display_party` TINYINT(1) NOT NULL DEFAULT 1,
  PRIMARY KEY (`document_type_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_parties`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_parties` (
  `party_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `party_name` VARCHAR(45) NULL,
  PRIMARY KEY (`party_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`case_task_documents`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`case_task_documents` (
  `document_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `task_id` INT UNSIGNED NOT NULL,
  `received_date` DATE NOT NULL,
  `document_type_id` INT UNSIGNED NOT NULL,
  `party_id` INT UNSIGNED NOT NULL,
  `document_desc` TEXT NULL,
  `file` VARCHAR(255) NULL,
  `created_by` INT UNSIGNED NULL,
  `created_date` DATETIME NULL,
  `last_modified_by` INT UNSIGNED NULL DEFAULT NULL,
  `last_modified_date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`document_id`),
  INDEX `fk_case_task_attachments1_idx` (`task_id` ASC),
  INDEX `fk_case_task_attachments_param_document_types1_idx` (`document_type_id` ASC),
  INDEX `fk_case_task_documents -_param_party_types -1_idx` (`party_id` ASC),
  CONSTRAINT `fk_case_task_attachments1`
    FOREIGN KEY (`task_id`)
    REFERENCES `isca`.`case_tasks` (`task_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_case_task_attachments_param_document_types1`
    FOREIGN KEY (`document_type_id`)
    REFERENCES `isca`.`param_document_types` (`document_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_case_task_documents -_param_party_types -1`
    FOREIGN KEY (`party_id`)
    REFERENCES `isca`.`param_parties` (`party_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_professions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_professions` (
  `profession_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `profession_name` VARCHAR(100) NULL,
  PRIMARY KEY (`profession_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`arbitrator_professions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`arbitrator_professions` (
  `arbitrator_id` INT UNSIGNED NOT NULL,
  `profession_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`arbitrator_id`, `profession_id`),
  INDEX `fk_arbitrator_professions -_param_professions -1_idx` (`profession_id` ASC),
  CONSTRAINT `fk_arbitrator_professions -_arbitrators -1`
    FOREIGN KEY (`arbitrator_id`)
    REFERENCES `isca`.`arbitrators` (`arbitrator_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_arbitrator_professions -_param_professions -1`
    FOREIGN KEY (`profession_id`)
    REFERENCES `isca`.`param_professions` (`profession_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`mace`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`mace` (
  `mace_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `date` DATE NULL,
  `time_start` TIME NULL,
  `time_end` TIME NULL,
  `venue` VARCHAR(45) NULL,
  `trainers` VARCHAR(100) NULL,
  `remarks` TEXT NULL,
  PRIMARY KEY (`mace_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`mace_arbitrator`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`mace_arbitrator` (
  `mace_id` INT UNSIGNED NOT NULL,
  `arbitrator_id` INT UNSIGNED NOT NULL,
  `time_in` TIME NULL,
  `time_out` TIME NULL,
  `speaker_flag` TINYINT(1) NULL,
  `mace_pts` DECIMAL(5,2) NULL,
  PRIMARY KEY (`mace_id`, `arbitrator_id`),
  INDEX `fk_mace_arbitrators2_idx` (`arbitrator_id` ASC),
  CONSTRAINT `fk_mace_arbitrators1`
    FOREIGN KEY (`mace_id`)
    REFERENCES `isca`.`mace` (`mace_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_mace_arbitrators2`
    FOREIGN KEY (`arbitrator_id`)
    REFERENCES `isca`.`arbitrators` (`arbitrator_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_book_classifications`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_book_classifications` (
  `book_class_no` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `book_class_name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`book_class_no`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_book_authors`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_book_authors` (
  `book_author_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `book_author_name` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`book_author_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`books`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`books` (
  `book_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `code_no` VARCHAR(45) NULL,
  `book_class_no` INT UNSIGNED NOT NULL,
  `title` VARCHAR(100) NOT NULL,
  `book_author_id` INT UNSIGNED NOT NULL,
  `publishing_house` VARCHAR(100) NULL,
  `publishing_date` DATE NULL,
  `copyright_date` DATE NULL,
  `edition` VARCHAR(45) NULL,
  `quantity` INT NULL,
  `contents` TEXT NULL,
  `created_by` INT UNSIGNED NULL,
  `created_date` DATETIME NULL,
  `last_modified_by` INT UNSIGNED NULL DEFAULT NULL,
  `last_modified_date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`book_id`),
  INDEX `fk_books -_param_book_classifications -1_idx` (`book_class_no` ASC),
  INDEX `fk_books -_param_book_authors -1_idx` (`book_author_id` ASC),
  CONSTRAINT `fk_books -_param_book_classifications -1`
    FOREIGN KEY (`book_class_no`)
    REFERENCES `isca`.`param_book_classifications` (`book_class_no`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_books -_param_book_authors -1`
    FOREIGN KEY (`book_author_id`)
    REFERENCES `isca`.`param_book_authors` (`book_author_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_book_subject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_book_subject` (
  `subject_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `subject` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`subject_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`book_subject`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`book_subject` (
  `book_id` INT UNSIGNED NOT NULL,
  `subject_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`book_id`, `subject_id`),
  CONSTRAINT `fk_book_subjects1`
    FOREIGN KEY (`book_id`)
    REFERENCES `isca`.`books` (`book_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_book_subjects2`
    FOREIGN KEY ()
    REFERENCES `isca`.`param_book_subject` ()
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`meetings`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`meetings` (
  `meeting_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `meeting_date` DATE NULL,
  `meeting_name` VARCHAR(100) NULL,
  `venue` VARCHAR(100) NULL,
  `agenda` TEXT NULL,
  `created_by` INT UNSIGNED NULL,
  `created_date` DATETIME NULL,
  `last_modified_by` INT UNSIGNED NULL DEFAULT NULL,
  `last_modified_date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`meeting_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_meeting_attendees`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_meeting_attendees` (
  `attendee_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `attendee_name` VARCHAR(100) NULL,
  `attendee_position` VARCHAR(100) NULL,
  PRIMARY KEY (`attendee_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`meeting_attendees`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`meeting_attendees` (
  `meeting_id` INT UNSIGNED NOT NULL,
  `attendee_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`meeting_id`, `attendee_id`),
  INDEX `fk_meeting_attendees -_meetings -1_idx` (`meeting_id` ASC),
  INDEX `fk_meeting_attendees -_param_attendees1_idx` (`attendee_id` ASC),
  CONSTRAINT `fk_meeting_attendees -_meetings -1`
    FOREIGN KEY (`meeting_id`)
    REFERENCES `isca`.`meetings` (`meeting_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_meeting_attendees -_param_attendees1`
    FOREIGN KEY (`attendee_id`)
    REFERENCES `isca`.`param_meeting_attendees` (`attendee_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`meeting_minutes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`meeting_minutes` (
  `meeting_id` INT UNSIGNED NOT NULL,
  `time_started` TIME NULL,
  `time_ended` TIME NULL,
  `minutes` TEXT NULL,
  `certified_correct_by` INT NULL,
  `noted_by` INT NULL,
  `final_flag` TINYINT(1) NULL,
  `created_by` INT UNSIGNED NULL,
  `created_date` DATETIME NULL,
  `last_modified_by` INT UNSIGNED NULL DEFAULT NULL,
  `last_modified_date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`meeting_id`),
  CONSTRAINT `fk_meeting_minutes_meetings -1`
    FOREIGN KEY (`meeting_id`)
    REFERENCES `isca`.`meetings` (`meeting_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`policies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`policies` (
  `policy_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `policy_title` VARCHAR(200) NULL,
  `policy_attachment` VARCHAR(255) NULL,
  `created_by` INT UNSIGNED NULL,
  `created_date` DATETIME NULL,
  `last_modified_by` INT UNSIGNED NULL DEFAULT NULL,
  `last_modified_date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`policy_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_policy_category`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_policy_category` (
  `policy_category_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `policy_category` VARCHAR(45) NULL,
  PRIMARY KEY (`policy_category_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`deliveries`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`deliveries` (
  `delivery_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `control_no` INT(2) NULL,
  `delivery_mode` ENUM('C', 'P') NULL COMMENT 'C - Courier, P - Personnel Service',
  `messenger` VARCHAR(45) NULL,
  `delivery_date` DATE NULL,
  `delivery_status` ENUM('O', 'RC', 'RT', 'U') NULL,
  `addressee` VARCHAR(255) NULL,
  `case_id` INT UNSIGNED NOT NULL,
  `delivery_subject` VARCHAR(255) NULL,
  `delivery_remarks` TEXT NULL,
  `tracking_no` VARCHAR(45) NULL,
  `returned_date` DATE NULL,
  `return_reason` TEXT NULL,
  `received_date` DATE NULL,
  `received_by` VARCHAR(45) NULL,
  `undelivery_reason` TEXT NULL,
  `created_by` INT UNSIGNED NULL,
  `created_date` DATETIME NULL,
  `last_modified_by` INT UNSIGNED NULL DEFAULT NULL,
  `last_modified_date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`delivery_id`),
  INDEX `fk_deliveries -_case1_idx` (`case_id` ASC),
  CONSTRAINT `fk_deliveries -_case1`
    FOREIGN KEY (`case_id`)
    REFERENCES `isca`.`cases` (`case_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`delivery_reports`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`delivery_reports` (
  `delivery_report_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `created_date` DATE NULL,
  `delivery_date` DATE NULL,
  PRIMARY KEY (`delivery_report_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`case_arbitrator_nominees`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`case_arbitrator_nominees` (
  `case_id` INT UNSIGNED NOT NULL,
  `arbitrator_id` INT UNSIGNED NOT NULL,
  `party_type_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`case_id`, `arbitrator_id`, `party_type_id`),
  INDEX `fk_case_arbitrator_nominees_arbitrators -1_idx` (`arbitrator_id` ASC),
  INDEX `fk_case_arbitrator_nominees_param_party_types -1_idx` (`party_type_id` ASC),
  CONSTRAINT `fk_case_arbitrator_nominees_cases -1`
    FOREIGN KEY (`case_id`)
    REFERENCES `isca`.`cases` (`case_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_case_arbitrator_nominees_arbitrators -1`
    FOREIGN KEY (`arbitrator_id`)
    REFERENCES `isca`.`arbitrators` (`arbitrator_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_case_arbitrator_nominees_param_party_types -1`
    FOREIGN KEY (`party_type_id`)
    REFERENCES `isca`.`param_parties` (`party_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_case_schedule`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_case_schedule` (
  `case_schedule_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `case_schedule` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`case_schedule_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_payment_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_payment_type` (
  `payment_type_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `payment_type` VARCHAR(100) NULL COMMENT 'Initial Deposit, Due upon TOR signing, Due upon penultimate hearing, Full payment, Arbitration fees, mediation fees, others',
  PRIMARY KEY (`payment_type_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`case_op_hdr`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`case_op_hdr` (
  `case_op_hdr_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cases_case_id` INT UNSIGNED NOT NULL,
  `payment_type_id` INT UNSIGNED NOT NULL,
  `coll_nature_group_code` VARCHAR(10) NULL,
  `reference_no` VARCHAR(20) NULL,
  `firm_name` VARCHAR(255) NULL,
  `address` LONGTEXT NULL,
  `op_no` VARCHAR(35) NULL,
  `op_date` DATE NOT NULL,
  `or_no` VARCHAR(35) NULL,
  `or_date` DATE NULL,
  `total_amount` DECIMAL(14,2) NULL,
  `status_code` VARCHAR(15) NULL,
  PRIMARY KEY (`case_op_hdr_id`),
  INDEX `fk_case_op_hdr_param_payment_types1_idx` (`payment_type_id` ASC),
  INDEX `fk_case_op_hdr_cases1_idx` (`cases_case_id` ASC),
  CONSTRAINT `fk_case_op_hdr_param_payment_types1`
    FOREIGN KEY (`payment_type_id`)
    REFERENCES `isca`.`param_payment_type` (`payment_type_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_case_op_hdr_cases1`
    FOREIGN KEY (`cases_case_id`)
    REFERENCES `isca`.`cases` (`case_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`case_op_dtl`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`case_op_dtl` (
  `dtl_reference_no` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `case_op_hdr_id` INT UNSIGNED NOT NULL,
  `call_nature_code` VARCHAR(10) NULL,
  `amount` DECIMAL(12,2) NULL,
  INDEX `fk_case_op_dtl_case_op_hdr1_idx` (`case_op_hdr_id` ASC),
  PRIMARY KEY (`dtl_reference_no`),
  CONSTRAINT `fk_case_op_dtl_case_op_hdr1`
    FOREIGN KEY (`case_op_hdr_id`)
    REFERENCES `isca`.`case_op_hdr` (`case_op_hdr_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`case_issue`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`case_issue` (
  `case_issue_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `case_id` INT UNSIGNED NOT NULL,
  `holdings` TEXT NULL,
  `reasons` TEXT NULL,
  `issue` TEXT NULL,
  PRIMARY KEY (`case_issue_id`),
  INDEX `fk_case_issues_cases1_idx` (`case_id` ASC),
  CONSTRAINT `fk_case_issues_cases1`
    FOREIGN KEY (`case_id`)
    REFERENCES `isca`.`cases` (`case_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`param_attachment_tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`param_attachment_tags` (
  `attachment_tag_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `attachment_tag_name` VARCHAR(45) NULL,
  PRIMARY KEY (`attachment_tag_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`policy_tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`policy_tags` (
  `policy_id` INT UNSIGNED NOT NULL,
  `attachment_tag_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`policy_id`, `attachment_tag_id`),
  INDEX `fk_policy_tags -_param_attachment_tags -1_idx` (`attachment_tag_id` ASC),
  CONSTRAINT `fk_policy_tags_policies -1`
    FOREIGN KEY (`policy_id`)
    REFERENCES `isca`.`policies` (`policy_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_policy_tags -_param_attachment_tags -1`
    FOREIGN KEY (`attachment_tag_id`)
    REFERENCES `isca`.`param_attachment_tags` (`attachment_tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`seminar_materials`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`seminar_materials` (
  `seminar_material_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `seminar_material_title` VARCHAR(100) NULL,
  `seminar_material_attachment` VARCHAR(255) NULL,
  `created_by` INT UNSIGNED NULL,
  `created_date` DATETIME NULL,
  `last_modified_by` INT UNSIGNED NULL DEFAULT NULL,
  `last_modified_date` DATETIME NULL DEFAULT NULL,
  PRIMARY KEY (`seminar_material_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`seminar_material_tags`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`seminar_material_tags` (
  `seminar_material_id` INT UNSIGNED NOT NULL,
  `attachment_tag_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`seminar_material_id`, `attachment_tag_id`),
  INDEX `fk_seminar_material_tags_param_attachment_tags -1_idx` (`attachment_tag_id` ASC),
  CONSTRAINT `fk_seminar_material_tags_seminar_materials1`
    FOREIGN KEY (`seminar_material_id`)
    REFERENCES `isca`.`seminar_materials` (`seminar_material_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_seminar_material_tags_param_attachment_tags -1`
    FOREIGN KEY (`attachment_tag_id`)
    REFERENCES `isca`.`param_attachment_tags` (`attachment_tag_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`parties`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`parties` (
  `party_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `party_name` VARCHAR(100) NULL,
  `party_address` VARCHAR(255) NULL,
  `party_email` VARCHAR(45) NULL,
  `party_contact_no` VARCHAR(45) NULL,
  PRIMARY KEY (`party_id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`case_parties`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`case_parties` (
  `case_id` INT UNSIGNED NOT NULL,
  `party_id` INT UNSIGNED NOT NULL,
  `party_type` INT UNSIGNED NOT NULL,
  `type_clm_rpd` ENUM('C','CC','TPC','R','TPR') NOT NULL,
  `status_flag` ENUM('O','A','D') NOT NULL,
  PRIMARY KEY (`case_id`, `party_id`),
  INDEX `fk_case_claimants_claimants1_idx` (`party_id` ASC),
  INDEX `fk_case_parties -_param_party_types1_idx` (`party_type` ASC),
  CONSTRAINT `fk_case_claimants_case1`
    FOREIGN KEY (`case_id`)
    REFERENCES `isca`.`cases` (`case_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_case_claimants_claimants1`
    FOREIGN KEY (`party_id`)
    REFERENCES `isca`.`parties` (`party_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_case_parties -_param_party_types1`
    FOREIGN KEY (`party_type`)
    REFERENCES `isca`.`param_parties` (`party_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`case_claims`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`case_claims` (
  `claim_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `claim_desc` VARCHAR(100) NULL,
  `claim_type` ENUM('M', 'N', 'P') NULL COMMENT 'M - Monetary, N - Non-monetary with monetary, P - Purely non-monetary',
  `claim_amount` DECIMAL(12,2) NULL,
  `case_op_hdr_id` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`claim_id`),
  INDEX `fk_case_claims -_case_op_hdr1_idx` (`case_op_hdr_id` ASC),
  CONSTRAINT `fk_case_claims -_case_op_hdr1`
    FOREIGN KEY (`case_op_hdr_id`)
    REFERENCES `isca`.`case_op_hdr` (`case_op_hdr_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`case_arbitrators_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`case_arbitrators_history` (
  `case_id` INT UNSIGNED NOT NULL,
  `arbitrator_id` INT UNSIGNED NOT NULL,
  `position` ENUM('S', 'C', 'M1', 'M2') NOT NULL DEFAULT 'S' COMMENT 'S - Sole Arbitrator, C - Chairman, M1 - Member 1,  M2 - Member2',
  `replace_reason` VARCHAR(255) NULL,
  `created_date` DATETIME NULL,
  `created_by` INT UNSIGNED NULL,
  PRIMARY KEY (`case_id`, `arbitrator_id`),
  INDEX `fk_case_arbitrators_arbitrators1_idx` (`arbitrator_id` ASC),
  CONSTRAINT `fk_case_arbitrators10`
    FOREIGN KEY (`case_id`)
    REFERENCES `isca`.`cases` (`case_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_case_arbitrators_arbitrators10`
    FOREIGN KEY (`arbitrator_id`)
    REFERENCES `isca`.`arbitrators` (`arbitrator_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `isca`.`case_mediator_history`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `isca`.`case_mediator_history` (
  `case_id` INT UNSIGNED NOT NULL,
  `mediator_id` INT UNSIGNED NOT NULL,
  `replacement_reason` VARCHAR(255) NULL,
  `created_by` INT UNSIGNED NULL,
  `created_date` DATETIME NULL,
  PRIMARY KEY (`case_id`, `mediator_id`),
  INDEX `fk_case_mediator_history_arbitrators -1_idx` (`mediator_id` ASC),
  CONSTRAINT `fk_case_mediator_history_cases -1`
    FOREIGN KEY (`case_id`)
    REFERENCES `isca`.`cases` (`case_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_case_mediator_history_arbitrators -1`
    FOREIGN KEY (`mediator_id`)
    REFERENCES `isca`.`arbitrators` (`arbitrator_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `isca`.`param_parties`
-- -----------------------------------------------------
START TRANSACTION;
USE `isca`;
INSERT INTO `isca`.`param_parties` (`party_id`, `party_name`) VALUES (1, 'C');
INSERT INTO `isca`.`param_parties` (`party_id`, `party_name`) VALUES (2, 'R');

COMMIT;


-- -----------------------------------------------------
-- Data for table `isca`.`param_payment_type`
-- -----------------------------------------------------
START TRANSACTION;
USE `isca`;
INSERT INTO `isca`.`param_payment_type` (`payment_type_id`, `payment_type`) VALUES (1, 'Initial Deposit');
INSERT INTO `isca`.`param_payment_type` (`payment_type_id`, `payment_type`) VALUES (2, 'Due upon TOR Signing');
INSERT INTO `isca`.`param_payment_type` (`payment_type_id`, `payment_type`) VALUES (3, 'Due upon Penultimate Hearing');
INSERT INTO `isca`.`param_payment_type` (`payment_type_id`, `payment_type`) VALUES (4, 'Post Award');

COMMIT;

