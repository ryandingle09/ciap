-- May 10, 2016
-- AJ Doc
UPDATE `cias`.`cias_param_application` SET `url`='http://localhost/phase2/' WHERE `application_code`='CEIS';
INSERT INTO `cias`.`cias_param_application` (`application_code`, `application_name`, `intranet_app_flag`, `url`, `app_group`) VALUES ('BAS', 'Budget and Accounting System', '1', 'http://localhost/phase2/', '2');
INSERT INTO `cias`.`cias_param_role` (`role_code`, `application_code`, `role_name`) VALUES ('BAS-ADMINISTRATOR', 'BAS', 'BAS Administrator');
UPDATE `cias`.`cias_param_resource` SET `icon`='fa fa-dashboard' WHERE `resource_code`='CEIS-DASHBOARD';
DELETE FROM `cias`.`cias_permission` WHERE `resource_code`='CEIS-DASHBOARD' and`role_code`='CEIS-ADMINISTRATOR';
DELETE FROM `cias`.`cias_permission` WHERE `resource_code`='CEIS-MAINTENANCE' and`role_code`='CEIS-ADMINISTRATOR';

INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('CEIS-REQUESTS', 'CEIS', 'Requests', '1', '2', 'fa fa-check-square', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('CEIS-CONTRACTORS', 'CEIS', 'Contractors', '1', '3', 'fa fa-group', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('CEIS-EQUIPMENT', 'CEIS', 'Equipment', '1', '4', 'fa fa-briefcase', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('CEIS-COUNTRY-PROFILE', 'CEIS', 'Country Profile', '1', '5', 'fa fa-map-marker', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('CEIS-RESOLUTIONS', 'CEIS', 'Resolutions (FTA)', '1', '6', 'fa fa-list-alt', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('CEIS-DIRECTORY', 'CEIS', 'Directory', '1', '7', 'fa fa-columns', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('CEIS-REPORTS', 'CEIS', 'Reports', '1', '8', 'fa fa-file-text', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('CEIS-CODE-LIBRARY', 'CEIS', 'Code Library', '1', '9', 'fa fa-code icon', '1');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('CEIS-GEOGRAPHICAL-DISTRIBUTION', 'CEIS', 'CEIS-CODE-LIBRARY', 'Geographical Distribution', '1', '1', 'fa fa-angle-right', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('CEIS-POSITIONS', 'CEIS', 'CEIS-CODE-LIBRARY', 'Positions', '1', '2', 'fa fa-angle-right', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('CEIS-PROJECT-TYPES', 'CEIS', 'CEIS-CODE-LIBRARY', 'Project Types', '1', '3', 'fa fa-angle-right', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('CIES-CONTRACT-INVOLVEMENT', 'CEIS', 'CEIS-CODE-LIBRARY', 'Contract Involvement', '1', '4', 'fa fa-angle-right', '0');

UPDATE `cias`.`cias_param_resource` SET `main_flag`='0' WHERE `resource_code`='CEIS-GEOGRAPHICAL-DISTRIBUTION';
UPDATE `cias`.`cias_param_resource` SET `main_flag`='0' WHERE `resource_code`='CEIS-POSITIONS';
UPDATE `cias`.`cias_param_resource` SET `main_flag`='0' WHERE `resource_code`='CEIS-PROJECT-TYPES';
UPDATE `cias`.`cias_param_resource` SET `main_flag`='0' WHERE `resource_code`='CIES-CONTRACT-INVOLVEMENT';


insert into cias_permission
select resource_code, 'CEIS-ADMINISTRATOR' from cias_param_resource
where application_code = 'CEIS';

DELETE FROM `cias`.cias_param_resource where resource_code = 'CEIS-MAINTENANCE';


UPDATE `cias`.`cias_param_resource` SET `resource_code`='CEIS-CONTRACT-INVOLVEMENT' WHERE `resource_code`='CIES-CONTRACT-INVOLVEMENT';

INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('BAS-DASHBOARD', 'BAS', 'Dashboard', '1', '1', 'fa fa-dashboard', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('BAS-LINE-ITEMS', 'BAS', 'Line Items', '1', '2', 'fa fa-suitcase', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('BAS-REALIGNMENT', 'BAS', 'Realignment', '1', '3', 'fa fa-suitcase', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('BAS-OBLIGATION-SLIPS', 'BAS', 'Obligation Slips', '1', '4', 'fa fa-suitcase', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('BAS-BILLINGS', 'BAS', 'Billings', '1', '5', 'fa fa-suitcase', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('BAS-DISBURSEMENT-VOUCHERS', 'BAS', 'Disbursement Vouchers', '1', '6', 'fa  fa-suitcase', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('BAS-CHECKS', 'BAS', 'Checks ', '1', '7', 'fa fa-suitcase', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('BAS-REPORTS', 'BAS', 'Reports', '1', '8', 'fa fa-suitcase', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('BAS-CODE-LIBRARY', 'BAS', 'Code Library', '1', '9', 'fa fa-suitcase', '1');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('BAS-CHART-ACCOUNTS', 'BAS', 'BAS-CODE-LIBRARY', 'Chart of Accounts', '0', '1', 'fa fa-suitcase', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('BAS-TYPE-SOURCE', 'BAS', 'BAS-CODE-LIBRARY', 'Type of Source', '0', '2', 'fa fa-suitcase', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('BAS-PAYEES', 'BAS', 'BAS-CODE-LIBRARY', 'Payees', '0', '3', 'fa fa-suitcase', '0');

insert into cias_permission
select 	resource_code, 'BAS-ADMINISTRATOR'
FROM	cias_param_resource
WHERE	application_code = 'BAS';

UPDATE `cias`.`cias_permission` SET `resource_code`='CEIS-CONTRACT-INVOLVEMENT' WHERE `resource_code`='CIES-CONTRACT-INVOLVEMENT' and`role_code`='CEIS-ADMINISTRATOR';

UPDATE `cias`.`cias_param_resource` SET `icon`='fa fa-gavel' WHERE `resource_code`='BAS-OBLIGATION-SLIPS';
UPDATE `cias`.`cias_param_resource` SET `icon`='fa fa-building-o' WHERE `resource_code`='BAS-BILLINGS';
UPDATE `cias`.`cias_param_resource` SET `icon`='fa  fa-file-o' WHERE `resource_code`='BAS-DISBURSEMENT-VOUCHERS';
UPDATE `cias`.`cias_param_resource` SET `icon`='fa fa-money' WHERE `resource_code`='BAS-CHECKS';
UPDATE `cias`.`cias_param_resource` SET `icon`='fa fa-file-text' WHERE `resource_code`='BAS-REPORTS';
UPDATE `cias`.`cias_param_resource` SET `icon`='fa fa fa-code icon icon' WHERE `resource_code`='BAS-CODE-LIBRARY';

-- AJ DOC
-- MAY 13, 2016
insert into cias_param_action values('SAVE_DRAFT', 'Save as Draft'),
('SAVE_FINAL', 'Save as Final'),
('DECLINE', 'Decline'),
('CERTIFY', 'Certify'),
('POST', 'Post'),
('VOID', 'Void'),
('OVERRIDE', 'Override'),
('RETURN_EXCESS','Return excess');

-- AJ DOC
-- MAY 17, 2016
ALTER TABLE cias_param_resource CHANGE COLUMN resource_name resource_name varchar(255) NOT NULL;

INSERT INTO cias_param_resource VALUES
( 'BAS-REPORTS-SAAOBD', 'BAS', 'BAS-REPORTS', 'Statement of Appropriation, Allotments, Obligations, Disbursements and Balances', 0, 1, NULL, 0 ),
( 'BAS-REPORTS-SAAOBD-EXP', 'BAS', 'BAS-REPORTS', 'Statement of Appropriation, Allotments, Obligations, Disbursements and Balances by object of Expenditures', 0, 1, NULL, 0 ),
( 'BAS-REPORTS-SAAOB', 'BAS', 'BAS-REPORTS', 'Statement of Appropriation, Allotments, Obligations and Balances', 0, 1, NULL, 0 ),
( 'BAS-REPORTS-STATUS-FUNDS', 'BAS', 'BAS-REPORTS', 'Status of MOOE Funds per Office', 0, 1, NULL, 0 ),
( 'BAS-REPORTS-RAOMOOE', 'BAS', 'BAS-REPORTS', 'Registry of Allotment and Obligations', 0, 1, NULL, 0 );

-- CHRISTIAN 
-- MAY 18, 2016

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;


CREATE TABLE IF NOT EXISTS `sys_param` (
  `sys_param_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sys_param_type` varchar(45) NOT NULL,
  `sys_param_name` varchar(100) NOT NULL,
  `sys_param_value` varchar(45) NOT NULL,
  `built_in_flag` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`sys_param_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=11 ;

INSERT INTO `sys_param` (`sys_param_id`, `sys_param_type`, `sys_param_name`, `sys_param_value`, `built_in_flag`) VALUES
(1, 'MODULE_LOCATION', 'Left', 'LEFT', 1),
(2, 'SMTP', 'PROTOCOL', 'smtp', 1),
(3, 'SMTP', 'SMTP_HOST', 'ssl://smtp.gmail.com', 1),
(4, 'SMTP', 'SMTP_PORT', '465', 1),
(5, 'SMTP', 'SMTP_USER', 'kebwaitforit@gmail.com', 1),
(6, 'SMTP', 'SMTP_PASS', 'technofreeze', 1),
(7, 'MODULE_LOCATION', 'On Page', 'ON_PAGE', 1),
(8, 'MODULE_LOCATION', 'Top Bar', 'TOP_BAR', 1),
(9, 'SMTP', 'SMTP_REPLY_NAME', 'LGU 360 Administrator', 1),
(10, 'SMTP', 'SMTP_REPLY_EMAIL', 'kebwaitforit@gmail.com', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- AJ DOC 
-- JUNE 10, 2016

UPDATE cias_param_application 
SET    url = 'http://localhost/p2/'
WHERE  application_code IN ('BAS','CEIS');

INSERT INTO cias_param_application VALUES('ISCA', 'Integrated Systems for Construction Arbitration', 1, 'http://localhost/p2/', 2);

UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-money128' WHERE `resource_code`='BAS-BILLINGS';
UPDATE `cias`.`cias_param_resource` SET `icon`='' WHERE `resource_code`='BAS-CHART-ACCOUNTS';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-rectangle18' WHERE `resource_code`='BAS-CHECKS';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-programming2' WHERE `resource_code`='BAS-CODE-LIBRARY';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-horizontal37' WHERE `resource_code`='BAS-DASHBOARD';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-newspaper16' WHERE `resource_code`='BAS-DISBURSEMENT-VOUCHERS';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-business177' WHERE `resource_code`='BAS-LINE-ITEMS';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-hammer53' WHERE `resource_code`='BAS-OBLIGATION-SLIPS';
UPDATE `cias`.`cias_param_resource` SET `icon`='' WHERE `resource_code`='BAS-PAYEES';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-gears5' WHERE `resource_code`='BAS-REALIGNMENT';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-newspaper16' WHERE `resource_code`='BAS-REPORTS';
UPDATE `cias`.`cias_param_resource` SET `icon`='' WHERE `resource_code`='BAS-TYPE-SOURCE';

UPDATE cias_param_resource 
SET icon = NULL
WHERE resource_code IN ('BAS-CHART-ACCOUNTS', 'BAS-PAYEES', 'BAS-TYPE-SOURCE');

UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-programming2' WHERE `resource_code`='CEIS-CODE-LIBRARY';
UPDATE `cias`.`cias_param_resource` SET `icon`='' WHERE `resource_code`='CEIS-CONTRACT-INVOLVEMENT';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-group66' WHERE `resource_code`='CEIS-CONTRACTORS';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-placeholder30' WHERE `resource_code`='CEIS-COUNTRY-PROFILE';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-speedometer35' WHERE `resource_code`='CEIS-DASHBOARD';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-hierarchical9' WHERE `resource_code`='CEIS-DIRECTORY';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-hammer53' WHERE `resource_code`='CEIS-EQUIPMENT';
UPDATE `cias`.`cias_param_resource` SET `icon`='' WHERE `resource_code`='CEIS-GEOGRAPHICAL-DISTRIBUTION';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-visible9' WHERE `resource_code`='CEIS-OPPURTUNITIES';
UPDATE `cias`.`cias_param_resource` SET `icon`='' WHERE `resource_code`='CEIS-POSITIONS';
UPDATE `cias`.`cias_param_resource` SET `icon`='' WHERE `resource_code`='CEIS-PROJECT-TYPES';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-volume44' WHERE `resource_code`='CEIS-REPORTS';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-browser69' WHERE `resource_code`='CEIS-REQUESTS';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-antique25' WHERE `resource_code`='CEIS-RESOLUTIONS';

UPDATE 	cias_param_resource
SET 	icon = NULL 
WHERE 	resource_code IN ('CEIS-PROJECT-TYPES', 'CEIS-POSITIONS', 'CEIS-GEOGRAPHICAL-DISTRIBUTION', 'CEIS-CONTRACT-INVOLVEMENT');

INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('ISCA-DASHBOARD', 'ISCA', NULL,'Dashboard', '1', '1', 'flaticon-speedometer35', '0');

INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`,`resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('ISCA-PAYMENTS', 'ISCA', NULL,'Payments', '1', '2', 'flaticon-money128', '1'),
('ISCA-PAYMENTS-ORDER', 'ISCA', 'ISCA-PAYMENTS', 'Order of Payments', 0, 1, NULL, 0),
('ISCA-PAYMENTS-BILL', 'ISCA', 'ISCA-PAYMENTS', 'Bill Payments', 0, 2, NULL, 0),
('ISCA-CASES', 'ISCA', NULL, 'Cases', 1,3,'flaticon-portfolio32',0),
('ISCA-ARBITRATORS', 'ISCA', NULL, 'Arbitrators', 1,4,'flaticon-group65',0),
('ISCA-MACE', 'ISCA', NULL, 'MACE', 1,5,'flaticon-sport17',0),
('ISCA-LIBRARY', 'ISCA', NULL, 'Library', 1,6,'flaticon-book202',1),
('ISCA-BOOKS', 'ISCA', 'ISCA-LIBRARY', 'Card Catalogues', 0,1,NULL,0),
('ISCA-BOARD-MEETINGS', 'ISCA', 'ISCA-LIBRARY', 'Board Meetings', 0,2,NULL,0),
('ISCA-BOARD-MINUTES', 'ISCA', 'ISCA-LIBRARY', 'Minutes of Meetings', 0,3,NULL,0),
('ISCA-DELIVERY-LOGS', 'ISCA', 'ISCA-LIBRARY', 'Delivery Logs', 0,4,NULL,0),
('ISCA-DELIVERY-REPORTS', 'ISCA', 'ISCA-LIBRARY', 'Delivery Reports', 0,5,NULL,0),
('ISCA-POLICIES', 'ISCA', NULL, 'Policy Files', 1,7,'flaticon-hammer53',0),
('ISCA-CASES-CLOSED', 'ISCA', NULL, 'Closed Cases', 1,8,'flaticon-archive29',0);

INSERT INTO `cias`.`cias_param_role` (`role_code`, `application_code`, `role_name`) VALUES ('ISCA-ADMINISTRATOR', 'ISCA', 'ISCA Administrator');
insert into cias_permission
select 	resource_code, 'ISCA-ADMINISTRATOR'
FROM	cias_param_resource
WHERE	application_code = 'ISCA';

UPDATE cias_param_resource
SET resource_code = 'CEIS-OPPORTUNITIES'
WHERE resource_code = 'CEIS-OPPURTUNITIES';

UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-visible9' WHERE `resource_code`='CEIS-OPPORTUNITIES';

/*JEFF
* 06/20/16
*/
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('ISCA-STATEMENT-OF-ACCOUNTS', 'ISCA', 'ISCA-PAYMENTS', 'Statement of Accounts', '0', '3', NULL, '0'),
('ISCA-REPORTS', 'ISCA', NULL, 'Reports', '1', '9', 'flaticon-printing23', '0'),
('ISCA-ACTIVE-CASES', 'ISCA', 'ISCA-CASES', 'Active Cases', '0', '1', NULL, '0'),
('ISCA-DISPOSAL', 'ISCA', 'ISCA-CASES', 'Disposal', '0', '3', NULL, '0'),
('ISCA-ARBITRATOR-LIST', 'ISCA', 'ISCA-ARBITRATORS', 'List of Arbitrators', '0', '1', NULL, '0'),
('ISCA-CODE-LIBRARIES', 'ISCA', NULL, 'Code Libraries', '1', '10', 'flaticon-book204', '1'),
('ISCA-TYPE-OF-CASE', 'ISCA', 'ISCA-CODE-LIBRARIES', 'Type of Case', '0', '1', NULL, '0'),
('ISCA-CASE-STATUS', 'ISCA', 'ISCA-CODE-LIBRARIES', 'Case Status', '0', '2', NULL, '0'),
('ISCA-APPEAL-STATUS', 'ISCA', 'ISCA-CODE-LIBRARIES', 'Appeal Status', '0', '3', NULL, '0'),
('ISCA-CASE-TYPE-DOCUMENT', 'ISCA', 'ISCA-CODE-LIBRARIES', 'Types of Case Document', '0', '4', NULL, '0'),
('ISCA-POLICY-CATEGORY', 'ISCA', 'ISCA-CODE-LIBRARIES', 'Policy Categories', '0', '5', NULL, '0'),
('ISCA-ARBITRATOR-PROFESSION', 'ISCA', 'ISCA-CODE-LIBRARIES', 'Arbitrator Profession', '0', '6', NULL, '0'),
('ISCA-MACE-RULES', 'ISCA', 'ISCA-CODE-LIBRARIES', 'MACE Rules', '0', '7', NULL, '0');


INSERT INTO `cias`.`cias_permission` (`resource_code`, `role_code`) VALUES ('ISCA-REPORTS', 'ISCA-ADMINISTRATOR'),
('ISCA-STATEMENT-OF-ACCOUNTS', 'ISCA-ADMINISTRATOR'),
('ISCA-DISPOSAL', 'ISCA-ADMINISTRATOR'),
('ISCA-ACTIVE-CASES', 'ISCA-ADMINISTRATOR'),
('ISCA-ARBITRATOR-LIST', 'ISCA-ADMINISTRATOR'),
('ISCA-CODE-LIBRARIES', 'ISCA-ADMINISTRATOR'),
('ISCA-TYPE-OF-CASE', 'ISCA-ADMINISTRATOR'),
('ISCA-CASE-STATUS', 'ISCA-ADMINISTRATOR'),
('ISCA-APPEAL-STATUS', 'ISCA-ADMINISTRATOR'),
('ISCA-CASE-TYPE-DOCUMENT', 'ISCA-ADMINISTRATOR'),
('ISCA-POLICY-CATEGORY', 'ISCA-ADMINISTRATOR'),
('ISCA-ARBITRATOR-PROFESSION', 'ISCA-ADMINISTRATOR'),
('ISCA-MACE-RULES', 'ISCA-ADMINISTRATOR')


UPDATE `cias`.`cias_param_resource` SET `display_child_flag`='1' WHERE `resource_code`='ISCA-CASES';
UPDATE `cias`.`cias_param_resource` SET `parent_resource_code`='ISCA-CASES', `main_flag`='0', `sort_no`='2', `icon`='' WHERE `resource_code`='ISCA-CASES-CLOSED';
	
UPDATE `cias`.`cias_param_resource` SET `parent_resource_code`='ISCA-ARBITRATORS', `main_flag`='0', `sort_no`='2', `icon`='' WHERE `resource_code`='ISCA-MACE';
UPDATE `cias`.`cias_param_resource` SET `display_child_flag`='1' WHERE `resource_code`='ISCA-ARBITRATORS';
UPDATE `cias`.`cias_param_resource` SET `resource_name`='Daily Communications' WHERE `resource_code`='ISCA-DELIVERY-LOGS';

/*end jeff*/

/*jeff 06/22/2016*/
UPDATE `cias`.`cias_param_resource` SET `resource_name`='Payments to Arbitrators' WHERE `resource_code`='ISCA-PAYMENTS-BILL';


-- KEBS VILLAROJO
-- JUNE 27, 2016
INSERT INTO cias.cias_param_resource_action
SELECT resource_code, 'VIEW' 
FROM cias.cias_param_resource WHERE application_code = 'ISCA';

INSERT INTO cias.cias_param_resource_action
SELECT resource_code, 'ADD' 
FROM cias.cias_param_resource WHERE application_code = 'ISCA';


INSERT INTO cias.cias_param_resource_action
SELECT resource_code, 'EDIT' 
FROM cias.cias_param_resource WHERE application_code = 'ISCA';

INSERT INTO cias.cias_param_resource_action
SELECT resource_code, 'DELETE' 
FROM cias.cias_param_resource WHERE application_code = 'ISCA';


INSERT INTO cias.cias_user_role
VALUES('2014-001', 'CM', 'ISCA-ADMINISTRATOR'),('2014-001', 'CM', 'BAS-ADMINISTRATOR');


INSERT INTO cias.cias_param_resource_action
SELECT resource_code, 'VIEW' 
FROM cias.cias_param_resource WHERE application_code = 'BAS';

INSERT INTO cias.cias_param_resource_action
SELECT resource_code, 'ADD' 
FROM cias.cias_param_resource WHERE application_code = 'BAS';


INSERT INTO cias.cias_param_resource_action
SELECT resource_code, 'EDIT' 
FROM cias.cias_param_resource WHERE application_code = 'BAS';

INSERT INTO cias.cias_param_resource_action
SELECT resource_code, 'DELETE' 
FROM cias.cias_param_resource WHERE application_code = 'BAS';

/* Jeff 06/28/2016 */
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `display_child_flag`) VALUES ('ISCA-SEMINAR-MATERIALS', 'ISCA', 'ISCA-LIBRARY', 'Seminar Materials', '0', '4', '0');
INSERT INTO `cias`.`cias_permission` (`resource_code`, `role_code`) VALUES ('ISCA-SEMINAR-MATERIALS', 'ISCA-ADMINISTRATOR');

UPDATE `cias`.`cias_param_resource` SET `sort_no`='6' WHERE `resource_code`='ISCA-DELIVERY-REPORTS';
UPDATE `cias`.`cias_param_resource` SET `sort_no`='5' WHERE `resource_code`='ISCA-DELIVERY-LOGS';

INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('ISCA-NATURE-OF-CASE', 'ISCA', 'ISCA-CODE-LIBRARIES', 'Nature of Case', '0', '1', '', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `display_child_flag`) VALUES ('ISCA-CASE-ACTIVITIES', 'ISCA', 'ISCA-CODE-LIBRARIES', 'Case Activities', '0', '6', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `display_child_flag`) VALUES ('ISCA-ATTENDEES', 'ISCA', 'ISCA-CODE-LIBRARIES', 'Attendees', '0', '8', '0');

UPDATE `cias`.`cias_param_resource` SET `sort_no`='2' WHERE `resource_code`='ISCA-TYPE-OF-CASE';
UPDATE `cias`.`cias_param_resource` SET `sort_no`='3' WHERE `resource_code`='ISCA-CASE-STATUS';
UPDATE `cias`.`cias_param_resource` SET `sort_no`='4' WHERE `resource_code`='ISCA-APPEAL-STATUS';
UPDATE `cias`.`cias_param_resource` SET `sort_no`='5' WHERE `resource_code`='ISCA-CASE-TYPE-DOCUMENT';
UPDATE `cias`.`cias_param_resource` SET `sort_no`='7' WHERE `resource_code`='ISCA-POLICY-CATEGORY';
UPDATE `cias`.`cias_param_resource` SET `sort_no`='9' WHERE `resource_code`='ISCA-ARBITRATOR-PROFESSION';
UPDATE `cias`.`cias_param_resource` SET `sort_no`='10' WHERE `resource_code`='ISCA-MACE-RULES';

INSERT INTO `cias`.`cias_permission` (`resource_code`, `role_code`) VALUES ('ISCA-NATURE-OF-CASE', 'ISCA-ADMINISTRATOR');
INSERT INTO `cias`.`cias_permission` (`resource_code`, `role_code`) VALUES ('ISCA-CASE-ACTIVITIES', 'ISCA-ADMINISTRATOR');
INSERT INTO `cias`.`cias_permission` (`resource_code`, `role_code`) VALUES ('ISCA-ATTENDEES', 'ISCA-ADMINISTRATOR');

/* end 06/28/2016 */
/*jeff 06/29/2016*/
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('ISCA-BOOK-AUTHOR', 'ISCA', 'ISCA-CODE-LIBRARIES', 'Book Author', '0', '11', '', '0');
INSERT INTO `cias`.`cias_permission` (`resource_code`, `role_code`) VALUES ('ISCA-BOOK-AUTHOR', 'ISCA-ADMINISTRATOR');


-- RODEL SATUITO
-- 2016.07.11 10:28AM

INSERT INTO cias_param_application
(application_code, application_name, intranet_app_flag, url, app_group)
VALUES
('MPIS', 'Market and Policies Information System', 1, 'http://localhost/p2/', 1),
('PIS', 'Personnel Information System', 1, 'http://localhost/p2/', 1);


INSERT INTO cias_param_resource 
(resource_code, application_code, parent_resource_code, resource_name, main_flag, sort_no, icon, display_child_flag)
VALUES
('MPIS-DASHBOARD', 'MPIS', NULL, 'Dashboard', 1, 1, NULL, 0),
('MPIS-POLICIES', 'MPIS', NULL, 'Policies', 1, 2, NULL, 0),
('MPIS-SOCIOECONOMICS', 'MPIS', NULL, 'Socioeconomics', 1, 3, NULL, 0),
('MPIS-REPORTS', 'MPIS', NULL, 'Reports', 1, 4, NULL, 0),
('MPIS-CODE-LIBRARIES', 'MPIS', NULL, 'Code Libraries', 1, 4, NULL, 1),
('MPIS-CODE-LIBRARIES-POLICY-CATEGORIES', 'MPIS', NULL, 'Policy Categories', 1, 1, NULL, 0),
('MPIS-CODE-LIBRARIES-BOARD-MEETING', 'MPIS', NULL, 'Board Meeting Classifications', 1, 2, NULL, 0),
('MPIS-CODE-LIBRARIES-ISSUANCE-CLASSIFICATIONS', 'MPIS', NULL, 'Issuance Classifications', 1, 3, NULL, 0),
('MPIS-CODE-LIBRARIES-INDICATOR-SOURCES', 'MPIS', NULL, 'Indicator Sources', 1, 4, NULL, 0),
('MPIS-CODE-LIBRARIES-PROFESSIONS', 'MPIS', NULL, 'Professions', 1, 5, NULL, 0);

INSERT INTO  cias_param_resource_action
(resource_code, action_code)
select resource_code, 'VIEW'
from cias_param_resource
where application_code = 'MPIS';


INSERT INTO cias_param_role
(role_code, application_code, role_name)
VALUES
('MPIS-ADMINISTRATOR', 'MPIS', 'MPIS Administrator');



INSERT INTO cias_permission 
(resource_code, role_code)
SELECT 
resource_code, 'MPIS-ADMINISTRATOR'
FROM cias_param_resource 
WHERE application_code = 'MPIS';

INSERT INTO cias_permission_action 
(resource_code, role_code, action_code)
SELECT 
resource_code, 'MPIS-ADMINISTRATOR', 'VIEW'
FROM cias_param_resource 
WHERE application_code = 'MPIS';



UPDATE `cias_param_resource` SET `main_flag`='0' , `parent_resource_code`='MPIS-CODE-LIBRARIES' WHERE `resource_code`='MPIS-CODE-LIBRARIES-BOARD-MEETING';
UPDATE `cias_param_resource` SET `main_flag`='0' , `parent_resource_code`='MPIS-CODE-LIBRARIES' WHERE `resource_code`='MPIS-CODE-LIBRARIES-INDICATOR-SOURCES';
UPDATE `cias_param_resource` SET `main_flag`='0' , `parent_resource_code`='MPIS-CODE-LIBRARIES' WHERE `resource_code`='MPIS-CODE-LIBRARIES-ISSUANCE-CLASSIFICATIONS';
UPDATE `cias_param_resource` SET `main_flag`='0' , `parent_resource_code`='MPIS-CODE-LIBRARIES' WHERE `resource_code`='MPIS-CODE-LIBRARIES-POLICY-CATEGORIES';
UPDATE `cias_param_resource` SET `main_flag`='0' , `parent_resource_code`='MPIS-CODE-LIBRARIES' WHERE `resource_code`='MPIS-CODE-LIBRARIES-PROFESSIONS';


UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-programming2' WHERE `resource_code`='MPIS-CODE-LIBRARIES';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-volume44' WHERE `resource_code`='MPIS-REPORTS';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-speedometer35' WHERE `resource_code`='MPIS-DASHBOARD';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-checked20' WHERE `resource_code`='MPIS-POLICIES';
UPDATE `cias`.`cias_param_resource` SET `icon`='flaticon-business177' WHERE `resource_code`='MPIS-SOCIOECONOMICS';



-- CHRISTIAN C. AQUINO
-- 2016.07.11 05:40PM
INSERT INTO cias_param_resource
VALUES
('CEIS-MASTER-LIST', 'CEIS', 'CEIS-CONTRACTORS', 'Master List', 0, 1, NULL, 0);


insert into cias_param_resource_action
values ('CEIS-MASTER-LIST', 'VIEW');


insert into cias_permission
values ('CEIS-MASTER-LIST', 'CEIS-ADMINISTRATOR');


insert into cias_permission_action
values ('CEIS-MASTER-LIST', 'CEIS-ADMINISTRATOR', 'VIEW');


UPDATE cias_param_resource SET parent_resource_code = 'CEIS-CONTRACTORS' WHERE resource_code = 'CEIS-DIRECTORY';


UPDATE `cias_param_resource` SET `resource_code`='CEIS-CONTRACTORS-MODULE' WHERE `resource_code`='CEIS-CONTRACTORS';
UPDATE `cias_param_resource` SET `resource_code`='CEIS-CONTRACTORS', `parent_resource_code`='CEIS-CONTRACTORS-MODULE' WHERE `resource_code`='CEIS-MASTER-LIST';
UPDATE `cias_param_resource` SET `parent_resource_code`='CEIS-CONTRACTORS-MODULE' WHERE `resource_code`='CEIS-DIRECTORY';



insert into cias_param_resource_action
values ('CEIS-CONTRACTORS-MODULE', 'VIEW');

insert into cias_permission
values ('CEIS-CONTRACTORS-MODULE', 'CEIS-ADMINISTRATOR');


insert into cias_permission_action
values ('CEIS-CONTRACTORS-MODULE', 'CEIS-ADMINISTRATOR', 'VIEW');



-- RODEL SATUITO
-- 2016.07.12 05:01PM

INSERT INTO cias_param_resource  
(resource_code, application_code, parent_resource_code, resource_name, main_flag, sort_no, icon, display_child_flag)
VALUES
('MPIS-INDICATORS', 'MPIS', 'MPIS-SOCIOECONOMICS', 'Indicators', 0, 1, NULL, 0),
('MPIS-RES-BLDG-CONS', 'MPIS', 'MPIS-SOCIOECONOMICS', 'Residential Building Construction', 0, 2, NULL, 0),
('MPIS-NON-RES-BLDG-CONS', 'MPIS', 'MPIS-SOCIOECONOMICS', 'Non-Residential Building Construction', 0, 3, NULL, 0),
('MPIS-FINANCIAL-RATIOS', 'MPIS', 'MPIS-SOCIOECONOMICS', 'Financial Ratios', 0, 4, NULL, 0),
('MPIS-CONS-EMPLOYMENT', 'MPIS', 'MPIS-SOCIOECONOMICS', 'Construction Employment', 0, 5, NULL, 0),
('MPIS-GNI-GDP-EXPENDITURE', 'MPIS', 'MPIS-SOCIOECONOMICS', 'GNI and GDP by Expenditure Share', 0, 6, NULL, 0),
('MPIS-GNI-GDP-INUDSTRIAL', 'MPIS', 'MPIS-SOCIOECONOMICS', 'GNI and GDP by Inudstrial Origin', 0, 7, NULL, 0),
('MPIS-GV-GVA', 'MPIS', 'MPIS-SOCIOECONOMICS', 'GV and GVA in Construction', 0, 8, NULL, 0),
('MPIS-CONS-COUNTRIES', 'MPIS', 'MPIS-SOCIOECONOMICS', 'Construction Countries', 0, 9, NULL, 0),
('MPIS-CONS-RELATED-PROF', 'MPIS', 'MPIS-SOCIOECONOMICS', 'Construction Related Licensed Professionals', 0, 10, NULL, 0);

INSERT INTO cias_param_resource_action (resource_code, action_code)
SELECT resource_code, 'VIEW'
FROM cias_param_resource
WHERE parent_resource_code = 'MPIS-SOCIOECONOMICS';

INSERT INTO cias_permission (resource_code, role_code)
SELECT resource_code, 'MPIS-ADMINISTRATOR'
FROM cias_param_resource
WHERE parent_resource_code = 'MPIS-SOCIOECONOMICS';


INSERT INTO cias_permission_action (resource_code, role_code, action_code)
SELECT resource_code, 'MPIS-ADMINISTRATOR', 'VIEW'
FROM cias_param_resource
WHERE parent_resource_code = 'MPIS-SOCIOECONOMICS';

UPDATE `cias_param_resource` SET `display_child_flag`='1' WHERE `resource_code`='MPIS-SOCIOECONOMICS';

INSERT INTO cias_param_role
(role_code, application_code, role_name)
VALUES
('CEIS-RECEIVING-STAFF', 'CEIS', 'Receiving Staff'),
('CEIS-EVALUATOR', 'CEIS', 'Evaluator'),
('CEIS-REVIEWER', 'CEIS', 'Reviewer'),
('CEIS-ENDORSER', 'CEIS', 'Endorser'),
('CEIS-RELEASING-STAFF', 'CEIS', 'Releasing Staff');


-- RODEL SATUIT0
-- 2016.07.25 11:27AM
UPDATE `cias`.`cias_param_resource` 
SET `resource_code`='MPIS-GNI-GDP-INDUSTRIAL' 
WHERE `resource_code`='MPIS-GNI-GDP-INUDSTRIAL';

UPDATE cias_param_resource_action
SET `resource_code`='MPIS-GNI-GDP-INDUSTRIAL' 
WHERE `resource_code`='MPIS-GNI-GDP-INUDSTRIAL';

UPDATE cias_permission 
SET `resource_code`='MPIS-GNI-GDP-INDUSTRIAL' 
WHERE `resource_code`='MPIS-GNI-GDP-INUDSTRIAL';

UPDATE cias_permission_action 
SET `resource_code`='MPIS-GNI-GDP-INDUSTRIAL' 
WHERE `resource_code`='MPIS-GNI-GDP-INUDSTRIAL';


-- RODEL SATUITO
-- 2016.07.26 03:43PM
INSERT INTO cias_param_resource 
(application_code, parent_resource_code, main_flag, icon, display_child_flag, resource_code, resource_name, sort_no)
VALUES
('MPIS', 'MPIS-REPORTS', 0, NULL, 0, 'MPIS-REPORTS-R1', 'Residential Building Construction', 1),
('MPIS', 'MPIS-REPORTS', 0, NULL, 0, 'MPIS-REPORTS-R2', 'Non-Residential Building Construction', 2),
('MPIS', 'MPIS-REPORTS', 0, NULL, 0, 'MPIS-REPORTS-R3', 'Summary of Number, Floor Area and Value of Building Construction ', 3),
('MPIS', 'MPIS-REPORTS', 0, NULL, 0, 'MPIS-REPORTS-R4', 'Financial Ratios', 4),
('MPIS', 'MPIS-REPORTS', 0, NULL, 0, 'MPIS-REPORTS-R5', 'Construction Employment', 5),
('MPIS', 'MPIS-REPORTS', 0, NULL, 0, 'MPIS-REPORTS-R6', 'Gross National Income and Gross Domestic Product by Expenditure Share', 6),
('MPIS', 'MPIS-REPORTS', 0, NULL, 0, 'MPIS-REPORTS-R7', 'Gross National Income and Gross Domestic Product by Industrial Origin', 7),
('MPIS', 'MPIS-REPORTS', 0, NULL, 0, 'MPIS-REPORTS-R8', 'Gross Value and Gross Value Added in Construction', 8),
('MPIS', 'MPIS-REPORTS', 0, NULL, 0, 'MPIS-REPORTS-R9', 'News Article', 9),
('MPIS', 'MPIS-REPORTS', 0, NULL, 0, 'MPIS-REPORTS-R10', 'Construction ASEAN Countries', 10),
('MPIS', 'MPIS-REPORTS', 0, NULL, 0, 'MPIS-REPORTS-R11', 'Number of Construction Related Licensed Professionals', 11),
('MPIS', 'MPIS-REPORTS', 0, NULL, 0, 'MPIS-REPORTS-R12', 'Cost of Construction Index', 12);


-- KEBS VILLAROJO
-- AUG 02,2016
CREATE TABLE `cias_user_app_tokens` (
  `employee_no` varchar(10) NOT NULL,
  `application_code` varchar(10) NOT NULL,
  `salt` TEXT NOT NULL,
  PRIMARY KEY (`employee_no`,`application_code`),
  CONSTRAINT `fk_employee_no_1` FOREIGN KEY (employee_no) REFERENCES `cias_user`(employee_no),
  CONSTRAINT `fk_application_code_1` FOREIGN KEY (application_code) REFERENCES `cias_param_application`(application_code)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- RODEL SATUIT0
-- 2016.08.03 01:28PM
UPDATE `cias_param_resource` SET `resource_name`='Policy Information', `display_child_flag`='1' WHERE `resource_code`='MPIS-POLICIES';
UPDATE `cias_param_resource` SET `display_child_flag`='0' WHERE `resource_code`='MPIS-POLICIES';


-- AJ DOC
-- AUG 4, 2016 2:31 PM
INSERT INTO `cias`.`cias_param_application` (`application_code`, `application_name`, `intranet_app_flag`, `url`, `app_group`) VALUES ('PSMS', 'Properties and Supplies Monitoring System', '1', 'http://localhost/p2/', '2');
INSERT INTO `cias`.`cias_param_role` (`role_code`, `application_code`, `role_name`) VALUES ('PSMS-ADMINISTRATOR', 'PSMS', 'PSMS Administrator');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('PSMS-ASSETS', 'PSMS', 'Assets', '1', '1', 'flaticon-building69', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('PSMS-SUPPLIES', 'PSMS', 'Supplies', '1', '2', 'flaticon-travel18', '1');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `display_child_flag`) VALUES ('PSMS-SUPPLIES-INFO', 'PSMS', 'PSMS-SUPPLIES', 'Supplies Info', '0', '1', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `display_child_flag`) VALUES ('PSMS-RIS', 'PSMS', 'PSMS-SUPPLIES', 'RIS', '0', '2', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('PSMS-CODE-LIBRARY', 'PSMS', '', 'Code Library', '1', '3', 'flaticon-programming2', '1');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('PSMS-ASSET-SITE', 'PSMS', 'PSMS-CODE-LIBRARY', 'Asset Site', '0', '1', 'flaticon-visible9', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('PSMS-ASSET-LOCATION', 'PSMS', 'PSMS-CODE-LIBRARY', 'Asset Location', '0', '2', 'flaticon-visible9', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('PSMS-ASSET-CLASSIFICATION', 'PSMS', 'PSMS-CODE-LIBRARY', 'Asset Classification', '0', '3', 'flaticon-visible9', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('PSMS-SUPPLIES-CATEGORIES', 'PSMS', 'PSMS-CODE-LIBRARY', 'Supplies Category and Items', '0', '4', 'flaticon-visible9', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('PSMS-REPORTS', 'PSMS', 'Reports', '1', '4', 'flaticon-clipboard94', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `display_child_flag`) VALUES ('PSMS-R1', 'PSMS', 'PSMS-REPORTS', 'Reports of supplies and materials issued', '0', '1', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `display_child_flag`) VALUES ('PSMS-R2', 'PSMS', 'PSMS-REPORTS', 'Report of the Physical Count of Property, Plant and Equipment', '0', '2', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `display_child_flag`) VALUES ('PSMS-R3', 'PSMS', 'PSMS-REPORTS', 'Distribution of ICT Equipment', '0', '3', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `display_child_flag`) VALUES ('PSMS-R4', 'PSMS', 'PSMS-REPORTS', 'Inventory Report of Unserviceable Property', '0', '4', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `display_child_flag`) VALUES ('PSMS-R5', 'PSMS', 'PSMS-REPORTS', 'Inventory of ICT Equipment', '0', '5', '0');
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`, `display_child_flag`) VALUES ('PSMS-R6', 'PSMS', 'PSMS-REPORTS', 'Stock Card', '0', '6', '0');
UPDATE cias_param_resource
SET icon = NULL
WHERE parent_resource_code = 'PSMS-REPORTS';

UPDATE cias_param_resource
SET icon = NULL
WHERE parent_resource_code = 'PSMS-CODE-LIBRARY';

UPDATE cias_param_resource
SET icon = NULL
WHERE parent_resource_code = 'PSMS-SUPPLIES';

INSERT INTO cias_permission ( resource_code, role_code )
SELECT 	resource_code, 'PSMS-ADMINISTRATOR'
FROM	cias_param_resource
WHERE	application_code = 'PSMS';

INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `resource_name`, `main_flag`, `sort_no`, `icon`, `display_child_flag`) VALUES ('PSMS-DASHBOARD', 'PSMS', 'Dashboard', '1', '1', 'flaticon-speedometer35', '0');
UPDATE `cias`.`cias_param_resource` SET `sort_no`='3' WHERE `resource_code`='PSMS-SUPPLIES';
UPDATE `cias`.`cias_param_resource` SET `sort_no`='2' WHERE `resource_code`='PSMS-ASSETS';
UPDATE `cias`.`cias_param_resource` SET `sort_no`='4' WHERE `resource_code`='PSMS-CODE-LIBRARY';
UPDATE `cias`.`cias_param_resource` SET `sort_no`='5' WHERE `resource_code`='PSMS-REPORTS';

insert into cias_permission ( resource_code, role_code )
values ('PSMS-DASHBOARD', 'PSMS-ADMINISTRATOR');


-- RUN IN ALL DATABASE IN PHASE TWO

CREATE TABLE `users` (
  `employee_no` varchar(10) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `token` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1


-- JHOMAR GALANO
INSERT INTO `cias`.`cias_user_role` (`employee_no`, `office_code`, `role_code`) VALUES ('2016-001', 'CM', 'PSMS-ADMINISTRATOR');

-- MIK
INSERT INTO `cias`.`cias_param_resource` (`resource_code`, `application_code`, `parent_resource_code`, `resource_name`, `main_flag`, `sort_no`) VALUES ('PSMS-ASSET-CATEGORY', 'PSMS', 'PSMS-CODE-LIBRARY', 'Asset Category', '0', '4');
INSERT INTO `cias`.`cias_permission` (`resource_code`, `role_code`) VALUES ('PSMS-ASSET-CATEGORY', 'PSMS-ADMINISTRATOR');
UPDATE `cias`.`cias_permission` SET `resource_code`='PSMS-SUPPLIES-CATEGORY' WHERE `resource_code`='PSMS-SUPPLIES-CATEGORIES' and`role_code`='PSMS-ADMINISTRATOR';
UPDATE `cias`.`cias_param_resource` SET `resource_code`='PSMS-SUPPLIES-CATEGORY', `resource_name`='Supplies Category' WHERE `resource_code`='PSMS-SUPPLIES-CATEGORIES';
UPDATE `cias`.`cias_param_resource` SET `display_child_flag`='1' WHERE `resource_code`='PSMS-REPORTS';

UPDATE `cias`.`cias_param_resource` SET `resource_code`='PSMS-REPORT-SUPPLIES' WHERE `resource_code`='PSMS-R1';
UPDATE `cias`.`cias_param_resource` SET `resource_code`='PSMS-REPORT-PROPERTY' WHERE `resource_code`='PSMS-R2';
UPDATE `cias`.`cias_param_resource` SET `resource_code`='PSMS-REPORT-ICT-EQUIPMENT' WHERE `resource_code`='PSMS-R3';
UPDATE `cias`.`cias_param_resource` SET `resource_code`='PSMS-REPORT-ICT-EQUIPMENT' WHERE `resource_code`='PSMS-R5';
UPDATE `cias`.`cias_param_resource` SET `resource_code`='PSMS-REPORT-UNSERVICEABLE-PROPERTY' WHERE `resource_code`='PSMS-R4';
UPDATE `cias`.`cias_param_resource` SET `resource_code`='PSMS-REPORT-ICT-EQUIPMENT' WHERE `resource_code`='PSMS-R5';
UPDATE `cias`.`cias_param_resource` SET `resource_code`='PSMS-REPORT-ICT-EQUIPMENT2' WHERE `resource_code`='PSMS-R5';
UPDATE `cias`.`cias_param_resource` SET `resource_code`='PSMS-REPORT-ICTE-DISTRIBUTION' WHERE `resource_code`='PSMS-REPORT-ICT-EQUIPMENT';
UPDATE `cias`.`cias_param_resource` SET `resource_code`='PSMS-REPORT-ICTE-INVENTORY' WHERE `resource_code`='PSMS-REPORT-ICT-EQUIPMENT2';
UPDATE `cias`.`cias_param_resource` SET `resource_code`='PSMS-REPORT-STOCK-CARD' WHERE `resource_code`='PSMS-R6';
UPDATE `cias`.`cias_permission` SET `resource_code`='PSMS-REPORT-ICTE-DISTRIBUTION' WHERE `resource_code`='PSMS-R1' and`role_code`='PSMS-ADMINISTRATOR';
UPDATE `cias`.`cias_permission` SET `resource_code`='PSMS-REPORT-ICTE-INVENTORY' WHERE `resource_code`='PSMS-R2' and`role_code`='PSMS-ADMINISTRATOR';
UPDATE `cias`.`cias_permission` SET `resource_code`='PSMS-REPORT-PROPERTY' WHERE `resource_code`='PSMS-R3' and`role_code`='PSMS-ADMINISTRATOR';
UPDATE `cias`.`cias_permission` SET `resource_code`='PSMS-REPORT-STOCK-CARD' WHERE `resource_code`='PSMS-R4' and`role_code`='PSMS-ADMINISTRATOR';
UPDATE `cias`.`cias_permission` SET `resource_code`='PSMS-REPORT-UNSERVICEABLE-PROPERTY' WHERE `resource_code`='PSMS-R5' and`role_code`='PSMS-ADMINISTRATOR';
UPDATE `cias`.`cias_permission` SET `resource_code`='PSMS-REPORT-SUPPLIES' WHERE `resource_code`='PSMS-R6' and`role_code`='PSMS-ADMINISTRATOR';
