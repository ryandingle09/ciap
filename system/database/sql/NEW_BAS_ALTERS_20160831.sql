-- RYAN DINGLE 2016.08.31 11:59AM

-- MySQL dump 10.13  Distrib 5.7.13, for Linux (x86_64)
--
-- Host: localhost    Database: bas
-- ------------------------------------------------------
-- Server version	5.7.13-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `param_account_codes`
--

LOCK TABLES `param_account_codes` WRITE;
/*!40000 ALTER TABLE `param_account_codes` DISABLE KEYS */;
INSERT INTO `param_account_codes` VALUES 
('501','Personal Services',NULL,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','2016-08-24 16:45:21'),
('50101','Salaries and Wages','501',1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','2016-07-12 17:38:21'),
('50101010','Salaries and Wages - Regular','50101',1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','2016-07-12 17:38:26'),
('50101020','Salaries and Wages - Casual/Contractual','50101',1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','2016-07-12 17:36:55'),
('50102','Other Compensation','501',1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','2016-07-12 17:36:44'),
('50102010','Personal Economic Relief Allowance ( PERA )','50102',1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','2016-07-12 17:37:15'),
('50102020','Representation Allowance ( RA )','50102',1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','0000-00-00 00:00:00'),
('502','Maintainance and Other Operating Expenses',NULL,1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','2016-08-02 09:00:47'),
('50201','Travelling Expenses','502',1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','2016-08-02 16:36:20'),
('50201010','Training Expenses','50201',1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','2016-08-02 16:36:33'),
('50201020','Supplies and Materials','50201',1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','2016-08-02 16:37:19'),
('50202','Utility Exepenses','502',1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','2016-08-02 16:37:51'),
('50202010','Communication Services','50202',1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','2016-08-02 16:38:15'),
('50202020','Advertising Expenses','50202',1,0,'0000-00-00 00:00:00','0000-00-00 00:00:00','2016-08-02 16:38:35');
/*!40000 ALTER TABLE `param_account_codes` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `line_items`
--

LOCK TABLES `line_items` WRITE;
/*!40000 ALTER TABLE `line_items` DISABLE KEYS */;
INSERT INTO `line_items` VALUES (21,2016,1,NULL,'I','Agency Specific Budget',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1,'2016-08-12 09:56:16',1,'2016-08-12 02:31:48'),(22,2016,1,21,'a','General Administration and Support',257000.00,0.00,0.00,0.00,1166000.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1,'2016-08-12 09:56:47',1,'2016-08-12 02:31:46'),(23,2016,1,22,'1','General Administration and Supervision',257000.00,50000.00,91000.00,50000.00,1166000.00,116000.00,1000000.00,50000.00,0.00,796930.00,0.00,0.00,791980.00,0.00,0.00,1000.00,0.00,4000.00,0.00,0.00,1,'2016-08-12 09:58:09',1,'2016-08-12 02:31:44'),(24,2016,1,NULL,'II','Operations',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1,'2016-08-12 09:58:56',1,'2016-08-12 02:31:42'),(25,2016,1,24,'a','MFO1 Construction Industry Regulatory and Enforcement Services',980000.00,0.00,0.00,0.00,980000.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1,'2016-08-12 09:59:41',1,'2016-08-12 02:31:40'),(26,2016,1,25,'ll.a.1','Licensing, Accreditation and registration construction contractors and administration of overseas construction incentive',182000.00,40000.00,51000.00,40000.00,182000.00,40000.00,47000.00,40000.00,0.00,5000.00,0.00,0.00,4000.00,0.00,0.00,9100.00,0.00,0.00,0.00,0.00,1,'2016-08-12 10:01:19',1,'2016-08-12 02:31:38'),(27,2016,1,25,'ll.a.2','Market development and overseas contrcution industry promotion',90000.00,30000.00,30000.00,30000.00,90000.00,30000.00,30000.00,30000.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1,'2016-08-12 10:02:18',1,'2016-08-12 02:31:35'),(28,2016,1,25,'ll.a.3','Monitoring and evaluation of performance of construction contractors',150000.00,50000.00,50000.00,50000.00,150000.00,50000.00,50000.00,50000.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1,'2016-08-12 10:03:53',1,'2016-08-12 02:31:32'),(29,2016,1,25,'II.a.4','Investigation and litigation of violations on Contractors license law',210000.00,70000.00,70000.00,70000.00,210000.00,70000.00,70000.00,70000.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1,'2016-08-12 10:11:15',1,'2016-08-12 02:31:30'),(30,2016,1,25,'II.a.5','Resolution of construction contract claims and disptes under constrcution contract which are bound by arbitration agreement',180000.00,60000.00,60000.00,60000.00,180000.00,60000.00,60000.00,60000.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1,'2016-08-12 10:12:51',1,'2016-08-12 02:31:28'),(31,2016,1,25,'II.a.6','Promotion and development of training and other manpower',150000.00,50000.00,50000.00,50000.00,150000.00,50000.00,50000.00,50000.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,1,'2016-08-12 10:13:30',1,'2016-08-12 02:31:25');
/*!40000 ALTER TABLE `line_items` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

--
-- Dumping data for table `param_check_mode_payments`
--

LOCK TABLES `param_check_mode_payments` WRITE;
/*!40000 ALTER TABLE `param_check_mode_payments` DISABLE KEYS */;
INSERT INTO `param_check_mode_payments` VALUES (1,'MDS Check'),(2,'Commercial Check'),(3,'ADA');
/*!40000 ALTER TABLE `param_check_mode_payments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `param_deduction_types`
--

LOCK TABLES `param_deduction_types` WRITE;
/*!40000 ALTER TABLE `param_deduction_types` DISABLE KEYS */;
INSERT INTO `param_deduction_types` VALUES (1,'PICAM','PICAM'),(2,'Due to Absences','DTA');
/*!40000 ALTER TABLE `param_deduction_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `param_disbursement_funds`
--

LOCK TABLES `param_disbursement_funds` WRITE;
/*!40000 ALTER TABLE `param_disbursement_funds` DISABLE KEYS */;
INSERT INTO `param_disbursement_funds` VALUES (1,'GAA'),(2,'CMDF'),(3,'CIAC');
/*!40000 ALTER TABLE `param_disbursement_funds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `param_offices`
--

LOCK TABLES `param_offices` WRITE;
/*!40000 ALTER TABLE `param_offices` DISABLE KEYS */;
INSERT INTO `param_offices` VALUES (178,NULL,'Office of the president'),(179,NULL,'Office of senate representatives');
/*!40000 ALTER TABLE `param_offices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `param_payees`
--

LOCK TABLES `param_payees` WRITE;
/*!40000 ALTER TABLE `param_payees` DISABLE KEYS */;
INSERT INTO `param_payees` VALUES (1,'Metro Manila Development Authority','Metro Manila Development Authority','9HGTTH09',0,1),(19,'Ninoy Aquino International Airport','NAIA 2,3 and 4','98UYGHK09',1,1),(21,'Philippine National Convention Center','Philippine National Convention Center','9uYHGGK',1,1),(26,'Manpower Services Inc.','Manpower Services Incorporation','59777YHG',1,1),(27,'Manila International Museum','Manila City','yHGTJH',0,1),(28,'Super Market Production','Henry Sy Businesses','8uuYH',0,0);
/*!40000 ALTER TABLE `param_payees` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `param_source_types`
--

LOCK TABLES `param_source_types` WRITE;
/*!40000 ALTER TABLE `param_source_types` DISABLE KEYS */;
INSERT INTO `param_source_types` VALUES (1,'Current Appropriation',1),(2,'Continuing Appropriation',1),(3,'Special Appropriation',1);
/*!40000 ALTER TABLE `param_source_types` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-31 11:42:20
