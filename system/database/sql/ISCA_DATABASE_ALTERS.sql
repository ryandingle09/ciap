-- SAMPLE FORMAT
-- RODEL SATUITO
-- 2016.05.30
-- ALTER STATEMENTS;

/***** PLEASE PUT CONTENTS ABOVE THIS COMMENT. *****

THIS ALTER FILE IS FOR ISCA SCHEMA ONLY!!!!

REMINDER:

1. No DROPPING of table please.
2. When using 'DELIMITER', reset the delimiter to ';' afterwards.
3. Please remove 'DEFINER=`root`@`localhost`' when creating a trigger.
4. Always check if the statement ends with a DELIMITER (;).
5. When inserting, use the format: INSERT INTO <table_name> (<field_1>, <field_2>, ...) VALUES (<value_1>, <value_2>, ...);
6. NAME FORMAT: REJ MEDIODIA 2015.01.23 11:43AM 
*********** NO STATEMENTS BELOW THIS. **************/


/*jeff 06/28/2016 */

ALTER TABLE `isca`.`param_case_types` 
ADD COLUMN `created_by` VARCHAR(100) NULL AFTER `case_type_name`,
ADD COLUMN `created_date` DATETIME NULL AFTER `created_by`,
ADD COLUMN `status` TINYINT(1) NULL AFTER `created_date`;

ALTER TABLE `isca`.`param_case_status` 
ADD COLUMN `created_by` VARCHAR(100) NULL AFTER `case_status_desc`,
ADD COLUMN `created_date` DATETIME NULL AFTER `created_by`,
ADD COLUMN `status` TINYINT(1) NULL AFTER `created_date`;

ALTER TABLE `isca`.`param_appeal_status` 
ADD COLUMN `created_by` VARCHAR(100) NULL AFTER `appeal_status_desc`,
ADD COLUMN `created_date` DATETIME NULL AFTER `created_by`,
ADD COLUMN `status` TINYINT(1) NULL AFTER `created_date`;

ALTER TABLE `isca`.`param_document_types` 
ADD COLUMN `created_by` VARCHAR(100) NULL AFTER `display_party`,
ADD COLUMN `created_date` DATETIME NULL AFTER `created_by`,
ADD COLUMN `status` TINYINT(1) NULL AFTER `created_date`;

/*jeff 06/29/2016*/
ALTER TABLE `isca`.`param_case_activity` 
ADD COLUMN `created_by` VARCHAR(100) NULL AFTER `case_activity`,
ADD COLUMN `created_date` DATETIME NULL AFTER `created_by`,
ADD COLUMN `status` TINYINT(1) NULL AFTER `created_date`;

ALTER TABLE `isca`.`param_policy_category` 
ADD COLUMN `created_by` VARCHAR(100) NULL AFTER `policy_category`,
ADD COLUMN `created_date` DATETIME NULL AFTER `created_by`,
ADD COLUMN `status` TINYINT(1) NULL AFTER `created_date`;

ALTER TABLE `isca`.`param_meeting_attendees` 
ADD COLUMN `created_by` VARCHAR(100) NULL AFTER `attendee_position`,
ADD COLUMN `created_date` DATETIME NULL AFTER `created_by`,
ADD COLUMN `status` TINYINT(1) NULL AFTER `created_date`;

ALTER TABLE `isca`.`param_professions` 
ADD COLUMN `created_by` VARCHAR(100) NULL AFTER `profession_name`,
ADD COLUMN `created_date` DATETIME NULL AFTER `created_by`,
ADD COLUMN `status` TINYINT(1) NULL AFTER `created_date`;

ALTER TABLE `isca`.`param_book_authors` 
ADD COLUMN `created_by` VARCHAR(100) NULL AFTER `book_author_name`,
ADD COLUMN `created_date` DATETIME NULL AFTER `created_by`,
ADD COLUMN `status` TINYINT(1) NULL AFTER `created_date`;


CREATE TABLE IF NOT EXIST `isca`.`param_mace_rule` (
`mace_rule_id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
`mace_credir_unit_required` INT(11) NULL,
`mace_credit_unit_period` INT(11) NULL,
`created_by` VARCHAR(100) NULL,
`created_date` DATETIME NULL,
`status` TINYINT(1) NULL,
PRIMARY KEY (`mace_rule_id`));

CREATE TABLE IF NOT EXIST `isca`.`param_nature_case` (
`nature_case_id` INT NOT NULL AUTO_INCREMENT,
`nature_case_name` VARCHAR(100) NULL,
`created_by` VARCHAR(100) NULL,
`created_date` DATETIME NULL,
`status` TINYINT(1) NULL,
PRIMARY KEY (`nature_case_id`));

ALTER TABLE `isca`.`cases` 
CHANGE COLUMN `case_nature` `case_nature` INT(10) NULL DEFAULT NULL COMMENT 'G - Government Contract, P - Private Contract' ;

ALTER TABLE `isca`.`cases` 
ADD INDEX `fk_cases_1_idx` (`case_nature` ASC);

ALTER TABLE `isca`.`cases` 
ADD CONSTRAINT `fk_cases_1`
FOREIGN KEY (`case_nature`)
REFERENCES `isca`.`param_nature_case` (`nature_case_id`)
ON DELETE NO ACTION
ON UPDATE NO ACTION;

ALTER TABLE `isca`.`books` 
DROP FOREIGN KEY `fk_books -_param_book_classifications -1`;
ALTER TABLE `isca`.`books` 
CHANGE COLUMN `book_class_no` `book_class_no` INT(10) NULL ;
