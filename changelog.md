CHANGELOG

/*
|---------------------------------------------------------------------
| popModal
|---------------------------------------------------------------------
| Date Modified: 03/18/2016
| Author: vadimsva
| Github: https://github.com/vadimsva/popModal
*/

- upgraded the version from v1.20 to v1.21
- customized styling for notifyModal and confirmModal

/*
|---------------------------------------------------------------------
| Materialize ()
|---------------------------------------------------------------------
| Date Modified: 03/10/2016
*/

- upgraded the version from v0.97.0 to v0.97.5
- added :not(.labelauty) selector to all [type="radio"] and [type="checkbox"]
- revised function accordionOpen(object) in materialize.js
  
  -- LINE 349 inside if (object.parent().hasClass('active')){} statement
	/* ADDED ARROW DOWN ON COLLAPSIBLE MENU */
	if(object.parent().parent().hasClass("menu"))
		object.parent().has(".collapsible-body").find(".collapsible-header").addClass("flaticon-down95").removeClass("flaticon-arrow621");
  
  -- LINE 352 inside else part of if (object.parent().hasClass('active')){} statement
    /* ADDED ARROW UP ON COLLAPSIBLE MENU */
    if(object.parent().parent().hasClass("menu"))
		object.parent().has(".collapsible-body").find(".collapsible-header").addClass("flaticon-arrow621").removeClass("flaticon-down95");