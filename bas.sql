-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 25, 2016 at 03:39 PM
-- Server version: 5.7.13-0ubuntu0.16.04.2
-- PHP Version: 7.0.10-1+deb.sury.org~xenial+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bas`
--

-- --------------------------------------------------------

--
-- Table structure for table `arbitrators`
--

CREATE TABLE `arbitrators` (
  `arbitrator_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` int(10) UNSIGNED DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `arbitrators`
--

INSERT INTO `arbitrators` (`arbitrator_id`, `name`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
(1, 'Ryan Dingle', 1, '2016-08-10 00:00:00', NULL, NULL),
(2, 'Scott Johanson', 1, '2016-08-04 00:00:00', NULL, NULL),
(3, NULL, 1, '2016-08-23 08:00:22', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `billings`
--

CREATE TABLE `billings` (
  `billing_id` int(10) UNSIGNED NOT NULL,
  `obligation_id` int(10) UNSIGNED NOT NULL,
  `bill_no` varchar(100) NOT NULL,
  `bill_date` date NOT NULL,
  `due_date` date DEFAULT NULL,
  `amount_due` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `billings`
--

INSERT INTO `billings` (`billing_id`, `obligation_id`, `bill_no`, `bill_date`, `due_date`, `amount_due`) VALUES
(4, 110, '56789', '2016-08-25', '2016-08-24', '3000.00'),
(5, 103, '324423432', '2016-08-26', '2016-08-25', '300.00');

-- --------------------------------------------------------

--
-- Table structure for table `cases`
--

CREATE TABLE `cases` (
  `case_id` int(10) UNSIGNED NOT NULL,
  `case_no` char(7) NOT NULL,
  `case_title` text NOT NULL,
  `created_by` int(10) UNSIGNED DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `last_modified_by` int(10) UNSIGNED DEFAULT NULL,
  `last_modified_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cases`
--

INSERT INTO `cases` (`case_id`, `case_no`, `case_title`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
(1, '11', 'case 1', 1, '2016-08-17 00:00:00', NULL, NULL),
(2, '12', 'case 2', 1, '2016-08-17 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `case_arbitrator_deductions`
--

CREATE TABLE `case_arbitrator_deductions` (
  `arbitrator_payment_id` int(10) UNSIGNED NOT NULL,
  `arbitrator_id` int(10) UNSIGNED NOT NULL,
  `deduction_type_id` int(10) UNSIGNED NOT NULL,
  `amount` decimal(25,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `case_arbitrator_deductions`
--

INSERT INTO `case_arbitrator_deductions` (`arbitrator_payment_id`, `arbitrator_id`, `deduction_type_id`, `amount`) VALUES
(1, 1, 2, '500.00'),
(2, 2, 2, '400.00');

-- --------------------------------------------------------

--
-- Table structure for table `case_arbitrator_fee_shares`
--

CREATE TABLE `case_arbitrator_fee_shares` (
  `arbitrator_payment_id` int(10) UNSIGNED NOT NULL,
  `arbitrator_id` int(10) UNSIGNED NOT NULL,
  `stage_activity_id` int(10) UNSIGNED NOT NULL,
  `percent` decimal(5,2) NOT NULL DEFAULT '0.00',
  `total_amount` decimal(25,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `case_arbitrator_fee_shares`
--

INSERT INTO `case_arbitrator_fee_shares` (`arbitrator_payment_id`, `arbitrator_id`, `stage_activity_id`, `percent`, `total_amount`) VALUES
(1, 1, 1, '5.00', '2000.00'),
(2, 2, 2, '3.00', '5000.00');

-- --------------------------------------------------------

--
-- Table structure for table `case_arbitrator_payments`
--

CREATE TABLE `case_arbitrator_payments` (
  `arbitrator_payment_id` int(10) UNSIGNED NOT NULL,
  `case_id` int(10) UNSIGNED NOT NULL,
  `proceeding_stage_id` int(10) UNSIGNED NOT NULL,
  `final_flag` tinyint(1) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `last_modified_by` int(11) DEFAULT NULL,
  `last_modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `case_arbitrator_payments`
--

INSERT INTO `case_arbitrator_payments` (`arbitrator_payment_id`, `case_id`, `proceeding_stage_id`, `final_flag`, `created_by`, `created_date`, `last_modified_by`, `last_modified_date`) VALUES
(1, 1, 1, 1, 1, '2016-08-17 00:00:00', NULL, NULL),
(2, 2, 2, 1, 1, '2016-08-17 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `checks`
--

CREATE TABLE `checks` (
  `check_id` int(10) UNSIGNED NOT NULL,
  `disbursement_id` int(10) UNSIGNED NOT NULL,
  `mode_of_payment` int(11) NOT NULL,
  `check_date` date NOT NULL,
  `check_no` varchar(100) NOT NULL,
  `check_amount` decimal(25,2) UNSIGNED NOT NULL,
  `check_status_id` int(10) UNSIGNED NOT NULL,
  `remarks` text,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_date` date NOT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `checks`
--

INSERT INTO `checks` (`check_id`, `disbursement_id`, `mode_of_payment`, `check_date`, `check_no`, `check_amount`, `check_status_id`, `remarks`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(1, 22, 2, '2016-08-31', '4534', '32445.00', 1, NULL, 1, '2016-08-25', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `disbursements`
--

CREATE TABLE `disbursements` (
  `disbursement_id` int(10) UNSIGNED NOT NULL,
  `dv_no` varchar(100) DEFAULT NULL,
  `dv_date` date DEFAULT NULL,
  `disbursement_fund_id` int(10) UNSIGNED NOT NULL,
  `obligation_id` int(10) UNSIGNED DEFAULT NULL,
  `billing_id` int(10) UNSIGNED DEFAULT NULL,
  `payee_id` int(10) UNSIGNED DEFAULT NULL,
  `case_no` varchar(100) DEFAULT NULL,
  `proceeding_stage_id` int(10) UNSIGNED DEFAULT NULL,
  `arbitrator_id` int(10) UNSIGNED DEFAULT NULL,
  `disbursed_amount` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `vat_rate` decimal(5,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `vat_amount` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `ewt_rate` decimal(5,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `wht_amount` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `disbursement_status_id` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `payee_address` text,
  `particulars` text,
  `remarks` text,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disbursements`
--

INSERT INTO `disbursements` (`disbursement_id`, `dv_no`, `dv_date`, `disbursement_fund_id`, `obligation_id`, `billing_id`, `payee_id`, `case_no`, `proceeding_stage_id`, `arbitrator_id`, `disbursed_amount`, `vat_rate`, `vat_amount`, `ewt_rate`, `wht_amount`, `disbursement_status_id`, `payee_address`, `particulars`, `remarks`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(21, '16-08-1', '2016-08-12', 1, 103, 4, 19, NULL, NULL, NULL, '800.00', '50.00', '6000.00', '45.00', '7000.00', 21, 'NAIA 2,3 and 4', 'test 2', 'voided', 1, '2016-08-12 10:53:56', 1, '2016-08-19 06:16:38'),
(22, '16-08-2', '2016-08-12', 1, 103, 4, 19, NULL, NULL, NULL, '2000.00', '5.00', '1000.00', '5.00', '2000.00', 22, 'NAIA 2,3 and 4', 'test 2', NULL, 1, '2016-08-12 10:54:09', 1, '2016-08-17 06:49:08'),
(23, '16-08-3', '2016-08-12', 2, NULL, NULL, 1, NULL, NULL, NULL, '9000.00', '27.00', '7000.00', '27.00', '12000.00', 23, 'Metro Manila Development Authority', 'test 1', NULL, 1, '2016-08-12 10:55:24', NULL, NULL),
(29, '16-08-4', '2016-08-17', 3, NULL, NULL, 1, '1', 1, 1, '2000.00', '0.00', '0.00', '0.00', '0.00', 30, NULL, 'qdqwdqwd', NULL, 1, '2016-08-17 09:40:23', NULL, NULL),
(31, '16-08-5', '2016-08-17', 3, NULL, NULL, NULL, '2', 2, 2, '5000.00', '0.00', '0.00', '0.00', '0.00', 32, NULL, 'test 2', NULL, 1, '2016-08-17 11:17:20', NULL, NULL),
(32, '16-08-6', '2016-08-17', 3, NULL, NULL, NULL, '1', 1, 1, '2000.00', '0.00', '0.00', '0.00', '0.00', 33, NULL, 'rrer', NULL, 1, '2016-08-17 17:40:52', NULL, NULL),
(33, '16-08-7', '2016-08-18', 3, NULL, NULL, NULL, '2', 2, 2, '5000.00', '0.00', '0.00', '0.00', '0.00', 34, NULL, 'hello world superman', NULL, 1, '2016-08-18 17:16:44', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `disbursement_charges`
--

CREATE TABLE `disbursement_charges` (
  `disbursement_id` int(10) UNSIGNED NOT NULL,
  `line_item_id` int(10) UNSIGNED DEFAULT NULL,
  `account_code` varchar(100) NOT NULL,
  `obligated_amount` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `disbursed_amount` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `base_tax_amount` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `ewt_rate` decimal(5,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `ewt_amount` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `vat_rate` decimal(5,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `vat_amount` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disbursement_charges`
--

INSERT INTO `disbursement_charges` (`disbursement_id`, `line_item_id`, `account_code`, `obligated_amount`, `disbursed_amount`, `base_tax_amount`, `ewt_rate`, `ewt_amount`, `vat_rate`, `vat_amount`) VALUES
(23, NULL, '50301010', '0.00', '5000.00', '5000.00', '12.00', '7000.00', '12.00', '4000.00'),
(23, NULL, '50101010', '0.00', '4000.00', '4000.00', '15.00', '5000.00', '15.00', '3000.00'),
(22, 23, '50102010', '400000.00', '2000.00', '4000.00', '5.00', '2000.00', '5.00', '1000.00'),
(21, 23, '50102010', '300000.00', '80000.00', '4000.00', '45.00', '7000.00', '50.00', '6000.00');

-- --------------------------------------------------------

--
-- Table structure for table `disbursement_deductions`
--

CREATE TABLE `disbursement_deductions` (
  `deduction_id` int(10) UNSIGNED NOT NULL,
  `disbursement_id` int(10) UNSIGNED NOT NULL,
  `deduction` varchar(100) NOT NULL,
  `deduction_amount` decimal(25,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disbursement_deductions`
--

INSERT INTO `disbursement_deductions` (`deduction_id`, `disbursement_id`, `deduction`, `deduction_amount`) VALUES
(8, 32, 'PICAM', '900.00'),
(10, 33, 'PICAM', '300.00'),
(12, 29, 'PICAM', '900.00');

-- --------------------------------------------------------

--
-- Table structure for table `fund_sources`
--

CREATE TABLE `fund_sources` (
  `year` int(4) UNSIGNED NOT NULL,
  `source_type_num` int(10) UNSIGNED NOT NULL,
  `final_flag` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `fund_sources`
--

INSERT INTO `fund_sources` (`year`, `source_type_num`, `final_flag`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(2016, 1, 1, 1, '2016-08-12 10:31:25', NULL, '2016-08-12 02:31:25');

-- --------------------------------------------------------

--
-- Table structure for table `line_items`
--

CREATE TABLE `line_items` (
  `line_item_id` int(10) UNSIGNED NOT NULL COMMENT 'Primary key',
  `year` int(4) UNSIGNED NOT NULL,
  `source_type_num` int(10) UNSIGNED NOT NULL,
  `parent_line_item_id` int(10) UNSIGNED DEFAULT NULL,
  `line_item_code` varchar(10) CHARACTER SET latin1 COLLATE latin1_general_cs DEFAULT NULL,
  `line_item_name` varchar(255) NOT NULL,
  `total_appropriation` decimal(25,2) NOT NULL DEFAULT '0.00',
  `ps_appropriation` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `mooe_appropriation` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `co_appropriation` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `total_allotment` decimal(25,2) NOT NULL DEFAULT '0.00',
  `ps_allotment` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `mooe_allotment` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `co_allotment` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `ps_appropriation_realigned` decimal(25,2) NOT NULL DEFAULT '0.00',
  `mooe_appropriation_realigned` decimal(25,2) NOT NULL DEFAULT '0.00',
  `co_appropriation_realigned` decimal(25,2) NOT NULL DEFAULT '0.00',
  `ps_allotment_realigned` decimal(25,2) NOT NULL DEFAULT '0.00',
  `mooe_allotment_realigned` decimal(25,2) NOT NULL DEFAULT '0.00',
  `co_allotment_realigned` decimal(25,2) NOT NULL DEFAULT '0.00',
  `ps_obligation` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `mooe_obligation` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `co_obligation` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `ps_disbursement` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `mooe_disbursement` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `co_disbursement` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `line_items`
--

INSERT INTO `line_items` (`line_item_id`, `year`, `source_type_num`, `parent_line_item_id`, `line_item_code`, `line_item_name`, `total_appropriation`, `ps_appropriation`, `mooe_appropriation`, `co_appropriation`, `total_allotment`, `ps_allotment`, `mooe_allotment`, `co_allotment`, `ps_appropriation_realigned`, `mooe_appropriation_realigned`, `co_appropriation_realigned`, `ps_allotment_realigned`, `mooe_allotment_realigned`, `co_allotment_realigned`, `ps_obligation`, `mooe_obligation`, `co_obligation`, `ps_disbursement`, `mooe_disbursement`, `co_disbursement`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(21, 2016, 1, NULL, 'I', 'Agency Specific Budget', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '2016-08-12 09:56:16', 1, '2016-08-12 02:31:48'),
(22, 2016, 1, 21, 'a', 'General Administration and Support', '248000.00', '0.00', '0.00', '0.00', '1157000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '2016-08-12 09:56:47', 1, '2016-08-12 02:31:46'),
(23, 2016, 1, 22, '1', 'General Administration and Supervision', '248000.00', '50000.00', '91000.00', '50000.00', '1157000.00', '107000.00', '1000000.00', '50000.00', '0.00', '796930.00', '0.00', '0.00', '791980.00', '0.00', '0.00', '1000.00', '0.00', '4000.00', '0.00', '0.00', 1, '2016-08-12 09:58:09', 1, '2016-08-12 02:31:44'),
(24, 2016, 1, NULL, 'II', 'Operations', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '2016-08-12 09:58:56', 1, '2016-08-12 02:31:42'),
(25, 2016, 1, 24, 'a', 'MFO1 Construction Industry Regulatory and Enforcement Services', '980000.00', '0.00', '0.00', '0.00', '980000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '2016-08-12 09:59:41', 1, '2016-08-12 02:31:40'),
(26, 2016, 1, 25, 'll.a.1', 'Licensing, Accreditation and registration construction contractors and administration of overseas construction incentive', '182000.00', '40000.00', '51000.00', '40000.00', '182000.00', '40000.00', '47000.00', '40000.00', '0.00', '5000.00', '0.00', '0.00', '4000.00', '0.00', '0.00', '9100.00', '0.00', '0.00', '0.00', '0.00', 1, '2016-08-12 10:01:19', 1, '2016-08-12 02:31:38'),
(27, 2016, 1, 25, 'll.a.2', 'Market development and overseas contrcution industry promotion', '90000.00', '30000.00', '30000.00', '30000.00', '90000.00', '30000.00', '30000.00', '30000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '2016-08-12 10:02:18', 1, '2016-08-12 02:31:35'),
(28, 2016, 1, 25, 'll.a.3', 'Monitoring and evaluation of performance of construction contractors', '150000.00', '50000.00', '50000.00', '50000.00', '150000.00', '50000.00', '50000.00', '50000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '2016-08-12 10:03:53', 1, '2016-08-12 02:31:32'),
(29, 2016, 1, 25, 'II.a.4', 'Investigation and litigation of violations on Contractors license law', '210000.00', '70000.00', '70000.00', '70000.00', '210000.00', '70000.00', '70000.00', '70000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '2016-08-12 10:11:15', 1, '2016-08-12 02:31:30'),
(30, 2016, 1, 25, 'II.a.5', 'Resolution of construction contract claims and disptes under constrcution contract which are bound by arbitration agreement', '180000.00', '60000.00', '60000.00', '60000.00', '180000.00', '60000.00', '60000.00', '60000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '2016-08-12 10:12:51', 1, '2016-08-12 02:31:28'),
(31, 2016, 1, 25, 'II.a.6', 'Promotion and development of training and other manpower', '150000.00', '50000.00', '50000.00', '50000.00', '150000.00', '50000.00', '50000.00', '50000.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', '0.00', 1, '2016-08-12 10:13:30', 1, '2016-08-12 02:31:25');

-- --------------------------------------------------------

--
-- Table structure for table `line_item_accounts`
--

CREATE TABLE `line_item_accounts` (
  `line_item_id` int(10) UNSIGNED NOT NULL,
  `account_code` varchar(100) NOT NULL,
  `appropriation_amount` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `allotment_amount` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `realigned_amount` decimal(25,2) NOT NULL,
  `obligated_amount` decimal(25,2) NOT NULL,
  `disbursed_amount` decimal(25,2) NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `line_item_accounts`
--

INSERT INTO `line_item_accounts` (`line_item_id`, `account_code`, `appropriation_amount`, `allotment_amount`, `realigned_amount`, `obligated_amount`, `disbursed_amount`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(23, '50101010', '10000.00', '100000.00', '0.00', '0.00', '0.00', 1, '2016-08-12 10:40:04', 1, '2016-08-22 08:11:45'),
(23, '50101020', '3000.00', '3020.00', '-3000.00', '0.00', '0.00', 1, '2016-08-12 10:40:04', 1, '2016-08-22 08:11:45'),
(23, '50102010', '10000.00', '5050.00', '0.00', '0.00', '4000.00', 1, '2016-08-12 10:40:04', 1, '2016-08-22 08:11:45'),
(23, '50201010', '5000.00', '3000.00', '0.00', '0.00', '0.00', 1, '2016-08-12 10:40:34', 1, '2016-08-22 07:49:50'),
(23, '50202010', '4000.00', '2000.00', '0.00', '0.00', '0.00', 1, '2016-08-12 10:40:34', 1, '2016-08-22 07:49:50'),
(23, '50202020', '70.00', '5000.00', '0.00', '0.00', '0.00', 1, '2016-08-22 15:28:25', 1, '2016-08-22 07:49:50'),
(23, '50301010', '6000.00', '4000.00', '0.00', '0.00', '0.00', 1, '2016-08-12 10:43:54', NULL, '2016-08-12 02:43:54'),
(23, '50302010', '6000.00', '40000.00', '0.00', '0.00', '0.00', 1, '2016-08-12 10:43:54', NULL, '2016-08-12 02:43:54'),
(26, '50101020', '8000.00', '8000.00', '0.00', '0.00', '0.00', 1, '2016-08-12 10:45:32', NULL, '2016-08-12 02:45:32'),
(26, '50202010', '5000.00', '4000.00', '3000.00', '0.00', '0.00', 1, '2016-08-12 10:45:44', NULL, '2016-08-12 02:45:44'),
(26, '50301020', '5000.00', '20000.00', '0.00', '0.00', '0.00', 1, '2016-08-12 10:45:55', NULL, '2016-08-12 02:45:55');

-- --------------------------------------------------------

--
-- Table structure for table `line_item_offices`
--

CREATE TABLE `line_item_offices` (
  `line_item_id` int(10) UNSIGNED NOT NULL,
  `office_num` int(10) UNSIGNED NOT NULL,
  `total_appropriation` decimal(25,2) NOT NULL DEFAULT '0.00',
  `ps_appropriation` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `mooe_appropriation` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `co_appropriation` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `total_allotment` decimal(25,2) NOT NULL DEFAULT '0.00',
  `ps_allotment` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `mooe_allotment` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `co_allotment` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `total_obligated_amount` decimal(25,2) NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_date` datetime DEFAULT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `line_item_offices`
--

INSERT INTO `line_item_offices` (`line_item_id`, `office_num`, `total_appropriation`, `ps_appropriation`, `mooe_appropriation`, `co_appropriation`, `total_allotment`, `ps_allotment`, `mooe_allotment`, `co_allotment`, `total_obligated_amount`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(23, 178, '0.00', '0.00', '40000.00', '0.00', '0.00', '0.00', '20000.00', '0.00', '0.00', 1, '2016-08-12 10:43:22', 1, '2016-08-19 08:38:09'),
(23, 179, '-700.00', '0.00', '300.00', '0.00', '-3300.00', '0.00', '700.00', '0.00', '0.00', 1, '2016-08-12 10:43:22', 1, '2016-08-19 08:38:09'),
(26, 178, '6000.00', '0.00', '6000.00', '0.00', '6000.00', '0.00', '6000.00', '0.00', '0.00', 1, '2016-08-12 10:46:03', 1, '2016-08-19 07:32:39'),
(26, 179, '0.00', '0.00', '5000.00', '0.00', '0.00', '0.00', '4000.00', '0.00', '0.00', 1, '2016-08-12 10:46:14', NULL, '2016-08-12 02:46:14');

-- --------------------------------------------------------

--
-- Table structure for table `line_item_realigns`
--

CREATE TABLE `line_item_realigns` (
  `realign_id` int(10) UNSIGNED NOT NULL,
  `year` int(4) UNSIGNED NOT NULL,
  `source_type_num` int(10) UNSIGNED NOT NULL,
  `release_date` date NOT NULL,
  `reference_no` varchar(100) NOT NULL,
  `posted_flag` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `line_item_realigns`
--

INSERT INTO `line_item_realigns` (`realign_id`, `year`, `source_type_num`, `release_date`, `reference_no`, `posted_flag`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(5, 2016, 1, '2016-08-25', '67899', 0, 1, '2016-08-16 18:27:13', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `line_item_realign_details`
--

CREATE TABLE `line_item_realign_details` (
  `realign_id` int(10) UNSIGNED NOT NULL,
  `realigned_amount` decimal(25,2) NOT NULL,
  `source_line_item_id` int(10) UNSIGNED NOT NULL,
  `source_account_code` varchar(100) NOT NULL,
  `recipient_line_item_id` int(10) UNSIGNED NOT NULL,
  `recipient_account_code` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `line_item_realign_details`
--

INSERT INTO `line_item_realign_details` (`realign_id`, `realigned_amount`, `source_line_item_id`, `source_account_code`, `recipient_line_item_id`, `recipient_account_code`) VALUES
(5, '3000.00', 23, '50101020', 26, '50202010');

-- --------------------------------------------------------

--
-- Table structure for table `line_item_releases`
--

CREATE TABLE `line_item_releases` (
  `release_id` int(10) UNSIGNED NOT NULL,
  `line_item_id` int(10) UNSIGNED NOT NULL,
  `reference_no` varchar(100) NOT NULL,
  `reference_date` date DEFAULT NULL,
  `total_release_amount` decimal(25,2) NOT NULL,
  `created_by` int(10) UNSIGNED NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(10) UNSIGNED DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `line_item_releases`
--

INSERT INTO `line_item_releases` (`release_id`, `line_item_id`, `reference_no`, `reference_date`, `total_release_amount`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(15, 23, '1234', '2016-08-12', '100000.00', 1, '2016-08-12 10:38:50', NULL, '2016-08-12 02:38:50'),
(16, 23, '9000', '2016-08-13', '80000.00', 1, '2016-08-12 10:44:53', NULL, '2016-08-12 02:44:53'),
(17, 26, '20000', '2016-08-31', '8000.00', 1, '2016-08-12 10:46:34', NULL, '2016-08-12 02:46:34'),
(18, 23, '34534', '2016-08-25', '50000.00', 1, '2016-08-12 11:48:39', NULL, '2016-08-12 03:48:39'),
(19, 23, '7000', '2016-09-22', '20000.00', 1, '2016-08-12 11:52:35', NULL, '2016-08-12 03:52:35'),
(20, 23, '3000', '2016-08-17', '30000.00', 1, '2016-08-12 11:53:14', NULL, '2016-08-12 03:53:14'),
(25, 23, '443234', '2016-08-18', '7000.00', 1, '2016-08-22 16:46:33', NULL, '2016-08-22 08:46:33'),
(26, 23, '2342', '2016-08-25', '8000.00', 1, '2016-08-22 16:47:18', NULL, '2016-08-22 08:47:18'),
(27, 23, '345324', '2016-08-02', '6000.00', 1, '2016-08-22 16:48:39', NULL, '2016-08-22 08:48:39'),
(28, 23, '32424', '2016-08-11', '1000.00', 1, '2016-08-22 17:31:25', NULL, '2016-08-22 09:31:25');

-- --------------------------------------------------------

--
-- Table structure for table `line_item_release_details`
--

CREATE TABLE `line_item_release_details` (
  `release_id` int(10) UNSIGNED NOT NULL,
  `account_code` varchar(100) NOT NULL,
  `allotment_amount` decimal(25,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `line_item_release_details`
--

INSERT INTO `line_item_release_details` (`release_id`, `account_code`, `allotment_amount`) VALUES
(15, '50101010', '100000.00'),
(16, '50201', '80000.00'),
(17, '50102020', '8000.00'),
(18, '50102010', '50000.00'),
(19, '50101020', '20000.00'),
(20, '50102020', '30000.00'),
(25, '501', '7000.00'),
(26, '501', '8000.00'),
(27, '501', '6000.00'),
(28, '50101020', '1000.00');

-- --------------------------------------------------------

--
-- Table structure for table `obligations`
--

CREATE TABLE `obligations` (
  `obligation_id` int(11) UNSIGNED NOT NULL,
  `manual_flag` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `os_no` varchar(100) DEFAULT NULL,
  `os_date` date DEFAULT NULL,
  `requested_date` date NOT NULL,
  `payee_id` int(10) UNSIGNED NOT NULL,
  `address` text,
  `particulars` text NOT NULL,
  `year` int(4) UNSIGNED NOT NULL,
  `source_type_num` int(10) UNSIGNED NOT NULL,
  `requested_amount` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `obligated_amount` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `disbursed_amount` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `obligation_status_id` int(10) UNSIGNED NOT NULL,
  `remarks` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obligations`
--

INSERT INTO `obligations` (`obligation_id`, `manual_flag`, `os_no`, `os_date`, `requested_date`, `payee_id`, `address`, `particulars`, `year`, `source_type_num`, `requested_amount`, `obligated_amount`, `disbursed_amount`, `obligation_status_id`, `remarks`) VALUES
(102, 0, NULL, NULL, '2016-08-31', 1, 'Metro Manila Development Authority', 'test 1', 2016, 1, '7000.00', '2500.00', '0.00', 104, NULL),
(103, 0, '16-12-2', '2016-08-12', '2016-08-06', 19, 'NAIA 2,3 and 4', 'test 2', 2016, 1, '5000.00', '3000.00', '0.00', 105, NULL),
(104, 0, '16-12-3', '2016-08-12', '2016-08-31', 28, 'Henry Sy Businesses', 'test 3', 2016, 1, '500.00', '100.00', '0.00', 106, NULL),
(110, 0, '16-19-5', '2016-08-19', '2016-08-24', 19, 'NAIA 2,3 and 4', 'rgegreg', 2016, 1, '900.00', '9000.00', '0.00', 114, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `obligation_charges`
--

CREATE TABLE `obligation_charges` (
  `obligation_id` int(11) UNSIGNED NOT NULL,
  `line_item_id` int(10) UNSIGNED NOT NULL,
  `account_code` varchar(100) NOT NULL,
  `office_num` varchar(100) NOT NULL,
  `requested_amount` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `obligated_amount` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00',
  `disbursed_amount` decimal(25,2) UNSIGNED NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obligation_charges`
--

INSERT INTO `obligation_charges` (`obligation_id`, `line_item_id`, `account_code`, `office_num`, `requested_amount`, `obligated_amount`, `disbursed_amount`) VALUES
(102, 23, '50101010', '178', '5000.00', '2000.00', '0.00'),
(102, 26, '50202010', '179', '2000.00', '500.00', '0.00'),
(103, 23, '50102010', '178', '5000.00', '3000.00', '0.00'),
(104, 26, '50301020', '178', '500.00', '100.00', '0.00'),
(110, 26, '50101020', '178', '900.00', '9000.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `obligation_signatories`
--

CREATE TABLE `obligation_signatories` (
  `signatory_id` int(10) UNSIGNED NOT NULL,
  `obligation_id` int(10) UNSIGNED NOT NULL,
  `role` enum('R','C') NOT NULL DEFAULT 'R' COMMENT 'R - Requested By; C - Certify By',
  `name` varchar(100) NOT NULL,
  `designation` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `obligation_signatories`
--

INSERT INTO `obligation_signatories` (`signatory_id`, `obligation_id`, `role`, `name`, `designation`) VALUES
(52, 94, 'C', 'EDNA S. DOMINGO', 'BUDGET OFFICER III'),
(53, 95, 'C', 'EDNA S. DOMINGO', 'BUDGET OFFICER III'),
(54, 96, 'C', 'EDNA S. DOMINGO', 'BUDGET OFFICER III'),
(55, 97, 'C', 'EDNA S. DOMINGO', 'BUDGET OFFICER III'),
(56, 98, 'C', 'EDNA S. DOMINGO', 'BUDGET OFFICER III'),
(57, 99, 'C', 'EDNA S. DOMINGO', 'BUDGET OFFICER III'),
(58, 100, 'C', 'EDNA S. DOMINGO', 'BUDGET OFFICER III'),
(59, 101, 'C', 'EDNA S. DOMINGO', 'BUDGET OFFICER III'),
(60, 102, 'C', 'EDNA S. DOMINGO', 'BUDGET OFFICER III'),
(61, 103, 'C', 'EDNA S. DOMINGO', 'BUDGET OFFICER III'),
(62, 104, 'C', 'EDNA S. DOMINGO', 'BUDGET OFFICER III'),
(68, 110, 'C', 'EDNA S. DOMINGO', 'BUDGET OFFICER III');

-- --------------------------------------------------------

--
-- Table structure for table `param_account_codes`
--

CREATE TABLE `param_account_codes` (
  `account_code` varchar(100) NOT NULL,
  `account_name` varchar(255) NOT NULL,
  `parent_account_code` varchar(100) DEFAULT NULL,
  `active_flag` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` datetime NOT NULL,
  `modified_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `param_account_codes`
--

INSERT INTO `param_account_codes` (`account_code`, `account_name`, `parent_account_code`, `active_flag`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
('501', 'Personal Services', NULL, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-24 16:45:21'),
('50101', 'Salaries and Wages', '501', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-12 17:38:21'),
('50101010', 'Salaries and Wages - Regular', '50101', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-12 17:38:26'),
('50101020', 'Salaries and Wages - Casual/Contractual', '50101', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-12 17:36:55'),
('50102', 'Other Compensation', '501', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-12 17:36:44'),
('50102010', 'Personal Economic Relief Allowance ( PERA )', '50102', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-07-12 17:37:15'),
('50102020', 'Representation Allowance ( RA )', '50102', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
('502', 'Maintainance and Other Operating Expenses', NULL, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-02 09:00:47'),
('50201', 'Travelling Expenses', '502', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-02 16:36:20'),
('50201010', 'Training Expenses', '50201', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-02 16:36:33'),
('50201020', 'Supplies and Materials', '50201', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-02 16:37:19'),
('50202', 'Utility Exepenses', '502', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-02 16:37:51'),
('50202010', 'Communication Services', '50202', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-02 16:38:15'),
('50202020', 'Advertising Expenses', '50202', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-02 16:38:35'),
('503', 'CO', NULL, 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-02 09:02:08'),
('50301', 'CO Salaries and Wages', '503', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-12 09:37:03'),
('50301010', 'CO Salaries and Wages - Regular', '50301', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-12 09:37:11'),
('50301020', 'CO Salaries and Wages - Casual/Contractual', '50301', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-12 09:37:18'),
('50302', 'CO Other Compensation', '503', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-12 09:37:25'),
('50302010', 'CO Personal Economic Relief Allowance ( PERA )', '50302', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-12 09:37:32'),
('50302020', 'CO Representation Allowance ( RA )', '50302', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2016-08-12 09:37:38');

-- --------------------------------------------------------

--
-- Table structure for table `param_check_mode_payments`
--

CREATE TABLE `param_check_mode_payments` (
  `mode_payment_id` int(11) NOT NULL,
  `mode_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `param_check_mode_payments`
--

INSERT INTO `param_check_mode_payments` (`mode_payment_id`, `mode_type`) VALUES
(1, 'MDS Check'),
(2, 'Commercial Check'),
(3, 'ADA');

-- --------------------------------------------------------

--
-- Table structure for table `param_check_status`
--

CREATE TABLE `param_check_status` (
  `check_status_id` int(10) UNSIGNED NOT NULL,
  `status_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `param_check_status`
--

INSERT INTO `param_check_status` (`check_status_id`, `status_name`) VALUES
(1, 'POST');

-- --------------------------------------------------------

--
-- Table structure for table `param_deduction_types`
--

CREATE TABLE `param_deduction_types` (
  `deduction_id` int(11) NOT NULL,
  `deduction_name` varchar(20) NOT NULL,
  `deduction_code` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `param_deduction_types`
--

INSERT INTO `param_deduction_types` (`deduction_id`, `deduction_name`, `deduction_code`) VALUES
(1, 'PICAM', 'PICAM'),
(2, 'Due to Absences', 'DTA');

-- --------------------------------------------------------

--
-- Table structure for table `param_disbursement_funds`
--

CREATE TABLE `param_disbursement_funds` (
  `disbursement_fund_id` int(10) UNSIGNED NOT NULL,
  `fund_name` varchar(50) NOT NULL COMMENT 'GAA, CMDF, CIAC'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `param_disbursement_funds`
--

INSERT INTO `param_disbursement_funds` (`disbursement_fund_id`, `fund_name`) VALUES
(1, 'GAA'),
(2, 'CMDF'),
(3, 'CIAC');

-- --------------------------------------------------------

--
-- Table structure for table `param_disbursement_status`
--

CREATE TABLE `param_disbursement_status` (
  `disbursement_status_id` int(10) UNSIGNED NOT NULL,
  `disbursement_status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `param_disbursement_status`
--

INSERT INTO `param_disbursement_status` (`disbursement_status_id`, `disbursement_status`) VALUES
(21, 'VOIDED'),
(22, 'APPROVED'),
(23, 'OVERRIDEN'),
(30, 'OVERRIDEN'),
(32, 'DISAPPROVED'),
(33, 'OVERRIDEN'),
(34, 'OVERRIDEN');

-- --------------------------------------------------------

--
-- Table structure for table `param_obligation_status`
--

CREATE TABLE `param_obligation_status` (
  `obligation_status_id` int(10) UNSIGNED NOT NULL,
  `obligation_status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `param_obligation_status`
--

INSERT INTO `param_obligation_status` (`obligation_status_id`, `obligation_status`) VALUES
(104, 'OVERRIDEN'),
(105, 'OVERRIDEN'),
(106, 'OVERRIDEN'),
(114, 'CERTIFIED');

-- --------------------------------------------------------

--
-- Table structure for table `param_offices`
--

CREATE TABLE `param_offices` (
  `office_num` int(10) UNSIGNED NOT NULL,
  `parent_office_num` int(10) UNSIGNED DEFAULT NULL,
  `office_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `param_offices`
--

INSERT INTO `param_offices` (`office_num`, `parent_office_num`, `office_name`) VALUES
(178, NULL, 'Office of the president'),
(179, NULL, 'Office of senate representatives');

-- --------------------------------------------------------

--
-- Table structure for table `param_payees`
--

CREATE TABLE `param_payees` (
  `payee_id` int(10) UNSIGNED NOT NULL,
  `payee_name` varchar(255) NOT NULL,
  `payee_address` text,
  `payee_tin` varchar(100) DEFAULT NULL,
  `vatable_flag` tinyint(1) NOT NULL DEFAULT '1',
  `active_flag` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `param_payees`
--

INSERT INTO `param_payees` (`payee_id`, `payee_name`, `payee_address`, `payee_tin`, `vatable_flag`, `active_flag`) VALUES
(1, 'Metro Manila Development Authority', 'Metro Manila Development Authority', '9HGTTH09', 0, 1),
(19, 'Ninoy Aquino International Airport', 'NAIA 2,3 and 4', '98UYGHK09', 1, 1),
(21, 'Philippine National Convention Center', 'Philippine National Convention Center', '9uYHGGK', 1, 1),
(26, 'Manpower Services Inc.', 'Manpower Services Incorporation', '59777YHG', 1, 1),
(27, 'Manila International Museum', 'Manila City', 'yHGTJH', 0, 1),
(28, 'Super Market Production', 'Henry Sy Businesses', '8uuYH', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `param_proceeding_stages`
--

CREATE TABLE `param_proceeding_stages` (
  `proceeding_stage_id` int(10) UNSIGNED NOT NULL,
  `proceeding_stage` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `param_proceeding_stages`
--

INSERT INTO `param_proceeding_stages` (`proceeding_stage_id`, `proceeding_stage`) VALUES
(1, 'stage 1'),
(2, 'stage 2');

-- --------------------------------------------------------

--
-- Table structure for table `param_proceeding_stage_activities`
--

CREATE TABLE `param_proceeding_stage_activities` (
  `proceeding_stage_activity_id` int(10) UNSIGNED NOT NULL,
  `proceeding_stage_id` int(10) UNSIGNED NOT NULL,
  `stage_activity` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `param_source_types`
--

CREATE TABLE `param_source_types` (
  `source_type_num` int(10) UNSIGNED NOT NULL,
  `source_type_name` varchar(100) NOT NULL,
  `active_flag` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `param_source_types`
--

INSERT INTO `param_source_types` (`source_type_num`, `source_type_name`, `active_flag`) VALUES
(1, 'Current Appropriation', 1),
(2, 'Continuing Appropriation', 1),
(3, 'Special Appropriation', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `arbitrators`
--
ALTER TABLE `arbitrators`
  ADD PRIMARY KEY (`arbitrator_id`);

--
-- Indexes for table `billings`
--
ALTER TABLE `billings`
  ADD PRIMARY KEY (`billing_id`),
  ADD KEY `fk_billings_obligations1_idx` (`obligation_id`);

--
-- Indexes for table `cases`
--
ALTER TABLE `cases`
  ADD PRIMARY KEY (`case_id`);

--
-- Indexes for table `case_arbitrator_deductions`
--
ALTER TABLE `case_arbitrator_deductions`
  ADD PRIMARY KEY (`arbitrator_payment_id`,`arbitrator_id`,`deduction_type_id`);

--
-- Indexes for table `case_arbitrator_fee_shares`
--
ALTER TABLE `case_arbitrator_fee_shares`
  ADD PRIMARY KEY (`arbitrator_payment_id`,`arbitrator_id`,`stage_activity_id`);

--
-- Indexes for table `case_arbitrator_payments`
--
ALTER TABLE `case_arbitrator_payments`
  ADD PRIMARY KEY (`arbitrator_payment_id`);

--
-- Indexes for table `checks`
--
ALTER TABLE `checks`
  ADD PRIMARY KEY (`check_id`),
  ADD KEY `fk_checks_disbursements1_idx` (`disbursement_id`),
  ADD KEY `fk_checks_param_check_status1_idx` (`check_status_id`);

--
-- Indexes for table `disbursements`
--
ALTER TABLE `disbursements`
  ADD PRIMARY KEY (`disbursement_id`),
  ADD KEY `fk_disbursements_obligations1_idx` (`obligation_id`),
  ADD KEY `fk_disbursements_billings1_idx` (`billing_id`),
  ADD KEY `fk_disbursements_param_disbursement_status1_idx` (`disbursement_status_id`),
  ADD KEY `fk_disbursements_param_disbursement_funds1_idx` (`disbursement_fund_id`),
  ADD KEY `fk_disbursements_param_payees1_idx` (`payee_id`),
  ADD KEY `fk_disbursements_param_proceeding_stages1_idx` (`proceeding_stage_id`),
  ADD KEY `fk_disbursements_arbitrators1_idx` (`arbitrator_id`);

--
-- Indexes for table `disbursement_charges`
--
ALTER TABLE `disbursement_charges`
  ADD KEY `fk_disbursement_charges_disbursements1_idx` (`disbursement_id`),
  ADD KEY `fk_disbursement_charges_line_item_accounts1_idx` (`line_item_id`,`account_code`);

--
-- Indexes for table `disbursement_deductions`
--
ALTER TABLE `disbursement_deductions`
  ADD PRIMARY KEY (`deduction_id`),
  ADD KEY `fk_disbursement_deductions_disbursements1_idx` (`disbursement_id`);

--
-- Indexes for table `fund_sources`
--
ALTER TABLE `fund_sources`
  ADD PRIMARY KEY (`year`,`source_type_num`),
  ADD KEY `fk_fund_sources_param_source_types1_idx` (`source_type_num`);

--
-- Indexes for table `line_items`
--
ALTER TABLE `line_items`
  ADD PRIMARY KEY (`line_item_id`),
  ADD KEY `fk_line_items_line_items1_idx` (`parent_line_item_id`),
  ADD KEY `fk_line_items_fund_sources1_idx` (`year`,`source_type_num`);

--
-- Indexes for table `line_item_accounts`
--
ALTER TABLE `line_item_accounts`
  ADD PRIMARY KEY (`line_item_id`,`account_code`),
  ADD KEY `fk_line_item_accounts_line_items1_idx` (`line_item_id`),
  ADD KEY `fk_line_item_accounts_param_account_codes1_idx` (`account_code`);

--
-- Indexes for table `line_item_offices`
--
ALTER TABLE `line_item_offices`
  ADD PRIMARY KEY (`line_item_id`,`office_num`),
  ADD KEY `fk_line_item_offices_param_offices1_idx` (`office_num`),
  ADD KEY `fk_line_item_offices_line_items1_idx` (`line_item_id`);

--
-- Indexes for table `line_item_realigns`
--
ALTER TABLE `line_item_realigns`
  ADD PRIMARY KEY (`realign_id`),
  ADD KEY `fk_line_item_realigns_fund_sources1_idx` (`year`,`source_type_num`);

--
-- Indexes for table `line_item_realign_details`
--
ALTER TABLE `line_item_realign_details`
  ADD KEY `fk_line_item_realign_details_line_item_realigns1_idx` (`realign_id`),
  ADD KEY `fk_line_item_realign_details_line_item_accounts1_idx` (`source_line_item_id`,`source_account_code`),
  ADD KEY `fk_line_item_realign_details_line_item_accounts2_idx` (`recipient_line_item_id`,`recipient_account_code`);

--
-- Indexes for table `line_item_releases`
--
ALTER TABLE `line_item_releases`
  ADD PRIMARY KEY (`release_id`),
  ADD KEY `fk_line_item_releases_line_items1_idx` (`line_item_id`);

--
-- Indexes for table `line_item_release_details`
--
ALTER TABLE `line_item_release_details`
  ADD PRIMARY KEY (`release_id`,`account_code`),
  ADD KEY `fk_line_item_release_details_line_item_releases1_idx` (`release_id`),
  ADD KEY `fk_line_item_release_details_param_account_codes1_idx` (`account_code`);

--
-- Indexes for table `obligations`
--
ALTER TABLE `obligations`
  ADD PRIMARY KEY (`obligation_id`),
  ADD KEY `fk_obligations_fund_sources1_idx` (`year`,`source_type_num`),
  ADD KEY `fk_obligations_param_obligation_status1_idx` (`obligation_status_id`),
  ADD KEY `fk_obligations_param_payees1_idx` (`payee_id`);

--
-- Indexes for table `obligation_charges`
--
ALTER TABLE `obligation_charges`
  ADD PRIMARY KEY (`obligation_id`,`line_item_id`,`account_code`,`office_num`),
  ADD KEY `fk_obligation_charges_obligations1_idx` (`obligation_id`),
  ADD KEY `fk_obligation_charges_line_item_accounts1_idx` (`line_item_id`,`account_code`);

--
-- Indexes for table `obligation_signatories`
--
ALTER TABLE `obligation_signatories`
  ADD PRIMARY KEY (`signatory_id`),
  ADD KEY `fk_obligation_signatories_obligations1_idx` (`obligation_id`);

--
-- Indexes for table `param_account_codes`
--
ALTER TABLE `param_account_codes`
  ADD PRIMARY KEY (`account_code`),
  ADD UNIQUE KEY `account_code` (`account_code`),
  ADD KEY `fk_param_account_codes_param_account_codes1_idx` (`parent_account_code`);

--
-- Indexes for table `param_check_mode_payments`
--
ALTER TABLE `param_check_mode_payments`
  ADD PRIMARY KEY (`mode_payment_id`);

--
-- Indexes for table `param_check_status`
--
ALTER TABLE `param_check_status`
  ADD PRIMARY KEY (`check_status_id`);

--
-- Indexes for table `param_deduction_types`
--
ALTER TABLE `param_deduction_types`
  ADD PRIMARY KEY (`deduction_id`);

--
-- Indexes for table `param_disbursement_funds`
--
ALTER TABLE `param_disbursement_funds`
  ADD PRIMARY KEY (`disbursement_fund_id`);

--
-- Indexes for table `param_disbursement_status`
--
ALTER TABLE `param_disbursement_status`
  ADD PRIMARY KEY (`disbursement_status_id`);

--
-- Indexes for table `param_obligation_status`
--
ALTER TABLE `param_obligation_status`
  ADD PRIMARY KEY (`obligation_status_id`);

--
-- Indexes for table `param_offices`
--
ALTER TABLE `param_offices`
  ADD PRIMARY KEY (`office_num`),
  ADD KEY `fk_param_offices_param_offices1_idx` (`parent_office_num`);

--
-- Indexes for table `param_payees`
--
ALTER TABLE `param_payees`
  ADD PRIMARY KEY (`payee_id`);

--
-- Indexes for table `param_proceeding_stages`
--
ALTER TABLE `param_proceeding_stages`
  ADD PRIMARY KEY (`proceeding_stage_id`);

--
-- Indexes for table `param_proceeding_stage_activities`
--
ALTER TABLE `param_proceeding_stage_activities`
  ADD PRIMARY KEY (`proceeding_stage_activity_id`);

--
-- Indexes for table `param_source_types`
--
ALTER TABLE `param_source_types`
  ADD PRIMARY KEY (`source_type_num`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `arbitrators`
--
ALTER TABLE `arbitrators`
  MODIFY `arbitrator_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `billings`
--
ALTER TABLE `billings`
  MODIFY `billing_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `cases`
--
ALTER TABLE `cases`
  MODIFY `case_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `case_arbitrator_payments`
--
ALTER TABLE `case_arbitrator_payments`
  MODIFY `arbitrator_payment_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `checks`
--
ALTER TABLE `checks`
  MODIFY `check_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `disbursements`
--
ALTER TABLE `disbursements`
  MODIFY `disbursement_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `disbursement_deductions`
--
ALTER TABLE `disbursement_deductions`
  MODIFY `deduction_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `line_items`
--
ALTER TABLE `line_items`
  MODIFY `line_item_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'Primary key', AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `line_item_realigns`
--
ALTER TABLE `line_item_realigns`
  MODIFY `realign_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `line_item_releases`
--
ALTER TABLE `line_item_releases`
  MODIFY `release_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `obligations`
--
ALTER TABLE `obligations`
  MODIFY `obligation_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `obligation_signatories`
--
ALTER TABLE `obligation_signatories`
  MODIFY `signatory_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `param_check_mode_payments`
--
ALTER TABLE `param_check_mode_payments`
  MODIFY `mode_payment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `param_check_status`
--
ALTER TABLE `param_check_status`
  MODIFY `check_status_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `param_deduction_types`
--
ALTER TABLE `param_deduction_types`
  MODIFY `deduction_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `param_disbursement_funds`
--
ALTER TABLE `param_disbursement_funds`
  MODIFY `disbursement_fund_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `param_disbursement_status`
--
ALTER TABLE `param_disbursement_status`
  MODIFY `disbursement_status_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `param_obligation_status`
--
ALTER TABLE `param_obligation_status`
  MODIFY `obligation_status_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT for table `param_offices`
--
ALTER TABLE `param_offices`
  MODIFY `office_num` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=180;
--
-- AUTO_INCREMENT for table `param_payees`
--
ALTER TABLE `param_payees`
  MODIFY `payee_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `param_proceeding_stages`
--
ALTER TABLE `param_proceeding_stages`
  MODIFY `proceeding_stage_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `param_proceeding_stage_activities`
--
ALTER TABLE `param_proceeding_stage_activities`
  MODIFY `proceeding_stage_activity_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `param_source_types`
--
ALTER TABLE `param_source_types`
  MODIFY `source_type_num` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `billings`
--
ALTER TABLE `billings`
  ADD CONSTRAINT `fk_billings_obligations1` FOREIGN KEY (`obligation_id`) REFERENCES `obligations` (`obligation_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `checks`
--
ALTER TABLE `checks`
  ADD CONSTRAINT `fk_checks_disbursements1` FOREIGN KEY (`disbursement_id`) REFERENCES `disbursements` (`disbursement_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_checks_param_check_status1` FOREIGN KEY (`check_status_id`) REFERENCES `param_check_status` (`check_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `disbursements`
--
ALTER TABLE `disbursements`
  ADD CONSTRAINT `fk_disbursements_arbitrators1` FOREIGN KEY (`arbitrator_id`) REFERENCES `arbitrators` (`arbitrator_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_disbursements_billings1` FOREIGN KEY (`billing_id`) REFERENCES `billings` (`billing_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_disbursements_obligations1` FOREIGN KEY (`obligation_id`) REFERENCES `obligations` (`obligation_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_disbursements_param_disbursement_funds1` FOREIGN KEY (`disbursement_fund_id`) REFERENCES `param_disbursement_funds` (`disbursement_fund_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_disbursements_param_disbursement_status1` FOREIGN KEY (`disbursement_status_id`) REFERENCES `param_disbursement_status` (`disbursement_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_disbursements_param_payees1` FOREIGN KEY (`payee_id`) REFERENCES `param_payees` (`payee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_disbursements_param_proceeding_stages1` FOREIGN KEY (`proceeding_stage_id`) REFERENCES `param_proceeding_stages` (`proceeding_stage_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `disbursement_charges`
--
ALTER TABLE `disbursement_charges`
  ADD CONSTRAINT `fk_disbursement_charges_disbursements1` FOREIGN KEY (`disbursement_id`) REFERENCES `disbursements` (`disbursement_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_disbursement_charges_line_item_accounts1` FOREIGN KEY (`line_item_id`,`account_code`) REFERENCES `line_item_accounts` (`line_item_id`, `account_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `disbursement_deductions`
--
ALTER TABLE `disbursement_deductions`
  ADD CONSTRAINT `fk_disbursement_deductions_disbursements1` FOREIGN KEY (`disbursement_id`) REFERENCES `disbursements` (`disbursement_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `fund_sources`
--
ALTER TABLE `fund_sources`
  ADD CONSTRAINT `fk_fund_sources_param_source_types1` FOREIGN KEY (`source_type_num`) REFERENCES `param_source_types` (`source_type_num`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `line_items`
--
ALTER TABLE `line_items`
  ADD CONSTRAINT `fk_line_items_fund_sources1` FOREIGN KEY (`year`,`source_type_num`) REFERENCES `fund_sources` (`year`, `source_type_num`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_line_items_line_items1` FOREIGN KEY (`parent_line_item_id`) REFERENCES `line_items` (`line_item_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `line_item_accounts`
--
ALTER TABLE `line_item_accounts`
  ADD CONSTRAINT `fk_line_item_accounts_line_items1` FOREIGN KEY (`line_item_id`) REFERENCES `line_items` (`line_item_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_line_item_accounts_param_account_codes1` FOREIGN KEY (`account_code`) REFERENCES `param_account_codes` (`account_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `line_item_offices`
--
ALTER TABLE `line_item_offices`
  ADD CONSTRAINT `fk_line_item_offices_line_items1` FOREIGN KEY (`line_item_id`) REFERENCES `line_items` (`line_item_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_line_item_offices_param_offices1` FOREIGN KEY (`office_num`) REFERENCES `param_offices` (`office_num`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `line_item_realigns`
--
ALTER TABLE `line_item_realigns`
  ADD CONSTRAINT `fk_line_item_realigns_fund_sources1` FOREIGN KEY (`year`,`source_type_num`) REFERENCES `fund_sources` (`year`, `source_type_num`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `line_item_realign_details`
--
ALTER TABLE `line_item_realign_details`
  ADD CONSTRAINT `fk_line_item_realign_details_line_item_accounts1` FOREIGN KEY (`source_line_item_id`,`source_account_code`) REFERENCES `line_item_accounts` (`line_item_id`, `account_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_line_item_realign_details_line_item_accounts2` FOREIGN KEY (`recipient_line_item_id`,`recipient_account_code`) REFERENCES `line_item_accounts` (`line_item_id`, `account_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_line_item_realign_details_line_item_realigns1` FOREIGN KEY (`realign_id`) REFERENCES `line_item_realigns` (`realign_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `line_item_releases`
--
ALTER TABLE `line_item_releases`
  ADD CONSTRAINT `fk_line_item_releases_line_items1` FOREIGN KEY (`line_item_id`) REFERENCES `line_items` (`line_item_id`) ON UPDATE NO ACTION;

--
-- Constraints for table `line_item_release_details`
--
ALTER TABLE `line_item_release_details`
  ADD CONSTRAINT `fk_line_item_release_details_line_item_releases1` FOREIGN KEY (`release_id`) REFERENCES `line_item_releases` (`release_id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_line_item_release_details_param_account_codes1` FOREIGN KEY (`account_code`) REFERENCES `param_account_codes` (`account_code`) ON UPDATE NO ACTION;

--
-- Constraints for table `obligations`
--
ALTER TABLE `obligations`
  ADD CONSTRAINT `fk_obligations_fund_sources1` FOREIGN KEY (`year`,`source_type_num`) REFERENCES `fund_sources` (`year`, `source_type_num`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_obligations_param_obligation_status1` FOREIGN KEY (`obligation_status_id`) REFERENCES `param_obligation_status` (`obligation_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_obligations_param_payees1` FOREIGN KEY (`payee_id`) REFERENCES `param_payees` (`payee_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `obligation_charges`
--
ALTER TABLE `obligation_charges`
  ADD CONSTRAINT `fk_obligation_charges_line_item_accounts1` FOREIGN KEY (`line_item_id`,`account_code`) REFERENCES `line_item_accounts` (`line_item_id`, `account_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_obligation_charges_obligations1` FOREIGN KEY (`obligation_id`) REFERENCES `obligations` (`obligation_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `obligation_signatories`
--
ALTER TABLE `obligation_signatories`
  ADD CONSTRAINT `fk_obligation_signatories_obligations1` FOREIGN KEY (`obligation_id`) REFERENCES `obligations` (`obligation_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `param_account_codes`
--
ALTER TABLE `param_account_codes`
  ADD CONSTRAINT `fk_param_account_codes_param_account_codes1` FOREIGN KEY (`parent_account_code`) REFERENCES `param_account_codes` (`account_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `param_offices`
--
ALTER TABLE `param_offices`
  ADD CONSTRAINT `fk_param_offices_param_offices1` FOREIGN KEY (`parent_office_num`) REFERENCES `param_offices` (`office_num`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
