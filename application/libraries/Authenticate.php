<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authenticate {
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$exemption = array("css", "common", "sign_up", "forgot_password","account_cron");
		$roles 	= array(
			'CEIS-ADMINISTRATOR',
			'ISCA-ADMINISTRATOR',
			'BAS-ADMINISTRATOR'
		);
		$this->CI->session->set_userdata('user_roles', $roles);		
		$this->CI->session->set_userdata( 'default_application_code',  strtoupper( PROJECT_CEIS ) );
		
		$this->CI->session->set_userdata(['user_id'=>'1']);				
// 		if(!in_array($this->CI->router->fetch_class(), $exemption))
// 			$this->check_user();
	}
	
	public function check_user()
	{				
		// CHECK IF SESSION EXISTS
		$authenticated = ($this->CI->session->has_userdata('user_id') === TRUE)? 1 : 0;
				
		if($this->CI->router->fetch_class() != "auth")
		{				
			if(!$authenticated)			
				header('location: '.base_url());
		}
		else
		{	
			if($authenticated AND $this->CI->router->fetch_method() != "sign_out")
				header('location: '.base_url().HOME_PAGE);
		}	

	}	
	
	
	public function sign_in($username, $password, $salted , $verify_pass_only = FALSE)
	{
		try 
		{
			$flag = 0;
			
			$user_info = $this->CI->auth_model->get_active_user($username);
			
			$allowed_val = array(BLOCKED, ACTIVE, EXPIRED);
			
			if(EMPTY($user_info) && !in_array($user_info['status_id'], $allowed_val))
				throw new Exception($this->CI->lang->line('invalid_login'));
			
			if($user_info['status_id'] == BLOCKED)
				throw new Exception($this->CI->lang->line('account_blocked'));
			
			if($user_info['status_id'] == EXPIRED)
				throw new Exception($this->CI->lang->line('account_expired'));
			
			// ENCRYPT THE PASSWORD 
			$password = ($salted)? $password : in_salt($password, $user_info["salt"], TRUE);
			if($password != $user_info['password'])
			{
				$this->CI->auth_model->update_attempts($user_info["user_id"], $user_info["attempts"]);
				$e_message = ($verify_pass_only) ? 'Incorrect Password.' : $this->CI->lang->line('invalid_login');
				throw new Exception($e_message);
			}
			if($verify_pass_only === TRUE) return TRUE;
			
			if($user_info['status_id'] == PENDING)
				throw new Exception($this->CI->lang->line('pending_account'));

			// GET AND CHECK USER ROLES	
			$user_roles	= $this->CI->auth_model->get_user_roles($user_info["user_id"], $user_info["attemps"]);
			if(EMPTY($user_roles))
				throw new Exception($this->CI->lang->line('contact_admin'));
				
			// SET THE USER INFO IN SESSION VARIABLES
			$arr = array(
				"user_id" => $user_info["user_id"],	
				"username" => $user_info["username"],
				"photo" => $user_info["photo"],
				"name" => $user_info["name"],
				"job_title" => $user_info["job_title"],
				"location_code" => $user_info["location_code"],
				"org_code" => $user_info["org_code"]
			);			
			$this->CI->session->set_userdata($arr);
			
			// SET USER ROLES IN SESSION VARIABLES
			$roles = array();
			foreach($user_roles as $role):
				$roles[] = $role['role_code'];
			endforeach;
				 			
			$this->CI->session->set_userdata('user_roles', $roles);

			// CHECK IF SESSION EXISTS
			if($this->CI->session->has_userdata('user_id') === FALSE)
				throw new Exception($this->CI->lang->line('system_error'));
							
		}
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}	
	}
	
	
	public function sign_out()
	{
		try {
			// DESTROY ALL SESSIONS
			$this->CI->session->sess_destroy();
			
			// CHECK IF SESSION_ID WAS DESTROYED		
			if($this->CI->session->has_userdata('user_id') === FALSE)
				throw new Exception($this->CI->lang->line('system_error'));									
		}
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}
		
	}
	
}