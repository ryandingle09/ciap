<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class File {
	public function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->helper('download');
	}
		
	public function download_file($filepath, $filename)
	{
		if(is_file($filepath)):
			$contents   = file_get_contents($filepath);

			force_download($filename, $contents);
		else:
			throw new Exception('Not a valid file!');
		endif;
	}


	public function delete_file($path)
	{
		if(is_dir($path))
		{
			$files = array_diff(scandir($path), array('..', '.'));
			foreach($files as $key => $filename):
				if( ! unlink($path.'/'.$filename))
					throw new Exception(sprintf($this->CI->lang->line('err_delete_file'), $filename));
			endforeach;

			rmdir($path);
		}	
		else
		{
			if( ! unlink($path)):
				throw new Exception(sprintf($this->CI->lang->line('err_delete_file'), $path));	
			endif;
		}	
	}
	
	

}
/* End of file */