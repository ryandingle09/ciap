<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sync_upload
{
	private $_remote_base_url;
	private $_local_base_url;
	private $_local_site_url;
	private $_local_base_dir;
	
	public function __construct()
	{
		$CI =& get_instance();
		$CI->load->helper('url');
		$CI->config->load('sync_upload');

		$this->_remote_base_url = $CI->config->item('sync_upload_remote_base_url');
		$this->_local_base_url = base_url($CI->config->item('sync_upload_local_base_url_path')) . '/';
		$this->_local_site_url = site_url($CI->config->item('sync_upload_local_site_url_path')) . '/';
		$this->_local_base_dir = $CI->config->item('sync_upload_local_dir_path');
	}
	
	public function generate_url($path)
	{
		return $this->_local_site_url . preg_replace('/(\%2F|\%5C)/', '/', urlencode($path));
	}
	
	public function produce_file($path, $redirect = TRUE)
	{
		$fs_path = strtr($path, '/', DS);

		$remote_file = $this->_remote_base_url . $fs_path;

		$local_file = $this->_local_base_dir . $fs_path;

		if (!$this->_is_local_file($local_file))
		{
			$this->_download_remote_file($remote_file, $local_file);
		}
		
		if ($this->_is_local_file($local_file))
		{
			if($redirect) {

				redirect($this->_local_base_url . $path);

			}
			
		}
	}
	
	private function _is_local_file($file)
	{

		return (is_readable($file) && is_file($file));
	}
	
	private function _download_remote_file($remote, $local)
	{

		$remote = str_replace('\\', '/', $remote);

		$remote_file = fopen($remote, 'rb');

		if ($remote_file)
		{

			$directory = dirname($local);
			
			if (!empty($directory) && !file_exists($directory) && !mkdir($directory, 0700, TRUE)) return;
			
			$local_file = fopen($local, 'wb');
	
			if ($local_file)
			{	
				while(!feof($remote_file))
				{

					fwrite($local_file, fread($remote_file, 1024 * 8 ), 1024 * 8);
				}
				
				fclose($local_file);
			}
			
			fclose($remote_file);
		}
	}

}