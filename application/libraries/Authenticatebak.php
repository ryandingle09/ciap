<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authenticate {
	
	public function __construct()
	{
		$this->CI =& get_instance();
		$exemption = array("css", "common", "sign_up", "forgot_password","account_cron");
		/* 
			$roles = array(
				'CEIS-ADMINISTRATOR',
				'ISCA-ADMINISTRATOR',
				'BAS-ADMINISTRATOR'
			);
			$this->CI->session->set_userdata('user_roles', $roles);		
			$this->CI->session->set_userdata( 'default_application_code',  strtoupper( PROJECT_CEIS ) );
		 */
		if(!in_array($this->CI->router->fetch_class(), $exemption))
				$this->check_user();
	}
	
	public function check_user()
	{				
		// CHECK IF SESSION_ID WAS STORED IN CI_SESSIONS TABLE
		$session_id				  = $this->CI->session->userdata('session_id');
		$session_employee_no	  = $this->CI->session->userdata('employee_no');
		$default_application_code = $this->CI->session->userdata('default_application_code');
		
		// CHECK IF SESSION EXISTS
		$authenticated = ($this->CI->session->has_userdata('employee_no') === TRUE && $this->CI->session->has_userdata('session_id') === TRUE)? 1 : 0;
		
		if (substr($this->CI->router->fetch_class(), -4) == '_api'){
			//EXCLUDE *_API
		}
		elseif($this->CI->router->fetch_class() != "auth")
		{			
			$uri = explode('/', uri_string());
			
			if( ! $authenticated)
			{	
				if(COUNT($uri) != 3 || ! ISSET($uri[2])):
					header('location: '.SERVER_ROOT_PATH_CIAS);
				else:
					$raw_url = base64_decode($this->_decode_clean_uri($uri[2]));
					$tokens  = explode('/', $raw_url);
					
					if(check_salt($tokens[0], $tokens[1], $tokens[2], FALSE))
					{
						$employee_no = substr($tokens[0], 0, strrpos($tokens[0], '-'));
	 				    
						//API TO GET SESSION DETAILS THEN CREATE SESSION HERE.
					 	$this->CI->load->library('rest', array('server' => AUTH_CIAP_INTRANET ),'auth_api');
					  
					  	$wsData   = $this->CI->auth_api->post('fetch_session_values', array('employee_no' => $employee_no));
					  	$response = convert_object_to_array($wsData);
					 
					  	if( ! EMPTY($response) && ISSET($response['status']) && $response['status'] == 1):
					  		$response['session']['logged_in']  = TRUE;
					  		$response['session']['session_id'] = session_id();
					  		$response['session']['selected_application'] = $uri[0];
					  		$response['session']['user_roles'] = $response['session']['user_roles']['user_role'];
					  		$this->CI->session->set_userdata($response['session']);
					  		

					  		redirect(base_url().$uri[0].'/'.$uri[1]);
					  	else:
					  		header('location: '.SERVER_ROOT_PATH_CIAS);	
					  	endif; 
					}
					else
					{
						header('location: '.SERVER_ROOT_PATH_CIAS);
					}
				endif;
			}
			else 
			{

				 //CHECK IF USER HAS TOKEN IF YES CURRENTLY LOGGED IN IF NO REDIRECT TO LOGIN PAGE ( MEANING NAKASIGN OUT )
				$this->CI->load->model('api/auth_api_model', 'aam');
			
				$this->CI->aam->set_dsn($uri[0]);
				
				$user_details = $this->CI->aam->select_user($session_employee_no, array('token'));
				
				if( EMPTY($user_details['token']) ){
					
					$this->CI->session->sess_destroy();
					
					redirect(SERVER_ROOT_PATH_CIAS);
					
				}

			}
		}
		else
		{	
			 if($authenticated AND $this->CI->router->fetch_method() != "sign_out") 
			 	header('location: '.SERVER_ROOT_PATH_CIAS); 
		}	
		
		
	}	
	
	
	public function sign_in($username, $password, $salted , $verify_pass_only = FALSE)
	{
		try 
		{
			$flag = 0;
			
			$user_info = $this->CI->auth_model->get_active_user($username);
			
			$allowed_val = array(BLOCKED, ACTIVE, EXPIRED);
			
			if(EMPTY($user_info) && !in_array($user_info['status_id'], $allowed_val))
				throw new Exception($this->CI->lang->line('invalid_login'));
			
			if($user_info['status_id'] == BLOCKED)
				throw new Exception($this->CI->lang->line('account_blocked'));
			
			if($user_info['status_id'] == EXPIRED)
				throw new Exception($this->CI->lang->line('account_expired'));
			
			// ENCRYPT THE PASSWORD 
			$password = ($salted)? $password : in_salt($password, $user_info["salt"], TRUE);
			if($password != $user_info['password'])
			{
				$this->CI->auth_model->update_attempts($user_info["user_id"], $user_info["attempts"]);
				$e_message = ($verify_pass_only) ? 'Incorrect Password.' : $this->CI->lang->line('invalid_login');
				throw new Exception($e_message);
			}
			if($verify_pass_only === TRUE) return TRUE;
			
			if($user_info['status_id'] == PENDING)
				throw new Exception($this->CI->lang->line('pending_account'));

			// GET AND CHECK USER ROLES	
			$user_roles	= $this->CI->auth_model->get_user_roles($user_info["user_id"], $user_info["attemps"]);
			if(EMPTY($user_roles))
				throw new Exception($this->CI->lang->line('contact_admin'));
				
			// SET THE USER INFO IN SESSION VARIABLES
			$arr = array(
				"user_id" => $user_info["user_id"],	
				"username" => $user_info["username"],
				"photo" => $user_info["photo"],
				"name" => $user_info["name"],
				"job_title" => $user_info["job_title"],
				"location_code" => $user_info["location_code"],
				"org_code" => $user_info["org_code"]
			);			
			$this->CI->session->set_userdata($arr);
			
			// SET USER ROLES IN SESSION VARIABLES
			$roles = array();
			foreach($user_roles as $role):
				$roles[] = $role['role_code'];
			endforeach;
				 			
			$this->CI->session->set_userdata('user_roles', $roles);

			// CHECK IF SESSION EXISTS
			if($this->CI->session->has_userdata('user_id') === FALSE)
				throw new Exception($this->CI->lang->line('system_error'));
							
		}
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}	
	}
	
	
	public function sign_out()
	{
		try {
			$app_group = get_avail_app_group();
			
			if(in_array(APP_GROUP_2,$app_group))
			{
				$this->CI->load->library('rest', array('server' => AUTH_CIAP_INTRANET));
				
				$wsData  = $this->CI->rest->post('unset_auth_values', array('cookie' => $this->CI->input->cookie()));
				
				$data    = convert_object_to_array($wsData);
			
				if( ! $data['status']) throw new Exception($data['msg']);
			}
		
			$this->CI->session->sess_destroy();		
		}
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}
		
	}
	
	
	private function _decode_clean_uri($uri)
	{
		try
		{
			return strtr($uri, '.~-', '+/=');
		}catch(Exception $e){
			throw $e;
		}
	}
}