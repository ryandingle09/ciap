<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
define('EXIT_SUCCESS', 0); // no errors
define('EXIT_ERROR', 1); // generic error
define('EXIT_CONFIG', 3); // configuration error
define('EXIT_UNKNOWN_FILE', 4); // file not found
define('EXIT_UNKNOWN_CLASS', 5); // unknown class
define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
define('EXIT_USER_INPUT', 7); // invalid user input
define('EXIT_DATABASE', 8); // database error
define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

/*
|--------------------------------------------------------------------------
| DIRECTORY and PATH
|--------------------------------------------------------------------------
|
| These constants are used when working with directory and file naming
|
*/
define('DS', DIRECTORY_SEPARATOR);
define('DOCUMENT_ROOT', dirname($_SERVER['SCRIPT_FILENAME']).DS);


/*
|--------------------------------------------------------------------------
| GENERAL CONSTANTS
|--------------------------------------------------------------------------
|
| These constants are used by the whole project/product
|
*/
	/*
	|---------------------------------------------------------------------
	| STATIC PATH
	|---------------------------------------------------------------------
	| These constants are used when defining the folder path of your css,
	| js, images and file upload
	*/
	define('PATH_CSS', 'static/css/');
	define('PATH_JS', 'static/js/');
	define('PATH_IMAGES', 'static/images/');
	define('PATH_UPLOADS', 'uploads/');
	define('PATH_USER_UPLOADS', 'uploads/users/');
	define('PATH_SETTINGS_UPLOADS', 'uploads/settings/');
	define('PATH_FILE_UPLOADS', 'uploads/files/');
	define('PATH_JS_MODULES', 'modules/');
	/*
	|---------------------------------------------------------------------
	| CSS AND JS
	|---------------------------------------------------------------------
	| These are the frequently used CSS and JS in 
	| resources(load_css and load_js)
	*/
		/* CHECKBOX / RADIO BUTTON */
		define('CSS_LABELAUTY', 'jquery-labelauty');
		define('JS_LABELAUTY', 'jquery-labelauty');
		
		/* DATATABLE */
		define('CSS_DATATABLE', 'jquery.dataTables');
		define('JS_DATATABLE', 'jquery.dataTables.min');
		define('JS_DATATABLE_RELOAD' ,'jquery.dataTables.fnReloadAjax');
		
		/* DATE PICKER / TIME PICKER */
		define('CSS_DATETIMEPICKER', 'jquery.datetimepicker');
		define('JS_DATETIMEPICKER', 'jquery.datetimepicker');
			
		/* DROPDOWN / SELECT FIELD */
		define('CSS_SELECTIZE', 'selectize.default');
		define('JS_SELECTIZE', 'selectize');
		
		/* MODAL */
		define('CSS_MODAL_COMPONENT', 'component');
		define('JS_MODAL_CLASSIE', 'classie');
		define('JS_MODAL_EFFECTS', 'modalEffects');
		
		/* SCROLL */
		define('CSS_PRETTIFY', 'prettify');
		define('JS_PRETTIFY', 'prettify');
		define('JS_SLIMSCROLL', 'jquery.slimscroll');
		
		/* TABS */
		define('CSS_TABS', 'easy-responsice-tabs');
		define('JS_TABS', 'easyResponsiveTabs');
		
		/* UPLOAD */
		define('CSS_UPLOAD', 'uploadfile');
		define('JS_UPLOAD', 'jquery.uploadfile');
	
		define("JS_EDITOR", 'ckeditor/ckeditor');
		
		/* LIST*/
		define("JS_LIST", 				'modules/ceis/list/list');
		define("JS_LIST_PAGINATE", 		'modules/ceis/list/list.pagination');
		define("JS_LIST_CALL_PAGINATE", 	'modules/ceis/list/call.pagination');
		
		define("CSS_LIST", 	'modules/ceis/list/list');
		
	/*
	|---------------------------------------------------------------------
	| LENGTH OF SALT
	|---------------------------------------------------------------------
	| Control the length of salt used for security purposes
	*/
	define('SALT_LENGTH', 15);

	/*
	|---------------------------------------------------------------------
	| SYSTEM STATUS
	|---------------------------------------------------------------------
	*/
	define('SYSTEM_ON', 1);
	
	/*
	|---------------------------------------------------------------------
	| PERMISSION ACTIONS
	|---------------------------------------------------------------------
	| These constants are used when defining the action to be made in 
	| permission
	*/ 
	define('ACTION_SAVE', 1);
	define('ACTION_ADD', 2);
	define('ACTION_EDIT', 3);
	define('ACTION_DELETE', 4);
	define('ACTION_VIEW', 5);
	define('ACTION_PRINT', 6);
	define('ACTION_LOCK', 7);
	define('ACTION_UPLOAD', 8);
	define('ACTION_OVERRIDE', 2);
	define('ACTION_VOID', 2);
	define('ACTION_EXCESS', 2);
	define('ACTION_CERTIFY', 2);
	
	/*
	 |---------------------------------------------------------------------
	 | QUERY STATUSES
	 |---------------------------------------------------------------------
	 | These constants are used when defining the status to be made in
	 | queries or in any action on form and condition in your models
	 */
	define('STATUS_OVERRIDEN', 'OVERRIDEN');
	define('STATUS_VOIDED', 'VOIDED');
	define('STATUS_EXCESS', 'RETURNED EXCESS');
	define('STATUS_POSTED', 'POSTED');
	define('STATUS_APPROVED', 'APPROVED');
	define('STATUS_SUBMITTED', 'SUBMITTED');
	define('STATUS_CERTIFIED', 'CERTIFIED');
	define('STATUS_DECLINED', 'DECLINED');
	define('STATUS_DISAPPROVED', 'DISAPPROVED');
	define('STATUS_DENIED', 'DENIED');
	define('STATUS_DRAFTED', 'DRAFTED');
	define('STATUS_ACTIVE', 'ACTIVE');
	define('STATUS_NOT_ACTIVE', 'NOT ACTIVE');
	/*
	
	
	/*
	|---------------------------------------------------------------------
	| BUTTON ACTIONS
	|---------------------------------------------------------------------
	| These constants are used as button labels
	*/
	define('BTN_LOG_IN', 'Log In');
	define('BTN_SIGN_UP', 'Sign Up');
	define('BTN_CREATE_ACCOUNT', 'Create Account');
	define('BTN_SAVE', 'Save');
	define('BTN_ADD', 'Add');
	define('BTN_UPDATE', 'Update');
	define('BTN_DELETE', 'Delete');
	define('BTN_CANCEL', 'Cancel');
	define('BTN_CLOSE', 'Close');
	define('BTN_POST', 'Post');
	//added
	define('BTN_OVERRIDE', 'Override');
	define('BTN_VOID', 'Void');
	define('BTN_PRINT', 'Print');
	define('BTN_GENERATE', 'Generate');
	define('BTN_APPROVED', 'Approve');
	define('BTN_DISAPRROVED', 'Disapprove');
	define('BTN_DRAFT', 'Save as Draft');
	define('BTN_SUBMIT', 'Submit');
	define('BTN_DECLINE', 'decline');
	define('BTN_CERTIFY', 'certify');
	define('BTN_EXCESS', 'Return Excess Amount');

	/*
	|---------------------------------------------------------------------
	| BUTTON VERBS
	|---------------------------------------------------------------------
	| These constants are used for replacing the loading text in the 
	| button after pressing it
	*/
	define('BTN_SIGNING_UP', 'Signing up');
	define('BTN_CREATING_ACCOUNT', 'Creating Account');
	define('BTN_LOGGING_IN', 'Logging in');
	define('BTN_EMAILING', 'Sending email');
	define('BTN_POSTING', 'Posting');
	define('BTN_SAVING', 'Saving');
	define('BTN_UPDATING', 'Updating');
	define('BTN_DELETING', 'Deleting');
	define('BTN_UPLOADING', 'Uploading');

	/*
	|---------------------------------------------------------------------
	| SEARCH USER USING THE FOLLOWING PARAMETERS
	|---------------------------------------------------------------------
	| These constants are used as a parameter for searching a specific 
	| user in get_active_user() function
	*/
	define('BY_USERNAME', 'username');
	define('BY_EMAIL', 'email');
	define('BY_RESET_SALT', 'reset_salt');

	/* 
	|---------------------------------------------------------------------
	| USER STATUS 
	|---------------------------------------------------------------------
	| These status are used for identifying the status of the user
	*/
	define('ACTIVE', '1');
	define('PENDING', '2');
	define('INACTIVE', '3');
	define('APPROVED', '4');
	define('DISAPPROVED', '5');
	define('DELETED', '6');
	define('BLOCKED', '7');
	define('DRAFT', '8');
	define('EXPIRED', '9');

	/*
	|---------------------------------------------------------------------
	| ALERT TYPE
	|---------------------------------------------------------------------
	| These types are used for identifying the notification class 
	| used by notifyModal
	*/
	define('SUCCESS', 'success');
	define('ERROR', 'error');

	/*
	|---------------------------------------------------------------------	
	| AUDIT TRAIL ACTIONS
	|---------------------------------------------------------------------
	| These constants are used when defining the action made in the process
	*/
	define('AUDIT_INSERT', 'INSERT');
	define('AUDIT_UPDATE', 'UPDATE');
	define('AUDIT_DELETE', 'DELETE');

	/* 
	|---------------------------------------------------------------------
	| GENDER
	|---------------------------------------------------------------------
	*/
	define('FEMALE', 'F');
	define('MALE', 'M');

	/* 
	|---------------------------------------------------------------------
	| ANONYMOUS ACCOUNT
	|---------------------------------------------------------------------
	| This anonymous account is used as the default user_id in the audit 
	| trail when a guest manually registers in the system
	*/
	define('ANONYMOUS_ID', 0);
	define('ANONYMOUS_USERNAME', 'anonymous');

	/* 
	|---------------------------------------------------------------------
	| ACCOUNT CREATOR
	|---------------------------------------------------------------------
	*/
	define('ADMINISTRATOR', 'ADMINISTRATOR');
	define('VISITOR', 'VISITOR');
		
	/* 
	|---------------------------------------------------------------------
	| LOGIN VIA
	|---------------------------------------------------------------------
	| These constants are used for identifying which method is used 
	| when logging in the system
	*/
	define('VIA_USERNAME', 'USERNAME');
	define('VIA_EMAIL', 'EMAIL');
	define('VIA_USERNAME_EMAIL', 'USERNAME_EMAIL');

	/*
	|---------------------------------------------------------------------	
	| MODAL EFFECT 
	|---------------------------------------------------------------------
	| Define the default transition when opening the modal window. 
	| See http://tympanus.net/Development/ModalWindowEffects/ for demo
	| 1 - Fade In and Scale 
	| 2 - Slide In (Right) 
	| 3 - Slide In (Bottom)
	| 4 - Newspaper 
	| 5 - Fall 
	| 6 - Side Fall 
	| 7 - Sticky Up
	| 8 - 3D Flip (Horizontal)
	| 9 - 3D Flip (Vertical) 
	| 10 - 3D Sign
	| 11 - Superscaled 
	| 12 - Just Me 
	| 13 - 3D Slit 
	| 14 - 3D Rotate Bottom
	| 15 - 3D Rotate in Left 
	| 16 - Blur 
	| 17 - Let Me In 
	| 18 - Make Way
	| 19 - Slid from Top
	*/
	define('MODAL_EFFECT', 1);

	/* 
	|---------------------------------------------------------------------	
	| TYPE OF EXPORTED FILE
	|---------------------------------------------------------------------
	| These constants are used to indicate the type of file to be exported
	*/
	define('EXPORT_PDF', 'pdf');
	define('EXPORT_EXCEL', 'xls');
	define('EXPORT_DOCUMENT', 'doc');
	
	/* 
	|---------------------------------------------------------------------	
	| TYPE OF SYSTEM PARAMETERS
	|---------------------------------------------------------------------	
	| These parameters are used when working with system emails
	*/
	define('SYS_PARAM_TYPE_SMTP', 'SMTP');
	define('SYS_PARAM_TYPE_SMTP_REPLY_NAME', 'SMTP_REPLY_NAME');
	define('SYS_PARAM_TYPE_SMTP_REPLY_EMAIL', 'SMTP_REPLY_EMAIL');

	/* 
	|---------------------------------------------------------------------	
	| LIMIT OF ITEMS SHOWN AND USED BY JSCROLL
	|---------------------------------------------------------------------
	| Control the default number of items to be shown before loading the 
	| next set of content
	*/
	define('ITEM_LIMIT', 5);


/*
|--------------------------------------------------------------------------
| CHANGE ALL CONSTANTS BELOW DEPENDING ON THE PROJECT
|--------------------------------------------------------------------------
*/
	/*
	|---------------------------------------------------------------------
	| PROJECT NAME
	|---------------------------------------------------------------------
	| Used for encryption_key and sess_cookie_name. Naming of constants  
	| should contain the project or company name and avoid long values
	| with spaces.
	*/
	define('PROJECT_NAME', 'CIAP PHASE 2');
	define('PROJECT_CODE', 'p2');

	/* 
	|---------------------------------------------------------------------
	| PROJECTS
	|---------------------------------------------------------------------
	| Define all project names included in your system.
	*/
	define('SYSAD', 'SYSAD');
	
	define('CEIS', 'CEIS');
	define('BAS', 'BAS');
	define('MPIS', 'MPIS');
	define('PIS', 'PIS');
	define('ASIS', 'ASIS');
	
	/* 
	|---------------------------------------------------------------------
	| PROJECT FOLDER 
	|---------------------------------------------------------------------
	| These constants are used when defining the project folder path of 
	| your controller in the hyperlink or function.
	*/
	define('PROJECT_CORE', 'sysad');
	
	define('PROJECT_CEIS', 'ceis');
	define('PROJECT_BAS', 'bas');
	define('PROJECT_MPIS', 'mpis');
	define('PROJECT_PIS', 'pis');
	define('PROJECT_ASIS', 'asis');
	define('PROJECT_ISCA', 'isca');
	define('PROJECT_PSMS', 'psms');


	/* 
	|---------------------------------------------------------------------
	| PARAM ACCOUNT CODES 
	|---------------------------------------------------------------------
	| These constants are use for identifying param acount codes under line items adding account code
	*/
	define('PARAM_ACCOUNT_CODE_PS', '501');
	define('PARAM_ACCOUNT_CODE_MOOE', '502');
	define('PARAM_ACCOUNT_CODE_CO', '503');



	/* 
	|---------------------------------------------------------------------
	| SYSTEMS DATABASE 
	|---------------------------------------------------------------------
	| Define the name of database/s used in the system.
	*/
	define('DB_CORE', 'cias');
	
	define('DB_CIAS', 'cias');
	define('DB_CEIS', 'ceis');
	define('DB_BAS', 'bas');
	define('DB_MPIS', 'mpis');
	define('DB_PIS', 'pis');
	define('DB_ASIS', 'asis');
	define('DB_ISCA', 'isca');
	define('DB_PSMS', 'psms');
	/*
	 |---------------------------------------------------------------------
	 | APP GROUP
	 |---------------------------------------------------------------------
	 | 
	 */
	define('APP_GROUP_2', 2);
	define('APP_GROUP_3', 3);
	
	/*
	|---------------------------------------------------------------------
	| HOMEPAGE
	|---------------------------------------------------------------------
	| Define the landing page after login.
	| Note that every project can have its own homepage.
	*/
	define('SYSAD_HOME_PAGE', PROJECT_CORE.'/dashboard');

	/* 
	|---------------------------------------------------------------------
	| PROJECT MODULES
	|---------------------------------------------------------------------
	| Declare all the modules in your project.
	*/
	/* update */
	define('MODULE_DASHBOARD', 1);
	define('MODULE_SETTINGS', 2);
	define('MODULE_USER_MANAGEMENT', 3);
	define('MODULE_USER', 4);
	define('MODULE_ROLE', 5);
	define('MODULE_PERMISSION', 6);
	define('MODULE_ORGANIZATION', 7);
	define('MODULE_FILE', 8);
	define('MODULE_WORKFLOW', 9);
	define('MODULE_AUDIT_TRAIL', 10);
	define('MODULE_SITE_SETTINGS', 11);
	define('MODULE_AUTH_SETTINGS', 12);
	define('MODULE_BAS', 11);

	define('MODULE_CEIS_POSITIONS', 'CEIS-POSITIONS');
	define('MODULE_CEIS_OPPORTUNITIES', 'CEIS-OPPORTUNITIES');
	define('MODULE_CEIS_EQUIPMENTS', 'CEIS-EQUIPMENTS');
	define('MODULE_CEIS_REQUESTS', 'CEIS-REQUESTS');
	define('MODULE_CEIS_CONTRACTORS', 'CEIS-CONTRACTORS');
	define('MODULE_CEIS_REPORTS', 'CEIS-REPORTS');
	define('MODULE_CEIS_GEOGRAPHICAL_DISTRIBUTIONS', 'CEIS-GEOGRAPHICAL-DISTRIBUTIONS');
	define('MODULE_CEIS_PROJECT_TYPES', 'CEIS-PROJECT-TYPES');
	define('MODULE_CEIS_CONTRACT_INVOLVEMENTS', 'CEIS-CONTRACT-INVOLVEMENT');
	
	define('MODULE_BAS_BILLINGS', 'BAS-BILLING');
	define('MODULE_BAS_CHART_ACCOUNTS', 'BAS-CHART-ACCOUNTS');
	define('MODULE_BAS_CODE_LIBRARY', 'BAS-CODE-LIBRARY');
	define('MODULE_BAS_DASHBOARD', 'BAS-DASHBOARD');
	define('MODULE_BAS_DISBURSEMENT_VOUCHERS', 'BAS-DISBURSEMENT-VOUCHERS');
	define('MODULE_BAS_LINE_ITEMS', 'BAS-LINE-ITEMS');
	define('MODULE_BAS_OBLIGATION_SLIPS', 'BAS-OBLIGATION-SLIPS');
	define('MODULE_BAS_PAYEES', 'BAS-PAYEES');
	define('MODULE_BAS_REALIGNMENT', 'BAS-REALIGNMENT');
	define('MODULE_BAS_REPORTS', 'BAS-REPORTS');
	define('MODULE_BAS_REPORTS_RAOMOOE', 'BAS-REPORTS-RAOMOOE');
	define('MODULE_BAS_REPORTS_SAAOB', 'BAS-REPORTS-SAAOB');
	define('MODULE_BAS_REPORTS_SAAOBD', 'BAS-REPORTS-SAAOBD');
	define('MODULE_BAS_REPORTS_SAAOBD_EXP', 'BAS-REPORTS-SAAOBD-EXP');
	define('MODULE_BAS_REPORTS_STATUS_FUNDS', 'BAS-REPORTS-STATUS-FUNDS');
	define('MODULE_BAS_TYPE_SOURCE', 'BAS-TYPE-SOURCE');

	define('MODULE_ISCA_POSITIONS', 'ISCA-POSITIONS');
	
	define('MODULE_MPIS_DASHBOARD', 'MPIS-DASHBOARD');
	define('MODULE_MPIS_POLICIES', 'MPIS-POLICIES');
	define('MODULE_MPIS_SOCIOECONOMICS', 'MPIS-SOCIOECONOMICS');
	define('MODULE_MPIS_REPORTS', 'MPIS-REPORTS');
	define('MODULE_MPIS_CODE_LIBRARIES', 'MPIS-CODE-LIBRARIES');
	
	define('MODULE_MPIS_CONS_COUNTRIES', 'MPIS-CONS-COUNTRIES');
	define('MODULE_MPIS_CONS_EMPLOYMENT', 'MPIS-CONS-EMPLOYMENT');
	define('MODULE_MPIS_CONS_RELATED_PROF', 'MPIS-CONS-RELATED-PROF');
	define('MODULE_MPIS_FINANCIAL_RATIOS', 'MPIS-FINANCIAL-RATIOS');
	define('MODULE_MPIS_GNI_GDP_EXPENDITURE', 'MPIS-GNI-GDP-EXPENDITURE');
	define('MODULE_MPIS_GNI_GDP_INUDSTRIAL', 'MPIS-GNI-GDP-INUDSTRIAL');
	define('MODULE_MPIS_GV_GVA', 'MPIS-GV-GVA');
	define('MODULE_MPIS_INDICATORS', 'MPIS-INDICATORS');
	define('MODULE_MPIS_NON_RES_BLDG_CONS', 'MPIS-NON-RES-BLDG-CONS');
	define('MODULE_MPIS_RES_BLDG_CONS', 'MPIS-RES-BLDG-CONS');
	
	define('MODULE_MPIS_CODE_LIBRARIES_BOARD_MEETING', 'MPIS-CODE-LIBRARIES-BOARD-MEETING');
	define('MODULE_MPIS_CODE_LIBRARIES_INDICATOR_SOURCES', 'MPIS-CODE-LIBRARIES-INDICATOR-SOURCES');
	define('MODULE_MPIS_CODE_LIBRARIES_ISSUANCE_CLASSIFICATIONS', 'MPIS-CODE-LIBRARIES-ISSUANCE-CLASSIFICATIONS');
	define('MODULE_MPIS_CODE_LIBRARIES_POLICY_CATEGORIES', 'MPIS-CODE-LIBRARIES-POLICY-CATEGORIES');
	define('MODULE_MPIS_CODE_LIBRARIES_PROFESSIONS', 'MPIS-CODE-LIBRARIES-PROFESSIONS');
	
	define('MODULE_PSMS_DASHBOARD', 'PSMS-DASHBOARD');
	define('MODULE_PSMS_ASSETS', 'PSMS-ASSETS');	
	define('MODULE_PSMS_ASSET_SITE', 'PSMS-ASSET-SITE');
	define('MODULE_PSMS_ASSET_LOCATION', 'PSMS-ASSET-LOCATION');
	define('MODULE_PSMS_ASSET_CLASSIFICATION', 'PSMS-ASSET-CLASSIFICATION');
	define('MODULE_PSMS_ASSET_CATEGORY', 'PSMS-ASSET-CATEGORY');
	define('MODULE_PSMS_SUPPLIES_CATEGORY', 'PSMS-SUPPLIES-CATEGORY');
	define('MODULE_PSMS_SUPPLIES_INFO', 'PSMS-SUPPLIES-INFO');
	define('MODULE_PSMS_RIS', 'PSMS-RIS');
	define('MODULE_PSMS_REPORTS', 'PSMS-REPORTS');
	
	
		
	/*
	|---------------------------------------------------------------------
	| META TAGS
	|---------------------------------------------------------------------
	*/
	define('SITE_DESCRIPTION', 'CIAP Information and Applications Systems');
	define('SITE_KEYWORDS', 'CIAP, CIAS');
	
	/*
	|---------------------------------------------------------------------
	| SITE SETTINGS
	|---------------------------------------------------------------------
	*/
	define('SITE_TITLE', 'CIAP Intranet');
	define('SITE_SKIN', 'orange');
	define('SITE_LAYOUT', '');
	define('SITE_HEADER', 'inverse');
	define('ACCOUNT_CREATOR', 'VISITOR');


/*
 |--------------------------------------------------------------------------
 | CEIS CONSTANTS
 |--------------------------------------------------------------------------
 */
	define("PATH_UPLOADS_REQUESTS", PATH_UPLOADS.'requests/');
	define("UPLOAD_REQUESTS_FOLDER", 'requests/');
	
	define('REQUEST_TYPE_NEW', 1);
	define('REQUEST_TYPE_REN', 2);
	define('REQUEST_TYPE_PC', 3);
	define('REQUEST_TYPE_MSC', 4);
	define('REQUEST_TYPE_EQP', 5);
	
	//CRON Paths
	define("PATH_UPLOADS_CRON_REQUEST", PATH_UPLOADS.'cron_request'.DS);
	define("PATH_UPLOADS_CRON_REQUEST_PROCESSING", PATH_UPLOADS_CRON_REQUEST.'processing'.DS);
	define("PATH_UPLOADS_CRON_REQUEST_UNPROCESSED", PATH_UPLOADS_CRON_REQUEST.'unprocessed'.DS);
	define("PATH_UPLOADS_CRON_REQUEST_PROCESSED", PATH_UPLOADS_CRON_REQUEST.'processed'.DS);
	define("PATH_UPLOADS_CRON_REQUEST_LOGS", PATH_UPLOADS_CRON_REQUEST.'logs'.DS);
	
	
	define("PATH_UPLOADS_PROGRESS_REPORT", PATH_UPLOADS.'cron_progress_report'.DS);
	define("PATH_UPLOADS_CRON_PROGRESS_REPORT_PROCESSING", PATH_UPLOADS_PROGRESS_REPORT.'processing'.DS);
	define("PATH_UPLOADS_CRON_PROGRESS_REPORT_UNPROCESSED", PATH_UPLOADS_PROGRESS_REPORT.'unprocessed'.DS);
	define("PATH_UPLOADS_CRON_PROGRESS_REPORT_PROCESSED", PATH_UPLOADS_PROGRESS_REPORT.'processed'.DS);
	define("PATH_UPLOADS_CRON_PROGRESS_REPORT_LOGS", PATH_UPLOADS_PROGRESS_REPORT.'logs'.DS);
	
	//CRON File Extension
	define("CRON_FILE_EXT",".txt");
	
	//CRON CIAP API Response
	define("API_RESPONSE_PROCESSED", '1');
	define("API_RESPONSE_PROCESSING", '2');
	define("API_RESPONSE_UNPROCESSED", '3');
	
	//Portal Path
	//define('SERVER_DOMAIN_PORTAL', '192.168.192.182');
	define('SERVER_DOMAIN_PORTAL', 'localhost');
	define('SERVER_ROOT_PATH_PORTAL', 'http://' . SERVER_DOMAIN_PORTAL . '/ciap_portal/');
	
	//define('SERVER_DOMAIN_CIAS', '192.168.192.182');
	define('SERVER_DOMAIN_CIAS', 'localhost');
	define('SERVER_ROOT_PATH_CIAS', 'http://' . SERVER_DOMAIN_CIAS . '/ciap_intranet/');
	
	define('AUTH_CIAP_INTRANET', SERVER_ROOT_PATH_CIAS . 'authenticate/auth_api/');
	define('CEIS_API_SERVER', SERVER_ROOT_PATH_PORTAL . 'api/ceis_cron_api/');
	define('CEIS_API_PROGRESS_REPORT_SERVER', SERVER_ROOT_PATH_PORTAL . 'api/ceis_cron_report_api/');
	define('CS_API_SERVER', SERVER_ROOT_PATH_CIAS . 'cs_api/ceis_cs_api/');
	define('PORTAL_API_SERVER', SERVER_ROOT_PATH_PORTAL . 'api/ceis_portal_api/');
	
	define('SERVER_DOMAIN_BAS', 'localhost/p2/bas/');
	define('BAS_API_SERVER', 'http://' . SERVER_DOMAIN_BAS . 'api/');
	
	
	define('PATH_PORTAL_PROCESSING', SERVER_ROOT_PATH_PORTAL.'/uploads/cron_request/');
	
	//CRON CIAP FILE FOLDER LOCATION
	define("FOLDER_RESPONSE_PROCESSED", 'PROCESSED');
	define("FOLDER_RESPONSE_PROCESSING", 'PROCESSING');
	define("FOLDER_RESPONSE_UNPROCESSED", 'UNPROCESSED');
	
	define('CONTRACT_TYPE_PROJECT', 'P');
	define('CONTRACT_TYPE_MANPOWER', 'M');
	
	/*
	 * REQUEST CONTRACT LOCAL
	 */
	define('CONTRACT_LOCAL', 'L');
	define('CONTRACT_OVERSEAS', 'O');
	
	/*
	 * REQUEST CONTRACT STATUS
	 */
	define('CONTRACT_STATUS_ONHAND', 1);
	define('CONTRACT_STATUS_ONGOING', 2);
	define('CONTRACT_STATUS_COMPLETED', 3);
	define('CONTRACT_STATUS_RESCINDED', 4);
	define('CONTRACT_STATUS_AUTHORIZATION', 5);
	define('CONTRACT_STATUS_AUTHORIZED', 6);
	
	/*
	 * REQUEST TASK STATUS
	 */
	define('REQUEST_TASK_NOT_YET_STARTED', 1);
	define('REQUEST_TASK_ONGOING', 2);
	define('REQUEST_TASK_DONE', 3);
	define('REQUEST_TASK_SKIP', 4);
	
	/*
	 * REQUEST STATUS
	 */
	define('REQUEST_IN_PROCESSING', 1);
	define('REQUEST_APPROVED', 3);
	define('REQUEST_DISAPPROVED', 4);
	
	/*
	 * REGISTRATION STATUS
	 */
	define('REG_STATUS_ACTIVE', 1);
	define('REG_STATUS_EXPIRED', 2);
	define('REG_STATUS_VOIDED', 3);
	
	/*
	 * STAFF TYPES
	 */
	define('STAFF_TYPE_DIRECTOR', 'D');
	define('STAFF_TYPE_PRINCIPAL', 'P');
	define('STAFF_TYPE_TECHNICAL', 'T');
	define('STAFF_TYPE_WORKER', 'W');
	
	/*
	 * CONTRACTOR CATEGORY
	 */
	define('CONTRACTOR_CONSTRUCTION', 1);
	define('CONTRACTOR_SPECIALTY', 2);
	define('CONTRACTOR_CONSULTANCY', 3);
	
	/*
	 * REQUEST MANPOWER SCHEDULE
	 */
	define('MANPOWER_CATEGORY_DIRECT', 'D');
	define('MANPOWER_CATEGORY_INDIRECT', 'I');
	
	/*
	 * PROCUREMENT TYPES
	 */
	define('PROCUREMENT_TYPE_BIDDING', 'B');
	define('PROCUREMENT_TYPE_NEGOTIATION', 'N');
	
	/*
	 * INVOLVEMENT TYPES
	 */
	define('INVOLVEMENT_TYPE_PRIME', 'P');
	define('INVOLVEMENT_TYPE_SUB', 'S');
	define('INVOLVEMENT_TYPE_OTHER', 'O');
	
	/*
	 * FINANCIAL ASSISTANCE
	 */
	define('FINANCIAL_ASSISTANCE_PRIME', 1);
	define('FINANCIAL_ASSISTANCE_OTHER', 0);
	define('FINANCIAL_ASSISTANCE_NONE', 2);
	
	
	/*
	 * DEFAULT VALUES
	 */
	define('DEFAULT_DATE', '0000-00-00');
	
	define('COMPLETE_FLAG', 'C');
	define('INCOMPLETE_FLAG', 'I');
	define('NONE_FLAG', 'N');
	define('APPROVE_FLAG', 'A');
	define('DISAPPROVE_FLAG', 'D');
	
	define('ACTIVE_FLAG', 1);
	define('DISACTIVE_FLAG', 0);
	
	define('COMPLIED_YES_FLAG', 'Y');
	define('COMPLIED_NO_FLAG', 'N');
	
	define('TYPE_AUDITED', 'A');
	define('TYPE_INTERIM', 'I');
	
	//SINGLE SIGN ON
	define('SS_KEY', '$1ngl3$!gN');
	
	define('DECIMAL_PLACES', 2);
	
	define('PHIL_CODE', 'PHL');
	
	/*
	 * MPIS CONSTANTS
	 *
	 */
	
	// param_building_construction_types
	define('BLDG_CONS_RES', 1);
	define('BLDG_CONS_NON_RES', 2);
	
	define('MARKET_TYPE_EXPENDITURE', 1);
	define('MARKET_TYPE_INDUSTRIAL', 2);
	define('MARKET_TYPE_CONSTRUCTION', 3);
	
	define('PRICE_TYPE_GVC', 28);
	define('PRICE_TYPE_GVA', 29);
	
	// MARKET PRICE TYPES
	define('MARKET_PRICE_TYPE_EXPEN_CONS', 5);
	define('MARKET_PRICE_TYPE_EXPEN_CAPITAL_FORMAT', 3);
	
	define('MARKET_PRICE_TYPE_INDUS_CONS', 20);
	define('MARKET_PRICE_TYPE_INDUS_GDP', 23);
	
	define('MARKET_PRICE_TYPE_GV_CONS', 28);
	define('MARKET_PRICE_TYPE_GV_PUB', 26);
	define('MARKET_PRICE_TYPE_GV_PRIV', 27);
	
	define('INDUSTRY_GROUP_ALL', 1);
	define('INDUSTRY_GROUP_CONS', 5);
	
	define('MPIS_START_YEAR', 1990);
	
	define('RATIO_LIQUIDITY', 1);
	define('RATIO_CURRENT_RATIO', 2);
	define('RATIO_QUICK_RATIO', 3);
	
	define('RATIO_ACIDITY', 4);
	define('RATIO_TOTAL_ASSETS_TURNOVER', 5);
	define('RATIO_FIXED_ASSETS_TURNOVER', 6);
	define('RATIO_INVENTORY_TURNOVER', 7);
	
	define('RATIO_LEVERAGE', 8);
	define('RATIO_DEBT_TOTAL_ASSETS', 9);
	define('RATIO_DEBT_NETWORK', 10);
	
	define('RATIO_PROFITABILITY', 11);
	define('RATIO_PROFIT_MARGIN_1', 12);
	define('RATIO_PROFIT_MARGIN_2', 13);
	define('RATIO_PROFIT_MARGIN_3', 14);
	define('RATIO_RETURN_TOTAL_ASSETS', 15);
	define('RATIO_RETURN_NETWORTH', 16);
	
	
	define('RATIO_ITEM_INVENTORIES', 1);
	define('RATIO_ITEM_TOTAL_CURRENT_ASSETS', 2);
	define('RATIO_ITEM_TOTAL_FIXED_ASSETS', 3);
	define('RATIO_ITEM_TOTAL_ASSETS', 4);
	define('RATIO_ITEM_TOTAL_CURRENT_LIABILITIES', 5);
	define('RATIO_ITEM_TOTAL_LIABILITIES', 6);
	define('RATIO_ITEM_TOTAL_NETWORTH', 7);
	define('RATIO_ITEM_CONSTRUCTION_INCOME', 8);
	define('RATIO_ITEM_NON_CONSTRUCTION_INCOME', 9);
	define('RATIO_ITEM_COST_CONSTRUCTION_OPERATION', 10);
	define('RATIO_ITEM_COST_NON_CONSTRUCTION_OPERATION', 11);
	define('RATIO_ITEM_TOTAL_COST_OPERATIONS', 12);
	define('RATIO_ITEM_TOTAL_REVENUES', 13);
	define('RATIO_ITEM_NET_INCOME_AFTER_TAX', 14);
	
	define('POL_CAT_BM', 1);
	define('POL_CAT_BR', 2);
	define('POL_CAT_ADMIN', 3);
	define('POL_CAT_LAW', 4);
	define('POL_CAT_BILL', 5);
	define('POL_CAT_CASE', 6);
	
	
	define('BM_CLASS_REGULAR', 1);
	define('BM_CLASS_SPECIAL', 2);
/*
|--------------------------------------------------------------------------
| BAS CONSTANTS
|--------------------------------------------------------------------------
*/

	/*
	 |--------------------------------------------------------------------------
	 | PSMS CONSTANTS
	 |--------------------------------------------------------------------------
	 */
	
	define('CSS_PSMS', 'psms');
	
	
/*
 |--------------------------------------------------------------------------
 | CS COLLECTION GROUP CONSTANTS
 |--------------------------------------------------------------------------
 */
	define('POCB_COLLECTION_GROUP', 'PODCB');
	
/* End of file constants.php */
/* Location: ./application/config/constants.php */