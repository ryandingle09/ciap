<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['sync_upload_remote_base_url'] = 'http://localhost/ciap_portal/uploads/';
$config['sync_upload_local_base_url_path'] = 'uploads';
$config['sync_upload_local_site_url_path'] = 'files/produce';
$config['sync_upload_local_dir_path'] = FCPATH . '/uploads/';
