<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Files extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('authenticate');
		$this->load->library('sync_upload');
	}
	
	// Please remove this function.
	// This is only for testing
	public function index()
	{
		$file = 'magtanim/tayo/ng/kamote.jpg';
		echo sprintf('<a href="%s">kamote</a>', htmlentities($this->sync_upload->generate_url($file)));
	}
	
	public function produce()
	{
		
		$path = implode('/', func_get_args());
		$this->sync_upload->produce_file($path);
	}
	
}