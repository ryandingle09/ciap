<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH . '/libraries/REST_Controller.php');

class Cs_official_receipt_api extends REST_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		
		// Getting values from the configuration
		$level			= $this->config->item('rlog_level');
		$enable			= $this->config->item('rlog_enable');
		$error_handler	= $this->config->item('rlog_error_handler');
		$location		= realpath(APPPATH) . DS . 'logs' . DS . 'api';
		
		// Setting up RLog
		RLog::location($location);
		RLog::level($level);
		RLog::enable($enable);
		RLog::setErrorHandler($error_handler);
		
		$this->load->model('api/cs_official_receipt_api_model', 'coram', TRUE);
	}
	
	
	
	
	public function update_or_post()
	{	
		try
		{
			$msg 	 	 = "";
			$success 	 = 0;
			$params      = array();
			$where       = array();
			$data    	 = $this->post();
			
			RLog::info('UPDATE ORDER OF PAYMENT');
			
			$ids 	  = explode('-', $data['application_ref_no']);
			$app_code = $data['app_code'];
			
			$params['or_date']    = date('Y-m-d', strtotime($data['or_date']));
			$params['or_number']  = $data['or_no'];
			$params['or_amount']  = $data['or_amount'];
			
			$where['request_id']      = $ids[1];
			$where['rqst_payment_id'] = $ids[2];
		/* 	RLog::info('DATE : '.var_export($data, TRUE));
			RLog::info('PARAMS : '.var_export($params, TRUE));
			RLog::info('WHERE  : '.var_export($where, TRUE)); */
			$this->coram->set_dsn($app_code);
			
			$this->coram->update_op_or_details($params, $where);
			
			$success 	= 1;
			$msg 	 	= "Successful";
		}catch(Exception $e){
			$msg = $e->getMessage();
			RLog::error($msg);
		}catch(PDOException $e){
			$msg = $e->getMessage();
			RLog::error($msg);
		}
	
		$wsData = array(
				"msg"         => $msg,
				"status"      => $success,
		);
	
		$this->response($wsData);
	}
}