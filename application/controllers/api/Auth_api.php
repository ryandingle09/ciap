<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH . '/libraries/REST_Controller.php');

class Auth_api extends REST_Controller
{
	private $projects = array(PROJECT_CEIS, PROJECT_BAS, PROJECT_MPIS, PROJECT_PIS, PROJECT_ASIS, PROJECT_ISCA, 'psms');
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('api/auth_api_model', 'aam');
		
 		$level			= $this->config->item('rlog_level');
		$enable			= $this->config->item('rlog_enable');
		$error_handler	= $this->config->item('rlog_error_handler');
		$location		= realpath(APPPATH) . DS . 'logs' . DS . 'default';
		
		// Setting up RLog
		RLog::location($location);
		RLog::level($level);
		RLog::enable($enable);
		RLog::setErrorHandler($error_handler); 
	}
	
	public function verify_user_post()
	{
		try
		{
			$status = 0;
			$data   = $this->post();
			
			$update_salt  = 0;
			$selected_app = $data['selected_application'];
			$employee_no  = $data['employee_no'];
			$first_name   = $data['first_name'];
			$last_name    = $data['last_name'];
			$api_salt     = $data['api_salt'];
			
			$this->aam->set_dsn($selected_app);
			
			Auth_api_model::beginTransaction();
			
			//CHECK IF APP IS CORRECT
			if(in_array($selected_app, $this->projects))
			{
				//GET USER DETAILS
				$user = $this->aam->select_user($employee_no);
				
				//GENERATE TOKEN
				$salt   = gen_salt();
				$token  = in_salt($selected_app.$employee_no, $salt);
				
				//IF USER HAS NO RECORD INSERT IN THE SELECTED APP
				if( EMPTY($user) ):
					//CREATE USER 
					$params = array('employee_no' => $employee_no, 'first_name' => $first_name, 'last_name' => $last_name, 'token' => $token);
					
					$this->aam->insert_user($params);
					
					$status = 1;
					$update_salt = 1;
				else:
					//IF USER IS ALREADY EXISTING VERIFY IF IT HAS TOKEN
					if( ! EMPTY($user['token']) )
					{
						//IF YES CHECK TOKEN 
						if(in_salt($selected_app.$employee_no, $api_salt) != $user['token'])
							throw new Exception('Invalid token!');
					} 
					//IF NO UPDATE TOKEN GENERATED
					else
					{
						$this->aam->update_user($employee_no, $token);
						$update_salt = 1;
					}
					
					/* 
					 //CHECK USER TOKEN
					 if(in_salt($selected_app.$employee_no, $api_salt) == $user['token'])
					 	$status = 1;
					 else
					 	throw new Exception('Invalid token!'); 
					 */
				endif;
			}	
			else
			{	
				throw new Exception('Invalid application!');
			}
			
			Auth_api_model::commit();
			
			$status = 1;
			$msg	= 'User verification successful!';
		}catch(Exception $e){
			Auth_api_model::rollback();
			
			$status = 0;
			$msg	= $e->getMessage();
			RLog::error($msg);
		}
		
		$this->response( array('status' => $status, 'msg' => $msg, 'update_salt' => $update_salt,'app_salt' => $salt) );
	}
	
	public function unset_token_post()
	{
		try
		{
			$data     = $this->input->post();
			$app_code = $data['app_code'];
			$employee_no = $data['employee_no'];
			
			$this->aam->set_dsn($app_code);
			
			$this->aam->update_user($employee_no, NULL);
				
			$msg      = 'Successful!';
			$status   = 1;
		}catch(Exception $e){
			$status   = 0;
			$msg	  = $e->getMessage();
				
			log_message('error', $msg);
		}
	
		$this->response( array('status' => $status, 'msg' => $msg) );
	}
	
	
	
	/* BELOW ARE OLDS CODES */
	public function set_auth_values_post()
	{ 
		try
		{ 
			$data  		= $this->post();
			$responses  = array();
			//GET THE CURRENT SESSION ID
			$session_id = session_id();
			
			//SET SESSION
		 	$newdata = array(
				'logged_in'        => TRUE,
				'session_id'       => $session_id,
		        'employee_no'      => $data['employee_no'],
		        'office_code'  	   => $data['office_code'],
				'salutation_code'  => $data['salutation_code'],
				'first_name'  	   => $data['first_name'],
				'middle_name'  	   => $data['middle_name'],
				'last_name'  	   => $data['last_name'],
				//'role_codes'  	   => $data['role_codes'],
		 		'user_roles'  	   => $data['role_codes'],
				'profile_pic_src'  => $data['profile_pic_src'],
				'selected_application'  	=> $data['selected_application'],
				'default_application_code'  => $data['default_application_code']
			);

			$this->session->set_userdata($newdata);  
			
			/* RETURN THESE DATA
			 * @url 	    - url where to redirect the user.
			 * @session_id  - pass the currrent session_id to create a cookie in ciap_intranet with this session_id as it's value.
			 * @cookie_name - the name of the cookie to be created.  
			 */
			$responses = array(
				'url'		  => base_url().$data['selected_application'].'/'.$data['selected_application'].'_dashboard',
				'session_id'  => $session_id, 
				'cookie_name' => $this->config->item('sess_cookie_name'), 
			); 
			
			$msg      = 'Successful!';
			$status   = 1;
		}catch(Exception $e){
			$status = 0;
			$msg	= $e->getMessage();
			log_message('error', $msg);
		}
		
		$this->response( array('status' => $status, 'msg' => $msg, 'data' => $responses) );
	}

	public function unset_auth_values_post()
	{  
		try
		{	
			$data    = $this->input->post();
			$cookies = $data['cookie'];
			$session_id = $cookies[$this->config->item('sess_cookie_name')];  
	
			$this->_destroy_session($session_id);
			
			//DESTROY CURRENT SESSION
			$this->session->sess_destroy();
			
			$msg     = 'Successful!';
			$status  = 1;
		}catch(Exception $e){
			$status = 0;
			$msg	= $e->getMessage();
			
			log_message('error', $msg);
		}
		
		$this->response( array('status' => $status, 'msg' => $msg) );
	}
	
	
	private function _destroy_session($session_id){
		$path = $this->config->item('sess_save_path').DIRECTORY_SEPARATOR.$this->config->item('sess_cookie_name').$session_id; 
		
		if (file_exists($path))
		{	
			setcookie(
				$this->config->item('cookie_name'),
				NULL,
				1,
				$this->config->item('cookie_path'),
				$this->config->item('cookie_domain'),
				$this->config->item('cookie_secure'),
				TRUE
			);	
			
			return unlink($path) ? 1 : 0;
		} 
		
		return 0;
	}
}