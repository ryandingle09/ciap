<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require(APPPATH . '/libraries/REST_Controller.php');

class Ceis_cron_report_api extends REST_Controller
{
	private $marker  = '-$!##$!@@#';
	private $key	 = 'c0n$+ruc+10n1ndu$+ry';
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('api/ceis_cron_report_api_model', 'ccram', TRUE);

		$this->load->library('encrypt');
		$this->load->library('email_template');
		$this->load->library('rest', array(
				'server' => CEIS_API_SERVER 
		),'ceis_api');
	}

	private function _process_file($filename, $folder, $cron=0)
	{
		try 
		{
			 RLog::info('Checking if file exist...');
			 $file_status = $this->_check_file_exist($filename);
			 $type  	  = ($cron) ? ' BY CRON' : ' DIRECTLY';
			 $success     = 0;
			 
			 if($file_status !== FALSE)
			 {
			 	switch ($file_status):
				 	case API_RESPONSE_PROCESSED: // 1 - naprocess na pla kaya return nalang
				 		$msg 	 = 'Success, The file has already been processed before.';
				 		$success = API_RESPONSE_PROCESSED;
			 		break;
				 	case API_RESPONSE_PROCESSING: // 2, meron nang copy dun sa processed folder, wag nang galawin dahil baka may ibang process na gumagawa ex: cron
				 		$msg 	 = "There's already a copy on PROCESSING Folder, and another script might be processing it at the moment.";
				 		$success = API_RESPONSE_PROCESSING;
			 		break;
				 	case API_RESPONSE_UNPROCESSED:// 3, meron nang copy dun sa unprocessed folder, iba yung naglahay so hayaan lang
				 		$msg 	 = "There's already a copy on UNPROCESSED Folder, and a cron job might be processing it at the momment.";
				 		$success = API_RESPONSE_UNPROCESSED;
			 		break;
			 	endswitch;

			 	$this->_log_cron_msg('FILE WAS ACCESSED'.$type.' | FILENAME : '.$filename.' | MSG : '.$msg, 1);
			 	
			 	//Send response immediately if the file status meets any one of conditions above
			 	return array("msg" => $msg, "status"  => $success);
			 }
			 else 
			 {
			 	$url  	 = CEIS_API_PROGRESS_REPORT_SERVER.'transfer_specific_file/'.$filename.'/'.$folder;
			 	$headers = @get_headers($url,1);
			 	
			 	if(!$headers || empty($headers) || is_null($headers) || $headers[0] == 'HTTP/1.1 404 Not Found')  throw new Exception('Unable to get headers, Server Offline.');
			 	if(!isset($headers['Filesize']) || !isset($headers['Filename'])) throw new Exception('No File.');
			 	
			 	$original_filesize = $headers['Filesize'];
			 	$original_filename = rtrim($headers['Filename'],CRON_FILE_EXT) . CRON_FILE_EXT;
			 	
			 	if($original_filename != $filename) throw new Exception('The filename has changed from '.$filename.' to '.$original_filename);
			 	
			 	$file = fopen($url, "rb");
			 	
			 	sleep(1);
			 	
			 	if($file)
			 	{
			 		$newf = @fopen (PATH_UPLOADS_CRON_PROGRESS_REPORT_PROCESSING.$filename, "wb");
			 		
			 		if(!$newf) throw new Exception('Server Offline \n'.PATH_UPLOADS_CRON_PROGRESS_REPORT_PROCESSING.$filename);
			 			
			 		$total_fetched = 0;
			 			
			 		while(!feof($file))
			 		{
			 			if(!($fetched = fread($file, 8 * 1000000)) && $original_filesize != $total_fetched) throw new Exception("Unable To Read.");
			 			$newf_stat = fstat($newf);
			 			fwrite($newf, $fetched, 8 * 1000000 );
			 			$total_fetched = $total_fetched + mb_strlen($fetched, '8bit');
			 	
			 		}
			 			
			 		if($file) fclose($file);
			 			
			 		if($newf) fclose($newf);
			 			
			 		clearstatcache();
			 			
			 		if($original_filesize == $total_fetched):
			 			$this->_validate_file($original_filename);
			 		else:
			 			throw new Exception('File Size Mismatch. Header Size ='.$original_filesize.', Total Fetched = '.$total_fetched. "\n");
			 		endif;
			 			
			 	}
			 	else
			 	{
			 		throw new Exception('Connection Problem.');
			 	}
			 	//INSERT DB SCRIPTS
				$data = $this->_get_file_contents(PATH_UPLOADS_CRON_PROGRESS_REPORT_PROCESSING.$filename);
				
				//$request_details    = $data['ceis_'.Ceis_cron_api_model::tbl_requests][0];
				/* REQUEST TYPE NAME */
				//$request_type   	= $this->ccam->get_request_type($request_details['request_type_id']);
				
				/* GET EMAIL BASE ON REQUEST TYPE
				if($request_details['request_type_id'] == REQUEST_TYPE_EQP):
					 $equipment_details  = $data['ceis_'.Ceis_cron_api_model::tbl_request_equipment][0];
					 $contractor_email   = $equipment_details['registrant_email'];
					 
					 $this->_log_cron_msg('FILE WAS ACCESSED'.$type.' | FILENAME : '.$filename.' | EMAIL MSG : '.$contractor_email.' | DATA : '.var_export($data, TRUE), 1);
				else:
					 $contractor_details = $data['ceis_'.Ceis_cron_api_model::tbl_request_contractor][0];
					 $contractor_email   = $contractor_details['rep_email'];
					 
					 $this->_log_cron_msg('FILE WAS ACCESSED'.$type.' | FILENAME : '.$filename.' | EMAIL MSG : '.$contractor_email.' | DATA : '.var_export($data, TRUE), 1);
				endif;
				 */
				
				$report_details 	= $data['ceis_'.Ceis_cron_report_api_model::tbl_progress_reports][0];
				$contract_id        = $report_details['contract_id'];
				$contract_details   = $this->ccram->get_contract_details($contract_id); 
				
				$email = $this->ccram->get_contractor_email_by_contract($contract_id);
				
				switch($report_details['quarter']):
			 		case 1  : $quarter = '1st'; break;
			 		case 2  : $quarter = '2nd'; break;
			 		case 3  : $quarter = '3rd'; break;
			 		case 4  : $quarter = '4th'; break;
			 		default : $quarter = '';
				endswitch;
				
				CEIS_Model::beginTransaction();
				
				$this->ccram->insert_report_data($data);
				
				//EMAIL FUNCTIONALITY
				$this->_email_client($email, array('project_title' => $contract_details['project_title'], 'year' => $report_details['year'], 'quarter' => $quarter) );
				
				//ADD API IF FUNCTION CALL IS FROM CRON
				
				
				//TRANSFER FILE TO PROCESSED
				$from_path = PATH_UPLOADS_CRON_PROGRESS_REPORT_PROCESSING; 
				$to_path   = PATH_UPLOADS_CRON_PROGRESS_REPORT_PROCESSED;
				
				$this->_transfer_file($from_path, $to_path, $filename);
				
				CEIS_Model::commit();

				$msg 	   = 'File transfer successful.';
				$success   = 1;
				
				$this->_log_cron_msg('FILE WAS ACCESSED'.$type.' | FILENAME : '.$filename.' | MSG : '.$msg, 1);
			 }

		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
			$this->_delete_file(PATH_UPLOADS_CRON_PROGRESS_REPORT_PROCESSING.$filename);
			$msg = $e->getMessage();
			$this->_log_cron_msg('FILE WAS ACCESSED'.$type.' | FILENAME : '.$filename.' | MSG : '.$msg, 0);
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();
			$this->_delete_file(PATH_UPLOADS_CRON_PROGRESS_REPORT_PROCESSING.$filename);
			$msg = $e->getMessage();
			$this->_log_cron_msg('FILE WAS ACCESSED'.$type.' | FILENAME : '.$filename.' | MSG : '.$msg, 0);
		}
		
		return array('msg' => $msg, 'status' => $success);
	}
	
	
	
	public function inform_file_transfer_ready_post()
	{
		try
		{ 
			$msg 	 	 = "";
			$success 	 = 0;
			$data    	 = $this->post();
			$filename    = $data['filename'].CRON_FILE_EXT;
			$result      = $this->_process_file($filename, FOLDER_RESPONSE_PROCESSING, 0);
			
			$this->response($result);
		}catch(Exception $e){
			$msg = $e->getMessage();

			$this->_log_cron_msg($msg, 0);
		}catch(PDOException $e){
			$msg = $e->getMessage();

			$this->_log_cron_msg($msg, 0);
		}
	}
	
	private function _check_file_exist($filename)
	{
		try{
			if(file_exists(PATH_UPLOADS_CRON_PROGRESS_REPORT_PROCESSING.$filename))  return API_RESPONSE_PROCESSING;
			
			if(file_exists(PATH_UPLOADS_CRON_PROGRESS_REPORT_PROCESSED.$filename))   return API_RESPONSE_PROCESSED;
			
			if(file_exists(PATH_UPLOADS_CRON_PROGRESS_REPORT_UNPROCESSED.$filename)) return API_RESPONSE_UNPROCESSED;
			
			return FALSE;
			
		}catch(Exception $e){
			throw $e;
		}
	}
	
	
	private function _validate_file($filename)
	{
		try
		{
			$fetched = '';
			$file 	 = @fopen(PATH_UPLOADS_CRON_PROGRESS_REPORT_PROCESSING.$filename, "r");
			if(!$file) throw new Exception('Server Offlineasdfasdf \n'.PATH_UPLOADS_CRON_PROGRESS_REPORT_PROCESSING.$filename);
			$total_fetched = 0;
			while(!feof($file))
			{
				$fetched = fread($file, 8 * 1000000);
			}
	
			$contents    = $fetched;
			$file_marker = substr($contents, -10);
			if($file_marker != $this->marker)
			{
				throw new Exception("\n Failed, ".$file_marker." is not equal to ".$this->marker." \n");
			}
		}
		catch(Exception $e)
		{
			//ADD UNLINK 
			$this->_delete_file(PATH_UPLOADS_CRON_PROGRESS_REPORT_PROCESSING.$filename);
			throw new Exception($e->getMessage().' Line :'. $e->getLine());
		}
	}
	
	private function _transfer_file($from, $to, $filename)
	{
		try
		{
			//Check if $from dir exists.
			if ( ! is_dir($from)):
				if( ! mkdir($from, DIR_WRITE_MODE, true))
					throw new Exception(sprintf($this->lang->line('err_create_dir'), ' CRON PROGRESS REPORT PROCESSING : '.$from));
			endif;
	
			//Check if $to dir exists.
			if ( ! is_dir($to)):
				if( ! mkdir($to, DIR_WRITE_MODE, true))
					throw new Exception(sprintf($this->lang->line('err_create_dir'), ' CRON PROGRESS REPORT PROCESSING : '.$to));
			endif;
						
			$from = $from.$filename;
			$to   = $to.$filename;
						
			//Transfer file then delete it from source directory.
			if(copy($from,$to)) unlink($from);
		}catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _delete_file($path)
	{
		if(file_exists($path)) 
			unlink($path);
		 else
		 	Rlog::error('File does not exist.');
	}
	
	private function _log_cron_msg($msg, $type)
	{
		// Getting values from the configuration
		$level			= $this->config->item('rlog_level');
		$enable			= $this->config->item('rlog_enable');
		$error_handler	= $this->config->item('rlog_error_handler');
		$location		= realpath(APPPATH) . DS . 'logs' . DS . 'cron_progress_report';
		
		// Setting up RLog
		RLog::location($location);
		RLog::level($level);
		RLog::enable($enable);
		RLog::setErrorHandler($error_handler);
		
		if($type)
			RLog::info($msg);
		else
			RLog::error($msg);
	}
	
	private function _get_file_contents($filepath)
	{
		try
		{
			$raw_data 	  = rtrim(file_get_contents($filepath),$this->marker);
			$decoded_data = $this->encrypt->decode($raw_data, $this->key,FALSE);
			$data		  = json_decode($decoded_data,TRUE);
			
			if(is_null($data) || empty($data) || !isset($data)) throw new Exception('Unable to cast json data.');
			
			return $data;
		}catch(Exception $e)
		{
			throw $e;
		}
	}

	private function _email_client($email, $add_data=array())
	{
		try 
		{ 
			 $params = array();
			 $params["fields"] 	 = array('sys_param_name', 'sys_param_value');
			 $params["where"] 	 = array('sys_param_type' => SYS_PARAM_TYPE_SMTP);
			 $params["multiple"] = TRUE;
			 $email_params 		 = get_values('sys_param_model', 'get_sys_param', $params, PROJECT_CORE);
			 //Ni try ko lang para hindi na ko may for loop :) :*
			 $formatted_params   = array_column($email_params, 'sys_param_value', 'sys_param_name');
			
			 $email_data = array();
			 $email_data['subject']    = 'Submission of progress report';
			 $email_data['to_email']   = array($email);
			 $email_data['from_name']  = $formatted_params[SYS_PARAM_TYPE_SMTP_REPLY_NAME];
			 $email_data['from_email'] = $formatted_params[SYS_PARAM_TYPE_SMTP_REPLY_EMAIL];

			 $this->email_template->send_email_template($email_data, 'cron_progress_report', $add_data);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function fetch_unprocessed_file_get()
	{
		try
		{
			if(is_cli())
			{
			 	$this->ccam->lock_cron();
			 	
			    $data 	  = array( 'key' => $this->key);
				$raw_data = $this->ceis_api->get('unprocessed_file', $data);
				$api_data = convert_object_to_array($raw_data);
				
				$this->_log_cron_msg('Fetching file', 1);
				
				if( ! EMPTY($raw_data) ||  ! EMPTY($api_data['status']))
				{	
					if(EMPTY($api_data['filename'])):
						$this->_log_cron_msg('No new files.', 1);
					else:
						$result = $this->_process_file($api_data['filename'], FOLDER_RESPONSE_UNPROCESSED, 1);
						
						if($result['status'])
						{	
							$result['filename'] = $api_data['filename'];

							$raw_data = $this->ceis_api->post('move_processed_file.json', $result);
							$api_data = convert_object_to_array($raw_data);
							
							if(EMPTY($raw_data) ||  EMPTY($api_data['status']))
								$this->_log_cron_msg('Error in tranfer of file in portal.', 1);	
						}
						else 
						{	
							throw new Exception($result['msg']);
						}
					endif;	
				}		
				else
				{	
					throw new Exception('Can\'t connect to API - GET UNPROCESSED FILE');	
				}
				
				$this->ccam->unlock_cron();
			}
			else
			{
				throw new Exception('Not accessed through command line.');
			}
		}
		catch(PDOException $e)
		{
			$this->ccam->unlock_cron();
			$msg = $e->getMessage();
			$this->_log_cron_msg($msg, 0);
		}
		catch(Exception $e)
		{
			$this->ccam->unlock_cron();
			$msg = $e->getMessage();
			$this->_log_cron_msg($msg, 0);
		}
	}
}