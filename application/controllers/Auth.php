<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends Base_Controller {

	public function __construct() 
	{
		parent::__construct();
	}
	
	
	public function index()
	{	
		$this->load->view('login');
	}	
	
	public function sign_in($username = NULL, $password = NULL) 
	{
		$flag = 0;
		$msg = "";
		$salted = FALSE;
		
		try {
			if(!IS_NULL($username) AND !IS_NULL($password)){
				$username = filter_var($username, FILTER_SANITIZE_STRING);
				$username = base64_url_decode($username);
				$password = filter_var(base64_url_decode($password), FILTER_SANITIZE_STRING);
				
				$this->auth_model->update_status($username);
				
				$salted = TRUE;
			} else {
				$params = get_params();
				
				$username = filter_var($params['username'], FILTER_SANITIZE_STRING);
				$password = filter_var($params['password'], FILTER_SANITIZE_STRING);
			}	
			
			// if(EMPTY($username)) throw new Exception($this->lang->line('username_required'));
			if(EMPTY($username)) throw new Exception($this->lang->line('email_required'));
			if(EMPTY($password)) throw new Exception($this->lang->line('password_required'));
			
			
			$this->authenticate->sign_in($username, $password, $salted);
			
			$flag = 1;						
		}
		catch(Exception $e)
		{
			$msg = $e->getMessage();
		}
		
		$result = array(
			"flag" => $flag,
			"msg" => $msg
		);
		
		if($salted){
			$this->authenticate->check_user();	
		} else {
			echo json_encode($result);
		}	
		
	}
	
	public function sign_out()
	{
		$flag = 0;
		$msg = "";
		
		try
		{
			//redirect(SERVER_ROOT_PATH_CIAS);
			
			/* $this->authenticate->sign_out();
			
			// Unset autologin variable
			delete_cookie('autologin');
			$flag = 1;		 */					
		}
					
		catch(Exception $e)
		{
			$msg = $e->getMessage();
		}
		
	 	$result	= array(
			"flag" => $flag,
			"msg"  => $msg,
			"url"  => SERVER_ROOT_PATH_CIAS 	
		); 
												
		echo json_encode($result);
			
	}
		
}
/* End of file auth.php */
/* Location: ./application/controllers/auth.php */