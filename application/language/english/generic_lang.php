<?php

// SYSTEM AUTHENTICATION
$lang['invalid_login'] = "Sorry the login was incorrect. Please try again.";
$lang['username_required'] = "Please provide your username.";
$lang['password_required'] = "Please provide your password.";
$lang['email_required'] = "Please provide your email.";
$lang['confirm_password'] = "Please confirm your password.";
$lang['contact_admin'] = "There is something wrong with your account, please contact the system's administrator.";
$lang['system_error'] = "There seems to be a problem with the system or your internet connection, please try again later.";
$lang['reset_password'] = "An instruction on how to reset your password has been sent to your email.";
$lang['email_exist'] = "Sorry, but your email is already registered with our system. Please use another email and try again.";
$lang['invalid_action'] = "Sorry, but your current action is invalid.";
$lang['password_reset']		= "Your new password has been reset, you may now login using your new password.";
$lang['signup_success']		= "Your registration has been sent to the site administrator for validation and approval.  Please check your email for further instructions.";
$lang['pending_account']	= "Sorry, your account is awaiting approval by the site administrator. Once approved or denied you will receive an email notice.";
$lang['account_blocked']	= "Sorry, this account has been blocked. Please contact the site administrator to unblock your account.";
$lang['account_expired']	= "Sorry, this account has already expired. Please contact the site administrator to renew your account.";


// ACTIONS MESSAGES
$lang['data_saved'] 		= "Record was successfully saved.";
$lang['data_updated'] 		= "Record was successfully updated.";
$lang['data_deleted'] 		= "Record was successfully deleted.";
$lang['data_not_saved'] 	= "An error occurred while saving the record. Please try again later.";
$lang['data_not_updated']	= "An error occurred while updating the record. Please try again later.";
$lang['data_not_deleted']	= "An error occurred while deleting the record. Please try again later.";
$lang['data_spec_saved'] 	= "%s was successfully saved.";
$lang['data_spec_deleted'] 	= "%s was successfully deleted.";

// SYSTEM MESSAGES
$lang['confirm_error']				= "There was an error on your password confirmation, please try again.";
$lang['parent_delete_error'] 		= "Record with dependency cannot be deleted.";
$lang['member_admin_delete_error'] 	= "This action cannot be performed because this member is assigned as the initiative manager.";
$lang['detail_view_error'] 			= "Parent table is empty, the following detail can't be viewed.";
$lang['detail_delete_error'] 		= "Parent table is empty, the following detail can't be deleted.";
$lang['detail_save_error'] 			= "Parent table is empty, the following detail can't be saved.";
$lang['data_empty'] 				= "No matching records found.";
$lang['is_required']			    = "%s is required.";
$lang['invalid_data'] 				= "Invalid data for %s";
$lang['err_duplicate_data']			= "Duplicate data entry";
$lang['err_duplicate_data_spec']	= "Duplicate data entry for %s.";
$lang['err_delete_file']			= "Unable to delete file. %s";

// PERMISSION MESSAGE
$lang['err_unauthorized_add'] = "Sorry, you don't have permission to add this record. Please contact the system's administrator.";
$lang['err_unauthorized_edit'] = "Sorry, you don't have permission to edit this record. Please contact the system's administrator.";
$lang['err_unauthorized_delete'] = "Sorry, you don't have permission to delete this record. Please contact the system's administrator.";
$lang['err_unauthorized_save'] = "Sorry, you don't have permission to save this record. Please contact the system's administrator.";
$lang['err_unauthorized_view'] = "Sorry, you don't have permission to view the record. Please contact the system's administrator.";
$lang['err_unauthorized_access'] = "Sorry, you don't have permission to access the page. Please contact the system's administrator.";

//AUDIT TRAIL 
$lang['audit_trail_add']					= "%s has been added";
$lang['audit_trail_update']					= "%s has been updated";
$lang['audit_trail_delete']					= "%s has been deleted";

$lang['audit_trail_update_specific']		= "%s has been updated for %s";
$lang['audit_trail_add_specific']			= "%s has been added for %s";
$lang['audit_trail_delete_specific']		= "%s has been deleted for %s";



// 
$lang['err_invalid_request']	= 'Error. Invalid request.';
$lang['err_internal_server']    = 'Internal Server Error.';


//CRON MSGS
$lang['err_create_dir']					    = "Can't create directory for %s";