<div style="width:85%; margin:0 auto;">
  <div style="background:#f2c65d; padding:20px 30px;"><img src="<?php echo base_url() . PATH_IMAGES ?>logo_new2.png" height="40" alt="logo" /></div>
  <div style="background:#f6f6f6; padding:30px;">
	<p style="font-size:14px; font-family:'Open Sans', arial; margin-bottom:20px;">Dear Sir/Madam,</p>
	<p style="font-size:14px; font-family:'Open Sans', arial;margin:10px 0 20px; line-height:25px;">
	  Your request for  <?php echo $request_type_name; ?> with the request number of <?php echo $request_id; ?>  has been <?php echo $request_status; ?>.
	</p>

	<p style="margin-top:20px; font-size:14px; font-family:'Open Sans', arial; line-height:25px;">Sincerely,<br/>Philippine Overseas Construction Board</p>	
	<p style="font-size:13px; color:#999; text-shadow: 0 0 2px #fff; font-family:'Open Sans', arial;margin:30px 0 0; line-height:25px;">
	  This email is sent automatically by the system, please do not reply directly. 
	</p>
  </div>
</div>
