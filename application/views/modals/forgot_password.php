<div class="md-header p-md"></div>
		  
<form id="forgot_password_form" class="default p-lg">
	<input type="hidden" name="msg_post_as" id="msg_post_as" value="Send Message"/>
	<input type="hidden" name="msg_recipient[]" id="msg_recipient" value=""/>
	<h5>Forgot your password?</h5>
	<div class="password-box">Enter the email registered with your account and we'll send you the link where you can change your password.</div>
	<div class="p">
	  <div class="form-group">
	    <input type="text" name="email" id="email" data-parsley-required="true" data-parsley-maxlength="255" data-parsley-type="email" data-parsley-trigger="keyup" placeholder="enter your email address"/>
	  </div>			  
	  <div class="text-right m-t-md"><button type="submit" class="btn-default red" id="forgot_password_btn" name="forgot_password_btn" value="<?php echo BTN_EMAILING ?>">Reset Password</button></div>
	</div>
	
</form>
<div class="md-footer p text-right"></div>

<script>
$(function(){
	
	var $base_url = "<?php echo base_url() ?>";
		$forgot_password = $("#forgot_password_form");

	$forgot_password.parsley().subscribe('parsley:form:success', function (formInstance) {
		formInstance.submitEvent.preventDefault();
		
		var data = $forgot_password.serialize();
		button_loader("forgot_password_btn", 1);
		$.post($base_url + "forgot_password/request_reset/", data, function(result) {
			if(result.flag == 1){
				close_modal("modal_forgot_password");
				alert_msg("<?php echo SUCCESS ?>", result.msg);
			} else {
				alert_msg("<?php echo ERROR ?>", result.msg);
				button_loader("forgot_password_btn", 0);
			}					  				
		}, 'json');
	});
		
});
</script>