<form>
	<?php 
		$style = (strtoupper($generate) == "CHART") ? "" : 'style="min-height: 600px; height: 600px;"';
	?>
	<div class="panel p-md" <?php echo $style ?>>
		<div class="col s12">
		
			<?php 
				switch(strtoupper($generate))
				{
					case "PDF":
						
						if(!EMPTY($pdf))
							echo '<object width="100%" height="100%" type="application/pdf" data="data:application/pdf;base64,'.base64_encode($pdf).'">Cant display PDF</object>';
						else
							echo '<div class="title"><h4>Cant display PDF</h4></div>';
					break;
					
					case "CHART":
						echo $chart;
					break;
				}
			
			?>
			
			
		</div>
		
	</div>
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	</div>
</form>
