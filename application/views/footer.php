<?php 
  if(!EMPTY($load_css)){
	foreach($load_css as $css): 
	  echo '<link href="'. base_url() . PATH_CSS . $css .'.css" rel="stylesheet" type="text/css">';
	endforeach; 
  }

  if(!EMPTY($load_js)){
	foreach($load_js as $js): 
	  echo '<script src="'. base_url() . PATH_JS . $js .'.js" type="text/javascript"></script>';
	endforeach; 
  }
?>	
 <?php 

	if( ISSET( $load_modal ) AND !EMPTY( $load_modal ) ) :

		foreach( $load_modal as $modal_id => $options ) :

			$size 		= '';
			$title 		= '';
			$id 		= '';

			$controller = '';
			$method 	= '';
			$module 	= '';

			$multiple 			= '';
			$scroll 			= '';
			$height 			= '';

			$ajax 				= false;

			if( is_numeric( $modal_id ) )
			{
				$id 			= $options;
				$clean 			= str_replace( array( 'modal_', '_' ), array( '', ' ' ), $options );

				$title 			= ucfirst( strtolower( $clean ) );

				$controller 	= $clean;
				$method 		= strtolower( $clean ).'_modal';

			}
			else 
			{

				$id 			= $modal_id;
				$clean 			= str_replace( array( 'modal_', '_' ), array( '', ' ' ), $modal_id );

				if( !EMPTY( $options ) AND is_array( $options ) )
				{
					$size 		= ( ISSET( $options['size'] ) ) ? $options['size'] 	 : '';

					if( ISSET( $options['title'] ) )
					{
						$title  = $options['title'];
					}
					else 
					{
						$title  = ucfirst( strtolower( $clean ) );
					}

					if( ISSET( $options['controller'] ) )
					{
						$controller = $options['controller'];
					}
					else 
					{
						$controller = $clean;
					}

					if( ISSET( $options['method'] ) )
					{
						$method 	= $options['method'];
					}
					else
					{
						if( ISSET( $options['multiple'] ) )
						{
							$method 	= strtolower( $clean ).'_modal';	
						}
						else 
						{
							$method 	= 'modal';
						}
						
					}

					if( ISSET( $options['module'] ) )
					{
						$module 	= $options['module'];
					}

					if( ISSET( $options['actions_elem'] ) )
					{
						$actions_elem = $options['actions_elem'];
					}

					if( ISSET( $options['actions_event'] ) )
					{
						$actions_event = $options['actions_event'];
					}

					if( ISSET( $options['height'] ) )
					{
						$height 		= $options['height'];
					}

					if( ISSET( $options['multiple'] ) )
					{
						$multiple 		= $options['multiple'];
					}

					if( ISSET( $options['ajax'] ) )
					{
						$ajax 			= $options['ajax'];
					}
					
				}
			}



?>
<div id="<?php echo $id ?>" class="md-modal md-effect-<?php echo MODAL_EFFECT ?> <?php echo $size ?> ">
	<div class="md-content">
		<a class="md-close icon">x</a>
		<h3 class="md-header"><?php echo $title ?></h3>
		<div id="<?php echo $id ?>_content"></div>
  	</div>
  
</div>
<div class="md-overlay"></div>

<script type="text/javascript">

	var modal_options = {},
		modalObj,
		<?php echo $id ?>;

	modal_options.controller = '<?php echo $controller ?>';
	modal_options.modal_id   = '<?php echo $modal_id ?>';
	modal_options.method     = '<?php echo $method ?>';

	<?php 
		if( !EMPTY( $ajax ) ) :
	?>

		modal_options.ajax 	 = '<?php echo $ajax ?>'

	<?php endif; ?>

	<?php 
		if( !EMPTY( $module ) ) :
	?>
			modal_options.module = '<?php echo $module ?>';
	<?php endif; ?>

	<?php
		if( !EMPTY( $height ) ) :
	?>
		modal_options.height 	= '<?php echo $height ?>';
	<?php endif; ?>
	
	<?php 
		if( !EMPTY( $multiple ) ) :
	?>

		<?php echo $id ?> = new handleModal( modal_options );

		function <?php echo $id.'_init' ?>( data_id )
		{
			var data_id  = data_id || '',
				options  = {};

			options.id 	 	= data_id;
		
			<?php if( !EMPTY( $height ) ) : ?>
				<?php echo $id ?>.loadViewJscroll( options );
			<?php else : ?>
				<?php echo $id ?>.loadView( options );
			<?php endif; ?>

	 	}

	<?php 
		else :
	?>
			modalObj 		= new handleModal( modal_options );
	<?php 
		endif;
	?>

	// var modalObj = new handleModal({ controller : 'bas_line_items', modal_id: 'modal_account_code', method: 'account_code_modal', module: '<?php echo PROJECT_BAS ?>' });
</script>
<?php 
		endforeach;
	endif;
?>
<script type="text/javascript">

  $(function(){

	/* START: Doc's code */
  	<?php 
  		if( ISSET( $loaded_init ) AND !EMPTY( $loaded_init ) ) :
  			foreach( $loaded_init as $init ) :
  	?>
  			<?php echo $init; ?>
  	<?php 
  			endforeach;
  		endif; 
  	?>
  	/* END: Doc's code */

	$('.scrollspy').scrollSpy();
	$('.tooltipped').tooltip('remove');
	$('.tooltipped').tooltip({delay: 50});

	// JSCROLLPANE
	<?php if(in_array('materialize', $load_js)){ ?>
	// DROPDOWN
	  $('.dropdown-button').dropdown({
			inDuration: 300,
			outDuration: 225,
			constrain_width: false, // Does not change width of dropdown to that of the activator
			hover: false, // Activate on hover
			gutter: 0, // Spacing from edge
			belowOrigin: true, // Displays dropdown below the button
			alignment: 'left' // Displays dropdown with edge aligned to the left of button
	  });
	<?php } ?>
	
	<?php if(in_array('jquery.jscrollpane', $load_js)){ ?>
	$('.scroll-pane').jScrollPane(settings).bind(
		'mousewheel',
		function(e)
		{
			e.preventDefault();
		}
	  );
	<?php } ?>
		
	// DATEPICKER
	<?php if(in_array('jquery.datetimepicker', $load_js)){ ?>

		datepicker_init();
	  
	<?php } else { ?>
	  // DESTROY DATEPICKER TO HIDE xdsoft_datetimepicker
   	/* 	if($('.datepicker,.datepicker_start,.datepicker_end,.timepicker').length === 0)
			$('.datepicker,.datepicker_start,.datepicker_end,.timepicker').datetimepicker('destroy');  */
	<?php } ?>
	
	// SELECTIZE
	<?php if(in_array('selectize', $load_js)){ ?>
		selectize_init();
	  
	  // ASSIGN VALUES TO SELECT (SINGLE)
	  <?php 
		if(ISSET($single)){
			foreach ($single as $k => $v): 
	  ?>
				$("#<?php echo $k ?>")[0].selectize.setValue('<?php echo $v ?>');
	  <?php 
			endforeach; 
		} 
	  ?>
	  
	  // ASSIGN VALUES TO SELECT (MULTIPLE)
	  <?php 
		if(ISSET($multiple)){
			foreach ($multiple as $a => $b):
				if(!EMPTY($b)){ 
					for($i=0; $i<count($b); $i++){	
	  ?>
						$("#<?php echo $a ?>")[0].selectize.addItem("<?php echo $b[$i] ?>");
	  <?php 		}
				}
			endforeach;	 
		}
	} ?>
	
	// LABELAUTY RADIO BUTTON
	<?php if(in_array('jquery-labelauty', $load_js)){ ?>
		$(".labelauty").labelauty({
			checked_label: "",
			unchecked_label: "",
		});
	<?php } ?>
	
	// NUMBER FORMAT
	<?php if(in_array('jquery.number.min', $load_js)){ ?>
		$('input.number').number(true, 2);
	<?php } ?>
	
	// UPLOAD FILE
	<?php 
	  if(in_array('jquery.uploadfile', $load_js)){ 
		foreach($upload as $upload):
	?>
	
		<?php if(ISSET($upload['page'])){?>
			var page = "<?php echo $upload['page'] ?>";
		<?php } ?>
		
		<?php if(ISSET($upload['form_name'])){?>
			var form_name = "<?php echo $upload['form_name'] ?>";
		<?php } ?>
		
		<?php if(ISSET($upload['multiple'])){?>
			var multiple = true;
		<?php } ?>
			
		<?php if(ISSET($upload['max_file'])){?>
			var max_file = <?php echo $upload['max_file'] ?>;
		<?php } ?>
		
		<?php if(ISSET($upload['show_progress'])){?>
			var show_progress = true;
		<?php } ?>
			
		<?php if(ISSET($upload['drag_drop'])){?>
			var drag_drop = true;
		<?php } ?>
			
		<?php if(ISSET($upload['show_preview'])){ ?>
			var show_preview = true;
			
			<?php if(ISSET($upload['preview_height'])){ ?>
				var preview_height = "<?php echo $upload['preview_height'] ?>";
			<?php } ?>
			
			<?php if(ISSET($upload['preview_width'])){ ?>
				var preview_width = "<?php echo $upload['preview_width'] ?>";
			<?php } ?>
		<?php } ?>
			
		var page = page || "",
			multiple = multiple || false,
			max_file = max_file || 1,
			show_progress = show_progress || false,
			drag_drop = drag_drop || false,
			show_preview = show_preview || false,
			preview_height = preview_height || "60px",
			preview_width = preview_width || "60px";
		
		var uploadObj = $("#<?php echo $upload['id']?>_upload").uploadFile({
			url: $base_url + "upload/",
			fileName: "file",
			allowedTypes:"<?php echo $upload['allowed_types']?>",
			acceptFiles:"*",	
			dragDrop: drag_drop,
			multiple: multiple,
			<?php if(ISSET($upload['max_file'])){?>
				maxFileCount: max_file,
			<?php } ?>
			allowDuplicates: true,
			duplicateStrict: false,
			showDone: false,
			showProgress: show_progress,
			showPreview: show_preview,
			<?php if(ISSET($upload['show_preview'])){?>
				previewHeight: preview_height,
				previewWidth: preview_width,
			<?php } ?>
			returnType:"json",	
			formData: {"dir":"<?php echo $upload['path']?>"},
			uploadFolder:$base_url + "<?php echo $upload['path']?>",
			onSuccess:function(files,data,xhr){
				
				switch(page){
					case 'site_settings':
						var avatar = $base_url + "<?php echo $upload['path']?>" + data;
						$("#<?php echo $upload['id']?>_src").attr("src", avatar);
						
						$("#<?php echo $upload['id']?>").val(data);
						$("#<?php echo $upload['id']?>_upload").prev(".ajax-file-upload").hide();
						$("#<?php echo $upload['id']?>_upload + div + div.ajax-file-upload-statusbar .ajax-file-upload-red").text("Delete");
					break;
					case 'files':
						$( ".field-multi-attachment .ajax-file-upload-filename" ).each(function() {
							var html = $(this).html();
							
							if(html == files){
								// EXPLODE VALUE OF DATA
								var id = data.toString().split('.');
								$('<input/>').attr({type:'hidden',name:'multiple_file_name[]', value:data, id: id[0]}).appendTo('#' + form_name);
								
								$("#save_upload_file").prop("disabled", false);
							}
						});
					break;
					case 'file_version':
						$("#<?php echo $upload['id']?>").val(data);
						console.log('<?php echo $upload['id']?>' + data);
						
						$("#save_upload_file_version").prop("disabled", false);
						$("#<?php echo $upload['id']?>_upload").prev(".ajax-file-upload").hide();
					break;
					case 'aip':
						$("#<?php echo $upload['id']?>").val(data);
					break;
				}
				
			},
			showDelete:true,
			deleteCallback: function(data,pd)
			{
				for(var i=0;i<data.length;i++)
				{
					$.post($base_url + "upload/delete/",{op:"delete",name:data[i],dir:"<?php echo $upload['path']?>"},
					function(resp, textStatus, jqXHR)
					{ 
						switch(page){
							case 'site_settings':
								$("#<?php echo $upload['id']?>_upload + div + div.ajax-file-upload-statusbar .ajax-file-upload-error").fadeOut();	
								$("#<?php echo $upload['id']?>").val(''); 
								
								<?php if(ISSET($upload['default_img_preview'])){ ?>
									var avatar = $base_url + "<?php echo PATH_IMAGES . $upload['default_img_preview'] ?>";
									$("#<?php echo $upload['id']?>_src").attr("src", avatar);
								<?php } ?>
								
								$("#<?php echo $upload['id']?>_upload").prev(".ajax-file-upload").show();
							break;
							case 'files':
								var id = data.toString().split('.');
								
								$("#"+ form_name +" input[id='"+ id[0] +"']").remove();
								$(".ajax-file-upload-error").fadeOut();
								
								var input_ctr = $('input[name="multiple_file_name[]"]').length;
								if(input_ctr == 0)
									$("#save_upload_file").prop("disabled", true);
							break;
							case 'file_version':
								$("#save_upload_file_version").prop("disabled", true);
								
						$("#<?php echo $upload['id']?>_upload").prev(".ajax-file-upload").show();
							break;
						}
					});
				}
				pd.statusbar.hide();
					
			},
			onLoad:function(obj)
			{
				$.ajax({
					cache: true,
					url: $base_url + "upload/existing_files/",
					dataType: "json",
					data: { dir: '<?php echo $upload['path']?>', file: $("#<?php echo $upload['id']?>").val()} ,
					success: function(data) 
					{
						for(var i=0;i<data.length;i++)
						{
							obj.createProgress(data[i]);
						}	
						
						if(data.length > 0){
						  $("#<?php echo $upload['id']?>_upload").prev(".ajax-file-upload").hide();
						  $("#<?php echo $upload['id']?>_upload + div + div.ajax-file-upload-statusbar .ajax-file-upload-red").text("Delete");
						} else {
						  <?php if(ISSET($upload['default_img_preview'])){ ?>
							var avatar = $base_url + "<?php echo PATH_IMAGES . $upload['default_img_preview'] ?>";
							$("#<?php echo $upload['id']?>_src").attr("src", avatar);
						  <?php } ?>
						}
					}
				});
			}
		});
	<?php 
	  endforeach;
	} ?>
	
	// DATATABLE
	<?php 
	  if(in_array('jquery.dataTables.min', $load_js) AND ISSET($datatable))
	  { 

	  	foreach($datatable as $dtable)
	  	{
	  		$datatable 	= $dtable["datatable"];	
	  		$scroll 	= ISSET($dtable["scroll"]) ? "100%" : "";

	  		echo "load_datatable('".$dtable["table_id"]."', '".$dtable["path"]."', '".$scroll."');";
	  	}
	  }				
	?>

	// DATATABLE2
	// Same as DATATABLE but have a multi-configuration
	<?php
		 if(in_array('jquery.dataTables.min', $load_js) AND ISSET($datatable2)){
	  		foreach($datatable2 as $dtable){
	  			$config 	= json_encode($dtable);
	  			echo "load_datatable2('".$config."');";
	  		}
		}				
	?>
	
  });


	<?php
		if(  ISSET( $load_init ) AND !EMPTY( $load_init )  )
		{
       		foreach($load_init as $init)
       		{
       			echo $init;
       		}        		
       	}
  	?>
</script> 