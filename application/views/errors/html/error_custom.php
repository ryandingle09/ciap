<?php 
$display_title = ( ISSET($title) || ! EMPTY($title)) ? $title : 'oops. invalid access.';	
$display_msg   = ( ISSET($message) || ! EMPTY($message)) ? $message : 'please contact system administrator.';
?>


<ul class="collapsible b-n" data-collapsible="expandable">
	<div class="collapsible-body row b-n" style="display: block">
	
		<div class="z-depth-4" style="width:93%;margin-left:5%;">
			<div class="card orange darken-3">
			      <div class="card-content white-text font-semibold">
			      	   <span class="card-title font-semibold"><?php echo strtoupper($display_title); ?></span>
			      	   <p><?php echo strtoupper($display_msg); ?></p>
				  </div>
			</div>
		</div>
	
	</div>
</ul>
