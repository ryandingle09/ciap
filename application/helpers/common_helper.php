<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
|--------------------------------------------------------------------------
| List of Useful Generic Functions
|--------------------------------------------------------------------------
*/

	/*
	|----------------------------------------------------------------------
	| Get Parameters from $_GET, $_POST, and $_FILES Function
	|----------------------------------------------------------------------
	| Note: Because almost all method that connected to AJAX is using this 
	| funtion, XSS is auto enabled. XSS or Cross Site Scripting Hack
	| prevention filter which can either run automatically to filter all
	| POST and COOKIE data that is encountered, or you can run it on a per
	| item basis
	|
	| @return array
	*/

	function get_params($xss = TRUE)
	{
		$CI =& get_instance();

		$get = $CI->input->get(NULL, $xss) ? $CI->input->get(NULL, $xss) : array();
		$post = $CI->input->post(NULL, $xss) ? $CI->input->post(NULL, $xss) : array();
		$params = array_merge(array_map('_secure_param', array_merge($get, $post)), $_FILES);

		return $params;
	}

	function _secure_param($value)
	{
		if (is_array($value)) {
			return array_map('_secure_param', $value);
		} else {
			return urldecode(trim($value));
		}
	}

	/*
	|----------------------------------------------------------------------
	| Get Settings Function
	|----------------------------------------------------------------------
	| Get a specific site setting detail
	|
	| @param string $type
	| @param string $name
	|
	| @return string
	*/
	
	function get_setting($type, $name)
	{
		$CI =& get_instance();
		$CI->load->model("settings_model");
		
		$setting = $CI->settings_model->get_specific_setting($type, $name);
		
		return $setting["setting_value"];
	}

	/*
	|----------------------------------------------------------------------
	| Generate Salt Function
	|----------------------------------------------------------------------
	| @param boolean $high_risk	: set to TRUE if the value being hashed 
	| 							  needs to be more secured (e.g. password) 
	|
	| @return string
	*/
	
	function gen_salt($high_risk = FALSE) {
		if($high_risk){
			return hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE));
		} else {
			return substr(hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), TRUE)), 0, SALT_LENGTH);
		}	
	}

	/*
	|----------------------------------------------------------------------
	| Generate Token Function
	|----------------------------------------------------------------------
	| @param string $id			: specifies the value of the id
	| @param string $salt		: specifies the value of the salt
	| @param boolean $high_risk	: set to TRUE if the value being hashed 
	| 							  needs to be more secured (e.g. password)
	|
	| @return string
	*/
	
	function in_salt($id, $salt, $high_risk = FALSE) {
		if($high_risk){
			return hash('sha512', sha1($id) . $salt);
		} else {
			return substr(hash('sha512', sha1($id) . $salt), 0, SALT_LENGTH);
		}	
	}
	
	/*
	|----------------------------------------------------------------------
	| Check Salt Function
	|----------------------------------------------------------------------
	| Check if the token or salt were not maliciously changed
	|
	| @param string $id
	| @param string $salt
	| @param string $token
	*/
	
	function check_salt($id, $salt, $token, $throw=TRUE) {
		$CI =& get_instance();

		if($token != in_salt($id, $salt))
		{
			if($throw)
				throw new Exception($CI->lang->line('invalid_action'));
			else 
				return FALSE;
		}
		
		return TRUE;
	}
	
	/*
	|----------------------------------------------------------------------
	| URL Encode Function
	|----------------------------------------------------------------------
	| Encodes a string to safely pass values in the URL
	| 
	| @param string $input
	| 
	| return string
	*/
	
	function base64_url_encode($input)
	{
		$CI =& get_instance();
		
		return strtr($CI->encryption->encrypt($input), '+/=', '.~-');
	}

	/*
	|----------------------------------------------------------------------
	| URL Decode Function
	|----------------------------------------------------------------------
	| Decodes any encoded values in the given string
	|
	| @param string $input
	| 
	| return string
	*/

	function base64_url_decode($input)
	{
		$CI =& get_instance();
		
	
		return $CI->encryption->decrypt(strtr($input, '.~-', '+/='));
	}
	
	/*
	|----------------------------------------------------------------------
	| Get Values Function
	|----------------------------------------------------------------------
	| Function for queries that needs dependency. This will run the query 
	| from a particular model without the need of a controller
	|
	| @param string $model		: name of the model where the query is 
	| 							  needed to be executed
	| @param string $function	: name of the function located in the 
	| 							  specified model
	| @param array $params		: array of values needed by the query for 
	| 							  dependency
	| @param string $module		: name of the project where the function is
	| 							  located
	| 
	| @return array
	*/
	
	function get_values($model, $function, $params = array(), $module = NULL)
	{
		$mod = (!IS_NULL($module))? $module."/".$model : $model;
		$CI =& get_instance();
		
		if(!$CI->load->is_model_loaded($mod))
			$CI->load->model($mod);
		
		try{
			
			$values = $CI->$model->$function($params);
		
			return $values;
		}
		catch(PDOException $e)
		{
			throw new PDOException($e->getMessage());
		}
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}		
	}
	
	/*
	|----------------------------------------------------------------------
	| Time Ago Function
	|----------------------------------------------------------------------
	| Used for comments and other form of communication to tell the time 
	| in seconds/minutes/hours/days/months/years/decades ago instead of the
	| exact time which might not be correct to some in another time zone
	| 
	| @param datetime $time		: specifies the date/time to be converted
	| @param int $ago			: set to 1 to append "ago" in the returned 
	| 							  value
	| @param int $period_only	: set to 1 to exclude date in the returned 
	| 							  value
	| 
	| @return string
	*/
	
	function get_date_format($time, $ago = 0, $period_only = 0)
	{
		$date = $prefix = $suffix = "";
		$convert_time = strtotime($time);
		$month_total_days = date("t");
		$periods = array("second", "minute", "hour", "day", "month", "year", "decade");
		$lengths = array("60","60","24",$month_total_days,"12","10");

		$now = time();

		IF($ago){
		   $difference = $now - $convert_time;
		   $suffix = " ago";
		}else{
		   $difference = $convert_time - $now;
		   $prefix = "In ";	
		}

		for($j = 0; $difference >= $lengths[$j] && $j < count($lengths)-1; $j++) {
		   $difference /= $lengths[$j];
		}

		$difference = floor($difference);

		if($difference > 1)
			$periods[$j].= "s";
		
		switch($periods[$j]){
			/* IF CURRENT PERIOD IS DAY */
			case $periods[3]:
				if($difference > 7)
					$date = date(', D M jS', strtotime($time));
			break;
			
			/* IF CURRENT PERIOD IS MONTH */
			case $periods[4]:
				$date = date(', D M jS', strtotime($time));
				$difference = ($difference == 1) ? "a" : $difference;
			break;
		}
		
		$time_ago = ($difference == 0 && is_numeric($difference)) ? "Just Now" : $prefix . $difference . ' ' . $periods[$j] . $suffix;
		$time_ago = ($period_only) ? $time_ago : $time_ago . $date;

		return $time_ago;
	}
	
	/*
	|----------------------------------------------------------------------
	| Generate Years Dropdown Function
	|----------------------------------------------------------------------
	| @Create dropdown of years
	| 
	| @param int $start_year	: specifies the year when the list will start
	| @param int $end_year		: specifies the year when the list will end
	| @param string $id			: the name and id of the select object
	| @param int $selected		: the value to be selected from the dropdown
	| 
	| @return string
	*/

	function create_years($start_year, $end_year, $id = 'year_select', $selected = NULL)
	{

		/* CURRENT YEAR */
		$selected = is_null($selected) ? date('Y') : $selected;

		/* RANGE OF YEARS */
		$r = range($start_year, $end_year);

		/* CREATE SELECT OBJECT */
		$select = '<select name="'.$id.'" id="'.$id.'" class="selectize">';
		foreach( $r as $year )
		{
			$select .= '<option value="'.$year.'"';
			$select .= ($year==$selected) ? ' selected="selected"' : '';
			$select .= '>'.$year.'</option>\n';
		}
		$select .= '</select>';
		
		return $select;
	}
	
	/*
	|----------------------------------------------------------------------
	| Generate Months Dropdown Function
	|----------------------------------------------------------------------
	| @Create dropdown list of months
	| 
	| @param string $id		: the name and id of the select object
	| @param int $selected	: the value to be selected from the dropdown
	| @param boolean $all	: set to FALSE to remove 'ALL' option from the 
	| 						  dropdown list
	| 
	| @return string
	*/

	function create_months($id = 'month_select', $selected = NULL, $all = TRUE)
	{
		/* ARRAY OF MONTHS */
		$months = array(
				1=>'January',
				2=>'February',
				3=>'March',
				4=>'April',
				5=>'May',
				6=>'June',
				7=>'July',
				8=>'August',
				9=>'September',
				10=>'October',
				11=>'November',
				12=>'December');

		/* CURRENT MONTH */
		$selected = is_null($selected) ? date('m') : $selected;
		
		/* CREATE SELECT OBJECT */
		$select = '<select name="'.$id.'" id="'.$id.'" class="selectize">\n';
		
		if($all)
			$select .= '<option value="0">All</option>\n';
		
		foreach($months as $key=>$mon)
		{
			$select .= '<option value="'.$key.'"';
			$select .= ($key==$selected) ? ' selected="selected"' : '';
			$select .= '>'.$mon.'</option>\n';
		}
		$select .= '</select>';
		
		return $select;
	}
	
	/*
	|----------------------------------------------------------------------
	| Generate Days Dropdown Function
	|----------------------------------------------------------------------
	| @Create dropdown list of days
	| @param int $month		: specifies the month in the selected calendar
	| @param int $month		: specifies the year in the selected calendar
	| @param string $id		: the name and id of the select object
	| @param int $selected	: the value to be selected from the dropdown
	| 
	| @return string
	*/
	
	function create_days($month, $year, $id = 'day_select', $selected = NULL)
	{
		$date_range = cal_days_in_month(CAL_GREGORIAN, $month, $year);
		
		/* RANGE OF DAYS */
		$r = range(1, $date_range);

		/* CURRENT DAY */
		$selected = is_null($selected) ? date('d') : $selected;
		
		/* CREATE SELECT OBJECT */
		$select = '<select name="'.$id.'" class="selectize" id="'.$id.'">\n';
		foreach ($r as $day)
		{
			$select .= '<option value="'.$day.'"';
			$select .= ($day==$selected) ? ' selected="selected"' : '';
			$select .= '>'.$day.'</option>\n';
		}
		$select .= '</select>';
		
		return $select;
	}
	
	function get_pass_error_msg()
	{
		$CI =& get_instance();
		
		if(!$CI->load->is_model_loaded('settings_model'))
			$CI->load->model('settings_model');
		
		return $CI->settings_model->get_pass_error_msg();
		
	}

	function get_avail_app_group()
	{
		$apps 	   = get_app_by_role();
		
		$app_group = array();
	
		foreach($apps as $a):
		$app_group[] = $a['app_group'];
		endforeach;
	
		return $app_group;
	}
	
	
	function get_app_by_role()
	{
		$CI =& get_instance();
		$CI->load->model('permission_model', 'perm', TRUE);
		
		$user_role_codes	= $CI->session->userdata('user_roles');
		
	
		$app = array();
		
		if(empty($user_role_codes))
		{
			return $app;
		}

		$check_app = array();
		foreach($user_role_codes as $role)
		{
			$result = $CI->perm->get_application_by_role($role);
			
			if(!empty($result)){
				foreach ($result as $result){				
					if(!in_array($result['application_code'], $check_app)){
						$app[] = array(
							'application_code'  => $result['application_code'],
							'app_group'			=> $result['app_group'],
							'url' 				=> $result['url']
						);
						
						$check_app[] = $result['application_code'];
					}			
				}	
			}
		}

		return $app; 
	}

	function get_menu($main_flag=1, $parent_menu=NULL)
	{
		$CI =& get_instance();
		
		$CI->load->model('permission_model', 'perm', TRUE);
		
		$app_code = $CI->session->userdata('default_application_code');


		
		if($CI->session->userdata('selected_application'))
		{
			$app_code = $CI->session->userdata('selected_application');
		}
		
		$user_role_codes	= $CI->session->userdata('user_roles');
		

		
		$menu		= array();
		
		if(empty($user_role_codes))
		{
			return $menu;
		}
		
		foreach($user_role_codes as $role)
		{
			
			$result = $CI->perm->get_resource_by_role($app_code, $role, $main_flag, $parent_menu);
			
			if(!empty($result)){
				foreach ($result as $resource){				
					if(!in_array($resource['resource_code'], $menu)){
						$menu[] = $resource['resource_code'];
					}			
				}
			}
		}
		
		
		
		$resource = array();
		if(!empty($menu)){		
			$resource = $CI->perm->get_resource_by_resource_code($menu);		
		}
		
		return $resource;

	}
	
	
	function convert_object_to_array($d) {
		if (is_object($d))
			$d = get_object_vars($d);
			return is_array($d) ? array_map(__METHOD__, $d) : $d;
	}
	
	function decimal_format($value) {
		return number_format($value, 2, '.', ',');
	}

	/**
	 *Format Capitalization
	 *
	 * @param string $id
	 * @param string $salt
	 * @param string $token
	 * @return
	 */
	function _ucfirst($string)
	{
		if(!EMPTY($string))
		{
			return ucfirst(mb_strtolower($string));
		}
	}
		
	function is_valid_number($number)
	{
		$trimmed_number = str_replace(",", "", $number);
		
		return (is_numeric($trimmed_number)) ? TRUE : FALSE;
	}
	
	function string_min($string, $len=50)
	{
		if(strlen($string) > $len):
			$formatted = substr($string, 0, $len).'...'; 
		else:
			$formatted = $string;
		endif;
		
		return $formatted;
	}
	
	function strtocontroller($string, $len=100)
	{
		return strtolower(str_replace('-', '_', $string));
	}
	
	
	function number_to_words($number){
		
		$obj = new NumberFormatter('en', NumberFormatter::SPELLOUT);
		
		return  $obj->format($number);
	}