<div class="page-title">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#">User Management</a></li>
	<li><a href="#" class="active">Roles</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Roles
		<span>Manage roles that can be assigned to a user.  Every role can have different privileges.</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    <button class="waves-effect waves-light" type="button" onclick="load_datatable('role_table','<?php echo PROJECT_CORE ?>/roles/get_role_list')"><i class="flaticon-arrows97"></i> Refresh</button>
	  </div>
	  
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_roles" id="add_role" name="add_role" onclick="modal_init()">Add New Role</button>
	  </div>
	</div>
  </div>
</div>

<div class="m-t-lg">
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="role_table">
  <thead>
	<tr>
	  <th width="20%">Code</th>
	  <th width="28%">Name</th>
	  <th width="10%">Built In</th>
	  <th width="30%">Assigned System/s</th>
	  <th width="12%" class="text-center">Actions</th>
	</tr>
  </thead>
  </table>
</div>

<!-- Modal -->
<div id="modal_roles" class="md-modal md-effect-<?php echo MODAL_EFFECT ?>">
  <div class="md-content">
	<a class="md-close icon">&times;</a>
	<h3 class="md-header">Role</h3>
	<div id="modal_roles_content"></div>
  </div>
</div>
<div class="md-overlay"></div>

<script type="text/javascript">
var modalObj = new handleModal({ controller : 'roles', modal_id: 'modal_roles', module: '<?php echo PROJECT_CORE ?>' });
	deleteObj = new handleData({ controller : 'roles', method : 'delete_role', module: '<?php echo PROJECT_CORE ?>' });
	updateObj = new handleData({ controller : 'roles', method : 'process' });
	
$(function(){	
	$("#cancel_role").on("click", function(){
		modalObj.closeModal();
	});
});
</script>