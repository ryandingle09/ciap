<?php $disabled = (EMPTY($process_id))? "disabled" : ""; ?>

<div class="page-title">
  <ul id="breadcrumbs">
	<li><a href="<?php echo base_url().PROJECT_CORE.'/dashboard' ?>">Home</a></li>
	<li><a href="<?php echo base_url().PROJECT_CORE.'/manage_workflow' ?>">Workflow Management</a></li>
	<li><a href="#" class="active">Create Workflow</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s12 p-r-n">
	  <h5>Create Workflow
		<span>Create a dynamic workflow by controlling its process, stages, steps and actions.</span>
	  </h5>
	</div>
  </div>
</div>

<div class="tabs-wrapper">
  <div>
    <ul class="tabs">
	  <li class="tab col s3"><a class="active" href="#tab_workflow_process" onclick="load_index('tab_workflow_process', 'workflow_process/tab/<?php echo $process_id ?>', '<?php echo PROJECT_CORE ?>')">Process</a></li>
	  <li class="tab col s3 <?php echo $disabled ?>"><a <?php if(!EMPTY($process_id)){ ?>href="#tab_workflow_stages" onclick="load_index('tab_workflow_stages', 'workflow_stages/tab/<?php echo $process_id ?>', '<?php echo PROJECT_CORE ?>')"<?php } ?>>Stages</a></li>
	  <li class="tab col s3 <?php echo $disabled ?>"><a <?php if(!EMPTY($process_id)){ ?>href="#tab_workflow_steps" onclick="load_index('tab_workflow_steps', 'workflow_steps/tab/<?php echo $process_id ?>', '<?php echo PROJECT_CORE ?>')"<?php } ?>>Steps</a></li>
	  <li class="tab col s3 <?php echo $disabled ?>"><a <?php if(!EMPTY($process_id)){ ?>href="#tab_workflow_actions" onclick="load_index('tab_workflow_actions', 'workflow_actions/tab/<?php echo $process_id ?>', '<?php echo PROJECT_CORE ?>')"<?php } ?>>Actions</a></li>
    </ul>
  </div>
</div>

  <div id="tab_workflow_process" class="tab-content col s12"></div>
  <div id="tab_workflow_stages" class="tab-content col s12"></div>
  <div id="tab_workflow_steps" class="tab-content col s12"></div>
  <div id="tab_workflow_actions" class="tab-content col s12"></div>
  
<script type="text/javascript">
$(function(){
	var path = "<?php echo base_url().PROJECT_CORE ?>/workflow_process/tab/<?php echo $process_id ?>";
	$("#tab_workflow_process").load(path).fadeIn("slow").show();
});
</script>