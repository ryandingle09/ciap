<div class="page-title">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#">User Management</a></li>
	<li><a href="#" class="active">Users</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Users
		<span>Manage system user accounts</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    <button class="waves-effect waves-light" type="button" onclick="window.location.reload()"><i class="flaticon-arrows97"></i> Refresh</button>
	  </div>
	  
	  <div class="input-field inline p-l-md">
	    <button class="btn waves-effect waves-light btn-success" name="add_user" id="add_user" type="button" onclick="content_form('users/form', '<?php echo PROJECT_CORE ?>')">Add New User</button>
	  </div>
	</div>
  </div>
</div>

<div class="m-t-lg">
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="user_table">
  <thead>
	<tr>
	  <th width="20%" style="padding-left:55px!important;">Username</th>
	  <th width="20%">First Name</th>
	  <th width="20%">Last Name</th>
	  <th width="20%">Email</th>
	  <th width="10%">Status</th>
	  <th width="10%" class="center-align">Actions</th>
	</tr>
  </thead>
  </table>
</div>

<script type="text/javascript">
  var deleteObj = new handleData({ controller : 'users', method : 'delete_user', module: '<?php echo PROJECT_CORE ?>'});
</script>