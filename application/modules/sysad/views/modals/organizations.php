<?php
$id       = (ISSET($id)) ? $id : '';
$salt     = (ISSET($salt)) ? $salt : '';
$token    = (ISSET($token)) ? $token : '';
$org_code = (ISSET($org_details['org_code']) && ! EMPTY($org_details['org_code'])) ? $org_details['org_code'] : '';
$disabled = (ISSET($org_details['org_code']) && ! EMPTY($org_details['org_code'])) ? 'disabled' : '';
$org_name = (ISSET($org_details['name']) && ! EMPTY($org_details['name'])) ? $org_details['name'] : '';
$website  = (ISSET($org_details['website']) && ! EMPTY($org_details['website'])) ? $org_details['website'] : '';
$email    = (ISSET($org_details['email']) && ! EMPTY($org_details['email'])) ? $org_details['email'] : '';
$phone    = (ISSET($org_details['phone']) && ! EMPTY($org_details['phone'])) ? $org_details['phone'] : '';
$fax      = (ISSET($org_details['fax']) && ! EMPTY($org_details['fax'])) ? $org_details['fax'] : '';
$org_parent_code = (ISSET($org_details['org_parent']) && ! EMPTY($org_details['org_parent'])) ? $org_details['org_parent'] : '';
$org_short_name  = (ISSET($org_details['short_name']) && ! EMPTY($org_details['short_name'])) ? $org_details['short_name'] : '';
?>
<form id="organization_form">
	<input type="hidden" name="id" id="id" value="<?php echo $id; ?>"/>
	<input type="hidden" name="salt"  id="salt" value="<?php echo $salt; ?>"/>
	<input type="hidden" name="token" id="token" value="<?php echo $token; ?>"/>
	
	<div class="form-float-label">

		<div class="row m-n">
			<div class="col s6">
				<div class="input-field">
					
					<input type="text" data-parsley-required="true" name="org_code"  id="org_code" value="<?php echo $org_code; ?>" <?php echo $disabled ?>  />
					<label for="org_code" class="active">Organization Code</label> 
				</div>
			</div>
			<div class="col s6">
				<div class="input-field">
					<label for="org_short_name" class="active">Organization Short Name</label>
					<input type="text" data-parsley-required="true"  name="org_short_name"  id="org_short_name" value="<?php echo $org_short_name; ?>" /> 
			  	</div>
			</div>
		</div>
	
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<label for="org_name" class="active">Organization Name</label>
					<input type="text" data-parsley-required="true" name="org_name"  id="org_name" value="<?php echo $org_name; ?>"  /> 
				</div>
			</div>
		</div>
			
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<label for="parent_org_code" class="active">Parent Organization</label>
					<select name="parent_org_code" id="parent_org_code" class="selectize"  placeholder="Select sector">
								<option value=""></option>
							<?php foreach($other_orgs as $key => $val): ?>
								<option value="<?php echo $val['value']; ?>"><?php echo $val['text']; ?></option>
							<?php endforeach; ?>
					</select> 
			  	</div>
			</div>
		</div>	
		
		<div class="row m-n">
			<div class="col s6">
				<div class="input-field">
					<label for="website" class="active">Website</label>
					<input type="text" data-parsley-required="true" name="website"  id="website" value="<?php echo $website; ?>"  /> 
			  	</div>
			</div>
			<div class="col s6">
				<div class="input-field">
					<label for="email" class="active">Email</label>
					<input type="email" data-parsley-type="email" data-parsley-required="true" name="email"  id="email" value="<?php echo $email; ?>"  /> 
			  	</div>
			</div>
		</div>	
			
		<div class="row m-n">
			<div class="col s6">
				<div class="input-field">
					<label for="tel_no" class="active">Telephone No.</label>
					<input type="text" data-parsley-type="integer" data-parsley-required="true" name="tel_no"  id="tel_no" value="<?php echo $phone; ?>"  /> 
			  	</div>
			</div>
			<div class="col s6">
				<div class="input-field">
					<label for="fax_no" class="active">Fax No.</label>
					<input type="text" data-parsley-type="integer" data-parsley-required="true" name="fax_no"  id="fax_no" value="<?php echo $fax; ?>"  /> 
			  	</div>
			</div>
		</div>		
			
	</div>
<!-- end of form-float-label -->
	<div class="md-footer default">
		<button type="submit" class="btn waves-effect waves-light" id="save_organization" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
		<a class="waves-effect waves-teal btn-flat" id="cancel_organization">Cancel</a>
  	</div>
<!-- end of md-footer -->  	
</form>

<script>
$(function(){
	$('.selectize').not(".filter").selectize();
	var $org     = $("#parent_org_code")[0].selectize;
		$parsley = $("#organization_form").parsley();
		$org.clear();

	<?php if( ISSET($org_parent_code)) : ?>
		$("#parent_org_code")[0].selectize.setValue('<?php echo $org_parent_code; ?>');
	<?php endif; ?>	
		
	$('#organization_form').on('submit', function(ev){
		ev.preventDefault();
		
		$parsley.validate();
		
		if($parsley.isValid()){
			button_loader('save_organization', 1);
			
			var form_data = $(this).serialize();
			$.ajax({
				url      : '<?php echo base_url() . PROJECT_CORE ?>/organizations/save',
				data     : form_data,		
				dataType : 'json',
				method   : 'POST',
				success  : function(response){
					var notif_type = (response.status) ?  "<?php echo SUCCESS ?>" : "<?php echo ERROR ?>";

					notification_msg(notif_type, response.msg);
					
					if(response.status){
						modalObj.closeModal();
					    load_datatable(response.table_id, response.path, "");
					}

					button_loader("save_organization",0);
				},
				error   : function(jqXHR, textStatus, errorThrown){
					console.log('Error : '+textStatus);
				}
			}); 
			
		}	
		
	});
		
});
</script>