<div class="page-title">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Audit Trail</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Audit Trail
		<span>Keep track of all activities in your system.</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="row m-b-n">
	    <div class="col s7 p-t-md">
		  <div class="btn-group">
			<button class="waves-effect waves-light" type="button"><i class="flaticon-arrows97"></i> Refresh</button>
			<button type="button" class="waves-effect waves-light dropdown-button"  data-beloworigin="false" data-activates="dropdown-download-to"><i class="flaticon-inbox36"></i> Download</button>
		  </div>
	  
		  <!-- Print To Dropdown Structure -->
		  <div id="dropdown-download-to" class="dropdown-content">
			<ul class="collection actions">
			  <li class="collection-item"><a href="#!" class="collection-link pdf" onclick="$('#role_table').tableExport({type:'pdf',escape:'false',ignoreColumn: [4], pdfFontSize:'7'})">PDF</a></li>
			  <li class="collection-item"><a href="#!" class="collection-link xlsx" onclick="$('#role_table').tableExport({type:'excel',escape:'false',ignoreColumn: [4]})">Excel</a></li>
			  <li class="collection-item"><a href="#!" class="collection-link docx" onclick="$('#role_table').tableExport({type:'doc',escape:'false',ignoreColumn: [4]})">Word&nbsp;Document</a></li>
			</ul>
		  </div>
		  <!-- End Print To Dropdown Structure -->
		</div>
		
		<div class="col s5 left-align">
		  <label>Filter by System</label>
		  <select name="filter_audit_log" id="filter_audit_log" class="selectize" placeholder="Select System">
		    <option value=""></option>
		    <option value="0">All</option>
			<?php foreach($systems as $system): ?>
	          <option value="<?php echo $system['system_code'] ?>"><?php echo $system['system_name'] ?></option>
			<?php endforeach; ?>
		  </select>
		</div>
	  </div>
	</div>
  </div>
</div>

<div class="m-t-lg">
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="audit_log_table">
  <thead>
	<tr>
	  <th width="25%">User</th>
	  <th width="12%">Module</th>
	  <th width="30%">Activity</th>
	  <th width="10%">Date</th>
	  <th width="15%">I.P</th>
	  <th width="8%" class="text-center">Actions</th>
	</tr>
  </thead>
  </table>
</div>

<!-- Modal -->
<div id="modal_roles" class="md-modal md-effect-<?php echo MODAL_EFFECT ?>">
  <div class="md-content">
	<a class="md-close icon">&times;</a>
	<h3 class="md-header">Roles</h3>
	<div id="modal_roles_content"></div>
	<div class="md-footer default">
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light" id="save_role" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  <?php //endif; ?>
	  <a class="waves-effect waves-teal btn-flat" id="cancel_role">Cancel</a>
	</div>
  </div>
</div>

<div class="md-modal md-effect-<?php echo MODAL_EFFECT ?> lg md-default" id="modal_audit_log">
  <div class="md-content">
	<a class="md-close icon">&times;</a>
	<h3 class="md-header">Audit Trail Details</h3>
	<div id="modal_audit_log_content"></div>
  </div>
</div>
<div class="md-overlay"></div>

<script type="text/javascript">
var modalObj = new handleModal({ controller : 'audit_log', modal_id: 'modal_audit_log', module: '<?php echo PROJECT_CORE ?>' });
	
$(function(){	
	$("#close_audit").on("click", function(){
		modalObj.closeModal();
	});
	
	<?php if(ISSET($system_code)){ ?>
	  $("#filter_audit_log").val("<?php echo $system_code ?>");
	<?php }else{ ?>
	  $("#filter_audit_log").val("0");
	<?php } ?>
	
	$("#filter_audit_log").change(function(){
		var system_code = $(this).val();
		
		if(system_code != 0){
			window.location.href = "<?php echo base_url() . PROJECT_CORE ?>/audit_log/filter/" + system_code;
		}else{
			window.location.href = "<?php echo base_url() . PROJECT_CORE ?>/audit_log/";
		}
	});
});
</script>