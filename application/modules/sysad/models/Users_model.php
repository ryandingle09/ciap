<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends SYSAD_Model {
                
	var $user_table = "users";
	var $status_table = "param_status";
	var $user_roles_table = "user_roles";
	var $org_table = "organizations";
	
	public function get_user_list($aColumns, $bColumns, $params)
	{
		try
		{
			$val = array(PENDING, APPROVED, DISAPPROVED, DELETED, ANONYMOUS_ID);
			
			$cColumns = array("A.user_id", "A.nickname", "A.email", "B.status", "CONCAT(A.fname, ' ', A.lname)");
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));
			
			$sWhere = $this->filtering($cColumns, $params, TRUE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM $this->user_table A, $this->status_table B
				WHERE A.status_id = B.status_id 
				AND A.status_id NOT IN (?,?,?,?)
				AND user_id != ?
				$filter_str
	        	$sOrder
	        	$sLimit
EOS;

			$val = array_merge($val,$filter_params);
			
			$stmt = $this->query($query, $val, TRUE);
						
			return $stmt;
		}	
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	
	public function get_user_details($user_id)
	{
		try
		{
			$query = <<<EOS
				SELECT A.*, GROUP_CONCAT(B.role_code SEPARATOR ',') roles, C.name org_name
				FROM $this->user_table A, $this->user_roles_table B, $this->org_table C
				WHERE A.user_id = B.user_id
				AND A.org_code = C.org_code
				AND A.user_id = ?
EOS;
		
			$stmt = $this->query($query, array($user_id), FALSE);
			
			return $stmt;
		}	
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	
	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_user_list($aColumns, $bColumns, $params);
			
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
		
			return $stmt;
		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$where = array();
			
			$fields = array("COUNT(user_id) cnt");
			
			$where["status_id"]["!="] = INACTIVE;
			$where["user_id"]["!="] = ANONYMOUS_ID;
			
			return $this->select_one($fields, $this->user_table, $where);
		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}	
	}
	
	public function get_specific_user($id)
	{
		try
		{
			$where = array();
			$fields = array("*");
			$where["user_id"] = $id;
				
			return $this->select_all($fields, $this->user_table, $where);
		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	
	public function insert_user($params)
	{
		try
		{		
			$val = array();
			
			$salt = gen_salt(TRUE);
					
			$val["lname"] = filter_var($params['lname'], FILTER_SANITIZE_STRING);
			$val["fname"] = filter_var($params['fname'], FILTER_SANITIZE_STRING);
			$val["mname"] = filter_var($params['mname'], FILTER_SANITIZE_STRING);
			$val["nickname"] = ISSET($params['nickname']) ? filter_var($params['nickname'], FILTER_SANITIZE_STRING) : NULL;
			$val["gender"] = filter_var($params['gender'], FILTER_SANITIZE_STRING);
			$val["contact_no"] = ISSET($params['contact_no']) ? filter_var($params['contact_no'], FILTER_SANITIZE_NUMBER_INT) : NULL;
			$val["mobile_no"] = ISSET($params['mobile_no']) ? filter_var($params['mobile_no'], FILTER_SANITIZE_NUMBER_INT) : NULL;
			$val["email"] = filter_var($params['email'], FILTER_SANITIZE_STRING);
			$val["org_code"] = filter_var($params["org"], FILTER_SANITIZE_NUMBER_INT);
			$val["job_title"] = filter_var($params['job_title'], FILTER_SANITIZE_STRING);
			$val["photo"] = !EMPTY($params['image']) ? filter_var($params['image'], FILTER_SANITIZE_STRING) : NULL;
			$val["status_id"] = ISSET($params["status"]) ? filter_var($params["status"], FILTER_SANITIZE_NUMBER_INT) : INACTIVE;			  
			$val["password"] = in_salt($params["password"], $salt, TRUE);
			$val["raw_password"] = base64_url_encode($params["password"]);
			$val["salt"] = $salt;
			$val["created_by"] = $this->session->userdata("user_id");
			$val["created_date"] = date('Y-m-d H:i:s');
			
			if(ISSET($params["username"]))
				$val["username"] = filter_var($params["username"], FILTER_SANITIZE_STRING);
			
			if(ISSET($params["send_email"]))
			  $val["mail_flag"] = filter_var($params["send_email"], FILTER_SANITIZE_NUMBER_INT);
			
			$user_id = $this->insert_data($this->user_table, $val, TRUE);
			
			if(!EMPTY($user_id) && !EMPTY($params["role"]))
				$this->__insert_user_roles($params["role"], $user_id);
			
			return $user_id;
			
		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	
	
	public function update_user($params)
	{
		try
		{
			$val = array();
			$where = array();
			
			$user_id = filter_var($params["user_id"], FILTER_SANITIZE_NUMBER_INT);
			
			$val["lname"] = filter_var($params['lname'], FILTER_SANITIZE_STRING);
			$val["fname"] = filter_var($params['fname'], FILTER_SANITIZE_STRING);
			$val["mname"] = filter_var($params['mname'], FILTER_SANITIZE_STRING);
			$val["nickname"] = filter_var($params['nickname'], FILTER_SANITIZE_STRING);
			$val["gender"] = filter_var($params['gender'], FILTER_SANITIZE_STRING);
			$val["contact_no"] = filter_var($params['contact_no'], FILTER_SANITIZE_NUMBER_INT);
			$val["mobile_no"] = filter_var($params['mobile_no'], FILTER_SANITIZE_NUMBER_INT);
			$val["email"] = filter_var($params['email'], FILTER_SANITIZE_STRING);
			$val["org_code"] = filter_var($params["org"], FILTER_SANITIZE_NUMBER_INT);
			$val["job_title"] = filter_var($params['job_title'], FILTER_SANITIZE_STRING);
			$val["photo"] = !EMPTY($params['image']) ? filter_var($params['image'], FILTER_SANITIZE_STRING) : NULL;
			$val["status_id"] = ISSET($params["status"]) ? filter_var($params["status"], FILTER_SANITIZE_NUMBER_INT) : INACTIVE;
			$val["modified_by"]	= $this->session->userdata("user_id");
			
			if(!EMPTY($params["password"])){
				$salt = gen_salt(TRUE);
				$val["password"] = in_salt($params["password"], $salt, TRUE);
				$val["raw_password"] = base64_url_encode($params["password"]);
				$val["salt"] = $salt;
			}
			
			if(ISSET($params["username"]))
				$val["username"] = filter_var($params["username"], FILTER_SANITIZE_STRING);
			
			$where["user_id"] = $user_id;
				
			$this->update_data($this->user_table, $val, $where);
			
			if(ISSET($params["role"]) && !EMPTY($params["role"])){
				$this->delete_data($this->user_roles_table, $where);
				$this->__insert_user_roles($params["role"], $user_id);
			}
			
		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	
	
	private function __insert_user_roles($roles, $user_id)
	{
		
		try
		{
			foreach ($roles as $role):
				$params	= array();
				$params["user_id"] = filter_var($user_id, FILTER_SANITIZE_NUMBER_INT);
				$params["role_code"] = filter_var($role, FILTER_SANITIZE_STRING);
				
				$this->insert_data($this->user_roles_table, $params);
				
			endforeach;
				
		} 
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}	
		
	}
	
	public function update_status($params)
	{
		try
		{
			$val = array();
			$where = array();
			
			$val["status_id"] = filter_var($params['status_id'], FILTER_SANITIZE_NUMBER_INT);
			$val["reason"] = ISSET($params['reason']) ? $params['reason'] : NULL;
			$val["modified_by"]	= $this->session->userdata("user_id");
			
			$where['user_id'] = $params['user_id'];
			
			$this->update_data($this->user_table, $val, $where);
			
			if(ISSET($params["role"]) && !EMPTY($params["role"])){
				$this->__insert_user_roles($params["role"], $params['user_id']);
			}
			
		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	
	
	public function check_email_exist($email, $id = 0)
	{
		try
		{
			$val = array();
			$val[] = $email;
			$val[] = DISAPPROVED;
			$val[] = DELETED;
			$where = "";
			
			if($id){
				$where.=" AND user_id = ? 
				  OR (SELECT IF(
					EXISTS(SELECT email FROM users
						WHERE email = ? AND status_id NOT IN(?,?)), 0, 1))";
				$val[] = $id;
				$val[] = $email;
				$val[] = DISAPPROVED;
				$val[] = DELETED;
			}
			
			$query = <<<EOS
				SELECT IF(
				EXISTS(
				  SELECT email FROM users
				  WHERE email =  ? AND status_id NOT IN(?,?) 
				  $where
				), 1, 0) email_exist
EOS;
			$stmt = $this->query($query, $val, FALSE);
			
			return $stmt;
		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	
	public function validate_current_password($id){
		
		try
		{
			$where = array();
			
			$fields = array("raw_password");
			$where["user_id"] = $id;
			
			return $this->select_one($fields, $this->user_table, $where);
		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}	
	}
	
	public function get_user_account_history($user_id , $limit = 0)
	{
		try
		{
			$query = "";
			$val = array(
				$user_id,
				$user_id
			);
			
			$limit_by = ($limit === 0) ? "" : " LIMIT " .$limit;
			
			$query = <<<EOS
				SELECT * FROM users WHERE user_id = ?
				UNION
				SELECT * FROM user_history WHERE user_id = ? ORDER BY modified_date DESC
				$limit_by
EOS;
			$stmt = $this->query($query, $val);
			
			return $stmt;
		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	
	public function check_password_history($user_id, $password)
	{
		try
		{
			//$password = filter_var($password, FILTER_SANITIZE_STRING);
			$x = get_setting(PASSWORD_CONSTRAINTS, PASS_CONS_HISTORY);
			$user_history = $this->get_user_account_history($user_id, $x);
			foreach ($user_history as $user_info)
			{
				// ENCRYPT THE PASSWORD
				$hashed_password = base64_url_decode($user_info["raw_password"]);
				
				if($password == $hashed_password)
				{
					throw new Exception('Password must not match any of the user\'s previous ' .$x . ' passwords.');
				}
			}
		} 
		catch(PDOException $e)
		{
			self::rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			self::rlog_error($e);
			
			throw $e;			
		}
	}
	
	
	public function get_user_salt(){
	
		try
		{
			$where = array();
	
			$fields = array("salt");
			$where["user_id"] = $this->session->userdata('user_id');
	
			return $this->select_one($fields, $this->user_table, $where);
		} 
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	
	public function update_password_status($duration)
	{
		try
		{
			if(intval($duration) <= 0) return TRUE;
			
			$query = <<<EOS
			UPDATE `users` SET status_id = ? WHERE status_id = ? AND (DATE_ADD(DATE(modified_date), INTERVAL ? DAY) > DATE(NOW()));
EOS;
			$stmt = $this->db->prepare($query);
			$stmt->execute(array(EXPIRED, ACTIVE, $duration));
		} 
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	
	public function get_accounts_to_remind($duration)
	{
		try
		{
			if(intval($duration) <= 0) return TRUE;
			
			$val = array(
				ACTIVE, 
				$duration
			);
			
			$query = <<<EOS
			SELECT user_id, username,DATEDIFF(DATE(NOW()),DATE(modified_date)) as remaining, CONCAT(fname, ' ',substr(mname, 1,1) , '. ', lname) as full_name, gender, email 
			FROM users WHERE status_id = ? AND (DATEDIFF(DATE(NOW()),DATE(modified_date)) <= ?)
EOS;
			$stmt = $this->query($query, $val);
			
			return $stmt;
		} 
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	
	
	/*
	 * Accepts setting type as parameter
	 * returns array with settting name as keys
	 * 
	 */
	public function get_settings_arr($setting_type)
	{
		try
		{
			$this->load->model('settings_model');
			$result = $this->settings_model->get_settings_value($setting_type);
			$return = array();
			foreach ($result as $row)
				$return[$row['setting_name']] = $row['setting_value'];
			
			return $return;
		} 
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	
	public function get_email_params()
	{
		try
		{
			$smtp = SYS_PARAM_TYPE_SMTP;
			$query = <<<EOS
			SELECT * FROM sys_param WHERE sys_param_type = '{$smtp}'
EOS;
			$stmt = $this->db->prepare($query);
			$stmt->execute();
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
			$return = array();
			foreach ($result as $row)
				$return[$row['sys_param_name']] = $row['sys_param_value'];
				
			return $return;
		} 
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}

	
}