<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Orgs_model extends SYSAD_Model {
                
	var $org_table = "organizations";
	var $user_table = "users";
	
	public function __construct() {
		parent::__construct(); 
	}		
	
	
	public function get_org_details($org_code)
	{
		try
		{
			$where = array();
			
			$fields = array("*");
			$where["org_code"] = $org_code;
				
			return $this->select_one($fields, $this->org_table, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function get_orgs()
	{
		try
		{
			$query = <<<EOS
				SELECT org_code, IF(org_parent IS NOT NULL, CONCAT("&emsp;&emsp;", name), name) office
				FROM $this->org_table
				GROUP BY org_code, org_parent
EOS;

			$stmt = $this->query($query);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function get_other_orgs($exclude)
	{
		try
		{
			$query = <<<EOS
				SELECT org_code value, name text
				FROM $this->org_table
				WHERE org_code != ?
EOS;
	
			$stmt = $this->query($query, array($exclude));
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}
	}
	
	public function get_org_list($aColumns, $bColumns, $params)
	{
		try
		{
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));
		
			$sWhere = $this->filtering($bColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
		
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields
				FROM $this->org_table a
					LEFT JOIN $this->org_table b ON a.org_parent = b.org_code
				$filter_str
	        	$sOrder
	        	$sLimit
EOS;
	
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}
	}
	
	
	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_org_list($aColumns, $bColumns, $params);
		
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
			$stmt = $this->query($query, NULL, FALSE);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(org_code) cnt");
			
			return $this->select_one($fields, $this->org_table);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}
	}
	
	
	public function insert_org($params)
	{
		try
		{
			$val = array();

			$val["org_code"]     = filter_var($params['org_code'], FILTER_SANITIZE_STRING);
			
			if(! EMPTY($params['parent_org_code']))
				$val["org_parent"] 	= filter_var($params['parent_org_code'], FILTER_SANITIZE_STRING);
			
			$val["short_name"] 	  = filter_var($params['org_short_name'], FILTER_SANITIZE_STRING);
			$val["name"] 		  = filter_var($params['org_name'], FILTER_SANITIZE_STRING);
			$val["website"] 	  = filter_var($params['website'], FILTER_SANITIZE_URL);
			$val["email"] 		  = filter_var($params['email'], FILTER_SANITIZE_EMAIL);
			$val["phone"]	      = filter_var($params['tel_no'], FILTER_SANITIZE_STRING);
			$val["fax"] 		  = filter_var($params['fax_no'], FILTER_SANITIZE_STRING);
			$val["location_code"] = $this->session->userdata("location_code");
			$val["created_by"]    = $this->session->userdata("user_id");
			$val["created_date"]  = date('Y-m-d H:i:s');
				
			$this->insert_data($this->org_table, $val);
				
			return $val['org_code'];
				
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}
	}
	
	
	public function update_org($params, $org_code)
	{
		try
		{
			$val   = array();
			$where = array();
				
			$where["org_code"] 	  = filter_var($org_code, FILTER_SANITIZE_STRING);
				
			
			$val["org_parent"] 	  = ! EMPTY($params['parent_org_code']) ? filter_var($params['parent_org_code'], FILTER_SANITIZE_STRING) : NULL;
			$val["short_name"] 	  = filter_var($params['org_short_name'], FILTER_SANITIZE_STRING);
			$val["name"] 		  = filter_var($params['org_name'], FILTER_SANITIZE_STRING);
			$val["website"] 	  = filter_var($params['website'], FILTER_SANITIZE_URL);
			$val["email"] 		  = filter_var($params['email'], FILTER_SANITIZE_EMAIL);
			$val["phone"]	      = filter_var($params['tel_no'], FILTER_SANITIZE_STRING);
			$val["fax"] 		  = filter_var($params['fax_no'], FILTER_SANITIZE_STRING);
			$val["modified_by"]	  = $this->session->userdata("user_id");
			$val["modified_date"] = date('Y-m-d H:i:s');
				
			$this->update_data($this->org_table, $val, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}
	}
	
	public function delete_org($id)
	{
		try
		{
			$where = array();
	
			$where['org_code'] = $id;
	
			$this->delete_data($this->org_table, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}
	}
	
	public function get_users_count($id)
	{
		try
		{
			$where = array();
				
			$fields = array("COUNT(*) cnt");
			$where["org_code"] = $id;
	
			return $this->select_one($fields, $this->user_table, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}
	}
			
}