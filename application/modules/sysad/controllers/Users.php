<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users extends SYSAD_Controller {

	private $module = MODULE_USER;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('users_model', 'users', TRUE);
	}
	
	public function index()
	{	
		$data = $resources = array();
		
		$resources['load_css'] = array('jquery.dataTables');
		$resources['load_js'] = array('jquery.dataTables.min');
		$resources['datatable'] = array('table_id' => 'user_table', 'path' => 'sysad/users/get_user_list');
		
		$this->template->load('users', $data, $resources);
	}
	
	
	public function get_user_list()
	{		
		$params = get_params();
		
		$aColumns = array("A.user_id", "A.nickname", "A.username", "A.fname","A.lname", "A.email", "B.status", "A.photo");
		$bColumns = array("username", "fname", "lname", "email", "status");
	
		$users = $this->users->get_user_list($aColumns, $bColumns, $params);
		$iTotal = $this->users->total_length();
		$iFilteredTotal = $this->users->filtered_length($aColumns, $bColumns, $params);
	
		$output = array(
			"sEcho" => intval($_POST['sEcho']),
			"iTotalRecords" => $iTotal["cnt"],
			"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
			"aaData" => array()
		);
		$cnt = 0;
		
		foreach ($users as $aRow):
			$cnt++;
			$row = array();
			$action = "<div class='table-actions'>";
		
			$user_id = $aRow["user_id"];
			$id = base64_url_encode($user_id);
			$salt = gen_salt();
			$token = in_salt($user_id, $salt);
			$url = $id."/".$salt."/".$token;
			$delete_action = 'content_delete("user","'.$id.'")';
			$img_src = (@getimagesize(base_url() . PATH_USER_UPLOADS . $aRow["photo"])) ? PATH_USER_UPLOADS . $aRow["photo"] : PATH_IMAGES . "avatar.jpg";
						
			for ( $i=0 ; $i<count($bColumns) ; $i++ )
			{
				$avatar = ($i == 0) ? '<img class="avatar" width="20" height="20" src="'.base_url(). $img_src.'" /> ' : '';
				$row[] =  $avatar . $aRow[ $bColumns[$i] ];
			}

			// if($this->permission->check_permission(MODULE_USER, ACTION_EDIT))
				$action .= "<a href='javascript:;' class='edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' onclick=\"content_form('users/form/".$url."', '".PROJECT_CORE."')\"></a>";
			
			// if($this->permission->check_permission(MODULE_USER, ACTION_DELETE))
				$action .= "<a href='javascript:;' onclick='".$delete_action."' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
			
			$action .= "</div>";
			
			if($cnt == count($users))
				$action.= "<script>$(function(){ $('.tooltipped').tooltip({delay: 50});	});</script>";
			
			$row[] = $action;
				
			$output['aaData'][] = $row;
		endforeach;
	
		echo json_encode( $output );
	}
	
	public function form($id = NULL, $salt = NULL, $token = NULL)
	{
		try
		{
			$data = array();
			$resources = array();
			
			$this->load->model('orgs_model', 'orgs', TRUE);
			$this->load->model('roles_model', 'roles', TRUE);
				
		  	$data['orgs'] = $this->orgs->get_orgs();
			$data['roles'] = $this->roles->get_roles();
			
			if(!IS_NULL($id)){
				$id = base64_url_decode($id);
				
				// CHECK IF THE SECURITY VARIABLES WERE CORRUPTED OR INTENTIONALLY EDITED BY THE USER
				check_salt($id, $salt, $token);
				
				$user = $this->users->get_user_details($id);
				$data['user'] = $user;
				
				$user_roles = $this->roles->get_user_roles($id);
				for ($i=0; $i<count($user_roles); $i++){
					$user_roles_arr[] = $user_roles[$i]["role_code"]; 
				}
				
				if(!EMPTY($user["org_code"]))
					$resources['single'] = array('org' => $user["org_code"]);
				
				if(!EMPTY($user_roles_arr))
					$resources['multiple'] = array('role' => $user_roles_arr);
			}	
			
			$resources['load_css'] = array('jquery-labelauty', 'selectize.default', 'uploadfile');
			$resources['load_js'] = array('jquery-labelauty', 'selectize', 'jquery.uploadfile');
			$resources['upload'] = array(
				array('id' => 'avatar', 'path' => PATH_USER_UPLOADS, 'allowed_types' => 'jpeg,jpg,png,gif', 'show_progress' => 1, 'show_preview' => 1)
			);
			$this->template->load('forms/users', $data, $resources);
		
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}	
	}
		
	public function process()
	{
		try
		{
			$flag = 0;
			$params	= get_params();
				
			$action = (EMPTY($params['user_id']))? AUDIT_INSERT : AUDIT_UPDATE;
	
			// SERVER VALIDATION
			$this->_validate($params, $action);
	
			// GET SECURITY VARIABLES
			$id		= filter_var($params['user_id'], FILTER_SANITIZE_NUMBER_INT);
			$salt	= $params['salt'];
			$token	= $params['token'];
			
			$name = $params['fname']. ' ' . $params['lname'];
	
			// CHECK IF THE SECURITY VARIABLES WERE CORRUPTED OR INTENTIONALLY EDITED BY THE USER
			check_salt($id, $salt, $token);
			
			$user_id 		= ($action == AUDIT_INSERT) ? 0 : $id;
			$email_exist	= $this->_validate_email($params['email'], $user_id);

			
			SYSAD_Model::beginTransaction();
			
			$audit_table[]	= $this->users->tbl_users;
			$audit_schema[]	= Base_Model::$schema_core;
			$audit_action[]	= $action;
			
			if(!$email_exist && EMPTY($id)){
					
				$prev_detail[]	= array();
				
				$id = $this->users->insert_user($params);
				$msg = $this->lang->line('data_saved');
				
				// GET THE DETAIL AFTER INSERTING THE RECORD
				$curr_detail[] = $this->users->get_specific_user($id);	
				
				// ACTIVITY TO BE LOGGED ON THE AUDIT TRAIL
				$activity = "%s has been added";
				$activity = sprintf($activity, $name);				
			
			}else if($email_exist && ($id)){
								
				// GET THE DETAIL FIRST BEFORE UPDATING THE RECORD
				$prev_detail[] = $this->users->get_specific_user($id);
				
				$this->users->update_user($params);
				$msg = $this->lang->line('data_updated');
				
				// GET THE DETAIL AFTER UPDATING THE RECORD
				$curr_detail[] = $this->users->get_specific_user($id);
				
				// ACTIVITY TO BE LOGGED ON THE AUDIT TRAIL
				$activity = "%s has been updated";
				$activity = sprintf($activity, $name);				
				
			}else{				
				throw new Exception($this->lang->line('email_exist'));
			}
			
			// LOG AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
									
			SYSAD_Model::commit();
			
			$flag = 1;
			if(!EMPTY($id) && ($params["send_email"]))
				$flag = $this->_send_welcome_email($id);
		}
		catch(PDOException $e)
		{
			SYSAD_Model::rollback();
		
			$msg = $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			SYSAD_Model::rollback();
				
			$msg = $this->rlog_error($e, TRUE);
		}
	
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	}
	
	private function _validate($params, $action = NULL)
	{
		if(EMPTY($params['lname']))
			throw new Exception('Last name is required.');
			
		if(EMPTY($params['fname']))
			throw new Exception('First name is required.');
	
		if(EMPTY($params['email']))
			throw new Exception('Email is required.');			
		
		if(EMPTY($params['org']))
			throw new Exception('Department/Agency is required.');
	
		if(ISSET($params['role']) AND EMPTY($params['role']))
			throw new Exception('Role is required.');
	
		if($action == AUDIT_INSERT){
			if(EMPTY($params['password']))
				throw new Exception('Password is required.');
				
			if(EMPTY($params['confirm_password']))
				throw new Exception('Please confirm password.');
				
			if($params['password'] != $params['confirm_password'])
				throw new Exception('Password did not match.');
		}
	}
	
	private function _validate_email($email, $id)
	{
		try
		{
			$exist_flag = $this->users->check_email_exist($email, $id);
			
			return $exist_flag['email_exist'];
		}
		
		catch(Exception $e)
		{
			throw new Exception($e->getMessage());
		}
		
	}
	
	public function delete_user()
	{
		try
		{
			$flag = 0;
			$params	= get_params();
	
			// CHECK IF THE SECURITY VARIABLES WERE CORRUPTED OR INTENTIONALLY EDITED BY THE USER
			$id = base64_url_decode($params['param_1']);
	
			// BEGIN TRANSACTION
			SYSAD_Model::beginTransaction();
			
			$audit_action[]	= AUDIT_UPDATE;
			$audit_table[]	= $this->users->tbl_users;
			$audit_schema[]	= Base_Model::$schema_core;
				
	
			// GET THE DETAIL FIRST BEFORE UPDATING THE RECORD
			$prev_detail[] = $this->users->get_user_details($id);
				
			$params['status_id'] = filter_var(DELETED, FILTER_SANITIZE_STRING);
			$params['user_id'] = filter_var($id, FILTER_SANITIZE_STRING);
			
			$this->users->update_status($params);
			
			$msg = $this->lang->line('data_deleted');
	
			// GET THE DETAIL AFTER UPDATING THE RECORD
			$curr_detail[] = $this->users->get_user_details($id);
			
			// ACTIVITY TO BE LOGGED ON THE AUDIT TRAIL
			$activity = "%s has been deleted";
			$activity = sprintf($activity, $prev_detail['fname'] . ' ' . $prev_detail['lname']);
			
			// LOG AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table
			);
			
			SYSAD_Model::commit();
			
			$flag = 1;
	
		}
		catch(PDOException $e)
		{
			SYSAD_Model::rollback();
		
			$msg = $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			SYSAD_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
	
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	}
	
	private function _send_welcome_email($id)
	{	
		try
		{
			$user_detail = $this->users->get_user_details($id);
			$created_by = $this->users->get_user_details($user_detail['created_by']);
			
			$flag = 0;
			$email_data = array();
			$template_data = array();
	
			$salt = gen_salt(TRUE);
			$system_title = get_setting(GENERAL, "system_title");
				
			// required parameters for the email template library
			$email_data["from_email"] = get_setting(GENERAL, "system_email");
			$email_data["from_name"] = $system_title;
			$email_data["to_email"] = array($user_detail['email']);
			$email_data["subject"] = 'New User Account';
				
			// additional set of data that will be used by a specific template
			$template_data["email"] = $user_detail['email'];
			$template_data["password"] = base64_url_encode($user_detail['password']);
			$template_data["raw_password"] = base64_url_decode($user_detail['raw_password']);
			$template_data["reason"] = $user_detail['reason'];
			$template_data["name"] = $user_detail['fname'] . ' ' . $user_detail['lname'];
			$template_data["created_by"] = $created_by['fname'] . ' ' . $created_by['lname'];
			$template_data["system_name"] = $system_title;
				
			$this->email_template->send_email_template($email_data, "emails/welcome_message", $template_data);
			//$flag = 1;
			$flag = $this->email->print_debugger();
			
			return $flag;
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	
	}
}

/* End of file Users.php */
/* Location: ./application/modules/sysad/controllers/Users.php */