<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Organizations extends SYSAD_Controller {
	
	private $module = MODULE_ORGANIZATION;
	private $table_id = 'organizations_table';
	private $table_path = PROJECT_CORE . '/organizations/get_organization_list/';
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('orgs_model', 'om', TRUE);
	}
	
	public function index()
	{	
		$data 	   =  array();
		$resources =  array();
		
		$resources['load_js']  = array('jquery.dataTables.min', 'selectize', 'tableExport','jquery.base64','html2canvas','jspdf/libs/sprintf','jspdf/jspdf','jspdf/libs/base64');
		$resources['load_css'] = array('jquery.dataTables', 'selectize.default');
		$resources['datatable'] = array('table_id' => $this->table_id, 'path' => $this->table_path);
		
		$this->template->load('organizations', $data, $resources);
		
	}

	public function get_organization_list($sector_code=NULL)
	{
		try{
			$rows     = array();
			$params   = get_params();
				
			$aColumns = array('a.org_code', 'a.org_parent', 'a.name', 'a.website', 'a.email', 'b.name as parent_org_name');
			$bColumns = array('a.org_code', 'a.name');
				
			$organizations  = $this->om->get_org_list($aColumns, $bColumns, $params, $sector_code);
			$iFilteredTotal = $this->om->filtered_length($aColumns, $bColumns, $params);
			$iTotal 	    = $this->om->total_length();
			
			$keys			= array_keys($organizations);
			$last_key 		= array_pop($keys);	 
			foreach($organizations as $key => $val):
				$actions    = '';
				$id  	    = base64_url_encode($val['org_code']);
				$salt	    = gen_salt();
				$token      = in_salt($val['org_code'], $salt);
						 
				$del_action = 'content_delete(\'Organization\', \''.$id.'\');';	
				$href		= $id.'/'.$salt.'/'.$token;
				$actions   .= '<div class="table-actions">';
				$actions   .= '<a href="javascript:;" id="edit_programs" class="md-trigger edit tooltipped" data-tooltip="Edit" data-position="bottom" data-modal="modal_organizations" onclick="modal_init(\''.$href.'\');" ></a>';
				$actions   .= '<a href="javascript:;" onclick="'.$del_action.'" class="delete tooltipped" data-tooltip="Delete" data-position="bottom" data-delay="50"></a>';
				$actions   .= '</div>';
				
				if($last_key == $key):
					$resources['load_js'] = array('modalEffects');
					$actions.= $this->load_resources->get_resource($resources, TRUE);
				endif; 
				
				$rows[]     = array(
						$val['name'],
						$val['parent_org_name'],
						$val['website'],
						$val['email'],
						$actions
				);
			endforeach;
	
			$output  = array(
					'aaData' 		=> $rows,
					'sEcho'		    => intval($_POST['sEcho']),
					'iTotalRecords' => $iTotal["cnt"],
					'iTotalDisplayRecords' => $iFilteredTotal["cnt"]
			);
				
			echo json_encode($output);
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
	public function modal($id=NULL, $salt=NULL, $token=NULL)
	{
		try{
			$data 	   = array();
			$resources = array();
			$org_code  = 0;
			if( ! EMPTY($id) && ! EMPTY($salt) && ! EMPTY($token)):
				
				$org_code = base64_url_decode($id);
				
				// CHECK IF THE SECURITY VARIABLES WERE CORRUPTED OR INTENTIONALLY EDITED BY THE USER
				check_salt($org_code, $salt, $token);
				
				$org_details = $this->om->get_org_details($org_code);
				
				$data['id']    = $id;
				$data['salt']  = $salt;
				$data['token'] = $token;
				$data['org_details'] = $org_details;
			endif;
			
			$data['other_orgs']   = $this->om->get_other_orgs($org_code);
			$data['table_path']   = PROJECT_CORE.$this->table_path;

			$this->load->view("modals/organizations", $data);

		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
	
	public function save()
	{
		try{
			$status 	 = 0;
			$params 	 = get_params();
			
			// SERVER VALIDATION
			$this->_validate($params);
			
			// GET SECURITY VARIABLES
			$id	= $params['id'];
			$salt = $params['salt'];
			$token = $params['token'];
			
			// BEGIN TRANSACTION
			SYSAD_Model::beginTransaction();
			
			$audit_table[] 	= $this->om->tbl_organizations;
			$audit_schema[]	= Base_Model::$schema_core;
			
			$org_short_name = $params['org_short_name'];
			
			if( ! EMPTY($id) && ! EMPTY($salt) && ! EMPTY($token)):
				$audit_action[] = AUDIT_UPDATE;
				
				$org_code   = base64_url_decode($id);
					
				// CHECK IF THE SECURITY VARIABLES WERE CORRUPTED OR INTENTIONALLY EDITED BY THE USER
				check_salt($org_code, $salt, $token);
				
				// GET THE DETAIL FIRST BEFORE UPDATING THE RECORD
				$prev_detail[] = array($this->om->get_org_details($org_code));
				
				$this->om->update_org($params, $org_code);
				$msg = $this->lang->line('data_updated');
				
				// GET THE DETAIL AFTER UPDATING THE RECORD
				$curr_detail[] = array($this->om->get_org_details($org_code));
				
				// ACTIVITY TO BE LOGGED ON THE AUDIT TRAIL
				$activity = $this->lang->line('audit_trail_update');
				
			else:
				$prev_detail[]	= array();
				$audit_action[] = AUDIT_INSERT;
				
				$org_code = $this->om->insert_org($params);
				$msg = $this->lang->line('data_saved');
				
				// GET THE DETAIL AFTER INSERTING THE RECORD
				$curr_detail[]    = array($this->om->get_org_details($org_code));
				
				// ACTIVITY TO BE LOGGED ON THE AUDIT TRAIL
				$activity = $this->lang->line('audit_trail_add');
			endif;
			
			$activity = sprintf($activity, $org_short_name);
			
			// LOG AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
			
			SYSAD_Model::commit();
			$status = 1;
			
		}
		catch(PDOException $e)
		{
			SYSAD_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
			
		}
		catch(Exception $e)
		{
			SYSAD_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}
		
		echo json_encode(array(
			'status' 		=> $status, 
			'msg' 		=> $msg,
			'table_id' 	=> $this->table_id,
			'path' 		=> $this->table_path
		));
	}
	
	
	public function delete_organizations()
	{
		try
		{
			$status 	    = 0;
			$params 	    = get_params();
			$org_code       = base64_url_decode($params['param_1']);

			// BEGIN TRANSACTION
			SYSAD_Model::beginTransaction();
			
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= $this->om->tbl_organizations;
			$audit_schema[]	= Base_Model::$schema_core;
			
			// GET THE DETAIL FIRST BEFORE DELETING THE RECORD
			$prev_detail[] = array($this->om->get_org_details($org_code));
			
			$this->om->delete_org($org_code);
			$msg = $this->lang->line('data_deleted');
			
			$curr_detail[] = array();
			// ACTIVITY TO BE LOGGED ON THE AUDIT TRAIL
			$activity = sprintf($this->lang->line('audit_trail_delete'), $prev_detail[0][0]['short_name']);
				
			// LOG AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
				
			SYSAD_Model::commit();
			$status = 1;
			
		}
		catch(PDOException $e)
		{
			SYSAD_Model::rollback();
		
			$msg = $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			SYSAD_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
	
		echo json_encode(array(
			'flag' 		=> $status, 
			'msg' 		=> $msg, 
			'reload' 	=> 'datatable', 
			'table_id' 	=> $this->table_id,
			'path' 		=> $this->table_path
		));
	}
	
	private function _validate($params)
	{
		if( ! ISSET($params['org_short_name']) || EMPTY($params['org_short_name']))
			throw new Exception(sprintf($this->lang->line('is_required'), 'Organization Short Name'));
		
		if( ! ISSET($params['org_name']) || EMPTY($params['org_name']))
			throw new Exception(sprintf($this->lang->line('is_required'), 'Organization Name'));
		
		if( ! ISSET($params['website']) || EMPTY($params['website']))
			throw new Exception(sprintf($this->lang->line('is_required'), 'Website'));
		
		if( ! ISSET($params['email']) || EMPTY($params['email']))
			throw new Exception(sprintf($this->lang->line('is_required'), 'Email'));
		
		$org_details = $this->om->get_org_details($params['org_code']);
		
		if(EMPTY($params['id']) && EMPTY($params['salt']) && EMPTY($params['token'])){
			
			if( ! ISSET($params['org_code']) || EMPTY($params['org_code']))
				throw new Exception(sprintf($this->lang->line('is_required'), 'Organization Code')); 
			 
			if(! EMPTY($org_details))
				throw new Exception('Duplicate Organization Code');
			
		}
	}
	
}
