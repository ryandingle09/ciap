<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cias_params_model extends SYSAD_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	
	public function get_params($table, $where=NULL, $fields ="*", $order_by = NULL )
	{
		
		try {			
			return $this->select_all($fields, $table, $where, $order_by);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
	}

	public function get_employees($select_fields, $where)
	{
		try
		{
			$fields	= implode(',', $select_fields);

			$where_arr	= $this->get_where_statement($where);
			$where 		= $where_arr['where'];
			$val 		= $where_arr['val'];
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM cias_user AS U
				LEFT JOIN cias_user_role AS R
				ON U.employee_no = R.employee_no
				$where
				ORDER BY U.first_name
				ASC
EOS;
		
			return $this->query($query, $val);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
	}


}		

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */