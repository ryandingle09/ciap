<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Psms_asset_sites_model extends PSMS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
		
	public function get_asset_site_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
			
			$filter_where 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
		
						
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s 				
				$filter_where
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query, PSMS_Model::tbl_param_sites); 				 				
			$stmt 	= $this->query($query, $filter_params);			
		
			return $stmt;
		}
		catch(PDOException $e)
		{

			throw $e;
		}
		catch(Exception $e)
		{

			throw $e;			
		}	
	}	
	
	
	public function filtered_length($select_fields, $where_fields, $params)
	{
		try
		{
			$this->get_asset_site_list($select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{

			throw $e;
		}
		catch(Exception $e)
		{

			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(site_id) cnt");
				
			return $this->select_one($fields, PSMS_Model::tbl_param_sites);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{			
			throw $e;			
		}	
	}
	
	public function get_specific_asset_site($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_one($fields, PSMS_Model::tbl_param_sites, $where);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function insert_asset_site($params){
				
		try
		{
			$val 					= array();
			$val['site_name']		= filter_var($params['site_name'], FILTER_SANITIZE_STRING);
			$val["site_desc"] 		= filter_var($params['description'], FILTER_SANITIZE_STRING);
			$val["created_by"] 		= $this->session->userdata("employee_no");
			$val["created_date"] 	= date('Y-m-d H:i:s');

			return $this->insert_data(PSMS_Model::tbl_param_sites, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function update_asset_site($params)
	{
		try
		{
			
			$val 					= array();
			$val['site_name']		= filter_var($params['site_name'], FILTER_SANITIZE_STRING);
			$val["site_desc"] 		= filter_var($params['description'], FILTER_SANITIZE_STRING);
			$val["modified_by"] 	= $this->session->userdata("employee_no");
			$val["modified_date"]	= date('Y-m-d H:i:s');
			
			
			$where 					= array();
			$where["site_id"]		= $params["site_id"];

			$this->update_data(PSMS_Model::tbl_param_sites, $val, $where);

		}		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	
	public function delete_asset_site($site_id)
	{
		try
		{			
			$this->delete_data(PSMS_Model::tbl_param_sites, array('site_id'=>$site_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */