<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Param_sites_model extends PSMS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	public function get_site($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_one($fields, PSMS_Model::tbl_param_sites, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}

	public function get_sites($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
						
			$filter_where 		= $where["search_str"];
			$filter_params 		= $where["search_params"];
						
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields
				FROM %s
				
				$filter_where
				
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query, 
				PSMS_Model::tbl_param_sites
			);
		
			$stmt 	= $this->query($query, $filter_params);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function insert_site($params){

		try
		{

			$val 							= array();
			$val['site_name']				= $params['site_name'];
			$val['site_desc']				= $params['site_desc'];
			$val['created_by']				= $this->session->userdata('employee_no');
			$val['created_date']			= date('Y-m-d H:i:s');;
			$val['modified_by']				= $params['modified_by'];
			$val['modified_date']			= $params['modified_date'];	
				
			return $this->insert_data(PSMS_Model::tbl_param_sites, $val, TRUE);			
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function update_site($params)
	{
		try
		{
			$val 						= array();
			$val['site_name']			= $params['site_name'];
			$val['site_desc']			= filter_var($params['site_desc'], FILTER_SANITIZE_NUMBER_INT);
			$val["modified_date"]		= date('Y-m-d H:i:s');
		
			$where 						= array();
			$where["site_id"]			= $params["site_id"];

			$this->update_data(PSMS_Model::tbl_param_sites, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */