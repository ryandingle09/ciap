<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Psms_asset_accountabilities_model extends PSMS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	public function get_accountability_list($select_fields, $where_fields, $paramsm, $where){
	
		try
		{	
			$fields	= implode(',', $select_fields);
			

			$where_arr		= $this->get_where_statement($where);
			$where_ 		= $where_arr['where'];
			$val			= $where_arr['val'];

			$where			= $this->filtering($where_fields, $params, FALSE);
			$order			= $this->ordering($where_fields, $params);
			$limit			= $this->paging($params);
			
			$filter_where 	= $where["search_str"];
			$filter_params 	= $where["search_params"];

						
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields
				FROM %s
				$filter_where
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query, PSMS_Model::tbl_asset_accountabilities);
		
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{

			throw $e;
		}
		catch(Exception $e)
		{

			throw $e;			
		}
	}


	public function filtered_length($select_fields, $where_fields, $params, $where)
	{
		try
		{
			$this->get_accountability_list($select_fields, $where_fields, $params, $where);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{

			throw $e;
		}
		catch(Exception $e)
		{

			throw $e;			
		}	
	}

	public function total_length($property_no)
	{
		try
		{
			$fields = array("COUNT(asset_accountability_id) cnt ");
			
			return $this->select_one($fields, PSMS_Model::tbl_asset_accountabilities, $where);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{			
			throw $e;			
		}	
	}

	public function get_asset_accountability($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_one($fields, PSMS_Model::tbl_asset_accountabilities, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			echo  $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			echo $e;
		}
	}

	public function get_accountabilities($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_all($fields, PSMS_Model::tbl_asset_accountabilities, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			echo  $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			echo $e;
		}
	}

	public function insert_accountability($params){

		try
		{
			$val 							= array();
			$val['property_no']				= filter_var($params['property_no'], FILTER_SANITIZE_STRING);
			$received_date					= date('Y-m-d', strtotime($params['received_date']));
			$val['received_date']			= filter_var($received_date, FILTER_SANITIZE_STRING);
			$val['employee_id']				= filter_var($params['employee_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['office_id']				= filter_var($params['office_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['attachment']				= filter_var($params['attachment'], FILTER_SANITIZE_STRING);
			$val['accountability_status_id']= filter_var($params['accountability_status_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['created_by']				= $this->session->userdata("employee_no");
			$val['created_date']			= date('Y-m-d H:i:s');
				
			return $this->insert_data(PSMS_Model::tbl_asset_accountabilities, $val, TRUE);			
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			echo $e;
		}
	}

	public function update_accountability($params)
	{
		try
		{
			$val 							= array();
			$val['property_no']				= filter_var($params['property_no'], FILTER_SANITIZE_STRING);
			$received_date					= date('Y-m-d', strtotime($params['received_date']));
			$val['received_date']			= filter_var($received_date, FILTER_SANITIZE_STRING);
			$val['employee_id']				= filter_var($params['employee_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['office_id']				= filter_var($params['office_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['attachment']				= filter_var($params['attachment'], FILTER_SANITIZE_STRING);
			$val['accountability_status_id']= filter_var($params['accountability_status_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['created_by']				= $this->session->userdata("employee_no");
			$val['created_date']			= date('Y-m-d H:i:s');
	
			$where 							= array();
			$where['asset_accountability_id'] = $params['asset_accountability_id'];
			$this->update_data(PSMS_Model::tbl_asset_accountabilities, $val, $where);
		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/psms_asset_accountabilities_model.php */