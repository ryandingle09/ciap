<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Psms_assets_model extends PSMS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	public function get_specific_asset($where)
	{
		try
		{
			$where_arr	= $this->get_where_statement($where);
			$where 		= $where_arr['where'];
			$val		= $where_arr['val'];

			$query = <<<EOS
				SELECT A.*, ACL.asset_category_id, ALO.site_id
				FROM %s A
				LEFT JOIN %s AS ACL
				ON A.asset_classification_id = ACL.asset_classification_id
				LEFT JOIN %s AS ALO
				ON A.asset_location_id = ALO.asset_location_id
				$where
	        	LIMIT 1
EOS;
			
			$query	= sprintf($query,
				PSMS_Model::tbl_assets,
				PSMS_Model::tbl_asset_classifications,
				PSMS_Model::tbl_asset_locations
			);
			$result = $this->query($query, $val);
			
			return !empty($result)?array_shift($result):array();
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}

	public function get_asset_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
						
			$filter_where 		= $where["search_str"];
			$filter_params 		= $where["search_params"];

			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				LEFT JOIN %s S ON A.asset_status_id = S.asset_status_id
				
				$filter_where
				
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query, 
				PSMS_Model::tbl_assets, 
				PSMS_Model::tbl_param_asset_status
			);
			
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function filtered_length($select_fields, $where_fields, $params)
	{
		try
		{
			$this->get_asset_list($select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{

			throw $e;
		}
		catch(Exception $e)
		{

			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(site_id) cnt");
				
			return $this->select_one($fields, PSMS_Model::tbl_param_sites);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{			
			throw $e;			
		}	
	}

	public function insert_asset($params){

		try
		{

			$val 									= array();
			$val['property_no']						= filter_var($params['property_no'], FILTER_SANITIZE_STRING);
			$val['old_property_no']					= filter_var($params['old_property_no'], FILTER_SANITIZE_STRING);
			$val['property_name']					= filter_var($params['property_name'], FILTER_SANITIZE_STRING);
			$val['property_desc']					= filter_var($params['property_desc'], FILTER_SANITIZE_STRING);
			$val['property_serial_no']				= filter_var($params['property_serial_no'], FILTER_SANITIZE_STRING);
			$val["asset_classification_id"]			= filter_var($params['asset_classification_id'], FILTER_SANITIZE_NUMBER_INT);
			$val["asset_location_id"] 				= filter_var($params['asset_location_id'], FILTER_SANITIZE_NUMBER_INT);
			$val["property_acquired_date"] 			= $params['property_acquired_date'];
			$val["property_ref_no"] 				= filter_var($params['property_ref_no'], FILTER_SANITIZE_STRING);
			$val["property_unit_cost"] 				= filter_var($params['property_unit_cost'], FILTER_SANITIZE_NUMBER_INT);;
			$val["property_accountable_officer"]	= filter_var($params['property_accountable_officer'], FILTER_SANITIZE_NUMBER_INT);
			$val["property_make"] 					= filter_var($params['property_make'], FILTER_SANITIZE_STRING);
			$val["property_model"] 					= filter_var($params['property_model'], FILTER_SANITIZE_STRING);
			$val["property_useful_life"] 			= 0;
			$val["property_useful_life_flag"] 		= 0;
			$val["asset_condition_id"] 				= filter_var($params['asset_condition_id'], FILTER_SANITIZE_NUMBER_INT);
			$val["asset_status_id"] 				= filter_var($params['asset_status_id'], FILTER_SANITIZE_NUMBER_INT);
			$val["file_name"] 						= $params['file_name'];
			$val["created_by"] 						= $this->session->userdata('employee_no');
			$val["created_date"] 					= date('Y-m-d H:i:s');
				
			return $this->insert_data(PSMS_Model::tbl_assets, $val, TRUE);			
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function update_asset($params)
	{
		try
		{
			$val 									= array();
			$val['property_no']						= filter_var($params['property_no'], FILTER_SANITIZE_STRING);
			//$val['old_property_no']					= filter_var($params['old_property_no'], FILTER_SANITIZE_STRING);
			$val['property_name']					= filter_var($params['property_name'], FILTER_SANITIZE_STRING);
			$val['property_desc']					= filter_var($params['property_desc'], FILTER_SANITIZE_STRING);
			$val['property_serial_no']				= filter_var($params['property_serial_no'], FILTER_SANITIZE_STRING);
			$val["asset_classification_id"]			= filter_var($params['asset_classification_id'], FILTER_SANITIZE_NUMBER_INT);
			$val["asset_location_id"] 				= filter_var($params['asset_location_id'], FILTER_SANITIZE_NUMBER_INT);
			$val["property_acquired_date"] 			= $params['property_acquired_date'];
			$val["property_ref_no"] 				= filter_var($params['property_ref_no'], FILTER_SANITIZE_STRING);
			$val["property_unit_cost"] 				= filter_var($params['property_unit_cost'], FILTER_SANITIZE_NUMBER_INT);;
			$val["property_accountable_officer"]	= filter_var($params['property_accountable_officer'], FILTER_SANITIZE_NUMBER_INT);
			$val["property_make"] 					= filter_var($params['property_make'], FILTER_SANITIZE_STRING);
			$val["property_model"] 					= filter_var($params['property_model'], FILTER_SANITIZE_STRING);
			$val["property_useful_life"] 			= 0;
			$val["property_useful_life_flag"] 		= 0;
			$val["asset_condition_id"] 				= filter_var($params['asset_condition_id'], FILTER_SANITIZE_NUMBER_INT);
			$val["asset_status_id"] 				= filter_var($params['asset_status_id'], FILTER_SANITIZE_NUMBER_INT);
			$val["file_name"] 						= $params['file_name'];
			$val["modified_by"] 					= $this->session->userdata('employee_no');
			$val["modified_date"] 					= date('Y-m-d H:i:s');

			$where 			= array();
			$key 			= $params['old_property_no_key'];
			$where[$key]	= $params['old_property_no'];

			$this->update_data(PSMS_Model::tbl_assets, $val, $where);
		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}


	public function delete_asset($property_no)
	{
		try
		{			
			$this->delete_data(PSMS_Model::tbl_assets, array('property_no' => $property_no));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */