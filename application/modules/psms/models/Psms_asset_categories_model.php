<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Psms_asset_categories_model extends PSMS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
		
	public function get_asset_category_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
			
			$filter_where 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
		
						
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s
				$filter_where
				
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query,
				PSMS_Model::tbl_asset_categories
			);	
			$stmt 	= $this->query($query, $filter_params);			
		
			return $stmt;
		}
		catch(PDOException $e)
		{

			throw $e;
		}
		catch(Exception $e)
		{

			throw $e;			
		}	
	}
	
	public function filtered_length($select_fields, $where_fields, $params)
	{
		try
		{
			$this->get_asset_category_list($select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{

			throw $e;
		}
		catch(Exception $e)
		{

			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(site_id) cnt");
				
			return $this->select_one($fields, PSMS_Model::tbl_param_sites);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{			
			throw $e;			
		}	
	}
	

	public function delete_asset_category($category_id)
	{
		try
		{			
			$this->delete_data(PSMS_Model::tbl_asset_categories, array('asset_category_id'=>$category_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	
	public function get_specific_asset_category($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_one($fields, PSMS_Model::tbl_asset_categories, $where);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function insert_asset_category($params){
				
		try
		{
			$val 								= array();
			$val['asset_category_name']			= $params['asset_category_name'];
			$val["asset_category_desc"] 		= filter_var($params['asset_category_desc'], FILTER_SANITIZE_STRING);
			$val["asset_category_useful_life"]	= filter_var($params['asset_category_useful_life'], FILTER_SANITIZE_NUMBER_INT);
			$val["created_by"] 					= $this->session->userdata("employee_no");
			$val["created_date"] 				= date('Y-m-d H:i:s');

			return $this->insert_data(PSMS_Model::tbl_asset_categories, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function update_asset_category($params)
	{
		try
		{
			$val 								= array();
			$val['asset_category_name']			= filter_var($params['asset_category_name'], FILTER_SANITIZE_STRING);
			$val["asset_category_desc"] 		= filter_var($params['asset_category_desc'], FILTER_SANITIZE_STRING);
			$val["asset_category_useful_life"]	= filter_var($params['asset_category_useful_life'], FILTER_SANITIZE_NUMBER_INT);
			$val["modified_by"]					= $this->session->userdata("employee_no");
			$val["modified_date"] 				= date('Y-m-d H:i:s');
			
			$where 							= array();
			$where["asset_category_id"]		= $params["asset_category_id"];

			$this->update_data(PSMS_Model::tbl_asset_categories, $val, $where);

		}		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	

}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */