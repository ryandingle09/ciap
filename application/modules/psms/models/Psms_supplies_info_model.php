<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Psms_supplies_info_model extends PSMS_Model
{
	public function __construct() {
		parent::__construct();	
	}
	
	
	public function get_supplies_info_list($select_fields, $where_fields, $params)
	{
		try
		{
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
				
			$filter_where 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
	
	
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields
				FROM %s A
				LEFT JOIN %s B
				ON B.supplies_item_id = A.supplies_item_id
				$filter_where
	        	$order
	        	$limit
EOS;
				
			$query	= sprintf($query, PSMS_Model::tbl_supplies, PSMS_Model::tbl_supplies_items);
			$stmt 	= $this->query($query, $filter_params);
	
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	
	public function filtered_length($select_fields, $where_fields, $params)
	{
		try
		{
			$this->get_supplies_info_list($select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
				
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(supplies_id) cnt");
	
			return $this->select_one($fields, PSMS_Model::tbl_supplies);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function delete_supplies_info($supplies_id)
	{
		try
		{
			$this->delete_data(PSMS_Model::tbl_supplies, array('supplies_id'=>$supplies_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_specific_supply_info($where){
	
		try
		{
			$fields = array("*");
	
			return $this->select_one($fields, PSMS_Model::tbl_supplies, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function insert_supply_info($params){
	
		try
		{
			
			$val 									= array();
			$val["supplies_item_id"]				= filter_var($params['supply_item_no'], FILTER_SANITIZE_NUMBER_INT);
			$val["supplies_item_desc"] 				= filter_var($params['supply_item_notes'], FILTER_SANITIZE_STRING);
			$val["supplies_acquired_date"]			= date_format(date_create($params['supply_received_date']), 'Y-m-d');
			$val["supplies_ref_no"] 				= filter_var($params['supply_refference_no'], FILTER_SANITIZE_STRING);
			$val["supplies_qty"]					= filter_var($params['supply_quantity'], FILTER_SANITIZE_NUMBER_INT);
			$val["supplies_uom"] 					= filter_var($params['supply_category'], FILTER_SANITIZE_STRING);
			$val["supplies_unit_cost"]				= filter_var($params['supply_unit_cost'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$val["supplies_total_cost"] 			= filter_var($params['supply_total_cost'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$val["supplies_accountable_officer"]	= filter_var($params['supply_accountable_person'], FILTER_SANITIZE_STRING);
			//TODO balance
			$val["supplies_balance"] 				= filter_var($params['supplies_balance'], FILTER_SANITIZE_STRING);
			$val["supplies_useful_life"]			= filter_var($params['supply_item_useful_life'], FILTER_SANITIZE_STRING);
			$val["created_by"] 						= $this->session->userdata("employee_no");
			$val["created_date"] 					= date('Y-m-d H:i:s');
			
			return $this->insert_data(PSMS_Model::tbl_supplies, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	
	public function update_supply_info($params)
	{
		try
		{

			$val 									= array();
			$val["supplies_item_id"]				= filter_var($params['supply_item_no'], FILTER_SANITIZE_NUMBER_INT);
			$val["supplies_item_desc"] 				= filter_var($params['supply_item_notes'], FILTER_SANITIZE_STRING);
			$val["supplies_acquired_date"]			= date_format(date_create($params['supply_received_date']), 'Y-m-d');
			$val["supplies_ref_no"] 				= filter_var($params['supply_refference_no'], FILTER_SANITIZE_STRING);
			$val["supplies_qty"]					= filter_var($params['supply_quantity'], FILTER_SANITIZE_NUMBER_INT);
			$val["supplies_uom"] 					= filter_var($params['supply_category'], FILTER_SANITIZE_STRING);
			$val["supplies_unit_cost"]				= filter_var($params['supply_unit_cost'], FILTER_SANITIZE_NUMBER_FLOAT);
			$val["supplies_total_cost"] 			= filter_var($params['supplies_total_cost'], FILTER_SANITIZE_NUMBER_FLOAT);
			$val["supplies_accountable_officer"]	= filter_var($params['supply_employee_no'], FILTER_SANITIZE_STRING);
			$val["supplies_balance"] 				= filter_var($params['supplies_balance'], FILTER_SANITIZE_STRING);
			$val["supplies_useful_life"]			= filter_var($params['supplies_useful_life'], FILTER_SANITIZE_STRING);
			$val["modified_by"] 					= $this->session->userdata("employee_no");
			$val["modified_date"]					= date('Y-m-d H:i:s');

			$where 						= array();
			$where["supplies_id"]		= $params["supplies_id"];
	
			$this->update_data(PSMS_Model::tbl_supplies, $val, $where);
	
		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch (Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	
	public function get_categories()
	{
		try {
			$fields = array("*");
			return $this->select_all($fields, PSMS_Model::tbl_supplies_categories);
		}
			catch (PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch (Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function get_items($select_fields, $where_fields, $params)
	{
		try
		{
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
		
			$filter_where 		= $where["search_str"];
			$filter_params 		= $where["search_params"];
		
			if($filter_where == "")
			$filter_category	= "WHERE A.supplies_category_id = " . $params["search_category"];
			else 
			$filter_category	= "AND A.supplies_category_id = " . $params["search_category"];
	
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields
				FROM %s A
				LEFT JOIN %s B
				ON B.supplies_category_id = A.supplies_category_id
				$filter_where
				$filter_category
	        	$order
	        	$limit
EOS;
				
			$query	= sprintf($query, PSMS_Model::tbl_supplies_items, PSMS_Model::tbl_supplies_categories);
			$stmt 	= $this->query($query, $filter_params);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function total_length_items()
	{
		try
		{
			$fields = array("COUNT(supplies_item_id) cnt");
	
			return $this->select_one($fields, PSMS_Model::tbl_supplies_items);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function filtered_length_items($select_fields, $where_fields, $params)
	{
		try
		{
			$this->get_items($select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
	
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function get_accountable_persons()
	{
		try
		{
			$query = <<<EOS
				SELECT A.employee_no, 
				CONCAT(A.first_name, ' ',A.last_name) full_name
				FROM %s A
				LEFT JOIN %s B
				ON B.employee_no = A.employee_no
				WHERE B.role_code = 'ACCOUNTING-OFFICER'
EOS;
			
			$query	= sprintf($query, PSMS_Model::tbl_cias_user, PSMS_Model::tbl_cias_user_role);
			$stmt 	= $this->query($query, $filter_params);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		
	}
	
	public function insert_item($params){
		try
		{
			$val							= array();
			$val['supplies_stock_no']		= filter_var($params['item_stock_no'], FILTER_SANITIZE_STRING);
			$val['supplies_item_name']		= filter_var($params['item_name'], FILTER_SANITIZE_STRING);
			$val['supplies_item_desc']		= filter_var($params['item_description'], FILTER_SANITIZE_STRING);
			$val['supplies_category_id']	= filter_var($params['item_category'], FILTER_SANITIZE_NUMBER_INT);
			
			return $this->insert_data(PSMS_Model::tbl_supplies_items, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function get_specific_supply_item($where){
	
		try
		{
			$fields = array("*");
	
			return $this->select_one($fields, PSMS_Model::tbl_supplies_items, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function get_specific_supply_category($where){
	
		try
		{
			$fields = array("*");
	
			return $this->select_one($fields, PSMS_Model::tbl_supplies_categories, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
}

/* End of file Psms_supplies_model.php */
/* Location: ./application/modules/psms/models/Psms_supplies_model.php */