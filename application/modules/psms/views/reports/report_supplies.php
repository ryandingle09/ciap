<?php
	
	$data = $report['content'];
	
	$offices = array();
	$item1 = array( 'Toner for HP-PRO 400', 454546, 5, 'pcs', '24,005.00', '124,205.00');
	$item2 = array( 'Flash Drive 16GB', 8454546, 1, 'pcs', '44,045.00', '44,045.00');
	$item3 = array( 'Wireless Microphone "Naly"', 1224545, 1, 'unit', '4,123.00', '4,123.00');
	$item4 = array( 'Stamp pad felt', 1224545, 2, 'pcs', '44,956.00', '13,356.00');
	$total = array( '', '', '', '', '', '39,938.00', '39,938.00');
	
	$office1 = array(
		'office' => 'CIAP - BUDGET',
		'supplies' => array(
			$item1, $item2, $item2, $item3, $total
		)
	);
	$office2 = array(
		'office' => 'CIAP - CASHIER',
		'supplies' => array(
			$item4, $item3, $item2, $item1, $total	
		)
	);
	$office3 = array(
		'office' => 'CIAP - RID',
		'supplies' => array(
			$item2, $item4, $item1,  $item4, $item3, $item2, $item1, $item1, $item2, $item1, $total
		)
	);
	$office4 = array(
		'office' => 'CIAP - RID',
		'supplies' => array(
			$item3, $item1, $item2,  $item4, $item3, $item1, $item1, $item1, $item2, $item1, $total
		)
	);
	$office5 = array(
		'office' => 'CIAP - RID',
		'supplies' => array(
			$item1, $total
		)
	);


	$offices[] = $office1;
	$offices[] = $office2;
	$offices[] = $office3;
	$offices[] = $office4;
	$offices[] = $office5;
	$offices[] = $office5;
	$offices[] = $office5;
?>
<style>
	/*------------- common --------------*/
	
	.*{
		font-size: 12px;
	}

	/*------------- tbl-pan --------------*/
	
	.title-pan .header1,
	.title-pan .header2,
	.title-pan .header3{
		text-align: center;
		font-size: 12px;
		margin: 0px !important;
		padding: 0px !important;
	}

	/*------------- tbl-pan --------------*/

	.tbl-pan{
		margin-top: 1.2em;
	}
	
	.tbl-pan .office{
		font-size: 14px;
		font-weight: bold;;
	}	
	.tbl-pan th,
	.tbl-pan td{
		font-size: 12px;
		text-align:left; 
	}
	.tbl-pan th{
		padding-bottom: 10px;
	}
	.tbl-pan td{
		font-size: 11px;
		padding-bottom: 5px;
	}
	.tbl-pan td.final{
		font-size: 12px;
		font-weight: bold;
	}
	.tbl-pan td.totalem{
		font-weight: bold;
		font-size: 12px;
		text-decoration: underline;
	}

	/*------------- breakdown-pan --------------*/

	.breakdown-pan .*{
		font-size:12px;
	}
	.breakdown-pan .header1{
		font-size:12px;
	}
	.breakdown{
		list-style:none;
		margin-left:40px;
	}
	.breakdown li{
		font-size:12px;
	}
	.liner1,
	.liner2{
		margin: 0 120px;
		width: 150px;
		border-bottom:solid 1px black;
	}
	.liner1{

	}
	.liner2{
		
	}
	.signatory-pan{
		font-size: 12px;
		text-align:center;
		width: 120px;
	}
	.signatory-pan .header1{
		margin-bottom:30px;
		font-size: 12px;
	}
	.signatory-pan .name{
		font-size: 12px;
		margin:0;
		font-weight:bold;
	}
	.signatory-pan .position{
		margin:0;
		
	}
	.p-b{
		margin-top: 100px;
		margin-left: 100px
	}
	.c-c{
		margin-left: 500px;
		margin-top: -85px;
	}
</style>
<div class="wrapper">
	<div class="title-pan">
		<h2 class="header1">REPORTS OF SUPPLIES AND MATERIALS</h2>
		<h2 class="header2">FOR THE MONTH OF AUGUST, 2014</h2>
		<h2 class="header3">101</h2>
	</div>
	<div class="content-pan">
		<?php foreach($offices as $office){ ?>
		<div class="tbl-pan">
			<p class="office"><?php echo $office['office']; ?></p>
			<table  width="100%" cellspacing="0" cellpadding="0" border="0">
				<thead>
					<tr>
						<th>Item</th>
						<th>PO No.</th>
						<th>Qty.</th>
						<th>Unit</th>
						<th>Unit Price</th>
						<th>Total</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($office['supplies'] as $supplies){ ?>
						<tr>
							<td class="item"><?php echo $supplies[0]; ?></td>
							<td class="po_no"><?php echo $supplies[1]; ?></td>
							<td class="qty"><?php echo $supplies[2]; ?></td>
							<td class="unit"><?php echo $supplies[3]; ?></td>
							<td class="price"><?php echo $supplies[4]; ?></td>
							<td class="total <?php echo !($supplies[0])?'final':''; ?>"><?php echo $supplies[5]; ?></td>
							<td class="totalem"><?php echo $supplies[6]; ?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
		</div>
		<?php } ?>
	</div>
	<div class="breakdown-pan">
		<p class="header1">Breakdown:</p>
		<ul class="breakdown">
			<li>155</li>
			<li>155</li>
			<li>155 (S.E)</li>
			<li>155</li>
			<li>155</li>
			<li>155</li>
		</ul>
		<p class="liner1">&nbsp;</p>
		<p class="liner2">&nbsp;</p>
	</div>
	<div class="signatory-pan p-b">
		<p class="header1">Prepared By:</p>
		<p class="name">Wilfredo P. Castro</p>
		<p class="position">Assistant Supply</p>
	</div>
	<div class="signatory-pan c-c">
		<p class="header1">Certified Correct:</p>
		<p class="name">Wilfredo P. Castro</p>
		<p class="position">Admin Officer III</p>
	</div>
</div>










