<?php
	
	$data = $report['content'];
	
	$distributions = array();
	$distribution1 = array( 'PCAB', 3, 6, 3);
	$distribution2 = array( 'PCOB', 3, 4, 3);
	$distribution3 = array( 'PDCB', 5, 2, 1);
	$distribution4 = array( 'CMDF', 2, 2, 4);
	$distribution5 = array( 'PPCMD', 3, 4, 6);
	$distribution6 = array( 'FASD', 6, 8, 8);
	
	$distributions[] = $distribution1;
	$distributions[] = $distribution2;
	$distributions[] = $distribution3;
	$distributions[] = $distribution4;
	$distributions[] = $distribution5;
	$distributions[] = $distribution6;
	$total_desktop = 34;
	$total_laptop = 76;
	$total_printer = 76;

?>
<style>

/*----------------- common ------------------*/

.title-pan{
	margin-bottom: 15px;
}
.title-pan .header1,
.title-pan .header2,
.title-pan .header3{
	font-size: 12px;
	margin: 0;
	text-align: center;
}

/*----------------- common ------------------*/

.tbl-pan table{
	border-collapse: collapse;
}
.tbl-pan th{
	font-size: 10px;
	text-align: center;
	padding: 10px 5px;
	border: 1px solid black;
}
.tbl-pan td{
	border: 1px solid black;
	padding: 5px;
	font-size: 11px;
}
.tbl-pan .total td{
	font-weight: bold;
	padding-bottom: 5px;
}
</style>
<div class="wrapper">
	<div class="title-pan">
		<h2 class="header1">DISTRIBUTION HARDWARE BY BOARD/CATEGORY</h2>
	</div>
	<div class="content-pan">
		<div class="tbl-pan">
			<table  width="100%" cellspacing="0" cellpadding="0" >
				<thead>
					<tr>
						<th>BOARDS/UNITS</th>
						<th>DESKTOP</th>
						<th>LAPTOP</th>
						<th>PRINTER</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($distributions as $distribution){ ?>
					<tr>
						<td class="unit" ><?php echo $distribution[0]; ?></td>
						<td class="desktop"><?php echo $distribution[1]; ?></td>
						<td class="laptop"><?php echo $distribution[2]; ?></td>
						<td class="laptop"><?php echo $distribution[3]; ?></td>
					</tr>
					<?php } ?>
					<tr class="total">
						<td class="total">TOTAL per Type</td>
						<td class="total_desktop"><?php echo $total_desktop; ?></td>
						<td class="total_laptop"><?php echo $total_laptop; ?></td>
						<td class="total_laptop"><?php echo $total_printer; ?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</div>









