<?php 
	//For prototype purposes
	//START
	//
	$year = 2015;
	$date = "October 30, 2015";
	$certified_by = "DOMINADOR R. DAYO";
	$acc_unit = "PERLITA M. RASING";
	$inspector = "ERWIN A. RIVERA";
	$witness = "YOLANDA P. JUAN";
	$authorized_official = "SONIA T. VALDEAVILLA";
	$sample_data  = array(
			array(" ", "Computer:", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "),
			array("12/08/2016", "CPU/Processor: Intel Pentium 2.4 Ghz.", "110801", "1", "14,570.00", "14,570.00", "13,113.00", "1,457.00", "Uns./Exeeded its useful life", "100.00", "For Bidding/Donation", "(2/F)"),
			array("12/08/2016", "IBM PENTIUM IV, 2.0 GHZ", "C9ws-03-01", "1", "70,255.00", "70,255.00", "63,229.50", "7,025.50", "Uns./Exeeded its useful life", "200.00", "For Bidding/Donation", "(2/F)"),
			array(" ", "Cabinet:", " ", " ", " ", " ", " ", " ", " ", " ", " ", " "),
			array("1990", "Filing, all steel, 4-drawer, wrinkle", "Cs2w4-6-1(2)", "3", "540.00", "1,620.00", "1,458.00", "162.00", "Uns./Exeeded its useful life", " ", "For Bidding/Donation", "(B4 CIAP entrance)")
	);
	//
	//END
?>
<style>
	.b-n
	{
		border: 0px;
	}
	
	.b-l-n
	{
		border-left: 0px;
	}
	
	.b-r-n
	{
		border-right: 0px;
	}
	
	.b-t-n
	{
		border-top: 0px;
	}
	
	.b-b-n
	{
		border-bottom: 0px;
	}
	
	.fxxsm
	{
		font-size: xx-small;
	}
	
</style>


<div style="float: left;">
	<p align="center">
		<small>
			<font size="2">
				<b>INVENTORY AND INSPECTION REPORT OF UNSERVICEABLE PROPERTY</b>
			</font>
		</small>
	</p>
</div>

<div>
	<table border="1"  style="border-collapse: collapse;">
		<tr class="b-n">
			<td style="border-style:hidden;" colspan="7">Fund:&nbsp;&nbsp;<font style="text-decoration:underline;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;101&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
			<td style="border-style:hidden;" colspan="5">As of:&nbsp;&nbsp;<font style="text-decoration:underline;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $date?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</font></td>
		</tr>
		<thead>
			<tr>
				<th width="70%" align="center" colspan="9">INVENTORY</th>
				<th width="30%" align="center" colspan="3">INSPECTION</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<th width="6%" class="fxxsm">Date Acq'd</th>
				<th width="12%" class="fxxsm">Particulars</th>
				<th width="7%" class="fxxsm">Property (P.O. No.)</th>
				<th width="6%" class="fxxsm">Qty</th>
				<th width="7%" class="fxxsm">Unit Cost</th>
				<th width="7%" class="fxxsm">Total Cost</th>
				<th width="7%" class="fxxsm">Accumulated Depreciation</th>
				<th width="8%" class="fxxsm">Net Book Value(Total)</th>
				<th width="11%" class="fxxsm">Remarks</th>
				<th width="8%" class="fxxsm">Appraised Value (Total)</th>
				<th width="11%" class="fxxsm">Disposition</th>
				<th width="10%" class="fxxsm">Remarks (location, date added, etc)</th>
			</tr>
			
			<!-- For prototype only -->
			<!-- START -->
			<?php 
				for($ctr=0;$ctr<count($sample_data[$ctr]);$ctr++)
				{
			?>
					<tr>
					<?php 
						for($ctr2=0;$ctr2<count($sample_data[$ctr]);$ctr2++)
						{
					?>
						<td class="b-t-n b-b-n fxxsm"  align="center"><?php echo $sample_data[$ctr][$ctr2]; ?></td>		
					<?php 
						}
					?>
					</tr>
			<?php	
				}
			?>
			<tr>
				<td class="b-r-n b-b-n">&nbsp;</td>
				<td class="b-l-n b-r-n b-b-n fxxsm" align="center">Page Total:</td>
				<td class="b-l-n b-r-n b-b-n" colspan="3"></td>
				<td class="b-l-n b-r-n b-b-n fxxsm" align="center">86445.00</td>
				<td class="b-l-n b-r-n b-b-n fxxsm" align="center">77800.50</td>
				<td class="b-l-n b-r-n b-b-n fxxsm" align="center">8644.50</td>
				<td class="b-l-n b-r-n b-b-n fxxsm" >&nbsp;</td>
				<td class="b-l-n b-r-n b-b-n fxxsm" align="center">300.00</td>
				<td class="b-l-n b-b-n fxxsm" colspan="2"></td>
			</tr>
			<tr>
				<td class="b-t-n b-b-n" colspan="12"><br></td>
			</tr>
			<tr>
				<!-- <td class="b-r-n b-t-n b-b-n">&nbsp;</td> -->
				<td class="b-r-n b-t-n b-b-n fxxsm" colspan="2">CERTIFIED BY:</td>
				<td class="b-n">&nbsp;</td>
				<td class="b-n fxxsm"  colspan="3">VERIFIED BY:</td>
				<td class="b-n fxxsm">&nbsp;</td>
				<td class="b-n fxxsm" colspan="2" style="text-align: justify;">I CERTIFY that I have inspected each and every article enumerated in this report.</td>
				<td class="b-n"></td>
				<td class="b-l-n b-t-n b-b-n fxxsm" colspan="2" style="text-align: justify;">I CERTIFY that I have witnesed the dispostion og the asticles enumerated on this ________ day of ________ <?php echo $year?></td>
			</tr>
			<tr>
				<!-- <td class="b-r-n b-t-n b-b-n">&nbsp;</td> -->
				<td class="b-r-n b-t-n b-b-n fxxsm" colspan="2" align="center"><b><?php echo $certified_by?></b></td>
				<td class="b-n">&nbsp;</td>
				<td class="b-n fxxsm"  colspan="3" align="center"><b><?php echo $acc_unit?></b></td>
				<td class="b-n">&nbsp;</td>
				<td class="b-n fxxsm" colspan="2" align="center"><b><?php echo $inspector?></b></td>
				<td class="b-n"></td>
				<td class="b-l-n b-t-n b-b-n fxxsm" colspan="2"></td>
			</tr>
			<tr>
				<!-- <td class="b-r-n b-t-n b-b-n">&nbsp;</td> -->
				<td class="b-r-n b-b-n fxxsm" colspan="2" valign="top" align="center">Name and Signature</td>
				<td class="b-n">&nbsp;</td>
				<td class="b-l-n b-r-n b-b-n fxxsm" colspan="3" valign="top" align="center">Name/Signature-Head of the Accounting Unit</td>
				<td class="b-n">&nbsp;</td>
				<td class="b-l-n b-r-n b-b-n fxxsm" colspan="2" valign="top" align="center">Name and Signature of Inspector</td>
				<td class="b-n"></td>
				<td class="b-l-n b-t-n b-b-n fxxsm" colspan="2"></td>
			</tr>
			<tr>
				<td class="b-t-n b-b-n b-r-n" colspan="10"><br></td>
				<td class="b-l-n b-t-n b-b-n fxxsm" colspan="2" align="center"><b><?php echo $witness?></b></td>
			</tr>
			<tr>
			<!-- 	<td class="b-r-n b-t-n b-b-n">&nbsp;</td> -->
				<td class="b-r-n b-t-n fxxsm" colspan="2" align="center">Supply Officer II</td>
				<td class="b-n">&nbsp;</td>
				<td class="b-l-n b-r-n b-t-n fxxsm" colspan="3" align="center">Accountant III/Member-Disposal C'tee</td>
				<td class="b-n">&nbsp;</td>
				<td class="b-l-n b-r-n b-t-n fxxsm" colspan="2" align="center">Property Inspector</td>
				<td class="b-n"></td>
				<td class="b-l-n b-l-n b-b-n fxxsm" colspan="2" align="center">Name and Signature of Witness</td>
			</tr>
			<tr>
				<td class="b-t-n b-b-n" colspan="12"><br></td>
			</tr>
			<tr>
				<!-- <td class="b-r-n b-t-n b-b-n">&nbsp;</td> -->
				<td class="b-r-n b-b-n fxxsm" colspan="2" valign="top" align="center">Date</td>
				<td class="b-n">&nbsp;</td>
				<td class="b-l-n b-r-n b-b-n fxxsm" colspan="3" valign="top" align="center">Date</td>
				<td class="b-n">&nbsp;</td>
				<td class="b-n" colspan="2" valign="top" align="center">&nbsp;</td>
				<td class="b-n"></td>
				<td class="b-l-n b-b-n fxxsm" colspan="2" align="center">Date</td>
			</tr>
			<tr>
				<td class="b-r-n b-b-n fxxsm" colspan="2"></td>
				<td class="b-l-n b-b-n fxxsm" colspan="10">APPROVED BY:</td>
			</tr>
			<tr>
				<td class="b-r-n b-b-n b-t-n" colspan="3"></td>
				<td class="b-n fxxsm" colspan="3" align="center"><b><?php echo $authorized_official?></b></td>
				<td class="b-n" colspan="3"></td>
				<td class="b-l-n b-t-n b-b-n" colspan="3"></td>
			</tr>
			<tr>
				<td class="b-r-n b-t-n" colspan="3"></td>
				<td class="b-l-n b-r-n fxxsm" colspan="3" valign="top" align="center">Authorized Official</td>
				<td class="b-l-n b-r-n b-t-n" colspan="3">&nbsp;</td>
				<td class="b-l-n b-r-n fxxsm" align="center">Date</td>
				<td class="b-l-n b-t-n" colspan="2"></td>
			</tr>
			<!-- END -->
			
			
		</tbody>
	</table>
	
</div>