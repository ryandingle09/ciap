<?php 
	//For prototype purposes
	//START
	//
	$ict_items = array(
		array("Rene E. Fajardo", "Desktop/All in one/HP/1TB/Intel Core i5/1.8GHz/4GB DDR3", "06/15/2015"),
		array("Dominador R. Dayo", "Laptop/LENOVO/1TB/Intel Core i5/2.4GHz/8GB DDR3", "03/08/2016"),
		array("Lorina Lauriquez", "Desktop/All in one/HP/1TB/Intel Core i7/2.4GHz/8GB DDR3", "05/29/2015")
	);
	//END
?>

<div style="float: left;">
	<p align="center">
		<small>
			<font size="2">
				<b>INVENTORY OF ICT EQUIPMENT</b>
			</font>
		</small>
	</p>
</div>

<div>
	<table width="100%" border="1" cellpadding="3" style="border-collapse: collapse;">
		<thead>
			<tr>
				<th width="35%" align="center">Board/Division & Name of Staff</th>
				<th width="50%" align="center">Description<br>(Type/Brand/HDisk/Processor/RAM)</th>
				<th width="20%" align="center">Month/Year Acquired</th>
			</tr>
		</thead>
		<tbody>
			<?php 
				for($ctr=0;$ctr<count($ict_items);$ctr++)
				{
			?>
				<tr>
					<?php 
						for($ctr2=0;$ctr2<count($ict_items[$ctr]);$ctr2++)
						{
					?>
						<td align="center"><?php echo $ict_items[$ctr][$ctr2]; ?></td>		
					<?php 
						}
					?>
					</tr>
			<?php 
				}
			?>
			
		
		</tbody>
	</table>
</div>


