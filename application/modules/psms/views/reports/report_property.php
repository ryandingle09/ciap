<?php
	
	$data = $report['content'];
	
	$categories = array();
	$equipment1 = array( 'Fax Machine', 'Toshiba', 'TFP28', '96048YU87', '2015-09-02', 'QE-2016-001', 'unit', '7.600.00', 1, 2, 3, 4, 'deffective');
	$equipment2 = array( 'Binding Machine', 'Toshiba', 'TFP28', '96048YU87', '2015-09-02', 'QE-2016-001', 'unit', '7.600.00', 1, 2, 3, 4, 'deffective');
	$equipment3 = array( 'AVR', 'Toshiba', 'TFP28', '96048YU87', '2015-09-02', 'QE-2016-001', 'unit', '7.600.00', 1, 2, 3, 4, 'deffective');
	$equipment4 = array( 'AVR (Electronic Voltage Regulator)', 'Colored Monitor', 'TFP28', '96048YU87', '2015-09-02', 'QE-2016-001','unit', '7.600.00', 1, 2, 3, 4, 'deffective');
	$equipment5 = array( 'Nebook', 'Colored Monitor', 'TFP28', '96048YU87', '2015-09-02', 'QE-2016-001', 'unit', '7.600.00', 1, 2, 3, 4, 'deffective');
	$equipment6 = array( 'Projector', 'Colored Monitor', 'TFP28', '96048YU87', '2015-09-02', 'QE-2016-001', 'unit', '7.600.00', 1, 2, 3, 4, 'deffective');
	
	$classification1 = array(
		'name' => 'OFFICE EQUIPMENT (221)',
		'equipment' => array(
			$equipment5, $equipment2, $equipment1, $equipment1, $equipment2			
		)
	);
	$classification2 = array(
		'name' => 'IT EQUIPMENT & SOFTWARE (Acct 223)',
		'equipment' => array(
			$equipment6, $equipment2, $equipment4, $equipment1, $equipment2
		)
	);
	$classification3 = array(
		'name' => 'COMMUNICATION EQUIPMENT (228)',
		'equipment' => array(
			$equipment1, $equipment2, $equipment3,	$equipment4, $equipment5, $equipment6,
		)
	);

	$categories[] = $classification2;
	$categories[] = $classification1;
	$categories[] = $classification3;
	$categories[] = $classification2;
	$categories[] = $classification3;
	$categories[] = $classification2;
	$categories[] = $classification1;
?>
<style>

/*----------------- common ------------------*/

.title-pan{
	margin-bottom: 15px;
}
.title-pan .header1,
.title-pan .header2,
.title-pan .header3{
	font-size: 12px;
	margin: 0;
	text-align: center;
}

/*----------------- common ------------------*/

.tbl-pan table{
	border-collapse: collapse;
}
.tbl-pan th{
	font-size: 10px;
	text-align: center;
	padding: 2px;
	border: 1px solid black;
}
.tbl-pan td{
	border: 1px solid black;
	padding: 2px;
	font-size: 11px;
}



</style>
<div class="wrapper">
	<div class="title-pan">
		<h2 class="header1">REPORT OF THE PHYSICAL COUNT OF PROPERTY, PLANT AND EQUIPMENT</h2>
		<h2 class="header2">DAVAO</h2>
		<h2 class="header3">As of July 9, 2015</h2>
	</div>
	<div class="content-pan">
		<div class="tbl-pan">
			<table  width="100%" cellspacing="0" cellpadding="0" >
				<thead>
					<tr>
						<th rowspan="2">ARTICLE</th>
						<th colspan="3">DESCRIPTION</th>
						<th rowspan="2">DATE ACQUIRED</th>
						<th rowspan="2">PROPERTY NUMBER (CMDF)</th>
						<th rowspan="2" width="20">UNIT OF MEASURE</th>
						<th rowspan="2">UNIT VALUE</th>
						<th rowspan="2" width="20">BALANCE PER CARD</th>
						<th rowspan="2" width="20">PER COUNT</th>
						<th colspan="2">SHORT AGE/</br> VERAGE</th>
						<th rowspan="2">REMARKS</th>
					</tr>
					<tr>
						<th>BRAND</th>
						<th>MODEL/TYPE</th>
						<th>SERIAL NO.</th>
						<th>QUANTITY</th>
						<th>VALUE</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($categories as $category){ ?>
					<tr>
						<td colspan="14">&nbsp;</td>
					</tr>
					<tr>
						<td colspan="13"><b><?php echo $category['name']; ?></b></td>
					</tr>
						<?php foreach($category['equipment'] as $equipment){ ?>
						<tr>
							<td class="item"><?php echo $equipment[0]; ?></td>
							<td class="brand"><?php echo $equipment[1]; ?></td>
							<td class="model"><?php echo $equipment[2]; ?></td>
							<td class="serial-no"><?php echo $equipment[3]; ?></td>
							<td class="date-acquired"><?php echo $equipment[4]; ?></td>
							<td class="property-no"><?php echo $equipment[5]; ?></td>
							<td class="unit-measure" align="center"><?php echo $equipment[6]; ?></td>
							<td class="unit-value" align="right"><?php echo $equipment[7]; ?></td>
							<td class="balance" align="center"><?php echo $equipment[8]; ?></td>
							<td class="per-count" align="center"><?php echo $equipment[9]; ?></td>
							<td class="quantity" align="center"><?php echo $equipment[10]; ?></td>
							<td class="value" align="center"><?php echo $equipment[11]; ?></td>
							<td class="remarks" align="center"><?php echo $equipment[12]; ?></td>
						</tr>
						<?php } ?>
					<?php } ?>
				</tbody>
			</table>
		</div>
		
	</div>
</div>










