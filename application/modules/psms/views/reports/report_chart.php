
		<script src="<?php echo base_url() . PATH_JS ?>amcharts/amcharts.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . PATH_JS ?>amcharts/serial.js" type="text/javascript"></script>
       
        <!-- scripts for exporting chart as an image -->
        <!-- Exporting to image works on all modern browsers except IE9 (IE10 works fine) -->
        <!-- Note, the exporting will work only if you view the file from web server -->
        <!--[if (!IE) | (gte IE 10)]> -->
        <script type="text/javascript" src="<?php echo base_url() . PATH_JS ?>amcharts/plugins/export/export.js"></script>
        <link  type="text/css" href="<?php echo base_url() . PATH_JS ?>amcharts/plugins/export/export.css" rel="stylesheet">
        <!-- <![endif]-->
        

        <script>
            var chart;
            var chart = AmCharts.makeChart("chartdiv", {
                "theme": "dark",
                "titles": [
					{
						"size": 15,
						"text": '<?php echo $title . $report['filter'] ?>'
					}
				],
                "type": "serial",
                "dataProvider": <?php echo $report['data'] ?>,
                "valueAxes": [{                    
                    "unit": "%",
                    "position": "left",
                    "title": "Percent",
                }],
                "startDuration": 0.5,
                "graphs": <?php echo $report['graphs'] ?>,
                "plotAreaFillAlphas": 0.5,
                "depth3D": 0,
                "angle": 0,
                "categoryField": '<?php echo $report['category_field'] ?>',
                "categoryAxis": {
                    "gridPosition": "start"
                },
                "export": {
                	"enabled": true
                 }
            });

            
			
        </script>
        
<div>
	<div id="chartdiv" style="width: 100%; height: 400px;"></div>
</div>


		
	  
        

