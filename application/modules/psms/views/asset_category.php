<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Asset </a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Assets Category
		<span>Manage Asset Category</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p -l-md p-r-md">
	  	<?php 
	  		//if(!$permission_add)
	  		if(true)
	  			echo '<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_asset_category" onclick="modal_asset_category_init(\''.$url.'\')">CREATE NEW ASSET CATEGORY</button>'; ?>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
	<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="categories_table">
		<thead>
			<tr>
				<th width="">NAME</th>
				<th width="">DESCRIPTION</th>
				<th width="">USEFUL LIFE</th>
				<th width="80px" class="text-center">ACTIONS</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'psms_asset_category', 
		method		: 'delete_asset_category', 
		module		: '<?php echo PROJECT_PSMS ?>' });
</script>