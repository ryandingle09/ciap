<div class="page-title">
	<ul id="breadcrumbs">
		<li><a href="#">Home</a></li>
		<li><a href="#" class="active">Reports</a></li>
	</ul>
	
	<div class="row m-b-n">
		<div class="col s6 p-r-n">
			<h5>Reports
				<span>Manage Reports</span>
			</h5>
		</div>
		
		<div class="col s6 p-r-n right-align">
      		<label></label>
    	</div>
  	</div>
</div>

<div class="m-t-lg"></div>

<ul class="collapsible panel m-t-lg" data-collapsible="expandable">
	<li>
		<div class="collapsible-header active">Generate Report</div>
		<div class="collapsible-body" >
			<form id="report_form">
				<div class="form-basic">
    			    <div class="row m-n">
                        <div class="col s12">
                    		<h6>Report</h6>
                    		<div class="input-field">
                                <select name="report" id="report" class="selectize"  placeholder="Select report">
                                    <option value="">select report</option>
                                    <?php 
                                    foreach($reports as $reports)
                                    echo '<option value="'.$reports['resource_code'].'">'.$reports['resource_name'].'</option>';
                                    ?>
                                </select> 
                            </div>
                        </div>
                    </div>
                    <div class="row m-n">
                        <div class="col s6">
                            <h6>FROM</h6>
                            <div class="input-field">                                       
                                <input name="from" id="date_from"  class="validate datepicker" size="16" type="text"  data-date-format="yyyy-mm-dd" >
                            </div>                         
                        </div>
                         <div class="col s6">
                            <h6>TO</h6>
                            <div class="input-field">                                       
                                <input name="to" id="date_to"  class="validate datepicker" size="16" type="text"  data-date-format="yyyy-mm-dd" >
                            </div>  
                        </div>
                    </div>
                    <div class="row m-n">
                        <div id="gen_type" class="col s12">
                            <h6>Generate As</h6>

                            <div class="col s12">
                                <div class="col s2">
                                    <input checked name="generate" type="radio" id="report_type_pdf" value="PDF"/>
                                    <label for="report_type_pdf">PDF</label>
                                </div>
                                
                                <div id="excel" class="col s2">
                                    <input name="generate" type="radio" id="report_type_excel" value="Excel"/>
                                    <label for="report_type_excel">Excel</label>
                                </div>
                                
                                <!--<div id="chart" class="col s2">
                                    <input name="generate" type="radio" id="report_type_chart" value="Chart"/>
                                    <label for="report_type_chart">Chart</label>
                                </div>-->
                            </div>
                        </div>
                    </div>
                </div>
        		<div class="panel-footer right-align">
              		<a class="hide" id="gen_link" hre="#" target="_tab"></a>
              		<button class="btn waves-effect waves-light"  id="generate_report" name="action" type="button" value="Save">Generate Report</button>     
                </div>
			</form>
		</div>
	</li>
</ul>