<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Supplies</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Supplies Category
		<span>Manage Supplies Category</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p -l-md p-r-md">
	  	<?php 
	  		//if(!$permission_add)
	  		if(true)
	  			echo '<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_supplies_category" onclick="modal_supplies_category_init(\''.$url.'/add'.'\')">CREATE NEW SUPPLIES CATEGORY</button>'; ?>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
	<table cellpadding="0" cellspacing="0" class="tbl-basic table table-default table-layout-auto" id="categories_table">
		<thead>
			<tr>
				<th width="">NAME</th>
				<th width="">TYPE</th>
				<th width="">USEFUL LIFE</th>
				<th width="">DESCRIPTION</th>
				<th width="">UNIT OF MEASURE</th>
				<th width="">OPTIMUM REORDER QTY</th>
				<th width="80px" class="text-center">ACTIONS</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'mpis_financial_ratios', 
		method		: 'delete_financial_ratio', 
		module		: '<?php echo PROJECT_MPIS ?>' });
</script>