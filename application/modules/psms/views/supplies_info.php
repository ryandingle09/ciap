<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Supplies</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Supplies</h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="col s2">
	  	&nbsp;
	  </div>
	  <div class="col s10">
		  <div class="input-field inline p-l-md p-r-md">
		  	<select name="category" id="category" class="browser-default" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
		  		<option <?php if($classification=='All'){ echo 'selected'; }?> value="All">All Categories</option>
		    	<option <?php if($classification=='I'){ echo 'selected'; }?> value="I">Ink</option>
		    	<option <?php if($classification=='S'){ echo 'selected'; }?> value="S">Scissors</option>
		  	</select>
		  </div>
		  <div class="input-field inline p-l-md p-r-md">
				<?php 
					if($permission_add)
						echo '<button type="button" class="btn waves-effect waves-light md-trigger btn-success" id="add_supply" name="add_supply" data-modal="modal_supply" onclick="modal_supply_init(\''.$url.''.'\')">Create New</button>'
				?>
		  </div>
      </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table class="table table-default table-layout-auto" id="supplies_info_table">
  <thead>
	<tr>
		<th width="15%" style='text-align : center'>Date Acquired</th>
	    <th width="20%" style='text-align : center'>Item Name</th>
	    <th width="15%" style='text-align : center'>Refference No.</th>
	    <th width="10%" style='text-align : center'>Quantity</th>
	    <th width="15%" style='text-align : center'>Unit Cost</th>
	    <th width="15%" style='text-align : center'>Total Cost</th>
	    <th width="30%" style='text-align : center'>Actions</th>
	</tr>
	
  </thead>
  </table>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'psms_supplies_info', 
		method		: 'delete_supplies_info', 
		module		: '<?php echo PROJECT_PSMS ?>' });
	
</script>
