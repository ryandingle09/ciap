<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Asset Location</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Assets Locations
		<span>Manage Asset Location</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p -l-md p-r-md">
	  	<?php 
	  		//if(!$permission_add)
	  		if(true)
	  			echo '<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_asset_location" onclick="modal_asset_location_init(\''.$url.'\')">CREATE NEW ASSET LOCATION</button>'; ?>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
	<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto tbl-basic" id="locations_table">
		<thead>
			<tr>
				<th width="">LOCATION</th>
				<th width="">SITE</th>
				<th width="80px" class="text-center">ACTIONS</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'psms_asset_location', 
		method		: 'delete_asset_location', 
		module		: '<?php echo PROJECT_PSMS ?>' });
</script>