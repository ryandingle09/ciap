<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Assets</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Assets
		<span>Manage Assets</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p -l-md p-r-md">
	  	<?php 
	  		//if(!$permission_add)
	  		if(true)
	  			echo '<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_asset" onclick="modal_asset_init(\''.$url.'\')">CREATE NEW ASSET</button>'; ?>

	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="assets_table">
	<thead>
		<tr>
			<th width="100px">DATE AQUIRED</th>
			<th width="">PROPERTY NAME</th>
			<th width="">PROPERTY NO.</th>
			<th width="">REFERENCE NO.</th>
			<th width="">STATUS</th>	  
			<th width="">SEFULL LIFE</th>	    
			<th width="" class="text-center">ACTIONS</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
</div>
<script>
  var r = $('#assets_table tfoot tr');
	  r.find('th').each(function(){
	    $(this).css('padding', 8);
	  });
	  $('#assets_table thead').append(r);
	  $('#search_0').css('text-align', 'center');	
</script>

<script>
	var deleteObj = new handleData({ 
		controller	: 'psms_assets', 
		method		: 'delete_asset', 
		module		: '<?php echo PROJECT_PSMS ?>' });
</script>