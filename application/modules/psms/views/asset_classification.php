<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Asset Classification</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Assets Classification
		<span>Manage Asset Classification</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p -l-md p-r-md">
	  	<?php 
	  		//if(!$permission_add)
	  		if(true)
	  			echo '<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_asset_classification" onclick="modal_asset_classification_init(\''.$url.'\')">CREATE NEW ASSET CLASSIFICATION</button>'; ?>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
	<table cellpadding="0" cellspacing="0" class="tbl-basic table table-default table-layout-auto" id="classifications_table">
		<thead>
			<tr>
				<th width="">CLASSIFICATION</th>
				<th width="">CATEGORY</th>
				<th width="80px" class="text-center">ACTIONS</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'psms_asset_classification', 
		method		: 'delete_asset_classification', 
		module		: '<?php echo PROJECT_PSMS ?>' });
</script>