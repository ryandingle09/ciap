<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Requests and Issuances</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Requests and Issuances</h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="col s2">
	  	&nbsp;
	  </div>
	  <div class="col s10">
		  <div class="input-field inline p-l-md p-r-md">
				<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_ris_info" id="add_ris" name="add_ris" onclick="modal_ris_info_init('new')">Create New</button>
		  </div>
      </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table class="table table-default table-layout-auto" id="ris_table">
  <thead>
	<tr>
		<th width="20%" style='text-align : center'>RIS No.</th>
	    <th width="20%" style='text-align : center'>Request Date</th>
	    <th width="25%" style='text-align : center'>Requested By</th>
	    <th width="20%" style='text-align : center'>Issuance Date</th>
	    <th width="15%" style='text-align : center'>Actions</th>
	</tr>
  </thead>
  </table>
</div>

<script>

</script>
