<form id="ris_infos_form">
	<input type="hidden" name='ris_type' id='ris_type' value="<?php echo ISSET($type) ? $type : "" ?>">
	<div class="form-float-label">
		<div class="row m-n">
    		<div class="col s10">
    	  		<ul class="tabs">
    	   			<li class="tab col s4"><a class="active" href="#ris_info">RIS Info</a></li>
     	  			<li class="tab col s4"><a href="#requested_items">Requested Items</a></li>
       				<li class="tab col s4"><a href="#issued_items">Issued Items</a></li>
     			</ul>
	    	</div>
    	</div>
    	<div id="ris_info" class="col s12">
    		<div class="row m-n">
				<div class="col s6">
					<div class="input-field">
						<label class="active font-lg">Request Information</label>
					</div>
		  			<div class="col s6">
		  				<div class="input-field">
			   		 		<label class="active">Request Date <i class="fa fa-calendar"></i></label>
                    		<input name="ris_request_date" id="ris_request_date" required="" aria-required="true" class="validate datepicker" size="16" type="text" value="<?php echo ISSET($request_date) ? $request_date : "" ; ?>" data-date-format="yyyy-mm-dd" >
                		</div>
                	</div>
                	<div class="col s6">
                		<div class="input-field">
                			<input type="text" class="validate" required="" aria-required="true" name="ris_no" id="ris_no" value="<?php echo ISSET($ris_no) ? $ris_no : "" ?>"/>
							<label for="ris_no" class="active">RIS No.</label>
                		</div>
                	</div>
		   		   	<div class="col s9">
				      	<div class="input-field">
							<input type="text" class="validate" required="" aria-required="true" name="ris_requested_by" id="ris_requested_by" value="<?php echo ISSET($ris_requested_by) ? $ris_requested_by : "" ?>"/>
							<label for="ris_requested_by" class="active">Requested By</label>
						</div>
					</div>
					<div class="col s3">
						<div class="input-field">
							<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="" id="" name="" onclick="">...</button>
						</div>
					</div>
					<div class="col s12">
						<div class="input-field">
		                	<label class="active">Purpose</label>
		                	<textarea name="ris_purpose" id= "ris_purpose" aria-required="true" class="materialize-textarea"><?php echo ISSET($purpose) ? $purpose : "" ; ?></textarea>
	            		 </div>   
	            	</div>             					
				</div>
				
				<div class="col s6">
					<div class="input-field">
						<label class="active font-lg">Issuance Information</label>
					</div>
					<div class="col s6">
		  				<div class="input-field">
			   		 		<label class="active">Issuance Date <i class="fa fa-calendar"></i></label>
                    		<input name="ris_issuance_date" id="ris_issuance_date" required="" aria-required="true" class="validate datepicker" size="16" type="text" value="<?php echo ISSET($issuance_date) ? $issuance_date : "" ; ?>" data-date-format="yyyy-mm-dd" >
                		</div>
                	</div>
                	<div class="col s6">
                		<div class="input-field">
                			&nbsp;
                		</div>
                	</div>
		   		   	<div class="col s9">
				      	<div class="input-field">
							<input type="text" class="validate" required="" aria-required="true" name="ris_received_by" id="ris_received_by" value="<?php echo ISSET($ris_received_by) ? $ris_received_by : "" ?>"/>
							<label for="ris_received_by" class="active">Received By</label>
						</div>
					</div>
					<div class="col s3">
						<div class="input-field">
							<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="" id="" name="" onclick="">...</button>
						</div>
					</div>
					<div class="col s12">
						<div class="input-field">
		                	<label class="active">Remarks</label>
		                	<textarea name="ris_remarks" id="ris_remarks" aria-required="true" class="materialize-textarea"><?php echo ISSET($remarks) ? $remarks : "" ; ?></textarea>
	            		 </div>   
	            	</div>
					<div class="col s12">
						<label>File</label>
						<div class="file-field input-field">
					    	<div class="btn" class="tooltipped m-r-sm" data-position="bottom" data-delay="50" data-tooltip="Upload">
				        	<i class="flaticon-upload111 xs"></i>
				        	<span>Select File</span>
				        	<input type="file" id="ris_file">
				      	</div>
				  		<div class="file-path-wrapper">
				        	<input class="file-path validate" type="text">
				      	</div>
  				  	</div>
				</div>	            	  
				</div>	
    		</div>
    		<div class="md-footer default">
				<button class="btn waves-effect waves-light btn-primary" id="rinfo_next" name="rinfo_next">Next</button>
			</div>
    	</div>
    	<div id="requested_items" class="col s12">
    		<div class="row m-n">
				<div class="col s12">
					<div class="input-field">
						<label class="active font-lg">Requested Item</label>
					</div>
				</div>
			</div>
    		<div class="row m-n">
			<table class="table table-default table-layout-auto highlight" id="supply_items_table">
	 			<thead>
					<tr>
						<th width="5%" style="text-align : center">#</th>
						<th width="25%" style="text-align : center">Category</th>
		    			<th width="25%" style="text-align : center">Item Name</th>
		    			<th width="20%" style="text-align : center">UOM</th>
		    			<th width="20%" style="text-align : center">Qty Requested</th>
		    			<th width="10%" style="text-align : center">Action</th>
					</tr>
	 			</thead>
	 			<!-- remove tbody this is for prototype purposes -->
	 			<tbody>
	 				<tr>
	 					<td style='text-align : center'>1</td>
	 					<td>
	 						<select name="category" id="category" class="browser-default" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup" style="height:30px !important">
		  						<option value="0">A4 Bond Paper</option>
		    					<option value="1">Ink</option>
		    					<option value="2">Scissors</option>
		  					</select>
	 					</td>
	 					<td>
	 						<select name="item_name" id="item_name" class="browser-default" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup" style="height:30px !important">
		    					<option value="0"></option>
		    					<option value="1">Ink for Brother LC4000001</option>
		    					<option value="2">Ink for Brother LC3900001</option>
		    					<option value="3">Best Buy A4 Bond Paper</option>
		    					<option value="3">Extra Buy A4 Bond Paper</option>
		  					</select>
	 					</td>
	 					<td style='text-align : center'>rim</td>
	 					<td><input type="text" class="validate right-align" aria-required="true" style="height:30px !important"/></td>
	 					<td class='table-actions'>
	 						<a href='javascript:;'
								class='tooltipped tooltipped delete'
								data-position='bottom'
								data-delay='50'
								data-tooltip='Delete'>
							</a>
						</td>
	 				</tr>
	 				<tr>
	 					<td style='text-align : center'>2</td>
	 					<td>
	 						<select name="category" id="category" class="browser-default" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup" style="height:30px !important">
		  						<option value="0">A4 Bond Paper</option>
		    					<option value="1" selected="selected">Ink</option>
		    					<option value="2">Scissors</option>
		  					</select>
	 					</td>
	 					<td>
	 						<select name="item_name" id="item_name" class="browser-default" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup" style="height:30px !important">
		    					<option value="0"></option>
		    					<option value="1">Ink for Brother LC4000001 black</option>
		    					<option value="2">Ink for Brother LC3900001 cyan</option>
		    					<option value="3">Best Buy A4 Bond Paper</option>
		    					<option value="3">Extra Buy A4 Bond Paper</option>
		  					</select>
	 					</td>
	 					<td style='text-align : center'>piece</td>
	 					<td><input type="text" class="validate right-align" aria-required="true" style="height:30px !important"/></td>
	 					<td class='table-actions'>
	 						<a href='javascript:;'
								class='tooltipped tooltipped delete'
								data-position='bottom'
								data-delay='50'
								data-tooltip='Delete'>
							</a>
						</td>
	 				</tr>
	 			</tbody>
  			</table>
			</div>
			<div class="md-footer default">
				<a class="waves-effect waves-teal btn-flat" id="req_back">Back</a>
   				<button class="btn waves-effect waves-light btn-success" id="req_next" name="req_next">Next</button>
			</div>
    	</div>
    	<div id="issued_items" class="col s12">
    		<form id="item1">
	    		<div class="row m-n">
					<div class="col s12">
						<div class="input-field">
							<label class="active font-lg">A4 Bond Paper (12 rims)</label>
						</div>
					</div>
				</div>
				<div class="col s12">
					<table class="table table-default table-layout-auto highlight" id="acc_info_table">
					  	<thead>
							<tr>
								<th width="20%" style='text-align : center'>Date Aquired</th>
							    <th width="30%" style='text-align : center'>Item Name</th>
							    <th width="20%" style='text-align : center'>Refference No.</th>
							    <th width="15%" style='text-align : center'>Qty Available</th>
							    <th width="15%" style='text-align : center'>Qty Issued</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="padding: 0px !important;" class="center-align">08/09/2014</td>
								<td style="padding: 0px !important;" class="center-align">BEST BUY A4 BOND PAPER</td>
								<td style="padding: 0px !important;" class="center-align">0001</td>
								<td style="padding: 0px !important;" class="center-align">3</td>
								<td style="padding: 0px !important;"><input type="text" class="validate center-align" aria-required="true" style="height:30px !important" value="3"/></td>
							</tr>
							<tr>
								<td style="padding: 0px !important;" class="center-align">07/09/2015</td>
								<td style="padding: 0px !important;" class="center-align">EXTRA A4 BOND PAPER</td>
								<td style="padding: 0px !important;" class="center-align">0002</td>
								<td style="padding: 0px !important;" class="center-align">4</td>
								<td style="padding: 0px !important;"><input type="text" class="validate center-align" aria-required="true" style="height:30px !important" value="4"/></td>
							</tr>
							<tr>
								<td style="padding: 0px !important;" class="center-align">08/01/2015</td>
								<td style="padding: 0px !important;" class="center-align">BEST BUY A4 BOND PAPER</td>
								<td style="padding: 0px !important;" class="center-align">0003</td>
								<td style="padding: 0px !important;" class="center-align">4</td>
								<td style="padding: 0px !important;"><input type="text" class="validate center-align" aria-required="true" style="height:30px !important" value="4"/></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td><input type="text" class="validate center-align" aria-required="true" style="height:30px !important" value="11"/></td>
							</tr>
						  </tbody>
					</table>
				</div>
    		</form>
    		
    		<form id="item2">
	    		<div class="row m-n">
					<div class="col s12">
						<div class="input-field">
							<label class="active font-lg">Ink (4 pcs)</label>
						</div>
					</div>
				</div>
				<div class="col s12">
					<table class="table table-default table-layout-auto highlight" id="acc_info_table">
					  	<thead>
							<tr>
								<th width="20%" style='text-align : center'>Date Aquired</th>
							    <th width="30%" style='text-align : center'>Item Name</th>
							    <th width="20%" style='text-align : center'>Refference No.</th>
							    <th width="15%" style='text-align : center'>Qty Available</th>
							    <th width="15%" style='text-align : center'>Qty Issued</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td style="padding: 0px !important;" class="center-align">08/09/2014</td>
								<td style="padding: 0px !important;" class="center-align">Ink for Brother LC3900001</td>
								<td style="padding: 0px !important;" class="center-align">0003</td>
								<td style="padding: 0px !important;" class="center-align">4</td>
								<td style="padding: 0px !important;" class="center-align"><input type="text" class="validate center-align" aria-required="true" style="height:30px !important" value="4"/></td>
							</tr>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
								<td><input type="text" class="validate center-align" aria-required="true" style="height:30px !important" value="4"/></td>
							</tr>
						  </tbody>
					</table>
				</div>
    		</form>
    		<div class="md-footer default">
				<a class="waves-effect waves-teal btn-flat" id="iss_back">Back</a>
   				<button class="btn waves-effect waves-light btn-success" id="iss_done" name="iss_done">Done</button>
			</div>
    	</div>
    	
  	</div>
</form>


<script>
	var ris =[{ris_no:"0001", req_date:"02/14/2015", req_by:"Christian Gil Aquino", iss_date:"07/09/2014"},
	          {ris_no:"0002", req_date:"04/20/2016", req_by:"Allen Joel Doctor", iss_date:"08/10/2015"}
    		];
	
  	$(document).ready(function(){
   		$('ul.tabs').tabs();
   		var ris_type = $('#ris_type').val();
		if(ris_type == "1"){
	   		$('#ris_no').val(ris[0]['ris_no']);
			$('#ris_request_date').val(ris[0]['req_date']);
			$('#ris_requested_by').val(ris[0]['req_by']);
			$('#ris_purpose').val("For printing");
			$('#ris_issuance_date').val(ris[0]['iss_date']);
			$('#ris_received_by').val(ris[0]['req_by']);
			$('#ris_remarks').val("As soon as posible");
		}else if(ris_type == "2"){
			$('#ris_no').val(ris[1]['ris_no']);
			$('#ris_request_date').val(ris[1]['req_date']);
			$('#ris_requested_by').val(ris[1]['req_by']);
			$('#ris_purpose').val("For printing");
			$('#ris_issuance_date').val(ris[1]['iss_date']);
			$('#ris_received_by').val(ris[1]['req_by']);
			$('#ris_remarks').val("As soon as posible");
		}
  	});
  	
  	$('#rinfo_next').on('click', function(){
  		$('ul.tabs').tabs('select_tab', 'requested_items');
  	});
  	
  	$('#req_next').on('click', function(){
  		$('ul.tabs').tabs('select_tab', 'issued_items');
  	});
  	
  	$('#req_back').on('click', function(){
  		$('ul.tabs').tabs('select_tab', 'ris_info');
  	});

  	$('#iss_back').on('click', function(){
  		$('ul.tabs').tabs('select_tab', 'requested_items');
  	});

  	$('#iss_done').on('click', function(){
  		modal_ris_info.closeModal();
  	});
</script>