<form id="accountable-form" class="accountabe-form ps-default-form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="property_no" value="<?php echo ISSET($property_no) ? $property_no : "" ?>">
	<input type="hidden" name="attachment" value="temp">
	<input type="hidden" name="accountability_status_id" value="1">
	<div class="form-float-label">
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<input
						id="received_date"
						name="received_date"
						required=""
						aria-required="true"
						class="validate datepicker"
						size="16"
						type="text"
						value=""
						data-date-format="yyyy-mm-dd" >
					<label for="received_date">DATE RECEIVED</label>		  
			   	</div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<label class="active" for="office_id">OFFICE</label>
					<select name="office_id" id="office_id" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select office</option>
						<?php 
							foreach($offices as $office)
							{
								$selected = ( !EMPTY($info['ciap_office_code']) AND $info['ciap_office_code'] == $office['ciap_office_code']) ? "selected" : "";
								echo '<option '.$selected.' value="'.$office['ciap_office_code'].'">'.$office['ciap_office_name'].'</option>';
							}				
						?>
					</select>	     
			   	</div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<label class="active" for="employee_id">ACCOUNTABLE EMPLOYEE</label>
					<select name="employee_id" id="employee_id" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select accountable</option>
						<?php 
							foreach($employees as $employee)
							{
								$employee_name = $employee['first_name'].' '.$employee['middle_name'].' '.$employee['last_name'];
								$selected = ( !EMPTY($info['employee_id']) AND $info['employee_id'] == $employee['employee_no']) ? "selected" : "";
								echo '<option '.$selected.' value="'.$employee['employee_no'].'">'.$employee_name.'</option>';
							}				
						?>
					</select>	     
			   	</div>
		   	</div>
			<!--<div class="col s12">
				<div class="input-field rb-field">
			    	<input id="person" type="text" class="validate" value="<?php echo isset($serial_no)?$serial_no:''; ?>">
			    	<label for="person">ACCOUNTABLE PERSON</label>
			    	<a href="javascript:;"
						class="btn btn-success btn-re rb-field-btn md-trigger"
						data-modal="modal_search_employee"
						onclick="modal_search_employee_init()">
						...
					</a>
		        </div>
			</div>-->
		</div>
		<div class="row m-n">
			<div class="col s12">
				<div class="file-field input-field">
					<div class="btn btn-re btn-success">
						<span>FILE</span>
						<input name="attachment" type="file">
					</div>
					<div class="file-path-wrapper">
						<input class="file-path validate" type="text">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="md-footer default">
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	    <button class="btn waves-effect waves-light" id="btn_save" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	</div>
</form>

<script>
	//RELOAD SITE SELECT OPTIONS
	$(document).ready(function() {
	    $('#office_id').bind('change', function(){
	        var office_code = $(this).val();
	        var security = $('input[name="security"]').val();
	        
	        var param = {
	            office_code: office_code,
	            security: security
	        };
	        //GET ASSET CLASSIFICATION OF THE SELECTED CATEGORY
	       $.ajax({
	            type: "POST",
	            url: $base_url+'psms/psms_asset_accountabilities/get_accountable_options',
	            data: param,
	            dataType: 'json',
	            success: function(results){
	                selectize_reload('employee_id', results.options);
	            },
	        });
	    });    
    });    

	$(document).ready(function() {
		Materialize.updateTextFields();
	});
	$(document).ready(function() {
		$('#status').bind('change', function(){
			document.getElementById("btn-change-status").click();
		});
	});
	$(function(){
	  	$('#accountable-form').parsley();
	  	$('#accountable-form').submit(function(e) {
	   		e.preventDefault();
	    	
			if ( $(this).parsley().isValid() ) {
		  		var data = $(this).serialize();
		  	
			  	button_loader('btn_save', 1);
			  	$.post("<?php echo base_url() . PROJECT_PSMS ?>/psms_asset_accountabilities/process/", data, function(result) {
					if(result.flag == 0){
				  		notification_msg("<?php echo ERROR ?>", result.msg);
				  		button_loader('btn_save', 0);
					}
					else
					{
				  		notification_msg("<?php echo SUCCESS ?>", result.msg);
				  		button_loader("btn_save",0);
				  		modal_asset_accountability.closeModal();
				  		load_datatable('accountability_history_table', '<?php echo PROJECT_PSMS ?>/psms_assets/get_accountability_history');
					}
				}, 'json');
		    }
		});

		<?php if(ISSET($involvement_code)){ ?>
		$('.input-field label').addClass('active');
		<?php } ?>
	})

</script>