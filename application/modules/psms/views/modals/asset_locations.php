<div class="row">
	<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto tbl-basic" id="locations_table">
		<thead>
			<tr>
				<th width="">LOCATION</th>
				<th width="">SITE</th>
				<th width="80px" class="text-center">ACTIONS</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<div class="row">
	<div class="col s12">
		<div class="md-footer default">
		  	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
		    <button
		    	class="btn waves-effect waves-light md-trigger"
		    	id="save_asset"
		    	data-modal="modal_asset_location"
		    	onclick="modal_asset_location_init('<?php echo $url; ?>')">ADD LOCATION</button>
		</div>
	</div>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'psms_asset_location', 
		method		: 'delete_asset_location', 
		module		: '<?php echo PROJECT_PSMS ?>' });
</script>