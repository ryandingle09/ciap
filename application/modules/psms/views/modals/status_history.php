<form id="status-history-form" class="status-history-form ps-default-form tbl-realign">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<div class="form-float-label">
		<div class="row m-n">
			<div class="col s12 no-padding">
				<table id="status_history"
						cellpadding="0"
						cellspacing="0"
						class="tbl-basic no-dt-length no-dt-filter table table-default table-layout-auto" >
					<thead>
						<tr>
							<th width="%">DATE</th>
							<th width="%">STATUS</th>
							<th width="%">REMARKS</th>
							<th width="%">MODIFIED BY</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="md-footer default">
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" >CLOSE</a>
	</div>
</form>

