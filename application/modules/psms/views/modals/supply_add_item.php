<form id="add_item_form">
<!-- TODO security -->
	<div class="form-float-label">
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
						<label class="active" for="add_supply_item_category">Category</label>
					  	<select name="item_category" id="item_category" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
					  		<option value="">Select Category</option>
							<?php foreach ($supply_categories as $category): ?>
								<option <?php  if($category["supplies_category_id"]== $supplies_category_id){echo selected; }?> value="<?php echo $category["supplies_category_id"]?>"><?php echo ISSET($category["supplies_category_id"]) ? $category["supplies_category_name"] : "" ?></option>
					  		<?php endforeach; ?>
					  	</select>
				</div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<input type="text" class="validate" required="" aria-required="true" name="item_stock_no" id="item_stock_no" value="<?php echo ISSET($item_stock_no) ? $item_stock_no : "" ?>"/>
					<label for="item_stock_no">Stock No.</label>
				</div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<input type="text" class="validate" required="" aria-required="true" name="item_name" id="item_name" value="<?php echo ISSET($supply_stock_no) ? $supply_stock_no : "" ?>"/>
					<label for="item_name">Item Name</label>
				</div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
	                <label>Description</label>
					 <textarea name="item_description" required="" aria-required="true" class="materialize-textarea"><?php echo ISSET($description) ? $description : "" ; ?></textarea>
            	 </div>  
            </div> 
		</div>

		</div>
			<div class="md-footer default">
			<a class="waves-effect waves-teal btn-flat cancel_modal">Cancel</a>
			<?php if($permission_save): ?>
		    	<button class="btn waves-effect waves-light btn-primary" id="save_item" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
			<?php endif; ?>
		</div>

</form>
<script>

	$(function(){
		var $form = $('#add_item_form');

		$form.parsley();
		$form.submit(function(e){
			e.preventDefault();
		    
			if ( $(this).parsley().isValid() ) {
				
				var data = $(this).serialize();
				
				button_loader('save_item', 1);

				$.post("<?php echo base_url() . PROJECT_PSMS ?>/psms_supply_items/process/", data, function(result) {
					if(result.flag == 0){
					  notification_msg("<?php echo ERROR ?>", result.msg);
					  
					  button_loader('save_item', 0);

					} else {
					  notification_msg("<?php echo SUCCESS ?>", result.msg);
					  
					  button_loader("save_item",0);
					  modal_supply_add_item.closeModal();

					  search_items();
					}
					
				}, 'json');  
			}
		});
	});

	function search_items(){
		var search_type = "";
		var category_id = $('#supply_category').val();

 		if($("#name").is(":checked"))
		{
			search_type = "name";
		}
		else
		{
			search_type = "all";
		}
		
		var search_str = $('#search_input').val();
		load_datatable('supply_items_table', '<?php echo PROJECT_PSMS ?>/psms_supply_items/get_items?search_type='+search_type+'&search_category='+category_id+'&search_str='+search_str);
		$('#supply_items_table_filter').hide();
	}
</script>