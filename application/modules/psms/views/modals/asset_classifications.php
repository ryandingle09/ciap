<div class="row">
	<table cellpadding="0" cellspacing="0" class="tbl-basic table table-default table-layout-auto" id="classifications_table">
		<thead>
			<tr>
				<th width="">CLASSIFICATION</th>
				<th width="">CATEGORY</th>
				<th width="80px" class="text-center">ACTIONS</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<div class="row">
	<div class="col s12">
		<div class="md-footer default">
		  	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
		    <button
		    	class="btn waves-effect waves-light md-trigger"
		    	id="save_asset"
		    	data-modal="modal_asset_classification"
		    	onclick="modal_asset_classification_init('<?php echo $url; ?>')">ADD CLASSIFICATION</button>
		</div>
	</div>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'psms_asset_classification', 
		method		: 'delete_asset_classification', 
		module		: '<?php echo PROJECT_PSMS ?>' });
</script>