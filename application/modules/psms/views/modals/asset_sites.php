 <div class="row">
	<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto tbl-basic" id="sites_table">
		<thead>
			<tr>
				<th width="">SITE NAME</th>
				<th width="">DESCRIPTION</th>
				<th width="80px" class="text-center">ACTIONS</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>
</div>
<div class="row">
	<div class="col s12">
		<div class="md-footer default">
		  	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
		    <button
		    	class="btn waves-effect waves-light md-trigger"
		    	id="save_asset"
		    	data-modal="modal_asset_site"
		    	onclick="modal_asset_site_init('<?php echo $url; ?>')">ADD SITE</button>
		</div>
	</div>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'psms_asset_site', 
		method		: 'delete_asset_site', 
		module		: '<?php echo PROJECT_PSMS ?>' });
</script>