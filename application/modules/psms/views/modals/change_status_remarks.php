<form id="change-status-form" class="change-status-form ps-default-form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<div class="form-float-label">
		<div class="row m-n">
			<div class="col s2 no-border">
				<div class="n">
					<i class="material-icons">error</i>
				</div>
			</div>
			<div class="col s10">
				<p class="p">Status Changed
				<span>You have changed the status of asset from "For Disposal" to "Disposed"</span></p>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<textarea id="textarea1" class="materialize-textarea" length="120"></textarea>
					<label for="textarea1">REMARKS <i class="i">Enter reason for changing the status</i></label>
				</div>
			</div>
		</div>
	</div>
	<div class="md-footer default">
	  	<a class="btn-flat cancel_modal" >Cancel</a>
	    <button class="btn waves-effect waves-light" id="save_asset" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	</div>
</form>