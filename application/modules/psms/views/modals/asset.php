<form id="asset_form" class="asset_form ps-default-form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<div class="form-float-label">
		<div class="row m-n">
			<div class="col s6">
				<p class="head-genInfo">GENERAL INFORMATION</p>
			</div>
			<div class="col s3">
				<div class="input-field">
					<label class="active" for="condition">CONDITION</label>
					<select name="asset_condition_id" id="condition" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select condition</option>
						<?php 
							foreach($conditions as $condition)
							{
								$selected = ( !EMPTY($info['asset_condition_id']) AND $info['asset_condition_id'] == $condition['asset_condition_id']) ? "selected" : "";
								echo '<option '.$selected.' value="'.$condition['asset_condition_id'].'">'.$condition['asset_condition_name'].'</option>';
							}				
						?>
					</select>	     
			   	</div>
			</div>
			<div class="col s3">
				<div class="input-field">
					<label class="active" for="status">STATUS</label>
					<select name="asset_status_id"
							id="status" 
							required=""
							aria-required="true" 
							class="selectize"
							data-parsley-required="true" 
							data-parsley-validation-threshold="0"
							data-parsley-trigger="keyup"
							data-modal="modal_change_status_remarks"
							onclick="modal_change_status_remarks_init()">
						<option value="">select status</option>
						<?php 
							foreach($statuses as $status)
							{
								$selected = ( !EMPTY($info['asset_status_id']) AND $info['asset_status_id'] == $status['asset_status_id']) ? "selected" : "";
								echo '<option '.$selected.' value="'.$status['asset_status_id'].'">'.$status['asset_status_name'].'</option>';
							}				
						?>			
					</select>
					<a  href="javascript:;"
						id="btn-change-status"
						class="md-trigger hidden tooltipped" 
						data-modal="modal_change_status_remarks"
						data-position="bottom"
						data-delay="50"
						data-tooltip="settings status"
						onclick="modal_change_status_remarks_init()"
					></a>
			   	</div>
			</div>
		</div>
		<?php if($action == 'edit'){ ?>
		<div class="row m-n">
			<div class="col s12 vh">
				<a href="javascript:;"
					class="md-trigger vh-link"
					data-modal="modal_status_history"
					onclick="modal_status_history_init()">
					VIEW HISTORY
				</a>
			</div>
		</div>
		<?php } ?>
		<div class="row m-n">
			<div class="col s4">
		        <div class="input-field">
			    	<input id="property_no" name="property_no" type="text" class="validate active" value="<?php echo isset($info['property_no'])?$info['property_no']:''; ?>">
			    	<label for="property_no">PROPERTY NO.</label>
		        </div>
		        <div class="line-btm"></div>
		        <div class="input-field">
			    	<input id="serial_no" type="text" name="property_serial_no" class="validate" value="<?php echo isset($info['property_serial_no'])?$info['property_serial_no']:''; ?>">
			    	<label for="serial_no">SERIAL NO.</label>
		        </div>
		       <div class="line-btm"></div>
			</div>
			<div class="col s4">
				<div class="input-field">
					<label class="active" for="category">CATEGORY</label>
					<select name="asset_category_id" id="category" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select category</option>
						<?php 
							foreach($categories as $category)
							{
								$selected = ( !EMPTY($info['asset_category_id']) AND $info['asset_category_id'] == $category['asset_category_id']) ? "selected" : "";
								echo '<option '.$selected.' value="'.$category['asset_category_id'].'">'.$category['asset_category_name'].'</option>';
							}				
						?>
					</select>
				</div>
				<div class="input-field rb-field">
					<label class="active" for="classification">CLASSIFICATION</label>
					<select name="asset_classification_id" id="classification" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select classification</option>
						<?php 
							foreach($classifications as $classification)
							{
								$selected = ( !EMPTY($info['asset_classification_id']) AND $info['asset_classification_id'] == $classification['asset_classification_id']) ? "selected" : "";
								echo '<option '.$selected.' value="'.$classification['asset_classification_id'].'">'.$classification['asset_classification_name'].'</option>';
							}				
						?>
					</select>
					<a href="javascript:;"
						class="rb-field-btn md-trigger tooltipped"
						data-modal="modal_classifications"
						data-position="bottom"
						data-delay="50"
						data-tooltip="classification settings"
						onclick="modal_classifications_init()">
						<i class="small material-icons i-gray">settings</i>
					</a>
				</div>
			</div>
			<div class="col s4">
		       <div class="input-field no-margin">
		       		<?php $src = ($action == 'edit')?'sample-mac.jpg':'img-default.jpg'; ?>
		        	<div class="img-pan">
		        		<div class="i">
		        			<img src="<?php echo base_urL().PATH_IMAGES.$src; ?>" />
		        		</div>
		        		<div class="p">
	        				<a href="javascript:;" class="btn-rm">REMOVE</a>
	        				<a href="javascript:;" class="btn-bp">BROWSE</a>
		        		</div>
		        	</div>
		        </div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s8">
				<div class="input-field">
			    	<input id="property_name" name="property_name" type="text" class="validate" value="<?php echo isset($info['property_name'])?$info['property_name']:''; ?>">
			   		<label for="property_name">PROPERTY NAME</label>		     
			   	</div>
		        <div class="input-field">
		        	<textarea id="property_desc" name="property_desc" class="materialize-textarea"><?php echo isset($info['property_desc'])?$info['property_desc']:''; ?></textarea>
          			<label for="property_desc">NOTES</label>
		        </div>
			</div>
			<div class="col s4">
				<p class="head-genInfo">LOCATION INFORMATION</p>
				
				<div class="input-field rb-field">
					<label class="active" for="site">SITE</label>
					<select name="site" id="site" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select site</option>
						<?php 
							foreach($sites as $site)
							{
								$selected = ( !EMPTY($info['site_id']) AND $info['site_id'] == $site['site_id']) ? "selected" : "";
								echo '<option '.$selected.' value="'.$site['site_id'].'">'.$site['site_name'].'</option>';
							}				
						?>
					</select>
					<a
						href="javascript:;"
						type="button"
						class="rb-field-btn md-trigger tooltipped"
						data-modal="modal_sites"
						data-position="bottom"
						data-delay="50"
						data-tooltip="sites settings"
						onclick="modal_sites_init('<?php echo $url; ?>')">
						<i class="small material-icons i-gray">settings</i>
					</a>
				</div>

				<div class="input-field rb-field">
					<label class="active" for="location">LOCATION</label>
					<select name="asset_location_id" id="location" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select location</option>
						<?php 
							foreach($locations as $location)
							{
								$selected = ( !EMPTY($info['asset_location_id']) AND $info['asset_location_id'] == $location['asset_location_id']) ? "selected" : "";
								echo '<option '.$selected.' value="'.$location['asset_location_id'].'">'.$location['asset_location_name'].'</option>';
							}				
						?>
					</select>
					<a href="" class="rb-field-btn">
						<a href="javascript:;"
						class="rb-field-btn md-trigger tooltipped"
						data-modal="modal_locations"
						data-position="bottom"
						data-delay="50"
						data-tooltip="locations settings"
						onclick="modal_locations_init()">
						<i class="small material-icons i-gray">settings</i>
					</a>
					</a>
				</div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s12">
				<p class="head-genInfo">AQUISITION INFORMATION</p>
			</div>
		</div>	
		<div class="row m-n">
			<div class="col s4">
			   	<div class="input-field">
				   	<label for="date_acquired">DATE AQUIRED <i class="fa fa-calendar"></i></label>				
					<input
						id="date_acquired"
						name="property_acquired_date"
						required=""
						aria-required="true"
						class="validate datepicker"
						size="16"
						type="text"
						value="<?php echo isset($info['property_acquired_date'])?$info['property_acquired_date']:''; ?>"
						data-date-format="yyyy-mm-dd" >
				</div>
			</div>
			<div class="col s2">
				<div class="input-field">
			    	<input id="reference_no" name="property_ref_no" type="text" class="validate" value="<?php echo isset($info['property_ref_no'])?$info['property_ref_no']:''; ?>">
			   		<label for="reference_no">REFERENCE NO.</label>		     
			   	</div>
			</div>
			<div class="col s2">
				<div class="input-field">
			    	<input id="unit_cost" name="property_unit_cost" type="text" class="validate" value="<?php echo isset($info['property_unit_cost'])?$info['property_unit_cost']:''; ?>">
			   		<label for="unit_cost">UNIT COST</label>		     
			   	</div>
			</div>
			<?php if($info){ ?>
			<div class="col s4">
			   	<div class="input-field">
					<label class="active" for="accountable_officer">ACCOUNTABLE OFFICER</label>
					<select name="property_accountable_officer" id="accountable_officer" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<?php if(isset($accountable_officer)){ ?>
						<option selected="selected" value="3"><?php echo $accountable_officer; ?></option>
						<?php } ?>
						<option value="">select accountable officer</option>
						<option value="1">Officer 1</option>
						<option value="2">Officer 2</option>
						<option value="3">Officer 3</option>
					</select>
				</div>
			</div>
			<?php } ?>
		</div>
		<div class="row m-n">
			<div class="col s4">
				<div class="input-field">
			    	<input id="make" name="property_make" type="text" class="validate" value="<?php echo isset($info['property_make'])?$info['property_make']:''; ?>">
			   		<label for="make">MAKE</label>		     
			   	</div>
			</div>
			<div class="col s4">
				<div class="input-field">
			    	<input id="model" name="property_model" type="text" class="validate" value="<?php echo isset($info['property_model'])?$info['property_model']:''; ?>">
			   		<label for="model">MODEL</label>		     
			   	</div>
			</div>
		</div>
		<?php if(!empty($info) and empty($accountabilities)){ ?>
		<div class="row m-n">
			<div class="col s12">
				<p class="head-genInfo">ACCOUNTABILITY INFORMATION</p>
			</div>
		</div>
		<div class="row m-n">
			<div class="input-field">
				<div class="add-asset-btn-pan">
					 <i class="i material-icons large">assignment_ind</i>
					 <i class="p">No accountable person</i>
					 <button 
					 	type="button"
					 	class="btn btn-success b md-trigger"
						data-modal="modal_accountable"
						onclick="modal_accountable_init('<?php echo $security;?>')">Add Accountable Person</button>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if(!empty($accountabilities)){ ?>
		<div class="row m-n">
			<table cellpadding="0" cellspacing="0" class="tbl-basic table table-default table-layout-auto" id="asset_history_table">
				<thead>
					<tr>
						<th width=""></th>
						<th width="">DATE RECEIVED</th>
						<th width="">ACCOUNTABILITY PERSON</th>
						<th width="">FILE</th>
						<th width="80px;">ACTION</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>	
		</div>
		<?php } ?>
	</div>
	<div class="md-footer default">
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	    <button class="btn waves-effect waves-light" id="btn_save" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	</div>
</form>

<script>
$(document).ready(function() {
	Materialize.updateTextFields();
});
$(document).ready(function() {
	$('#status').bind('change', function(){
		document.getElementById("btn-change-status").click();
	});
});
$(function(){
  	$('#asset_form').parsley();
  	$('#asset_form').submit(function(e) {
   		e.preventDefault();
    	
		if ( $(this).parsley().isValid() ) {
	  		var data = $(this).serialize();
	  	
		  	button_loader('btn_save', 1);
		  	$.post("<?php echo base_url() . PROJECT_PSMS ?>/psms_assets/process/", data, function(result) {
				if(result.flag == 0){
			  		notification_msg("<?php echo ERROR ?>", result.msg);
			  		button_loader('btn_save', 0);
				}
				else
				{
			  		notification_msg("<?php echo SUCCESS ?>", result.msg);
			  		button_loader("btn_save",0);
			  		modal_asset.closeModal();
			  		load_datatable('assets_table', '<?php echo PROJECT_PSMS ?>/psms_assets/get_asset_list');
				}
			}, 'json');
	    }
	});

	<?php if(ISSET($involvement_code)){ ?>
	$('.input-field label').addClass('active');
	<?php } ?>
})

</script>