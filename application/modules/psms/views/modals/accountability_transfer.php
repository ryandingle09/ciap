<form id="asset_form" class="accountability-transfer-form ps-default-form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<div class="form-float-label">
		<div class="row m-n">
			<div class="col s6">
				<p class="head-genInfo">CURRENT ACCOUNTABILITY INFO</p>
		        <div class="input-field">
		          <div class="info-pan">
			          	<p>DATE RECEIVED
			          		<span>2016/12/02</span>
			          	</p>
			          	<p>OFFICE
			          		<span>Main Office</span>
			          	</p>
			          	<p>ACCCOUNTABLE PERSON
			          		<span>Atty. Mike Pangilinan</span>
			          	</p>
			          	<p>FILE
			          		<a href="#"><i class="iblue">accountability_form.pdf</i></a>
			          	</p>
			         </div>
		          </textarea>
		          <label for="textarea1"></label>
		        </div>
			</div>
			<div class="col s6">
				<p class="head-genInfo">TRANSTER TO</p>
				<div class="input-field">
					<input
						id="date_received"
						name="date_received"
						required=""
						aria-required="true"
						class="validate datepicker"
						size="16"
						type="text"
						value=""
						data-date-format="yyyy-mm-dd" >
					<label for="date_received">DATE RECEIVED</label>		  
			   	</div>
			   	<div class="input-field">
					<label class="active" for="condition">OFFICE</label>
					<select name="condition" id="condition" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select office</option>
						<option value="2">Main Office - Makati</option>
						<option value="2">Davao Branch</option>
						<?php if(isset($office)){ ?>
						<option selected="selected" value="3"><?php echo $office; ?></option>
						<?php } ?>
					</select>	     
			   	</div>
			  	<div class="input-field rb-field">
			    	<input id="person" type="text" class="validate" value="<?php echo isset($serial_no)?$serial_no:''; ?>">
			    	<label for="person">ACCOUNTABLE PERSON</label>
			    	<a href="javascript:;"
						class="btn btn-success btn-re rb-field-btn md-trigger tooltipped"
						data-tooltip="search employee"
						data-position="bottom"
						data-delay="50" 
						data-modal="modal_search_employee"
						onclick="modal_search_employee_init()">
						...
					</a>
			   	</div>
			   	<div class="file-field input-field">
					<div class="btn  btn-re btn-success">
						<span>select file</span>
						<input type="file">
					</div>
					<div class="file-path-wrapper">
						<input class="file-path validate" type="text">
					</div>
			    </div>
			</div>
		</div>
	</div>
	<div class="md-footer default">
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	    <button class="btn waves-effect waves-light" id="save_asset" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	</div>
</form>
