<form id="ad-form" class="ad-form ps-default-form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<div class="form-float-label">
		<div class="row m-n">
			<div class="col s6">
				<div class="input-field">
					<input
						name="date_aquired"
						required=""
						aria-required="true"
						class="validate datepicker"
						size="16"
						type="text"
						value=""
						data-date-format="yyyy-mm-dd" >
					<label for="model">DEACTIVATION DATE</label> 
			   	</div>
			</div>
			<div class="col s6">
				<a href="javascript:;"
					type="button"
					class="btn btn-re btn-success md-trigger"
					data-modal="modal_accountability_deactivate"
					onclick="modal_accountability_deactivate_init('<?php echo $url; ?>')">
					REUSABLE
				</a>
				<a href="javascript:;"
					type="button"
					class="btn btn-de md-trigger"
					data-modal="modal_accountability_deactivate"
					onclick="modal_accountability_deactivate_init('<?php echo $url; ?>')">
					DEFECTIVE
				</a>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<textarea id="textarea1" class="materialize-textarea" length="120"></textarea>
					<label for="textarea1">REMARKS <i class="i">Enter reason for accountability deactivation</i></label>
				</div>
			</div>
		</div>
	</div>
	<div class="md-footer default">
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	    <button class="btn waves-effect waves-light" id="save_asset" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	</div>
</form>