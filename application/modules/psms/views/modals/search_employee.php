<form id="accountabe-form" class="accountabe-form ps-default-form tbl-realign">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<div class="form-float-label">
		<div class="row m-n">
			<!--<div class="col s6">
				<p>Search: </p>
			</div>-->
			<div class="col s6">
				<p>
			      <input class="with-gap" name="group1" type="radio" id="test1"  />
			      <label for="test1">Name Only</label>
			    </p>
			</div>
			<div class="col s6">
				<p>
			      <input class="with-gap" name="group1" type="radio" id="test2"  />
			      <label for="test2">All Columns</label>
			    </p>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s12 no-padding">
				<table cellpadding="0" cellspacing="0" class="tbl-basic table table-default table-layout-auto" id="search_employee_table">
					<thead>
						<tr>
							<th width="20%">EMPLOYEE NO.</th>
							<th width="40%">NAME</th>
							<th width="20%">OFFICE</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width="">000384</td>
							<td width="">John Smith</td>
							<td width="">AFMD</td>
						</tr>
						<tr>
							<td width="">000444</td>
							<td width="">John Nash</td>
							<td width="">POBC</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="md-footer default">
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	    <button class="btn waves-effect waves-light" id="save_asset" value="">OK</button>
	</div>
</form>

