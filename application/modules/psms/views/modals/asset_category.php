<form id="asset_category_form" class="asset_category_form ps-default-form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<div class="form-float-label">
		<div class="row m-n">
			<div class="col s8">
				<div class="input-field">
					<input id="name" name="asset_category_name" required="true" aria-required="true" type="text" class="validate active" value="<?php echo isset($info['asset_category_name'])?$info['asset_category_name']:''; ?>">
			    	<label for="name">NAME</label>
				</div>
			</div>
			<div class="col s4">
				<div class="input-field">
					<input id="useful_life" name="asset_category_useful_life" type="text" class="validate active" value="<?php echo isset($info['asset_category_useful_life'])?$info['asset_category_useful_life']:''; ?>">
			    	<label for="useful_life">USEFUL LIFE (YEARS)</label>
		        </div>
			</div>
		</div>
		<div class="row m-n">
			<!--<div class="col s8">
				<div class="input-field">
					<input
						id="effectivity_date"
						name="effectivity_date"
						required=""
						aria-required="true"
						class="validate datepicker"
						size="16"
						type="text"
						value="<?php echo isset($info['effectivity_date'])?$info['effectivity_date']:''; ?>"
						data-date-format="yyyy-mm-dd" >
					<label for="effectivity_date">EFFECTIVITY DATE</label>
		        </div>
			</div>-->
		</div>
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<textarea id="description" name="asset_category_desc" class="materialize-textarea"><?php echo isset($info['asset_category_desc'])?$info['asset_category_desc']:''; ?></textarea>
					<label for="description">DESCRIPTION</label>
				</div>
			</div>
		</div>
	</div>
	<div class="md-footer default">
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	    <button class="btn waves-effect waves-light" id="save_btn" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	</div>
</form>
<script>

$(function(){
  
  var $form = $('#asset_category_form');
  
  $form.parsley();
  
  $form.submit(function(e) {
	  
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {
	  var data = $(this).serialize();
	  
	  button_loader('save_btn', 1);
	  
	  $.post("<?php echo base_url() . PROJECT_PSMS ?>/psms_asset_category/process/", data, function(result) {
		  
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  
		  button_loader('save_btn', 0);
		  
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  
		  button_loader("save_btn",0);
		  
		  modal_asset_category.closeModal();
		  
		  load_datatable('categories_table', '<?php echo PROJECT_PSMS ?>/psms_asset_category/get_category_list');
		}
		
	  }, 'json');       
    }
  });
  

});
</script>