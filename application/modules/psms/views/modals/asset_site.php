<form id="asset_site_form" class="asset_site_form ps-default-form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<div class="form-float-label">
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">				
			    	<input id="site_name" name="site_name" type="text" class="validate" required="true" aria-required="true"  value="<?php echo isset($info['site_name'])?$info['site_name']:''; ?>">
			    	<label for="site_name" class="active">SITE NAME</label>
		        </div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
		        	<textarea id="description" name="description" class="materialize-textarea"><?php echo isset($info['site_desc'])?$info['site_desc']:''; ?></textarea>
          			<label for="description">DESCRIPTION</label>
		        </div>
			</div>
		</div>
	</div>
	<div class="md-footer default">
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	  	
	  	<?php if($permission_save): ?>
	    	<button class="btn waves-effect waves-light" id="save_btn" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	    <?php endif; ?>
	</div>
</form>


<script>

$(function(){
  
  var $form = $('#asset_site_form');
  
  $form.parsley();
  
  $form.submit(function(e) {
	  
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {


	  var data = $(this).serialize();
	  
	  button_loader('save_btn', 1);
	  
	  $.post("<?php echo base_url() . PROJECT_PSMS ?>/psms_asset_site/process/", data, function(result) {
		  
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  
		  button_loader('save_btn', 0);
		  
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  
		  button_loader("save_btn",0);
		  
		  modal_asset_site.closeModal();
		  
		  load_datatable('sites_table', '<?php echo PROJECT_PSMS ?>/psms_asset_site/get_sites');
		}
		
	  }, 'json');       
    }
  });
  

});
</script>