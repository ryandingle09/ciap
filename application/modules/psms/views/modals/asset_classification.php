<form id="asset_classification_form" class="asset_classification_form ps-default-form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<div class="form-float-label">
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
			    	<label class="active" for="category">CATEGORY</label>
					<select name="asset_category_id" id="category" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select category</option>
						<?php 
							foreach($categories as $category)
							{
								$selected = ( !EMPTY($info['asset_category_id']) AND $info['asset_category_id'] == $category['asset_category_id']) ? "selected" : "";
								echo '<option '.$selected.' value="'.$category['asset_category_id'].'">'.$category['asset_category_name'].'</option>';
							}				
						?>
					</select>	     
		        </div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
		        	<textarea id="cl-name" name="asset_classification_name" class="materialize-textarea"><?php echo isset($info['asset_classification_name'])?$info['asset_classification_name']:''; ?></textarea>
          			<label for="cl-name">CLASSIFICATION NAME</label>
		        </div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s12" >
				<div class="input-field" style="margin-bottom:10px;">
					<a href="javascript:;"
						class="btn btn-success waves-effect waves-light">
						<i class="material-icons left">done</i> ACTIVE
					</a>
				</div>
			</div>
		</div>
	</div>
	<div class="md-footer default">
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	    <button class="btn waves-effect waves-light" id="save_btn" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	</div>
</form>
<script>

$(function(){
  
  var $form = $('#asset_classification_form');
  
  $form.parsley();
  
  $form.submit(function(e) {
	  
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {
	  var data = $(this).serialize();
	  
	  button_loader('save_btn', 1);
	  
	  $.post("<?php echo base_url() . PROJECT_PSMS ?>/psms_asset_classification/process/", data, function(result) {
		  
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  
		  button_loader('save_btn', 0);
		  
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  
		  button_loader("save_btn",0);
		  
		  modal_asset_classification.closeModal();
		 				
		  load_datatable2({
			table_id: 'classifications_table',
			sAjaxSource: '<?php echo base_url() . PROJECT_PSMS ?>/psms_asset_classification/get_classification_list',
			bPaginate: false,
			bFilter: false,
			bInfo:false
		  });
		}
		
	  }, 'json');       
    }
  });
  

});
</script>