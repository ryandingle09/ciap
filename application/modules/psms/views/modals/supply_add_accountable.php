<form id="add_accountable_form m-b-lg">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
	<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
	<div class="form-float-label">
		
		<div class="row m-n">
			<div class="col s6">
		  		<div class="input-field">
			    	<label>Received Date <i class="fa fa-calendar"></i></label>
                    <input name="add_accountable_received_date" required="" aria-required="true" class="validate datepicker" size="16" type="text" value="<?php echo ISSET($received_date) ? $received_date : "" ; ?>" data-date-format="yyyy-mm-dd" >
                </div>
            </div>
        	<div class="col s6">
            	<div class="input-field">
                	<input type="text" class="validate" required="" aria-required="true" name="add_accountable_refference_no" id="add_accountable_refference_no" value="<?php echo ISSET($supply_refference_no) ? $supply_refference_no : "" ?>"/>
					<label for="add_accountable_refference_no">Refference No</label>
                </div>
          	</div>
		</div>
		
		<div class="row m-n">
			<div class="col s12">
			<div class="input-field">
					<label class="active" for="add_accountable_office">Category</label>
				  	<select name="add_accountable_office" id="add_accountable_office" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
				  		<option value="Select Office">Select Office</option>
				    	<option <?php if($classification=='HR'){ echo 'selected'; }?> value="HR">Human Resource Office</option>
				    	<option <?php if($classification=='IT'){ echo 'selected'; }?> value="IT">IT Office</option>
				  		<option <?php if($classification=='EN'){ echo 'selected'; }?> value="EN">Engineering Office</option>
				  	</select>
			</div>
			</div>
		</div>
	
		<div class="row m-n">
			<div class="col s12">
		      	<div class="input-field col s9">
					<input type="text" class="validate" required="" aria-required="true" name="accountable_person_name" id="accountable_person_name" value="<?php echo ISSET($accountable_person_name) ? $accountable_person_name : "" ?>"/>
					<label for="accountable_person_name">Accountable Person</label>
				</div>
				<div class="input-field col s3">
					<button type="button" class="btn waves-effect waves-light md-trigger btn-success" id="add_accountable_person" name="add_accountable_person" data-modal="modal_supply_accountable_persons" onclick="modal_supply_accountable_persons_init()">...</button>
				</div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s12">
					<label>File</label>
				  <div class="file-field input-field">
				      <div class="btn" class="tooltipped m-r-sm" data-position="bottom" data-delay="50" data-tooltip="Upload">
				        <i class="flaticon-upload111 xs"></i>
				        <span>Select File</span>
				        <input type="file">
				      </div>
				      <div class="file-path-wrapper">
				        <input class="file-path validate" type="text">
				      </div>
  				  </div>
			</div>
		</div>
		
		</div>
			<div class="md-footer default">
			<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
		    <button class="btn waves-effect waves-light" id="save_accountable" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
		</div>

</form>