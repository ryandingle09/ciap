<form id="asset_location_form" class="asset_location_form ps-default-form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<div class="form-float-label">
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<label class="active" for="site_id">SITE</label>
					<select name="site_id" id="site_id" required="true" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select site</option>
						<?php 
							foreach($sites as $site)
							{
								$selected = ( !EMPTY($info['site_id']) AND $info['site_id'] == $site['site_id']) ? "selected" : "";
								echo '<option '.$selected.' value="'.$site['site_id'].'">'.$site['site_name'].'</option>';
							}				
						?>
					</select> 
		        </div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
		        	<textarea id="name" required="true" aria-required="true" name="asset_location_name" class="materialize-textarea"><?php echo isset($info['asset_location_name'])?$info['asset_location_name']:''; ?></textarea>
          			<label for="name">LOCATION</label>
		        </div>
			</div>
		</div>
	</div>
	<div class="md-footer default">
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	    <button class="btn waves-effect waves-light" id="save_btn" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	</div>
</form>
<script>

$(function(){
  
  var $form = $('#asset_location_form');
  
  $form.parsley();
  
  $form.submit(function(e) {
	  
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {
	  var data = $(this).serialize();
	  
	  button_loader('save_btn', 1);
	  
	  $.post("<?php echo base_url() . PROJECT_PSMS ?>/psms_asset_location/process/", data, function(result) {
		  
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  
		  button_loader('save_btn', 0);
		  
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  
		  button_loader("save_btn",0);
		  
		  modal_asset_location.closeModal();
		  
		  load_datatable('locations_table', '<?php echo PROJECT_PSMS ?>/psms_asset_location/get_locations');
		}
		
	  }, 'json');       
    }
  });
  

});
</script>