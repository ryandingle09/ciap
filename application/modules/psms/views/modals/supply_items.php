<form id="supply_items_form">
<div class="page-title form-basic m-b-lg">
 <div class="row m-b-n">
	<div class="col s12">
		<div class="btn-group" id="search">
	  		<label style="font-weight: bold">Search :</label>
	   		<input name="searh_gr" type="radio" id="name" value="name" checked/>
	      	<label for="name">Name Only</label>
	      	<input name="searh_gr" type="radio" id="all" value="all"/>
	      	<label for="all">All Columns</label>
		</div>
	</div>
	<div class="col s3">
	   	<input type="text" name="search_input" id="search_input"/>
	</div>
	<div class="col s4">
		<button type="button" class="btn waves-effect waves-light btn-primary" name="search_btn" id="search_btn">Go</button>
		<button type="button" class="btn waves-effect waves-light btn-secondary md-trigger" data-modal="modal_supply_add_item" id="add_item_btn" name="add_item_btn" onclick="modal_supply_add_item_init()">Add Item</button>
	</div>
</div>
</div>
<div class="pre-datatable"></div>
<div>
  <table class="table table-default table-layout-auto" id="supply_items_table">
	  <thead>
		<tr>
			<th width="5%"  style="text-align : center">&nbsp;</th>
			<th width="25%" style="text-align : center">Stock No.</th>
		    <th width="25%" style="text-align : center">Category</th>
		    <th width="45%" style="text-align : center">Name</th>
		</tr>
		
	 </thead>
  </table>
</div>

<div class="md-footer default">
	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
    <button type="button" class="btn waves-effect waves-light btn-success" id="ok_supply" name="ok_supply">OK</button>
</div>
</form>


<script>

	$(function () {
		if($('#supply_category').val() == "")
		{
			$('#supply_items_table').text("No category was selected.");
		}else{
			search_items();
		}
	} );

	$('#search_btn').on('click', function(){
		search_items();
	});
    
	function search_items(){
		var search_type = "";
		var category_id = $('#supply_category').val();

 		if($("#name").is(":checked"))
		{
			search_type = "name";
		}
		else
		{
			search_type = "all";
		}
		
		var search_str = $('#search_input').val();
		load_datatable('supply_items_table', '<?php echo PROJECT_PSMS ?>/psms_supply_items/get_items?search_type='+search_type+'&search_category='+category_id+'&search_str='+search_str);
		$('#supply_items_table_filter').hide();
	}

	var item_id = "";
	function get_item_id(td){
		item_id = td.id;
	}

	$('#ok_supply').on('click', function(){
		$.post("<?php echo base_url() . PROJECT_PSMS ?>/psms_supply_items/get_specific_item", { 'item_id' : item_id}, function(result){
			$('#supply_item_name').val(result.info.supplies_item_name);
			$('#supply_item_no').val(result.info.supplies_item_id);
			$('#supply_item_notes').val(result.info.supplies_item_desc);
			
		}, 'json');
		
		modal_supply_items.closeModal();
		
	});
</script>

