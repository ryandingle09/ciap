<form id="asset_category_form" class="asset_category_form ps-default-form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<div class="form-float-label">
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<input id="name" type="text" class="validate active" value="<?php echo isset($name)?$name:''; ?>">
			    	<label for="name">NAME</label>
				</div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s8">
				<div class="input-field">
					<label class="active" for="type">TYPE</label>
					<select name="type"
							id="type" 
							required=""
							aria-required="true" 
							class="selectize"
							data-parsley-required="true" 
							data-parsley-validation-threshold="0"
							data-parsley-trigger="keyup">
						<option value="">select type</option>
						<option value="1">type 1</option>
						<option value="2">type 2</option>
						<?php if(isset($type)){ ?>
						<option selected="selected" value="4"><?php echo $type; ?></option>
						<?php } ?>
					</select>
		        </div>
			</div>
			<div class="col s4">
				<div class="input-field">
					<input id="useful_life" type="text" class="validate active" value="<?php echo isset($useful_life)?$useful_life:''; ?>">
			    	<label for="useful_life">USEFUL FILE (YEARS)</label>
				</div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<textarea id="description" class="materialize-textarea"><?php echo isset($description)?$description:''; ?></textarea>
					<label for="description">DESCRIPTION</label>
				</div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s6">
				<div class="input-field">
					<label class="active" for="unit_of_measure">UNIT OF MEASURE</label>
					<select name="type"
							id="unit_of_measure" 
							required=""
							aria-required="true" 
							class="selectize"
							data-parsley-required="true" 
							data-parsley-validation-threshold="0"
							data-parsley-trigger="keyup">
						<option value="">select unit of measure</option>
						<?php if(isset($unit_of_measure)){ ?>
						<option selected="selected" value="4"><?php echo $unit_of_measure; ?></option>
						<?php } ?>
					</select>
		        </div>
			</div>
			<div class="col s6">
				<div class="input-field">
					<input id="optimum_reorder_qty" type="text" class="validate active" value="<?php echo isset($optimum_reorder_qty)?$optimum_reorder_qty:''; ?>">
			    	<label for="optimum_reorder_qty">OPTIMUM REORDER QTY</label>
				</div>
			</div>
			
		</div>
	</div>
	<div class="md-footer default">
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	    <button class="btn waves-effect waves-light" id="save_category" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	</div>
</form>
