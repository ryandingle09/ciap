<form id="supply_accountable_person_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
	<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
	<div class="form-float-label form-basic">
		<div class="row m-b-n">
	  		<div class="col s12">
			 <div class="col s3">
			   	<div class="input-field">
			   		<input type="text"/>
			 	</div>
			 </div>
			 <div class="col s4">
			   	<div class="input-field">
					<button type="button" class="btn waves-effect waves-light btn-primary" >Go</button>
			 		<!-- <button type="button" class="btn waves-effect waves-light md-trigger btn-secondary" data-modal="modal_supply_add_item" id="supply_add_item" name="supply_add_item" onclick="modal_supply_add_item_init()">Add Item</button>  -->
			 	</div>
			 </div>
			</div>
		</div>

		<div>
		  	<table class="table table-default table-layout-auto highlight" id="supply_accountable_persons">
				  <thead>
					<tr>
						<th width="5%" style="text-align : center">&nbsp;</th>
						<th width="20%" style="text-align : center">Name</th>
					</tr>
				 </thead>
				<tbody>
					<tr id="accper_tr1">
					</tr>
					<tr id="accper_tr2">
				</tbody>
		  </table>
  	 	</div>
  	  
	  	<div class="md-footer default">
	    	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	    	<button class="btn waves-effect waves-light" id="ok_accountable_person" type="button" value="OK">OK</button>
	  	</div>
</div>

</form>


<script>
	var accountable_persons = [
	                           {name: "Engr. Leilani DL Del Prado"},
	                           {name: "Lorina Lauriquez"}
		                      ];

	var acc_text_1 = "<td  style='text-align : center'> <input name='accper_group' type='checkbox' id='accper_cb1'/> <label for='accper_cb1'></label> </td>"
		+	"<td style='text-align : center'>" + accountable_persons[0]['name'] + "</td>";
	document.getElementById("accper_tr1").innerHTML = acc_text_1;

	var acc_text_2 = "<td  style='text-align : center'> <input name='accper_group' type='checkbox' id='accper_cb2'/> <label for='accper_cb2'></label> </td>"
		+	"<td style='text-align : center'>" + accountable_persons[1]['name'] + "</td>";
	document.getElementById("accper_tr2").innerHTML = acc_text_2;	

	$("input:checkbox").on('click', function() {
		  var $box = $(this);
		  if ($box.is(":checked")) {
		    var group = "input:checkbox[name='" + $box.attr("name") + "']";
		    $(group).prop("checked", false);
		    $box.prop("checked", true);
		  } else {
		    $box.prop("checked", false);
		  }
		});

	var accountable_person = "";
		
	$( function() {
		$('#ok_accountable_person').on('click', function(){
			if(document.getElementById('accper_cb1').checked){
				accountable_person = "Engr. Leilani DL Del Prado";
			}else if(document.getElementById('accper_cb2').checked){
				accountable_person = "Lorina Lauriquez";
			}
			
			modal_supply_accountable_persons.closeModal();
			$('#accountable_person_name').val(accountable_person);
		});
	} );
	
</script>
	