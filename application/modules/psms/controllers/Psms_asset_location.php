<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Psms_asset_location extends PSMS_Controller 
{
	private $module = MODULE_PSMS_ASSET_LOCATION;
	
	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;

	public function __construct()
	{
		parent::__construct();

		$this->load->model('psms_asset_locations_model', 'asset_locations');
		$this->load->model('params_model', 'params');
		
		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);
	}

	public function index()
	{	
		try{
			
			if($this->permission)
			{
				$data 		= array();
				$resources 	= array(
					'load_css' 	=> array(
						CSS_DATATABLE,
						CSS_SELECTIZE,
						CSS_MODAL_COMPONENT,
						CSS_DATETIMEPICKER,
						'psms',
					),
					'load_js' 	=> array(
						JS_DATATABLE,
						JS_SELECTIZE,
						JS_MODAL_CLASSIE,
						JS_MODAL_EFFECTS,
						JS_DATETIMEPICKER,
					),
					'load_modal' 	=> array(
						'modal_asset_location' => array(
							'controller'	=> __CLASS__,
							'method'		=> 'modal_asset_location', // DEFAULT VALUE IS MODAL
							'module'		=> PROJECT_PSMS,
							'title'			=> 'Asset Location',
							'size'			=> 'xs',
							'multiple'   	=> true,
						),	
					),	
					'datatable'		=> array(
						array(
							'table_id' => 'locations_table', 
							'path' => PROJECT_PSMS.'/psms_asset_location/get_locations'
						)
					)
				);
				$view_page	= "asset_location";
			}
			else
			{
				$view_page	= "errors/html/error_permission";
			}

			// CONSTRUCT SECURITY VARIABLES
			$hash_id 		= $this->hash(0);
			$encoded_id 	= base64_url_encode($hash_id);
			$salt 			= gen_salt();
			$token 			= in_salt($encoded_id, $salt);
			$data['url']	= $encoded_id."/".$salt."/".$token;

			$this->template->load($view_page, $data, $resources);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	public function get_locations()
	{
		try {
			
			$params = get_params(); // DO NOT USE: $_POST $this->input->post
			
			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> 0,
				"iTotalDisplayRecords" 	=> 0,
				"aaData" 				=> array()
			);			
			
			if(!$this->permission)
				throw new Exception($this->lang->line('err_unauthorized_view'));
			
			// FIELDS TO BE SELECTED FROM TABLE
			$select_fields = array(
				"L.asset_location_id",				
				"L.asset_location_name",
				"S.site_name",
			);
			
			// APPEARS ON TABLE
			$where_fields = array(
				"L.asset_location_name",
				"S.site_name",
			);			
				
			$results 		= $this->asset_locations->get_asset_location_list($select_fields, $where_fields, $params);
			$total	 		= $this->asset_locations->total_length(); // TOTAL COUNT OF RECORDS
			$filtered_total = $this->asset_locations->filtered_length($select_fields, $where_fields, $params); // TOTAL COUNT OF RECORDS PER PAGE

			if(!EMPTY($results))
			{
				foreach($results as $data)
				{
					
					// PRIMARY KEY
					$primary_key	= $data['asset_location_id'];
					
					// CONSTRUCT SECURITY VARIABLES
					$hash_id 		= $this->hash($primary_key);
					$encoded_id 	= base64_url_encode($hash_id);
					$salt 			= gen_salt();
					$token 			= in_salt($encoded_id, $salt);
					$url 			= $encoded_id."/".$salt."/".$token;
					
									
					// START: CONSTRUCT ACTION ICONS
					$action = "<div class='table-actions'>";
					
						if($this->permission_edit) :
							$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_asset_location' onclick=\"modal_asset_location_init('".$url."')\"></a>";
						endif;
						
						if($this->permission_delete) :
							$delete = 'content_delete("asset_location", "'.$url.'")';
							$action.= "<a href='javascript:;' onclick='" . $delete. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'>&nbsp;</a>";
						endif;
					
					$action .= '</div>';
					// END: CONSTRUCT ACTION ICONS				
					
					$row 	= array();
					$row[]	= $data['asset_location_name'];
					$row[]	= $data['site_name'];
					$row[]	= $action;
					
					$output['aaData'][] = $row;
					
				}
				
				$output["iTotalRecords"] 		= $total["cnt"];
				$output["iTotalDisplayRecords"] = $filtered_total["cnt"];
			}
			
		}
		catch(PDOException $e)
		{
			$fields	= array(
				"site_name"	=> 'Site Name',
				"site_desc"	=> 'Site Description'
			);
			
			$msg = $this->get_user_message($e, $fields);
		}
		catch(Exception $e)
		{
			$msg = $e->getMessage();		
		}
		

		echo json_encode( $output );
	}

	public function get_location_options()
	{
	
		try {
			
			$flag 			= 0;
			$msg			= "";
			$options 		= array();
			$params 		= get_params();	// DO NOT USE: $_POST $this->input->post


			$this->_validate_security($params);

			if(!$this->permission)
				throw new Exception($this->lang->line('err_unauthorized_view'));
			
			// GET CLASSIFICATIONS
			$where = array();	
			$where['site_id'] = $params['site_id'];
			$results = $this->asset_locations->get_asset_locations($where);

			if(!EMPTY($results))
			{
				foreach($results as $data)
				{
					$options[] = array(
						'id' => $data['asset_location_id'],
						'name' => $data['asset_location_name'],
					);
				}
			}
		}
		catch(PDOException $e)
		{
			$fields	= array(
				"asset_location_id"	=> 'Asset Location ID',
				"asset_location_name"	=> 'Asset location Name'
			);
			
			$msg = $this->get_user_message($e, $fields);
		}
		catch(Exception $e)
		{
			$msg = $e->getMessage();		
		}

		$info = array(
			"flag"	=> $flag,
			"msg" 	=> $msg,
			'options' => $options
		);

		echo json_encode( $info );
	}

	public function modal_locations($encoded_id, $salt, $token)
	{
		try
		{
			$data 		= array();
			$resources 	= array();
			
			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			$resources = array(
				'load_css' => array(
					'selectize.default'
				),
				'load_js' 	=> array(
					JS_DATATABLE,
					'popModal.min',
					'selectize',
				),
				'loaded_init'=> array(
					'datepicker_init();',
					'selectize_init();'
				),
				'load_modal' 	=> array(
					'modal_asset_location' => array(
						'controller'	=> 'psms_asset_location',
						'method'		=> 'modal_asset_location', // DEFAULT VALUE IS MODAL
						'module'		=> PROJECT_PSMS,
						'title'			=> 'Asset Location',
						'size'			=> 'xs',
						'multiple'   	=> true,
					),	
				),	
				'datatable2' => array(
					array(
						'table_id'	=> 'locations_table', 
						'sAjaxSource' => base_url().PROJECT_PSMS.'/psms_asset_location/get_locations',
					)
				)
			);

			// CONSTRUCT SECURITY VARIABLES
			$hash_id 		= $this->hash(0);
			$encoded_id 	= base64_url_encode($hash_id);
			$salt 			= gen_salt();
			$token 			= in_salt($encoded_id, $salt);
			$data['url']	= $encoded_id."/".$salt."/".$token;

			$this->load->view("modals/asset_locations", $data);
			$this->load_resources->get_resource($resources);
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

	public function modal_asset_location($encoded_id, $salt, $token)
	{
		try
		{
			$msg	= "";			
			$data 	= array();
			
			// STORES THE SELECTED RECORD
			$info	= array();
				
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);
			
			$hash_id = base64_url_decode($encoded_id);
			
			$data['permission_save']	= $this->permission_add;
			
			if($this->hash(0) != $hash_id)
			{
				
				$key			= $this->get_hash_key('asset_location_id');
				$where			= array();
				$where[$key]	= $hash_id;	
				
				$info = $this->asset_locations->get_specific_asset_location($where);
				
				IF(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));
				
				$data['permission_save']	= $this->permission_edit;				
			}
			
			$data['info']	= $info; // DETAILS

			// GET PARAMS
			$data['sites']	= $this->params->get_params(PSMS_Model::tbl_param_sites);
			
			
			// REGENERATE SECURITY VARIABLES
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;


			$resources = array(
				'load_css' => array(),
				'load_js' 	=> array(),
				'loaded_init'=> array(
					///'datepicker_init();',
					'Materialize.updateTextFields();',
					'selectize_init();',
					'ModalEffects.re_init();',
				),

			);
			$this->load->view("modals/asset_location", $data);
			$this->load_resources->get_resource($resources);
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

	public function process()
	{
	
		try {
			
			$flag 			= 0;
			$msg			= "";
			$params 		= get_params();			
							
			// SERVER VALIDATION
			$this->_validate($params);
			
			
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('asset_location_id');
			
			$where			= array();
			$where[$key]	= $params['hash_id'];
			
			$info 			= $this->asset_locations->get_specific_asset_location($where);
			$asset_location_id		= ISSET($info['asset_location_id']) ? $info['asset_location_id'] : 0;
			
			PSMS_Model::beginTransaction();
			
			// IF ID IS EMPTY, IT MEANS INSERT/SAVE RECORD
			if(EMPTY($asset_location_id))
			{
				if($this->permission_add === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_add'));
								
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				$audit_action[]	= AUDIT_INSERT;
				$audit_table[]	= PSMS_Model::tbl_asset_locations;
				$audit_schema[]	= DB_PSMS;
				$prev_detail[]	= array();
				
				$asset_location_id = $this->asset_locations->insert_asset_location($params); // SAVES THE RECORD
				
				// GET THE CURRENT DETAIL
				$where							= array();
				$where['asset_location_id'] 	= $asset_location_id;
				$info 							= $this->asset_locations->get_specific_asset_location($where);
				$title							= $info['asset_location_name'];
				
				$curr_detail[]	= array($info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				

				$activity	= "%s has been added in asset sites.";
				$activity 	= sprintf($activity, $title);
				
				$msg  = $this->lang->line('data_saved');
			}
			else
			{
				if($this->permission_edit === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_edit'));
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				$audit_action[]	= AUDIT_UPDATE;
				$audit_table[]	= PSMS_Model::tbl_asset_locations;
				$audit_schema[]	= DB_PSMS;
				$prev_detail[]	= array($info);
				
				
				$params['asset_location_id']	= $asset_location_id;
				$this->asset_locations->update_asset_location($params); // UPDATES THE RECORD
				
				// GET THE CURRENT DETAIL
				$where						= array();
				$where['asset_location_id']	= $asset_location_id;
				$info 						= $this->asset_locations->get_specific_asset_location($where);
				$title						= $info['asset_location_name'];
				
				$curr_detail[]	= array($info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				
				$activity	= "%s has been updated in asset locations.";
				$activity 	= sprintf($activity, $title);
				
				$msg  = $this->lang->line('data_updated');
			}
			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
				
			
			PSMS_Model::commit();
			
			$flag = 1;
		}
		catch(PDOException $e)
		{
			PSMS_Model::rollback();
			
			$this->rlog_error($e);
			
			$fields = array(
				'asset_location_name'	=> 'Asset location Name',		
			);
			
			$msg	= $this->get_user_message($e, $fields);
		}
		catch(Exception $e)
		{
			PSMS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag"	=> $flag,
			"msg" 	=> $msg
		);
		
		echo json_encode($info);
		
	}
	
	private function _validate(&$params)
	{
		try
		{
				
			$this->_validate_security($params);
				
			//SPECIFY HERE INPUTS FROM USER
			$fields 				= array();
			$fields['site_id']	= 'Site ID';
			$fields['asset_location_name']	= 'Asset Location Name';	
			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
				
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}	
	
	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
	
	
			$security = explode('/', $params['security']);
	
			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];
	
			check_salt($params['encoded_id'], $params['salt'], $params['token']);
	
			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}


	private function _validate_input($params)
	{
		try
		{
				
			$validation['site_id'] = array(
				'data_type' => 'digit',
				'name'		=> 'Site ID',

			);
			
			$validation['asset_location_name'] = array(
				'data_type' => 'string',
				'name'		=> 'Site Description',
				'min_len'	=> 3,
				'max_len'	=> 255
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function delete_asset_location($params)
	{
		try
		{			
			$flag				= 0;
			$msg				= "Error";
				
			if($this->permission_delete === FALSE)
				throw new Exception($this->lang->line('err_unauthorized_delete'));
				
			$params 			= get_params();
			$params['security']	= $params['param_1'];
	
			$this->_validate_security($params);
	
			// CHECK IF EXISTING RECORD
			$key 			= $this->get_hash_key('asset_location_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];
				
			$info 			= $this->asset_locations->get_specific_asset_location($where);
								
			if(EMPTY($info['asset_location_id']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			$asset_location_id 	= $info['asset_location_id'];
			$title				= $info['asset_location_name'];
				
			PSMS_Model::beginTransaction();
				
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= PSMS_Model::tbl_asset_locations;
			$audit_schema[]	= DB_PSMS;
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL
	
			$this->asset_locations->delete_asset_location($asset_location_id);
	
			// SET AS AN ARRAY
			$curr_detail[]	= array();
				
			// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $title);
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
				
			PSMS_Model::commit();
	
			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}
		catch(PDOException $e)
		{
			PSMS_Model::rollback();
	
			$this->rlog_error($e);
			
			$fields	= array(
				'site_name'	=> 'Site Name',
				'site_desc'	=> 'Site Description',
			);
			
			$msg	= $this->get_user_message($e, $fields);
		}
		catch(Exception $e)
		{
	
			PSMS_Model::rollback();
			
			$this->rlog_error($e);
	
			$msg	= $e->getMessage();
		}
	
		$info = array(
			"flag" 		=> $flag,
			"msg" 		=> $msg,
			"reload" 	=> 'datatable',
			"table_id" 	=> 'locations_table',
			"path"		=> PROJECT_PSMS.'/psms_asset_location/get_locations'
		);
	
		echo json_encode($info);
	}
}