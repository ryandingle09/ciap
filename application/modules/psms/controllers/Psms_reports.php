<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Psms_reports extends PSMS_Controller {
	
	private $module = MODULE_PSMS_REPORTS;
	
	
	public $permission_view		= FALSE;
	
	
	public function __construct()
	{
		parent::__construct();
		
		
		$this->load->model('params_model', 'params');
		$this->load->model('cias_params_model', 'cias_params');
		
		$this->permission			= $this->check_permission($this->module);		
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
	
	}
	
	public function index()
	{	
		try{
			$data = $resources = $modal = array();
			
			
			
			if($this->permission)
			{
				$modal = array(
					'modal_report' => array(
						'controller'	=> __CLASS__,
						'module'		=> PROJECT_MPIS,
						'multiple'		=> TRUE,
						'height'		=> '450px;',
						'ajax'			=> TRUE,
						'size'			=> 'XL'
					),
						
					'modal_chart' => array(
						'module'		=> PROJECT_MPIS,
						'controller'	=> __CLASS__,
						'method'		=> 'modal_chart',							
						'multiple'		=> TRUE,							
						'size'			=> 'XL'
					)
				);
				
				
				$resources['load_js'] 		= array(JS_DATETIMEPICKER, 'chart.min', 'modules/psms/psms_reports');
				$resources['load_css'] 		= array(CSS_DATETIMEPICKER);
				$resources['load_modal'] 	= $modal;

				
				$data['reports']	= $this->cias_params->get_params(SYSAD_Model::tbl_param_resource, array('parent_resource_code' => MODULE_PSMS_REPORTS), "*", array("sort_no" => "ASC"));
				
				$data['year_max']	= date('Y');
				$data['year_min']	= MPIS_START_YEAR;
				
				$view_page	= "reports";
			}
			else
			{
				$view_page	= "errors/html/error_permission";
			}

			$this->template->load($view_page, $data, $resources);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
	
	public function generate_report()
	{		
		
		try {
			
			$success		= 0;
			$msg			= '';
			$report			= "";
			$report_title	= "";
			$report_data	= array();
			$portrait 		= false; 
			
			$params			= get_params();
			
			$generate		= strtoupper($params['generate']);

			$report_page	= 'report';
						
			switch($params['report'])
			{
				case "PSMS-REPORT-SUPPLIES":
					
					$report			= strtocontroller($params['report']);
					$report_page 	= substr($report, 5);
					$report_title	= 'REPORTS OF SUPPLIES AND MATERIALS ISSUED';
					$portrait = true;

					$this->load->library('../modules/psms/controllers/psms_reports_assets');
					$report_data['report'] = $this->psms_reports_assets->generate();
					$html = $this->load->view('reports/' . $report_page, $report_data, TRUE);

					$success	= 1;
				break;

				case "PSMS-REPORT-PROPERTY":
					
					$report			= strtocontroller($params['report']);
					$report_page 	= substr($report, 5);
					$report_title	= 'REPORTS OF THE PHYSICAL COUNT OF PROPERTY, PLANT AND EQUIPMENT';
					

					$this->load->library('../modules/psms/controllers/psms_reports_property');
					$report_data['report'] = $this->psms_reports_property->generate();
					$html = $this->load->view('reports/' . $report_page, $report_data, TRUE);

					$success	= 1;
				break;
				

				case "PSMS-REPORT-ICTE-DISTRIBUTION":

					$report			= strtocontroller($params['report']);
					$report_page 	= substr($report, 5);
					$report_title	= 'DISTRIBUTION OF ICT EQUIPMENT';

					$this->load->library('../modules/psms/controllers/psms_reports_icte_distribution');
					$report_data['report'] = $this->psms_reports_icte_distribution->generate();
					$html = $this->load->view('reports/' . $report_page, $report_data, TRUE);

					$success	= 1;
				break;
				
				case "PSMS-REPORT-UNSERVICEABLE-PROPERTY":
				
					$report			= strtocontroller($params['report']);
					$report_page 	= substr($report, 5);
					$report_title	= 'INVENTORY AND INSPECTION REPORT OF UNSERVICEABLE PROPERTY';
				
					$this->load->library('../modules/psms/controllers/psms_reports_unserviceable_property');
					$report_data['report'] = $this->psms_reports_unserviceable_property->generate();
					$html = $this->load->view('reports/' . $report_page, $report_data, TRUE);
				
					$success	= 1;
				break;
				
				case "PSMS-REPORT-ICTE-INVENTORY":
				
					$report			= strtocontroller($params['report']);
					$report_page 	= substr($report, 5);
					$report_title	= 'INVENTORY OF ICT EQUIPMENT';
				
					$this->load->library('../modules/psms/controllers/psms_reports_icte_inventory');
					$report_data['report'] = $this->psms_reports_icte_inventory->generate();
					$html = $this->load->view('reports/' . $report_page, $report_data, TRUE);
				
					$success	= 1;
				break;
				
				case "PSMS-REPORT-STOCK-CARD":
				
					$report			= strtocontroller($params['report']);
					$report_page 	= substr($report, 5);
					$report_title	= 'STOCK CARD';
				
					$this->load->library('../modules/psms/controllers/psms_reports_stock_card');
					$report_data['report'] = $this->psms_reports_stock_card->generate();
					$html = $this->load->view('reports/' . $report_page, $report_data, TRUE);
				
					$success	= 1;
				break;
				
			}
			
			$report_page				= (strtoupper($params['generate']) == "CHART") ? 'report_chart' : 'report';

			$filename 					= $report . '_' . time();
			$report_data['title']		= $report_title;
			$report_data['generate']	= $params['generate'];
			
			switch($generate)
			{
				
				case 'PDF':
					$modal_data					= array();
					$modal_data['generate']		= $params['generate'];
					$modal_data['pdf'] 			= $this->pdf( $filename . '.pdf', $html, $portrait, 'S', $header=NULL, FALSE );
					$modal						= $this->load->view('modals/report_container_modal', $modal_data, TRUE);

					$info 	= array(
							'success'	=> $success,
							'msg'		=> $msg,
							'modal' 	=> $modal
					);
						
					echo  json_encode($info);
				break;
				
				case 'CHART':
					$modal_data					= array();
					$modal_data['generate']		= $params['generate'];
					$modal_data['chart'] 		= $html;
					$modal						= $this->load->view('modals/report_container_modal', $modal_data, TRUE);
			
					$info 	= array(
							'success'	=> $success,
							'msg'		=> $msg,
							'modal' 	=> $modal
					);
			
					echo  json_encode($info);
				break;
						
				case 'EXCEL':
					
					$this->convert_excel( $html, $filename , $report_title);
				break;
			}			
			
		}
		catch(PDOException $e)
		{
			$msg = $e->getMessage();
		}
		catch(Exception $e)
		{
			$msg = $e->getMessage();			
		}
	}
	
	public function modal_chart()
	{
		$resources['load_js'] 		= array('chart.min', 'modules/mpis/mpis_reports');
		$resources['load_init']		= array('ReportChart.init();');
		
		$this->load->view('modals/chart_container_modal');
		$this->load_resources->get_resource($resources);
	}
}

/* End of file Mpis_reports.php */
/* Location: ./application/modules/mpis/controllers/mpis_reports.php */