<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Psms_supplies_category extends PSMS_Controller 
{
	private $module = MODULE_PSMS_SUPPLIES_CATEGORY;
	
	public function __construct()
	{
		parent::__construct();

	}

	public function index()
	{
		try{
			
			if($this->permission)
			{
				$data 		= array();
				$resources 	= array(
					'load_css' 	=> array(
						CSS_DATATABLE,
						CSS_MODAL_COMPONENT,
						CSS_DATETIMEPICKER,
						'psms',
					),
					'load_js' 	=> array(
						JS_DATATABLE,
						JS_MODAL_CLASSIE,
						JS_MODAL_EFFECTS,
						JS_DATETIMEPICKER,
					),
					'load_modal' 	=> array(
						'modal_supplies_category' => array(
							'controller'	=> __CLASS__,
							'method'		=> 'modal_supplies_category', // DEFAULT VALUE IS MODAL
							'module'		=> PROJECT_PSMS,
							'title'			=> 'Supply Category',
							'size'			=> 'xs',
							'multiple'   	=> true,
						),
					),
					'datatable'		=> array(
						array(
							'table_id' => 'categories_table',
							'path' => PROJECT_PSMS.'/'.__CLASS__.'/get_categories'
						)
					)
				);
				$view_page	= "supplies_category";
			}
			else
			{
				$view_page	= "errors/html/error_permission";
			}

			// CONSTRUCT SECURITY VARIABLES
			$hash_id 		= $this->hash(0);
			$encoded_id 	= base64_url_encode($hash_id);
			$salt 			= gen_salt();
			$token 			= in_salt($encoded_id, $salt);
			$data['url']	= $encoded_id."/".$salt."/".$token;

			$this->template->load($view_page, $data, $resources);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	public function get_categories()

	{
		$btns = '<div class="table-actions">
					<a href="javascript:;"
						class="tooltipped edit md-trigger"
						data-position="bottom"
						data-delay="50"
						data-modal="modal_supplies_category"
						onclick="modal_supplies_category_init(\'h/s/t/edit\')"
						data-tooltip="Edit">
					</a>
					<a href="javascript:;"
						class="tooltipped delete"
						data-position="bottom"
						data-delay="50"
						data-tooltip="Delete">
					</a>
				</div>';

		$output = array(
			"aaData" => array(
				array('Computers', 'Semi-Expendable', 3, 'Office Equipments', 2, 3, $btns),
				array('Computers', 'Semi-Expendable', 1, 'Office Equipments', 1, 4, $btns),
				array('Computers', 'Semi-Expendable', 10, 'Office Equipments', 10, 13, $btns),
			)
		);

		echo json_encode( $output );
	}

	public function modal_supplies_category($encoded_id, $salt, $token, $action)
	{
		try
		{
			$data 		= array();
			$resources 	= array();
			
			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			$resources = array(
				'load_css' => array( ),
				'load_js' 	=> array(JS_DATETIMEPICKER),
				'loaded_init'=> array(
					'selectize_init();',
					'Materialize.updateTextFields();'
				)
			);

			if($action == 'edit'){
				$data['name'] = 'Laptops';
				$data['type'] = 'type';
				$data['useful_life'] = 5;
				$data['description'] = 'Office Laptops';
				$data['unit_of_measure'] = '';
				$data['optimum_reorder_qty'] = 3;
			}
			
			$this->load->view("modals/supply_category", $data);
			$this->load_resources->get_resource($resources);
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

}