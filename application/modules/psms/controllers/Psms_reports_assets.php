<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Psms_reports_assets extends PSMS_Controller {
	
	private $year_start			= 0;
	private $year_end			= 0;
	private $quarter			= "Year-end";
	private $quarter_arr		= array(1 => '1st', 2 => '2nd', 3 => '3rd', 4 => '4th', 5 => 'Year-end');
	private $sources			= NULL;
	private $summary_sources	= array(0 => 'Total', 1 => 'Residential Building Construction', 2 => 'Non Residential Building Construction');	
	private $regions			= NULL;
	
	public function __construct()
	{
		parent::__construct();
		
		//$this->load->model('building_construction_model', 'building_construction');
		//$this->load->model('params_model', 'params');
		

	}
		
	public function generate()
	{		
		
		try {
			
			$params				= get_params();
			
			switch(strtoupper($params['generate']))
			{
				case "PDF":
				case "EXCEL":
			
					$head	= $this->_construct_report_header();
					$body	= $this->_construct_report_body($info);
						
					$report = array(
						'content'	=> $head . $body								
					);
						
					return $report;				
					
				break;
			}
		}
		catch (PDOException $e)
		{
			throw $e;
		}
		catch (Exception $e)
		{
			throw $e;
		}
					
	}
	
	public function _construct_report_header(){
		return '<div>_construct_report_header</div>';
	}
	public function _construct_report_body(){
		return '<div>_construct_report_header</div>';
	}
}

/* End of file Mpis_reports.php */
/* Location: ./application/modules/mpis/controllers/mpis_reports.php */