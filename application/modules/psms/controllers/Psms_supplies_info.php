<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Psms_supplies_info extends PSMS_Controller 
{
	
	private $module = MODULE_PSMS_SUPPLIES_INFO;
	
	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('psms_supplies_info_model', 'supplies_info');
		
		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);
		
	}
	
	public function index()
	{
		
		try
		{
			if($this->permission)
			{ 
				$data		= array();
				$resources	= array(
						'load_css'	=> array(
							CSS_DATATABLE
						),
						'load_js'	=> array(
							JS_DATATABLE
						),
						'load_modal'	=> array(
							'modal_supply' 	=> array(
									'controller'	=> __CLASS__,
									'module'		=> PROJECT_PSMS,
									'multiple'		=> true,
									'method'		=> 'modal_supply',
									'size'			=> 'xl'
							)
						),
						'datatable'		=> array(
								array(
										'table_id'	=> 'supplies_info_table', 
										'path'		=> PROJECT_PSMS.'/psms_supplies_info/get_supplies')
						)
				);
				$view_page = "supplies_info";		
			}
			else {
				$view_page = "errors/html/error_permission";
			}
			
		}
		catch (PDOException $e)
		{
			$msg = $e->getMessage();
		}
		catch(Exception $e)
		{
			$msg = $e->getMessage();
		}
		
		$data['permission_add'] = $this->permission_add;
		
		// CONSTRUCT SECURITY VARIABLES
		$hash_id		= $this->hash(0);
		$encoded_id 	= base64_url_encode($hash_id);
		$salt			= gen_salt();
		$token			= in_salt($encoded_id, $salt);
		$data['url']	= $encoded_id."/".$salt."/".$token;
		
		$this->template->load($view_page, $data, $resources);

	}
	
	public function get_supplies()
	{
		try
		{
			$params = get_params(); // DO NOT USE: $_POST $this->input->post
			
			$output = array(
				"sEcho"					=> intval($params['sEcho']),					
				"iTotalRecords"			=> 0,
				"iTotalDisplayRecords"	=> 0,
				"aaData"				=> array()					
			);
			
			if(!$this->permission)
				throw new Exception($this->lang->line('err_unauthorized_view'));
		
			// FIELDS TO BE SELECTED FROM TABLE
			$select_fields = array(
				"A.supplies_id",
				"A.supplies_acquired_date", 
				"A.supplies_ref_no",
				"A.supplies_qty", 
				"A.supplies_unit_cost",
				"A.supplies_total_cost",
				"B.supplies_item_name"
			);
			
			// APPEAR ON TABLE
			$where_fields = array(
				"supplies_acquired_date",
				"supplies_item_name",
				"supplies_ref_no",
				"supplies_qty",
				"supplies_unit_cost",
				"supplies_total_cost"
			);
			
			$results		= $this->supplies_info->get_supplies_info_list($select_fields, $where_fields, $params);
			$total			= $this->supplies_info->total_length(); // TOTAL COUNT OF RECORDS
			$filtered_total	= $this->supplies_info->filtered_length($select_fields, $where_fields, $params); // TOTAL COUNT OF RECORDS PER PAGE
		
			if(!EMPTY($results))
			{
				foreach ($results as $data)
				{
					
					// PRIMARY KEY
					$primary_key	= $data['supplies_id'];
					
					// CONSTRUCT SECURITY VARIABLES
					$hash_id	= $this->hash($primary_key);
					$encoded_id	= base64_url_encode($hash_id);
					$salt		= gen_salt();
					$token		= in_salt($endcoded_id, $salt);
					$url		= $encoded_id."/".$salt."/".$token;
					
					// START: CONSTRUCT ACTION ICONS
					$action = "<div class='table-actions'>";

					if($this->permission_edit) :
						$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_supply' onclick=\"modal_supply_init('".$url."')\"></a>";
					endif;
					
					if($this->permission_delete) :
						$delete = 'content_delete("asset site", "'.$url.'")';
						$action.= "<a href='javascript:;' onclick='" . $delete. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					endif;
					
					$action .= '</div>';
					// END: CONSTRUCT ACTION ICONS
					
					$row 	= array();
					$row[]	= $data['supplies_acquired_date'];
					$row[]	= $data['supplies_item_name'];
					$row[]	= $data['supplies_ref_no'];
					$row[]	= $data['supplies_qty'];
					$row[]	= $data['supplies_unit_cost'];
					$row[]	= $data['supplies_total_cost'];
					$row[]	= $action;
						
					$output['aaData'][] = $row;
				}
				
				$output["iTotalRecords"] 		= $total["cnt"];
				$output["iTotalDisplayRecords"] = $filtered_total["cnt"];
			}
		}
		catch (PDOExcepton $e)
		{
			$fields = array(
					"supplies_id"			=> "Supplies Id",
					"supplies_aquired_date" => "Aquired Date",
					"supplies_item_name" 	=> "Item Name",
					"supplies_ref_no"		=> "Refference No.",
					"supplies_qty"			=> "Quantity",
					"supplies_unit_cost"	=> "Unit Cost",
					"supplies_total_cost"	=> "Total Cost"
			);
			$msg = $this->get_user_message($e, $fields);
		}
		
		catch(Exception $e)
		{
			$msg = $e->getMessage();		
		}
		

		echo json_encode( $output );
	
	}
	
	
	public function modal_supply($encoded_id, $salt, $token)
	{
		try 
		{
			$msg	= "";
			$resources = array(
				'load_css'		=> array(CSS_SELECTIZE, CSS_DATETIMEPICKER),
				'load_js'		=> array(JS_SELECTIZE,JS_DATETIMEPICKER),
				'load_modal'	=> array(
						'modal_supply_items' 	=> array(
								'controller'	=> 'psms_supply_items',
								'multiple'		=> true,
								'module'		=> PROJECT_PSMS,
								'method'		=> 'modal_supply_items',
								'size'			=> 'lg'
						),
						'modal_supply_add_accountable' 	=> array(
										'controller'	=> __CLASS__,
										'multiple'		=> true,
										'module'		=> PROJECT_PSMS,
										'method'		=> 'modal_supply_add_accountable'
						)
				),
				'loaded_init'	=> array('ModalEffects.re_init();')
					
			);
			
			// STORES CATEGORIES
			$data['supply_categories']			= $this->supplies_info->get_categories();
			
			// STORES ACCOUNTABLE PERSONS
			$data['supply_accountable_persons']	= $this->supplies_info->get_accountable_persons();
			
			
			// STORES THE SELECTED RECORD
			$info	= array();
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);
				
			$hash_id = base64_url_decode($encoded_id);
				
			$data['permission_save']	= $this->permission_add;
			
			if($this->hash(0) != $hash_id)
			{
			
				$key			= $this->get_hash_key('supplies_id');
				$where			= array();
				$where[$key]	= $hash_id;
			
				$info = $this->supplies_info->get_specific_supply_info($where);
			
				IF(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));
			
				$data['permission_save']	= $this->permission_edit;
			}
				
			$data['info']	= $info; // DETAILS
				
			// REGENERATE SECURITY VARIABLES
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
				
			$this->load->view('modals/supply', $data);
			$this->load_resources->get_resource($resources);
			
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
		
	}
	
	// USE THIS FUNCTION TO SAVE OR UPDATE THE RECORD
	public function process()
	{
	
		try 
		{
			$flag 			= 0;
			$msg			= "";
			$params 		= get_params();

			// SERVER VALIDATION
			$this->_validate($params);
				
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('supplies_id');
				
			$where			= array();
			$where[$key]	= $params['hash_id'];
				
			$info 			= $this->supplies_info->get_specific_supply_info($where);
			$supplies_id	= ISSET($info['supplies_id']) ? $info['supplies_id'] : 0;
				
			
			PSMS_Model::beginTransaction();
				
			// IF ID IS EMPTY, IT MEANS INSERT/SAVE RECORD
			if(EMPTY($supplies_id))
			{
				if($this->permission_add === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_add'));
	
					// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
					$audit_action[]	= AUDIT_INSERT;
					$audit_table[]	= PSMS_Model::tbl_supplies;
					$audit_schema[]	= DB_PSMS;
					$prev_detail[]	= array();
	
					$supplies_id = $this->supplies_info->insert_supply_info($params); // SAVES THE RECORD
	
					// GET THE CURRENT DETAIL
					$where					= array();
					$where['supplies_id']	= $supplies_id;
					$info 					= $this->supplies_info->get_specific_supply_info($where);
					$title					= $info['supplies_id'];
	
					$curr_detail[]	= array($info);
					// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
	
	
					$activity	= "%s has been added in supplies info.";
					$activity 	= sprintf($activity, $title);
	
					$msg  = $this->lang->line('data_saved');
			}
			else
			{
				if($this->permission_edit === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_edit'));
	
					// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
					$audit_action[]	= AUDIT_UPDATE;
					$audit_table[]	= PSMS_Model::tbl_supplies;
					$audit_schema[]	= DB_PSMS;
					$prev_detail[]	= array($info);
	
					$params['supplies_id']	= $supplies_id;
					$this->supplies_info->update_supply_info($params); // UPDATES THE RECORD
	
					// GET THE CURRENT DETAIL
					$where				= array();
					$where['supplies_id']	= $supplies_id;
					$info 				= $this->supplies_info->get_specific_supply_info($where);
					$title				= $info['supplies_id'];
	
					$curr_detail[]	= array($info);
					// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
	
					$activity	= "%s has been updated in supplies info.";
					$activity 	= sprintf($activity, $title);
	
					$msg  = $this->lang->line('data_updated');
	
	
	
			}
				
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
					$activity,
					$this->module,
					$prev_detail,
					$curr_detail,
					$audit_action,
					$audit_table,
					$audit_schema
					);
	
				
			PSMS_Model::commit();
				
			$flag = 1;
		}
		catch(PDOException $e)
		{
			PSMS_Model::rollback();
				
			$this->rlog_error($e);

			$fields = array(
				'supply_item_name'				=> 'Item Name',
				'supplies_item_desc'			=> 'Note',
				'supplies_acquired_date'		=> 'Received Date ',
				'supplies_ref_no'				=> 'Refference No.',
				'supplies_qty'					=> 'Quantity',
				'supplies_uom'					=> 'Category',
				'supplies_unit_cost'			=> 'Unit Cost',
				'supplies_total_cost'			=> 'Total Cost',
				'supplies_accountable_officer'	=> 'Accountable Officer'
				
			);
				
			$msg	= $this->get_user_message($e, $fields);
		}
		catch(Exception $e)
		{
			PSMS_Model::rollback();
				
			$msg = $this->rlog_error($e, TRUE);
		}
	
		$info = array(
				"flag"	=> $flag,
				"msg" 	=> $msg
		);
	
		echo json_encode($info);
	
	}
	
	private function _validate(&$params)
	{
		try
		{
	
			$this->_validate_security($params);
	
			//SPECIFY HERE INPUTS FROM USER

			$fields 								= array();
			$fields['supply_item_name']				= 'Item Name';
// 			$fields['supply_item_notes']					= 'Note';
			$fields['supply_received_date']			= 'Received Date ';
			$fields['supply_refference_no']			= 'Refference No.';
			$fields['supply_quantity']				= 'Quantity';
			$fields['supply_category']				= 'Category';
			$fields['supply_unit_cost']				= 'Unit Cost';
// 			$fields['supplies_total_cost']			= 'Site Name';
			$fields['supply_accountable_person']	= 'Accountable Officer';
			
			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
	
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}
	
	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
	
	
			$security = explode('/', $params['security']);
	
			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];
	
			check_salt($params['encoded_id'], $params['salt'], $params['token']);
	
			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	
	private function _validate_input($params)
	{
		try
		{
			$validation['supply_category'] = array(
					'data_type' => 'int',
					'name'		=> 'Category',
			);
			
			$validation['supply_item_name'] = array(
					'data_type' => 'string',
					'name'		=> 'Item Name',
					'min_len'	=> 3,
					'max_len'	=> 100
			);
				
			$validation['supply_item_notes'] = array(
					'data_type' => 'string',
					'name'		=> 'Notes',
					'min_len'	=> 3,
					'max_len'	=> 255
			);
			
			$validation['supply_received_date'] = array(
					'data_type' => 'int',
					'name'		=> 'Received Date',
			);
			
			$validation['supply_refference_no'] = array(
					'data_type' => 'int',
					'name'		=> 'Refference No',
					'min_len'	=> 3,
					'max_len'	=> 45
			);
			
			$validation['supply_quantity'] = array(
					'data_type' => 'int',
					'name'		=> 'Quantity',
					'min_len'	=> 1,
					'max_len'	=> 11
			);
			
			$validation['supply_unit_cost'] = array(
					'data_type' => 'decimal',
					'name'		=> 'Unit Cost',
					'min_len'	=> 1,
					'max_len'	=> 25
			);
			
			$validation['supply_total_cost'] = array(
					'data_type' => 'decimal',
					'name'		=> 'Total Cost',
					'min_len'	=> 1,
					'max_len'	=> 25
			);
			
			
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function modal_supply_add_accountable(){
		$datea = array();
		$resources = array();
		$resources['load_css']		= array(CSS_SELECTIZE);
		$resources['load_js']		= array(JS_SELECTIZE);
		
		$resources['load_modal']	= array(
				'modal_supply_accountable_persons' 	=> array(
						'controller'	=> __CLASS__,
						'multiple'		=> true,
						'module'		=> PROJECT_PSMS,
						'method'		=> 'modal_supply_accountable_persons',
						'size'			=> 'md'
				)
		);
			
// 		$resources['loaded_init']	= array(
// 				'ModalEffects.re_init();'
// 		);
		
		
		$this->load->view('modals/supply_add_accountable', $data);
		$this->load_resources->get_resource($resources);
	}
	
	public function modal_supply_accountable_persons(){
		$datea = array();
		$resources = array();
		$resources['load_css']		= array(CSS_SELECTIZE);
		$resources['load_js']		= array(JS_SELECTIZE);
	
		$this->load->view('modals/supply_accountable_persons', $data);
		$this->load_resources->get_resource($resources);
	}
	
	public function get_specific_category()
	{
		$data						= array();
		try
		{
			$params 						= get_params();
			$where							= array();
			$where['supplies_category_id']	= $params['category_id'];
			$info 							= $this->supplies_info->get_specific_supply_category($where);
		
			$data['info']					= $info;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		
		echo json_encode($data);
	}
	
}

/* End of file Psms_supplies_info.php */
/* Location: ./application/modules/psms/controllers/Psms_supplies_info.php */