<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Psms_asset_accountability_history extends PSMS_Controller 
{
	private $module = MODULE_PSMS_ASSET_ACCOUNTABILITY_HISTORY;

	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;	
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('psms_assets_model', 'assets');
		$this->load->model('psms_asset_accountabilities_model', 'accountabilities');
		$this->load->model('params_model', 'params');
		$this->load->model('cias_params_model', 'cias');
		
		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);
	}


	public function get_asset_accountability_history()
	{
	
		try {
			
			$params = get_params(); // DO NOT USE: $_POST $this->input->post
			
			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> 0,
				"iTotalDisplayRecords" 	=> 0,
				"aaData" 				=> array()
			);			
			
			if(!$this->permission)
				throw new Exception($this->lang->line('err_unauthorized_view'));
			
			// FIELDS TO BE SELECTED FROM TABLE
			$select_fields = array(
				"CS.asset_classification_id",
				"CS.asset_classification_name",
				"CT.asset_category_name",
			);
			
			// APPEARS ON TABLE
			$where_fields = array(
				"CS.asset_classification_name",
				"CT.asset_category_name",
			);
				
			$results 		= $this->asset_classifications->get_asset_classification_list($select_fields, $where_fields, $params);
			$total	 		= $this->asset_classifications->total_length(); // TOTAL COUNT OF RECORDS
			$filtered_total = $this->asset_classifications->filtered_length($select_fields, $where_fields, $params); // TOTAL COUNT OF RECORDS PER PAGE
			

			if(!EMPTY($results))
			{

				foreach($results as $data)
				{
					// PRIMARY KEY
					$primary_key	= $data['asset_classification_id'];
					
					// CONSTRUCT SECURITY VARIABLES
					$hash_id 		= $this->hash($primary_key);
					$encoded_id 	= base64_url_encode($hash_id);
					$salt 			= gen_salt();
					$token 			= in_salt($encoded_id, $salt);
					$url 			= $encoded_id."/".$salt."/".$token;
					
					// START: CONSTRUCT ACTION ICONS
					$action = "<div class='table-actions'>";
					
						if($this->permission_edit) :
							$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_asset_classification' onclick=\"modal_asset_classification_init('".$url."')\"></a>";
						endif;
						
						if($this->permission_delete) :
							$delete = 'content_delete("asset_classification", "'.$url.'")';
							$action.= "<a href='javascript:;' onclick='" . $delete. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'>&nbsp</a>";
						endif;
					
					$action .= '</div>';
					// END: CONSTRUCT ACTION ICONS				
					
					$row 	= array();
					$row[]	= $data['asset_classification_name'];
					$row[]	= $data['asset_category_name'];
					$row[]	= $action;
					
					$output['aaData'][] = $row;
					
				}
				
				$output["iTotalRecords"] 		= $total["cnt"];
				$output["iTotalDisplayRecords"] = $filtered_total["cnt"];
			}
			
		}
		catch(PDOException $e)
		{
			$fields	= array(
				"asset_classification_name"	=> 'Asset Classification Name',
				"asset_category_name"	=> 'Asset Category Name'
			);
			
			$msg = $this->get_user_message($e, $fields);
		}
		catch(Exception $e)
		{
			$msg = $e->getMessage();		
		}
		

		echo json_encode( $output );
	}

	public function get_accountable_options()
	{
	
		try {
			
			$flag 			= 0;
			$msg			= "";
			$options 		= array(0, 'select accountable');
			$params 		= get_params();	// DO NOT USE: $_POST $this->input->post

			$this->_validate_security($params);

			if(!$this->permission)
				throw new Exception($this->lang->line('err_unauthorized_view'));

			//GET PARAMS
			$select_fields = array(
				'U.employee_no',
				'U.first_name',
				'U.middle_name',
				'U.last_name',
				'R.role_code',
			);

			$where_emp 	= array(
				'R.role_code' => 'CIAP-STAFF',
				'R.office_code' => $params['office_code'] 
			);

			$results = $this->cias->get_employees($select_fields, $where_emp);

			if(!EMPTY($results))
			{
				foreach($results as $data)
				{
					$employee_name = $data['first_name'].' ';
					$employee_name.= $data['middle_name'].' ';
					$employee_name.= $data['last_name'];

					$options[] = array(
						'id' => $data['employee_no'],
						'name' => $employee_name,
					);
				}
			}
		}
		catch(PDOException $e)
		{
			$fields	= array();
			
			$msg = $this->get_user_message($e, $fields);
		}
		catch(Exception $e)
		{
			$msg = $e->getMessage();		
		}

		$info = array(
			"flag"	=> $flag,
			"msg" 	=> $msg,
			'options' => $options
		);

		echo json_encode( $info );
	}

	public function modal_accountable($encoded_id, $salt, $token)
	{
	
		try
		{
			$data 		= array();
			$resources 	= array();
			

			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);
			$hash_id = base64_url_decode($encoded_id);
			$key	= $this->get_hash_key('property_no');

			$where			= array();
			$where[$key]	= $hash_id;
				
			$asset = $this->assets->get_specific_asset($where);
			
			if($asset){
				$data['property_no'] = $asset['property_no'];
			}

			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;

			$resources = array(
				'load_css' => array(
					'selectize.default'
				),
				'load_js' 	=> array(
					'popModal.min',
					'selectize',
				),
				'loaded_init'=> array(
					'datepicker_init();',
					'selectize_init();',
					'ModalEffects.re_init();',
				),
				'load_modal' => array(
					'modal_search_employee' => array(
						'controller'	=> __CLASS__,
						'method'		=> 'modal_search_employee', // DEFAULT VALUE IS MODAL
						'module'		=> PROJECT_PSMS,
						'title'			=> 'Search Employee',
						'multiple'   	=> true,
						'size'			=> 'md',
					),	
				),
			);


			$data['employees']	= array();
			$data['offices']	= $this->cias->get_params('cias_param_ciap_office');
			
			$this->load->view("modals/asset_accountable", $data);
			$this->load_resources->get_resource($resources);
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

	public function process()
	{
		
		try {
			
			$flag 			= 0;
			$msg			= "";
			$params 		= get_params();			
			
			// SERVER VALIDATION
			$this->_validate($params);
			
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('property_no');
						
			$where = array();
			$where[$key]			= $params['hash_id'];
			$where['property_no']	= $params['property_no'];
			$where['employee_id']	= $params['employee_id'];
			$where['office_id']		= $params['office_id'];
			$where['accountability_status_id']  = $params['accountability_status_id'];

			$info = $this->accountabilities->get_asset_accountability($where);

			PSMS_Model::beginTransaction();

			// IF ID IS EMPTY, IT MEANS INSERT/SAVE RECORD
			if(EMPTY($info))
			{
				if($this->permission_add === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_add'));
				
				// CHECK IF THE RECORD IS ALREADY EXIST
				//$exist = $this->_check_asset_accountability($params);
				
				//if($exist)	
					//throw new Exception('This record is arlready added.');							
	
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				$audit_action[]	= AUDIT_INSERT;
				$audit_table[]	= PSMS_Model::tbl_asset_accountabilities;
				$audit_schema[]	= DB_PSMS;
				$prev_detail[]	= array();
				
				$accountable = $this->accountabilities->insert_accountability($params); // SAVES THE RECORD

				// GET THE CURRENT DETAIL
				$where					= array();
				$where[$key]			= $params['hash_id'];
				$where['property_no']	= $params['property_no'];
				$where['employee_id']	= $params['employee_id'];
				$where['office_id']		= $params['office_id'];
				$where['accountability_status_id']  = $params['accountability_status_id'];

				$info 								= $this->accountabilities->get_asset_accountability($where);
				$title								= $info['employee_id'].' '.$info['property_no'];
				
				$curr_detail[]	= array($info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				

				$activity	= "%s has been added in asset accountabilities.";
				$activity 	= sprintf($activity, $title);
				
				$msg  = $this->lang->line('data_saved');
			}
			else
			{
				if($this->permission_edit === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_edit'));
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				$audit_action[]	= AUDIT_UPDATE;
				$audit_table[]	= PSMS_Model::tbl_asset_classifications;
				$audit_schema[]	= DB_PSMS;
				$prev_detail[]	= array($info);
				
				
				$params['asset_classification_id']	= $asset_classification_id;
				$this->asset_classifications->update_asset_classification($params); // UPDATES THE RECORD
				
				// GET THE CURRENT DETAIL
				$where								= array();
				$where['asset_classification_id']	= $asset_classification_id;
				$info 								= $this->asset_classifications->get_specific_asset_classification($where);
				$title								= $info['asset_classification_name'];
				
				$curr_detail[]	= array($info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				
				$activity	= "%s has been updated in asset classifications.";
				$activity 	= sprintf($activity, $title);
				
				$msg  = $this->lang->line('data_updated');
			}
			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
				
			
			PSMS_Model::commit();
			
			$flag = 1;
		}
		catch(PDOException $e)
		{
			PSMS_Model::rollback();
		
			$fields = array(
				'asset_classification_name'	=> 'Asset classification Name',		
			);
			
			$msg = $this->get_user_message($e, $fields);
		}
		catch(Exception $e)
		{
			PSMS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag"	=> $flag,
			"msg" 	=> $msg
		);
		
		echo json_encode($info);
	}
	
	/* private function _check_asset_accountability($params)
	{
	
		try {
				
			$where = array();
			$where['property_no']	= $params['property_no'];
			$where['employee_id']	= $params['employee_id'];
			$where['office_id']		= $params['office_id'];
			$where['attachment']	= $params['attachment'];
			$where['accountability_status_id']		= $params[''];
				
			$info 	= $this->accountabilities->get_asset_accountability($where);
				
			return $info;
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}*/

	private function _validate(&$params)
	{
		try
		{
			$this->_validate_security($params);
				
			//SPECIFY HERE INPUTS FROM USER
			$fields 				= array();
			$fields['property_no']			= 'Property No';
			$fields['received_date']		= 'Received Date';
			$fields['employee_id']			= 'Employee ID';
			$fields['office_id']			= 'Office ID';
			$fields['attachment']			= 'Attachment';
			$fields['accountability_status_id'] = 'Accountability Status ID';

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
				
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}	
	
	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
	
	
			$security = explode('/', $params['security']);
	
			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];
	
			check_salt($params['encoded_id'], $params['salt'], $params['token']);
	
			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function _validate_input($params)
	{
		try
		{
			$validation['property_no'] = array(
				'data_type' => 'string',
				'name'		=> 'Property Number',
				'min_len'	=> 3,
				'max_len'	=> 255
			);
			
			$validation['recieved_date'] = array(
				'data_type' => 'date',
				'name'		=> 'Site Classification',
				'min_len'	=> 3,
				'max_len'	=> 255
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function delete_asset_classification($params)
	{
		try
		{
			$flag				= 0;
			$msg				= "Error";
				
			if($this->permission_delete === FALSE)
				throw new Exception($this->lang->line('err_unauthorized_delete'));
				
			$params 			= get_params();
			$params['security']	= $params['param_1'];
	
			$this->_validate_security($params);
	
			// CHECK IF EXISTING RECORD
			$key 			= $this->get_hash_key('asset_classification_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];
				
			$info 			= $this->asset_classifications->get_specific_asset_classification($where);
								
			if(EMPTY($info['asset_classification_id']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			$asset_classification_id 	= $info['asset_classification_id'];
			$title						= $info['asset_classification_name'];
				
			PSMS_Model::beginTransaction();
				
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= PSMS_Model::tbl_asset_classifications;
			$audit_schema[]	= DB_PSMS;
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL
	
			$this->asset_classifications->delete_asset_classification($asset_classification_id);
	
			// SET AS AN ARRAY
			$curr_detail[]	= array();
				
			// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $title);
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
				
			PSMS_Model::commit();
	
			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}
		catch(PDOException $e)
		{
			PSMS_Model::rollback();
	
			$this->rlog_error($e);
			
			$fields	= array(
				'site_name'	=> 'Site Name',
				'site_desc'	=> 'Site Description',
			);
			
			$msg	= $this->get_user_message($e, $fields);
		}
		catch(Exception $e)
		{
	
			PSMS_Model::rollback();
			
			$this->rlog_error($e);
	
			$msg	= $e->getMessage();
		}
	
		$info = array(
			"flag" 		=> $flag,
			"msg" 		=> $msg,
			"reload" 	=> 'datatable',
			"table_id" 	=> 'classifications_table',
			"path"		=> PROJECT_PSMS . '/Psms_asset_classification/get_classification_list/'
		);
		echo json_encode($info);
	}

}