<?php if( ! defined('BASEPATH')) exit('No direct script acces allowed');

class Psms_ris extends PSMS_Controller {
	private $module = MODULE_PSMS_RIS;
	
	public function __construct()
	{
		parent:: __construct();
	}
	
	public function index()
	{
		$data = $resources = $modal = array();
		
		$modal = array(
			'modal_ris_info' 	=> array(
					'controller'	=> __CLASS__,
					'module'		=> PROJECT_PSMS,
					'multiple'		=> true,
					'method'		=> 'modal_ris_info',
					'size'			=> 'lg'
			)
		);
		
		$resources['load_css']		= array();
		$resources['load_css'] 		= array(CSS_DATATABLE);
		$resources['load_js'] 		= array(JS_DATATABLE);
		$resources['load_modal'] 	= $modal;
		$resources['datatable'] 	= array();
		$resources['datatable'][] 	= array('table_id' => 'ris_table', 'path' => PROJECT_PSMS.'/psms_ris/get_ris_list');
		
		$this->template->load('requests_and_issuances', $data, $resources);
		
	}
	
	public function get_ris_list()
	{
		$btn1 = "<div class='table-actions'>
					<a href='javascript:;'
						class='md-trigger tooltipped edit'
						data-position='bottom'
						data-delay='50'
						data-tooltip='Edit'
						data-modal='modal_ris_info'
						onclick=modal_ris_info_init('1')
					</a>
					<a href='javascript:;'
						class='tooltipped tooltipped delete'
						data-position='bottom'
						data-delay='50'
						data-tooltip='Delete'>
					</a>
				</div>";
		
		$btn2 = "<div class='table-actions'>
					<a href='javascript:;'
						class='md-trigger tooltipped edit'
						data-position='bottom'
						data-delay='50'
						data-tooltip='Edit'
						data-modal='modal_ris_info'
						onclick=modal_ris_info_init('2')
					</a>
					<a href='javascript:;'
						class='tooltipped tooltipped delete'
						data-position='bottom'
						data-delay='50'
						data-tooltip='Delete'>
					</a>
				</div>";
		
		$output = array(
				"aaData" => array(
						array("0001", "02/14/2015", "Christian Gil Aquino", "07/09/2014", $btn1),
						array("0002", "04/20/2016" , "Allen Joel Doctor", "08/10/2015", $btn2)
				)
		);
		
		echo json_encode( $output );
		
	}
	
	public function modal_ris_info($type)
	{
		$data = array();
		$resources = array();
		$resources['load_css'] 	= array(CSS_SELECTIZE, CSS_DATETIMEPICKER);
		$load_js 				= array('jquery.jscrollpane',JS_SELECTIZE,JS_DATETIMEPICKER);
		
		
		$data["type"] = $type;
		$resources['load_js'] = $load_js;
		$this->load->view('modals/requests_and_issuances_infos',$data);
		$this->load_resources->get_resource($resources);
	}
	
}

/* End of file Psms_ris.php */
/* Location: ./application/modules/psms/controllers/Psms_ris.php */