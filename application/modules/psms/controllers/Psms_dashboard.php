<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Psms_dashboard extends PSMS_Controller 
{
	private $module = MODULE_PSMS_DASHBOARD;

	public function __construct()
	{
		parent::__construct();

	}

	public function index()
	{

		$data 				= array();
		$resources 			= array();

		$this->template->load('dashboard', $data, $resources);
	}

}