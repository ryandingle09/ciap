<?php if( ! defined('BASEPATH')) exit('No direct script acces allowed');

class Psms_reports_stock_card extends PSMS_Controller {
	
	public function __construct()
	{
		parent:: __construct();
	}
	
	public function generate()
	{
	
		try {
				
			$params				= get_params();
				
			switch(strtoupper($params['generate']))
			{
				case "PDF":
				case "EXCEL":
						
					$head	= $this->_construct_report_header();
					$body	= $this->_construct_report_body($info);
	
					$report = array(
							'content'	=> $head . $body,
					);
	
					return $report;
						
					break;
			}
		}
		catch (PDOException $e)
		{
			throw $e;
		}
		catch (Exception $e)
		{
			throw $e;
		}
			
	}
	
	public function _construct_report_header(){
		return '<div>_construct_report_header</div>';
	}
	public function _construct_report_body(){
		return '<div>_construct_report_header</div>';
	}
	

}

/* End of file Psms_reports_stock_card.php */
/* Location: ./application/modules/psms/controllers/Psms_reports_stock_card.php */