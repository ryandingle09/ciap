<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Psms_assets extends PSMS_Controller 
{
	private $module = MODULE_PSMS_ASSETS;

	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('psms_assets_model', 'assets');
		$this->load->model('psms_asset_classifications_model', 'classifications');
		$this->load->model('psms_asset_categories_model', 'categories');
		$this->load->model('psms_asset_site_model', 'categories');
		$this->load->model('psms_asset_locations_model', 'locations');
		$this->load->model('psms_asset_accountabilities_model', 'accountabilities');

		$this->load->model('params_model', 'params');

		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);
	}

	public function index()
	{
		try{
			
			if($this->permission)
			{
				$data 		= array();
				$resources 	= array(
					'load_css' 	=> array(
						CSS_DATATABLE,
						CSS_SELECTIZE,
						CSS_MODAL_COMPONENT,
						CSS_DATETIMEPICKER,
						'psms',
					),
					'load_js' 	=> array(
						JS_DATATABLE,
						JS_SELECTIZE,
						JS_MODAL_CLASSIE,
						JS_MODAL_EFFECTS,
						JS_DATETIMEPICKER,
					),
					'load_modal' 	=> array(
						'modal_asset' => array(
							'controller'	=> __CLASS__,
							'method'		=> 'modal_asset', // DEFAULT VALUE IS MODAL
							'module'		=> PROJECT_PSMS,
							'title'			=> 'Asset',
							'size'			=> 'lg',
							'multiple'   	=> true,
							'height'		=> '350px',
						),
					),
					'load_init'=> array(
					),
					'datatable2' => array(
						array(
							'table_id'	=> 'assets_table', 
							'sAjaxSource' => base_url().PROJECT_PSMS.'/psms_assets/get_asset_list',
						)
					)	
				);

				// STORE THE PERMISSION
				$data['permission_add']	= $this->permission_add;
				
				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash(0);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);
				$data['url']	= $encoded_id."/".$salt."/".$token;
				$view_page	= "assets";
			}
			else
			{
				$view_page	= "errors/html/error_permission";
			}

			$this->template->load($view_page, $data, $resources);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}

	public function get_asset_list()
	{
		try {
			
			$params = get_params(); // DO NOT USE: $_POST $this->input->post
			
			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> 0,
				"iTotalDisplayRecords" 	=> 0,
				"aaData" 				=> array()
			);			
			
			if(!$this->permission)
				throw new Exception($this->lang->line('err_unauthorized_view'));
			
			// FIELDS TO BE SELECTED FROM TABLE
			$select_fields = array(
				"A.property_no",
				"A.property_acquired_date",				
				"A.property_name",
				"A.property_ref_no",
				"S.asset_status_name",
				"A.property_useful_life",
			);
			
			// APPEARS ON TABLE
			$where_fields = array(
				"A.property_no",
				"A.property_acquired_date",				
				"A.property_name",
				"A.property_ref_no",
				"S.asset_status_name",
				"A.property_useful_life",	
			);			
				
			$results 		= $this->assets->get_asset_list($select_fields, $where_fields, $params);
			$total	 		= $this->assets->total_length(); // TOTAL COUNT OF RECORDS
			$filtered_total = $this->assets->filtered_length($select_fields, $where_fields, $params); // TOTAL COUNT OF RECORDS PER PAGE
			
			if(!EMPTY($results))
			{
				foreach($results as $data)
				{
					
					// PRIMARY KEY
					$primary_key	= $data['property_no'];
					
					// CONSTRUCT SECURITY VARIABLES
					$hash_id 		= $this->hash($primary_key);
					$encoded_id 	= base64_url_encode($hash_id);
					$salt 			= gen_salt();
					$token 			= in_salt($encoded_id, $salt);
					$url 			= $encoded_id."/".$salt."/".$token;
					
									
					// START: CONSTRUCT ACTION ICONS
					$action = "<div class='table-actions'>";
					
						if($this->permission_edit) :
							$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_asset' onclick=\"modal_asset_init('".$url."')\"></a>";
						endif;
						
						if($this->permission_delete) :
							$delete = 'content_delete("asset_site", "'.$url.'")';
							$action.= "<a href='javascript:;' onclick='" . $delete. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'>&nbsp</a>";
						endif;
					
					$action .= '</div>';
					// END: CONSTRUCT ACTION ICONS				
					
					$row 	= array();
					$row[]	= $data['property_no'];
					$row[]	= $data['property_acquired_date'];
					$row[]	= $data['property_name'];
					$row[]	= $data['property_ref_no'];
					$row[]	= $data['asset_status_name'];
					$row[]	= $data['property_useful_life'];
					$row[]	= $action;
					
					$output['aaData'][] = $row;
					
				}

				$output["iTotalRecords"] 		= $total["cnt"];
				$output["iTotalDisplayRecords"] = $filtered_total["cnt"];
			}
		}
		catch(PDOException $e)
		{
			$fields	= array(
				"property_no" => 'property_no',
				"property_name"	=> 'property_name'
			);
			
			$msg = $this->get_user_message($e, $fields);
		}
		catch(Exception $e)
		{
			$msg = $e->getMessage();		
		}
		echo json_encode( $output );		
	}

	public function get_status_history()
	{
		$output = array(
			"aaData" => array(
				array('2015-01-01', '<b>In stock</b> to <b>In service</b>', 'Assigned to Ms. Lorie Rodriguez', 'John Felix Bencito'),
				array('2015-12-03', '<b>In service</b> to <b>For repair</b>', 'Pulled out for repair', 'Dala M. Hazim'),
				array('2015-12-03', '<b>For repair</b> to <b>In service</b>', 'Returned to Ms. Lorie Rodriguez', 'Dendi Calbordozo'),
			)
		);

		echo json_encode( $output );
	}

	public function get_search_employees()
	{

		$output = array(
			"aaData" => array(
				array('2014-001', 'Dir. Mike Galang', 'Main Office'),
				array('2014-002', 'Engr. Lucille Rodriguez', 'Davao Branch'),
			)
		);

		echo json_encode( $output );
	}

	public function modal_asset($encoded_id, $salt, $token)
	{
		try
		{
			$msg		= "";			
			$data 		= array();
			$json_data	= "";
			
			// STORES THE SELECTED RECORD
			$info	= array();
				
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);
			
			$hash_id = base64_url_decode($encoded_id);
			
			$data['permission_save']	= $this->permission_add;
			
			if($this->hash(0) != $hash_id)
			{
				
				$key			= $this->get_hash_key('property_no');

				$where			= array();
				$where[$key]	= $hash_id;
				$info = $this->assets->get_specific_asset($where);
			
				IF(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));
				

				// GET PARAMS
				$where 				 	 = array('asset_category_id' => $info['asset_category_id']);
				$data['classifications'] = $this->params->get_params(PSMS_Model::tbl_asset_classifications, $where);
				
				$where		 		 	 = array('site_id' => $info['site_id']);
				$data['locations'] 		 = $this->params->get_params(PSMS_Model::tbl_asset_locations, $where);
				
				$where 					 = array('property_no' => $info['property_no']);
				$data['accountabilities']= $this->accountabilities->get_accountabilities($where);
				$data['permission_save'] = $this->permission_edit;

				// REGENERATE DATATABLES PARAMETERS
				$ajax_param = json_encode(array(
					'name' => 'property_no',
					'value' => $info['property_no'],
				));
			}
			
			$data['info']	= $info; // DETAILS

			// GET PARAMS
			$data['conditions']	= $this->params->get_params(PSMS_Model::tbl_param_asset_conditions);
			$data['statuses']	= $this->params->get_params(PSMS_Model::tbl_param_asset_status);
			$data['categories']	= $this->params->get_params(PSMS_Model::tbl_asset_categories);
			$data['sites']		= $this->params->get_params(PSMS_Model::tbl_param_sites);
			
			// REGENERATE SECURITY VARIABLES
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;

		

			//SET DATATABLES AJAX DATA
			$resources = array(
				'load_css' => array(
					CSS_DATATABLE,
					CSS_MODAL_COMPONENT, 
					'selectize.default'
				),
				'load_js' 	=> array(
					JS_DATATABLE,
					JS_PSMS_ASSET,
					'popModal.min',
					'selectize',

				),
				'loaded_init'=> array(
					'datepicker_init();',
					'selectize_init();',
					'ModalEffects.re_init();',
				),
				'datatable2' => array(
					array(
						'table_id'	=> 'asset_history_table',
						'sAjaxSource' => base_url().PROJECT_PSMS.'/psms_asset_accountabilities/get_accountability_list/',
						'fnServerParams' => addslashes($ajax_param),
					),
				),
				'load_modal' => array(
					'modal_classifications' => array(
						'controller'	=> 'psms_asset_classification',
						'method'		=> 'modal_classifications', // DEFAULT VALUE IS MODAL
						'module'		=> PROJECT_PSMS,
						'title'			=> 'MANAGE CLASSIFICATION',
						'multiple'   	=> true,
						'size'			=> 'md',
					),	
					'modal_sites' => array(
						'controller'	=> 'psms_asset_site',
						'method'		=> 'modal_sites', // DEFAULT VALUE IS MODAL
						'module'		=> PROJECT_PSMS,
						'title'			=> 'MANAGE SITES',
						'multiple'   	=> true,
						'size'			=> 'md'
					),
					'modal_locations' => array(
						'controller'	=> 'psms_asset_location',
						'method'		=> 'modal_locations', // DEFAULT VALUE IS MODAL
						'module'		=> PROJECT_PSMS,
						'title'			=> 'MANAGE LOCATIONS',
						'multiple'   	=> true,
						'size'			=> 'md'
					),
					'modal_accountable' => array(
						'controller'	=> 'psms_asset_accountabilities',
						'method'		=> 'modal_accountable', // DEFAULT VALUE IS MODAL
						'module'		=> PROJECT_PSMS,
						'title'			=> 'ACCOUNTABLE',
						'multiple'   	=> true,
						'size'			=> 'sm',
					),		
					'modal_accountability_transfer' => array(
						'controller'	=> __CLASS__,
						'method'		=> 'modal_accountability_transfer', // DEFAULT VALUE IS MODAL
						'module'		=> PROJECT_PSMS,
						'title'			=> 'ACCOUNTABILITY TRANSFER',
						'multiple'   	=> true,
						'size'			=> 'md',
					),		
					'modal_accountability_deactivate' => array(
						'controller'	=> __CLASS__,
						'method'		=> 'modal_accountability_deactivate', // DEFAULT VALUE IS MODAL
						'module'		=> PROJECT_PSMS,
						'title'			=> 'ACCOUTABILITY DEACTIVATE',
						'multiple'   	=> true,
						'size'			=> 'sm',
					),
					'modal_change_status_remarks' => array(
						'controller'	=> __CLASS__,
						'method'		=> 'modal_change_status_remarks', // DEFAULT VALUE IS MODAL
						'module'		=> PROJECT_PSMS,
						'title'			=> 'REMARKS',
						'multiple'   	=> true,
						'size'			=> 'sm',
					),
					'modal_status_history' => array(
						'controller'	=> __CLASS__,
						'method'		=> 'modal_status_history', // DEFAULT VALUE IS MODAL
						'module'		=> PROJECT_PSMS,
						'title'			=> 'STATUS HISTORY',
						'multiple'   	=> true,
						'size'			=> 'lg',
					),	
				),	
				
			);
			$data['action'] = $action;
		
			$this->load->view("modals/asset", $data);
			$this->load_resources->get_resource($resources);
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

	public function modal_accountability_transfer($encoded_id, $salt, $token)
	{
	
		try
		{
			$data 		= array();
			$resources 	= array();
			
			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			$resources = array(
				'load_css' => array(
					'selectize.default'
				),
				'load_js' 	=> array(
					'popModal.min',
					'selectize',
				),
				'loaded_init'=> array(
					'datepicker_init();',
					'selectize_init();'
				),
				'load_modal' => array(
					'modal_search_employee' => array(
						'controller'	=> __CLASS__,
						'method'		=> 'modal_search_employee', // DEFAULT VALUE IS MODAL
						'module'		=> PROJECT_PSMS,
						'title'			=> 'Search Employee',
						'multiple'   	=> true,
						'size'			=> 'md',
					),	
				),	
			);
			
			$this->load->view("modals/accountability_transfer", $data);
			$this->load_resources->get_resource($resources);
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

	public function modal_accountability_deactivate($encoded_id, $salt, $token)
	{
	
		try
		{
			$data 		= array();
			$resources 	= array();
			
			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			$resources = array(
				'load_css' => array(
					'selectize.default'
				),
				'load_js' 	=> array(
					'popModal.min',
					'selectize',
				),
				'loaded_init'=> array(
					'datepicker_init();',
					'selectize_init();'
				)
			);
			
			$this->load->view("modals/accountability_deactivate", $data);
			$this->load_resources->get_resource($resources);
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

	public function modal_search_employee($encoded_id, $salt, $token)
	{
	
		try
		{
			$data 		= array();
			$resources 	= array();
			
			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			$resources = array(
				'load_css' => array(
					'selectize.default'
				),
				'load_js' 	=> array(
					JS_DATATABLE,
					'popModal.min',
					'selectize',
				),
				'loaded_init'=> array(
					'datepicker_init();',
					'selectize_init();'
				),
				'datatable2' => array(
					array(
						'table_id'	=> 'search_employee_table', 
						'sAjaxSource' => base_url().PROJECT_PSMS.'/psms_assets/get_search_employees',
						'bPaginate' => false,
						'bInfo'		=> false,
					)
				)
			);
			
			$this->load->view("modals/search_employee", $data);
			$this->load_resources->get_resource($resources);
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

	public function modal_change_status_remarks($encoded_id, $salt, $token){
		try
		{
			$data 		= array();
			$resources 	= array();
			
			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			$resources = array(
				'load_css' => array(),
				'load_js' 	=> array(),
				'loaded_init'=> array(),
			);
			
			$this->load->view("modals/change_status_remarks", $data);
			$this->load_resources->get_resource($resources);
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

	public function modal_status_history($encoded_id, $salt, $token)
	{
	
		try
		{
			$data 		= array();
			$resources 	= array();
			
			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			$resources = array(
				'load_css' => array(
					'selectize.default'
				),
				'load_js' 	=> array(
					JS_DATATABLE,
					'popModal.min',
					'selectize',
				),
				'loaded_init'=> array(
					'datepicker_init();',
					'selectize_init();'
				),
				'datatable2' => array(
					array(
						'table_id'	=> 'status_history', 
						'sAjaxSource' => base_url().PROJECT_PSMS.'/psms_assets/get_status_history',
						'bPaginate' => false,
						'bFilter'	=> false,
						'bInfo'		=> false,
					)
				)
			);
			
			$this->load->view("modals/status_history", $data);
			$this->load_resources->get_resource($resources);
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function process()
	{
		try
		{
			$flag 			= 0;
			$params 		= get_params();
			
			// SERVER VALIDATION
			$this->_validate($params);
			
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('property_no');

			$where			= array();
			$where[$key]	= $params['hash_id'];
			
			$info 			= $this->assets->get_specific_asset($where);
			$property_no	= ISSET($info['property_no']) ? $info['property_no'] : 0;			

			PSMS_Model::beginTransaction();
			
			// IF EMPTY MEANS INSERT/SAVE RECORD
			
			IF(EMPTY($info)){
				
				
				// CHECK IF THE RECORD IS ALREADY EXIST
				// *** $exist = $this->_check_bldg_cons($params);
				
				// *** IF($exist)	
					// *** throw new Exception('This record is arlready added.');
				
				
				if($this->permission_add === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_add'));

				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_INSERT;
				$audit_table[]	= PSMS_Model::tbl_assets;
				$audit_schema[]	= DB_PSMS;
				$prev_detail[]	= array();
				
				$asset_id = $this->assets->insert_asset($params); // SAVES THE BUILDING CONSTRUCTION RECORD
				
				// GET THE CURRENT DATA				
				$current_info 	= $this->assets->get_specific_asset(array("property_no" => $property_no));
				
				$curr_detail[]	= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				
				$activity_title	= $current_info['property_no'];

				$activity	= "%s has been added in assets.";
				$activity 	= sprintf($activity, $activity_title);

				$msg  = $this->lang->line('data_saved');
			}

			else{

				if($this->permission_edit === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_edit'));
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				
				$prev_info 	= $this->assets->get_specific_asset(array("property_no" => $property_no)); // GET THE CURRENT DATA
				
				$activity_title	= $prev_info['property_no'];

				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= PSMS_Model::tbl_assets;
				$audit_schema[]	= DB_PSMS;
				$prev_detail[]	= array($prev_info);

	
				$params['old_property_no'] 		= $params['hash_id'];
				$params['old_property_no_key']	= $key; 
				$this->assets->update_asset($params); // UPDATE THE ASSET

				$current_info 	= $this->assets->get_specific_asset(array("property_no" => $params['property_no'])); // GET THE CURRENT DATA
				
				$curr_detail[]	= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				
				
				$activity	= "%s has been updated in Assets.";
				$activity 	= sprintf($activity, $activity_title);

				$msg  = $this->lang->line('data_updated');
			}

			$this->rlog_info(var_export($prev_detail, true));
			$this->rlog_info(var_export($curr_detail, true));
			$this->rlog_info(var_export($audit_action, true));
			$this->rlog_info(var_export($audit_table, true));
			$this->rlog_info(var_export($audit_schema, true));
			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table, 
				$audit_schema
			);
			
			PSMS_Model::commit();

			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			PSMS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
			
		}
		catch(Exception $e)
		{
			PSMS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	}

	private function _validate(&$params)
	{
		try
		{

			$this->_validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields 				= array();
			$fields['property_no']				= 'Property No.';
			//$fields['old_property_no']		= 'old_property_no';
			$fields['property_name']			= 'Property Name';
			$fields['property_desc']			= 'Property Notes';
			$fields['property_serial_no']		= 'Property Serial Number';
			$fields['asset_classification_id']	= 'Classification ID';
			$fields['asset_location_id']		= 'Location ID';
			$fields['property_acquired_date']	= 'Acquired Date';
			$fields['property_ref_no']			= 'Reference Number';
			$fields['property_unit_cost']		= 'Unit Cost';
			$fields['property_accountable_officer']	= 'Accountable Officer';
			$fields['property_make']			= 'Make';
			$fields['property_model']			= 'Model';
			//$fields['property_useful_life']		= 'Property Useful Life';
			//$fields['property_useful_life_flag']= 'property_useful_life_flag';
			$fields['asset_condition_id']		= 'Condition ID';
			$fields['asset_status_id']			= 'Status ID';
			//$fields['file_name']				= '';
			
			

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function _validate_input($params)
	{
		try
		{
			$validation['property_no'] = array(
				'data_type' => 'string',
				'name'		=> 'Property No.',
				'min_len'	=> 5,
				'max_len'	=> 255
			);
			$validation['property_name'] = array(
				'data_type' => 'string',
				'name'		=> 'Property Name',
				'min_len'	=> 5,
				'max_len'	=> 255
			);
			$validation['property_desc'] = array(
				'data_type' => 'string',
				'name'		=> 'Property Notes',
				'min_len'	=> 5,
				'max_len'	=> 60000
			);
			$validation['property_serial_no'] = array(
				'data_type' => 'string',
				'name'		=> 'Property Serial No.',
				'min_len'	=> 5,
				'max_len'	=> 255
			);
			$validation['asset_classification_id'] = array(
				'data_type' => 'digit',
				'name'		=> 'Asset Classification ID'
			);
			
			$validation['asset_location_id'] = array(
				'data_type' => 'digit',
				'name'		=> 'Asset Location ID'
			);
			$validation['property_acquired_date'] = array(
				'data_type' => 'date',
				'name'		=> 'Property Acquired Date'
			);

			$validation['property_ref_no'] = array(
				'data_type' => 'string',
				'name'		=> 'Property Reference No.',
				'min_len'	=> 5,
				'max_len'	=> 255
			);
			$validation['property_unit_cost'] = array(
				'data_type' => 'digit',
				'name'		=> 'Property Unit Cost'
			);
			$validation['property_accountable_officer'] = array(
				'data_type' => 'digit',
				'name'		=> 'Property Accountable Officer'
			);
			$validation['property_make'] = array(
				'data_type' => 'string',
				'name'		=> 'Property Make',
				'min_len'	=> 3,
				'max_len'	=> 255
			);
			$validation['property_model'] = array(
				'data_type' => 'string',
				'name'		=> 'Property Model',
				'min_len'	=> 5,
				'max_len'	=> 255
			);
			/* $validation['property_useful_life'] = array(
				'data_type' => 'digit',
				'name'		=> 'Property Useful Life'
			);*/
			$validation['asset_condition_id'] = array(
				'data_type' => 'digit',
				'name'		=> 'Asset Condition ID'
			);
			$validation['asset_status_id'] = array(
				'data_type' => 'digit',
				'name'		=> 'Asset Status ID'
			);
			
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function delete_asset($params)
	{
		try
		{	
			$flag				= 0;
			$msg				= "Error";
				
			if($this->permission_delete === FALSE)
				throw new Exception($this->lang->line('err_unauthorized_delete'));
				
			$params 			= get_params();
			$params['security']	= $params['param_1'];
	
			$this->_validate_security($params);
	
			// CHECK IF EXISTING RECORD
			$key 			= $this->get_hash_key('property_no');
			$where			= array();
			$where[$key]	= $params['hash_id'];
				
			$info 			= $this->assets->get_specific_asset($where);
								
			if(EMPTY($info['property_no']))
				throw new Exception($this->lang->line('err_invalid_request'));
			
			$property_no 	= $info['property_no'];
			$property_name	= $info['property_name'];
				
			PSMS_Model::beginTransaction();
				
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= PSMS_Model::tbl_assets;
			$audit_schema[]	= DB_PSMS;
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL
	
			$this->assets->delete_asset($property_no);
	
			// SET AS AN ARRAY
			$curr_detail[]	= array();
				
			// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $title);
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
				
			PSMS_Model::commit();
	
			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}
		catch(PDOException $e)
		{
			PSMS_Model::rollback();
	
			$this->rlog_error($e);
			
			$fields	= array(
				'property_no'	=> 'Property Number',
				'propert_name'	=> 'Property Name',
			);
			
			$msg	= $this->get_user_message($e, $fields);
		}
		catch(Exception $e)
		{
	
			PSMS_Model::rollback();
			
			$this->rlog_error($e);
	
			$msg	= $e->getMessage();
		}
	
		$info = array(
			"flag" 		=> $flag,
			"msg" 		=> $msg,
			"reload" 	=> 'datatable',
			"table_id" 	=> 'assets_table',
			"path"		=> PROJECT_PSMS.'/psms_assets/get_asset_list/'
		);
	
		echo json_encode($info);
	}
}