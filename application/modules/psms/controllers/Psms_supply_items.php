<?php if( ! defined('BASEPATH')) exit ('No direct script access allowed');

class Psms_supply_items extends PSMS_Controller {
	
	private $module = MODULE_PSMS_SUPPLIES_INFO;
	
	public $permission_add		= FALSE;
	public $permission_save		= FALSE;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('psms_supplies_info_model', 'supplies_info');
		
		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);

	}
	
	public function index()
	{
		
		try 
		{
			$data		= array();
		}
		
		catch (PDOException $e)
		{
			$msg = $e->getMessage();
		}
		
		catch(Exception $e)
		{
			$msg = $e->getMessage();
		}
		
	}
	
	public function modal_supply_items(){
		$data		= array();
		
		$data['permission_add'] = $this->permission_add;
		
		// VALIDATE IF ONE CATEGORY IS SELECTED BEFORE OPENING MODAL
		
		$resources	= array(
				'load_modal'	=> array(
						'modal_supply_add_item' 	=> array(
								'controller'	=> __CLASS__,
								'multiple'		=> true,
								'module'		=> PROJECT_PSMS,
								'method'		=> 'modal_supply_add_item'
						)
				),
				'datatable'		=> array(
						array(
								'table_id'	=> 'supply_items_table',
								'path'		=> PROJECT_PSMS.'/psms_supply_items/get_items'
						)
				)
	
		);
		
		
		$this->load->view('modals/supply_items',$data);
		$this->load_resources->get_resource($resources);
	}
	
	
	public function get_items()
	{
		try
		{
			$params_search = get_params(); // DO NOT USE: $_POST $this->input->post
			$params['search_type']		= $params_search['search_type'];
			$params['search_category']	= $params_search['search_category'];
			$params['sSearch']			= $params_search['search_str'];
			$params['sEcho']			= 1;
			$params['iColumns']			= 4;
			$params['sColumns'] 		= ',,,';
			$params['iDisplayStart']	= 0;
			$params['iDisplayLength'] 	= -1;
			$params['mDataProp_0']		= 0;
			$params['sSearch_0'] 		= '';
			$params['bRegex_0']			= false;
			$params['bSearchable_0']	= true;
			$params['bSortable_0'] 		= true;
			$params['mDataProp_1'] 		= 1;
			$params['sSearch_1'] 		= '';
			$params['bRegex_1'] 		= false;
			$params['bSearchable_1'] 	= true;
			$params['bSortable_1'] 		= true;
			$params['mDataProp_2'] 		= 2;
			$params['sSearch_2'] 		= '';
			$params['bRegex_2'] 		= false;
			$params['bSearchable_2'] 	= true;
			$params['bSortable_2'] 		= true;
			$params['mDataProp_3'] 		= 3;
			$params['sSearch_3'] 		= '';
			$params['bRegex_3'] 		= false;
			$params['bSearchable_3'] 	= true;
			$params['bSortable_3'] 		= false;
			$params['bRegex'] 			= false;
			$params['iSortCol_0'] 		= 0;
			$params['sSortDir_0'] 		= asc;
			$params['iSortingCols']		= 1;
			
			$output = array(
					"sEcho"					=> intval($params['sEcho']),
					"iTotalRecords"			=> 0,
					"iTotalDisplayRecords"	=> 0,
					"aaData"				=> array()
			);
	
	
			// FIELDS TO BE SELECTED FROM TABLE
			$select_fields = array(
					"A.supplies_item_id",
					"A.supplies_stock_no",
					"B.supplies_category_name",
					"A.supplies_item_name",
					"A.supplies_item_desc"
			);
			
	
			// APPEAR ON TABLE
			if($params['search_type'] == "name")
			{
				$where_fields = array(
						"supplies_item_name"
				);
			}
			else
			{
				$where_fields = array(
						"supplies_stock_no",
						"supplies_item_name",
						"supplies_category_name"
				);
			}
			
			$results		= $this->supplies_info->get_items($select_fields, $where_fields, $params); // STORES ITEMS
			$total			= $this->supplies_info->total_length_items(); // TOTAL COUNT OF RECORDS
			$filtered_total	= $this->supplies_info->filtered_length_items($select_fields, $where_fields, $params); // TOTAL COUNT OF RECORDS PER PAGE
			
			if(!EMPTY($results))
			{
				foreach ($results as $data)
				{
					// PRIMARY KEY
					$primary_key	= $data['supplies_item_id'];
						
					// START: CONSTRUCT ACTION ICONS
					$action = "<input name='cbgroup1' type='radio' id='" . $data["supplies_item_id"] ."' onclick='get_item_id(this)'/> <label for='". $data["supplies_item_id"] ."'></a>";
					// END: CONSTRUCT ACTION ICONS
					$row 	= array();
					$row[]	= $action;
					$row[]	= $data['supplies_stock_no'];
					$row[]	= $data['supplies_category_name'];
					$row[]	= $data['supplies_item_name'];
					$row[]	= $action1;
					
					$output['aaData'][] = $row;
				}
	
				$output["iTotalRecords"] 		= $total["cnt"];
				$output["iTotalDisplayRecords"] = $filtered_total["cnt"];
			}
	
		}
		catch (PDOExcepton $e)
		{
			$fields = array(
					"A.supplies_item_id"		=> "Item ID",
					"A.supplies_stock_no"		=> "Stock No.",
					"B.supplies_category_name"	=> "Category",
					"A.supplies_item_name"		=> "Item Name",
					"A.supplies_item_desc"		=> "Notes"
			);
			$msg = $this->get_user_message($e, $fields);
		}
	
		catch(Exception $e)
		{
			$msg = $e->getMessage();
		}
	
	
		echo json_encode( $output );
	}
	
	public function modal_supply_add_item(){
		$data = array();
	
		$resources = array(
				'load_css'	=> array(CSS_SELECTIZE),
				'load_js'	=> array(JS_SELECTIZE),
		);
		
		// STORES CATEGORIES
		$data['supply_categories']		= $this->supplies_info->get_categories();
		
		$data['permission_save']	= $this->permission_add;
	
	
		$this->load->view('modals/supply_add_item',$data);
		$this->load_resources->get_resource($resources);
	}
	
	public function process()
	{
		try
		{
			$flag 			= 0;
			$msg			= "";
			$params 		= get_params();
			
			
			//SERVER VALIDATION
			$this->_validate($params);
		
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 				= $this->get_hash_key('supplies_item_id');
		
			$where				= array();
			$where[$key]		= $params['hash_id'];
		
			$info 				= $this->supplies_info->get_specific_supply_item($where);
			$supplies_item_id	= ISSET($info['supplies_item_id']) ? $info['supplies_item_id'] : 0;
				
			PSMS_Model::beginTransaction();
		
			// IF ID IS EMPTY, IT MEANS INSERT/SAVE RECORD
			if(EMPTY($supplies_item_id))
			{
				if($this->permission_add === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_add'));
					
					// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
					$audit_action[]	= AUDIT_INSERT;
					$audit_table[]	= PSMS_Model::tbl_supplies_items;
					$audit_schema[]	= DB_PSMS;
					$prev_detail[]	= array();

					$supplies_item_id = $this->supplies_info->insert_item($params); // SAVES THE RECORD

					// GET THE CURRENT DETAIL
					$where						= array();
					$where['supplies_item_id']	= $supplies_item_id;
					$info 						= $this->supplies_info->get_specific_supply_item($where);
					$title						= $info['supplies_item_id'];
		
					$curr_detail[]	= array($info);
					// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
		
		
					$activity	= "%s has been added in supplies items.";
					$activity 	= sprintf($activity, $title);
		
					$msg  = $this->lang->line('data_saved');
			}

			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
					$activity,
					$this->module,
					$prev_detail,
					$curr_detail,
					$audit_action,
					$audit_table,
					$audit_schema
					);
		
		
			PSMS_Model::commit();
		
			$flag = 1;
		}
		catch(PDOException $e)
		{
			PSMS_Model::rollback();
		
			$this->rlog_error($e);
		
			$fields = array(
				'item_category'		=> 'Category',
				'item_stock_no'		=> 'Stock No.',
				'item_name'			=> 'Item Name ',
				'item_description'	=> 'Description'
		
			);
		
			$msg	= $this->get_user_message($e, $fields);
		}
		catch(Exception $e)
		{
			PSMS_Model::rollback();
		
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
				"flag"	=> $flag,
				"msg" 	=> $msg
		);
		
		echo json_encode($info);
	}
	
	private function _validate(&$params)
	{
		try
		{
	
// 			$this->_validate_security($params);
	
			//SPECIFY HERE INPUTS FROM USER
	
			$fields 						= array();
			$fields['item_category']		= 'Category';
			$fields['item_stock_no']		= 'Stock No.';
			$fields['item_name']			= 'Item Name ';
			$fields['item_description']		= 'Description';
				
			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
	
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}
	
	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
	
	
			$security = explode('/', $params['security']);
	
			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];
	
			check_salt($params['encoded_id'], $params['salt'], $params['token']);
	
			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	
	private function _validate_input($params)
	{
		try
		{
						
						$validation['item_stock_no'] = array(
								'data_type' => 'string',
								'name'		=> 'Stock No.',
								'min_len'	=> 3,
								'max_len'	=> 45
						);
						
						$validation['item_name'] = array(
								'data_type' => 'string',
								'name'		=> 'Item Name',
								'min_len'	=> 3,
								'max_len'	=> 100
						);
	
						$validation['item_description'] = array(
								'data_type' => 'string',
								'name'		=> 'Description',
								'min_len'	=> 3,
								'max_len'	=> 255
						);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function get_specific_item()
	{
		$data						= array();
		try
		{
			$params 					= get_params();
			$where						= array();
			$where['supplies_item_id']	= $params['item_id'];
			$info 						= $this->supplies_info->get_specific_supply_item($where);
		
			$data['info']				= $info;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		
		echo json_encode($data);
		
	}
}