<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Isca_books extends ISCA_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("book_model", "book");
	}

	public function index() {

		//point to catalog list as default

		$this->card_catalogues();

	}

	public function card_catalogues() {

		$data = $resources = $modal = array();

		$modal = array(
			'card_catalog' 		=> array(
				'controller'	=> __CLASS__,
				'module'		=> PROJECT_ISCA,
				'height'		=> '500px',
			)
		);
		
		$resources['load_css'] 		= array(CSS_DATATABLE);
		$resources['load_js'] 		= array(JS_DATATABLE, JS_EDITOR);
		$resources['load_modal'] 	= $modal;
		$resources['datatable'][] 	= array('table_id' => 'books_table', 'path' => PROJECT_ISCA.'/isca_books/get_catalog_list');
		
		$this->template->load('library/card_catalog', $data, $resources);
	}


	/*GET ALL CATALOG LIST*/
	public function get_catalog_list() {

		try {

			$params 		= get_params();

			$select_fields 	= array("A.book_id","A.code_no", "A.title", "B.book_author_name", "A.publishing_date", "A.quantity");

			$where_fields	= array("A.code_no", "A.title", "B.book_author_name", "A.publishing_date", "A.quantity");

			$books = $this->book->get_book_list($select_fields, $where_fields, $params);

			$total 				= $this->book->total_length();
			$filtered_total 	= $this->book->filtered_length($select_fields, $where_fields, $params);



			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> $total["cnt"],
				"iTotalDisplayRecords" 	=> $filtered_total["cnt"],
				"aaData" 				=> array()
			);
			
		 	foreach ($books as $data):

 				$book_id 		= $data['book_id'];
 				$hash_id 		= $this->hash($book_id);
 				$encoded_id 	= base64_url_encode($hash_id);
 				$salt 			= gen_salt();
 				$token 			= in_salt($encoded_id, $salt);
 				$url 			= $encoded_id.'/'.$salt.'/'.$token;

 				$delete_action = 'content_delete("Book", "'.$url.'")';
 				
 				$action = "<div class='table-actions'>";

 						$action .= "<a href='javascript:;' class='md-trigger view tooltipped' data-modal='card_catalog' data-tooltip='View' data-position='bottom' data-delay='50' onclick=\"modal_init('".$url."')\"></a>";
					//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
						$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='card_catalog' onclick=\"modal_init('".$url."')\"></a>";
					//endif;

					//if (($this->permission->check_permission(MODULE_ROLE, ACTION_DELETE))) : 	  
						$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					//endif;
				
				$action .= '</div>';


				$row = array();

				$row[]	= $data['code_no'];
				$row[]	= $data['title'];
				$row[]	= $data["book_author_name"];
				$row[]	= $data['publishing_date'];
				$row[]	= $data['quantity'];
				$row[] 	= $action;


				$output['aaData'][] = $row;
	 		endforeach;

		 	echo json_encode($output);
		}
		catch (PDOException $e) {
			echo $e->getMessage();
		}
	 	catch (Exception $e) {
	 		echo $e->getMessage();
		}
		
	}	

	/*EITHER SAVE OR EDIT PROCESS*/
	public function process() {

		try {

			$flag 	= 0;
			$msg 	= "Error";

			$params = get_params();

			/*SERVER VALIDATE*/
			$this->_validate($params);

			$key 			= $this->get_hash_key("book_id");
			$where 			= array();
			$where[$key] 	= $params["hash_id"];

			$current_info 	= $this->book->get_specific_book($where);
			$book_id 		= $current_info['book_id'];

			ISCA_Model::beginTransaction();

			/*IF EMPTY ID DATA IS FOR CREATE*/
			if(empty($book_id)) {

				/*DATA VARIABLES FOR AUDIT TRAILS*/
				$audit_action[]		= AUDIT_INSERT;
				$audit_table[]		= ISCA_Model::tbl_card_catalog;
				$audit_schema[]		= DB_ISCA;
				$prev_detail[]		= array();
				/*END AUDIT TRAIL VARIABLES*/

				/*SAVE DATA*/
				$book_id = $this->book->insert_book($params);

				/*GET CURRENT DATA*/
				$info 			= $this->book->get_specific_book(array("book_id" => $book_id) , TRUE);
				$curr_detail[]  = $info;

				$book_title = $info['title'];

				/*DATA USED FOR AUDIT TRAIL*/
				$activity	= "%s has been added in card catalog.";
				$activity 	= sprintf($activity, $book_title);
				/*END*/

				$msg  = $this->lang->line("data_saved");

			} else {


				/*DATA VARIABLES FOR AUDIT TRAILS*/
				$audit_action[]		= AUDIT_UPDATE;
				$audit_table[]		= ISCA_Model::tbl_arbitrator;
				$audit_schema[]		= DB_ISCA;
				$prev_detail[]		= array($current_info);
				/*END AUDIT TRAIL VARIABLES*/

				/* UPDATE DATA*/
				$params['book_id']	= $book_id;
				$this->book->update_book($params);

				/*GET CURRENT DETAIL*/
				$info 				= $this->book->get_specific_book(array("book_id" => $book_id), TRUE);
				$curr_detail[] 		= $info;

				$book_title = $info['title'];

				/*DATA USED FOR AUDIT TRAIL*/
				$activity	= "%s has been updated in card catalog.";
				$activity 	= sprintf($activity, $book_title);
				/*END*/


				// TO BE DISPLAYED IN USER
				$msg  = $this->lang->line('data_updated');

			}

			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);


			ISCA_Model::commit();


			$flag 	= 1;
			$class	= SUCCESS;


		}
		catch(PDOException $e) {

			ISCA_Model::rollback();
						
			$msg = $this->rlog_error($e, TRUE);
		} 
		catch(Exception $e) {

			ISCA_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}

		$info = array(
			"flag" => $flag,
			"class" => $class,
			"msg" => $msg,
		);

		echo  json_encode($info);
	}

	/*DELETE CARD CATALOG*/
	public function delete_card_catalog() {
		try {

			$flag 	= 0;
			$msg 	= "Error";

			$params 				= get_params();
			$params["security"]  	= $params["param_1"];

			$this->validate_security($params);

			$key 			= $this->get_hash_key("book_id");
			$where 			= array();
			$where[$key] 	= $params["hash_id"];

			$info = $this->book->get_specific_book($where);
			
			if(empty($info["book_id"]))
				throw new Exception($this->lang->line("err_invalid_request"));

			$book_title = $info['title'];

			ISCA_Model::beginTransaction();

			/*DATA VARIABLES FOR AUDIT TRAILS*/
			$audit_action[]		= AUDIT_DELETE;
			$audit_table[]		= ISCA_Model::tbl_arbitrator;
			$audit_schema[]		= DB_ISCA;
			$prev_detail[]		= array($info);
			/*END AUDIT TRAIL VARIABLES*/

			$this->book->delete_book($info['book_id']);

			$curr_detail[] = array();

			/*DATA USED FOR AUDIT TRAIL*/
			$activity	= "%s has been deleted in card catalog.";
			$activity 	= sprintf($activity, $book_title);
			/*END*/
			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			ISCA_Model::commit();

			$flag = 1;
			$msg = $this->lang->line("data_deleted");

		} 
		catch(PDOException $e) {

			ISCA_Model::rollback();
			
			$msg	= $e->getMessage();

		}
		catch(Exception $e) {

			ISCA_Model::rollback();
			
			$msg	= $e->getMessage();
		}

		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
		
	}


	/*MODAL*/
	public function modal($encoded_id = null, $salt = null, $token = null) {

		try {

			$data = array();
			
			$resources = array();
			$resources['load_css']	 = array(CSS_DATETIMEPICKER, 'isca', 'selectize.default');
			$resources['load_js']	 = array(JS_DATETIMEPICKER, 'selectize'); 

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('book_id');
				$where			= array();
				$where[$key]	= $hash_id;
				$info = $this->book->get_specific_book($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['book_id'] 				= 	$info['book_id'];
				$data['code_no'] 				= 	$info['code_no'];	
				$data['book_class_no'] 			= 	$info['book_class_no'];
				$data['title'] 					= 	$info['title'];
				$data["book_author"]			=	$info['book_author_id'];
				$data["publishing_house"]		=	$info['publishing_house'];	
				$data["publishing_date"]		=	$info['publishing_date'];	
				$data["copyright_date"]			=	$info['copyright_date'];	
				$data["edition"]				=	$info['edition'];	
				$data["quantity"]				=	$info['quantity'];	
				$data["content_book"]			=	$info['contents'];	


				// CREATE NEW SECURITY VARIABLES
				$book_id		= $info['book_id'];
				$hash_id 		= $this->hash($book_id);			

			} else {
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}
			$select_fields = array("A.book_author_id", "A.book_author_name");
			$authors = $this->book->get_authors($select_fields, null, null);
			$data['authors'] = $authors;

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}

		// modal view path
		$this->load->view('modals/library/card_catalog', $data);

		//load modal resources
		$this->load_resources->get_resource($resources);
	}


	/*SERVER VALIDATIONS*/
	private function _validate(&$params)
	{
		try
		{					
			$this->validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields 						= array();
			$fields['title']				= 'Book Title';
			$fields['code_no']				= 'Code No.';
			$fields['class_no']				= 'Book Class No.';
			$fields['book_author']			= 'Book Author';
			$fields['publishing_date']		= 'Publishing Date';
			$fields['content_book']			= 'Book Content';


			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_input($params)
	{
		try
		{
			$validation['title'] = array(
				'data_type' => 'string',
				'name'		=> 'Book Title',
				'max_len'	=> 100
			);
			$validation['publishing_house'] = array(
				'data_type' => 'string',
				'name'		=> 'Publisihing House',
				'max_len'	=> 100
			);
			$validation['edition'] = array(
				'data_type' => 'string',
				'name'		=> 'Edition',
				'max_len'	=> 45
			);
			$validation['quantity'] = array(
				'data_type' => 'digit',
				'name'		=> 'Number of Copies',
			);
			$validation['class_no'] = array(
				'data_type' => 'digit',
				'name'		=> 'Class No.',
			);

			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
}