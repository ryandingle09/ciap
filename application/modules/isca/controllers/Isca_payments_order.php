<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Isca_payments_order extends ISCA_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("payment_order_model", "payment_order");
	}

	public function index() {

		$data = $resources = $modal = array();

		$modal = array(
			'payments_order' 	=> array(
				'controller'	=> __CLASS__,
				'module'		=> PROJECT_ISCA,
			)
		);
		
		$resources['load_css'] 		= array(CSS_DATATABLE, 'isca');
		$resources['load_js'] 		= array(JS_DATATABLE, JS_EDITOR);
		$resources['load_modal'] 	= $modal;
		$resources['datatable'][] 	= array('table_id' => 'arbitrator_table', 'path' => PROJECT_ISCA.'/isca_payments_order/get_payments_order_list');
		

		$this->template->load('payments_order', $data, $resources);

	}

	public function get_payments_order_list() {

		try {

			$params = get_params();

			$select_fields = array("A.case_id","B.case_op_hdr_id", "B.op_date",  "C.payment_type", "A.case_no", "A.case_title", "B.case_payment_type", "B.total_amount", "B.status_code");
			$where_fields = array("B.op_date", "C.payment_type", "A.case_no", "A.case_title", "B.case_payment_type", "B.total_amount", "B.status_code");

			$case_datas = $this->payment_order->get_case_list($select_fields, $where_fields, $params);
			$total 			= $this->payment_order->total_length();
			$filtered_total = $this->payment_order->filtered_length($select_fields, $where_fields, $params);

			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> $total["cnt"],
				"iTotalDisplayRecords" 	=> $filtered_total["cnt"],
				"aaData" 				=> array()
			);

			foreach ($case_datas as $case) {

				$case_op_hdr_id 		= $case['case_op_hdr_id'];
				$hash_id				= $this->hash($case_op_hdr_id);
				$id 					= base64_url_encode($hash_id);
				$salt 					= gen_salt();
				$token 					= in_salt($id, $salt);
				$url 					= $id."/".$salt."/".$token;

				$view_action 	= 'content_form("isca_payments_order/form", "'.PROJECT_ISCA.'", "'.$url.'")';

				$action = "<div class='table-actions'>";

					//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
						$action .= "<a href='javascript:;' class='view tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' onclick='".$view_action."' ></a>";
					//endif;
				
					$action .= '</div>';

				$row 	= array();

				$row[] 	= $case['op_date'];
				$row[] 	= $case['case_payment_type'];
				$row[] 	= $case['case_no'];
				$row[] 	= $case['case_title'];
				$row[] 	= $case['payment_type'];
				$row[] 	= $case['total_amount'];
				$row[] 	= $case['status_code'];
				$row[] 	= $action;

				$output['aaData'][] = $row;

			}
			echo json_encode($output);

		}
		catch (PDOException $e) {
			
			echo $e->getMessage();
		}
		catch (Exception $e) {
			
			echo $e->getMessage();
		}
		
	}	

	public function form ($id = NULL, $salt = NULL, $token = NULL ) {

		try {
			$data		= array();
			
			$resources  			= array();
			$resources['load_css'] 	= array('selectize.default', 'isca', CSS_DATETIMEPICKER);
			$resources['load_js'] 	= array('selectize', JS_DATETIMEPICKER);

			if(!is_null($id)) {
				
				$hash_id = base64_url_decode($id);

				// CHECK IF THE SECURITY VARIABLES WERE CORRUPTED OR INTENTIONALLY EDITED BY THE USER
				check_salt($id, $salt, $token);

				$key 				= $this->get_hash_key("case_op_hdr_id");
				$where 				= array();
				$where[$key] 		= $hash_id;
				$case_op_hdr_data 	= $this->payment_order->get_payment_order_data($where);
				
				$case_id = $case_op_hdr_data['cases_case_id'];

				$data['case_op_hdr_data'] = $case_op_hdr_data;

				// get case party data
				$select_fields 			= array("A.*", "B.party_type", "B.type_clm_rpd", "B.status_flag");
				$data['case_parties'] 	=  $this->payment_order->get_case_party($select_fields, null, $case_id);

				// case data
				$data['case_details'] = $this->payment_order->get_specific_payment_order(array("case_id" => $case_id));

				// get case arbitrators data
				$select_fields				 	= array("A.arbitration_mode", "A.number_disputants");
				$data['case_arbitration_dtl'] 	=  $this->payment_order->get_case_arbitration_dtl($select_fields, null, $case_id);

				// get case claims 
				
				$data['case_claims'] 	= $this->payment_order->get_specific_case_claims($where, TRUE);

				$this->template->load('forms/view_op', $data, $resources);

			}

		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

	public function create_payment_order() {

		try {
			
			$data		= array();
			
			$resources  			= array();
			$resources['load_css'] 	= array('selectize.default', 'isca', CSS_DATETIMEPICKER);
			$resources['load_js'] 	= array('selectize','isca/payment_order', JS_DATETIMEPICKER);

			$action_type = $_SESSION['action_type'];
			
			if (is_null($action_type)) 
				throw new Exception("Invalid action. Page cannot access directly.");

			/*CREATING PAYMENT ORDER FROM EXISTING CASE*/
			if($action_type == 1) {

				$case_id = $_SESSION['case_id'];

				// validate case id sent from action validation
				if(empty($case_id))
					throw new Exception("Invalid request. Required data missing.");

				$key 			= $this->get_hash_key('case_id');
				$where 			= array();
				$where[$key] 	= $case_id;

				// get selected case data
				$case_data = $this->payment_order->get_specific_payment_order($where);
				if(empty($case_data))
					throw new Exception($this->lang->line('err_invalid_request'));

				$case_id 				= $case_data['case_id'];
				$data['case_details'] 	= $case_data;

				// get case party data
				$select_fields 			= array("A.*", "B.party_type", "B.type_clm_rpd", "B.status_flag");
				$data['case_parties'] 	=  $this->payment_order->get_case_party($select_fields, null, $case_id);


				foreach ($data['case_parties'] as $parties) {

					/*need two security variables for two composit primary keys*/

					/*party id securities*/
					$party_id 		= $parties['party_id'];

					$hash_id 		= $this->hash($party_id);
					$encoded_id 	= base64_url_encode($hash_id);
					$salt 			= gen_salt();
					$token 			= in_salt($encoded_id, $salt);

					$url 			= $encoded_id."/".$salt."/".$token;

					/*case id securities*/
					$case_hash_id 		= $this->hash($case_id);
					$case_encoded_id 	= base64_url_encode($case_hash_id);
					$case_salt 			= gen_salt();
					$case_token 		= in_salt($case_encoded_id, $case_salt);
					
					$case_url 			= $case_encoded_id."/".$case_salt."/".$case_token;

					$delete_action 		= 'content_delete("party", "'.$url.'", "'.$case_url.'")';
					
					$action = "<div class='table-actions'>";
					//if (($this->permission->check_permission(MODULE_ROLE, ACTION_DELETE))) : 	  
						$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					//endif;
					$action .= '</div>';
					
					$data['case_parties']['action'][] = $action;
				}
				
				// get case arbitrators data
				$select_fields				 	= array("A.arbitration_mode", "A.number_disputants");
				$data['case_arbitration_dtl'] 	=  $this->payment_order->get_case_arbitration_dtl($select_fields, null, $case_id);

				// get case claims 
				$select_fields		 	= array("B.claim_desc", "B.claim_type", "B.claim_amount");
				$data['case_claims'] 	= $this->payment_order->get_case_claims($select_fields, null, $case_id);
				
				$hash_id = $this->hash($case_id);

			} else {

				$hash_id = $this->hash(0);
			}
			
			$encoded_id 		= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token 				= in_salt($encoded_id, $salt);
			$data['security'] 	= $encoded_id . '/' . $salt . '/' . $token;

			$select_fields 				= array("A.payment_type_id", "A.payment_type");
			$data['payment_types'] 		= $this->payment_order->get_payment_type_params($select_fields, null, null);
			
			$select_fields 		= array("A.party_id", "A.party_name", "A.party_address", "A.party_contact_no");			
			$data['parties'] 	= $this->payment_order->get_party_list($select_fields, null, null);

			$this->template->load('forms/create_payment_order', $data, $resources);

		} 
		catch (PDOException $e) {
			echo  $e->getMessage();
		}
		catch (Exception $e) {
			echo $e->getMessage();
		}
	}

	/*PROCESS IF PAYMENT ORDER TO CREATE IS FROM EXISTING CASE OR NEW*/
	public function check_request_create()  {

		try {

			$flag		= 0;
			$msg 		= ERROR;
			$class 		= ERROR;

			$params 	= get_params();

			$this->_validate($params);

			if($params['payment_type'] == 1) {

				$key 			= $this->get_hash_key("case_id");
				$where 			= array();
				$where[$key] 	= $params['case_id'];

				$case_data 		= $this->payment_order->get_specific_payment_order($where);

				if (empty($case_data))
					throw new Exception($this->lang->line('err_invalid_request'));

				$_SESSION['case_id'] = $params['case_id'];
			}


			$_SESSION['action_type']  = $params['payment_type'];

			$flag = 1;

		}
		catch (PDOException $e) {

			$msg = $e->getMessage();

		}
		catch(Exception $e) {
			$msg = $e->getMessage();
		}
		$info = array(
			"flag" 		=> $flag,
			"msg" 		=> $msg,
			"class" 	=> $class
		);

		echo json_encode($info);
	}

	/*SAVING NEW CLAIMANT VIA AJAX
	 * TO GET THE CLAIMANT ID 
	 * FROM CREATE CALL BACK OF SELCTIZE
	**/

	public function new_claimant() {

		try {

			$flag 	= 0;
			$msg 	= ERROR;

			$data 	= get_params();
			$id 	= $this->payment_order->insert_new_claimant($data);

			if(!empty($id)) {

				$flag 	= 1;
				$msg 	= SUCCESS;
			}

		} 
		catch (PDOException $e) {

			ISCA_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		catch (Exception $e) {

			ISCA_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}

		$info = array(
			"flag" 		=> $flag,
			"class"		=> $class,
			"msg" 		=> $msg,
			"id"		=>$id
		);
		echo  json_encode($info);
	}

	/* GETTING SPECIFIC PARTY DATA
	 * TO FILL THE PARTY FIELD 
	 * (ADDRESS, CONTACT) ON SELECT
	**/
	public function party_data() {

		try {

			$params 		= get_params();

			$key 			= "party_id";
			$where 			= array();
			$where[$key] 	= $params['id'];

			$info = $this->payment_order->get_specific_party($where);

			echo json_encode($info);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e) {
			echo $e->getMessage();
		}
	}

	/* GET PARTY LIST TO FILL SELECTIZE DATAS */
	public function get_parties() {

		try {

			$select_fields 		= array("A.party_id value", "A.party_name text");		
			$data 				= array();
			$data['parties'] 	= $this->payment_order->get_party_list($select_fields, null, null);
			
			echo json_encode($data);
		}
		catch (PDOException $e) {

			echo  $e->getMessage();
		}
		catch (Exception $e) {

			echo $e->getMessage();
		}	
	}

	/* FORM VALIDATION FOR PARTIES */
	public function validate_parties() {

		try {

			$flag 		= 0;
			$msg 		= ERROR;
			$class 		= ERROR;
			$params 	= get_params();
		
			$this->_validate_parties($params);

			$flag 		= 1;
			$msg 		= SUCCESS;
			$class 		= SUCCESS;

		} catch(Exception $e) {

			$msg = $e->getMessage();
		}
		
		$info = array(
			"flag" 		=> $flag,
			"class"		=> $class,
			"msg" 		=> $msg
		);
		
		echo json_encode($info);
	}

	/* FORM VALIDATION FOR CONTACT */

	public function validate_contract() {
		
		try {

			$flag 		= 0;
			$msg 		= ERROR;
			$class 		= ERROR;

			$params = get_params();
		
			$this->_validate_contract($params);

			$flag 		= 1;
			$msg 		= SUCCESS;
			$class 		= SUCCESS;
		} 
		catch (Exception $e) {

			$msg = $e->getMessage();
		}

		$info = array(
			"flag" 		=> $flag,
			"class"		=> $class,
			"msg" 		=> $msg
		);
		
		echo json_encode($info);
	}

	/* PROCESS DATA FOR SAVING */
	public function process() {

		try {

			$flag 		= 0; 
			$msg 		= ERROR;
			$class 		= ERROR;

			$params 	= get_params();

			$this->_validate_process($params);
			
			$key 			= $this->get_hash_key('case_id');
			$where 			= array();
			$where[$key] 	= $params['hash_id'];

			$case_info	 	= $this->payment_order->get_specific_payment_order($where);
			$case_id		= $case_info['case_id'];


			ISCA_Model::beginTransaction();

			if (empty($case_id)) {

				/*DATA VARIABLES FOR AUDIT TRAILS*/
				$audit_action[]		= AUDIT_INSERT;
				$audit_table[]		= ISCA_Model::tbl_case;
				$audit_schema[]		= DB_ISCA;
				$prev_detail[]		= array();
				/*END AUDIT TRAIL VARIABLES*/

				/*SAVE DATA*/
				$params['case_payment_type'] = "New";
				$case_id = $this->payment_order->insert_payment_order($params);

				/*GET CURRENT DETAIL*/
				$info 				= $this->payment_order->get_specific_payment_order(array("case_id" => $case_id), TRUE);
				$curr_detail[] 		= $info;

				$case_data = 'Case Id: '.$info['case_id'];

				/*DATA USED FOR AUDIT TRAIL*/
				$activity	= "%s has been added in Cases.";
				$activity 	= sprintf($activity, $arbitrator_name);
				/*END*/

				// TO BE DISPLAYED IN USER
				$msg  = $this->lang->line('data_saved');


			} else {
				
				//new payment order on existing case
				
				/*DATA VARIABLES FOR AUDIT TRAILS*/
				$audit_action[]		= AUDIT_INSERT;
				$audit_table[]		= ISCA_Model::tbl_case_op_hdr;
				$audit_schema[]		= DB_ISCA;
				$prev_detail[]		= array();
				/*END AUDIT TRAIL VARIABLES*/

				$params['case_payment_type'] = "Existing";
				$case_op_hdr = $this->payment_order->insert_case_op_hdr($params, $case_id);

				$msg  = $this->lang->line('data_saved');

			}

			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			ISCA_Model::commit();

			$flag 	= 1;
			$class	= SUCCESS;

		}

		catch (PDOException $e) {

			ISCA_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		catch (Exception $e) {

			ISCA_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}

		$info = array(
			"flag" 		=> $flag,
			"class"		=> $class,
			"msg" 		=> $msg
		);
		echo  json_encode($info);
	}

	/*DELETING PARTY ON CREATING EXISTING CASE*/
	public function delete_party() {

		try {

			$flag 	= 0 ;
			$msg 	= ERROR;

			$params 				= get_params();
			$params['security'] 	= $params['param_1'];
			$params['case'] 		= $params['param_2'];

			$this->validate_security($params);

			/*2nd security validation*/
			$this->_validate_security_case($params); // prevents the hash_id from being overidden on 1st validation


			$key 		= $this->get_hash_key('party_id');
			$case_key 	= $this->get_hash_key('case_id');

			$where 				= array();
			$where[$key] 		= $params['hash_id']; // hash id = party id from 1st validation of security
			$where[$case_key] 	= $params['case_id']; //case_id is generated from 2nd validation of security

			$info = $this->payment_order->get_specific_case_party($where);

			if(empty($info))
				throw new Exception($this->lang->line('err_invalid_request'));
			
			$party_data = "party id: ".$info['party_id']."in case id: ".$info['case id'];

			ISCA_Model::beginTransaction();

			/*DATA VARIABLES FOR AUDIT TRAILS*/
			$audit_action[]		= AUDIT_UPDATE;
			$audit_table[]		= ISCA_Model::tbl_case_party;
			$audit_schema[]		= DB_ISCA;
			$prev_detail[]		= array($info);
			/*END AUDIT TRAIL VARIABLES*/	

			$this->payment_order->withdrawn_party($info);

			$curr_detail[] = array();

			/*DATA USED FOR AUDIT TRAIL*/
			$activity	= "%s has been withdrawn in case parties.";
			$activity 	= sprintf($activity, $party_data);
			/*END*/


			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			ISCA_Model::commit();

			$flag = 1;
			$msg = $this->lang->line("data_deleted");
		}
		catch (PDOException $e) {

			ISCA_Model::rollback();
			
			$msg	= $e->getMessage();
		}
		catch (Exception $e) {

			ISCA_Model::rollback();
			
			$msg	= $e->getMessage();
		}
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
	}

	/* CHECK PARTIES SPECIFIC FOR CREATING ON EXISTING CASE VALIDATE IF NEW OR DELETED ENTRY */
	public function check_parities() {

		try {

			$flag 		= 0; 
			$msg 		= ERROR;
			$class 		= ERROR;

			$params = get_params();

			$this->validate_security($params);

			$key 			= $this->get_hash_key("case_id");
			$where 			= array();
			$where[$key] 	= $params['hash_id'];

			$data = $this->payment_order->get_specific_case_party($where, TRUE);

			/**
			* IF NOT EMPTY DATA MEANS REQUEST
			* CREATING FROM EXISTING CASE
			* CHECK IF NEW PARTY IS ADDED
			* IF NEW DATA ADDED SAVED DATA
			**/
			if(!empty($data)) {
				
				
				ISCA_Model::beginTransaction();

				foreach ($data as $party) {
					$case_id = $party['case_id'];
					$clm_ctr = 0;
					$rpn = 0;
					
					$where = array();
					$where['case_id'] = $party['case_id'];

					foreach ($params['claimant_name'] as $claimant_id) {

						// if claimant exist check flag status
						// if flag status = D (deleted/withdrawn) re-update data
						if($party['party_id'] == $claimant_id && $party['status_flag'] == "D") {

							$val 				= array("party_type" => "1", "status_flag" => "A");
							$where['party_id'] 	= $claimant_id;

							$this->payment_order->update_case_party($val, $where);
						}

						$where['party_id'] 	= $claimant_id;

						$check_party_claimant = $this->payment_order->get_specific_case_party($where);

						if(empty($check_party_claimant)) {

							$data 							= array();
							$data['claimant_name'][] 		= $claimant_id;
							$data['claimant_address'][] 	= $params['claimant_address'][$clm_ctr];
							$data['claimant_contact'][] 	= $params['claimant_contact'][$clm_ctr];
							$data['status_flag'][] 			= "A";

							$this->payment_order->insert_parties_claimant($data, $party['case_id']);
						}

						$clm_ctr+=1;

					}

					foreach ($params['respondent_name'] as $respondent_id) {

						// if respondent exist check flag status
						// if flag status = D (deleted/withdrawn) re-update data
						if($party['party_id'] == $respondent_id && $party['status_flag'] == "D") {

							$val 				= array("party_type" => "2", "status_flag" => "A");
							$where['party_id'] 	= $respondent_id;
							
							$this->payment_order->update_case_party($val, $where);

						}

						$where['party_id'] = $respondent_id;
						
						$check_party_respondent = $this->payment_order->get_specific_case_party($where);

						if(empty($check_party_respondent)) {

							$data 							= array();
							$data['respondent_name'][] 		= $respondent_id;
							$data['respondent_address'][] 	= $params['respondent_address'][$rpn];
							$data['respondent_contact'][] 	= $params['respondent_contact'][$rpn];
							$data['status_flag'][] 			= "A";

							$this->payment_order->insert_parties_respondent($data, $party['case_id']);
						}

						$rpn+=1;	
					}

				}

				$select_fields 			= array("A.party_name", "B.party_type", "B.status_flag");
				$data = $this->payment_order->get_case_party_data($select_fields, NULL, $case_id);
				
				ISCA_Model::commit();
			}
			
			// re-set data for possible new entries
			$flag = 1;
			$msg = SUCCESS;
			$class = SUCCESS;
		}
		catch (PDOException $e) {

			ISCA_Model::rollback();
			
			$msg	= $e->getMessage();
		}
		catch (Exception $e) {

			ISCA_Model::rollback();
			
			$msg	= $e->getMessage();
		}
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg,
			"class" => $class,
			"data" 	=> $data
		);

		echo json_encode($info);
	}

	/*MODAL*/
	public function modal($encoded_id = null, $salt = null, $token = null) {

		try {

			$data = array();

			$resources 				= array();

			
			$resources['load_css'] 	= array('jquery-labelauty', 'isca', 'selectize.default');
			$resources['load_js'] 	= array('jquery-labelauty', 'selectize');

			$select_fields 		= array("A.case_id", "A.case_title");
			$cases 				= $this->payment_order->get_cases_data($select_fields, null, null);


			foreach ($cases as $case) {

				$row = array();

				$hash_id 			= $this->hash($case['case_id']);
				
				$encoded_id			= base64_url_encode($hash_id);
				$salt 				= gen_salt();
				$token				= in_salt($encoded_id, $salt);

				$row['case_id']		= $encoded_id . '/' . $salt . '/' . $token;
				$row['case_title']	= $case['case_title'];

				$data['cases'][] = $row;
			}

			$hash_id 	= $this->hash(0);				

			$encoded_id				= base64_url_encode($hash_id);
			$salt 					= gen_salt();
			$token					= in_salt($encoded_id, $salt);
			$data['security']		= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}

		// modal view path
		$this->load->view('modals/payments_order', $data);

		// load modal resources
		$this->load_resources->get_resource($resources);
	}
	
	/*VALIDATE PARTIES INPUTS*/
	private function _validate_parties(&$params) {
		
		try {

			$fields = array();
			$fields['claimant_name'] 		= "Claimant Name";
			$fields['claimant_address'] 	= "Claimant Address";
			$fields['claimant_contact'] 	= "Claimant Contact";

			$fields['respondent_name'] 		= "Respondent Name";
			$fields['respondent_address'] 	= "Respondent Address";
			$fields['respondent_contact'] 	= "Respondent Contact";

			$this->check_required_fields($params, $fields);
			
	
			return $this->_validate_input_parties($params);

		}
	 	catch (Exception $e) {
			throw $e;		
		}		
	}

	private function _validate_input_parties($params)
	{
		try
		{
			//declare all input data validations
			$validation['claimant_name'] = array(
				'data_type' 	=> 'string',
				'name'			=> 'Claimant Name',
				'max_len'		=> 100
			);
			
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	/*END OF PARTIES VALIDATION*/

	/*VALIDATE CONTRACT INPUTS*/
	private function _validate_contract(&$params) {

		try {
			
			$fields = array();
			// VALIDATE INPUTS AS ARBITRATION
			if (isset($params['mode_of_settlement']) && $params['mode_of_settlement'] == 'A') {

				$fields['mode_of_arbitration'] 					= "Arbitration Mode";
				/*$fields['number_of_disputants'] 				= "Number of Disputants";*/
				$fields['arbitration_contract_description'] 	= "Contract Description";
				$fields['arbitration_contract_amount'] 			= "Contract Amount";
				$fields['type_claim'] 							= "Type of Claim";

 			} else if(isset($params['mode_of_settlement']) && $params['mode_of_settlement'] == 'M') {

 			//VALIDATE INPUTS AS MEDIATION
				$fields['mediation_contract_description'] 		= "Contract Description";
				$fields['mediation_contract_amount'] 			= "Contract Amount";

 			}

 			$this->check_required_fields($params, $fields);
 			
 			
 			return $this->_validate_input_parties($params);

		} catch (Exception $e) {

			throw $e;

		}
	}
	/*END OF CONTRACT VALIDATION*/

	/*VALIDATE PROCESS INPUTS*/
	private function _validate_process(&$params) {
		try
		{					
			$this->validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields 					= array();

			$fields['date_created']		= 'Date';
			$fields['payment_type'] 	= "Type of Payment";

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input_process($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function _validate_input_process($params)
	{
		try
		{
			$validation['payment_type'] = array(
				'data_type' 	=> 'string',
				'name'			=> 'Payment type',
				'max_len'		=> 100
			);
			
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	/*END OF PROCESS VALIDATIONS*/

	private function _validate(&$params)
	{
		try
		{					
			$this->validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields 					= array();
			if(isset($params['payment_type']) && $params['payment_type'] == 1) {

				$fields['case']	= 'Case';
			}

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_input(&$params)
	{
		try
		{
			$validation['case'] = array(
				'data_type' 	=> 'string',
				'name'			=> 'Case'
			);
			if($params['payment_type'] == 1) {

				$this->_validate_security_case($params);
			}
			
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function _validate_security_case(&$params)
	{
		try
		{
			
			if(EMPTY($params['case']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
	
			$case = explode('/', $params['case']);
	
			$params['case_encoded_id']		= $case[0];
			$params['case_salt']			= $case[1];
			$params['case_token']			= $case[2];
	
			check_salt($params['case_encoded_id'], $params['case_salt'], $params['case_token']);
	
			$params['case_id']		= base64_url_decode($params['case_encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

}
