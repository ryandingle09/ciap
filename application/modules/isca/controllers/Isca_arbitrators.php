<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Isca_arbitrators extends ISCA_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("arbitrator_model", "arbitrator");
	}

	public function index() {

		$data = $resources = $modal = array();

		$modal = array(
			'modal_arbitrator' 	=> array(
				'controller'	=> __CLASS__,
				'module'		=> PROJECT_ISCA,
				'height'		=> '450px'
			)
		);
		
		$resources['load_css'] 		= array(CSS_DATATABLE);
		$resources['load_js'] 		= array(JS_DATATABLE);
		$resources['load_modal'] 	= $modal;
		$resources['datatable'][] 	= array('table_id' => 'arbitrator_table', 'path' => PROJECT_ISCA.'/isca_arbitrators/get_arbitrator_list');
		
		$this->template->load('arbitrators', $data, $resources);
	}
	
	/*GET ALL ARBITRATOR LIST*/
	public function get_arbitrator_list() {

		try {

			$params			= get_params();

			$select_fields 	= array("A.id", "CONCAT(A.first_name, ' ', A.last_name, ' ', A.middle_initial) as Name", "A.area", "A.profession", "A.sole", "A.status");

			$where_fields 	= array("CONCAT(A.first_name, ' ', A.last_name, ' ', A.middle_initial)", "area", "profession", "sole", "status");

			$arbitrators 	= $this->arbitrator->get_arbitrator_list($select_fields, $where_fields, $params);
			$total 			= $this->arbitrator->total_length();
			$filtered_total = $this->arbitrator->filtered_length($select_fields, $where_fields, $params);

			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> $total["cnt"],
				"iTotalDisplayRecords" 	=> $filtered_total["cnt"],
				"aaData" 				=> array()
			);


			foreach ($arbitrators as $data):

				$arbitrator_id 	= $data['id'];

				$hash_id 		= $this->hash($arbitrator_id);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("arbitrator", "'.$url.'")';

				$action = "<div class='table-actions'>";

					//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
						$action .= "<a href='javascript:;' class='md-trigger tooltipped edit' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_arbitrator' onclick=\"modal_init('".$url."')\"></a>";
					//endif;

					//if (($this->permission->check_permission(MODULE_ROLE, ACTION_DELETE))) : 	  
						$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					//endif;
				
					$action .= '</div>';

				$row 		=  array();
			 	$row[] 		=  $data['Name'];
			 	$row[] 		=  $data['area'];
			 	$row[] 		=  $data['profession'];
			 	$row[] 		=  $data['sole'];
			 	$row[] 		=  $data['status'];
			 	$row[] 		=  $action;

				$output['aaData'][] = $row;

			endforeach;

		 	echo json_encode($output);

	 	} catch(PDOException $e) {

			echo $e->getMessage();
		}

		catch (Exception $e) {

			echo $e->getMessage();
		}
		
	}	
	
	/*VIEW ARBITRABTOR DATA*/
	public function view_arbitrator() {

		try {

			$data = $resources  = array();

			$resources['load_css'] 		= array(CSS_DATATABLE);
			$resources['load_js'] 		= array(JS_DATATABLE);

			$this->template->load('arbitrator_view', $data, $resources);

		} catch(PDOException $e) {

			echo $e->getMessage();

		}
		catch(Exception $e){

			echo $e->getMessage();

		}
	}

	/*GET SPECIFIC ARBITRATOR DETAILS*/
	public function get_arbitrator_details() {


		$data = $resources  = array();

		$output = array(
			"sEcho" => 1,
			"iTotalRecords" => "1",
			"iTotalDisplayRecords" => "1",
			"aaData" => array()
		);
		
	 	$data[] =  'Sample Data';
	 	$data[] =  'Sample Data';
	 	$data[] =  'Sample Data';
	 	$data[] =  'Sample Data';
	 	$data[] =  'Sample Data';
	 	$data[] =  'Sample Data';
	 	$data[] =  'Sample Data';

		$output['aaData'][] = $data;

	 	echo json_encode($output);
	}

	/*GET ARBITRATORS MACE PTS*/
	public function get_arbitrator_mace_pts() {

		$data = $resources  = array();

		$output = array(
			"sEcho" => 1,
			"iTotalRecords" => "1",
			"iTotalDisplayRecords" => "1",
			"aaData" => array()
		);
		
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';

		$output['aaData'][] = $data;

	 	echo json_encode($output);
	}

	
	/*PROCESS FUNTIION INSERT/EDIT*/
	public function process() {
		try {

			$flag	 	= 0;
			$class 		= ERROR;

			$params 	= get_params();

			//SERVER VALIDATE FIELDS
			$this->_validate($params);

			$key 			= $this->get_hash_key("id");
			$where 			= array();
			$where[$key] 	= $params["hash_id"];

			$current_info 	= $this->arbitrator->get_specific_arbitrator($where);
			$arbitrator_id	= $current_info['id'];

			ISCA_Model::beginTransaction();

			/*IF EMPTY ID MEANS SAVE/CREATING DATA*/
			if(empty($arbitrator_id)) {

				/*DATA VARIABLES FOR AUDIT TRAILS*/
				$audit_action[]		= AUDIT_INSERT;
				$audit_table[]		= ISCA_Model::tbl_arbitrator;
				$audit_schema[]		= DB_ISCA;
				$prev_detail[]		= array();
				/*END AUDIT TRAIL VARIABLES*/

				/*SAVE DATA*/
				$arbitrator_id = $this->arbitrator->insert_arbitrator($params);

				/*GET CURRENT DETAIL*/
				$info 				= $this->arbitrator->get_specific_arbitrator(array("id" => $arbitrator_id), TRUE);
				$curr_detail[] 		= $info;


				$arbitrator_name = $info['first_name'].' '.$info['middle_initial'].' '.$info['last_name'];


				/*DATA USED FOR AUDIT TRAIL*/
				$activity	= "%s has been added in arbitrator.";
				$activity 	= sprintf($activity, $arbitrator_name);
				/*END*/

				// TO BE DISPLAYED IN USER
				$msg  = $this->lang->line('data_saved');

			} else {

				
				/*DATA VARIABLES FOR AUDIT TRAILS*/
				$audit_action[]		= AUDIT_UPDATE;
				$audit_table[]		= ISCA_Model::tbl_arbitrator;
				$audit_schema[]		= DB_ISCA;
				$prev_detail[]		= array($current_info);
				/*END AUDIT TRAIL VARIABLES*/

				/* UPDATE DATA*/
				$params['id']	= $arbitrator_id;
				$this->arbitrator->update_arbitrator($params);

				/*GET CURRENT DETAIL*/
				$info 				= $this->arbitrator->get_specific_arbitrator(array("id" => $arbitrator_id), TRUE);
				$curr_detail[] 		= $info;


				$arbitrator_name = $info['first_name'].' '.$info['middle_initial'].' '.$info['last_name'];

				/*DATA USED FOR AUDIT TRAIL*/
				$activity	= "%s has been updated in arbitrator.";
				$activity 	= sprintf($activity, $arbitrator_name);
				/*END*/


				// TO BE DISPLAYED IN USER
				$msg  = $this->lang->line('data_updated');

			}

			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			ISCA_Model::commit();

			$flag 	= 1;
			$class	= SUCCESS;
			
		} 

		catch (PDOException $e) {

			ISCA_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		catch (Exception $e) {

			ISCA_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}

		$info = array(
			"flag" 	=> $flag,
			"class"	=> $class,
			"msg" 	=> $msg
		);
	
		
		echo json_encode($info);
	}

	/*MODAL*/
	public function modal($encoded_id = null, $salt = null, $token = null) {

		try {

			$data = array();

			$resources = array();
			$resources['load_css'] 		= array(CSS_DATETIMEPICKER, 'jquery-labelauty', 'isca', 'selectize.default');
			$resources['load_js'] 		= array(JS_DATETIMEPICKER, 'jquery-labelauty', 'popModal.min', 'selectize');

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->arbitrator->get_specific_arbitrator($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['id'] 					= $info['id'];
				$data['first_name'] 			= $info['first_name'];
				$data['last_name'] 				= $info['last_name'];
				$data['middle_initial'] 		= $info['middle_initial'];
				$data['created_by'] 			= $info['created_by'];	
				$data['created_date'] 			= $info['created_date'];
				$data["readonly_value"]			= "readonly";	

				// CREATE NEW SECURITY VARIABLES
				$arbitrator_id	= $info['id'];
				$hash_id 		= $this->hash($arbitrator_id);			

			} else {
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}

		// modal view path
		$this->load->view('modals/arbitrator', $data);

		//load resources for modal
		$this->load_resources->get_resource($resources);
	}


	/*DELETE ARBITRATOR*/
	public function delete_arbitrator() {

		try {  


			$flag = 0; 
			$msg = "Error";

			$params = get_params();
			$params['security'] = $params['param_1'];

			$this->validate_security($params);

			$key = $this->get_hash_key("id");
			$where = array();
			$where[$key] = $params['hash_id'];

			$info = $this->arbitrator->get_specific_arbitrator($where);
			if (empty($info['id'])) {
				throw new Exception($this->lang->line('err_invalid_request'));
			}

			$arbitrator_name = $info['first_name'].' '.$info['middle_initial'].' '.$info['last_name'];

			ISCA_Model::beginTransaction();

			/*DATA VARIABLES FOR AUDIT TRAILS*/
			$audit_action[]		= AUDIT_DELETE;
			$audit_table[]		= ISCA_Model::tbl_arbitrator;
			$audit_schema[]		= DB_ISCA;
			$prev_detail[]		= array($info);
			/*END AUDIT TRAIL VARIABLES*/

			$this->arbitrator->delete_arbitrator($info['id']);

			$curr_detail[] = array();

			/*DATA USED FOR AUDIT TRAIL*/
			$activity	= "%s has been deleted in arbitrator.";
			$activity 	= sprintf($activity, $arbitrator_name);
			/*END*/


			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			

			ISCA_Model::commit();


			$flag = 1;
			$msg = $this->lang->line("data_deleted");

			
		} 
		catch (PDOException $e) {

			ISCA_Model::rollback();
			
			$msg	= $e->getMessage();
		}
		catch (Exception $e) {

			ISCA_Model::rollback();
			
			$msg	= $e->getMessage();
		}
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
		
	}

	/*SERVER VALIDATIONS*/
	private function _validate(&$params)
	{
		try
		{					
			$this->validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields 					= array();
			$fields['first_name']	= 'First name';

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}
	private function _validate_input($params)
	{
		try
		{
			$validation['first_name'] = array(
				'data_type' => 'string',
				'name'		=> 'First Name',
				'max_len'	=> 100
			);
			
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
}