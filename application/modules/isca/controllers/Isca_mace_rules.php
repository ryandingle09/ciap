<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Isca_mace_rules extends ISCA_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("code_library_model", "code_library");
	}

	public function index() {

		$data = $resources = $modal = array();
		
		$modal = array(
			'modal_mace_rule' 	=> array(
				'controller'	=> __CLASS__,
				'module'		=> PROJECT_ISCA,
			)
		);

		$resources['load_css'] 		= array(CSS_DATATABLE);
		$resources['load_js'] 		= array(JS_DATATABLE, JS_EDITOR);
		$resources['load_modal'] 	= $modal;
		$resources['datatable'][] 	= array('table_id' => 'mace_rule_table', 'path' => PROJECT_ISCA.'/isca_mace_rules/get_mace_rules_list');
		
		$this->template->load('code_libraries/mace_rule', $data, $resources);

	}

	public function get_mace_rules_list() {

		try {

			$params 			= get_params();

			$select_fields 		= array("A.mace_rule_id", "A.mace_credit_unit_required", "A.mace_credit_unit_period", "A.created_by", "A.created_date", "A.status");
			$where_fields 		= array("A.mace_credit_unit_required", "A.mace_credit_unit_period", "A.created_by", "A.created_date", "A.status");

			$case_activities 		= $this->code_library->get_code_list($select_fields, $where_fields, $params, ISCA_MODEL::tbl_mace_rule, "A.mace_rule_id");
			$total 				= $this->code_library->total_length(ISCA_MODEL::tbl_mace_rule, "mace_rule_id");
			$filtered_total 	= $this->code_library->filtered_length($select_fields, $where_fields, $params , ISCA_MODEL::tbl_mace_rule, "A.mace_rule_id");


			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> $total["cnt"],
				"iTotalDisplayRecords" 	=> $filtered_total["cnt"],
				"aaData" 				=> array()
			);

			foreach ($case_activities as $data):

				$mace_rule_id 	= $data['mace_rule_id'];

				$hash_id 		= $this->hash($mace_rule_id);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("Mace Rule", "'.$url.'")';

				$action = "<div class='table-actions'>";

					//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
						$action .= "<a href='javascript:;' class='md-trigger tooltipped edit' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_mace_rule' onclick=\"modal_init('".$url."')\"></a>";
					//endif;

					//if (($this->permission->check_permission(MODULE_ROLE, ACTION_DELETE))) : 	  
						$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					//endif;
				
					$action .= '</div>';

				$row 		=  array();
			 	$row[] 		=  $data['mace_credit_unit_required'];
			 	$row[] 		=  $data['mace_credit_unit_period'];
			 	$row[] 		=  $data['created_by'];
			 	$row[] 		=  $data['created_date'];
			 	$row[] 		=  ($data['status'] == 1) ? "Active" : "In-active";
			 	$row[] 		=  $action;

				$output['aaData'][] = $row;

			endforeach;

		 	echo json_encode($output);

		}
		catch(PDOException $e) {
			echo $e->getMessage();
		}
		catch (Exception $e) {
			echo $e->getMessage();
		}
	}	

	/*MODAL*/
	public function modal($encoded_id = null, $salt = null, $token = null) {

		try {

			$data = array();

			$resources = array();
			$resources['load_css'] 		= array(CSS_DATETIMEPICKER, 'jquery-labelauty', 'isca', 'selectize.default');
			$resources['load_js'] 		= array(JS_DATETIMEPICKER, 'jquery-labelauty','selectize');

			// for update
			if(!EMPTY($encoded_id)){
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('mace_rule_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->code_library->get_specific_code($where, ISCA_MODEL::tbl_mace_rule);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['mace_rule_id'] 						= $info['mace_rule_id'];
				$data['mace_credit_unit_required'] 			= $info['mace_credit_unit_required'];
				$data['mace_credit_unit_period'] 			= $info['mace_credit_unit_period'];
				$data['status'] 							= $info['status'];
				
				// CREATE NEW SECURITY VARIABLES
				$mace_rule_id	= $info['mace_rule_id'];
				$hash_id 		= $this->hash($mace_rule_id);			

			} else {
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}

		// modal view path
		$this->load->view('modals/code_libraries/mace_rule',$data);

		//load resources for modal
		$this->load_resources->get_resource($resources);
	}

	public function process () {

		try {

			$flag = 0;
			$class = ERROR;

			$params = get_params();

			$this->_validate($params);

			$key 			= $this->get_hash_key("mace_rule_id");
			$where 			= array();
			$where[$key] 	= $params["hash_id"];

			$current_info 	= $this->code_library->get_specific_code($where, ISCA_MODEL::tbl_mace_rule);

			$mace_rule_id = $current_info['mace_rule_id'];

			ISCA_Model::beginTransaction();

			/*INITIATE DATA TO SAVE*/
			$val 								= array();
			$val['mace_credit_unit_required'] 	= filter_var($params['mace_credit_unit_required'], FILTER_SANITIZE_STRING);
			$val['mace_credit_unit_period'] 	= filter_var($params['mace_credit_unit_period'], FILTER_SANITIZE_STRING);
			$val['status']						= isset($params['rule_status'])  ?  $params['rule_status'] : 0;

			// FOR SAVING DATA
			if (empty($mace_rule_id)) {

				/*DATA VARIABLES FOR AUDIT TRAILS*/
				$audit_action[]		= AUDIT_INSERT;
				$audit_table[]		= ISCA_Model::tbl_mace_rule;
				$audit_schema[]		= DB_ISCA;
				$prev_detail[]		= array();
				/*END AUDIT TRAIL VARIABLES*/

				/*additional data if action is saving*/	
				$val['created_by']		= "Admin"; 
				$val['created_date']	= time();

				$mace_rule_id 		= $this->code_library->insert_code($val, ISCA_Model::tbl_mace_rule);

				$info 				= $this->code_library->get_specific_code(array("mace_rule_id" => $mace_rule_id), ISCA_MODEL::tbl_mace_rule);
				$curr_detail[] 		= $info;

				$mace_credit_unit_required = $info['mace_credit_unit_required'];

				/*DATA USED FOR AUDIT TRAIL*/
				$activity	= "%s has been added in param mace rule.";
				$activity 	= sprintf($activity, $mace_credit_unit_required);
				/*END*/

				// TO BE DISPLAYED IN USER
				$msg  = $this->lang->line('data_saved');

			} else {

				/*DATA VARIABLES FOR AUDIT TRAILS*/
				$audit_action[]		= AUDIT_UPDATE;
				$audit_table[]		= ISCA_Model::tbl_mace_rule;
				$audit_schema[]		= DB_ISCA;
				$prev_detail[]		= array($current_info);
				/*END AUDIT TRAIL VARIABLES*/

				$where 						= array();
				$where['mace_rule_id'] 	= $mace_rule_id;

				$this->code_library->update_code(ISCA_Model::tbl_mace_rule, $val, $where);

				$info				 = $this->code_library->get_specific_code(array("mace_rule_id" => $mace_rule_id), ISCA_MODEL::tbl_mace_rule);
				$curr_detail[] 		 = $info;

				$mace_credit_unit_required = $info['mace_credit_unit_required'];

				/*DATA USED FOR AUDIT TRAIL*/
				$activity	= "%s has been updated in param mace rule.";
				$activity 	= sprintf($activity, $mace_credit_unit_required);
				/*END*/

				// TO BE DISPLAYED IN USER
				$msg  = $this->lang->line('data_updated');

			}

			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			ISCA_Model::commit();

			$flag 	= 1;
			$class	= SUCCESS;

		}
		catch (PDOException $e) {

			ISCA_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		catch (Exception $e) {

			ISCA_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}

		$info = array(
			"flag" 	=> $flag,
			"class"	=> $class,
			"msg" 	=> $msg
		);
	
		
		echo json_encode($info);
	}
	public function delete_code() {

		try {
			$flag = 0;
			$class= ERROR;

			$params = get_params();
			$params['security'] = $params['param_1'];

			$this->validate_security($params);

			$key 			= $this->get_hash_key("mace_rule_id");
			$where 			= array();
			$where[$key] 	= $params["hash_id"];

			$info = $this->code_library->get_specific_code($where, ISCA_MODEL::tbl_mace_rule);
			
			if(empty($info['mace_rule_id']))
				throw new Exception($this->lang->line('err_invalid_request'));

			$mace_credit_unit_required = $info['mace_credit_unit_required'];

			ISCA_Model::beginTransaction();

			/*DATA VARIABLES FOR AUDIT TRAILS*/
			$audit_action[]		= AUDIT_DELETE;
			$audit_table[]		= ISCA_Model::tbl_mace_rule;
			$audit_schema[]		= DB_ISCA;
			$prev_detail[]		= array($info);
			/*END AUDIT TRAIL VARIABLES*/

			$where 					= array();
			$where['mace_rule_id'] 	= $info['mace_rule_id'];

			$this->code_library->delete_code(ISCA_Model::tbl_mace_rule, $where);

			$curr_detail[] = array();

			/*DATA USED FOR AUDIT TRAIL*/
			$activity	= "%s has been deleted in param mace rule.";
			$activity 	= sprintf($activity, $mace_credit_unit_required);
			/*END*/


			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			

			ISCA_Model::commit();


			$flag = 1;
			$msg = $this->lang->line("data_deleted");
				

		}
		catch (PDOException $e) {

			ISCA_Model::rollback();
			
			$msg	= $e->getMessage();
		}
		catch (Exception $e) {

			ISCA_Model::rollback();
			
			$msg	= $e->getMessage();
		}
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
	}
	private function _validate(&$params)
	{
		try
		{					
			$this->validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields 								= array();
			$fields['mace_credit_unit_required']	= 'Mace Credit Unit Required';
			$fields['mace_credit_unit_period']		= 'Mace Credit Unit Period';

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}
	private function _validate_input($params)
	{
		try
		{
			$validation['mace_credit_unit_period'] = array(
				'data_type' => 'digit',
				'name'		=> 'Mace Credit Unit Period',
			);
			$validation['mace_credit_unit_required'] = array(
				'data_type' => 'digit',
				'name'		=> 'Mace Credit Unit Required',
			);
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

}
