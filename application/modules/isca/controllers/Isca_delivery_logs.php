<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Isca_delivery_logs extends ISCA_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {

		//point to catalog list as default
		$this->view_agenda();

	}

	public function view_agenda() {

		$data = $resources = $modal = array();

		$modal = array(
			'delivery_log' 	=> array(
				'controller'	=> __CLASS__,
				'module'		=> PROJECT_ISCA,
				'height'		=> '500px',
			)
		);
		
		$resources['load_css'] 		= array(CSS_DATATABLE);
		$resources['load_js'] 		= array(JS_DATATABLE, JS_EDITOR);
		$resources['load_modal'] 	= $modal;
		$resources['datatable'][] 	= array('table_id' => 'arbitrator_table', 'path' => PROJECT_ISCA.'/isca_delivery_logs/get_delivery_log_list');
		
		$this->template->load('library/delivery_log', $data, $resources);
	}


	/*GET ALL CATALOG LIST*/
	public function get_delivery_log_list() {

		$data = $resources = $modal = array();

		$output = array(
			"sEcho" => 1,
			"iTotalRecords" => "1",
			"iTotalDisplayRecords" => "1",
			"aaData" => array()
		);
		
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[]	 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';

		$output['aaData'][] = $data;

	 	echo json_encode($output);
	}	



	/*MODAL*/
	public function modal($encoded_id = null, $salt = null, $token = null) {

		try {

			$data = array();

			$resources 				= array();
			$resources['load_css'] 	= array(CSS_DATETIMEPICKER, 'isca', 'selectize.default');
			$resources['load_js'] 	= array(JS_DATETIMEPICKER,'selectize');

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('position_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->positions->get_specific_position($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['position_id'] 			= $info['position_id'];
				$data['position_name'] 			= $info['position_name'];
				$data['created_by'] 			= $info['created_by'];	
				$data['created_date'] 			= $info['created_date'];
				$data["readonly_value"]			= "readonly";	

				// CREATE NEW SECURITY VARIABLES
				$position_id	= $info['position_id'];
				$hash_id 		= $this->hash($position_id);			

			} else {
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}

		// modal view path
		$this->load->view('modals/library/delivery_log',$data);

		// load modal resources
		$this->load_resources->get_resource($resources);
	}
}
