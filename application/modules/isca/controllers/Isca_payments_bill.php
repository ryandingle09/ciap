<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Isca_payments_bill extends ISCA_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {

		$data = $resources = $modal = array();
		
		$resources['load_css'] 		= array(CSS_DATATABLE);
		$resources['load_js'] 		= array(JS_DATATABLE, JS_EDITOR);
		$resources['datatable'][] 	= array('table_id' => 'arbitrator_table', 'path' => PROJECT_ISCA.'/isca_payments_bill/get_payments_bill_list');
		
		$this->template->load('payments_bill', $data, $resources);

	}


	/*GET ALL CATALOG LIST*/
	public function get_payments_bill_list() {

		$data = $resources = $modal = array();

		$output = array(
			"sEcho" => 1,
			"iTotalRecords" => "1",
			"iTotalDisplayRecords" => "1",
			"aaData" => array()
		);

		$url = "";
		$delete_action = "";

		$action = "<div class='table-actions'>";

			$action .= "<a href='javascript:;' class='view tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' onclick=\"modal_init('".$url."')\"></a>";
			//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
				$action .= "<a href='javascript:;' class='tooltipped edit' data-tooltip='Edit' data-position='bottom' data-delay='50' onclick=\"modal_init('".$url."')\"></a>";
			//endif;

			//if (($this->permission->check_permission(MODULE_ROLE, ACTION_DELETE))) : 	  
				$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
			//endif;
		
		$action .= '</div>';
		
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[]	 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  $action;

		$output['aaData'][] = $data;

	 	echo json_encode($output);
	}	

	public function create_payment_bill() {

		$data		= array();
		$resources 	= array();

		$resources['load_css'] 	= array('selectize.default', 'isca', CSS_DATETIMEPICKER);
		$resources['load_js'] 	= array('selectize', JS_DATETIMEPICKER);

		$this->template->load('forms/create_payment_bill', $data, $resources);

	}

}
