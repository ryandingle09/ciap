<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Isca_board_meetings extends ISCA_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("meeting_model", "meeting");
	}

	public function index() {

		$data = $resources = $modal = array();

		$modal = array(
			'board_meeting' 	=> array(
				'controller'	=> __CLASS__,
				'module'		=> PROJECT_ISCA,
				'height'		=> '500px',

			)
		);
		
		$resources['load_css'] 		= array(CSS_DATATABLE);
		$resources['load_js'] 		= array(JS_DATATABLE, JS_EDITOR);
		$resources['load_modal'] 	= $modal;
		$resources['datatable'][] 	= array('table_id' => 'meeting_agenda', 'path' => PROJECT_ISCA.'/isca_board_meetings/get_agenda_list');
		
		$this->template->load('library/agenda', $data, $resources);

	}


	/*GET ALL CATALOG LIST*/
	public function get_agenda_list() {

		try {

			$params = get_params();

			$select_fields 		= array("A.meeting_id", "A.meeting_date", "A.meeting_name", "A.venue");
			$where_fields 		= array("meeting_date", "meeting_name", "venue");

			$meetings		 	=  $this->meeting->get_meeting_list($select_fields, $where_fields, $params);
			$tota				= $this->meeting->total_length();
			$filtered_total 	= $this->meeting->filtered_length($select_fields, $where_fields, $params);

			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> $total["cnt"],
				"iTotalDisplayRecords" 	=> $filtered_total["cnt"],
				"aaData" 				=> array()
			);


			foreach($meetings as $data) :

				/*PRIMARY KEY*/
				$meeting_id = $data['meeting_id'];

				/*CREATING SECURITY VARIABLES*/
				$hash_id 		= $this->hash($meeting_id);
				$encoded_id		= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);
				$url 			= $encoded_id.'/'.$salt.'/'.$token;

				$delete_action = '(content_delete("Meeting", "'.$url.'"))';

				$action = "<div class='table-actions'>";

					$action .= "<a href='javascript:;' class='view tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' onclick=\"modal_init('".$url."')\"></a>";
					$action .="<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='EDIT' dat-position='bottom' data-delay='50' data-modal='board_meeting' onclick=\"modal_init('".$url."')\"> </a>";
					$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";


				$action .="</div>";

				$row = array();

				$row[] = $data['meeting_date'];
				$row[] = $data['meeting_name'];
				$row[] = $data['venue'];
				$row[] = $action;

				$output['aaData'][] = $row;

			endforeach;

			echo json_encode($output);
		}
		catch (PDOException $e) {
			echo $e->getMessage();
		}
		catch (Exception $e) {
			echo $e->getMessage();
		}
	}


	/*UPDATE OR CREATE MEETING*/
	public function process() {

		try {

			$flag = 0;
			$msg = 'Error';

			$params = get_params();

			/*SERVER VALIDATE*/
			$this->_validate($params);

			$key 			= $this->get_hash_key('meeting_id');
			$where 			= array();
			$where[$key] 	= $params['hash_id'];

			$current_info 	= $this->meeting->get_specific_meeting($where);
			$meeting_id 	= $current_info['meeting_id'];


			ISCA_Model::beginTransaction();

			if(empty($meeting_id)) {

				/*DATA VARIABLES FOR AUDIT TRAILS*/
				$audit_action[]		= AUDIT_INSERT;
				$audit_table[]		= ISCA_Model::tbl_meeting;
				$audit_schema[]		= DB_ISCA;
				$prev_detail[]		= array();
				/*END AUDIT TRAIL VARIABLES*/

				$meeting_id = $this->meeting->insert_meeting($params);

				/*GET CURRENT DATA*/
				$info 				= $this->meeting->get_specific_meeting(array("meeting_id" => $meeting_id) , TRUE);
				$curr_detail[]  	= $info;

				$meeing_name = $curr_detail['meeting_name'];

				/*DATA USED FOR AUDIT TRAIL*/
				$activity	= "%s has been added in card catalog.";
				$activity 	= sprintf($activity, $meeing_name);
				/*END*/

				$msg  = $this->lang->line("data_saved");


			} else {

				/*DATA VARIABLES FOR AUDIT TRAILS*/
				$audit_action[]		= AUDIT_UPDATE;
				$audit_table[]		= ISCA_Model::tbl_meeting;
				$audit_schema[]		= DB_ISCA;
				$prev_detail[]		= array($current_info);
				/*END AUDIT TRAIL VARIABLES*/

				/* UPDATE DATA*/
				$params['meeting_id']	= $meeting_id;
				$this->meeting->update_meeting($params);

				/*GET CURRENT DETAIL*/
				$info 				= $this->meeting->get_specific_meeting(array("meeting_id" => $meeting_id), TRUE);
				$curr_detail[] 		= $info;

				$meeting_name = $info['title'];

				/*DATA USED FOR AUDIT TRAIL*/
				$activity	= "%s has been updated in board meetings.";
				$activity 	= sprintf($activity, $meeting_name);
				/*END*/

				$msg  = $this->lang->line('data_updated');

			}

			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);


			ISCA_Model::commit();


			$flag 	= 1;
			$class	= SUCCESS;

		}
		catch (PDOException $e) {

			ISCA_Model::rollback();
						
			$msg = $this->rlog_error($e, TRUE);
		}
	 	catch (Exception $e) {

			ISCA_Model::rollback();
						
			$msg = $this->rlog_error($e, TRUE);
		}

		$info = array(
			"flag" => $flag,
			"class" => $class,
			"msg" => $msg,
		);

		echo  json_encode($info);
	}

	/*DELETE MEETING*/

	public function delete_meeting() {

		try  {


			$flag = 0; 
			$msg = 'Error';

			$params 				= get_params();
			$params['security'] 	= $params['param_1'];

			$this->validate_security($params);

			$key 			= 	$this->get_hash_key('meeting_id');
			$where			=	array();
			$where[$key]	=  $params['hash_id'];

			$info = $this->meeting->get_specific_meeting($where);

			if(empty($info))
				throw new Exception($this->lang->line("err_invalid_request"));

			ISCA_Model::beginTransaction();

			$meeting_name = $info['meeting_name'];

			/*DATA VARIABLES FOR AUDIT TRAILS*/
			$audit_action[]		= AUDIT_DELETE;
			$audit_table[]		= ISCA_Model::tbl_meeting;
			$audit_schema[]		= DB_ISCA;
			$prev_detail[]		= array($info);
			/*END AUDIT TRAIL VARIABLES*/

			$this->meeting->delete_meeting($info['meeting_id']);


			$curr_detail[] = array();

			/*DATA USED FOR AUDIT TRAIL*/
			$activity	= "%s has been deleted in board meeting.";
			$activity 	= sprintf($activity, $meeting_name);
			/*END*/
			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			ISCA_Model::commit();

			$flag = 1;
			$msg = $this->lang->line("data_deleted");
				
		} catch (PDOException $e) {

			ISCA_Model::rollback();
			
			$msg	= $e->getMessage();

		} catch (Exception $e) {

			ISCA_Model::rollback();
			
			$msg	= $e->getMessage();

		}
		
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
	}

	/*MODAL*/
	public function modal($encoded_id = null, $salt = null, $token = null) {

		try {

			$data = array();
			$resources = array();
			$resources['load_css'] 		= array(CSS_DATETIMEPICKER);
			$resources['load_js'] 		= array(JS_DATETIMEPICKER);

			// for update
			if(!EMPTY($encoded_id)) {

				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('meeting_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->meeting->get_specific_meeting($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['meeting_id'] 			= $info['meeting_id'];
				$data['meeting_date'] 			= $info['meeting_date'];
				$data['meeting_name'] 			= $info['meeting_name'];	
				$data['venue'] 					= $info['venue'];
				$data["agenda_data"]			= $info['agenda'];

				// CREATE NEW SECURITY VARIABLES
				$meeting_id		= $info['meeting_id'];
				$hash_id 		= $this->hash($meeting_id);			

			} else {
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e) {
			echo $e->getMessage();
		}
		catch(Exception $e) {
			echo $e->getMessage();
		}

		// modal view path
		$this->load->view('modals/library/agenda',$data);

		//load resources for modal
		$this->load_resources->get_resource($resources);
	}

	/*SERVER VALIDATIONS*/
	private function _validate(&$params)
	{
		try
		{					
			$this->validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields 					= array();
			$fields['meeting_name']		= 'Meeting Name';
			$fields['meeting_date']		= 'Meeting Date';
			$fields['venue']			= 'Meeting Venue';
			$fields['agenda_data']		= 'Meeting Agenda';

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_input($params)
	{
		try
		{
			$validation['meeting_name'] = array(
				'data_type' => 'string',
				'name'		=> 'Meeting Name',
				'max_len'	=> 100
			);
			$validation['venue'] = array(
				'data_type' => 'string',
				'name'		=> 'Meeting Venue',
				'max_len'	=> 100
			);
			$validation['agenda_data'] = array(
				'data_type' => 'string',
				'name'		=> 'Meeting Venue'
			);
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
}