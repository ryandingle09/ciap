<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Isca_cases_closed extends ISCA_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {

		$data = $resources = $modal = array();

		$resources['load_css'] 		= array(CSS_DATATABLE);
		$resources['load_js'] 		= array(JS_DATATABLE);
		$resources['datatable'][] 	= array('table_id' => 'case_close_table', 'path' => PROJECT_ISCA.'/isca_cases_closed/get_case_close_list');
		
		$this->template->load('case_close', $data, $resources);

	}

	public function get_case_close_list() {

		$data = $resources = $modal = array();

		$output = array(
			"sEcho" => 1,
			"iTotalRecords" => "1",
			"iTotalDisplayRecords" => "1",
			"aaData" => array()
		);
		
		$url = "";
		$delete_action = "";

		$action = "<div class='table-actions'>";

			$action .= "<a href='javascript:;' class='view tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' onclick=\"modal_init('".$url."')\"></a>";
			//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
				$action .= "<a href='javascript:;' class='md-trigger tooltipped edit' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='payments_order' onclick=\"modal_init('".$url."')\"></a>";
			//endif;
		
		$action .= '</div>';

	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[]	 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  $action;

		$output['aaData'][] = $data;

	 	echo json_encode($output);
	}	

	public function view_case_closed() {

		$data = array();
		$resources['load_css'] 		= array(CSS_DATATABLE, 'jquery-labelauty','isca', 'selectize.default');
		$resources['load_js'] 		= array(JS_DATATABLE, 'jquery-labelauty', 'selectize');
		
		$this->template->load('view_case_closed', $data, $resources);

	}

}
