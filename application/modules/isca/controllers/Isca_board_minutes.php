<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Isca_board_minutes extends ISCA_Controller {

	public function __construct() {

		parent::__construct();
		$this->load->model("attendees_model", "attendees");
		$this->load->model("meeting_model", "meetings");
		$this->load->model("minutes_model", "minutes");
	}

	public function index() {

		//point to catalog list as default

		$this->minutes_of_meeting();
	}

	public function minutes_of_meeting() {

		$data = $resources = $modal = array();

		$modal = array(
			'meeting_minutes' 		=> array(

				'controller'	=> __CLASS__,
				'module'		=> PROJECT_ISCA,
				'height'		=> '500px'
			)
			 
		);
		
		$resources['load_css'] 		= array(CSS_DATATABLE);
		$resources['load_js'] 		= array(JS_DATATABLE, JS_EDITOR);
		$resources['load_modal'] 	= $modal;
		$resources['datatable'][] 	= array('table_id' => 'minutes_table', 'path' => PROJECT_ISCA.'/isca_board_minutes/get_minutes_meeting_list');
		
		$this->template->load('library/meeting_minutes', $data, $resources);
	}

	/* GET ALL CATALOG LIST */
	public function get_minutes_meeting_list() {

		try {

			$params			= get_params();


			$select_fields 	= array("A.meeting_id", "A.created_date", "CONCAT(A.time_started, ' - ', A.time_ended) as time", "B.meeting_name", "A.final_flag");

			$where_fields 	= array("A.created_date", "CONCAT(A.time_started, ' - ', A.time_ended)", "B.meeting_name", "A.final_flag");

			$minutes_list 	= $this->minutes->get_minutes_list($select_fields, $where_fields, $params);
			$total 			= $this->minutes->total_length();
			$filtered_total = $this->minutes->filtered_length($select_fields, $where_fields, $params);

			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> $total["cnt"],
				"iTotalDisplayRecords" 	=> $filtered_total["cnt"],
				"aaData" 				=> array()
			);


			foreach ($minutes_list as $data):

				$meeting_id 	= $data['meeting_id'];

				$hash_id 		= $this->hash($meeting_id);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("Minute Meeting", "'.$url.'")';

				$action = "<div class='table-actions'>";

						$action .= "<a href='javascript:;' class='md-trigger tooltipped view' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='meeting_minutes' onclick=\"modal_init('".ACTION_VIEW.'/'.$url."')\"></a>";
						
						if($data['final_flag'] == '1') {
							//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
								$action .= "<a href='javascript:;' class='md-trigger tooltipped edit' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='meeting_minutes' onclick=\"modal_init('".ACTION_EDIT.'/'.$url."')\"></a>";
							//endif;

							//if (($this->permission->check_permission(MODULE_ROLE, ACTION_DELETE))) : 	  
								$action .= "<a href='javascript:;' onclick='" .$delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
							//endif;
						}
				
				$action .= '</div>';

				$row 		= array();
			 	$row[] 		=  $data['created_date'];
			 	$row[] 		=  $data['time'];
			 	$row[] 		=  $data['meeting_name'];
			 	$row[] 		=  ($data['final_flag'] == '0') ? "Final" : "Draft";
			 	
			 	$row[] 		=	$action;

				$output['aaData'][] = $row;

			endforeach;

		 	echo json_encode($output);

	 	} catch(PDOException $e) {

			echo $e->getMessage();
		}

		catch (Exception $e) {

			echo $e->getMessage();
		}
	}

	/* MODAL */
	public function modal($action_id = NULL, $encoded_id = null, $salt = null, $token = null) {

		try {

			$data = array();
			
			$resources = array();
			$resources['load_css'] 		= array(CSS_DATETIMEPICKER, "isca", "selectize.default");
			$resources['load_js'] 		= array(JS_DATETIMEPICKER, "selectize");

			$resources['loaded_init']	= array('ModalEffects.re_init();');
			$resources['load_modal']	= array(

				'manage_attendees' 		=> array(
					'controller'	=> __CLASS__,
					'module'		=> PROJECT_ISCA,
					'multiple'		=>	true,
					'height'		=> '500px',
					'method'		=> 'modal_attendees',
					'size'			=> 'md',

				),
			);

			// for update
			if(!EMPTY($encoded_id)) {
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('meeting_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->minutes->get_specific_minutes($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['meeting_id'] 			= $info['meeting_id'];
				$data['minutes_data'] 			= $info['minutes'];
				$data['created_by'] 			= $info['created_by'];	
				$data['created_date'] 			= $info['created_date'];
				$data['status_flag'] 			= $info['final_flag'];
				$data['time_started'] 			= $info['time_started'];
				$data['time_ended'] 			= $info['time_ended'];

				/*GET ALL MEETING ATTENDEES*/
				$attendees_id 			= $this->attendees->get_attended_list($info['meeting_id']);
				$data["attended_id"] 	= array();

				/*flatten attended id array*/
				foreach ($attendees_id as $ids) {

					$data["attended_id"][] = $ids['attendee_id'];
				}

				// CREATE NEW SECURITY VARIABLES
				$meeting_id		= $info['meeting_id'];
				$hash_id 		= $this->hash($meeting_id);			

			} else {
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			/* MEETING DROP DOWN SELECT DATA*/
			$meeting_data 		= array();

			$meeting_data 		= ($action_id == ACTION_VIEW) ? $this->meetings->get_meeting_all_list_drop_down() : $this->meetings->get_meeting_list_drop_down();

			/*ATTENDEES DROP DOWN SELECT DATA*/
			$attendees_data 			= array();
			$select_fields_attendees 	= array("A.attendee_id", "A.attendee_name");

			$attendees_data 			= $this->attendees->get_attendee_list($select_fields_attendees, null, null);

			$data['meeting_list'] 		= $meeting_data;
			$data['attendees_list'] 	= $attendees_data;

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);

			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			$data['action_id']  = $action_id;

		}

		catch(PDOException $e) {

			echo $e->getMessage();
		}
		catch(Exception $e) {

			echo $e->getMessage();
		}

		// modal view path
		$this->load->view('modals/library/meeting_minutes',$data);

		//load resources for modal
		$this->load_resources->get_resource($resources);
	}

	public function process() {

		try {

			$flag = 0;
			$msg = ERROR;

			$params = get_params();

			//SERVER VALIDATE FIELDS
			$this->_validate($params);

			$key = $this->get_hash_key('meeting_id');
			$where = array();
			$where[$key] = $params['hash_id'];

			$current_info = $this->minutes->get_specific_minutes($where);
			$meeting_id = $current_info['meeting_id'];

			ISCA_Model::beginTransaction();

			//CREATE DATA
			if(empty($meeting_id)) {

				/*DATA VARIABLES FOR AUDIT TRAILS*/
				$audit_action[]		= AUDIT_INSERT;
				$audit_table[]		= ISCA_Model::tbl_minutes;
				$audit_schema[]		= DB_ISCA;
				$prev_detail[]		= array();
				/*END AUDIT TRAIL VARIABLES*/

				$meeting_id = $this->minutes->insert_minutes($params);

				$info = $this->minutes->get_specific_minutes(array("meeting_id" => $meeting_id));
				$curr_detail[] = $info;

				$meeting_data = 'Meeting id: '.$curr_detail['meeting_id'];

				/*DATA USED FOR AUDIT TRAIL*/
				$activity	= "%s has been added in minutes meeting.";
				$activity 	= sprintf($activity, $meeting_data);
				/*END*/

				// TO BE DISPLAYED IN USER
				$msg  = $this->lang->line('data_saved');


			} else {

				/*DATA VARIABLES FOR AUDIT TRAILS*/
				$audit_action[]		= AUDIT_UPDATE;
				$audit_table[]		= ISCA_Model::tbl_minutes;
				$audit_schema[]		= DB_ISCA;
				$prev_detail[]		= array($current_info);
				/*END AUDIT TRAIL VARIABLES*/

				/* UPDATE DATA*/
				$params['meeting_id']	= $meeting_id;
				$this->minutes->update_minutes($params);

				/*GET CURRENT DETAIL*/
				$info 				= $this->minutes->get_specific_minutes(array("meeting_id" => $meeting_id), TRUE);
				$curr_detail[] 		= $info;


				$meeting_data = 'Meeting id: '.$curr_detail['meeting_id'];

				/*DATA USED FOR AUDIT TRAIL*/
				$activity	= "%s has been updated in minutes meeting.";
				$activity 	= sprintf($activity, $meeting_data);
				/*END*/


				// TO BE DISPLAYED IN USER
				$msg  = $this->lang->line('data_updated');

			}

			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			ISCA_Model::commit();

			$flag 	= 1;
			$class	= SUCCESS;

		}
		catch (PDOException $e) {

			ISCA_Model::rollback();

			$msg = $this->rlog_error($e, TRUE);
		}
		catch (Exception $e) {

			ISCA_Model::rollback();

			$msg = $this->rlog_error($e, TRUE);
		}

		$info = array(
			"flag" 	=> $flag,
			"class"	=> $class,
			"msg" 	=> $msg
		);
	
		
		echo json_encode($info);

	}

	public function delete_minutes() {
		try {
			$flag = 0;
			$msg = ERROR;

			$params = get_params();

			$params['security'] = $params['param_1'];

			$this->_validate_security($params);

			$key = $this->get_hash_key('meeting_id');
			$where = array();
			$where[$key] = $params['hash_id'];

			$info =	$this->minutes->get_specific_minutes($where);
			if(empty($info['meeting_id'])) 
				throw new Exception($this->lang->line("err_invalid_request"));

			$meeting_data = "Meeting id: ".$info['meeting_id'];
			
			ISCA_Model::beginTransaction();

			/*DATA VARIABLES FOR AUDIT TRAILS*/
			$audit_action[]		= AUDIT_DELETE;
			$audit_table[]		= ISCA_Model::tbl_minutes;
			$audit_schema[]		= DB_ISCA;
			$prev_detail[]		= array($info);
			/*END AUDIT TRAIL VARIABLES*/

			$this->minutes->delete_minutes($info['meeting_id']);

			$curr_detail[] = array();

			/*DATA USED FOR AUDIT TRAIL*/
			$activity	= "%s has been deleted in minutes meeting.";
			$activity 	= sprintf($activity, $meeting_data);
			/*END*/

			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			

			ISCA_Model::commit();


			$flag = 1;
			$msg = $this->lang->line("data_deleted");


		} 
		catch (PDOException $e) {

			ISCA_Model::rollback();

			$msg	= $e->getMessage();
		}
		catch (Exception $e) {

			ISCA_Model::rollback();

			$msg	= $e->getMessage();
		}
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
	}	
	
	/* MODAL ATTENDEES */
	public function modal_attendees($encoded_id = null, $salt = null, $token = null) {

		try {

			$data = array();
			
			$resources = array();
			$resources['load_css'] 		= array(CSS_DATETIMEPICKER, CSS_DATATABLE);
			$resources['load_js'] 		= array(JS_DATETIMEPICKER, JS_DATATABLE);
			$resources['datatable'][] 	= array('table_id' => 'attendees_table', 'path' => PROJECT_ISCA.'/isca_board_minutes/get_attendees_list');

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('position_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->positions->get_specific_position($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['position_id'] 			= $info['position_id'];
				$data['position_name'] 			= $info['position_name'];
				$data['created_by'] 			= $info['created_by'];	
				$data['created_date'] 			= $info['created_date'];
				$data["readonly_value"]			= "readonly";	

				// CREATE NEW SECURITY VARIABLES
				$position_id	= $info['position_id'];
				$hash_id 		= $this->hash($position_id);			

			} else {
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}

		// modal view path
		$this->load->view('modals/library/manage_attendees', $data);

		//load resources for modal
		$this->load_resources->get_resource($resources);
	}

	/* MANAGE ATTENDEES DETAILS */
	public function get_attendees_list() {

		try {

			
			$params = get_params();

			$select_fields 	= array("A.attendee_id", "A.attendee_name", "A.attendee_position");
			$where_fields 	= array("attendee_name", "attendee_position");

			$attendees 		= $this->attendees->get_attendee_list($select_fields, $where_fields, $params);
			$total 			= $this->attendees->total_length();
			$filtered_total = $this->attendees->filtered_length($select_fields, $where_fields, $params);


			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> $total["cnt"],
				"iTotalDisplayRecords" 	=> $filtered_total["cnt"],
				"aaData" 				=> array()
			);

			foreach ($attendees as $data):

				$attendee_id 	= $data['attendee_id'];

				$hash_id 		= $this->hash($attendee_id);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("Attendee", "'.$url.'")';

				$action = "<div class='table-actions'>";

					//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
						$action .= "<a href='javascript:;' class='tooltipped edit' data-tooltip='Edit' ></a>";
					//endif;

					//if (($this->permission->check_permission(MODULE_ROLE, ACTION_DELETE))) : 	  
						$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					//endif;
				
					$action .= '</div>';

				$row 		= 	array();
			 	$row[] 		=  	"<input type='text' value='".$data["attendee_name"]."' disabled>";
			 	$row[] 		=  	"<input type='text' value='".$data["attendee_position"]."' disabled>";
			 	$row[] 		=	$action;

				$output['aaData'][] = $row;

			endforeach;

		 	echo json_encode($output);

		} 
		catch(PDOException $e) {

			echo $e->getMessage();
		}
		catch (Exception $e) {

			echo $e->getMessage();
		}
	}

	/* PROCESS ATTENDEES */
	public function process_attendees() {

		try {
			$flag	 	= 0;
			$class 		= ERROR;

			$params 	= get_params();

			//SERVER VALIDATE FIELDS
			//$this->_validate($params);

			$key 			= $this->get_hash_key("attendee_id");
			$where 			= array();
			$where[$key] 	= $params["hash_id"];

			$current_info 	= $this->attendees->get_specific_attendee($where);
			$attendee_id	= $current_info['attendee_id'];

			ISCA_Model::beginTransaction();

			/*IF EMPTY ID MEANS SAVE/CREATING DATA*/
			if(empty($attendee_id)) {

				/*DATA VARIABLES FOR AUDIT TRAILS*/
				$audit_action[]		= AUDIT_INSERT;
				$audit_table[]		= ISCA_Model::tbl_attendees;
				$audit_schema[]		= DB_ISCA;
				$prev_detail[]		= array();
				/*END AUDIT TRAIL VARIABLES*/

				/*SAVE DATA*/
				$attendee_id = $this->attendees->insert_attendee($params);

				/*GET CURRENT DETAIL*/
				$info 				= $this->attendees->get_specific_attendee(array("attendee_id" => $attendee_id), TRUE);
				$curr_detail[] 		= $info;


				$attendee_name = $info['attendee_name'];


				/*DATA USED FOR AUDIT TRAIL*/
				$activity	= "%s has been added in arbitrator.";
				$activity 	= sprintf($activity, $attendee_name);
				/*END*/

				// TO BE DISPLAYED IN USER
				$msg  = $this->lang->line('data_saved');
			} else {

			}

			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			ISCA_Model::commit();

			$flag 	= 1;
			$class	= SUCCESS;
			
		}
		catch (PDOException $e) {

			ISCA_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		catch (Exception $e) {

			ISCA_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}

		$info = array(
			"flag" 	=> $flag,
			"class"	=> $class,
			"msg" 	=> $msg
		);
	
		
		echo json_encode($info);
	}


	public function delete_attendee() {
		try {
			$flag = 0;
			$msg = ERROR;

			$params = get_params();

			$params['security'] = $params['param_1'];

			$this->_validate_security($params);

			$key = $this->get_hash_key('attendee_id');
			$where = array();
			$where[$key] = $params['hash_id'];

			$info =	$this->attendees->get_specific_attendee($where);

			if(empty($info['attendee_id'])) 
				throw new Exception($this->lang->line("err_invalid_request"));

			$meeting_data = "Attendee id: ".$info['attendee_id'];
			
			ISCA_Model::beginTransaction();

			/*DATA VARIABLES FOR AUDIT TRAILS*/
			$audit_action[]		= AUDIT_DELETE;
			$audit_table[]		= ISCA_Model::tbl_attendees;
			$audit_schema[]		= DB_ISCA;
			$prev_detail[]		= array($info);
			/*END AUDIT TRAIL VARIABLES*/

			$this->attendees->delete_attendee($info['attendee_id']);

			$curr_detail[] = array();

			/*DATA USED FOR AUDIT TRAIL*/
			$activity	= "%s has been deleted in meeting attendees.";
			$activity 	= sprintf($activity, $meeting_data);
			/*END*/

			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			

			ISCA_Model::commit();


			$flag = 1;
			$msg = $this->lang->line("data_deleted");


		} 
		catch (PDOException $e) {

			ISCA_Model::rollback();

			$msg	= $e->getMessage();
		}
		catch (Exception $e) {

			ISCA_Model::rollback();

			$msg	= $e->getMessage();
		}
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
	}	
	/* SERVER VALIDATIONS */
	private function _validate(&$params)
	{
		try
		{
			
			$this->_validate_security($params);
			
			
			$fields = array();
			$fields['meeting_id']			= 'Meeting';

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}	
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _validate_input($params)
	{
		try
		{
			$validation['meeting_id'] = array(
					'data_type' => 'string',
					'name'		=> 'Meeting',
					'max_len'	=> 100
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}


}