<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Isca_seminar_materials extends ISCA_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model("seminar_material_model", "material");
	}

	public function index() {

		//point to catalog list as default

		$this->card_catalogues();

	}

	public function card_catalogues() {

		$data = $resources = $modal = array();

		$modal = array(
			'seminar_materials' => array(
				'controller'	=> __CLASS__,
				'module'		=> PROJECT_ISCA,
			)
		);
		
		$resources['load_css'] 		= array(CSS_DATATABLE);
		$resources['load_js'] 		= array(JS_DATATABLE);
		$resources['load_modal'] 	= $modal;
		$resources['datatable'][] 	= array('table_id' => 'arbitrator_table', 'path' => PROJECT_ISCA.'/isca_seminar_materials/get_seminar_material_list');
		
		$this->template->load('library/seminar_materials', $data, $resources);
	}


	/*GET ALL CATALOG LIST*/
	public function get_seminar_material_list() {

		$data = $resources = $modal = array();

		$output = array(
			"sEcho" => 1,
			"iTotalRecords" => "1",
			"iTotalDisplayRecords" => "1",
			"aaData" => array()
		);
		
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';

		$output['aaData'][] = $data;

	 	echo json_encode($output);
	}	


	public function process() {

		try
		{
			$flag = 0;
			$msg = ERROR;

			$params = get_params();

			$this->_validate($params);

			$key = $this->get_hash_key('seminar_material_id');
			$where = array();
			$where[$key] = $params['hash_id'];

			$current_info = $this->material->get_specific_material($where);

			$seminar_material_id = $current_info['seminar_material_id'];

			ISCA_Model::beginTransactios();

			if(empty($seminar_material_id)) {

				/*DATA VARIABLES FOR AUDIT TRAILS*/
				$audit_action[]		= AUDIT_INSERT;
				$audit_table[]		= ISCA_Model::tbl_seminar_material;
				$audit_schema[]		= DB_ISCA;
				$prev_detail[]		= array();
				/*END AUDIT TRAIL VARIABLES*/

				$this->material->insert_material($params);

			} else {



			}



		}
		catch (PDOException $e) {

			ISCA_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		catch (Exception $e) {

			ISCA_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}


		$info = array(
			"flag" 	=> $flag,
			"class"	=> $class,
			"msg" 	=> $msg
		);
	
		
		echo json_encode($info);

	}

	/*MODAL*/
	public function modal($encoded_id = null, $salt = null, $token = null) {

		try {

			$data = array();
			
			$resources = array();
			$resources['load_css']	 = array('selectize.default', 'uploadfile','isca');
			$resources['load_js']	 = array('selectize', 'jquery.uploadfile'); 
			$resources['upload'] = array(
				array('id' => 'seminar_file', 'path' => PATH_UPLOAD_SEMINAR_MATERIALS, 'allowed_types' => 'pdf,docx', 'show_progress' => 1,
				 'show_preview' => 1, 'page'=> "aip")
			);

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('position_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->positions->get_specific_position($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['position_id'] 			= $info['position_id'];
				$data['position_name'] 			= $info['position_name'];
				$data['created_by'] 			= $info['created_by'];	
				$data['created_date'] 			= $info['created_date'];
				$data["readonly_value"]			= "readonly";	

				// CREATE NEW SECURITY VARIABLES
				$position_id	= $info['position_id'];
				$hash_id 		= $this->hash($position_id);			

			} else {
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}

		// modal view path
		$this->load->view('modals/library/seminar_materials',$data);

		//load resources for modal
		$this->load_resources->get_resource($resources);
	}


		/*SERVER VALIDATIONS*/
	private function _validate(&$params)
	{
		try
		{					
			$this->_validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields 					= array();
			$fields['first_name']	= 'First name';

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}
	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	private function _validate_input($params)
	{
		try
		{
			$validation['first_name'] = array(
				'data_type' => 'string',
				'name'		=> 'First Name',
				'max_len'	=> 100
			);
			
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
}