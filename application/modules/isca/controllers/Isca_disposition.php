<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Isca_disposition extends ISCA_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {

		$data = $resources = $modal = array();

		$modal = array(
			'payments_order' 	=> array(
				'controller'	=> __CLASS__,
				'module'		=> PROJECT_ISCA,
			)
		);
		
		$resources['load_css'] 		= array(CSS_DATATABLE);
		$resources['load_js'] 		= array(JS_DATATABLE, JS_EDITOR);
		$resources['load_modal'] 	= $modal;
		$resources['datatable'][] 	= array('table_id' => 'arbitrator_table', 'path' => PROJECT_ISCA.'/isca_disposition/get_disposition_list');
		
		$this->template->load('disposition_status', $data, $resources);

	}


	/*GET ALL CATALOG LIST*/
	public function get_disposition_list() {

		$data = $resources = $modal = array();

		$output = array(
			"sEcho" => 1,
			"iTotalRecords" => "1",
			"iTotalDisplayRecords" => "1",
			"aaData" => array()
		);
		
		$url = "";
		$delete_action = "";

		$action = "<div class='table-actions'>";

			$action .= "<a href='javascript:;' class='view tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' onclick=\"modal_init('".$url."')\"></a>";
			//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
				$action .= "<a href='javascript:;' class='md-trigger tooltipped edit' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='payments_order' onclick=\"modal_init('".$url."')\"></a>";
			//endif;
		
		$action .= '</div>';

	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[]	 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  'Sample Data';
	 	$data[] 	=  $action;

		$output['aaData'][] = $data;

	 	echo json_encode($output);
	}	

	public function create_payment_order() {

		$data		= array();
		$resources 	= array();

		$resources['load_css'] 	= array('selectize.default', 'isca', CSS_DATETIMEPICKER);
		$resources['load_js'] 	= array('selectize', JS_DATETIMEPICKER);

		$this->template->load('create_payment_order', $data, $resources);

	}

	/*MODAL*/
	public function modal($encoded_id = null, $salt = null, $token = null) {

		try {

			$data = array();

			$resources 				= array();

			
			$resources['load_css'] 	= array('jquery-labelauty', 'isca', 'selectize.default');
			$resources['load_js'] 	= array('jquery-labelauty', 'selectize');

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('position_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->positions->get_specific_position($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['position_id'] 			= $info['position_id'];
				$data['position_name'] 			= $info['position_name'];
				$data['created_by'] 			= $info['created_by'];	
				$data['created_date'] 			= $info['created_date'];
				$data["readonly_value"]			= "readonly";	

				// CREATE NEW SECURITY VARIABLES
				$position_id	= $info['position_id'];
				$hash_id 		= $this->hash($position_id);			

			} else {
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}

		// modal view path
		$this->load->view('modals/payments_order',$data);

		// load modal resources
		$this->load_resources->get_resource($resources);
	}
}
