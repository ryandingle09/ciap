<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Isca_dashboard extends ISCA_Controller {

	public function __construct() {
		parent::__construct();		
	}

	public function index() {

		$data = $resources = $modal = array();
		
		$this->template->load('dashboard', $data, $resources);
	}
}	