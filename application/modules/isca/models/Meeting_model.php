<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Meeting_model extends ISCA_Model {
                
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_meeting_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields = str_replace(" , ", " ", implode(", ", $select_fields));

			$where	= $this->filtering($where_fields, $params, FALSE); 
			$order	= $this->ordering($where_fields, $params);
			$limit 	= $this->paging($params); 
			
			$filter_str 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
		
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				$filter_str
				GROUP BY A.meeting_id
	        	$order
	        	$limit
EOS;
	        			
			$query	= sprintf($query, ISCA_Model::tbl_meeting);
			$stmt	= $this->query($query, $filter_params);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_meeting_list_drop_down()
	{
		try
		{	
			
		
			
			$query = <<<EOS
				SELECT meeting_id, meeting_name
				FROM %s A
				WHERE A.meeting_id NOT IN (SELECT B.meeting_id FROM %s B WHERE B.final_flag = 0)
				
EOS;
	        			
			$query	= sprintf($query, ISCA_Model::tbl_meeting, ISCA_Model::tbl_minutes);
			$stmt	= $this->query($query, $filter_params);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	public function get_meeting_all_list_drop_down()
	{
		try
		{	
			
		
			
			$query = <<<EOS
				SELECT meeting_id, meeting_name
				FROM %s A
EOS;
	        			
			$query	= sprintf($query, ISCA_Model::tbl_meeting, ISCA_Model::tbl_minutes);
			$stmt	= $this->query($query, $filter_params);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_meeting_list($aColumns, $bColumns, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(meeting_id) cnt");
				
			return $this->select_one($fields, ISCA_Model::tbl_meeting);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_meeting($id)
	{
		try
		{			
			$this->delete_data(ISCA_Model::tbl_meeting, array('meeting_id'=>$id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_meeting_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("meeting_id" => $id);
				
			return $this->select_one($fields, ISCA_Model::tbl_meeting, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}
	
	public function insert_meeting($params){
			
		try
		{
			$val 							= array();
			$val["meeting_date"] 			= filter_var($params['meeting_date'], FILTER_SANITIZE_STRING);
			$val["meeting_name"] 			= filter_var($params['meeting_name'], FILTER_SANITIZE_STRING);
			$val["venue"] 					= filter_var($params['venue'], FILTER_SANITIZE_STRING);
			$val["agenda"] 					= $params['agenda_data'];
			$val["created_date"] 			= 'STAITC';
			$val["created_by"] 				= 'STAITC';
			$val["last_modified_by"] 		= 'STAITC';
			$val["last_modified_date"] 		= 'STAITC';
			
			
			return $this->insert_data(ISCA_Model::tbl_meeting, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}


	public function get_specific_meeting($where, $multi=FALSE) {

		try
		{
			$fields 	= array("*");
			$function	= ($multi) ? "select_all" : "select_one";
			
			return $this->{$function}($fields, ISCA_Model::tbl_meeting, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function update_meeting($params)
	{
		try
		{
			
			$val 							= array();
			$val["meeting_date"] 			= filter_var($params['meeting_date'], FILTER_SANITIZE_STRING);
			$val["meeting_name"] 			= filter_var($params['meeting_name'], FILTER_SANITIZE_STRING);
			$val["venue"] 					= filter_var($params['venue'], FILTER_SANITIZE_STRING);
			$val["agenda"] 					= $params['agenda_data'];
			$val["created_date"] 			= 'STAITC';
			$val["created_by"] 				= 'STAITC';
			$val["last_modified_by"] 		= 'STAITC';
			$val["last_modified_date"] 		= 'STAITC';

			$where 						= array();
			$where["meeting_id"]		= $params["meeting_id"];

			$this->update_data(ISCA_Model::tbl_meeting, $val, $where);

		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}

}