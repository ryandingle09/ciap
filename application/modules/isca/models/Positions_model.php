<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Positions_model extends CEIS_Model {
                
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_position_details($position_id)
	{
		try
		{
			$fields 			= array("role_code", "role_name");
			
			$where 				= array();
			$where["role_code"] = $role_code;
			
			return $this->select_one($fields, $this->role_table, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
		
	
	public function get_position_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields = str_replace(" , ", " ", implode(", ", $select_fields));

			$where	= $this->filtering($where_fields, $params, FALSE); 
			$order	= $this->ordering($where_fields, $params);
			$limit 	= $this->paging($params); 
			
			$filter_str 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
		
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				$filter_str
				GROUP BY A.position_id
	        	$order
	        	$limit
EOS;
	        			
			$query	= sprintf($query, CEIS_Model::tbl_positions);
			$stmt	= $this->query($query, $filter_params);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_position_list($aColumns, $bColumns, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(position_id) cnt");
				
			return $this->select_one($fields, CEIS_Model::tbl_positions);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_position($position_id)
	{
		try
		{			
			$this->delete_data(CEIS_Model::tbl_positions, array('position_id'=>$position_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_positions_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("position_id" => $id);
				
			return $this->select_one($fields, CEIS_Model::tbl_positions, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}
	
	public function insert_position($params){
			
		try
		{
			$val 					= array();
			$val["position_name"] 	= filter_var($params['position_name'], FILTER_SANITIZE_STRING);
			$val["created_date"] 	= date('Y-m-d H:i:s');

			return $this->insert_data(CEIS_Model::tbl_positions, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}


	public function get_specific_position($where, $multi=FALSE){

		try
		{
			$fields 	= array("*");
			$function	= ($multi) ? "select_all" : "select_one";
			
			return $this->{$function}($fields, CEIS_Model::tbl_positions, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function update_position($params)
	{
		try
		{
			$val 						= array();			
			$val["position_name"] 		= filter_var($params['position_name'], FILTER_SANITIZE_STRING);
			$val["modified_date"]		= date('Y-m-d H:i:s');
			
			$where 						= array();
			$where["position_id"]		= $params["position_id"];

			$this->update_data(CEIS_Model::tbl_positions, $val, $where);

		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}

}