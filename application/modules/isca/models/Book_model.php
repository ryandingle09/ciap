<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Book_model extends ISCA_Model {
                
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_book_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields = str_replace(" , ", " ", implode(", ", $select_fields));

			$where	= $this->filtering($where_fields, $params, FALSE); 
			$order	= $this->ordering($where_fields, $params);
			$limit 	= $this->paging($params); 
			
			$filter_str 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
		
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				JOIN param_book_authors B ON A.book_author_id = B.book_author_id
				$filter_str
				GROUP BY A.book_id
	        	$order
	        	$limit
EOS;
	        			
			$query	= sprintf($query, ISCA_Model::tbl_card_catalog);
			$stmt	= $this->query($query, $filter_params);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	public function get_authors($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields = str_replace(" , ", " ", implode(", ", $select_fields));

			$where	= $this->filtering($where_fields, $params, FALSE); 
			$order	= $this->ordering($where_fields, $params);
			$limit 	= $this->paging($params); 
			
			$filter_str 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
		
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				$filter_str
				GROUP BY A.book_author_id
	        	$order
	        	$limit
EOS;
	        			
			$query	= sprintf($query, ISCA_Model::tbl_book_author);
			$stmt	= $this->query($query, $filter_params);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_book_list($aColumns, $bColumns, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(book_id) cnt");
				
			return $this->select_one($fields, ISCA_Model::tbl_card_catalog);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_book($book_id)
	{
		try
		{			
			$this->delete_data(ISCA_Model::tbl_card_catalog, array('book_id'=>$book_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_arbitrator_count($book_id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("book_id" => $book_id);
				
			return $this->select_one($fields, ISCA_Model::tbl_card_catalog, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}
	
	public function insert_book($params){
			
		try
		{
			$val 							= array();
			$val["code_no"] 				= filter_var($params['code_no'], FILTER_SANITIZE_STRING);
			$val["book_class_no"] 			= filter_var($params['book_class_no'], FILTER_SANITIZE_STRING);
			$val["title"] 					= filter_var($params['title'], FILTER_SANITIZE_STRING);
			$val["book_author_id"] 			= filter_var($params['book_author'], FILTER_SANITIZE_STRING);
			$val["publishing_house"] 		= filter_var($params['publishing_house'], FILTER_SANITIZE_STRING);
			$val["publishing_date"] 		= filter_var($params['publishing_date'], FILTER_SANITIZE_STRING);
			$val["copyright_date"] 			= filter_var($params['copyright_date'], FILTER_SANITIZE_STRING);
			$val["edition"] 				= filter_var($params['edition'], FILTER_SANITIZE_STRING);
			$val["quantity"] 				= filter_var($params['quantity'], FILTER_SANITIZE_STRING);
			$val["contents"] 				= $params['content_book'];

			
			return $this->insert_data(ISCA_Model::tbl_card_catalog, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}


	public function get_specific_book($where, $multi=FALSE){

		try
		{
			$fields 	= array("*");
			$function	= ($multi) ? "select_all" : "select_one";
			
			return $this->{$function}($fields, ISCA_Model::tbl_card_catalog, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function update_book($params)
	{
		try
		{
			
			$val 							= array();
			$val["code_no"] 				= filter_var($params['code_no'], FILTER_SANITIZE_STRING);
			$val["book_class_no"] 			= filter_var($params['book_class_no'], FILTER_SANITIZE_STRING);
			$val["title"] 					= filter_var($params['title'], FILTER_SANITIZE_STRING);
			$val["book_author_id"] 			= filter_var($params['book_author'], FILTER_SANITIZE_STRING);
			$val["publishing_house"] 		= filter_var($params['publishing_house'], FILTER_SANITIZE_STRING);
			$val["publishing_date"] 		= filter_var($params['publishing_date'], FILTER_SANITIZE_STRING);
			$val["copyright_date"] 			= filter_var($params['copyright_date'], FILTER_SANITIZE_STRING);
			$val["edition"] 				= filter_var($params['edition'], FILTER_SANITIZE_STRING);
			$val["quantity"] 				= filter_var($params['quantity'], FILTER_SANITIZE_STRING);
			$val["contents"] 				= $params['content_book'];

			$where 					= array();
			$where["book_id"]		= $params["book_id"];

			$this->update_data(ISCA_Model::tbl_card_catalog, $val, $where);

		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}

}