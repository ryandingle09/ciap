<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Payment_order_model extends ISCA_Model {
                
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_payment_type_params($select_fields, $where_fields, $params) {

		try
		{	
			$fields 	= str_replace(" , ", " ", implode(", ", $select_fields));

			$where		= $this->filtering($where_fields, $params, FALSE); 
			$order		= $this->ordering($where_fields, $params);
			$limit 		= $this->paging($params); 
				
			$filter_str 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
		
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				$filter_str
				GROUP BY A.payment_type_id
	        	$order
	        	$limit
EOS;
			$query	= sprintf($query, ISCA_Model::tbl_payment_types);
			$stmt	= $this->query($query, $filter_params);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}

	}

	public function get_party_list($select_fields, $where_fields, $params) {

		try
		{	
			$fields = str_replace(" , ", " ", implode(", ", $select_fields));
		
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
EOS;
	        			
			$query	= sprintf($query, ISCA_Model::tbl_parties);
			$stmt	= $this->query($query, $filter_params);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}

	}

	public function get_case_party($select_fields, $where_fields, $case_id) {
		
		try
		{	
			$fields = str_replace(" , ", " ", implode(", ", $select_fields));

			//$where	= array("case_id" => $case_id);
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A LEFT JOIN case_parties B
				ON B.party_id = A.party_id
				WHERE case_id = ? and B.status_flag != ?
				GROUP BY A.party_id
	        	
EOS;
			$val 	= array($case_id, "D");
	        			
			$query	= sprintf($query, ISCA_Model::tbl_parties);
			$stmt	= $this->query($query, $val);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}	
	}
	public function get_case_party_data($select_fields, $where_fields, $case_id) {
		
		try
		{	
			$fields = str_replace(" , ", " ", implode(", ", $select_fields));

			//$where	= array("case_id" => $case_id);
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A LEFT JOIN case_parties B
				ON B.party_id = A.party_id
				WHERE case_id = ?
				GROUP BY A.party_id
	        	
EOS;
			$val = array($case_id);

			$query	= sprintf($query, ISCA_Model::tbl_parties);
			$stmt	= $this->query($query, $val);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}	
	}

	public function get_case_claims($select_fields, $where_fields, $case_id) {
		try
		{	
			$fields = str_replace(" , ", " ", implode(", ", $select_fields));

			//$where	= array("case_id" => $case_id);
			
			$query = <<<EOS
				SELECT B.* 
				FROM %s A
				LEFT JOIN %s B ON A.case_op_hdr_id = B.case_op_hdr_id
				where A.case_op_hdr_id =
				( 
					SELECT C.case_op_hdr_id
				   	FROM %s C
				   	WHERE C.cases_case_id = ? 
				   	GROUP BY C.case_op_hdr_id 
				   	ORDER BY C.case_op_hdr_id DESC 
				   	LIMIT 1
				)				
	        	
EOS;
			$val	= array($case_id);

			$query	= sprintf($query, ISCA_Model::tbl_case_op_hdr, ISCA_Model::tbl_case_claims, ISCA_Model::tbl_case_op_hdr);
			$stmt	= $this->query($query, $val);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}	
	}
	public function get_specific_case_claims($where, $multi = FALSE) {
		try
		{
			$fields 	= array("*");
			$function	= ($multi) ? "select_all" : "select_one";
			
			return $this->{$function}($fields, ISCA_Model::tbl_case_claims, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	public function get_case_arbitration_dtl($select_fields, $where_fields, $case_id){
		try
		{	
			$fields = str_replace(" , ", " ", implode(", ", $select_fields));

			//$where	= array("case_id" => $case_id);
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A 
				WHERE case_id = ?
				GROUP BY A.case_id
	        	
EOS;
	        $val	= array($case_id);

			$query	= sprintf($query, ISCA_Model::tbl_case_arbitration_detail);
			$stmt	= $this->query($query, $val);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}	
	}	
		
	public function get_cases_data($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields = str_replace(" , ", " ", implode(", ", $select_fields));

			$where	= $this->filtering($where_fields, $params, FALSE); 
			$order	= $this->ordering($where_fields, $params);
			$limit 	= $this->paging($params); 
			
			$filter_str 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
		
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				$filter_str
				GROUP BY A.case_id
	        	$order
	        	$limit
EOS;
	        			
			$query	= sprintf($query, ISCA_Model::tbl_case);
			$stmt	= $this->query($query, $filter_params);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}	
	}
	public function get_payment_order_data($where) {
		try 
		{

			$stmt = $this->select_one("*", ISCA_Model::tbl_case_op_hdr, $where);
			
			return $stmt;
		}	
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}

	public function get_case_list($select_fields, $where_fields, $params) {
		try
		{	
			$fields = str_replace(" , ", " ", implode(", ", $select_fields));

			$where	= $this->filtering($where_fields, $params, FALSE); 
			$order	= $this->ordering($where_fields, $params);
			$limit 	= $this->paging($params); 
			
			$filter_str 	= $where["search_str"];
			$filter_params 	= $where["search_params"];

			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A LEFT JOIN case_op_hdr B ON A.case_id = B.cases_case_id
				JOIN param_payment_type C ON C.payment_type_id = B.payment_type_id
				$filter_str
				$order
	        	$limit
	        	
EOS;
	        			
			/*GROUP BY A.case_id*/
			$query	= sprintf($query, ISCA_Model::tbl_case);
			$stmt	= $this->query($query, $filter_params);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}	
	}
	public function get_specific_payment_order($where, $multi=FALSE){

		try
		{
			$fields 	= array("*");
			$function	= ($multi) ? "select_all" : "select_one";
			
			return $this->{$function}($fields, ISCA_Model::tbl_case, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_specific_party($where, $multi=FALSE) {
		try
		{
			$fields 	= array("*");
			$function	= ($multi) ? "select_all" : "select_one";
			
			return $this->{$function}($fields, ISCA_Model::tbl_parties, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	public function get_specific_case_party($where, $multi=FALSE) {
		try
		{
			$fields 	= array("*");
			$function	= ($multi) ? "select_all" : "select_one";
			
			return $this->{$function}($fields, ISCA_Model::tbl_case_party, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}	

	public function insert_payment_order($params) {
			
		try
		{
			$val 						= array();
			
			$val["case_title"] 			= filter_var($params['case_title'], FILTER_SANITIZE_STRING);
			$val["short_title"] 		= $params['case_title']; /*filter_var($params['case_title'], FILTER_SANITIZE_STRING);*/
			$val["settlement_mode"] 	= filter_var($params['mode_of_settlement'], FILTER_SANITIZE_STRING);
			$val["created_by"] 			= "test";
			$val["created_date"] 		= $params['date_created'];

			$case_id = $this->insert_data(ISCA_Model::tbl_case, $val, TRUE);

			if(!empty($case_id)) {

				
				$this->insert_parties_claimant($params, $case_id);
				$this->insert_parties_respondent($params, $case_id);

				if (isset($params['mode_of_settlement']) && $params['mode_of_settlement'] == 'A') {
					
					$this->_insert_arbitration_details($params, $case_id);
				}

				

				$this->insert_case_op_hdr($params, $case_id);

				/*to follow*/
				//$this->_insert_case_op_dtl($params, $case_id);
			}

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function insert_new_claimant($params) {

		try {

			$data = array();

			$data['party_name'] 	= $params['value'];

			return  $this->insert_data(ISCA_Model::tbl_parties, $data, TRUE);
		}
		catch (PDOException $e) {

			$this->rlog_error($e);
			throw $e;
			
		}
	}
	
	public function insert_parties_claimant($params = array(), $case_id) {

		try {

			$ctr = 0;

			foreach ($params['claimant_name'] as $claimant) {

				$data = array();

				//$data['party_name'] 		= $claimant;
				$data['party_address'] 		= $params['claimant_address'][$ctr];
				$data['party_contact_no'] 	= $params['claimant_contact'][$ctr];
				
				$where 						= array();
				$where["party_id"]			= $claimant;

				//INSERT PARTY DETAIL CLAIMANT
				$this->update_data(ISCA_Model::tbl_parties, $data, $where);

				if(!empty($claimant) && !empty($case_id)) {

					$data_case 					= array();
					$data_case['status_flag']	= (isset($params['status_flag'][$ctr])) ? $params['status_flag'][$ctr] : "O" ;
					$data_case['case_id'] 		= $case_id;
					$data_case['party_id'] 		= $claimant;
					$data_case['party_type'] 	= '1';

					//INSERT CASE PARTY
					$this->insert_data(ISCA_Model::tbl_case_party, $data_case, TRUE);
				}

				$ctr+=1;

			}

		}
		catch (PDOException $e) {

			$this->rlog_error($e);
			throw $e;
			
		}
	}

	public function insert_parties_respondent($params = array(), $case_id) {

		try {

			$ctr = 0;

			foreach ($params['respondent_name'] as $respondent) {

				$data = array();

				//$data['party_name']			 	= $respondent;
				$data['party_address'] 			= $params['respondent_address'][$ctr];
				$data['party_contact_no'] 		= $params['respondent_contact'][$ctr];
				
				$where 						= array();
				$where["party_id"]			= $respondent;

				//UPDATE PARTY DETAIL RESPODENTS
				$this->update_data(ISCA_Model::tbl_parties, $data, $where);

				if(!empty($respondent) && !empty($case_id)) {

					$data_case = array();

					$data_case['status_flag']	= (isset($params['status_flag'][$ctr])) ? $params['status_flag'][$ctr] : "O" ;
					$data_case['case_id'] 		= $case_id;
					$data_case['party_id'] 		= $respondent;
					$data_case['party_type'] 	= '2';

					//INSERT CASE PARTY
					$this->insert_data(ISCA_Model::tbl_case_party, $data_case, TRUE);
				}

				$ctr+=1;
			}

		}
		catch (PDOException $e) {

			$this->rlog_error($e);
			throw $e;
			
		}
	}

	private function _insert_arbitration_details($params = array(), $case_id) {

		try {


			$data = array();

			$data['case_id'] 				= $case_id;
			$data['arbitration_mode'] 		= $params['mode_of_arbitration'];
			$data['number_disputants'] 		= $params['number_of_disputants'];

			//INSERT CASE PARTY
			$this->insert_data(ISCA_Model::tbl_case_arbitration_detail, $data, TRUE);
				

		}

		catch (PDOException $e) {

			$this->rlog_error($e);
			throw $e;
			
		}
	}

	private function _insert_case_claims($params = array(), $case_op_hdr_id) {

		try {

			$ctr = 0;
			if($params['mode_of_settlement'] == 'A') {

				foreach ($params['arbitration_contract_description'] as $contract_desc) {

					$data = array();

					$data['case_op_hdr_id']			= $case_op_hdr_id;
					$data['claim_desc']			 	= $contract_desc;
					$data['claim_type'] 			= $params['type_claim'][$ctr];
					$data['claim_amount'] 			= $params['arbitration_contract_amount'][$ctr];

					$this->insert_data(ISCA_Model::tbl_case_claims, $data, TRUE);

					$ctr+=1;
				}

			} else if ($params['mode_of_settlement'] == 'M') {

				foreach ($params['mediation_contract_description'] as $contract_desc) {

					$data = array();

					$data['case_op_hdr_id']			= $case_op_hdr_id;
					$data['claim_desc']			 	= $contract_desc;
					$data['claim_amount'] 			= $params['mediation_contract_amount'][$ctr];

					$this->insert_data(ISCA_Model::tbl_case_claims, $data, TRUE);

					$ctr+=1;
				}

			}
			
			
		}

		catch (PDOException $e) {

			$this->rlog_error($e);
			throw $e;
			
		}
	}

	public function insert_case_op_hdr($params = array(), $case_id) {

		try {

			$firm_name 					= implode(',',  array_values($params['claimant_name']));
			$address 					= implode(',',  array_values($params['claimant_address']));

			$data 						= array();
			$data['case_payment_type'] 	= $params['case_payment_type'];
			$data['cases_case_id'] 		= $case_id;
			$data['payment_type_id']	= $params['payment_type'];
			$data['reference_no']		= "case_no";
			$data['firm_name']			= $firm_name;
			$data['address']			= $address;
			$data['total_amount']		= isset($params['total_amount']) ? $params['total_amount'] : '1000';
			$data['status_code']		= 'Submitted'; //sample

			$val = array();
			$val['case_title'] = $params['case_title'];

			$where = array("case_id" => $case_id);

			// update case title
			$this->update_data(ISCA_Model::tbl_case, $val, $where);

			$case_op_hdr_id =   $this->insert_data(ISCA_Model::tbl_case_op_hdr, $data, TRUE);
			$this->_insert_case_claims($params, $case_op_hdr_id);
		}
		catch (PDOException $e) {

			$this->rlog_error($e);
			throw $e;
			
		}

	}

	public function delete_arbitrator($id)
	{
		try
		{			
			$this->delete_data(ISCA_Model::tbl_arbitrator, array('id'=>$id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function withdrawn_party($params) {
		try {

			$val  = array();
			$val['status_flag'] = "D";

			$where 						= array();
			$where["case_id"]			= $params["case_id"];
			$where["party_id"]			= $params["party_id"];
			
			$this->update_data(ISCA_Model::tbl_case_party, $val, $where);
		} 
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	public function update_case_party($val=array(), $where) {

		try {

			$this->update_data(ISCA_Model::tbl_case_party, $val, $where);

		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}

	}

	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_case_list($aColumns, $bColumns, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}	
	}
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(case_id) cnt");
				
			return $this->select_one($fields, ISCA_Model::tbl_case);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
}