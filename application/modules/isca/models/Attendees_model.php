<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Attendees_model extends ISCA_Model {
                
	
	public function __construct()
	{
		parent::__construct();
	}
	
		
	
	public function get_attendee_list($select_fields, $where_fields, $params)
	{
		try
		{	
		
			$fields = str_replace(" , ", " ", implode(", ", $select_fields));

			$where	= $this->filtering($where_fields, $params, FALSE); 
			$order	= $this->ordering($where_fields, $params);
			$limit 	= $this->paging($params); 
			
			$filter_str 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
		
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				$filter_str
				GROUP BY A.attendee_id
	        	$order
	        	$limit
EOS;
	        			
			$query	= sprintf($query, ISCA_Model::tbl_attendees);
			$stmt	= $this->query($query, $filter_params);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	

	public function get_attended_list($meeting_id)
	{
		try
		{	
			
			
			$query = <<<EOS
				SELECT A.attendee_id
				FROM %s A
				WHERE A.meeting_id = $meeting_id
EOS;
	        			
			$query	= sprintf($query, ISCA_Model::tbl_meeting_attendees);
		
			$stmt	= $this->query($query);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_attendee_list($aColumns, $bColumns, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(attendee_id) cnt");
				
			return $this->select_one($fields, ISCA_Model::tbl_attendees);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_attendee($attendee_id)
	{
		try
		{			
			$this->delete_data(ISCA_Model::tbl_attendees, array('attendee_id'=>$attendee_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_attendee_count($attendee_id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("attendee_id" => $attendee_id);
				
			return $this->select_one($fields, ISCA_Model::tbl_attendees, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}
	
	public function insert_attendee($params){
			
		try
		{
			$val 							= array();
			$val["attendee_position"] 			= filter_var($params['attendee_position'], FILTER_SANITIZE_STRING);
			$val["attendee_name"] 			= filter_var($params['attendee_name'], FILTER_SANITIZE_STRING);
			
			
			return $this->insert_data(ISCA_Model::tbl_attendees, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}


	public function get_specific_attendee($where, $multi=FALSE) {

		try
		{
			$fields 	= array("*");
			$function	= ($multi) ? "select_all" : "select_one";
			
			return $this->{$function}($fields, ISCA_Model::tbl_attendees, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function update_attendee($params)
	{
		try
		{
			
			$val 							= array();
			$val["attendee_date"] 			= filter_var($params['attendee_date'], FILTER_SANITIZE_STRING);
			$val["attendee_name"] 			= filter_var($params['attendee_name'], FILTER_SANITIZE_STRING);
			$val["venue"] 					= filter_var($params['venue'], FILTER_SANITIZE_STRING);
			$val["agenda"] 					= $params['agenda_data'];
			$val["created_date"] 			= 'STAITC';
			$val["created_by"] 				= 'STAITC';
			$val["last_modified_by"] 		= 'STAITC';
			$val["last_modified_date"] 		= 'STAITC';

			$where 				= array();
			$where["attendee_id"]		= $params["attendee_id"];

			$this->update_data(ISCA_Model::tbl_attendees, $val, $where);

		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}

}