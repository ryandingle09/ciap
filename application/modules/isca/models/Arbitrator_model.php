<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Arbitrator_model extends ISCA_Model {
                
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_arbitrator_details($id)
	{
		try
		{
			$fields 			= array("role_code", "role_name");
			
			$where 				= array();
			$where["role_code"] = $role_code;
			
			return $this->select_one($fields, $this->role_table, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
		
	
	public function get_arbitrator_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields = str_replace(" , ", " ", implode(", ", $select_fields));

			$where	= $this->filtering($where_fields, $params, FALSE); 
			$order	= $this->ordering($where_fields, $params);
			$limit 	= $this->paging($params); 
			
			$filter_str 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
		
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				$filter_str
				GROUP BY A.id
	        	$order
	        	$limit
EOS;
	        			
			$query	= sprintf($query, ISCA_Model::tbl_arbitrator);
			$stmt	= $this->query($query, $filter_params);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_arbitrator_list($aColumns, $bColumns, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(id) cnt");
				
			return $this->select_one($fields, ISCA_Model::tbl_arbitrator);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_arbitrator($id)
	{
		try
		{			
			$this->delete_data(ISCA_Model::tbl_arbitrator, array('id'=>$id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_arbitrator_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("id" => $id);
				
			return $this->select_one($fields, ISCA_Model::tbl_arbitrator, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}
	
	public function insert_arbitrator($params){
			
		try
		{
			$val 					= array();
			$val["first_name"] 		= filter_var($params['first_name'], FILTER_SANITIZE_STRING);
			$val["last_name"] 		= filter_var($params['last_name'], FILTER_SANITIZE_STRING);
			$val["middle_initial"] 	= filter_var($params['middle_initial'], FILTER_SANITIZE_STRING);
			$val["profession"] 		= filter_var($params['profession'], FILTER_SANITIZE_STRING);
			$val["area"] 			= "test";
			$val["sole"] 			= "test_data"; //for sample
			$val["status"] 			= "test_data"; // for sample
			
			return $this->insert_data(ISCA_Model::tbl_arbitrator, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}


	public function get_specific_arbitrator($where, $multi=FALSE){

		try
		{
			$fields 	= array("*");
			$function	= ($multi) ? "select_all" : "select_one";
			
			return $this->{$function}($fields, ISCA_Model::tbl_arbitrator, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function update_arbitrator($params)
	{
		try
		{
			
			$val 					= array();
			$val["first_name"] 		= filter_var($params['first_name'], FILTER_SANITIZE_STRING);
			$val["last_name"] 		= filter_var($params['last_name'], FILTER_SANITIZE_STRING);
			$val["middle_initial"] 	= filter_var($params['middle_initial'], FILTER_SANITIZE_STRING);
			$val["profession"] 		= filter_var($params['profession'], FILTER_SANITIZE_STRING);
			$val["area"] 			= "test";
			$val["sole"] 			= "test_data"; //for sample
			$val["status"] 			= "test_data"; // for sample

			$where 						= array();
			$where["id"]		= $params["id"];

			$this->update_data(ISCA_Model::tbl_arbitrator, $val, $where);

		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}

}