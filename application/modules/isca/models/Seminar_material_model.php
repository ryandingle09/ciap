<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Seminar_material_model extends ISCA_Model {
                
	var $meeting_attendees_tbl = "meeting_attendees";
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_material_details($meeting_id)
	{
		try
		{
			$fields 			= array("role_code", "role_name");
			
			$where 				= array();
			$where["role_code"] = $role_code;
			
			return $this->select_one($fields, $this->role_table, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
		
	
	public function get_material_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields = str_replace(" , ", " ", implode(", ", $select_fields));

			$where	= $this->filtering($where_fields, $params, FALSE); 
			$order	= $this->ordering($where_fields, $params);
			$limit 	= $this->paging($params); 
			
			$filter_str 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
		
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				LEFT JOIN isca_meetings B ON A.meeting_id = B.id 
				$filter_str
				GROUP BY A.meeting_id
	        	$order
	        	$limit
EOS;
	        			
			$query	= sprintf($query, ISCA_Model::tbl_seminar_material);
			$stmt	= $this->query($query, $filter_params);
	
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_minutes_list($aColumns, $bColumns, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(meeting_id) cnt");
				
			return $this->select_one($fields, ISCA_Model::tbl_seminar_material);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_material($meeting_id)
	{
		try
		{			
			$where =  array('meeting_id' => $meeting_id);
			$this->delete_data(ISCA_Model::tbl_seminar_material, $where);	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_material_count($meeting_id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("meeting_id" => $meeting_id);
				
			return $this->select_one($fields, ISCA_Model::tbl_seminar_material, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}
	
	public function insert_material($params){
			
		try
		{
			$val 										= array();
			$val["seminar_material_title"] 				= $params['title'];
			$val["seminar_material_attatchment"] 		= $params['seminar_file'];
			$val["created_by"] 							= 'STAITC';
			$val["created_date"] 						= 'STAITC';
			$val["last_modified_by"] 					= 'STAITC';
			$val["last_modified_date"] 					= 'STAITC';
			
			$this->insert_data(ISCA_Model::tbl_seminar_material, $val, TRUE);

			if(isset($params['attendees_id']) && !empty($params['meeting_id'])) {

				$this->_insert_attendees($params['attendees_id'], $params['meeting_id']);
			}
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}


	public function get_specific_material($where, $multi=FALSE) {

		try
		{
			$fields 	= array("*");
			$function	= ($multi) ? "select_all" : "select_one";
			
			return $this->{$function}($fields, ISCA_Model::tbl_seminar_material, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function update_material($params)
	{
		try
		{
			
			$val 							= array();
			$val["meeting_id"]	 			= filter_var($params['meeting_id'], FILTER_SANITIZE_STRING);
			$val["time_started"] 			= $params['time_started'];
			$val["time_ended"] 				= $params['time_ended'];
			$val["minutes"] 				= $params['minutes_data'];
			$val["final_flag"] 				= $params['status_flag'];
			$val["created_by"] 				= 'STAITC';
			$val["created_date"] 			= 'STAITC';
			$val["last_modified_by"] 		= 'STAITC';
			$val["last_modified_date"] 		= 'STAITC';

			$where 				= array();
			$where["meeting_id"]		= $params["meeting_id"];

			$this->update_data(ISCA_Model::tbl_seminar_material, $val, $where);

			/*DELETR CURRENT ATTENDEES AND RE INSERT THE NEW DATA*/
			if(isset($params['attendees_id']) && !empty($params['meeting_id'])) {

				$this->delete_data($this->meeting_attendees_tbl, $where);
				$this->_insert_attendees($params['attendees_id'], $params['meeting_id']);
			}

		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}

	private function _insert_tags ($attendees, $meeting_id) {
		try
		{
			foreach ($attendees as $attendee_id):

				$params	= array();
				$params["meeting_id"] = filter_var($meeting_id, FILTER_SANITIZE_STRING);
				$params["attendee_id"] = filter_var($attendee_id, FILTER_SANITIZE_NUMBER_INT);
				
				$this->insert_data($this->meeting_attendees_tbl, $params);
				
			endforeach;
				
		} 
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}	
	}

}