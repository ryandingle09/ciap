<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Closed Cases</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Closed Cases
		<span>Manage Closed Cases</span>
	  </h5>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="case_close_table">
  <thead>
	<tr>
	  <th width="15%">DATE</th>
	  <th width="15%">CASE NO.</th>
	  <th width="15%">CASE TITLE</th>
	  <th width="20%">NATURE OF CASE</th>
	  <th width="15%">SORTED</th>
	  <th width="15%"></th>
	</tr>
  </thead>
  </table>
</div>
