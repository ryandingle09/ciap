<div class="page-title">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Cases</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Cases
		<span>Manage Cases</span>
	  </h5>
	</div>
  </div>
</div>

<div class="m-t-lg">
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="case_table">
  <thead>
	<tr>
	  <th width="15%">DATE</th>
	  <th width="15%">CASE NO.</th>
	  <th width="15%">CASE TITLE</th>
	  <th width="20%">NATURE OF CASE</th>
	  <th width="15%"></th>
	</tr>
  </thead>
  </table>
</div>
