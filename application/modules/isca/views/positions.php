<div class="page-title">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">ISCA Positions</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>ISCA Positions
		<span>Manage Positions</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    <button class="waves-effect waves-light" type="button" onclick="load_datatable('position_table','<?php echo PROJECT_CEIS ?>/ceis_positions/get_position_list')"><i class="flaticon-arrows97"></i> Refresh</button>
	  </div>
	  
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_position" id="add_position" name="add_position" onclick="modal_init()">Add New Position</button>
	  </div>
	</div>
  </div>
</div>

<div class="m-t-lg">
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="position_table">
  <thead>
	<tr>
	  <th width="20%">Name of Position</th>
	  <th width="20%">Created By</th>
	  <th width="20%">Created Date</th>
	  <th width="15%">Last Modified By</th>
	  <th width="15%">Last Modified Date</th>
	  <th width="10%" class="text-center">Actions</th>
	</tr>
  </thead>
  </table>
</div>


<script>
	var deleteObj = new handleData({ 
		controller 	: 'isca_positions', 
		method 		: 'delete_position', 
		module		: '<?php echo PROJECT_ISCA ?>' 
	});
</script>
