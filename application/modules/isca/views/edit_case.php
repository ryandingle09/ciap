<div class="page-title">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" >Case</a></li>
	<li><a href="#" class="active">Edit Case</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Cases
		<span>Edit Case</span>
	  </h5>
	</div>
  </div>
</div>

<form id="case_form">
	<div class="m-t-lg">

		<div class="row" style="background-color:#fff;">

			<div>

			  <ul class="tabs"style="background-color:#b2dfdb !important;" >
			    <li class="tab col s2"><a href="#case_tab" style="color:black" class="active">Case Information</a></li>
			    <li class="tab col s2"><a href="#party_tab" style="color:black" >Parties</a></li>
			    <li class="tab col s2"><a href="#arbitrator_tab"  style="color:black" >Arbitrator</a></li>
			    <li class="tab col s2"><a href="#case_status" style="color:black" >Case Status</a></li>
			    <li class="tab col s2"><a href="#order_payment_tab" style="color:black" >Order of Payments</a></li>
			  </ul>

			</div>

			<!-- CASE CONTENT TABS -->
			<div id="case_tab" class="col s12">
				<div class="container">
					<div class="row">
						<div class="col s12" style="border-bottom:1px solid #a5a5a5">
							<div class="col s6">
								<br/>
								<br/>
								<h6>Basic Information</h6>
							</div>
							<div class="col s6 " >
							  <div class="input-field " style="float:right">
							    <input type="checkbox" class="labelauty" name="status" id="status" value="<?php echo ACTIVE ?>" data-labelauty="Inactive|ACTIVE" checked/>
						      </div>
						    </div>
						</div>
					</div>

					<div class="row">
						<div class="col s8">
							<span style="font-weight:bold"> CASE NO.  </span> <span> 01-2016</span>
						</div> 
						<div class="col s4">
							<span style="font-weight:bold"> DATE FILED  </span> <span> Jan 26, 2016</span>
						</div> 
					</div>

					<div class="row">
						<div class="col s8">
							<span style="font-weight:bold"> CASE TITLE  </span> <span> Juna Dela Cruz vs. Juan Dela Rosa</span>
						</div> 
						<div class="col s4">
							 <span> STAFF-IN-CHARGE</span>
						</div> 
					</div>

					<div class="row">
						<div class="col s8">
							<span style="font-weight:bold"> MODE OF SETTLEMENT  </span> <span> Arbitration </span>
						</div> 
						<div class="col s4">
							 <select>
							 	<option value=""> Option1 </option>
							 	<option value="1"> Option2 </option>
							 	<option value="2"> Option3 </option>
							 	<option value="3"> Option4 </option>
							 </select>
						</div> 
					</div>

					<div class="row">

						<div class = "input-field col s6">
							<label> SHORT TITLE </label>
							<input type="text" class="validate" value="title"/>
						</div>

						<div class = "input-field col s4">
							<br>
							<select>
							 	<option value=""> Option1 </option>
							 	<option value="1"> Option2 </option>
							 	<option value="2"> Option3 </option>
							 	<option value="3"> Option4 </option>
							 </select>
							<label> CASE STATUS </label> 
						</div>
						<div class="input-field col s2">
							<label class="label">&nbsp;</label>
							<input type="checkbox" class="labelauty" name="appeal" id="appeal" value="<?php echo ACTIVE ?>" data-labelauty="n/a|On-appeal" checked/>
						</div>
					</div>


					<div class="row">

						<div class = "input-field col s4">
							<br>
							<label> NATURE OF CASE </label> 
							<select class="multi" id="nature_case">
							 	<option value=""> Option1 </option>
							 	<option value="1"> Option2 </option>
							 	<option value="2"> Option3 </option>
							 	<option value="3"> Option4 </option>
							 </select>
						</div>

						<div class = "input-field col s3">
							<input type="text" class="datepicker" value="05/14/1993"> 
							<label > DATE OF SHERIFF'S RETURN </label> 
						</div>
						
						<div class = "input-field col s3">
							<input type="text" class="datepicker" value="05/14/1993"> 
							<label > TARGET DATE OF AWARD </label> 
						</div>
						
						<div class = "input-field col s2">
							<label> GROSS AWARD</label> 
							<input type="text" name="gross_award" value="AWARD">
						</div>

					</div>

					<div class="row">

						<div class = "input-field col s4">
							<br>
							<label> TYPE OF CASE </label> 
							<select>
							 	<option value=""> Option1 </option>
							 	<option value="1"> Option2 </option>
							 	<option value="2"> Option3 </option>
							 	<option value="3"> Option4 </option>
							 </select>
						</div>

						<div class = "input-field col s3">
							<input type="text" value="1000"> 
							<label > SUM IN DISPUTE(FINAL) </label> 
						</div>
						
						<div class = "input-field col s3">
							<input type="text" class="datepicker" value="05/14/1993"> 
							<label > TARGET DATE OF AWARD </label> 
						</div>
						
						<div class = "input-field col s2">
							<label> NET AWARD</label> 
							<input type="text" name="gross_award" value="AWARD">
						</div>

					</div>

					<div class="row">
						<div class="col s12">
							<h6 style="font-weight:bold">Appeal</h6>
							<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered" id="appeal_table">
								<thead>
									<tr>
										<th width="10%">DATE</th>
										<th width="15%">TYPE</th>
										<th width="15%">APPELANT</th>
										<th width="10%">STATUS</th>
										<th width="10%">G.R.NO.</th>
										<th width="5%">ACTION</th>
									</tr>
								</thead>
								<tbody class="tbody_apeal">
									<!-- SAMPLE/STATIC DATA -->

									<tr>
										<td> 
											<span> 05/14/1993 </span>
										</td>
										<td>
											<span> Supreme Court </span>
										</td>
										<td>
											<span> Juan Dela Cruz </span>
										</td>
										<td>
											<span> Affirmed </span>
										</td>
										<td>
											<span> 1401077 </span>
										</td>
										<td> 
											<div class='table-actions'>
												<a href='javascript:;' class='edit delete_attendees'></a>
												<a href='javascript:;' class='delete delete_attendees'></a>
											</div>
										</td>
									</tr>

									<tr>
										<td> 
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="datepicker">
									 			</div>	
									 		</div>
										</td>
										<td>
											<select placeholder='Select Attendees'>
												<option value=""> Select Attendees</option>
												<option value="1"> Juan Dela Cruz</option>
												<option value="2"> Juan Dela Cruz</option>
												<option value="3"> Juan Dela Cruz</option>
												<option value="4"> Juan Dela Cruz</option>
												<option value="5"> Juan Dela Cruz</option>
												<option value="6"> Juan Dela Cruz</option>
												<option value="7"> Juan Dela Cruz</option>
											</select>
										</td>
										<td>
											<select placeholder='Select Attendees'>
												<option value=""> Select Attendees</option>
												<option value="1"> Juan Dela Cruz</option>
												<option value="2"> Juan Dela Cruz</option>
												<option value="3"> Juan Dela Cruz</option>
												<option value="4"> Juan Dela Cruz</option>
												<option value="5"> Juan Dela Cruz</option>
												<option value="6"> Juan Dela Cruz</option>
												<option value="7"> Juan Dela Cruz</option>
											</select>
										</td>
										<td>
											<select  placeholder='Select Attendees'>
												<option value=""> Select Attendees</option>
												<option value="1"> Juan Dela Cruz</option>
												<option value="2"> Juan Dela Cruz</option>
												<option value="3"> Juan Dela Cruz</option>
												<option value="4"> Juan Dela Cruz</option>
												<option value="5"> Juan Dela Cruz</option>
												<option value="6"> Juan Dela Cruz</option>
												<option value="7"> Juan Dela Cruz</option>
											</select>
										</td>
										<td>
											<div class="input-field">
												<input type="text">
											</div>
										</td>
										<td> 
											<div class='input-field table-actions'>
												<a href='javascript:;' class='save delete_attendees'></a>
												<a href='javascript:;' class='delete delete_attendees'></a>
											</div>
										</td>
									</tr>

									<tr class="tr_add_appeal">
										<td  colspan="6" style="text-align:right"> 
											<a href="javascript:;" class="btn waves-effect waves-light blue" id="add_appeal" name="add_appeal">ADD</a>
									 	</td>
									</tr>	
								</tbody>	
							</table>
						</div>
					</div>

				</div>

			</div>

			<!-- Parties CONTENT TAB -->
			<div id="party_tab" class="col s12">
				<div class="container">
					<!-- CLIMANT TABLE -->
					<div class="row">
						<div class="col s12">
							<h6 style="font-weight:bold">Claimant</h6>
							<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered" id="claimant_table">
								<thead>
									<tr>
										<th width="10%">NAME</th>
										<th width="15%">BUSINESS ADDRESS</th>
										<th width="15%">EMAIL ADDRESS</th>
										<th width="10%">CONTACT NO.</th>
										<th width="5%"></th>
									</tr>
								</thead>
								<tbody class="tbody_claimant">
									<!-- SAMPLE/STATIC DATA -->

									<tr>
										<td> 
											<span> Juan Dela Cruz </span>
										</td>
										<td>
											<span> Address </span>
										</td>
										<td>
											<span> juandc@email.com </span>
										</td>
										<td>
											<span> 092312345678 </span>
										</td>
										<td> 
											<div class='table-actions'>
												<a href='javascript:;' class='edit delete_attendees'></a>
												<a href='javascript:;' class='delete delete_attendees'></a>
											</div>
										</td>
									</tr>

									<tr>
										<td>
											<select placeholder='Select Attendees'>
												<option value=""> Select Attendees</option>
												<option value="1"> Juan Dela Cruz</option>
												<option value="2"> Juan Dela Cruz</option>
												<option value="3"> Juan Dela Cruz</option>
												<option value="4"> Juan Dela Cruz</option>
												<option value="5"> Juan Dela Cruz</option>
												<option value="6"> Juan Dela Cruz</option>
												<option value="7"> Juan Dela Cruz</option>
											</select>
										</td>
										<td> 
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>
										<td> 
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>
										<td> 
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>
										<td> 
											<div class='input-field table-actions'>
												<a href='javascript:;' class='save delete_attendees'></a>
												<a href='javascript:;' class='delete delete_attendees'></a>
											</div>
										</td>
									</tr>

									<tr class="tr_add_claimant">
										<td  colspan="6" style="text-align:right"> 
											<a href="javascript:;" class="btn waves-effect waves-light blue" id="add_claimant" name="add_claimant">ADD</a>
									 	</td>
									</tr>

								</tbody>	
							</table>
						</div>
					</div>

					<!-- CLAIMANT COUNSEL TABLE -->
					<div class="row">
						<div class="col s12">
							<h6>Legal Counsel/Authorized Representative</h6>
							<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered" id="claimant_counsel_table">
								<thead>
									<tr>
										<th width="10%">NAME</th>
										<th width="15%">BUSINESS ADDRESS</th>
										<th width="15%">EMAIL ADDRESS</th>
										<th width="10%">CONTACT NO.</th>
										<th width="5%"></th>
									</tr>
								</thead>
								<tbody class="tbody_claimant_counsel">
									<!-- SAMPLE/STATIC DATA -->

									<tr>
										<td> 
											<span> Juan Dela Cruz </span>
										</td>
										<td>
											<span> Address </span>
										</td>
										<td>
											<span> juandc@email.com </span>
										</td>
										<td>
											<span> 092312345678 </span>
										</td>
										<td> 
											<div class='table-actions'>
												<a href='javascript:;' class='edit delete_attendees'></a>
												<a href='javascript:;' class='delete delete_attendees'></a>
											</div>
										</td>
									</tr>

									<tr>
										<td>
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>
										<td> 
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>
										<td> 
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>
										<td> 
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>
										<td> 
											<div class='input-field table-actions'>
												<a href='javascript:;' class='save delete_attendees'></a>
												<a href='javascript:;' class='delete delete_attendees'></a>
											</div>
										</td>
									</tr>

									<tr class="tr_add_claimant_counsel">
										<td  colspan="6" style="text-align:right"> 
											<a href="javascript:;" class="btn waves-effect waves-light blue" id="add_claimant_counsel" name="add_claimant_counsel">ADD</a>
									 	</td>
									</tr>
								</tbody>	
							</table>
						</div>
					</div>

					<!-- RESPONDENT TABLE -->
					<div class="row">
						<div class="col s12">
							<h6 style="font-weight:bold">Respondent</h6>
							<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered" id="respondent_table">
								<thead>
									<tr>
										<th width="10%">NAME</th>
										<th width="15%">BUSINESS ADDRESS</th>
										<th width="15%">EMAIL ADDRESS</th>
										<th width="10%">CONTACT NO.</th>
										<th width="5%"></th>
									</tr>
								</thead>
								<tbody class="tbody_respondent">
									<!-- SAMPLE/STATIC DATA -->

									<tr>
										<td> 
											<span> Juan Dela Cruz </span>
										</td>
										<td>
											<span> Address </span>
										</td>
										<td>
											<span> juandc@email.com </span>
										</td>
										<td>
											<span> 092312345678 </span>
										</td>
										<td> 
											<div class='table-actions'>
												<a href='javascript:;' class='edit delete_attendees'></a>
												<a href='javascript:;' class='delete delete_attendees'></a>
											</div>
										</td>
									</tr>

									<tr>
										<td>
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>
										<td> 
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>
										<td> 
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>
										<td> 
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>
										<td> 
											<div class='input-field table-actions'>
												<a href='javascript:;' class='save delete_attendees'></a>
												<a href='javascript:;' class='delete delete_attendees'></a>
											</div>
										</td>
									</tr>

									<tr class="tr_add_respondent">
										<td  colspan="6" style="text-align:right"> 
											<a href="javascript:;" class="btn waves-effect waves-light blue" id="add_respondent" name="add_respondent">ADD</a>
									 	</td>
									</tr>
								</tbody>	
							</table>
						</div>
					</div>

					<!-- RESPONDENT COUNSEL TABLE -->
					<div class="row">
						<div class="col s12">
							<h6>Legal Counsel/Authorized Representative</h6>
							<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered" id="respondent_counsel_table">
								<thead>
									<tr>
										<th width="10%">NAME</th>
										<th width="15%">BUSINESS ADDRESS</th>
										<th width="15%">EMAIL ADDRESS</th>
										<th width="10%">CONTACT NO.</th>
										<th width="5%"></th>
									</tr>
								</thead>
								<tbody class="tbody_respondent_counsel">
									<!-- SAMPLE/STATIC DATA -->

									<tr>
										<td> 
											<span> Juan Dela Cruz </span>
										</td>
										<td>
											<span> Address </span>
										</td>
										<td>
											<span> juandc@email.com </span>
										</td>
										<td>
											<span> 092312345678 </span>
										</td>
										<td> 
											<div class='table-actions'>
												<a href='javascript:;' class='edit delete_attendees'></a>
												<a href='javascript:;' class='delete delete_attendees'></a>
											</div>
										</td>
									</tr>

									<tr>
										<td>
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>
										<td> 
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>
										<td> 
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>
										<td> 
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>
										<td> 
											<div class='input-field table-actions'>
												<a href='javascript:;' class='save delete_attendees'></a>
												<a href='javascript:;' class='delete delete_attendees'></a>
											</div>
										</td>
									</tr>

									<tr class="tr_add_respondent_counsel">
										<td  colspan="6" style="text-align:right"> 
											<a href="javascript:;" class="btn waves-effect waves-light blue" id="add_respondent_counsel" name="add_respondent_counsel">ADD</a>
									 	</td>
									</tr>
								</tbody>	
							</table>
						</div>
					</div>
				</div>
			</div>

			<!-- ARBITRATOR CONTENT TAB -->
			<div id="arbitrator_tab" class="col s12">

				<div class="container">

					<div class="row">
						<div class="cols 3">
							<span style="font-weight:bold">MODE OF ARBITRATION: &nbsp;</span> <span> Tribunal </span>
						</div>
					</div>

					<div class="row">
						<div class="cols 3">
							<span style="font-weight:bold">NUMBER OF DISPUTANTS: &nbsp;</span> <span> 2 </span>
						</div>
					</div>

					<div class="row">

						<div class="input-field col s3">
							<label class="active">CASE ACTIVITY</label><br>						
							<select placeholder='Select Attendees'>
								<option value=""> Select Attendees</option>
								<option value="1"> Juan Dela Cruz</option>
								<option value="2"> Juan Dela Cruz</option>
								<option value="3"> Juan Dela Cruz</option>
								<option value="4"> Juan Dela Cruz</option>
								<option value="5"> Juan Dela Cruz</option>
								<option value="6"> Juan Dela Cruz</option>
								<option value="7"> Juan Dela Cruz</option>
							</select>
						</div>
						
						<div class="input-field col s4">
							<label class="label">&nbsp;</label>
							<input type="checkbox" class="labelauty" name="with_arbitrator_clause" id="with_arbitrator_clause" value="<?php echo ACTIVE ?>" data-labelauty="n/a|With Arbitrator Clause" checked/>
						</div>

					</div>
					
					<div class="row">
						<div class="col s12">
							<h6>Choice Of Arbitrator</h6>
						</div>
						<div class="col s12">
							<h6 style="font-size:11px;border-bottom:1px solid #a5a5a5;padding-bottom:5px;">Maximum of six (6) nominees (by order of preferences)</h6>
						</div>
					</div>

					<div class="row">

						<div class="input-field col s6">
							<label class="active">CLAIMANTS</label><br>						
							<select placeholder='Select Attendees' id="claimnants_select">
								<option value=""> Select Attendees</option>
								<option value="1"> Juan Dela Cruz</option>
								<option value="2"> Juan Dela Cruz</option>
								<option value="3"> Juan Dela Cruz</option>
								<option value="4"> Juan Dela Cruz</option>
								<option value="5"> Juan Dela Cruz</option>
								<option value="6"> Juan Dela Cruz</option>
								<option value="7"> Juan Dela Cruz</option>
							</select>
						</div>
						
						<div class="input-field col s6">
							<label class="active">RESPONDENTS</label><br>						
							<select placeholder='Select Attendees' id="respondents_select">
								<option value=""> Select Attendees</option>
								<option value="1"> Juan Dela Cruz</option>
								<option value="2"> Juan Dela Cruz</option>
								<option value="3"> Juan Dela Cruz</option>
								<option value="4"> Juan Dela Cruz</option>
								<option value="5"> Juan Dela Cruz</option>
								<option value="6"> Juan Dela Cruz</option>
								<option value="7"> Juan Dela Cruz</option>
							</select>
						</div>


						
					</div>

					<div class="row">

						<div class="col s6">
							<h6 style="font-weight:bold">Assigned Arbitrators</h6>
						</div>
						<div class="col s6 right-align">
							<br>
							<a href="javascript:;" class="view" > view arbitrator history</a>
						</div>
						
						
					</div>

					<!-- ASSIGNED ARBITRATOR TABLE -->
					<div class="row">
						<div class="col s12">
							<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered" id="claimant_table">
								
								<thead>

									<tr>
										<th width="10%">POSITION</th>
										<th width="15%">NAME</th>
										<th width="15%">SHARING FEE</th>
										<th width="5%"></th>
									</tr>
									
								</thead>

								<tbody class="tbody_claimant">

									<!-- SAMPLE/STATIC DATA -->

									<tr>
										<td> 
											<span> Chairman </span>
										</td>
										<td>
											<span> Arbitrator 1 </span>
										</td>
										<td>
											<span> 10 </span>
										</td>
										<td> 
											<div class='table-actions'>
												<a href='javascript:;' class='edit delete_attendees'></a>
												<a href='javascript:;' class='delete delete_attendees'></a>
											</div>
										</td>
									</tr>

									<tr>
										
										<td> 
											<span> Member1 </span>
										</td>

										<td>
											<select placeholder='Select Attendees'>
												<option value=""> Select Attendees</option>
												<option value="1"> Juan Dela Cruz</option>
												<option value="2"> Juan Dela Cruz</option>
												<option value="3"> Juan Dela Cruz</option>
												<option value="4"> Juan Dela Cruz</option>
												<option value="5"> Juan Dela Cruz</option>
												<option value="6"> Juan Dela Cruz</option>
												<option value="7"> Juan Dela Cruz</option>
											</select>
										</td>

										<td> 
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>

										<td> 
											<div class='input-field table-actions'>
												<a href='javascript:;' class='save delete_attendees'></a>
												<a href='javascript:;' class='delete delete_attendees'></a>
											</div>
										</td>

									</tr>

									<tr>
										
										<td> 
											<span> Member2 </span>
										</td>

										<td>
											<select placeholder='Select Attendees'>
												<option value=""> Select Attendees</option>
												<option value="1"> Juan Dela Cruz</option>
												<option value="2"> Juan Dela Cruz</option>
												<option value="3"> Juan Dela Cruz</option>
												<option value="4"> Juan Dela Cruz</option>
												<option value="5"> Juan Dela Cruz</option>
												<option value="6"> Juan Dela Cruz</option>
												<option value="7"> Juan Dela Cruz</option>
											</select>
										</td>

										<td> 
											<div class="row">
												<div class="input-field col s12">
									 				<input type="text" class="validate">
									 			</div>	
									 		</div>
										</td>

										<td> 
											<div class='input-field table-actions'>
												<a href='javascript:;' class='save delete_attendees'></a>
												<a href='javascript:;' class='delete delete_attendees'></a>
											</div>
										</td>

									</tr>

								</tbody>	
							</table>
						</div>
					</div>
					
				</div>
			</div>

			<!-- CASE STATUs CONTENT TAB -->
			<div id="case_status" class="col s12">
				<div class="container">
					
					<div class="row">
						<div class="col s6 offset-s6">
							<ul class="tabs" style="background-color:#b2dfdb !important;" >
							    <li class="tab col s2"><a href="#task_tab" style="color:black" class="active" >Task</a></li>
							    <li class="tab col s2"><a href="#remark_tab" style="color:black" >Remarks</a></li>
							    <li class="tab col s2"><a href="#docs_tab"  style="color:black" >Documents</a></li>
						  	</ul>
						</div>
					</div>
					
					<!-- TASK DIV CONTENT -->
					<div class="row" id="task_tab">
						<div class="col s12">
							<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="task_table">
								<thead>
									<tr>
										<th width="15%">TASK/ACTIVITY</th>
										<th width="15%">RESOURCE</th>
										<th width="15%">TAT</th>
										<th width="20%">STATUS</th>
										<th width="15%"></th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
					
					<!-- REMARKS DIV CONTENT -->
					<div class="row" id="remark_tab">
						<div class="col s12">
							REMARKS TAB
						</div>
					</div>

					<!-- DOCS DIV CONTENT -->
					<div class="row" id="docs_tab">
						<div class="row s12">
							DOCS TAB
						</div>
					</div>
				</div>
			</div>

			<!-- ORDER PAYMENT CONTENT TAB -->
			<div id="order_payment_tab" class="col s12">
				ORDER PAYMENT
			</div>
		</div>
	</div>
</form>
<script type="text/javascript">
	
	$(function() {
		
		$('ul.tabs').tabs();

		$('.input-field label').addClass('active');

		$("#nature_case").selectize({maxItems:null});
		$("#claimnants_select").selectize({maxItems:null});
		$("#respondents_select").selectize({maxItems:null});

		$("#case_form select").selectize();

		/*INITIALIZE DATATABLE*/
		load_datatable('task_table', '<?php echo PROJECT_ISCA ?>/isca_cases/task_table_data/');

		/*APPEAL TR ADDING*/
		$("#add_appeal").click(function(){

			var fields = "<tr>"+
							"<td>"+ 
								"<div class='input-field'>"+
						 			"<input type='text' class='datepicker' name='dates'>"+
						 		"</div>"+	
							"</td>"+

							"<td>"+
								"<select  placeholder='Select Attendees'>"+
									"<option value=''> Select Attendees</option>"+
									"<option value='1'> Juan Dela Cruz</option>"+
									"<option value='2'> Juan Dela Cruz</option>"+
									"<option value='3'> Juan Dela Cruz</option>"+
								"</select>"+
							"</td>"+

							"<td>"+
								"<select  placeholder='Select Attendees'>"+
									"<option value=''> Select Attendees</option>"+
									"<option value='1'> Juan Dela Cruz</option>"+
									"<option value='2'> Juan Dela Cruz</option>"+
									"<option value='3'> Juan Dela Cruz</option>"+
								"</select>"+
							"</td>"+


							"<td>"+
								"<select  placeholder='Select Attendees'>"+
									"<option value=''> Select Attendees</option>"+
									"<option value='1'> Juan Dela Cruz</option>"+
									"<option value='2'> Juan Dela Cruz</option>"+
									"<option value='3'> Juan Dela Cruz</option>"+
								"</select>"+
							"</td>"+

							"<td>"+
								"<div class='input-field col s12'>"+
									"<input type='text'>"+
								"</div>"+
							"</td>"+

							"<td>"+ 
								"<div class='input-field table-actions'>"+
									"<a href='javascript:;' class='save delete_attendees'></a>"+
									"<a href='javascript:;' class='delete delete_attendees'></a>"+
								"</div>"+
							"</td>"+

						"</tr>";
			
			$(".tbody_apeal").find('tr:last').before(fields);

			// re-initialize selectize
			$("#appeal_table select").selectize();

			// re-initailize date picker
		   	$('.datepicker').datetimepicker ({
				timepicker:false,
				scrollInput: false,
				format:'m/d/Y',
				formatDate:'m/d/Y'
			});
		});
		
		/*CLAIMANT TR ADDING*/
		$("#add_claimant").click(function(){

			var fields = "<tr>"+

							"<td>"+
								"<select  placeholder='Select Attendees'>"+
									"<option value=''> Select Attendees</option>"+
									"<option value='1'> Juan Dela Cruz</option>"+
									"<option value='2'> Juan Dela Cruz</option>"+
									"<option value='3'> Juan Dela Cruz</option>"+
								"</select>"+
							"</td>"+

							"<td>"+ 
								"<div class='row'>"+
									"<div class='input-field col s12'>"+
							 			"<input type='text' class='datepicker' name='dates'>"+
							 		"</div>"+	
						 		"</div>"+
							"</td>"+

							"<td>"+ 
								"<div class='row'>"+
									"<div class='input-field col s12'>"+
							 			"<input type='text' class='datepicker' name='dates'>"+
							 		"</div>"+	
						 		"</div>"+
							"</td>"+

							"<td>"+ 
								"<div class='row'>"+
									"<div class='input-field col s12'>"+
							 			"<input type='text' class='datepicker' name='dates'>"+
							 		"</div>"+	
						 		"</div>"+
							"</td>"+

							"<td>"+ 
								"<div class='input-field table-actions'>"+
									"<a href='javascript:;' class='save delete_attendees'></a>"+
									"<a href='javascript:;' class='delete delete_attendees'></a>"+
								"</div>"+
							"</td>"+

						"</tr>";
			
			$(".tbody_claimant").find('tr:last').before(fields);

			// re-initialize selectize
			$("#claimant_table select").selectize();
		});
	
		/*CLAIMANT COUNSEL TR ADDING*/
		$("#add_claimant_counsel").click(function(){

			var fields = "<tr>"+

							"<td>"+
								"<div class='row'>"+
									"<div class='input-field col s12'>"+
							 			"<input type='text' class='datepicker' name='dates'>"+
							 		"</div>"+	
						 		"</div>"+
							"</td>"+

							"<td>"+ 
								"<div class='row'>"+
									"<div class='input-field col s12'>"+
							 			"<input type='text' class='datepicker' name='dates'>"+
							 		"</div>"+	
						 		"</div>"+
							"</td>"+

							"<td>"+ 
								"<div class='row'>"+
									"<div class='input-field col s12'>"+
							 			"<input type='text' class='datepicker' name='dates'>"+
							 		"</div>"+	
						 		"</div>"+
							"</td>"+

							"<td>"+ 
								"<div class='row'>"+
									"<div class='input-field col s12'>"+
							 			"<input type='text' class='datepicker' name='dates'>"+
							 		"</div>"+	
						 		"</div>"+
							"</td>"+

							"<td>"+ 
								"<div class='input-field table-actions'>"+
									"<a href='javascript:;' class='save delete_attendees'></a>"+
									"<a href='javascript:;' class='delete delete_attendees'></a>"+
								"</div>"+
							"</td>"+

						"</tr>";
			
			$(".tbody_claimant_counsel").find('tr:last').before(fields);
		});

		/*CLAIMANT COUNSEL TR ADDING*/
		$("#add_respondent").click(function(){

			var fields = "<tr>"+

							"<td>"+
								"<div class='row'>"+
									"<div class='input-field col s12'>"+
							 			"<input type='text' class='datepicker' name='dates'>"+
							 		"</div>"+	
						 		"</div>"+
							"</td>"+

							"<td>"+ 
								"<div class='row'>"+
									"<div class='input-field col s12'>"+
							 			"<input type='text' class='datepicker' name='dates'>"+
							 		"</div>"+	
						 		"</div>"+
							"</td>"+

							"<td>"+ 
								"<div class='row'>"+
									"<div class='input-field col s12'>"+
							 			"<input type='text' class='datepicker' name='dates'>"+
							 		"</div>"+	
						 		"</div>"+
							"</td>"+

							"<td>"+ 
								"<div class='row'>"+
									"<div class='input-field col s12'>"+
							 			"<input type='text' class='datepicker' name='dates'>"+
							 		"</div>"+	
						 		"</div>"+
							"</td>"+

							"<td>"+ 
								"<div class='input-field table-actions'>"+
									"<a href='javascript:;' class='save delete_attendees'></a>"+
									"<a href='javascript:;' class='delete delete_attendees'></a>"+
								"</div>"+
							"</td>"+

						"</tr>";
			
			$(".tbody_respondent").find('tr:last').before(fields);
		});

		/*CLAIMANT COUNSEL TR ADDING*/
		$("#add_respondent_counsel").click(function(){

			var fields = "<tr>"+

							"<td>"+
								"<div class='row'>"+
									"<div class='input-field col s12'>"+
							 			"<input type='text' class='datepicker' name='dates'>"+
							 		"</div>"+	
						 		"</div>"+
							"</td>"+

							"<td>"+ 
								"<div class='row'>"+
									"<div class='input-field col s12'>"+
							 			"<input type='text' class='datepicker' name='dates'>"+
							 		"</div>"+	
						 		"</div>"+
							"</td>"+

							"<td>"+ 
								"<div class='row'>"+
									"<div class='input-field col s12'>"+
							 			"<input type='text' class='datepicker' name='dates'>"+
							 		"</div>"+	
						 		"</div>"+
							"</td>"+

							"<td>"+ 
								"<div class='row'>"+
									"<div class='input-field col s12'>"+
							 			"<input type='text' class='datepicker' name='dates'>"+
							 		"</div>"+	
						 		"</div>"+
							"</td>"+

							"<td>"+ 
								"<div class='input-field table-actions'>"+
									"<a href='javascript:;' class='save delete_attendees'></a>"+
									"<a href='javascript:;' class='delete delete_attendees'></a>"+
								"</div>"+
							"</td>"+

						"</tr>";
			
			$(".tbody_respondent_counsel").find('tr:last').before(fields);
		});
		
	})
</script>
 