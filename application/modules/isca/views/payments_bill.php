<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Payments to Arbitrators</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Payments to Arbitrators
		<span>Manage Payments to Arbitrators</span>
	  </h5>
	</div>
	<div class="col s6 right-align">
	  <div class="btn-group">
	    <button class="waves-effect waves-light" type="button" onclick="load_datatable('arbitrator_table','<?php echo PROJECT_ISCA?>/isca_payments_bill/get_payments_bill_list')"><i class="flaticon-arrows97"></i> Refresh</button>
	  </div>
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light btn-success" id="add_arbitrator" name="add_arbitrator" onclick="(location.href='<?php echo base_url('isca/isca_payments_bill/create_payment_bill');?>')">Create new</button>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable">&nbsp;</div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="arbitrator_table">
  <thead>
	<tr>
	  <th width="15%">CASE NO.</th>
	  <th width="15%">STAGE PROCEEDINGS</th>
	  <th width="15%">% OF FEE</th>
	  <th width="15%">STATUS</th>
	  <th width="10%"></th>
	</tr>
  </thead>
  </table>
</div>
