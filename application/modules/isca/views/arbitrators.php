<div class="page-title m-b-lg" >
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Arbitrators</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Arbitrators
		<span>Manage Arbitrators</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    <button class="waves-effect waves-light" type="button" onclick="load_datatable('arbitrator_table','<?php echo PROJECT_ISCA?>/isca_arbitrators/get_arbitrator_list')"><i class="flaticon-arrows97"></i> Refresh</button>
	  </div>
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_arbitrator" id="add_arbitrator" name="add_arbitrator" onclick="modal_init()">Add New Arbitrator</button>
	  </div>
	</div>
  </div>
</div>
<div class="pre-datatable">&nbsp;</div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="arbitrator_table">
  <thead>
	<tr>
	  <th width="20%">NAME</th>
	  <th width="20%">AREA</th>
	  <th width="20%">POSITION(S)</th>
	  <th width="15%">SOLE</th>
	  <th width="15%">STATUS</th>
	  <th width="10%" class="text-center">Action</th>
	</tr>
  </thead>
  </table>
</div>

<script>
	var deleteObj = new handleData({ 
		controller 	: 'isca_arbitrators', 
		method 		: 'delete_arbitrator', 
		module		: '<?php echo PROJECT_ISCA ?>' 
	});
</script>

