
<form id="modal_soa">
	<input type="hidden" name="security" value="<?php echo $security ?>">

  	<div class="row">
        <div class="input-field col s12">
			<label class="active">CASE</label>
			<br>
			<select id="profession"  name="profession" class="selectize" >
				<option value="" disabled selected>--Select Case--</option>
				<option value="1">Option 1</option>
				<option value="2">Option 2</option>
				<option value="3">Option 3</option>
			</select>
		</div>
	</div>

	<div class="row">
		<div class="input-field col s5">
			<label class="active">TYPE OF PAYMENT</label>
			<br>
			<select id="region"  name="region" class="selectize">
				<option value="1">Option 1</option>
				<option value="2">Option 2</option>
				<option value="3">Option 3</option>
			</select>
		</div>
	</div>

	<div class="row">
		<div class="col s12">
			<h6 class="font-bold"> ARBITRATION FEE </h6>
			<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered ttbl">
				<thead>
					<tr>
						<th class="center-align">FEES</th>
						<th class="center-align">AMOUNT (Php)</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Filling Fee</td>
						<td class="center-align"> 10,000 </td>
					</tr>
					<tr>
						<td>Administrtive Fee</td>
						<td class="center-align"> 10,000 </td>
					</tr>
					<tr>
						<td>Arbitrator's Fee</td>
						<td class="center-align"> 10,000 </td>
					</tr>
					<tr>
						<td>Arbitration Development Fund Fee</td>
						<td class="center-align"> 10,000 </td>
					</tr>
					<tr >
						<td class="font-bold">Total</td>
						<td class="center-align"> 10,000 </td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<br><br>
			<h6 class="font-bold"> BREAK DOWN OF FEES </h6>
			<table cellpadding="0" cellspacing="0" class="table  table-default table-layout-auto bordered ttbl">
				<thead>
					<tr>
						<th class="center-align">FEES</th>
						<th class="center-align">UPON TOR SIGNING</th>
						<th class="center-align">BEFORE FINAL HEARING</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>Filling Fee</td>
						<td class="center-align"> 10,000 </td>
						<td class="center-align"> 10,000 </td>
					</tr>
					<tr>
						<td>Administrtive Fee</td>
						<td class="center-align"> 10,000 </td>
						<td class="center-align"> 10,000 </td>
					</tr>
					<tr>
						<td>Arbitrator's Fee</td>
						<td class="center-align"> 10,000 </td>
						<td class="center-align"> 10,000 </td>
					</tr>
					<tr>
						<td>Arbitration Development Fund Fee</td>
						<td class="center-align"> 10,000 </td>
						<td class="center-align"> 10,000 </td>
					</tr>
					<tr >
						<td class="font-bold">Total</td>
						<td class="center-align"> 10,000 </td>
						<td class="center-align"> 10,000 </td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<br><br>
			<h6 class="font-bold"> DEPOSIT </h6>
			<table cellpadding="0" cellspacing="0" class="table  table-default table-layout-auto bordered ttbl">
				<thead>
					<tr>
						<th class="center-align">OR NUMBER</th>
						<th class="center-align">OR DATE</th>
						<th class="center-align">AMOUNT PAID</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>0001</td>
						<td class="center-align"> 06/23/2016 </td>
						<td class="center-align"> 10,000 </td>
					</tr>
					<tr>
						<td>0002</td>
						<td class="center-align"> 06/23/2016 </td>
						<td class="center-align"> 10,000 </td>
					</tr>
					<tr>
						<td>0001</td>
						<td class="center-align"> 06/23/2016 </td>
						<td class="center-align"> 10,000 </td>
					</tr>
					
					<tr >
						<td class="font-bold"></td>
						<td class="center-align font-bold"> Total </td>
						<td class="center-align"> 10,000 </td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
	<div class="md-footer default">
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light btn-secondary pull-left" id="print_soa" >PRINT SOA</button>
	  <?php //endif; ?>
	  <a class="waves-effect waves-teal btn-flat" id="cancel_position">BACK</a>
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light btn-primary" id="save_draft" >SAVE AS DRAFT</button>
	  <?php //endif; ?>
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light btn-primary" id="save_final" >SAVE AS FINAL</button>
	  <?php //endif; ?>
	</div>
</form>
<style type="text/css">
.ttbl tbody>tr>td, .ttbl thead>tr>th {
	border:1px solid #A4ADB4 !important;
}
</style>
<script>
$(function() {

  //help_text("role_form");

  // materialize select only the select element inside form
  /*$('#arbitrator_form select').material_select();*/

  $('#arbitrator_form').parsley();
  
  $('#arbitrator_form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {
	  var data = $(this).serialize();
	  
	  button_loader('save_position', 1);
	  $.post("<?php echo base_url() . PROJECT_ISCA ?>/isca_arbitrators/process/", data, function(result) {
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  button_loader('save_position', 0);
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  button_loader("save_position",0);
		  modalObj.closeModal();
		  
		  load_datatable('arbitrator_table', '<?php echo PROJECT_ISCA ?>/isca_arbitrators/get_arbitrator_list/');
		}
	  }, 'json');       
    }
  });
  
  <?php if(!EMPTY($id)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>


	$("#cancel_position").on("click", function(){
		modalObj.closeModal();
	});
})
</script>