<form id="catalog_form">
	<input type="hidden" name="security" value="<?php echo $security ?>">

	<!-- CONTROL NO FIELDS -->
	<div class="row">
    
        <div class="input-field col s12">
			<h6> CONTROL NO. <span>02</span> </h6>
        </div>
  	
  	</div>

	<!-- MODE FIELDS -->
	<div class="row">

		<div class="input-field col s6">

			<label for="delivery_mode" class="active">MODE OF DELIVERY</label>
			<br>
			<select id="delivery_mode"  name="delivery_mode" class="selectize">
				<option value="" disabled selected>--Select Mode Of Delivery--</option>
				<option value="1">Option 1</option>
				<option value="2">Option 2</option>
				<option value="3">Option 3</option>
			</select>

        </div>

		<div class="input-field col s6">

			<label for="title">MESSENGER</label>
			<input type="text" class="validate" id="title" name="title" >

		</div>

	</div>

  	<!-- STATUS FIELD -->
	<div class="row">
	
		<div class="input-field col s5">
	
			<input type="text" class="datepicker">
			<label>DATE OF DELIVERY</label>
	
		</div>
	
		<div class="input-field col s7">
	
			<label for="delivery_status" class="active">STATUS OF DELIVERY</label>
			<br>
			<select id="delivery_status"  name="delivery_status" class="selectize">
				<option value="" disabled selected>--Select Status Of Delivery--</option>
				<option value="1">Option 1</option>
				<option value="2">Option 2</option>
				<option value="3">Option 3</option>
			</select>
    
        </div>
	
	</div>

	<!-- ADDRESSEE FIELD -->
	<div class="row">
	
		<div class="input-field col s12">
	
			<label> ADDRESSEE </label>
			<input type="text" id="addressee" name="addressee" class="validate">
	
		</div>
	
	</div>

	<!-- CASE FIELDS -->
	<div class="row">
	
		<div class="input-field col s4">
	
			<label> CASE NO. </label>
			<input type="text" id="case_no" name="case_no" class="validate">
	
		</div>
	
		<div class="input-field col s8">
	
			<label> SUBJECT </label>
			<input type="text" id="subject" name="subject" class="validate">
	
		</div>
	
	</div>

	<!-- REMARKS -->
	<div class="row">
	
		<div class="input-field col s12">
    
          <label style="padding-bottom:15px;">REMARKS</label>
          <textarea id="contents" style="margin-top:20px"> </textarea>
    
        </div>
	
	</div>


	<div class="md-footer default">
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light" id="save_position" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  <?php //endif; ?>
	  <a class="waves-effect waves-teal btn-flat" id="cancel_position">Cancel</a>
	</div>

</form>

<script>

$(function() {

  $('#catalog_form').parsley();

  $('#catalog_form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {
	  var data = $(this).serialize();
	  
	  button_loader('save_position', 1);
	  $.post("<?php echo base_url() . PROJECT_CEIS ?>/ceis_positions/process/", data, function(result) {
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  button_loader('save_position', 0);
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  button_loader("save_position",0);
		  modalObj.closeModal();
		  
		  load_datatable('position_table', '<?php echo PROJECT_CEIS ?>/ceis_positions/get_position_list/');
		}
	  }, 'json');       
    }
  });
  

	$("#cancel_position").on("click", function(){
		modalObj.closeModal();
	});

	CKEDITOR.replace('contents');

  <?php if(EMPTY($catalog_id)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>

});
</script>