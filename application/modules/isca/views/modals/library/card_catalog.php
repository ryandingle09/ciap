<form id="catalog_form">
	<input type="hidden" name="security" value="<?php echo $security ?>">

	<!-- CODE FIELDS -->
	<div class="row">
        <div class="col s12">
	        <div class="input-field col s6">
	          <label for="code_no">CODE NO.</label>
	          <input id="code_no" name="code_no" type="text" class="validate" value="<?php echo (!empty($code_no)) ? $code_no : '';?>">
	       
	        </div>

	        
	        <div class="input-field col s6">
				<label>CLASS NO.</label>
				<input id="class_no" name="class_no" type="text" class="validate" value="<?php echo (!is_null($book_class_no)) ? $book_class_no : '';?>"> 
	        </div>

  		</div>
  	</div>

  	<!-- TITLE FIELDS -->
  	<div class="row">
        <div class="col s12">
	        <div class="input-field col s12">
			
				<label for="title">TITLE</label>
				<input id="title" name="title" type="text" class="validate" value="<?php echo (!empty($title)) ? $title : '';?>">
		
			</div>
		</div>
	</div>

	<!-- AUTHOR FIELDS -->
	<div class="row">
		<div class="col s12">
			<div class="input-field col s12">
		
				<label for="book_author">AUTHOR</label>
				<br><br>
				<select id="book_author"  name="book_author" class="selectize">
					<option value="" disabled selected>--Select Author--</option>
					<?php foreach ($authors as $author ) { ?>
						<option value="<?php echo $author['book_author_id']; ?>" <?php echo ($book_author == $author['book_author_id']) ? "selected" : ''?>> <?php echo $author['book_author_name']; ?> </option>
					<?php } ?>
				</select>
		
			</div>
		</div>
	</div>


	<!-- PUBLISH FIELD -->
	
	<div class="row">
		<div class="col s12">
			<div class="input-field col s12">
		
				<label for="publishing_house">PUBLISHING HOUSE</label>
				<input id="publishing_house" name="publishing_house" type="text" class="validate" value="<?php echo (!empty($publishing_house)) ? $publishing_house : '';?>">
		
			</div>
		</div>
	</div>


	<!-- YEAR PUBLISHED FIELD -->
	<div class="row">
		<div class="col s12">
			<div class="input-field col s6">
		
				<label for="publishing_date">DATE PUBLISHED</label>
				<input type="text" class="datepicker" id="publishing_date" name="publishing_date" value="<?php echo (!empty($publishing_date)) ? $publishing_date : '';?>">
		
			</div>
		
			<div class="input-field col s6">
		
				<label for="copyright_date">COPYRIGHT DATE</label>
				<input type="text" class="datepicker" id="copyright_date" name="copyright_date" value="<?php echo (!empty($copyright_date)) ? $copyright_date : '';?>">
		
			</div>
		</div>
	</div>

	<!-- EDITION FIELD -->
	<div class="row">
		<div class="col s12">
			<div class="input-field col s6">
	    
	          <label for="edition">EDITION</label>
	          <input id="edition" name="edition" type="text" class="validate" value="<?php echo (!empty($edition)) ? $edition : '';?>">
	    
	        </div>
	    
	        <div class="input-field col s6">
	    
	          <label for="quantity">NUMBER OF COPIES</label>
	          <input id="quantity" name="quantity" type="text" class="validate" value="<?php echo (!empty($quantity)) ? $quantity : '';?>">
	    
	        </div>
		</div>
	</div>	
	
	<!-- TABLE OF CONTENTS -->
	<div class="row">
		<div class="col s12">
			<div class="input-field col s12">
	    
	          <label>TABLE CONTENTS</label>
	          <br><br>
	          <textarea id="contents" name="content_book" style="margin-top:20px" value="<?php echo (!empty($content_book)) ? $content_book : '';?>" > <?php echo (!empty($content_book)) ? $content_book : '';?> </textarea >
	    
	        </div>
		</div>
	</div>


	<div class="md-footer default">
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light" id="save_position" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  <?php //endif; ?>
	  <a class="waves-effect waves-teal btn-flat" id="cancel_position">Cancel</a>
	</div>
	
</form>


<script>

$(function() {

  //help_text("role_form");

  $('#catalog_form').parsley();

  $('#catalog_form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {

		// update text are content
		for (instance in CKEDITOR.instances) {

			CKEDITOR.instances[instance].updateElement();
		}

		var data = $(this).serialize();
	  
		button_loader('save_position', 1);

		$.post("<?php echo base_url() . PROJECT_ISCA ?>/isca_books/process/", data, function(result) {

			if(result.flag == 0) {

				notification_msg("<?php echo ERROR ?>", result.msg);
				button_loader('save_position', 0);

			} else {

				notification_msg("<?php echo SUCCESS ?>", result.msg);
				button_loader("save_position", 0);
				modalObj.closeModal();

				load_datatable('books_table', '<?php echo PROJECT_ISCA ?>/isca_books/get_catalog_list/');
			}

		}, 'json');       
    }
  });

  	$("#cancel_position").on("click", function(){
		modalObj.closeModal();
	});

	CKEDITOR.replace('contents');

  <?php if(!EMPTY($book_id)) { ?>
	$('.input-field label').addClass('active');
  <?php } ?>

})
</script>