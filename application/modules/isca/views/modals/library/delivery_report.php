<form id="catalog_form">
	<input type="hidden" name="security" value="<?php echo $security ?>">

	<!-- CONTROL NO FIELDS -->
	<div class="row">
    
        <div class="input-field col s5">

			<input type="text" class="datepicker">
			<label>DATE</label>

        </div>
  	
  	</div>


  	<!-- STATUS FIELD -->
	<div class="row">
	
		<div class="input-field col s12">
	
			<input type="text" class="validate">
			<label>TITLE</label>
	
		</div>
	
	</div>

	
	<div class="md-footer default">
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light" id="save_position" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  <?php //endif; ?>
	  <a class="waves-effect waves-teal btn-flat" id="cancel_position">Cancel</a>
	</div>

</form>

<script>

$(function() {

  //help_text("role_form");

  // materialize select only the select element inside form
  $('#catalog_form select').material_select();

  $('#catalog_form').parsley();

  $('#catalog_form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {
	  var data = $(this).serialize();
	  
	  button_loader('save_position', 1);
	  $.post("<?php echo base_url() . PROJECT_CEIS ?>/ceis_positions/process/", data, function(result) {
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  button_loader('save_position', 0);
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  button_loader("save_position",0);
		  modalObj.closeModal();
		  
		  load_datatable('position_table', '<?php echo PROJECT_CEIS ?>/ceis_positions/get_position_list/');
		}
	  }, 'json');       
    }
  });
  

	$("#cancel_position").on("click", function(){
		modalObj.closeModal();
	});
	
  <?php if(EMPTY($catalog_id)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>

});
</script>