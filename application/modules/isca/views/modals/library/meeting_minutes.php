<form id="minutes_form">
	<input type="hidden" name="security" value="<?php echo $security ?>">

	<!-- STATUS FIELDS -->
	<div class="row">
		<div class="col s12">
			<?php if ($action_id == ACTION_VIEW) { ?>
				<div class="col s6">

					<h6> Status: <strong> <?php echo ($status_flag == '0') ? 'Final' : 'Draft';?> </strong></h6>
				</h6>
			<?php } else { ?>
				<div class="input-field col s6">

					<br><br>
					<label class="label m-t-n-xs block">STATUS</label>
					<select id="status" name="status_flag" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="" disabled selected> --Select Status-- </option>
						<option value="0" <?php echo ($status_flag == '0') ? 'selected' : '';?> >Final</option>
						<option value="1" <?php echo ($status_flag == '1') ? 'selected' : '';?> >Draft</option>
					</select>
				</div>
			<?php } ?>
		</div>
	</div>


	<!-- MEETING FIELDS -->
	<div class="row">
		<div class="col s12">
			<?php if ($action_id == ACTION_VIEW) { ?>
        		<?php foreach ($meeting_list as $meeting) { ?>
					<div class="col s6">

						 <?php echo ($meeting_id == $meeting['meeting_id']) ? "<h6> Board Meeting: <strong>". $meeting["meeting_name"] ."</strong> </h6>" : '';?> 
					</div>
				<?php } ?>
        	<?php } else { ?>
        		<div class="input-field col s12">
					<label class="label m-t-n-xs block">BOARD MEETING</label>
					<select id="meeting_id"  name="meeting_id" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="" disabled selected> --Select Board Meeting-- </option>

						<?php foreach ($meeting_list as $meeting) { ?>

							<option value="<?php echo $meeting['meeting_id'];?>" <?php echo ($meeting_id == $meeting['meeting_id']) ? 'selected' : '';?> ><?php echo $meeting['meeting_name'];?></option>

						<?php } ?>
					</select>
        		</div>
			<?php } ?>
	    </div>
  	</div>
  

  	<!-- TIME FIELDS -->
  	<div class="row" id="timer">
  		<div class="col s12">
  			<?php if($action_id == ACTION_VIEW) {?>
  				<div class="col s6">
  					<h6>Time Started: <strong><?php echo $time_started ?> </strong> </h6>
  				</div>
  				<div class="col s6">
  					<h6>Time Ended: <strong><?php echo $time_ended ?> </strong> </h6>
  				</div>
  			<?php } else { ?>
			        <div class="input-field col s6">

						<label>TIME STARTED</label>
						<input id="time_start" name="time_started" type="text" class="validate timepicker" value="<?php echo (!empty($time_started)) ? $time_started : '';?> ">

					</div>

					<div class="input-field col s6">

						<label>TIME ENDED</label>
						<input id="time_end" name="time_ended" type="text" class="validate timepicker" value="<?php echo (!empty($time_ended)) ? $time_ended : '';?> ">

					</div>
			<?php } ?>
		</div>
	</div>

	
	<!-- Attendees FIELD -->
	<div class="row">
		<div class="col s12">

			<div class="input-field col s10">

				<label class="label m-t-n-xs block">ATTENDEES</label>
				<select id="attendees_select"  name="attendees_id[]" class="selectize" placeholder="Select Attendees" multiple data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">

					<option value="" disabled selected>--Select Attendees--</option>

					<?php foreach ($attendees_list as $attendee) { ?>

						<option value="<?php echo $attendee['attendee_id'];?>" <?php echo (in_array($attendee['attendee_id'], $attended_id)) ? "selected" :'';?> > <?php echo $attendee['attendee_name'];?> </option>

					<?php }?>

				</select>


			</div>

			<div class=" input-field col s1">

			 	<a type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="manage_attendees" id="add_attendees" name="add_attendees" onclick="manage_attendees_init()"><i class="flaticon-arrows97"></i></a>
			
			</div>

			
		</div>
	</div>
	<br>
	
	<!-- MINUTES CONTENTS -->
	<div class="row">
		<div class="col s12">
			<div class="input-field col s12">

	          <label style="padding-bottom:15px;">MINUTES OF MEETING</label>
	          <textarea id="contents" name="minutes_data" style="margin-top:20px" value="<?php echo (!empty($minutes_data)) ? $minutes_data : '' ;?>"><?php echo (!empty($minutes_data)) ? $minutes_data : '' ;?> </textarea>

	        </div>
        </div>
	</div>


	<div class="md-footer default">
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light" id="save_position" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  <?php //endif; ?>
	  <a class="waves-effect waves-teal btn-flat" id="cancel_position">Cancel</a>
	</div>
</form>

<script>

$(function() {

	//help_text("role_form");


	/* $('#attendees_select').selectize({maxItems:null});
	$('#minutes_form select').selectize();*/

	$('#minutes_form').parsley();

	$('#minutes_form').submit(function(e) {
		e.preventDefault();

		if ( $(this).parsley().isValid() ) {

			for (instance in CKEDITOR.instances) {

				CKEDITOR.instances[instance].updateElement();
			}

			var data = $(this).serialize();
			
			button_loader('save_position', 1);

			$.post("<?php echo base_url() . PROJECT_ISCA ?>/isca_board_minutes/process/", data, function(result) {

				if(result.flag == 0) {

					notification_msg("<?php echo ERROR ?>", result.msg);
					button_loader('save_position', 0);

				} else {

					notification_msg("<?php echo SUCCESS ?>", result.msg);
					button_loader("save_position",0);
					modalObj.closeModal();

					load_datatable('minutes_table', '<?php echo PROJECT_ISCA ?>/isca_board_minutes/get_minutes_meeting_list/');

				}

			}, 'json');       
		}
	});


	$("#cancel_position").on("click", function() {
	modalObj.closeModal();
	});

	CKEDITOR.replace('contents');

	<?php if(EMPTY($catalog_id)){ ?>
	$('.input-field label').addClass('active');
	<?php } ?>

})
</script>