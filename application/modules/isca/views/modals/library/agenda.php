<form id="agenda_form">
	<input type="hidden" name="security" value="<?php echo $security ?>">

	<!-- DATE FIELD -->
	<div class="row">
		<div class="col s12">
			<div class="input-field col s5">
			
				<input type="text" class="datepicker" name="meeting_date" value="<?php  echo (!empty($meeting_date)) ? $meeting_date : ''; ?>">
				<label>DATE</label>
			
			</div>
		</div>
	</div>

  	<!-- NAME OF MEETING FIELDS -->
  	<div class="row">
    	<div class="col s12">
	        <div class="input-field col s12">
		
				<input id="name_meeting" type="text" class="validate" name="meeting_name" value="<?php  echo (!empty($meeting_name)) ? $meeting_name : ''; ?>">
				<label>NAME OF MEETING</label>
		
			</div>
		</div>
	</div>

	<!-- VENUE FIELDS -->
	<div class="row">
		<div class="col s12">
			<div class="input-field col s12">
		
				<input id="venue" type="text" class="validate" name="venue" value="<?php  echo (!empty($venue)) ? $venue : ''; ?>">
				<label>VENUE</label>
		
			</div>
		</div>
	</div>
	
	<!-- ANGENDA -->
	<div class="row">
		<div class="col s12">
			<div class="input-field col s12">
	    
	          <label style="padding-bottom:15px;">AGENDA</label>
	          <textarea id="contents" style="margin-top:20px" name="agenda_data" value="<?php  echo (!empty($agenda_data)) ? $agenda_data : ''; ?>"> <?php echo (!empty($agenda_data)) ? $agenda_data : '';?> </textarea>
	    
	        </div>
		</div>
	</div>


	<div class="md-footer default">
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light" id="save_position" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  <?php //endif; ?>
	  <a class="waves-effect waves-teal btn-flat" id="cancel_position">Cancel</a>
	</div>
</form>


<script>

$(function() {

  //help_text("role_form");

  // materialize select only the select element inside form
  $('#agenda_form select').material_select();

  $('#agenda_form').parsley();

  $('#agenda_form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {

		// update text are content
		for (instance in CKEDITOR.instances) {

			CKEDITOR.instances[instance].updateElement();
		}
		var data = $(this).serialize();

		button_loader('save_position', 1);

		$.post("<?php echo base_url() . PROJECT_ISCA ?>/isca_board_meetings/process/", data, function(result) {
			
			if(result.flag == 0) {

			notification_msg("<?php echo ERROR ?>", result.msg);
			button_loader('save_position', 0);

		} else {

			notification_msg("<?php echo SUCCESS ?>", result.msg);
			button_loader("save_position",0);
			modalObj.closeModal();

			load_datatable('meeting_agenda', '<?php echo PROJECT_ISCA ?>/isca_board_meetings/get_agenda_list/');

		}

	  }, 'json');       
    }
  });
  
  <?php if(!EMPTY($meeting_id)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>
  
  $("#cancel_position").on("click", function(){
		modalObj.closeModal();
	});

  // replace text area 
  CKEDITOR.replace('contents');
})

</script>