<form id="attendee_form">
	<div class="row">
	  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="attendees_table">
		  <thead>
			<tr>
			  <th width="20%">NAME</th>
			  <th width="20%">DESIGNATION</th>
			  <th width="5%" class="text-center">Action</th>
			</tr>
		  </thead>
	  </table>
	  <div class="col s3 offset-s9 right-align">
	  	<br>
	  	<a href="javascript:;" class="btn waves-effect waves-teal blue" id="add_attendee_name">ADD</a>
	  </div>
	</div>
	<div class="md-footer default">
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light" id="save_position" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  <?php //endif; ?>
	  <a class="waves-effect waves-teal btn-flat cancel_modal" id="cancel_position">Cancel</a>
	</div>

</form>

<script>

var deleteObj = new handleData({ 
	controller 	: 'isca_board_minutes', 
	method 		: 'delete_attendee', 
	module		: '<?php echo PROJECT_ISCA ?>' 
});
 
$(function() {

	//help_text("role_form");

	// materialize select only the select element inside form
	$('#attendee_form select').material_select();

	$('#attendee_form').parsley();

	$('#attendee_form').submit(function(e) {
		e.preventDefault();

		if ( $(this).parsley().isValid() ) {
			var data = $(this).serialize();

			button_loader('save_position', 1);

			$.post("<?php echo base_url() . PROJECT_CEIS ?>/isca_board_minutes/process/", data, function(result) {

				if(result.flag == 0){

					notification_msg("<?php echo ERROR ?>", result.msg);
					button_loader('save_position', 0);
				} else {

					notification_msg("<?php echo SUCCESS ?>", result.msg);
					button_loader("save_position",0);
					manage_attendees.closeModal();

					load_datatable('attendees_table', '<?php echo PROJECT_ISCA ?>/isca_board_minutes/get_attendees_list/?>');
				}

			}, 'json');   
		}
	});

	var counter = 0;

	$("#add_attendee_name").click(function() {
		var id_name 		= 'attendee_'+counter;
		var id_position		= 'position_'+counter;

		var fields = "<tr>"+

			"<td>"+

				"<div class='input-field'>"+
					"<input type='text' id= '"+id_name+"' >"+
				"</div>"+

			"</td>"+

			"<td>"+

				"<div class='input-field'>"+
					"<input type='text' id= '"+id_position+"'>"+
				"</div>"+

			"</td>"+

			"<td>"+

				"<div class='table-actions'>"+
						"<br><br>"+
						"<a href='javascript:;' class='save save_data' id='"+counter+"'> </a>"+
						"<a href='javascript:;' class='delete delete_data'> </a>"+
				"</div>"+

			"</td>"+

		"</tr>";

		$("#attendees_table").find("tr:last").after(fields);

		counter+=1;
	});

	$("#attendees_table").on('click' , '.save_data', (function() {

		var id	 = $(this, 'a').attr("id");

		var attendee_name = $("#attendee_"+id).val();
		var attendee_position = $("#position_"+id).val();

		var data = {
			attendee_name : attendee_name,
			attendee_position : attendee_position
		};

		$.post("<?php echo base_url() . PROJECT_ISCA ?>/isca_board_minutes/process_attendees/", data, function(result) {

			if(result.flag == 0){

			  notification_msg("<?php echo ERROR ?>", result.msg);
			} else {

			  notification_msg("<?php echo SUCCESS ?>", result.msg);
			  
			  load_datatable('attendees_table', '<?php echo PROJECT_ISCA ?>/isca_board_minutes/get_attendees_list/?>');
			}
			
	  }, 'json');

	}));

	$("#cancel_position").on("click", function(){
	
		modalObj.closeModal();
	});

	<?php if(EMPTY($catalog_id)){ ?>

		$('.input-field label').addClass('active');
	<?php } ?>

});
</script>

