<form id="mace_form">
	<input type="hidden" name="security" value="<?php echo $security ?>">

	<!-- DATE/TIME FIELDS -->
	<div class="row">
		<div class="col s12">
			<div class="input-field col s6">
				<label>DATE</label>
				<input type="text" class="datepicker">
			</div>
			<div class="input-field col s3">
				<input id="time_start" type="text" class="validate timepicker">
				<label>FROM</label>
			</div>
			<div class="input-field col s3">
				<input id="time_end" type="text" class="validate timepicker">
				<label>TO</label>
			</div>
		</div>
	</div>


	<!-- VENUE FIELDS -->
	<div class="row">
		<div class="col s12">
	        <div class="input-field col s12">
				<label>VENUE</label>
				<input type="text">
	        </div>
    	</div>
  	</div>
	
	<!-- TRAINER/S FIELD -->
	<div class="row">
		<div class="col s12">
			<div class="input-field col s12">
				<label>TRAINER/S</label>
				<input type="text">
			</div>
		</div>
	</div>

	
	<!-- REMARKS CONTENTS -->
	<div class="row">
		<div class="col s12">
			<div class="input-field col s12">
	          <label style="padding-bottom:15px;">REMARKS</label>
	          <textarea id="contents" style="margin-top:20px; height:200px;	"> </textarea>
	        </div>
    	</div>
	</div>

	<!-- ARBITRATOR ATTENDEES TABLE -->
	<div class="row">
		<div class="col s12">
			<div class="col s6">
				<br>
				<h6 style="font-weight:bold"> ARBITRATORS ATTENDED </h6>
			</div>
			<div class="col s3 offset-s3 right-align">
				<a href="javascript:;" class="btn waves-effect waves-light blue" id="add_attendess" name="add_attendess" type="submit" value="add">ADD</a>
			</div>
			<div class="col s12">
				<br>
	    		<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered" id="attendees_table">
					<thead>
						<tr>
							<th class="center-align" width="15%">ARBITRATOR</th>
							<th class="center-align" width="30%">TIME IN</th>
							<th class="center-align" width="30%">TIME OUT</th>
							<th class="center-align" width="10%">PARTICIPATION HRS</th>
							<th class="center-align" width="10%">LECTURE HRS</th>
							<th class="center-align" width="5%"></th>
						</tr>
					</thead>
					<tbody class="tbody_attendees">
						<tr>
							<td>
								<select>
									<option value="1">Option 1</option>
									<option value="2">Option 2</option>
									<option value="3">Option 3</option>
									<option value="4">Option 4</option>
									<option value="5">Option 5</option>
								</select>
							</td>

							<td> 
								<div class='input-field'>
									<input type="text" class="validate timepicker" name="time_start"/> 
								</div>

							</td>

							<td>

								<div class='input-field'>
									<input type="text" class="validate timepicker" name="time_end"/> 
								</div>

							</td>

							<td>

								<div class='input-field'>
									<input type="text"  /> 
								</div>

							</td>
							<td>

								<div class='input-field'>
									<input type="text"  /> 
								</div>

							</td>

							<td>
								<div class='table-actions'>
									<br>
									<a href='javascript:;' class='delete delete_attendees'></a>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>


	<div class="md-footer default">
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light" id="save_position" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  <?php //endif; ?>
	  <a class="waves-effect waves-teal btn-flat" id="cancel_position">Cancel</a>
	</div>
</form>

<script>

$(function() {

  //help_text("role_form");

  // materialize select only the select element inside form
  $('#mace_form select').selectize();


  $('#mace_form').parsley();

  $('#mace_form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {
	  var data = $(this).serialize();
	  
	  button_loader('save_position', 1);
	  $.post("<?php echo base_url() . PROJECT_CEIS ?>/ceis_positions/process/", data, function(result) {
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  button_loader('save_position', 0);
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  button_loader("save_position",0);
		  modalObj.closeModal();
		  
		  load_datatable('position_table', '<?php echo PROJECT_CEIS ?>/ceis_positions/get_position_list/');
		}
	  }, 'json');       
    }
  });
  

	$("#cancel_position").on("click", function(){
		modalObj.closeModal();
	});


  <?php if(EMPTY($catalog_id)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>

	$("#add_attendess").click(function() {

		var fields = "<tr>"+
						"<td>"+
							"<select>"+
								"<option value='1'>Option 1</option>"+
								"<option value='2'>Option 2</option>"+
								"<option value='3'>Option 3</option>"+
								"<option value='4'>Option 4</option>"+
								"<option value='5'>Option 5</option>"+
							"</select>"+
						"</td>"+

						"<td>"+ 

							"<div class='input-field'>"+
								"<input type='text' class='validate timepicker' name='time_start'/> "+
							"</div>"+

						"</td>"+

						"<td>"+

							"<div class='input-field'>"+
								"<input type='text' class='validate timepicker' name='time_end'/> "+
							"</div>"+

						"</td>"+

						"<td>"+

							"<div class='input-field'>"+
								"<input type='text'  />"+
							"</div>"+

						"</td>"+

						"<td>"+

							"<div class='input-field'>"+
								"<input type='text'  />"+
							"</div>"+

						"</td>"+

						"<td>"+

							"<div class='table-actions'>"+
								"<br>"+
								"<a href='javascript:;' class='delete delete_attendees'> </a>"+
							"</div>"+

						"</td>"+

					"</tr>";
		$('.tbody_attendees').find('tr:last').after(fields);
		
		// RE INITIALIZE SELECT
		$('#mace_form select').selectize();

		//re initialize timepicker

		$('.timepicker').datetimepicker({
			datepicker:false,
			format:'h:i A'
		  });

	}); 
	
	// delete amount tr
		$("#attendees_table").on('click' , '.delete_attendees', (function() {
			
			var tr_count = $('#attendees_table>tbody>tr').length;
			/*if( tr_count > 1) {*/
				$(this).closest('tr').remove();
			/*} else {
				alert("Content cannot be empty.");
			}*/

		}));

})
</script>