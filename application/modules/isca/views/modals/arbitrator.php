<form id="arbitrator_form">
	<input type="hidden" name="security" value="<?php echo $security ?>">

	<!-- NAME FIELDS -->
	<div class="row">
		<div class="col s12">
	        <div class="input-field col s5">
	          <input id="first_name" name="first_name" type="text" class="validate" value="<?php echo(!EMPTY($first_name)) ? $first_name : ''; ?>">
	          <label for="first_name">FIRST NAME</label>
	        </div>

	        <div class="input-field col s4">
	          <input id="last_name" name="last_name" type="text" class="validate" value="<?php echo(!EMPTY($last_name)) ? $last_name : ''; ?>">
	          <label for="last_name">LAST NAME</label>
	        </div>

	        <div class="input-field col s3">
	          <input id="mid_name" name="middle_initial" type="text" class="validate"value="<?php echo(!EMPTY($middle_initial)) ? $middle_initial : ''; ?>">
	          <label for="last_name">M.I</label>
	        </div>
		</div>
  	</div>

  	<!-- PROFESSION FIELDS -->
  	<div class="row">
  		<div class="col s12">
	        <div class="input-field col s12">
				<label class="active">PROFESSION</label>
				<br>
				<select id="profession"  name="profession" class="selectize" multiple >
					<option value="" disabled selected>--Select Profession--</option>
					<option value="1">Option 1</option>
					<option value="2">Option 2</option>
					<option value="3">Option 3</option>
				</select>
			</div>
		</div>
	</div>

	<!-- AREA FIELDS -->
	<div class="row">
		<div class="col s12">
			<div class="input-field col s3">

				<label class="active">AREA</label>
				<br>
				<select id="area"  name="area" class="selectize">
					<option value="" disabled selected>--Select Area--</option>
					<option value="1">Option 1</option>
					<option value="2">Option 2</option>
					<option value="3">Option 3</option>
				</select>

			</div>

			<div class="input-field col s5">

				<label class="active">REGION</label>
				<br>
				<select id="region"  name="region" class="selectize">
					<option value="" disabled selected>--Select Region--</option>
					<option value="1">Option 1</option>
					<option value="2">Option 2</option>
					<option value="3">Option 3</option>
				</select>

			</div>

			<div class="input-field col s4">

				<label class="active">STATUS</label>
				<br>
				<select id="status"  name="status" class="selectize">
					<option value="" disabled selected>--Select Status--</option>
					<option value="1">Option 1</option>
					<option value="2">Option 2</option>
					<option value="3">Option 3</option>
				</select>
			</div>
		</div>
	</div>

	<!-- YEAR ACREDITATION FIELD -->
	<div class="row">
		<div class="col s12">
			<div class="input-field col s5">

				<label>YEAR OF ACCREDITATION</label>
				<input type="text" class="datepicker" name="year_accreditation">

			</div>

			<div class="input-field col s6">
					
					<input type="checkbox" class="labelauty" name="sole_qualified" id="sole_qualified" value="<?php echo ACTIVE ?>" data-labelauty="Qualified Sole Arbitrator|Qualified Sole Arbitrator" checked/>
			</div>
		</div>
	</div>

	<!-- MACE FIELD -->
	<div class="row">
		<div class="col s12">
			<div class="input-field col s3">
			
				<label>MACE PTS</label>
				<input id="mace_pts" type="text" class="validate">
			
			</div>

			<div class="input-field col s6">
				<input type="checkbox" class="labelauty" name="mace_excempt" id="mace_excempt" value="<?php echo ACTIVE ?>" data-labelauty="Not Excempted on MACE|Excempted on MACE" />
			</div>
		</div>
	</div>

	<!-- RESUME FIELD -->
	<div class="row">
		<div class="col s12">
			<div class="file-field input-field col s5">

		      <div class="btn">

		        <span>File</span>
		        <input type="file" multiple>

		      </div>

		      <div class="file-path-wrapper">

		        <input class="file-path validate" type="text" placeholder="Upload one or more files">

		      </div>

		    </div>
		</div>
	</div>


	<div class="md-footer default">
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light" id="save_position" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  <?php //endif; ?>
	  <a class="waves-effect waves-teal btn-flat" id="cancel_position">Cancel</a>
	</div>
</form>

<script>
$(function() {

  //help_text("role_form");

  // materialize select only the select element inside form
  /*$('#arbitrator_form select').material_select();*/

  $('#arbitrator_form').parsley();
  
  $('#arbitrator_form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {
	  var data = $(this).serialize();
	  
	  button_loader('save_position', 1);
	  $.post("<?php echo base_url() . PROJECT_ISCA ?>/isca_arbitrators/process/", data, function(result) {
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  button_loader('save_position', 0);
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  button_loader("save_position",0);
		  modalObj.closeModal();
		  
		  load_datatable('arbitrator_table', '<?php echo PROJECT_ISCA ?>/isca_arbitrators/get_arbitrator_list/');
		}
	  }, 'json');       
    }
  });
  
  <?php if(!EMPTY($id)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>


	$("#cancel_position").on("click", function(){
		modalObj.closeModal();
	});
})
</script>