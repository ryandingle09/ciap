
<form id="position_form">
	<input type="hidden" name="security" value="<?php echo $security ?>">
	
	<div class="form-float-label">
	  <div class="row m-n">
	    <div class="col s12">
		  <div class="input-field">
		    <input type="text" class="validate" required="" aria-required="true" name="position_name" id="position_name" value="<?php echo ISSET($position_name) ? $position_name : "" ?>"/>
		    <label for="position_name">Name</label>
	      </div>
	    </div>
	  </div>
	</div>
	
	<div class="md-footer default">
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light" id="save_position" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  <?php //endif; ?>
	  <a class="waves-effect waves-teal btn-flat" id="cancel_position">Cancel</a>
	</div>
</form>
<script>
$(function(){
	
	$('#position_form').parsley();
 	$('#position_form').submit(function(e) {
	  
		e.preventDefault();
    
		if ( $(this).parsley().isValid() ) {
		
			var data = $(this).serialize();
	  
			button_loader('save_position', 1);

			$.post("<?php echo base_url() . PROJECT_ISCA ?>/isca_positions/process/", data, function(result) {

				button_loader('save_position', 0);
				
				if(result.flag == 1){		
					modalObj.closeModal();
		  
					load_datatable('position_table', '<?php echo PROJECT_ISCA ?>/isca_positions/get_position_list/');
				}

				notification_msg(result.class, result.msg);	
					
	  		}, 'json');       
    	}
	});

 	$("#cancel_position").on("click", function(){
		modalObj.closeModal();
	});
  
  	<?php IF(!EMPTY($position_id)){ ?>
		$('.input-field label').addClass('active');
  	<?php } ?>
})
</script>