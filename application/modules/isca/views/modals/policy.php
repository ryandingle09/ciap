<form id="policy_form">
	<input type="hidden" name="security" value="<?php echo $security ?>">

  	<!-- PROFESSION FIELDS -->
  	<div class="row">

        <div class="input-field col s12">

			<select id="profession"  name="profession" >
				<option value="" disabled selected>--Select Category--</option>
				<option value="1">Option 1</option>
				<option value="2">Option 2</option>
				<option value="3">Option 3</option>
			</select>
			<label>CATEGORY</label>

		</div>

	</div>

	<!-- TITLE FIELD -->
	<div class="row">

		<div class="input-field col s12">

			<input type="text" id="title" name="title" required="">
			<label>TITLE</label>

		</div>

	</div>

	<!-- YEAR ACREDITATION FIELD -->
	<div class="row">

		 <div class="input-field col s12">

			<select id="profession"  name="profession" multiple>
				<option value="" disabled selected>--Select Keywords--</option>
				<option value="1">Option 1</option>
				<option value="2">Option 2</option>
				<option value="3">Option 3</option>
			</select>
			<label>KEYWORDS</label>

		</div>

	</div>

	

	<!-- FILES FIELD -->
	<div class="row">

		<div class="file-field input-field col s6">

	      <div class="btn">

	        <span>File</span>
	        <input type="file" multiple>

	      </div>

	      <div class="file-path-wrapper">

	        <input class="file-path validate" type="text" placeholder="Upload one or more files">

	      </div>

	    </div>

	</div>


	<div class="md-footer default">
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light" id="save_position" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  <?php //endif; ?>
	  <a class="waves-effect waves-teal btn-flat" id="cancel_position">Cancel</a>
	</div>
</form>

<script>
$(function() {

  //help_text("role_form");

  // materialize select only the select element inside form
  $('#policy_form select').material_select();

  $('#policy_form').parsley();
  
  $('#policy_form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {
	  var data = $(this).serialize();
	  
	  button_loader('save_position', 1);
	  $.post("<?php echo base_url() . PROJECT_CEIS ?>/ceis_positions/process/", data, function(result) {
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  button_loader('save_position', 0);
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  button_loader("save_position",0);
		  modalObj.closeModal();
		  
		  load_datatable('position_table', '<?php echo PROJECT_CEIS ?>/ceis_positions/get_position_list/');
		}
	  }, 'json');       
    }
  });
  
  <?php if(EMPTY($arbitrator_id)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>


	$("#cancel_position").on("click", function(){
		modalObj.closeModal();
	});
})
</script>