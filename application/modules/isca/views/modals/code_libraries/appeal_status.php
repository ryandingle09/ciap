<form id="agenda_form">
	<input type="hidden" name="security" value="<?php echo $security ?>">

  	<!-- NAME OF MEETING FIELDS -->
  	<div class="row">
    	<div class="col s12">
	        <div class="input-field col s12">
		
				<input id="appeal_status_name" type="text" class="validate" name="appeal_status_name" value="<?php  echo (!empty($appeal_status_name)) ? $appeal_status_name : ''; ?>">
				<label>Appeal Status</label>
		
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col s12">
			<div class="input-field col s6">
				<h6> STATUS </h6>
				<input type="checkbox" class="labelauty" name="appeal_status" id="status" value="<?php echo !is_null($status) ? (  ($status == '1') ? '1': '0' ) : '1' ?>" data-labelauty="In-Active|Active" <?php echo !is_null($status) ? (  ($status == 1) ? "checked" : ""  ) : "checked" ?> />
			</div>
		</div>
	</div>
	


	<div class="md-footer default">
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light" id="save_position" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  <?php //endif; ?>
	  <a class="waves-effect waves-teal btn-flat" id="cancel_position">Cancel</a>
	</div>
</form>


<script>

$(function() {

  $('#agenda_form').parsley();

  $('#agenda_form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {

		// update text are content
		for (instance in CKEDITOR.instances) {

			CKEDITOR.instances[instance].updateElement();
		}
		var data = $(this).serialize();

		button_loader('save_position', 1);

		$.post("<?php echo base_url() . PROJECT_ISCA ?>/isca_appeal_status/process/", data, function(result) {
			
			if(result.flag == 0) {

			notification_msg("<?php echo ERROR ?>", result.msg);
			button_loader('save_position', 0);

		} else {

			notification_msg("<?php echo SUCCESS ?>", result.msg);
			button_loader("save_position",0);
			modalObj.closeModal();

			load_datatable('case_appeal_table', '<?php echo PROJECT_ISCA ?>/isca_appeal_status/get_appeal_status_list/');

		}

	  }, 'json');       
    }
  });
  
  <?php if(!EMPTY($appeal_status_id)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>
  
	$("#cancel_position").on("click", function(){
		modalObj.closeModal();
	});
	
	$(".labelauty").change(function () {
		var val = $(this).val();
		(val == 1) ? $(this).val('0') : $(this).val('1');
	});
})

</script>