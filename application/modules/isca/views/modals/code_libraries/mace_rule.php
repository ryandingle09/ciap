<form id="attendee_form">
	<input type="hidden" name="security" value="<?php echo $security ?>">

  	<!-- NAME OF MEETING FIELDS -->
  	<div class="row">
    	<div class="col s12">
	        <div class="input-field col s6">
		
				<input id="mace_credit_unit_required" type="text" class="validate" name="mace_credit_unit_required" value="<?php  echo (!empty($mace_credit_unit_required)) ? $mace_credit_unit_required : ''; ?>">
				<label>MACE CREDIT UNITS REQUIRED</label>
		
			</div>
		</div>
	</div>
	<div class="row">
    	<div class="col s12">
	        <div class="input-field col s6">
		
				<input id="mace_credit_unit_period" type="text" class="validate" name="mace_credit_unit_period" value="<?php  echo (!empty($mace_credit_unit_period)) ? $mace_credit_unit_period : ''; ?>">
				<label>MACE CREDIT UNIT PERDIOD(in years)</label>
		
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col s12">
			<div class="input-field col s6">
				<h6> STATUS </h6>
				<input type="checkbox" class="labelauty" name="rule_status" id="status" value="<?php echo !is_null($status) ? (  ($status == '1') ? '1': '0' ) : '1' ?>" data-labelauty="In-Active|Active" <?php echo !is_null($status) ? (  ($status == 1) ? "checked" : ""  ) : "checked" ?> />
			</div>
		</div>
	</div>
	


	<div class="md-footer default">
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light" id="save_position" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  <?php //endif; ?>
	  <a class="waves-effect waves-teal btn-flat" id="cancel_position">Cancel</a>
	</div>
</form>


<script>

$(function() {

  $('#attendee_form').parsley();

  $('#attendee_form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {

		var data = $(this).serialize();

		button_loader('save_position', 1);

		$.post("<?php echo base_url() . PROJECT_ISCA ?>/isca_mace_rules/process/", data, function(result) {
			
			if(result.flag == 0) {

			notification_msg("<?php echo ERROR ?>", result.msg);
			button_loader('save_position', 0);

		} else {

			notification_msg("<?php echo SUCCESS ?>", result.msg);
			button_loader("save_position",0);
			modalObj.closeModal();

			load_datatable('mace_rule_table', '<?php echo PROJECT_ISCA ?>/isca_mace_rules/get_mace_rules_list/');

		}

	  }, 'json');       
    }
  });
  
  <?php if(!EMPTY($mace_rule_id)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>

	$("#cancel_position").on("click", function(){
		modalObj.closeModal();
	});

	$(".labelauty").change(function () {
	var val = $(this).val();
		(val == 1) ? $(this).val('0') : $(this).val('1');
	});
})

</script>