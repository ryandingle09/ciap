<form id="document_type_form">
	<input type="hidden" name="security" value="<?php echo $security ?>">

  	<!-- NAME OF MEETING FIELDS -->
  	<div class="row">
    	<div class="col s12">
	        <div class="input-field col s12">
		
				<input id="document_type" type="text" class="validate" name="document_type" value="<?php  echo (!empty($document_type)) ? $document_type : ''; ?>">
				<label>TYPE OF CASE DOCUMENT</label>
		
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col s12">
			<div class="input-field col s6">
				<h6> STATUS </h6>
				<input type="checkbox" class="labelauty" name="document_status" id="status" value="<?php echo !is_null($status) ? (  ($status == '1') ? '1': '0' ) : '1' ?>" data-labelauty="In-Active|Active" <?php echo !is_null($status) ? (  ($status == 1) ? "checked" : ""  ) : "checked" ?> />
			</div>
		</div>
	</div>
	


	<div class="md-footer default">
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light" id="save_position" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  <?php //endif; ?>
	  <a class="waves-effect waves-teal btn-flat" id="cancel_position">Cancel</a>
	</div>
</form>


<script>

$(function() {

  $('#document_type_form').parsley();

  $('#document_type_form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {

		var data = $(this).serialize();

		button_loader('save_position', 1);

		$.post("<?php echo base_url() . PROJECT_ISCA ?>/isca_case_type_document/process/", data, function(result) {
			
			if(result.flag == 0) {

			notification_msg("<?php echo ERROR ?>", result.msg);
			button_loader('save_position', 0);

		} else {

			notification_msg("<?php echo SUCCESS ?>", result.msg);
			button_loader("save_position",0);
			modalObj.closeModal();

			load_datatable('case_type_document_table', '<?php echo PROJECT_ISCA ?>/isca_case_type_document/get_case_type_document_list/');

		}

	  }, 'json');       
    }
  });
  
  <?php if(!EMPTY($document_type_id)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>
  
	$("#cancel_position").on("click", function(){
		modalObj.closeModal();
	});
	$(".labelauty").change(function () {
		var val = $(this).val();
		(val == 1) ? $(this).val('0') : $(this).val('1');
	});
})

</script>