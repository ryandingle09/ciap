<form id="payment_order">
	<input type="hidden" name="security" value="<?php echo $security ?>">

  	<!-- PAYMENT TYPE FIELDS -->
  	<div class="row">

       <div class="col s12 p-t-md">
   			<h6>TYPE OF PAYMENT</h6>
			
			<div class="input-field col s2">

				<input type="radio" class="labelauty" name="payment_type" id="payment_types" value="0" data-labelauty="NEW" checked/>
			
			</div>

			<div class="input-field col s3">

				<input type="radio" class="labelauty" name="payment_type" id="payment_type" value="1" data-labelauty="EXISTING" />
			
			</div>

	    </div>
	    
	</div>

	<!-- CASE FIELD -->
	<div class="row">

		 <div class="input-field col s12">
			<label for="case" class="active">CASE</label>
			<br>
			<select id="case"  name="case" disabled>
				<option value="" disabled selected>--Select Case--</option>
				<?php foreach ($cases as $case => $value) {?>
					<option value="<?php echo $value['case_id'];?>"> <?php echo $value['case_title'];?></option>
				<?php }?>
			</select>

		</div>

	</div>

	<div class="md-footer default">
	  <?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    <button class="btn waves-effect waves-light" id="next_btn" value="next">NEXT</button>
	  <?php //endif; ?>
	  <a class="waves-effect waves-teal btn-flat" id="cancel_position">Cancel</a>
	</div>
</form>

<script>
$(function() {

  //help_text("role_form");

  $('#case').selectize();

  $('#payment_order').parsley();
  
  $('#payment_order').submit(function(e) {

	e.preventDefault();
  	
    
	if ( $(this).parsley().isValid()) {
	  var data = $(this).serialize();
	 
	  button_loader('next_btn', 1);

	  $.post("<?php echo base_url() . PROJECT_ISCA ?>/isca_payments_order/check_request_create/", data, function(result) {
		if(result.flag == 0) {
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  button_loader('next_btn', 0);
		} else {
		 location.href = "<?php echo base_url() . PROJECT_ISCA ?>/isca_payments_order/create_payment_order/";
		  
		}
	  }, 'json');
    }
  });
  
  <?php if(EMPTY($arbitrator_id)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>


	$("#cancel_position").on("click", function(){
		modalObj.closeModal();
	});

	$(".labelauty").change(function (){
		var val = $(this).val();

		/*ENABLE-DISABLE THE CASE SELECT OPTIONS*/
		(val == 1) ? $('#case').selectize()[0].selectize.enable() : $('#case').selectize()[0].selectize.disable();

	});
})
</script>