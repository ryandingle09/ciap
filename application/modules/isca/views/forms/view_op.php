<div class="page-title">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Payment Order</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 ">
	  <h5>Order of Payments
		<span>Payment Order Detail</span>
	  </h5>
	</div>
  </div>
</div>


<input type="hidden" name="security" value="<?php echo $security ?>">

<div class="m-t-lg bg-white" >
	
	<ul class="tabs bg-dark-blue-green">

		<li class="tab col s2"><a href="#parties" style="color:black" class="active" >Parties</a></li>
		<li class="tab col s2"><a href="#contracts" style="color:black" >Contracts & Settlement</a></li>
		<li class="tab col s2"><a href="#order_of_payments"  style="color:black">Order of Payments</a></li>

	</ul>

	<div class="container">
		<!-- PARTIES CONTENT TABS -->
		<div id="parties" class="col s12">
			<!-- CLAIMANT TABLE -->
			<div class="row">
				<div class="col s12">
					<h6> Claimant </h6>
		    		<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered " id="claimant_table" style="text-align:center">
						<thead>
							<tr>
								<th width="15%">NAME</th>
								<th width="15%">TYPE OF CLAIMANT</th>
								<th width="15%">BUSINESS ADDRESS</th>
								<th width="15%">CONTACT NO.</th>
							</tr>
						</thead>
						<tbody class="tbody">
							<?php 
								if(!empty($case_parties)) {
									$ctr = 0;
									foreach ($case_parties as $party) {
										if($party['party_type'] == '1') {
							?>
											<tr>
												<td>
												 	<h6> <?php echo $party['party_name'] ?></h6>
												</td>

												<td>
													<h6>
														<?php echo ($case_parties[$ctr]['type_clm_rpd'] == 'C') ? "Claimant" : ( ($case_parties[$ctr]['type_clm_rpd'] == 'CC') ? "Cross Claimant" : "Third-Party Claimant") ?> 
													</h6>	
												</td>

												<td>
										 			<h6> <?php echo $party['party_address'];?> </h6>
								 		 		</td>

												<td> 
										 			<h6> <?php echo $party['party_contact_no'];?> </h6> 
												</td>

											</tr>
							<?php
							 			}
							 		$ctr+=1;
							 		}
								} 
							?>
							
						</tbody>	
					</table>
				</div>
			</div>

			<!-- RESPONDENT TABLE -->
			<div class="row">
				<div class="col s12">
					<h6> Respondent </h6>
		    		<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="respondent_table">
						<thead>
							<tr>
								<th width="15%">NAME</th>
								<th width="15%">TYPE OF RESPONDENT</th>
								<th width="15%">BUSINESS ADDRESS</th>
								<th width="15%">CONTACT NO.</th>
							</tr>
						</thead>
						<tbody class="tbody_respondent">
							<?php 
								if(!empty($case_parties)) {
									$ctr = 0;
									foreach ($case_parties as $party) {
										if($party['party_type'] == '2') {
							?>
											<tr>
												<td>
													<h6> <?php echo $party['party_name'];?> </h6>
												</td>

												<td>
													<h6> <?php echo ($case_parties[$ctr]['type_clm_rpd'] == 'R') ? "Respondent" : "Third-Party Respondent"?> </h6>
												</td>

												<td> 
										 			<h6> <?php echo $party['party_address'];?> </h6>
												</td>
												<td> 
											 		<h6> <?php echo $party['party_contact_no'];?> </h6>
											 	</td>
											</tr>
							<?php
									 	}
									 	$ctr+=1;
									}
								}
							?>
						</tbody>	
					</table>
				</div>
			</div>
			<div class="col s3 offset-s9 right-align">
				<button class="btn waves-effect waves-light" id="btnContinue" name="btnContinue" type="submit" value="next">NEXT</button>
			</div>
			<div class="col s12"> 
				&nbsp;
			</div>
		</div>

		<!-- CONTRACT CONTENT TAB -->
		<div id="contracts" class="col s12">

			<div class="row">
				<div class="col s12" id="short_title">
					<br/>
					<h6> CASE TITLE: 
						<span style="margin-left:30px;" >
							<strong>  <?php echo !empty($case_details) ? $case_details['case_title'] : ''; ?> </strong> 
					 	</span>
					</h6>
			 	</div>
			</div>

			<div class="row"> 

				<div class="input-field col s3">
					
					<h6> MODE OF SETTLEMENT: <strong>  <?php echo ($case_details['settlement_mode'] == 'A') ? "Arbitration" : "Mediation";?> </strong>  </h6>

				</div>

				<div class="input-field col s3" id="mode_of_arbitration">
					
					
					<div class="<?php echo (!empty($case_arbitration_dtl)) ? "visible" : 'hidden';?>">
			 			<h6> MODE OF ARBITRATION: <strong> <?php echo ($case_arbitration_dtl[0]['arbitration_mode'] == 'S') ? 'Sole' : 'Tribunal'; ?> </strong> </h6>
			 		</div>
					
				</div>

			</div>

			<div class="row" id="number_of_disputants">

				<div class="input-field col s12">
					<h6>Number of Disputants:</h6>
				</div>

				<div class="col s12">
					<div class="input-field col s2 right-align">
						<h6 id="total_claimant">Total Claimant(s): <strong>  <?php echo count($party['party_name'])?> </strong>  </h6>
						<h6 id="total_respondent"> Total Respondet(s): <strong>  <?php echo count($party['party_name'])?> </strong>   </h6>
					</div>
				</div>
			</div>
		
			<!-- CONTACT CLAIM TABLE -->
			<div class="row">
				<div class="col s12">
					<h6 > Contract Claim 
					</h6>

		    		<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto <?php echo (!empty($case_details) && $case_details['settlement_mode'] == 'M') ? 'hidden' : '';?>" id="arbitration_amount_table">
						<thead>
							<tr>
								<th width="25%">CONTRACT DESCRIPTION</th>
								<th width="20%">TYPE OF CLAIM</th>
								<th width="15%">AMOUNT OF CLAIM</th>
								<th width="5%"></th>
							</tr>
						</thead>

						<tbody class="tbody_amount">
							<?php 
								if (!empty($case_claims)) {
									if($case_details['settlement_mode'] == 'A') {
										foreach ($case_claims as $case_claim) {
							?>
										<tr>
											<td> 
									 			<h6> <?php echo $case_claim['claim_desc'];?> </h6>
										 	</td>
											<td>
												<h6> 
													<?php echo ($case_claim['claim_type'] == "M") ? "Monetary" : ( ($case_claim['claim_type'] == "N") ? "Non-Monetary with Monetary" : "Purely Non-Monetary") ;?>
												</h6>
											</td>
											<td> 
												<h6>
													<?php echo $case_claim['claim_amount'];?>
												</h6>
										 	</td>
										</tr>
							<?php 
									}
								}
							} 
							?>
								
						</tbody>	
					</table>

					<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto <?php echo ((!empty($case_details) && $case_details['settlement_mode'] == 'A') || empty($case_details)) ? 'hidden' : '';?>" id="mediation_amount_table">
						<thead>
							<tr>
								<th width="25%">CONTRACT DESCRIPTION</th>
								<th width="15%">AMOUNT OF CLAIM</th>
							</tr>
						</thead>
						<tbody class="tbody_amount">
							<?php 
								if(!empty($case_claims)) { 
									if($case_details['settlement_mode'] == 'M') {
										foreach ($case_claims as $case_claim) { 
							?>
											<tr>
												<td> 
											 		<h6> <?php echo $case_claim['claim_desc'];?> </h6>
											 	</td>

												<td> 	
											 		<h6> <?php echo $case_claim['claim_amount'];?> </h6>
											 	</td>
												
											</tr>
							<?php 
										}
									}
								}  
							?>
						</tbody>	
					</table>

				</div>
			</div>

			<div class="col s12 right-align">
				<a class="btn-flat waves-effect waves-light" id="btnBack" name="btnBack" value="back">BACK</a>
				<button class="btn waves-effect waves-light" id="btnContinueContract" name="btnContinueContract" type="submit" value="next">NEXT</button>
			</div>

			<div class="col s12"> 
				&nbsp;
			</div>
		</div>

		<!-- PAMENTS CONTENT TAB -->
		<div id="order_of_payments" class="col s12">

			<div class="row">
				<div class="input-field col s3">  
					<h6> DATE: <strong> <?php echo $case_op_hdr_data['op_date'];?> <strong> </h6>
				</div>

				<div class="input-field col s4">  
					<h6> TYPE OF PAYMENT: <strong>
						 <?php
							$payment_id = $case_op_hdr_data['payment_type_id']; 
							/* mutli ternary for checking payment type id */
							echo ($payment_id == '1') ? "Initial Deposit" : ( 
									($payment_id == "2") ? "Due upon TOR Signing" : ( 
										$payment_id == "3") ? "Due upon Penultimate Hearing" : "Post Award" );
						?>
					</strong>
					</h6>
				</div>
			</div>

			<div class="row">
				<div  style="margin:0 auto;width:100%; border:1px solid #9e9e9e;height:333px;" id="content_slip">
					<div style="width: 100%; border-bottom: 1px solid #9e9e9e; text-align: center;background-color:#e8eaf6;padding:10px;font-weight:bold;">
						SUMMARY OF FEES
					</div>

					<div style="float:left" class="col s6">

						<div class="row">
							<div class="col s12">
							<br/>
							<h6 style="font-weight:bold"> Total Fees </h6>
								<div class="col s6">
									<h6> Filing Fee</h6>
								</div>
								<div class="col s6 right-align">
									<h6> PhP 25.50</h6>
								</div>
							</div>

							<div class="col s12">
								<div class="col s6 ">
									<h6> Adminitrative Fee</h6>
								</div>
								<div class="col s6 right-align">
									<h6> PhP 76.50</h6>
								</div>
							</div>

							<div class="col s12">
								<div class="col s6 ">
									<h6> Mediator`s Fee</h6>
								</div>
								<div class="col s6 right-align" style="border-bottom:1px solid black">
									<h6> PhP 153.00</h6>
								</div>
							</div>
							<div class="col s12">
								<div class="col s6 ">
									<h6 style="font-weight:bold"> Total </h6>
								</div>
								<div class="col s6 right-align">
									<h6> PhP 255.00</h6>
								</div>
							</div>
						</div>
					</div>
					<div style="float:left" class="col s6">
						<div class="row">
							<div class="col s12">
							<br/>
							<h6 style="font-weight:bold"> Total Amount due upon filing </h6>
								<div class="col s6">
									<h6> Filing Fee(100%)</h6>
								</div>
								<div class="col s6 right-align">
									<h6> PhP 25.50</h6>
								</div>
							</div>

							<div class="col s12">
								<div class="col s6 ">
									<h6> Adminitrative Fee(100%)</h6>
								</div>
								<div class="col s6 right-align">
									<h6> PhP 76.50</h6>
								</div>
							</div>

							<div class="col s12">
								<div class="col s7 ">
									<h6> Mediator`s Fee(50%)</h6>
								</div>
								<div class="col s5 right-align" style="border-bottom:1px solid black">
									<h6> PhP 76.50</h6>
								</div>
							</div>
							<div class="col s12">
								<div class="col s7 ">
									<h6 style="font-weight:bold"> *Total amount due upon filing </h6>
								</div>
								<div class="col s5 right-align">
									<h6> PhP 178.50</h6>
								</div>
							</div>
						</div>
					</div>
					
					<div style="float:right" class="col s6">
						<div class="row">
							<div class="col s12">
								<div class="col s7">
								<h6 style="font-weight:bold"> **Plus: Legal Research Fund </h6>
								</div>
								<div class="col s5 right-align">
									<h6> PhP 0.26</h6>
								</div>
							</div>
						</div>
					</div>

					<div  class="col s12">
						<div class="row">
							<div class="col s12">
								<span style="font-style:italic;"> *Please pay in Cash or Manager's Check, payable to <span style="font-weight:bold;">Construction Industry Arbitration Commission</span> </span>
							</div>
						</div>
					</div>
					<div  class="col s12">
						<div class="row">
							<div class="col s12">
								<span style="font-style:italic;"> **Please pay to CIAP Cashier in Cash Only </span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	var deleteObj = new handleData({ 
		controller 	: 'isca_payments_order', 
		method 		: 'delete_party', 
		module		: '<?php echo PROJECT_ISCA ?>',
	});
</script>