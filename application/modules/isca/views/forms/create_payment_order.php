<div class="page-title">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Payment Order</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 ">
	  <h5>Order of Payments
		<span>Create Payment Order</span>
	  </h5>
	</div>
  </div>
</div>

<form id="form_order_payment">

	<input type="hidden" name="security" value="<?php echo $security ?>">

	<div class="m-t-lg bg-white" >
		
		<ul class="tabs bg-dark-blue-green">

			<li class="tab col s2"><a href="#parties" style="color:black" class="active" >Parties</a></li>
			<li class="tab col s2"><a href="#contracts" style="color:black" >Contracts & Settlement</a></li>
			<li class="tab col s2"><a href="#order_of_payments"  style="color:black">Order of Payments</a></li>

		</ul>

		<div class="container">
			<!-- PARTIES CONTENT TABS -->
			<div id="parties" class="col s12">
				<!-- CLAIMANT TABLE -->
				<div class="row">
					<div class="col s12">
						<h6> Claimant </h6>
			    		<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered " id="claimant_table" style="text-align:center">
							<thead>
								<?php if(!empty($case_parties)) {?>
								<tr>
									<th width="15%">NAME</th>
									<th width="15%">TYPE OF CLAIMANT</th>
									<th width="15%">BUSINESS ADDRESS</th>
									<th width="15%">CONTACT NO.</th>
									<th width="5%"></th>
								</tr>
								<?php } else { ?>
								<tr>
									<th width="15%">NAME</th>
									<th width="15%">TYPE OF CLAIMANT</th>
									<th width="15%">BUSINESS ADDRESS</th>
									<th width="15%">CONTACT NO.</th>
									<th width="5%"></th>
								</tr>
								<?php } ?>
							</thead>
							<tbody class="tbody">
								<?php 
									if(!empty($case_parties)) {
										$ctr = 0;
										foreach ($case_parties as $party) {
											if($party['party_type'] == '1') {
								?>
												<tr>
													<td>

														<select class="claimant_name " id="claimant_name" name="claimant_name[]">
															<option value="" selected> Select Claimant </option>
															<?php foreach ($parties as $claimant) { ?>
																<option value="<?php echo $claimant['party_id'];?>" <?php echo ($claimant['party_id'] == $party['party_id']) ? "selected" : ''?> > 
																	<?php echo $claimant['party_name'];?>
																</option>
															<?php }?>
														</select>

													</td>

													<td>
														<select class="claimant_type" id="claimant_type" name="claimant_type[]">
															<option value="C"  <?php echo ($case_parties[$ctr]['type_clm_rpd'] == 'C') ? "selected" : ''?>  >Claimant</option>
															<option value="CC"  <?php echo ($case_parties[$ctr]['type_clm_rpd'] == 'CC') ? "selected" : ''?> >Cross Claimant</option>
															<option value="TPC"  <?php echo ($case_parties[$ctr]['type_clm_rpd'] == 'TPC') ? "selected" : ''?> >Third-party Claimant</option>
														</select>
													</td>

													<td>
														<div class="input-field">
												 			<input type="text" class="claimant_address" name="claimant_address[]" value="<?php echo $party['party_address'];?>" >
												 		</div>
									 		 		</td>

													<td> 
														<div class="input-field">
												 			<input type="text" class="claimant_contact" name="claimant_contact[]" value="<?php echo $party['party_contact_no'];?>" >
												 		</div>
													</td>

													<td> 
														<br>
														<?php echo $case_parties['action'][$ctr]; ?>
													</td>	

												</tr>
								<?php
								 			}
								 			$ctr +=1;
								 			
								 		}
								 		
									} else {
								?>
										<tr>
											<td>
												<select class="claimant_name" id="claimant_name" name="claimant_name[]">
													<option value="" selected> Select Claimant </option>
													<?php foreach ($parties as $claimant) { ?>

														<option value="<?php echo $claimant['party_id'];?>" > 
															<?php echo $claimant['party_name'];?>
														</option>

													<?php }?>
												</select>
											</td>

											<td>
												<select class="claimant_type" id="claimant_type" name="claimant_type[]">
													<option value="C" selected>Claimant</option>
													<option value="CC" >Cross Claimant</option>
													<option value="TPC" >Third-party Claimant</option>
												</select>
											</td>

											<td>
												<div class="input-field">
										 			<input type="text" class="claimant_address" name="claimant_address[]">
										 		</div>
							 		 		</td>

											<td> 
												<div class="input-field">
										 			<input type="text" class="claimant_contact" name="claimant_contact[]">
										 		</div>
											</td>	

											<td> 
												<div class='table-actions'>
													<br>
													<a href='javascript:;' class='delete delete_claimant tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>
												</div>
											</td>
										</tr>
								<?php 
								}
								?>
								
								<tr class="tr_add">
									<td  colspan="5" style="text-align:right"> 
										<a class="btn waves-effect waves-light blue" id="add_claimant" name="add_claimant" value="add">ADD</a>
								 	</td>
								</tr>
								
							</tbody>	
						</table>
					</div>
				</div>

				<!-- RESPONDENT TABLE -->
				<div class="row">
					<div class="col s12">
						<h6> Respondent </h6>
			    		<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="respondent_table">
							<thead>
								<?php if(!empty($case_parties)) { ?>
								<tr>
									<th width="15%">NAME</th>
									<th width="15%">TYPE OF RESPONDENT</th>
									<th width="15%">BUSINESS ADDRESS</th>
									<th width="15%">CONTACT NO.</th>
									<th width="5%"></th>
								</tr>
								<?php } else { ?>
								<tr>
									<th width="15%">NAME</th>
									<th width="15%">TYPE OF RESPONDENT</th>
									<th width="15%">BUSINESS ADDRESS</th>
									<th width="15%">CONTACT NO.</th>
									<th width="5%"></th>
								</tr>
								<?php } ?>
							</thead>
							<tbody class="tbody_respondent">
								<?php 
									if(!empty($case_parties)) {
										$ctr = 0;
										foreach ($case_parties as $party) {

											if($party['party_type'] == '2') {
								?>
												<tr>
													<td>
														<select class="respondent_name" id="respondent_name" name="respondent_name[]" readonly="readonly">
															<option value="" selected> Select Respondent </option>
															<?php foreach ($parties as $respondent) { ?>
																<option value="<?php echo $respondent['party_id'] ?>" <?php echo ($respondent['party_id'] == $party['party_id']) ? "selected" : ''?> > 
																	<?php echo $respondent['party_name'];?>
																</option>
															<?php } ?>
														</select>
													<!-- 	<div class="input-field">
												 			<input type="text" value="<?php echo $party['party_name'];?>" readonly="readonly">
												 		</div> -->
													</td>

													<td>
														<select class="claimant_type" id="claimant_type" name="claimant_type[]">
															<option value="R"  <?php echo ($case_parties[$ctr]['type_clm_rpd'] == 'R') ? "selected" : ''?>  >Respondent</option>
															<option value="TPR"  <?php echo ($case_parties[$ctr]['type_clm_rpd'] == 'TPR') ? "selected" : ''?> >Third-party Respondent</option>
														</select>
													</td>

													<td> 
														<div class="input-field">
												 			<input type="text" class="respondent_address" name="respondent_address[]" value="<?php echo $party['party_address'];?>" >
												 		</div>
													</td>
													<td> 
														<div class="input-field">
												 			<input type="text"	class="respondent_contact" name="respondent_contact[]" value="<?php echo $party['party_contact_no'];?>" >
												 		</div>
												 	</td>
												 	<td>
												 		<br>
														<?php echo $case_parties['action'][$ctr]; ?>
													</td>
												</tr>
								<?php
										 	}
										 	$ctr +=1;
										}
									} else {
								?>
										<tr>
											<td>
												<select class="respondent_name" id="respondent_name" name="respondent_name[]">
													<option value="" selected> Select Respondent </option>
													<?php foreach ($parties as $respondent) { ?>

														<option value="<?php echo $respondent['party_id'] ?>">
															<?php echo $respondent['party_name'];?>
														</option>

													<?php } ?>
												</select>
											</td>

											<td>
												<select class="respondent_type" id="respondent_type" name="respondent_type[]">
													<option value="R" selected>Respondent</option>
													<option value="TPR" >Third-party Respondent</option>
												</select>
											</td>

											<td> 
												<div class="input-field">
										 			<input type="text" class="respondent_address" name="respondent_address[]">
										 		</div>
											</td>
											<td> 
												<div class="input-field">
										 			<input type="text"	class="respondent_contact" name="respondent_contact[]" >
										 		</div>
										 	</td>
											<td>
												<div class='table-actions'>
													<br>
													<a href='javascript:;' class='delete delete_respondent tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>
												</div>
											</td>
										</tr>
								<?php
									}
								?>
								
								<tr class="tr_add_respondent">
									<td  colspan="5" style="text-align:right"> 
										<a class="btn waves-effect waves-light blue" id="add_respondent" name="add_respondent"  value="add">ADD</a>
								 	</td>
								</tr>
							</tbody>	
						</table>
					</div>
				</div>
				<div class="col s3 offset-s9 right-align">
					<button class="btn waves-effect waves-light" id="btnContinue" name="btnContinue" type="submit" value="next">NEXT</button>
				</div>
				<div class="col s12"> 
					&nbsp;
				</div>
			</div>
</form>
<form id="form_order_payment_contract">
			<!-- CONTRACT CONTENT TAB -->
			<div id="contracts" class="col s12">

				<div class="row">
					<div class="col s12" id="short_title">
						<br/>
						<h6> CASE TITLE: 
							<span style="margin-left:40px;" >
								<?php echo !empty($case_details) ? $case_details['case_title'] : ''; ?>
						 	</span>
						 	<input type="hidden" name="case_title" id="case_title" value="<?php echo !empty($case_details) ? $case_details['case_title'] : ''; ?>">
						</h6>
				 	</div>
				</div>

				<div class="row"> 

					<div class="input-field col s3">
						
						<label class="active">MODE OF SETTLEMENT</label>
						<br>
						<div class="<?php echo (!empty($case_details)) ? "visible" : 'hidden';?>">
				 			<input type="text"  value="<?php echo ($case_details['settlement_mode'] == 'A') ? "Arbitration" : "Mediation";?>" readonly="readonly">
				 		</div>

						<select name="mode_of_settlement" id="mode_of_settlement" class="<?php echo (!empty($case_details)) ? "hidden" : '';?>">
							<option value = "A" <?php echo (!empty($case_details) && $case_details['settlement_mode'] == 'A') ? "selected" : '';?> >Arbitration</option>
							<option value = "M" <?php echo (!empty($case_details) && $case_details['settlement_mode'] == 'M') ? "selected" : '';?> >Mediation</option>
					 	</select>

					</div>

					<div class="input-field col s3" id="mode_of_arbitration">
						<label class="active">MODE OF ARBITRATION</label>
						<br>
						<div class="<?php echo (!empty($case_arbitration_dtl)) ? "visible" : 'hidden';?>">
				 			<input type="text"  value="<?php echo ($case_arbitration_dtl[0]['arbitration_mode'] == 'S') ? 'Sole' : 'Tribunal'; ?>" readonly="readonly">
				 		</div>
						<select name="mode_of_arbitration" class="<?php echo (!empty($case_details)) ? "hidden" : '';?>">
							<option value = "S" <?php echo (!empty($case_arbitration_dtl) && $case_arbitration_dtl[0]['arbitration_mode'] == "S") ? "selected" : '';?> > Sole </option>
							<option value = "T" <?php echo (!empty($case_arbitration_dtl) && $case_arbitration_dtl[0]['arbitration_mode'] == "T") ? "selected" : '';?> > Tribunal </option>
					 	</select>
					</div>

				</div>

				<div class="row" id="number_of_disputants">

					<div class="input-field col s12">
						<h6>Number of Disputants:</h6>
					</div>

					<div class="col s12">
						<div class="input-field col s2 right-align">
							<h6 id="total_claimant">Total Claimant(s): 2</h6>
							<h6 id="total_respondent"> Total Respondet(s): 2</h6>
							<input type="hidden" name="number_of_disputants" id="total_disputants">
						</div>
					</div>
				</div>
			
				<!-- CONTACT CLAIM TABLE -->
				<div class="row">
					<div class="col s12">
						<h6 > Contract Claim 
						</h6>

			    		<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto <?php echo (!empty($case_details) && $case_details['settlement_mode'] == 'M') ? 'hidden' : '';?>" id="arbitration_amount_table">
							<thead>
								<tr>
									<th width="25%">CONTRACT DESCRIPTION</th>
									<th width="20%">TYPE OF CLAIM</th>
									<th width="15%">AMOUNT OF CLAIM</th>
									<th width="5%"></th>
								</tr>
							</thead>

							<tbody class="tbody_amount">
								<?php 
									if (!empty($case_claims)) {
										if($case_details['settlement_mode'] == 'A') {
											foreach ($case_claims as $case_claim) {
								?>
											<tr>
												<td> 
													<div class="input-field">
											 			<input type="text" name="arbitration_contract_description[]" value="<?php echo $case_claim['claim_desc'];?>" >
											 		</div>

											 	</td>
												<td>
													<select class="type_claim[]" name="type_claim[]">
														<option value="M" <?php echo ($case_claim['claim_type'] == "M") ? "selected" : '' ;?> > Monetary</option>
														<option value="N" <?php echo ($case_claim['claim_type'] == "N") ? "selected" : '' ;?>  >  Non-Monetary with Monetary </option>
														<option value="P" <?php echo ($case_claim['claim_type'] == "P") ? "selected" : '' ;?>  > Purely Non-Monetary </option>
													</select>
												</td>
												<td> 

													<div class="input-field">
											 			<input type="text" name="arbitration_contract_amount[]" value="<?php echo $case_claim['claim_amount'];?>" >
											 		</div>

											 	</td>
												<td> 
													<div class='table-actions'>
														<br>
														<a href='javascript:;' class='delete delete_amount tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>
													</div>
												</td>
											</tr>
								<?php 
										}
									}
								} 
								else {
								?> 
									<tr>
										<td> 
											<div class="input-field">
									 			<input type="text" name="arbitration_contract_description[]">
									 		</div>

									 	</td>
										<td>
											<select class="type_claim[]" name="type_claim[]">
												<option value="M" selected> Monetary</option>
												<option value="N">  Non-Monetary with Monetary </option>
												<option value="P"> Purely Non-Monetary </option>
											</select>
										</td>
										<td> 

											<div class="input-field">
									 			<input type="text" name="arbitration_contract_amount[]">
									 		</div>

									 	</td>
										<td> 
											<div class='table-actions'>
												<br>
												<a href='javascript:;' class='delete delete_amount tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>
											</div>
										</td>
									</tr>

								<?php } ?>
								
								<tr class="tr_arbitration_add_amount">
									<td  colspan="4" style="text-align:right"> 
										<a class="btn waves-effect waves-light blue" id="arbitration_add_amount" name="arbitration_add_amount"  value="add">ADD</a>
								 	</td>
								</tr>	
							</tbody>	
						</table>

						<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered <?php echo ((!empty($case_details) && $case_details['settlement_mode'] == 'A') || empty($case_details)) ? 'hidden' : '';?>" id="mediation_amount_table">
							<thead>
								<tr>
									<th width="25%">CONTRACT DESCRIPTION</th>
									<th width="15%">AMOUNT OF CLAIM</th>
									<th width="5%"></th>
								</tr>
							</thead>
							<tbody class="tbody_amount">
								<?php 
									if(!empty($case_claims)) { 
										if($case_details['settlement_mode'] == 'M') {
											foreach ($case_claims as $case_claim) { 
								?>
												<tr>
													<td> 

														<div class="input-field">
												 			<input type="text" name="mediation_contract_description[]" value="<?php echo $case_claim['claim_desc'];?>" >
												 		</div>

												 	</td>

													<td> 

														<div class="input-field">
												 			<input type="text" name="mediation_contract_amount[]" value="<?php echo $case_claim['claim_amount'];?>" >
												 		</div>

												 	</td>
													<td> 
														<div class='table-actions'>
															<br>
															<a href='javascript:;' class='delete delete_amount tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>
														</div>
													</td>
												</tr>
												
								<?php 
											}

										}
									} else { 
								?>
										<tr>
											<td> 

												<div class="input-field">
										 			<input type="text" name="mediation_contract_description[]">
										 		</div>

										 	</td>

											<td> 

												<div class="input-field">
										 			<input type="text" name="mediation_contract_amount[]">
										 		</div>

										 	</td>
											<td> 
												<div class='table-actions'>
													<br>
													<a href='javascript:;' class='delete delete_amount tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50' ></a>
												</div>
											</td>
										</tr>
									
								<?php } ?>
									<tr class="tr_mediation_add_amount">
										<td  colspan="4" style="text-align:right"> 
											<a class="btn waves-effect waves-light blue" id="mediation_add_amount" name="mediation_add_amount"  value="add">ADD</a>
									 	</td>
									</tr>
							</tbody>	
						</table>

					</div>
				</div>

				<div class="col s12 right-align">
					<a class="btn-flat waves-effect waves-light" id="btnBack" name="btnBack" value="back">BACK</a>
					<button class="btn waves-effect waves-light" id="btnContinueContract" name="btnContinueContract" type="submit" value="next">NEXT</button>
				</div>

				<div class="col s12"> 
					&nbsp;
				</div>
			</div>
</form>
<form id="form_order_payment_process">
			<!-- PAMENTS CONTENT TAB -->
			<div id="order_of_payments" class="col s12">

				<div class="row">
					<div class="input-field col s3">  
						<label> DATE</label>
						<input type="text" class="datepicker" name="date_created"> 
					</div>

					<div class="input-field col s3">  
						<label class="active"> TYPE OF PAYMENT</label>
						<br>
						<select id="type_payment" name="payment_type">
							<?php foreach ($payment_types as $payment_type) { ?>

								<option value="<?php echo $payment_type['payment_type_id']?>"> <?php echo $payment_type['payment_type'];?> </option>
							
							<?php } ?>
						</select>

					</div>
					<div class="input-field col s3">  
						<br/>
						<a class="btn waves-effect waves-light" id="generate_slip" name="generate_slip" value="add">GENERATE</a>
					</div>
				</div>

				 
			<div class="row">
				<div  class="b b-light-gray" style="height:333px;" id="content_slip">
					<div class="b-b b-light-gray center-align bg-gray p-sm font-bold" >
						SUMMARY OF FEES
					</div>

					<div style="float:left" class="col s6">

						<div class="row">
							<div class="col s12">
							<br/>
							<h6 class="font-bold"> Total Fees </h6>
								<div class="col s6">
									<h6> Filing Fee</h6>
								</div>
								<div class="col s6 right-align">
									<h6> PhP 25.50</h6>
								</div>
							</div>

							<div class="col s12">
								<div class="col s6 ">
									<h6> Adminitrative Fee</h6>
								</div>
								<div class="col s6 right-align">
									<h6> PhP 76.50</h6>
								</div>
							</div>

							<div class="col s12">
								<div class="col s6 ">
									<h6> Mediator`s Fee</h6>
								</div>
								<div class="col s6 right-align b-b">
									<h6> PhP 153.00</h6>
								</div>
							</div>
							<div class="col s12">
								<div class="col s6 ">
									<h6 class="font-bold"> Total </h6>
								</div>
								<div class="col s6 right-align">
									<h6> PhP 255.00</h6>
								</div>
							</div>
						</div>
					</div>
					<div style="float:left" class="col s6">
						<div class="row">
							<div class="col s12">
							<br/>
							<h6 class="font-bold"> Total Amount due upon filing </h6>
								<div class="col s6">
									<h6> Filing Fee(100%)</h6>
								</div>
								<div class="col s6 right-align">
									<h6> PhP 25.50</h6>
								</div>
							</div>

							<div class="col s12">
								<div class="col s6 ">
									<h6> Adminitrative Fee(100%)</h6>
								</div>
								<div class="col s6 right-align">
									<h6> PhP 76.50</h6>
								</div>
							</div>

							<div class="col s12">
								<div class="col s7 ">
									<h6> Mediator`s Fee(50%)</h6>
								</div>
								<div class="col s5 right-align b-b">
									<h6> PhP 76.50</h6>
								</div>
							</div>
							<div class="col s12">
								<div class="col s7 ">
									<h6 class="font-bold"> *Total amount due upon filing </h6>
								</div>
								<div class="col s5 right-align">
									<h6> PhP 178.50</h6>
								</div>
							</div>
						</div>
					</div>
					
					<div style="float:right" class="col s6">
						<div class="row">
							<div class="col s12">
								<div class="col s7">
								<h6 class="font-bold"> **Plus: Legal Research Fund </h6>
								</div>
								<div class="col s5 right-align">
									<h6> PhP 0.26</h6>
								</div>
							</div>
						</div>
					</div>

					<div  class="col s12">
						<div class="row">
							<div class="col s12">
								<span style="font-style:italic;"> *Please pay in Cash or Manager's Check, payable to <span class="font-bold">Construction Industry Arbitration Commission</span> </span>
							</div>
						</div>
					</div>
					<div  class="col s12">
						<div class="row">
							<div class="col s12">
								<span style="font-style:italic;"> **Please pay to CIAP Cashier in Cash Only </span>
							</div>
						</div>
					</div>
				</div>
			</div>
				<div class="row">
					<br/>
					<!-- <div class="col s12 right-align">
						<a href="#" class="btn">View</a>
						<a href="#" class="btn">Download</a>
					</div> -->
				</div>

				<div class="row">

					<div class="col s12 right-align" >
						<a class = "btn-flat waves-effect waves-light" id="btnBackOP" name="btnBackOP" > BACK</a>
						<button class="btn waves-effect waves-light" id="btnContinueSubmit" name="btnContinueSubmit" type="submit" value="add">SUBMIT</button>
					</div>

				</div>
			</div>
		</div>
	</div>
</form>
<script>
	var deleteObj = new handleData({ 
		controller 	: 'isca_payments_order', 
		method 		: 'delete_party', 
		module		: '<?php echo PROJECT_ISCA ?>',
	});
</script>