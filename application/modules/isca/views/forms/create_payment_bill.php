<div class="page-title">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Payments to Arbitrators</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Payments to Arbitrators
		<span>Create Payment to Arbitrators</span>
	  </h5>
	</div>
  </div>
</div>

<div class="m-t-lg bg-white">

	<ul class="tabs"style="background-color:#b2dfdb !important;" >
		<li class="tab col s2"><a href="#case_tab" style="color:black" class="active">Case</a></li>
		<li class="tab col s2"><a href="#deductions_tab" style="color:black" >Deduction</a></li>
		<li class="tab col s2"><a href="#print_tab"  style="color:black" >Print</a></li>
	</ul>

	<div class="container">
		<!-- CASE CONTENT TABS -->
		<div id="case_tab" class="col s12">

			<div class="row">
				<div class="input-field col s4">
					<br/>
					<label for="case_no" class="active">CASE NO.</label>
					<select name="case_no" id="case_no">
						<option value = "1" selected> option 1</option> 
						<option value = "2"> option 2</option>
						<option value = "3"> option 3</option>
				 	</select>
				</div>
			</div>
			
			<div class="row">
				<div class="input-field col s5">
					<br/>
					<label for ="case_stage"class="active">STAGE OF PROCEEDING</label>
					<select name="case_stage" id="case_stage">
						<option value = "1" selected> option 1</option> 
						<option value = "2"> option 2</option>
						<option value = "3"> option 3</option>
				 	</select>
				</div>
				<div class="input-field col s4">
					<label >% OF ARBITRATOR'S FEE</label>
				<input type="text" name ="arbitrator_fee" >
				</div>
			</div>

			<!-- HEARINGS/MEETINGS TABLE -->
			<div class="row">
				<div class="col s12">
					<h6> Hearings/Meetings </h6>
		    		<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered" id="hearings_table">
						<thead>
							<tr>
								<th width="10%">DATE</th>
								<th width="25%">ATTENDEES</th>
								<th width="5%"></th>
							</tr>
						</thead>
						<tbody class="tbody_attendees">
							<tr>
								<td> 
									<div class="input-field">
							 			<input type="text" class="datepicker"/>
							 		</div>	
								</td>
								<td>
									<select class='multi' placeholder='Select Attendees'>
										<option value=""> Select Attendees</option>
										<option value="1"> Juan Dela Cruz</option>
										<option value="2"> Juan Dela Cruz</option>
										<option value="3"> Juan Dela Cruz</option>
										<option value="4"> Juan Dela Cruz</option>
										<option value="5"> Juan Dela Cruz</option>
										<option value="6"> Juan Dela Cruz</option>
										<option value="7"> Juan Dela Cruz</option>
									</select>
								</td>
								<td> 
									<div class='table-actions'>
										<a href='javascript:;' class='delete delete_attendees'></a>
									</div>
								</td>
							</tr>
							<tr class="tr_add_attendess">
								<td  colspan="4" style="text-align:right"> 
									<button class="btn waves-effect waves-light blue" id="add_attendess" name="add_attendess" type="submit" value="add">ADD</button>
							 	</td>
							</tr>	
						</tbody>	
					</table>
				</div>
			</div>

		</div>
		
		<!-- DEDUCTIONS CONTENT TAB -->
		<div id="deductions_tab" class="col s12">

			<!-- CHAIRMAN TABLE -->
			<div class="row">
				<div class="col s12">
					<br/>
					<h6 style="font-weight:bold;"> CHARIMAN: Last Name, First Name
					</h6>
		    		<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered" id="chariman_table">
						<thead>
							<tr>
								<th width="15%">DEDUCTION TYPE</th>
								<th width="25%">DESCRIPTION</th>
								<th width="10%">AMOUNT</th>
								<th width="5%"></th>
							</tr>
						</thead>
						<tbody class="tbody_amount_chairman">
							<tr>
								<td>
									<select class="deduction_type" >
										<option value="0"selected> Juan Dela Cruz</option>
										<option value="1"> Juan Dela Cruz</option>
										<option value="2"> Juan Dela Cruz</option>
										<option value="3"> Juan Dela Cruz</option>
									</select>
								</td>
								<td> 

									<div class="input-field">
							 			<input type="text">
							 		</div>

							 	</td>

								<td> 

									<div class="input-field">
							 			<input type="text">
							 		</div>

							 	</td>
								<td> 
									<div class='table-actions'>
										<a href='javascript:;' class='delete delete_amount_chairman'></a>
									</div>
								</td>
							</tr>


							<tr class="tr_add_amount_chairman">
								<td  colspan="4" style="text-align:right"> 
									<button class="btn waves-effect waves-light blue" id="add_amount_chaiman" name="add_amount_chaiman" type="submit" value="add">ADD</button>
							 	</td>
							</tr>	
						</tbody>	
					</table>
				</div>
			</div>

			<!-- MEMBER 1 TABLE -->
			<div class="row">
				<div class="col s12">
					<br/>
					<h6 style="font-weight:bold;"> MEMBER 1: Last Name, First Name
					</h6>
		    		<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered" id="member1_table">
						<thead>
							<tr>
								<th width="15%">DEDUCTION TYPE</th>
								<th width="25%">DESCRIPTION</th>
								<th width="10%">AMOUNT</th>
								<th width="5%"></th>
							</tr>
						</thead>
						<tbody class="tbody_amount_member1">
							<tr>
								<td>
									<select class="deduction_type" >
										<option value="0"selected> Juan Dela Cruz</option>
										<option value="1"> Juan Dela Cruz</option>
										<option value="2"> Juan Dela Cruz</option>
										<option value="3"> Juan Dela Cruz</option>
									</select>
								</td>
								<td> 

									<div class="input-field">
							 			<input type="text">
							 		</div>

							 	</td>

								<td> 

									<div class="input-field">
							 			<input type="text">
							 		</div>

							 	</td>
								<td>
									<div class='table-actions'>
										<a href='javascript:;' class='delete delete_amount_member1'></a>
									</div>
								</td>
							</tr>

							
							<tr class="tr_add_amount_member1">
								<td  colspan="4" style="text-align:right"> 
									<button class="btn waves-effect waves-light blue" id="add_amount_member1" name="add_amount_member1" type="submit" value="add">ADD</button>
							 	</td>
							</tr>	
						</tbody>	
					</table>
				</div>
			</div>

			<!-- MEMBER 2 TABLE -->
			<div class="row">
				<div class="col s12">
					<br/>
					<h6 style="font-weight:bold;"> MEMBER 2: Last Name, First Name
					</h6>
		    		<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered" id="member2_table">
						<thead>
							<tr>
								<th width="15%">DEDUCTION TYPE</th>
								<th width="25%">DESCRIPTION</th>
								<th width="10%">AMOUNT</th>
								<th width="5%"></th>
							</tr>
						</thead>
						<tbody class="tbody_amount_member2">
							<tr>
								<td>
									<select class="deduction_type" >
										<option value="0"selected> Juan Dela Cruz</option>
										<option value="1"> Juan Dela Cruz</option>
										<option value="2"> Juan Dela Cruz</option>
										<option value="3"> Juan Dela Cruz</option>
									</select>
								</td>
								<td> 

									<div class="input-field">
							 			<input type="text">
							 		</div>

							 	</td>

								<td> 

									<div class="input-field">
							 			<input type="text">
							 		</div>

							 	</td>
								<td> 
									<div class='table-actions'>
										<a href='javascript:;' class='delete delete_amount_member2'></a>
									</div>
								</td>
							</tr>

							
							<tr class="tr_add_amount_member2">
								<td  colspan="4" style="text-align:right"> 
									<button class="btn waves-effect waves-light blue" id="add_amount_member2" name="add_amount_member2" type="submit" value="add">ADD</button>
							 	</td>
							</tr>	

						</tbody>	
					</table>
				</div>
			</div>

		</div>

		<!-- PRINT CONTENT TAB -->
		<div id="print_tab" class="col s12 m-b-lg">
			<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered" id="print_table">
				<thead>
					<tr>
						<th width="15%">ARBITRATOR</th>
						<th width="10%">FEE</th>
						<th width="15%">DEDUCTION</th>
						<th width="10%">STATUS</th>
						<th width="10%">AC</th>
					</tr>
				</thead>

				<tbody class="tbody_print">
					<tr>
						<td>Chairman: Lastname, Firstname</td>
						<td>FEE</td>
						<td>Deduction</td>
						<td>Status</td>
						<td>Action</td>
					</tr>
					<tr>
						<td>Member1: Lastname, Firstname</td>
						<td>FEE</td>
						<td>Deduction</td>
						<td>Status</td>
						<td>Action</td>
					</tr>
					<tr>
						<td>Member2: Lastname, Firstname</td>
						<td>FEE</td>
						<td>Deduction</td>
						<td>Status</td>
						<td>Action</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div class="col s12 right-align">
			<button class="btn waves-effect waves-light" id="add_amount" name="add_amount" type="submit" value="add">NEXT</button>
			<button class="btn waves-effect waves-light" id="add_amount" name="add_amount" type="submit" value="add">BACK</button>
		</div>
		
		</div>
	</div>
</div>
<script type="text/javascript">
	
	// $(".tbody_attendees").append(fields).after($(".tr_add_attendess"));

	$(function() {

		$('ul.tabs').tabs();

		//init selectize
		$("#hearings_table select").selectize({maxItems:null});
		$("#case_tab select").selectize();
		$("#deductions_tab select").selectize();

		/*CASE TAB ADD TR FOR MEETINGS/HEARINGS*/
		$("#add_attendess").click(function() {
			var fields = "<tr>"+

							"<td>"+
								"<div class='input-field'>"+
						 			"<input type='text' class='datepicker'/>"+
						 		"</div>"+
							"</td>"+

							"<td>"+
								"<select class='attendees_name multi' placeholder='Select Attendees'>"+
									"<option value=''> --Select Claimant-- </option>"+
									"<option value='2' > Juan Dela Cruz</option>"+
									"<option value='3' > Juan Dela  Cruz</option>"+
									"<option value='4' > Juan Dela Cruz</option>"+
								"</select>"+
							"</td>"+

							"<td>"+
								"<div class='table-actions'>"+
									"<a href='javascript:;' class='delete delete_attendees'></a>"+
								"</div>"+
							"</td>"+

						"</tr>";
			$(".tbody_attendees").find('tr:last').before(fields);
			$("#hearings_table select").selectize({maxItems:null});
		});

		// delete hearings tr
		$("#hearings_table").on('click' , '.delete_attendees', (function() {

			var tr_count = $('#hearings_table>tbody>tr').length;

			/*trap for deleting all table row*/
			if( tr_count > 2) {
				$(this).closest('tr').remove();
			} else {

				alert("Content cannot be empty.");
			}

		}));

		/*END CASE TAB*/

		/*DEDUCTION TAB ADD TR */

		/*CHAIRMAN TABLE*/
		$("#add_amount_chaiman").click(function() {
			var fields = "<tr>"+

							"<td>"+
								"<select >"+
									"<option value='2' > Juan Dela Cruz</option>"+
									"<option value='3' > Juan Dela  Cruz</option>"+
									"<option value='4' > Juan Dela Cruz</option>"+
								"</select>"+
							"</td>"+

							"<td>"+
								"<div class='input-field'>"+
						 			"<input type='text' />"+
						 		"</div>"+
							"</td>"+

							"<td>"+
								"<div class='input-field'>"+
						 			"<input type='text'/>"+
						 		"</div>"+
							"</td>"+

							"<td>"+
								"<div class='table-actions'>"+
									"<a href='javascript:;' class='delete delete_amount_chairman'></a>"+
								"</div>"+
							"</td>"+

						"</tr>";
			$(".tbody_amount_chairman").find('tr:last').before(fields);
			$("#deductions_tab select").selectize();
		});

		// delete hearings tr
		$("#chariman_table").on('click' , '.delete_amount_chairman', (function() {

			var tr_count = $('#chariman_table>tbody>tr').length;

			/*trap for deleting all table row*/
			if( tr_count > 2) {
				$(this).closest('tr').remove();
			} else {

				alert("Content cannot be empty.");
			}
			
		}));


		/*MEMBER 1 TABLE*/
		$("#add_amount_member1").click(function() {
			var fields = "<tr>"+

							"<td>"+
								"<select >"+
									"<option value='2' > Juan Dela Cruz</option>"+
									"<option value='3' > Juan Dela  Cruz</option>"+
									"<option value='4' > Juan Dela Cruz</option>"+
								"</select>"+
							"</td>"+

							"<td>"+
								"<div class='input-field'>"+
						 			"<input type='text' />"+
						 		"</div>"+
							"</td>"+

							"<td>"+
								"<div class='input-field'>"+
						 			"<input type='text'/>"+
						 		"</div>"+
							"</td>"+

							"<td>"+
								"<div class='table-actions'>"+
									"<a href='javascript:;' class='delete delete_amount_member1'></a>"+
								"</div>"+
							"</td>"+

						"</tr>";
			$(".tbody_amount_member1").find('tr:last').before(fields);
			$("#deductions_tab select").selectize();
		});

		// delete hearings tr
		$("#member1_table").on('click' , '.delete_amount_member1', (function() {
			
			var tr_count = $('#member1_table>tbody>tr').length;

			/*trap for deleting all table row*/
			if( tr_count > 2) {
				$(this).closest('tr').remove();
			} else {

				alert("Content cannot be empty.");
			}
		}));

		/*MEMBER 2 TABLE*/
		$("#add_amount_member2").click(function() {
			var fields = "<tr>"+

							"<td>"+
								"<select >"+
									"<option value='2' > Juan Dela Cruz</option>"+
									"<option value='3' > Juan Dela  Cruz</option>"+
									"<option value='4' > Juan Dela Cruz</option>"+
								"</select>"+
							"</td>"+

							"<td>"+
								"<div class='input-field'>"+
						 			"<input type='text' />"+
						 		"</div>"+
							"</td>"+

							"<td>"+
								"<div class='input-field'>"+
						 			"<input type='text'/>"+
						 		"</div>"+
							"</td>"+

							"<td>"+
								"<div class='table-actions'>"+
									"<a href='javascript:;' class='delete delete_amount_member2'></a>"+
								"</div>"+
							"</td>"+

						"</tr>";
			$(".tbody_amount_member2").find('tr:last').before(fields);
			$("#deductions_tab select").selectize();
		});

		// delete hearings tr
		$("#member2_table").on('click' , '.delete_amount_member2', (function() {

			var tr_count = $('#member2_table>tbody>tr').length;

			/*trap for deleting all table row*/
			if( tr_count > 2) {
				
				$(this).closest('tr').remove();
			} else {

				alert("Content cannot be empty.");
			}

		}));

		/*END DEDUCTION TAB*/


	})
</script>
 