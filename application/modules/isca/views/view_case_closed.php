<div class="page-title">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Closed Case</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Closed Case
		<span>View Closed Case</span>
	  </h5>
	</div>
  </div>
</div>
<form id="case_form">
	<div class="m-t-lg bg-white">			
		<!-- CASE CONTENT TABS -->
		<div id="case_tab" class="col s12">

			<div class="container">

				<div class="row">

    				<h6 style="font-weight:bold"> Case Information</h6>

					<div class="col s12" style="border-bottom:1px solid #a5a5a5">
						<br/>
						<br/>
						<h6>Basic Information</h6>
					</div>

				</div>

				<div class="row">
					<div class="col s8">
						<span style="font-weight:bold"> CASE NO.  </span> <span> 01-2016</span>
					</div> 
					<div class="col s4">
						<span style="font-weight:bold"> DATE FILED  </span> <span> June 10, 2016 </span>
					</div> 
				</div>

				<div class="row">
					<div class="col s8">
						<span style="font-weight:bold"> CASE TITLE  </span> <span> Juna Dela Cruz vs. Juan Dela Rosa</span>
					</div> 
					<div class="col s4">
						 <span style="font-weight:bold"> STAFF-IN-CHARGE </span> <span> Staff Name </span>
					</div> 
				</div>

				<div class="row">
					<div class="col s8">
						<span style="font-weight:bold"> SHORT TITLE  </span> <span> Juna Dela Cruz vs. Juan Dela Rosa</span>
					</div> 
					<div class="col s4">
						 <span style="font-weight:bold"> CASE STATUS </span> <span> Resolved </span>
					</div> 
				</div>

				<div class="row">
					<br>
					<div class="col s12">
						<span style="font-weight:bold"> MODE OF SETTLEMENT </span> <span> Arbitration </span>
					</div> 
				</div>

				<div class="row">
					<div class="col s12">
						<span style="font-weight:bold"> End of Retention Period </span> <span> June 10, 2016 </span>
					</div> 
				</div>

				<div class="row">
					<div class="input-field col s4" >
						<label for="case_location"> CASE LOCATION</label>
						<input type="text" >
					</div>
				</div>

				<div class="row">
					<div class="col s12">
						<h6 style="font-weight:bold">Claimant</h6>
						<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered">
							<thead>
								<tr>
									<th width="70%">DOCUMENT TITLE</th>
									<th width="20%">DOCUMENT DATE</th>
									<th width="10%">AC</th>
								</tr>
							</thead>

							<tbody>
								<tr>
									<td> SAMPLE</td>
									<td> SAMPLE</td>
									<td> SAMPLE</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>

				<div class="row">
					<br>
					<br>
					<div class="col s12">
						<h6 style="font-weight:bold">Respondent</h6>
						<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered">
							<thead>
								<tr>
									<th width="70%">DOCUMENT TITLE</th>
									<th width="20%">DOCUMENT DATE</th>
									<th width="10%">AC</th>
								</tr>
							</thead>

							<tbody>
								<tr>
									<td> SAMPLE</td>
									<td> SAMPLE</td>
									<td> SAMPLE</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</form>