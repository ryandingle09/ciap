<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Policies</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Policies
		<span>Manage Policies</span>
	  </h5>
	</div>
	<div class="col s6 right-align">
	  <div class="btn-group">
	    <button class="waves-effect waves-light" type="button" onclick="load_datatable('arbitrator_table','<?php echo PROJECT_ISCA?>/isca_policies/get_policy_list')"><i class="flaticon-arrows97"></i> Refresh</button>
	  </div>
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_policy" id="add_arbitrator" name="add_arbitrator" onclick="modal_init()">Add New Policy</button>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="arbitrator_table">
  <thead>
	<tr>
	  <th width="20%">DATE</th>
	  <th width="20%">CATEGORY</th>
	  <th width="20%">TITLE</th>
	  <th width="15%">KEYWORDS</th>
	  <th width="15%"></th>
	</tr>
  </thead>
  </table>
</div>
