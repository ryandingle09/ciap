<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Order of Payment</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Order of Payment
		<span>Manage Payment Order</span>
	  </h5>
	</div>
	<div class="col s6 right-align">
	  <div class="btn-group">
	    <button class="waves-effect waves-light" type="button" onclick="load_datatable('arbitrator_table','<?php echo PROJECT_ISCA?>/isca_payments_order/get_payments_order_list')"><i class="flaticon-arrows97"></i> Refresh</button>
	  </div>
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="payments_order" id="add_payment_order" name="add_payment_order" onclick="modal_init()">Create new Order Payment </button>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable">&nbsp;</div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="arbitrator_table">
  <thead>
	<tr>
	  <th width="15%">OP DATE</th>
	  <th width="10%">TYPE OF PAYMENT</th>
	  <th width="10%">CASE NO.</th>
	  <th width="20%">CASE TITLE</th>
	  <th width="15%">TYPE OF PAYMENT</th>
	  <th width="15%">AMOUNT</th>
	  <th width="10%">STATUS</th>
	  <th width="5%"></th>
	</tr>
  </thead>
  </table>
</div>
	