<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Nature of Case</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Nature of Case
		<span>Manage Nature of Case</span>
	  </h5>
	</div>
	<div class="col s6 right-align">
	  <div class="btn-group">
	    <button class="waves-effect waves-light" type="button" onclick="load_datatable('nature_case_table','<?php echo PROJECT_ISCA?>/isca_appeal_status/get_appeal_status_list')"><i class="flaticon-arrows97"></i> Refresh</button>
	  </div>
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_nature_case" id="add_meeting" name="add_meeting" onclick="modal_init()">Create New</button>
	  </div>
	</div>
  </div>
</div>
<div class="pre-datatable">&nbsp;</div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="nature_case_table">
  <thead>
	<tr>
	  <th width="20%">NATURE OF CASE</th>
	  <th width="20%">CREATED BY</th>
	  <th width="20%">CREATED DATE</th>
	  <th width="20%">STATUS</th>
	  <th width="10%">ACTION</th>
	</tr>
  </thead>
  </table>
</div>
<script >

	var deleteObj = new handleData({ 
		controller 	: 'isca_nature_of_case', 
		method 		: 'delete_code', 
		module		: '<?php echo PROJECT_ISCA ?>' 
	});

</script>