<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Mace Rule</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Mace Rule
		<span>Manage Mace Rule</span>
	  </h5>
	</div>
	<div class="col s6 right-align">
	  <div class="btn-group">
	    <button class="waves-effect waves-light" type="button" onclick="load_datatable('mace_rule_table','<?php echo PROJECT_ISCA?>/isca_mace_rules/get_mace_rules_list')"><i class="flaticon-arrows97"></i> Refresh</button>
	  </div>
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_mace_rule" id="add_meeting" name="add_meeting" onclick="modal_init()">Create New</button>
	  </div>
	</div>
  </div>
</div>
<div class="pre-datatable">&nbsp;</div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="mace_rule_table">
  <thead>
	<tr>
	  <th width="20%">CREDIT UNITS REQUIRED</th>
	  <th width="20%">CREDIT UNITS PERIOD</th>
	  <th width="20%">CREATED BY</th>
	  <th width="20%">CREATED DATE</th>
	  <th width="20%">STATUS</th>
	  <th width="10%">ACTION</th>
	</tr>
  </thead>
  </table>
</div>
<script >

	var deleteObj = new handleData({ 
		controller 	: 'isca_mace_rules', 
		method 		: 'delete_code', 
		module		: '<?php echo PROJECT_ISCA ?>' 
	});

</script>