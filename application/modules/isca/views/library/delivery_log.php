<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#">Library</a></li>
	<li><a href="#" class="active">Delivery Logs</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Delivery Logs
		<span>Manage Delivery Logs</span>
	  </h5>
	</div>
	<div class="col s6 right-align">
	  <div class="btn-group">
	    <button class="waves-effect waves-light" type="button" onclick="load_datatable('arbitrator_table','<?php echo PROJECT_ISCA?>/isca_delivery_logs/get_delivery_log_list')"><i class="flaticon-arrows97"></i> Refresh</button>
	  </div>
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="delivery_log" id="add_arbitrator" name="add_arbitrator" onclick="modal_init()">Create Delivery Log</button>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable">&nbsp;</div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="arbitrator_table">
  <thead>
	<tr>
	  <th width="10%">DATE</th>
	  <th width="15%">CONTROL NO.</th>
	  <th width="10%">CASE NO.</th>
	  <th width="15%">ADDRESSEE</th>
	  <th width="10%">MESSENGER</th>
	  <th width="15%">DATE OF DELIVERY</th>
	  <th width="15%">STATUS</th>
	  <th width="15%">AC</th>
	</tr>
  </thead>
  </table>
</div>
