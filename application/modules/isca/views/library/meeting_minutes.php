<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#">Library</a></li>
	<li><a href="#" class="active">Minutes of Board Meetings</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Minutes of Board Meetings
		<span>Manage Minutes of Board Meetings</span>
	  </h5>
	</div>
	<div class="col s6 right-align">
	  <div class="btn-group">
	    <button class="waves-effect waves-light" type="button" onclick="load_datatable('minutes_table','<?php echo PROJECT_ISCA?>/isca_board_minutes/get_minutes_meeting_list')"><i class="flaticon-arrows97"></i> Refresh</button>
	  </div>
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="meeting_minutes" id="add_minutes_meeting" name="add_minutes_meeting" onclick="modal_init()">Create New</button>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
	<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="minutes_table">
		<thead>
			<tr>
				<th width="20%">DATE</th>
				<th width="20%">TIME</th>
				<th width="20%">MEETING</th>
				<th width="20%">STATUS</th>
				<th width="15%"></th>
			</tr>
		</thead>
	</table>
</div>
<script>
var deleteObj = new handleData({ 
		controller 	: 'isca_board_minutes', 
		method 		: 'delete_minutes', 
		module		: '<?php echo PROJECT_ISCA ?>' 
	}); 
</script
