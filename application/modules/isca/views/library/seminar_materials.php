<div class="page-title">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#">Library</a></li>
	<li><a href="#" class="active">Seminar Materials</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Seminar Materials
		<span>Manage Seminar Materials</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    <button class="waves-effect waves-light" type="button" onclick="load_datatable('arbitrator_table','<?php echo PROJECT_ISCA?>/isca_seminar_materials/get_seminar_material_list')"><i class="flaticon-arrows97"></i> Refresh</button>
	  </div>
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="seminar_materials" id="add_arbitrator" name="add_arbitrator" onclick="modal_init()">Create Seminar Material</button>
	  </div>
	</div>
  </div>
</div>

<div class="m-t-lg">
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="arbitrator_table">
  <thead>
	<tr>
	  <th width="20%">DATE</th>
	  <th width="20%">CATEGORY</th>
	  <th width="20%">TITLE</th>
	  <th width="15%">KEYWORDS</th>
	  <th width="15%">AC</th>
	</tr>
  </thead>
  </table>
</div>

