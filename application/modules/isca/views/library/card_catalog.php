<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#">Library</a></li>
	<li><a href="#" class="active">Card Catalogues</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Library
		<span>Manage Card Catalogues</span>
	  </h5>
	</div>
	<div class="col s6  right-align">
	  <div class="btn-group">
	    <button class="waves-effect waves-light" type="button" onclick="load_datatable('books_table','<?php echo PROJECT_ISCA?>/isca_books/get_catalog_list')"><i class="flaticon-arrows97"></i> Refresh</button>
	  </div>
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="card_catalog" id="add_arbitrator" name="add_arbitrator" onclick="modal_init()">Create New</button>
	  </div>
	</div>
  </div>
</div>
<div class="pre-datatable">&nbsp;</div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="books_table">
  <thead>
	<tr>
	  <th width="20%">CODE NO</th>
	  <th width="20%">TITLE</th>
	  <th width="20%">AUTHOR</th>
	  <th width="15%">PUBLICATION DATE</th>
	  <th width="15%">QTY</th>
	  <th width="10%" class="text-center">AC</th>
	</tr>
  </thead>
  </table>
</div>
<script>
	var deleteObj = new handleData({ 
		controller 	: 'isca_books', 
		method 		: 'delete_card_catalog', 
		module		: '<?php echo PROJECT_ISCA ?>'
	});
</script>
