<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#">Library</a></li>
	<li><a href="#" class="active">Board Meetings</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Board Meetings
		<span>Manage Agenda</span>
	  </h5>
	</div>
	<div class="col s6 right-align">
	  <div class="btn-group">
	    <button class="waves-effect waves-light" type="button" onclick="load_datatable('meeting_agenda','<?php echo PROJECT_ISCA?>/isca_board_meetings/get_agenda_list')"><i class="flaticon-arrows97"></i> Refresh</button>
	  </div>
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="board_meeting" id="add_meeting" name="add_meeting" onclick="modal_init()">Create New Agenda</button>
	  </div>
	</div>
  </div>
</div>
<div class="pre-datatable">&nbsp;</div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="meeting_agenda">
  <thead>
	<tr>
	  <th width="20%">DATE</th>
	  <th width="20%">MEETING</th>
	  <th width="20%">VENUE</th>
	  <th width="15%"></th>
	</tr>
  </thead>
  </table>
</div>
<script >

	var deleteObj = new handleData({ 
		controller 	: 'isca_board_meetings', 
		method 		: 'delete_meeting', 
		module		: '<?php echo PROJECT_ISCA ?>' 
	});

</script>