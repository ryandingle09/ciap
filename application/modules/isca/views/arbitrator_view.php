<div class="page-title">
	<ul id="breadcrumbs">
		<li><a href="#">Home</a></li>
		<li><a href="#" class="active">Arbitrators</a></li>
	</ul>
	<div class="row m-b-n">
		<div class="col s6 p-r-n">
			<h5>View Arbitrators
			<span>Arbitrator Details</span>
			</h5>
		</div>
		<div class="col s6 right-align">
			<div class="input-field inline p-l-md">
				<button type="button" class="btn waves-effect waves-light  btn-success"  id="add_arbitrator" name="add_arbitrator" >Back To List</button>
			</div>
		</div> 
	</div>
</div>
<form id="case_form">

<div class="m-t-lg bg-white" >
	<div class="col s12">

		<div class="container">			

			<div class="col s12" >
				<br>
				<h6>Basic Information</h6>
			</div>
			
			<div class="row">	
				<div class="col s12" style="border-bottom:1px solid #999;"></div>
			</div>

			<div class="row">	
				<div class="col s12" style="margin-top:20px">
					<div class="col s5">
						 <span style="font-weight:bold;"> NAME </span> <span style="margin-left:20px"> Sample Name </span> 
					</div>
					<div class="col s5">
						 <span style="font-weight:bold;"> SOLE </span> <span style="margin-left:20px"> Sample Name </span> 
					</div>
					<div class="col s2 right-align">
						 <a class="btn" >Reserve</a>
					</div>
				</div>
			</div>

			<div class="row">	
				<div class="col s12">
					<div class="col s5">
						 <span style="font-weight:bold;"> AREA </span> <span style="margin-left:20px"> Luzon - National Capital Region </span> 
					</div>
					<div class="col s6">
						 <span style="font-weight:bold;"> YEAR OF ACCREDITATION </span> <span style="margin-left:20px"> 2015 </span> 
					</div>
				</div>
			</div>

			<div class="row" style="padding-bottom:20px;">
				<div class="col s12">
					<div class="col s5">
						 <span style="font-weight:bold;"> POSTION/S </span> 
						 <ul>
						 	<li>Environmental Engineer</li>
						 	<li>Electornics and Communications Engineer</li>	
						 </ul>
					</div>
					
					<div class="col s3">
						 <span style="font-weight:bold;"> TOTAL MACE POINTS </span> <span style="margin-left:20px"> 3 </span> 
						 <div style="margin-top:15px;">
						 <span style="font-weight:bold;"> RESUME </span> <span style="margin-left:20px"> view </span>  <span style="margin-left:20px"> Download </span> 
						 </div>
					</div>
					<div class="col s3">
						 <span style="font-weight:bold;"> EXEMPTED ON MACE </span> <span style="margin-left:20px"> X </span> 
					</div>
				</div>
			</div>

			<div class="row">	
				<div class="col s12">
					<div class="col s12">
						<h6 >Assigned Cases</h6>
					</div>
					<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered" id="assigned_case">
						<thead>
							<tr>
								<th width="15%">DATE</th>
								<th width="15%">CASE NO</th>
								<th width="15%">CASE TITLE</th>
								<th width="15%">NATURE OF CASE</th>
								<th width="15%">ROLE</th>
								<th width="10%" class="text-center">STATUS</th>
								<th width="10%" class="text-center">AC</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>

			<div class="row">	
				<div class="col s12">
					<div class="col s12">
						<br><br>
						<h6>MACE Attended</h6>
					</div>
					<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto bordered" id="mace_attended">
						<thead>
							<tr>
								<th width="12%">DATE</th>
								<th width="12%">TIME - IN </th>
								<th width="12%">TIME - OUT</th>
								<th width="15%">VENUE</th>
								<th width="15%">PARTICIPATION HOURS	</th>
								<th width="10%" class="text-center">LECTURE HOURS</th>
								<th width="10%" class="text-center">MACE PTS</th>
								<th width="10%" class="text-center">AC</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>

</form>
<script type="text/javascript">

$(function() {


	load_datatable('assigned_case', "<?php ECHO PROJECT_ISCA.'/isca_arbitrators/get_arbitrator_details' ?>", false);
	load_datatable('mace_attended', "<?php ECHO PROJECT_ISCA.'/isca_arbitrators/get_arbitrator_mace_pts' ?>", false);
	$("#assigned_case_filter, #assigned_case_length").css("display","none");
	$("#mace_attended_filter, #mace_attended_length").css("display","none");
});

</script>
