<div class="page-title">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Disposition Status</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Status of Disposition
		<span>Manage Disposition Status</span>
	  </h5>
	</div>
  </div>
</div>

<div class="m-t-lg">
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="arbitrator_table">
  <thead>
	<tr>
	  <th width="10%">CASE NO.</th>
	  <th width="15%">CASE TITLE</th>
	  <th width="15%">END OF RETENTION PERION</th>
	  <th width="22%">TYPE OF CASE DOCUMENT</th>
	  <th width="15%">DATE DISPOSED</th>
	  <th width="15%">DATE RETURNED</th>
	  <th width="8%"></th>
	</tr>
  </thead>
  </table>
</div>
	