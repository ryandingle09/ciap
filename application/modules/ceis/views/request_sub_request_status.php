<?php 
$request_date   = ( ISSET($request_details['requested_date']) && ! EMPTY($request_details['requested_date']) && $request_details['requested_date'] != DEFAULT_DATE ) ? date('F d, Y', strtotime($request_details['requested_date'])) : '';
$request_no     = ( ISSET($request_details['request_id']) && ! EMPTY($request_details['request_id']) ) ? $request_details['request_id'] : '';
$reference_no   = ( ISSET($request_details['reference_no']) && ! EMPTY($request_details['reference_no']) ) ? $request_details['reference_no'] : '';
$request_status = ( ISSET($request_details['request_status_name']) && ! EMPTY($request_details['request_status_name']) ) ? $request_details['request_status_name'] : '';
?>
<ul class="collapsible panel" data-collapsible="expandable">
	<li>
		<div class="collapsible-header active">Request Status</div> 
    
		<div class="collapsible-body" style="display: block">
		
			<div class="row">
              <div class="input-field col s6 ">
              	<i class="material-icons prefix">date_range</i>
                <input id="application_date" name="application_date" type="text" class="black-text m-t-xs" value="<?php echo $request_date; ?>" data-parsley-required="true" disabled>
                <label for="application_date" class="grey-text-bold active">Application Date</label>
              </div>
              <div class="input-field col s6 ">
                <i class="material-icons prefix">payment</i>
                <input id="request_no" name="request_no" class="black-text m-t-xs"" value="<?php echo $request_no; ?>" type="text" data-parsley-required="true" disabled>
                <label for="request_no" class="grey-text-bold active">Request No</label>
              </div>
            </div>
		
			<div class="row">
              <div class="input-field col s6 ">
              	<i class="material-icons prefix">payment</i>
                <input id="reference_no" name="reference_no" type="text" class="black-text m-t-xs" value="<?php echo $reference_no; ?>" data-parsley-required="true" disabled>
                <label for="reference_no" class="grey-text-bold active">Reference No</label>
              </div>
              <div class="input-field col s6 ">
                <i class="material-icons prefix">hourglass_empty</i>
                <input id="reference_no" name="reference_no" class="black-text m-t-xs" value="<?php echo $request_status; ?>" type="text" data-parsley-required="true" disabled>
                <label for="reference_no" class="grey-text-bold active">Status</label>
              </div>
            </div>
		
			<div class="row">
		     	<ul class="tabs grey darken-3">
		         <li class="tab col s3"><a onclick="LoadTab.getPage('request_sub_request_status_tasks');" href="#tasks_container">Tasks</a></li>
		         <li class="tab col s3"><a onclick="LoadTab.getPage('request_sub_request_status_remarks');" href="#remarks_container">Remarks</a></li>
		         <li class="tab col s3"><a onclick="LoadTab.getPage('request_sub_request_status_attachments');" href="#attachements_container">Attachments</a></li>
  		        </ul>
			   
			    <div id="tasks_container" class="col s12"></div>
			    <div id="remarks_container" class="col s12"></div>
			    <div id="attachements_container" class="col s12"></div>
			</div>

		    	
		    <div class="row">	
		    	<div id="tab_status_content" class="col s12">
		    	</div>					
		  	</div>
        </div>
    </li>
</ul>
<script>
$(function(){
	 $('ul.tabs').tabs();
});
</script>