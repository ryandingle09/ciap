<ul class="collapsible panel" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active">Projects
    </div>
    <div class="collapsible-body row" style="display: block !important;">
      <table id="table_contractors_projects">
        <thead>
          <th>Title</th>
          <th>Location</th>
          <th>Owner</th>
          <th>Contract Cost</th>
          <th>Contract Status</th>
          <th>Authorized by POCB</th>
          <th>Last Submitted Progress Report</th>
          <th>Actions</th>
        </thead>
        <tbody>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tbody>
      </table>
    </div>
  </li>
</ul>

<ul class="collapsible panel" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active">Manpower Services
    </div>
    <div class="collapsible-body row" style="display: block !important;">
      <table id="table_contractors_services">
        <thead>
          <th>Title</th>
          <th>Location</th>
          <th>Owner</th>
          <th>Contract Cost</th>
          <th>Contract Status</th>
          <th>Authorized by POCB</th>
          <th>Last Submitted Progress Report</th>
          <th>Actions</th>
        </thead>
        <tbody>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tbody>
      </table>
    </div>
  </li>
</ul>