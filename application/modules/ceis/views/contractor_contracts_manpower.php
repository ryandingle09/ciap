<div class="row">
  <div class="col s12 p-t-sm">
    <ul class="tabs" style="overflow-x:hidden;">
      <li class="tab col s3"><a class="active" href="#manpower">Manpower Requirements</a></li>
      <li class="tab col s3"><a href="#financial">Financial</a></li>
      <li class="tab col s3"><a href="#mobalization">Mobilization</a></li>
      <li class="tab col s3"><a href="#foreignremittance">Foreign Remittance</a></li>
    </ul>
  </div>
  <div id="manpower" class="col s12">
    <ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active"> 
        </div>
        <div class="collapsible-body row" style="display: block">
          <table class="striped">
            <thead>
              <th width="33%"></th>
              <th class="right-align" width="33%">PROPESSIONAL</th>
              <th class="right-align" width="34%">SKILLED/SEMISKILLED WORKERS</th>
            </thead>
            <tbody>
              <tr>
                <td>Total Requirements</td>
                <td class="right-align"><?php echo ISSET($p_req_manpower) ? number_format($p_req_manpower,'2') : "" ?></td>
                <td class="right-align"><?php echo ISSET($s_req_manpower) ? number_format($s_req_manpower,'2') : "" ?></td>
              </tr>
              <tr>
                <td>Total No. Mobilized</td>
                <td class="right-align"><?php echo ISSET($p_mob_manpower) ? number_format($p_mob_manpower,'2') : "" ?></td>
                <td class="right-align"><?php echo ISSET($s_mob_manpower) ? number_format($s_mob_manpower,'2') : "" ?></td>
              </tr>
              <tr>
                <td>Total Manpower on-site</td>
                <td class="right-align"><?php echo ISSET($p_onsite_manpower) ? number_format($p_onsite_manpower,'2') : "" ?></td>
                <td class="right-align"><?php echo ISSET($s_onsite_manpower) ? number_format($s_onsite_manpower,'2') : "" ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </li>
    </ul>
  </div>
  <div id="financial" class="col s12">
    <ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active"> 
        </div>
        <div class="collapsible-body row" style="display: block">
          <table class="striped">
            <thead>
              <th width="33%"></th>
              <th class="right-align" width="33%">PROPESSIONAL</th>
              <th class="right-align" width="34%">SKILLED/SEMISKILLED WORKERS</th>
            </thead>
            <tbody>
              <tr>
                <td>Amount of Payroll</td>
                <td class="right-align"><?php echo ISSET($p_payroll_amount) ? number_format($p_payroll_amount,'2') : "" ?></td>
                <td class="right-align"><?php echo ISSET($s_payroll_amount) ? number_format($s_payroll_amount,'2') : "" ?></td>
              </tr>
              <tr>
                <td>Contractor Service Fee</td>
                <td class="right-align"><?php echo ISSET($p_service_fee) ? number_format($p_service_fee,'2') : "" ?></td>
                <td class="right-align"><?php echo ISSET($s_service_fee) ? number_format($s_service_fee,'2') : "" ?></td>
              </tr>
              <tr>
                <td>Others</td>
                <td class="right-align"><?php echo ISSET($p_other_fee) ? number_format($p_other_fee,'2') : "" ?></td>
                <td class="right-align"><?php echo ISSET($s_other_fee) ? number_format($s_other_fee,'2') : "" ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </li>
    </ul>
  </div>
  <div id="mobalization" class="col s12">
    <ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active">
        </div>
        <div class="collapsible-body row" style="display: block !important">
          <table class="striped" id="table_contract_mobilization">
            <thead>
              <th>Name</th>
              <th>Position</th>
              <th>Salary (USD)</th>
              <th>Contract Duration (Days)</th>
              <th>Date of Departure (Mobilized)</th>
              <th>Date of Arrival (Demobilized)</th>
              <th>Actions</th>
            </thead>
            <tbody>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tbody>
          </table>
        </div>
      </li>
    </ul>
  </div>
  <div id="foreignremittance" class="col s12">
    <ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active">  
        </div>
        <div class="collapsible-body row" style="display: block !important">
          <table class="striped" id="table_contract_foreign_remittance">
            <thead>
              <th>Date No.</th>
              <th>Amount Remitted</th>
              <th>Country of Origin</th>
              <th>Receiving Bank</th>
              <th>Actions</th>
            </thead>
            <tbody>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tbody>
          </table>
        </div>
      </li>
    </ul>
  </div>
</div>

<script>
  $(document).ready(function(){
    $('ul.tabs').tabs();
  });
</script>
