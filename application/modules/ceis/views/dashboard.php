<div class="page-title">
  <ul id="breadcrumbs">
  <li><a href="#">Home</a></li>
  <li><a href="#" class="active">Dashboard</a></li>
  </ul>
  <div class="row m-b-n">
  <div class="col s6 p-r-n">
    <h5>Dashboard
    <span>Manage Dashboard</span>
    </h5>
  </div>
  </div>
</div>
<?php echo '<pre>'.var_export($this->session->userdata, TRUE).'</pre>'; ?>
<div class="m-t-lg">
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="position_table">
  <thead>
  <tr>
    <th width="20%">Code</th>
    <th width="28%">Name</th>
    <th width="10%">Built In</th>
    <th width="30%">Assigned System/s</th>
    <th width="12%" class="text-center">Actions</th>
  </tr>
  </thead>
  </table>
</div>

<!-- Modal -->
<div id="modal_position" class="md-modal md-effect-<?php echo MODAL_EFFECT ?>">
  <div class="md-content">
  <a class="md-close icon">&times;</a>
  <h3 class="md-header">Position</h3>
  <div id="modal_position_content"></div>
  </div>
</div>
<div class="md-overlay"></div>

<script type="text/javascript">
var modalObj = new handleModal({ controller : 'positions', modal_id: 'modal_position', method: 'modal_add', module: '<?php echo PROJECT_CEIS ?>' });
  deleteObj = new handleData({ controller : 'positions', method : 'delete_position', module: '<?php echo PROJECT_CEIS ?>' });
  updateObj = new handleData({ controller : 'positions', method : 'process' });
  
</script>