<div class="page-title">
  <ul id="breadcrumbs">
    <li><a href="#">Home</a></li>
    <li><a href="#">Requests</a></li>
    <li><a href="#" class="active">Work Experience</a></li>
  </ul>

  <div class="row m-b-n">
    <div class="col s6 p-r-n">
      <h5>Work Experience (Manpower)
      <span></span>
      </h5>
    </div>

    <div class="col s6 p-r-n right-align">
      <div class="btn-group inline p-l-md">
        <a href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_work_experience"" type="button" class="btn waves-effect waves-light btn-success btn-large green darken-3"> Projects</a>
      </div>
      
      <div class="btn-group inline p-l-md">
        <a href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_work_experience_manpower" type="button" class="btn waves-effect waves-light btn-success btn-large" >Manpower Services</a>
      </div>
    </div>
  </div>
</div>

<ul class="collapsible panel m-t-lg" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active">Manpower Services
    </div>
    <div class="collapsible-body row">
      <table class="table table-striped m-b-none" id="tbl_work_experience_projects" data-ride="datatables">
        <thead>
          <tr>
            <th width="15%">Title</th>
            <th width="15%">Location</th>
            <th width="15%">Owner</th>
            <th width="15%">Contract Cost</th>
            <th width="15%">Status</th>
            <th width="15%">Action</th>
          </tr>
        </thead>

        <tbody>
          <tr>
            <td width="15%">
              <input type="text" class="form-control" data-type="phone"  data-required="true">
            </td>

            <td width="15%">
              <input type="text" class="form-control" data-type="phone"  data-required="true">
            </td>

            <td width="15%">
              <input type="text" class="form-control" data-type="phone"  data-required="true">
            </td>

            <td width="15%">
              <input type="text" class="form-control" data-type="phone"  data-required="true">
            </td>

            <td width="15%">
              <input type="text" class="form-control" data-type="phone"  data-required="true">
            </td>

            <td width="15%">
              <!-- <a href="<?php echo base_url(); ?>ceis/ceis_requests/equipment_remarks" data-toggle="ajaxModalStack" class="btn btn-sm btn-icon btn-dark"><i class="fa fa-search"></i></a> -->
              <a href="<?php echo base_url(); ?>ceis/ceis_requests/equipment_remarks" ><i class="flaticon-search95"></i></a>
              <a href="#"><i class="flaticon-arrows97"></i></a>
              <a href="#"><i class="flaticon-recycle69"></i></a>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </li>
</ul>