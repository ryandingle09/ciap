<ul class="collapsible panel" data-collapsible="expandable">
	<li>
		<div class="collapsible-header active">Request Equipment Info</div> 
    
		<div class="collapsible-body row" style="display: block">
			<div class="row">
          		<div class="col s6">
		            <div>
						<label>Type of Equipment</label>
		              <input disabled="" type="text" class="form-control" readonly="" placeholder="Type equipment_type Equipment" data-required="true" value="<?php echo $equip_details['equipment_type_name']; ?>">
		            </div>
		        </div>

		        <div class="col s6">
		            <div>
						<label>Serial Number</label>
		              <input disabled="" type="text" class="form-control" readonly="" placeholder="Serial Number" data-required="true" value="<?php echo $equip_details['serial_no']; ?>">
		            </div>
		        </div>

		        <div class="col s6">
		            <div>
						<label>Reference No</label>
		              <input disabled="" type="text" class="form-control" readonly="" placeholder="Reference No" data-required="true" value="<?php echo $equip_details['serial_no']; ?>">
		            </div>
		        </div>

		        <div class="col s6">
		            <div>
						<label>Motor No</label>
		              <input disabled="" type="text" class="form-control" readonly="" placeholder="Motor No" data-required="true" value="<?php echo $equip_details['motor_no']; ?>">
		            </div>
		        </div>

		        <div class="col s6">
		            <div>
						<label>Brand</label>
		              <input disabled="" type="text" class="form-control" readonly="" placeholder="Brand" data-required="true" value="<?php echo $equip_details['brand']; ?>">
		            </div>
		        </div>
		        <div class="col s6">
		            <div>
						<label>Color</label>
		              <input disabled="" type="text" class="form-control" readonly="" placeholder="Color" data-required="true" value="<?php echo $equip_details['color']; ?>">
		            </div>
		        </div>
		        <div class="col s6">
		            <div>
						<label>Model</label>
		              <input disabled="" type="text" class="form-control" readonly="" placeholder="Model" data-required="true" value="<?php echo $equip_details['model']; ?>">
		            </div>
		        </div>
		        <div class="col s6">
		            <div>
						<label>Year</label>
		              <input disabled="" type="text" class="form-control" readonly="" placeholder="Year" data-required="true" value="<?php echo $equip_details['year']; ?>">
		            </div>
		        </div>
		        <div class="col s6">
		            <div>
						<label>Size/Capacity</label>
		              <input disabled="" type="text" class="form-control" readonly="" placeholder="Size/Capacity" data-required="true" value="<?php echo $equip_details['capacity']; ?>">
		            </div>
		        </div>
		        <div class="col s6">
		            <div>
						<label>Flywheel HP</label>
		              <input disabled="" type="text" class="form-control" readonly="" placeholder="Flywheel HP" data-required="true" value="<?php echo $equip_details['flywheel']; ?>">
		            </div>
		        </div>
		        <div class="col s6">
		            <div>
						<label>Date Acquired</label>
		              <input disabled="" type="text" class="form-control" readonly="" placeholder="Date Acquired" data-required="true" value="<?php echo date('F d, Y', strtotime($equip_details['acquired_date'])); ?>">
		            </div>
		        </div>
		        <div class="col s6">
		            <div>
						<label>Acquisition Cost</label>
		              <input disabled="" type="text" class="form-control" readonly="" placeholder="In Processing" data-required="true" value="<?php echo $equip_details['acquisition_model']; ?>">
		            </div>
		        </div>
		        <div class="col s6">
		            <div>
						<label>Acquired From</label>
		              <input disabled="" type="text" class="form-control" readonly="" placeholder="Acquired From" data-required="true" value="<?php echo $equip_details['acquired_from']; ?>">
		            </div>
		        </div>
		        <div class="col s6">
		            <div>
						<label>Equipment Present Location</label>
		              <input disabled="" type="text" class="form-control" readonly="" placeholder="Equipment Present Location" data-required="true" value="<?php echo $equip_details['location']; ?>">
		            </div>
		        </div>
		        <div class="col s6">
		            <div>
						<label>Present Condition</label>
		              <input disabled="" type="text" class="form-control" readonly="" placeholder="Present Condition" data-required="true" value="<?php echo $equip_details['present_condition']; ?>">
		            </div>
		        </div>
		        <div class="col s6">
		            <div>
						<label>Type of Ownership</label>
		              <input disabled="" type="text" class="form-control" readonly="" placeholder="Type of Ownership" data-required="true" value="<?php echo $equip_details['ownership_type']; ?>">
		            </div>
		        </div>
			</div>
		    	
		    <div class="row">	
		    	<div id="tab_status_content" class="col s12">
		    	</div>					
		  	</div>
        </div>
    </li>
</ul>