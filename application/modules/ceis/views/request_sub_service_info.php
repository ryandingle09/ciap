<div class="row">
  <div class="col s12">
    <ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active">Project Info 
        </div>
        <div class="collapsible-body row" style="display: block">
          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">library_books</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Project Title" data-required="true" value="<?php echo ISSET($project['project_title']) ? $project['project_title'] : "" ; ?>">
              <label class="grey-text-bold active">Project Title</label>
            </div>
          </div>
          
          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">location_on</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Project Location" data-required="true" value="<?php echo ISSET($project['project_location']) ? $project['project_location'] : "" ; ?>">
              <label class="grey-text-bold active">Project Location</label>
            </div>
          </div>

          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">location_on</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Province" data-required="true" value="<?php echo ISSET($project['province']) ? $project['province'] : "" ; ?>">
              <label class="grey-text-bold active">Province</label>
            </div>
          </div>

          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">location_on</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Country" data-required="true" value="<?php echo ISSET($project['country_name']) ? $project['country_name'] : "" ; ?>">
              <label class="grey-text-bold active">Country</label>
            </div>
          </div>

          <div class="input-field col s12">
            <div>
              <i class="material-icons prefix">library_books</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="General Description of the Project" data-required="true" value="<?php echo ISSET($project['project_description']) ? $project['project_description'] : "" ; ?>">
              <label class="grey-text-bold active">General Description of the Project</label>
            </div>
          </div>

          <div class="input-field col s12">
            <div>
              <i class="material-icons prefix">library_books</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="General Scope of Work" data-required="true" value="<?php echo ISSET($project['service_scope']) ? $project['service_scope'] : "" ; ?>">
              <label class="grey-text-bold active">General Scope of Services</label>
            </div>
          </div>
          
          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">monetization_on</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Estimated Contract Price (USD)" data-required="true" value="<?php echo ISSET($project['contract_price']) ? $project['contract_price'] : "" ; ?>">
              <label class="grey-text-bold active">Estimated Contract Price (USD)</label>
            </div>
          </div>
    
          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">date_range</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Contract Duration" data-required="true" value="<?php echo ISSET($project['contract_duration']) ? $project['contract_duration'] : "" ; ?>">
              <label class="grey-text-bold active">Contract Duration</label>
            </div>
          </div>

          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">monetization_on</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Estimated Manpower Required" data-required="true" value="<?php echo ISSET($project['est_manpower_required']) ? $project['est_manpower_required'] : "" ; ?>">
              <label class="grey-text-bold active">Estimated Manpower Required</label>
            </div>
          </div>

        </div>
  
      <div class="collapsible-header active">BIDDING/NEGOTIATION</div>
      <div class="collapsible-body row" style="display: block">
        <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">date_range</i>
              <input disabled="" type="text" readonly="" placeholder="Date" data-required="true" value="<?php echo ISSET($project['bidding_date']) ? date('F d, Y', strtotime($project['bidding_date'])) : "" ; ?>">
              <label class="grey-text-bold active">Date</label>
            </div>
          </div>

          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">location_on</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Place" data-required="true" value="<?php echo ISSET($project['bidding_place']) ? $project['bidding_place'] : '' ; ?>">
              <label class="grey-text-bold active">Place</label>
            </div>
          </div>
       </div>

    <div class="collapsible-header active">FINANCIAL TERMS AND CONDITIONS
    </div>
    <div class="collapsible-body row" style="display: block">
       <div class="input-field col s6">
          <div>
            <i class="material-icons prefix">account_circle</i>
            <input disabled="" type="text" class="form-control" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($project['financed_by']) ? $project['financed_by'] : '' ; ?>">
            <label class="grey-text-bold active">Projects to be Financed by</label>
          </div>
        </div>

        <div class="input-field col s6">
          <div>
            <i class="material-icons prefix">library_books</i>
            <input disabled="" type="text" class="form-control" readonly="" placeholder="Enter Bid Bond" data-required="true" value="<?php echo ISSET($project['guarantees_required']) ? $project['guarantees_required'] : '' ; ?>">
            <label class="grey-text-bold active">Guarantees Required</label>
          </div>
        </div>
    </div>
  </li>
</ul>
