<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Contractors</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Contractors
		<span>Manage Contractors</span>
	  </h5>
	</div>
	<div class="col s6 p-r-md right-align">
	  <div class="btn-group">
	    
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable">&nbsp;</div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="contractor_table">
  <thead>
	<tr>
	  	<th width="20%">Contractor Name</th>
	    <th width="15%">Category</th>
	    <th width="10%">Reg No.</th>
	    <th width="10%">Reg Date</th>
	    <th width="10%">Expiry Date</th>
	    <th width="15%">Last Renewal Date</th>
	    <th width="10%">Status</th>
	    <th width="10%">Actions</th>
	</tr>
  </thead>
  </table>
</div>
