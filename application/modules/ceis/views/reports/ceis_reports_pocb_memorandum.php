<?php
	$for 		= 'The Chairman and Members of the Board';

	$thru 		= 'LEILANI DEL PRADO';
	$thru_pos	= 'Officer-in-Charge';

	$from 		= 'JULIET A. TERNATE';
	$from_pos	= 'Secretary II, International Monitoring Division';

	$subject 	= 'Report on the Overseas Performance of POCB Registered Contractors ';
	$date 		= date("F j, Y"); 

	$placeholder = 'placeholder';

//FOREIGN EXCHANGE REMITTANCE
	/*foreach ($submitted_foreign_exchange_info as $submitted_foreign_exchange_info): 
		//number of remitted foreign exchange 
		$foreign_remittance_count 			= $submitted_foreign_exchange_info['companies_remitted_count'];

		//total number of remmitted foreign exchange
		$total_remitted_earning 			= $submitted_foreign_exchange_info['total_remitted_earning'];
	endforeach;*/

	/*foreach ($total_foreign_exchange_info as $total_foreign_exchange_info): 
		//total foreign exchange
		$all_companies_remitted_count 		= $total_foreign_exchange_info['all_companies_remitted_count']; 

		//total number of remmitted foreign exchange
		$all_total_earning 					= $total_foreign_exchange_info['all_total_earning'];
	endforeach;*/

	/*foreach ($prev_submitted_foreign_exchange_info as $prev_submitted_foreign_exchange_info): 
		//total number of remmitted foreign exchange for previous year
		$prev_total_remitted_earning 		= $prev_submitted_foreign_exchange_info['total_remitted_earning'];
	endforeach;*/

	$diff_percent 	= (($total_remitted_earning - $prev_total_remitted_earning)/$prev_total_remitted_earning)*100;
	$diff_value 	= $total_remitted_earning - $prev_total_remitted_earning;
	$peak = '';
	
	if($total_remitted_earning > $prev_total_remitted_earning ){
		$peak = 'increased';
	}elseif($total_remitted_earning==$prev_total_remitted_earning){
		$peak = 'same';
	}else{
		$peak = 'decreased';
	}


//NEW CONTRACTS WON
	/*foreach ($projects_new_contracts_memorandum_info as $projects_new_contracts_memorandum_info):
		//total project count
		$tot_project_count = $projects_new_contracts_memorandum_info['project_contract_count'];
	endforeach;

	foreach ($service_new_contracts_memorandum_info as $service_new_contracts_memorandum_info):
		//total service count
		$tot_service_count = $service_new_contracts_memorandum_info['service_contract_count'];
	endforeach;

	foreach ($service_new_contracts_reg_memorandum_info as $service_new_contracts_reg_memorandum_info):
		//total service count
		$tot_reg_service_count = $service_new_contracts_reg_memorandum_info['reg_service_contract_count'];
	endforeach;*/

//MANPOWER ON-SITE
	/*foreach ($workers_count_memorandum_info as $workers_count_memorandum_info): 
		$tot_worker_count = $workers_count_memorandum_info['worker'];
	endforeach;*/

	/*foreach ($sum_country_onsite_info as $sum_country_onsite_info):
		$tot_country_onsite = $sum_country_onsite_info['grand_total'];
	endforeach;*/

	/*foreach ($prev_workers_count_memorandum_info as $prev_workers_count_memorandum_info): 
		$tot_prev_workers_count_memorandum_info = $prev_workers_count_memorandum_info['worker'];
	endforeach;*/


	foreach ($ind_country_onsite_info as $ind_country_onsite_info):
		$value1 = ($ind_country_onsite_info['sum_onsite']/$tot_worker_count)*100;
		$value2 = $ind_country_onsite_info['region_name'];
		
		$string1 .= $value1 .',';
		$string2 .= $value2 .',';	
	endforeach;
	
	list($perc1, $perc2) = explode(',', $string1);
	list($perc3, $perc4) = explode(',', $string2);

	$country1 = $perc3.' countries';
	$country2 = 'and the '.$perc4.' countries';
	
	$diff_onsite_percent 		= (($tot_worker_count - $tot_prev_workers_count_memorandum_info)/$tot_prev_workers_count_memorandum_info)*100;
	//$diff_onsite_percent 	= ($tot_worker_count / $diff_onsite_value) * 100;

//MANPOWER DEPLOYED
	/*foreach ($manpower_deployed_memorandum_info as $manpower_deployed_memorandum_info): 
		$tot_total_workers = $manpower_deployed_memorandum_info['total_workers'];
	endforeach;*/

	/*foreach ($prev_manpower_deployed_memorandum_info as $prev_manpower_deployed_memorandum_info): 
		$prev_tot_total_workers = $prev_manpower_deployed_memorandum_info['total_workers'];
	endforeach;
*/
	$deployed_percentage = (($tot_total_workers - $prev_tot_total_workers) / $prev_tot_total_workers) *100;
	
	$def_peak = '';
	
	if($tot_total_workers > $prev_tot_total_workers ){
		$def_peak = 'increased';
	}elseif($tot_total_workers==$prev_tot_total_workers){
		$def_peak = 'same';
	}else{
		$def_peak = 'decreased';
	}

//OUTSTANDING OVERSEAS CONTRACTS
	/*foreach ($companies_outstanding_count_info as $companies_outstanding_count_info): 
		$tot_companies_outstanding_count = $companies_outstanding_count_info['project_count'];
	endforeach;*/

	/*foreach ($project_outstanding_count_info as $project_outstanding_count_info): 
		$tot_project_outstanding_count = $project_outstanding_count_info['project_company_count'];
	endforeach;*/

	/*foreach ($project_consultant_outstanding_count_info as $project_consultant_outstanding_count_info): 
		$tot_project_consultant_outstanding_count = $project_consultant_outstanding_count_info['project_consultant_count'];
	endforeach;*/

	/*foreach ($service_consultant_outstanding_count_info as $service_consultant_outstanding_count_info): 
		$tot_service_consultant_outstanding_count = $service_consultant_outstanding_count_info['service_consultant_count'];
	endforeach;*/

	/*foreach ($project_ongoing_count_info as $project_ongoing_count_info): 
		$total_project_ongoing = $project_ongoing_count_info['total_sum'];
	endforeach;*/

	/*foreach ($service_outstanding_count_info as $service_outstanding_count_info): 
		$tot_service_outstanding_count = $service_outstanding_count_info['service_company_count'];
	endforeach;*/

	/*foreach ($prev_service_consultant_outstanding_count_info as $prev_service_consultant_outstanding_count_info): 
		$tot_prev_service_consultant_outstanding_count = $prev_service_consultant_outstanding_count_info['service_consultant_count'];
	endforeach;*/

	/*foreach ($prev_service_outstanding_count_info as $prev_service_outstanding_count_info): 
		$tot_prev_service_outstanding_count = $prev_service_outstanding_count_info['service_company_count'];
	endforeach;*/

	$outstanding_overseas_percentage = (($tot_service_outstanding_count - $tot_prev_service_outstanding_count) / $tot_prev_service_outstanding_count) *100;

	$out_peak = '';
	
	if($tot_service_outstanding_count > $tot_prev_service_outstanding_count ){
		$out_peak = 'increased';
	}elseif($tot_service_outstanding_count==$tot_prev_service_outstanding_count){
		$out_peak = 'same';
	}else{
		$out_peak = 'decreased';
	}

//COMPLETED CONTRACTS
	/*foreach ($manpower_completed_contracts_memorandum_info as $manpower_completed_contracts_memorandum_info):
		$tot_completed_service_count = $manpower_completed_contracts_memorandum_info['completed_contract'];
	endforeach;*/

	/*foreach ($project_completed_contract_memorandum_info as $project_completed_contract_memorandum_info):
		$tot_completed_project_count = $project_completed_contract_memorandum_info['completed_contract'];
	endforeach;*/
	
?>
	<div style="float: left;"  width="8.4%">
		<img src="static/images/pocb-logo.jpg">
	</div>
	<div style="float: left;"  width="84%">
		<small>
			<p align="center" style="line-height: 1;">
				<font size="2">
					Republic of the Philippines<br>
					Department of Trade and Industry<br>
					Contruction Industry Authority of the Philippines<br>
					<b style="font-family:Times New Roman;">PHILIPPINE OVERSEAS CONSTRUCTION BOARD</b><br>
					5/F, Executive Bldg. Center, Buendia cor. Makati Ave., Makati City 1200, Philippines<br>
					Telephone: 896-1831 & 33 Telefax: 896-4569 E-mail: <font style="font-family:Times New Roman;">pocb@skyinet.net or pocb2009@yahoo.com</font>
				</font>
			</p>
		</small>
	</div>
	<div style="float: left;" width="7.5%">
		<img src="static/images/dti-logo.png">
	</div>
	<br>
	<br>
	<label>
		<b>MEMORANDUM</b><br>
	</label>
	<br>
	<label>
		<b>FOR &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:</b>
	</label> &nbsp; <?php echo $for; ?>
		<p></p><br>
	<label>
		<b>THRU &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:</b> 
	</label> <b>&nbsp; <?php echo $thru; ?></b>
		<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<?php echo $thru_pos; ?></p><br>
	<label>
		<b>FROM &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:</b>
	</label> <b>&nbsp; <?php echo $from; ?></b>
		<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<?php echo $from_pos; ?></p><br>
	<label>
		<b>SUBJECT&nbsp; &nbsp;:</b>
	</label> &nbsp; <?php echo $subject; ?>
		<p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;( <?php 
			switch ($quarter) {
			    case 1:
			        $date_holder = "As of March 31";
			        break;
			    case 2:
			        $date_holder = "As of June 30";
			        break;
			    case 3:
			        $date_holder = "As of September 30";
			        break;
		        case 4:
		        	$date_holder = "As of December 31";
		        	break;
		        case 5:
			        $date_holder = "From January to December";
			        break;
			}
		?>
		<?php echo $date_holder; ?>
		<?php echo $year; ?> )
		</p>
		<br>
	<label>
		<b>DATE &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;:</b> 
	</label> &nbsp; <?php echo $date; ?>
		<p>
<b>*****************************************************************************************************************************</b></p>
	<label>
		<b>FOREIGN EXCHANGE REMITTANCE</b>
	</label>
	<br>
	<p>
		<?php echo $date_holder; ?> <?php echo $year; ?>, <?php echo $foreign_remittance_count; ?> of the <?php echo $all_companies_remitted_count; ?> POCB registered companies operating overseas have remitted a total foreign exchange earnings of <?php echo number_format($total_remitted_earning,2); ?> (please see Annex "A"). Compared with last year's report of <?php echo number_format($prev_total_remitted_earning,2); ?> for the same period, the value has <?php echo $peak; ?> by <?php echo round(str_replace('-', '', $diff_percent),2); ?>% or  <?php echo number_format($diff_value,2); ?>. 
		<?php
			if($total_remitted_earning!=$prev_total_remitted_earning){
				echo '(Not all companies have submitted their report as of this reporting).';
			}
		?>
	</p>
	<br>
	<label>
		<b>NEW CONTRACT WON</b>
	</label>
	<br>
	<p>
		<?php echo $date_holder; ?> <?php echo $year; ?>, there was <?php if($tot_project_count==0) { echo 'no'; } else { echo $tot_project_count; } ?> project contract won during the period, while <?php echo $tot_reg_service_count; ?> POCB registered contractors obtained <?php echo $tot_service_count; ?> new manpower service contracts (please see Annex "B"). These service contracts are based on man-month billings, hence the contract price cannot be exactly ascertained.
	</p>
	<br>
	<label>
		<b>MANPOWER ON-SITE</b>
	</label>
	<br>
	<p>
		<?php echo $date_holder; ?> <?php echo $year; ?>, there were <?php echo $tot_worker_count;  ?> Filipino workers employed in overseas project contracts, consultancy services and manpower service contracts (please see Annexes "A" and "D"). Of this number of workers, <?php echo $tot_country_onsite; ?> representing <?php echo round($perc1,2); ?>% and <?php echo round($perc2,2); ?>% of the total mapower on-site are distributed in the <?php echo $country1; ?> <?php echo $country2; ?>, respectively (please see Annexes "E1" and "E2").
	</p>
		
	<p>
		Compared with last year's report of <?php echo $tot_prev_workers_count_memorandum_info; ?> the total number of Filipino workers on-site has increased by <?php echo round($diff_onsite_percent,2); ?>%. (Not all companies have submitted their report as of this reporting). 
	</p>
	<br>
	<label>
		<b>MANPOWER DEPLOYED</b>
	</label>
	<br>
	<p>
		<?php echo $date_holder; ?> <?php echo $year; ?>, a total of <?php echo $tot_total_workers; ?> Filipino workers were deployeed in overseas project contracts and manpower service contracts (please see Annex "A"). Compared with last year's report with <?php echo $prev_tot_total_workers; ?> workers, the number of Filipino workers deployed overseas has <?php echo $def_peak; ?> by <?php echo round(str_replace('-', '', $deployed_percentage),2); ?>%.
	</p>
	<br>
	<label>
		<b>OUTSTANDING OVERSEAS CONTRACTS</b>
	</label>
	<br>
	<p>
		<?php echo $date_holder; ?> <?php echo $year; ?>, there are <?php echo ISSET($tot_companies_outstanding_count) ? $tot_companies_outstanding_count : 'no'; ?> POCB registered companies engaged in overseas construction activities. Of this number, <?php echo $tot_project_outstanding_count; ?> consultant is undertaking <?php echo $tot_project_consultant_outstanding_count; ?> consultancy contract the rest are manpower service contracts. With the aggregate value of one overseas contracts amounting to <?php echo number_format($total_project_ongoing,2); ?> (please see Annex "F"). In addition, <?php echo ISSET($tot_service_outstanding_count) ? $tot_service_outstanding_count : 'no'; ?> contractors are engaged in the supply of manpower services to <?php echo $tot_service_consultant_outstanding_count; ?> overseas projects of their respective foreign clients (please see Annex "G").
	</p>
	<p>
		Compared to last year, there were <?php echo $tot_prev_service_outstanding_count; ?> contractors undertaking <?php echo $tot_service_consultant_outstanding_count; ?> service contracts, the number of service contracts has <?php echo $out_peak; ?> by <?php echo $outstanding_overseas_percentage; ?>%.
	</p>
	<br>
	<label>
		<b>COMPLETE CONTRACTS</b>
	</label>
	<br>
	<p>
		<?php echo $date_holder; ?> <?php echo $year; ?>, there were <?php if($tot_completed_service_count==0){ echo 'no';} else{ echo $tot_completed_service_count; } ?> manpower service contracts completed. (please see Annex "H").
	</p>
	<p>
		During the same period last year, <?php if($tot_completed_project_count==0){ echo 'no';} else{ echo $tot_completed_project_count; } ?> project contract was completed.
	</p>
	<p>
		For the information of the Board.
	</p>