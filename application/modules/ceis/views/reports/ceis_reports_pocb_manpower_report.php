<?php
	$ctr = 1;

	foreach ($manpower_info as $manpower_info_temp):
		$total_manpower_info_temp 		+= $manpower_info_temp['onsite_manpower'];
	endforeach;
?>
<div align="right">
	<label>
		<b>Annex "D"</b>
	</label>
</div>
<div>
	<p align="center">
		<b>PHILIPPINE OVERSEAS CONSTRUCTION BOARDS<br>Manpower Report on Services Contracts</b>
	</p>
	<p align="center">
		<b>
		<?php 
			switch ($quarter) {
			    case 1:
			        echo "as of March 31,";
			        break;
			    case 2:
			        echo "as of June 30,";
			        break;
			    case 3:
			        echo "as of September 30,";
			        break;
		        case 4:
		        	echo "as of December 31,";
		        	break;
		        case 5:
			        echo "January to December";
			        break;
			}
		?>
		<?php echo $year; ?>
		</b>
	</p>
</div>

<div>
	<table width="100%" border="1" cellpadding="3" style="border-collapse: collapse;">
		<thead>
			<tr>
				<th width="55%" colspan="2" align="center">Name of Company</th>
				<th width="15%" align="center">Number of Contractors</th>
				<th width="15%" align="center">Manpower <br> On-site</th>
				<th width="15%" align="center">Percent % Distribution</th>
			</tr>
		</thead>

		<tbody>
			<?php
				$total_number_of_contract 	= 0;
				$total_onsite_manpower 		= 0;

				foreach ($manpower_info as $manpower_info):
			?>
					<tr>
						<td width="3%" ><?php echo $ctr; ?></td>
						<td><?php echo $manpower_info['contractor_name']; ?></td>
						<td align="right" ><?php echo $manpower_info['number_of_contract']; ?></td>
						<td align="right" ><?php echo $manpower_info['onsite_manpower']; ?></td>
						<td align="right" >
						<?php 
							$perc = ($manpower_info['onsite_manpower'] / $total_manpower_info_temp)*100;
							echo number_format($perc,2);
						?>	
						</td>
					</tr>
			<?php
				$ctr++;

				$total_number_of_contract 	+= $manpower_info['number_of_contract'];
				$total_onsite_manpower 		+= $manpower_info['onsite_manpower'];
				$total_perc 				+= $perc;
			
				// $grand_contract = ($number_of_contract/$total_number_of_contract)*100;

				endforeach;
			?>
			
		</tbody>
		<tfoot>
			<tr>
				<td align="center" colspan="2" width="5%"><b>GRAND TOTAL</b></td>
				<td align="right"  width="15%"><b><?php echo $total_number_of_contract; ?></b></td>
				<td align="right"  width="15%"><b><?php echo $total_onsite_manpower; ?></b></td>
				<td align="right"  width="15%"><b><?php echo number_format($total_perc,2); ?></b></td>	
			</tr>
		</tfoot>
	</table>
</div>