<?php 
$special_consultancy 	  = ($contractor_details['contractor_category_id'] == CONTRACTOR_CONSULTANCY) ? 'X' : '';
$special_contractor  	  = ($contractor_details['contractor_category_id'] == CONTRACTOR_SPECIALTY) ? 'X' : '' ;
$construction_contractor  = ($contractor_details['contractor_category_id'] == CONTRACTOR_CONSTRUCTION) ? 'X' : '' ;

// EVALUATION DETAILS
$eval_legal  	 = ($evaluation_details['legal_requirement'] == COMPLETE_FLAG) ? 'COMPLETE' : 'INCOMPLETE';
$eval_work_exp   = ($evaluation_details['work_experience'] == COMPLETE_FLAG) ? 'COMPLETE' : 'INCOMPLETE';
$eval_staff      = ($evaluation_details['staff_reqmt'] == COMPLETE_FLAG) ? 'COMPLETE' : 'INCOMPLETE';
$eval_committee  = ($evaluation_details['committee_recommend'] == APPROVE_FLAG) ? 'APPROVED' : 'DISAPPROVED';

//FINANCIAL DETAILS
$statement_date  = ( ISSET($financial_details['statement_date']) && ! EMPTY($financial_details['statement_date']) && $financial_details['statement_date'] != DEFAULT_DATE) ? date('d F Y', strtotime($financial_details['statement_date'])) : '';
$paid_up  	   	 = (ISSET($financial_details['paid_up']) && ! EMPTY($financial_details['paid_up'])) ? decimal_format($financial_details['paid_up']) : 0.00;
$networth 		 = (ISSET($financial_details['networth']) && ! EMPTY($financial_details['networth'])) ? decimal_format($financial_details['networth']) : 0.00;

//CONTRACTOR DETAILS 
$year 			 = date('Y', strtotime($contractor_details['last_renewal_date']));  
$capital_stock 	 = (ISSET($financial_details['common_shares']) && ! EMPTY($financial_details['common_shares'])) ? $financial_details['common_shares'] : 0.00;
$par_value 	   	 = (ISSET($financial_details['par_value']) && ! EMPTY($financial_details['par_value'])) ? $financial_details['par_value'] : 0;
$auth_cap_stock  = round($capital_stock * $par_value, 2);


?>

<div>
	<div>
		<div align="left">
			<b>POCB FORM 01-E</b><br>
			<b>MEMORANDUM FOR THE PHILIPPINE OVERSEAS CONSTRUCTION BOARD</b>
			<br><br>
		</div>
		<div style="float:left;">
			<div style="width:50%; float:left;">
				Subject: Summary Evaluation for <?php echo $request_details['request_type_name']; ?>   of <b><?php echo $contractor_details['contractor_name']; ?></b><br>
			</div>

			<div style="width:50%; float:left;">
				Date: 11 January 2016<br>
			</div>
		</div>
	</div>
	<br>
	<div>
		( <?php echo $special_consultancy; ?> ) Specialized Consultancy ( <?php echo $construction_contractor; ?> ) Construction Contractor ( <?php echo $special_contractor; ?> ) Specialty Contractor
	</div>
	<div>===================================================================================</div>
	<table width="100%" cellpadding="3">
		<thead>
			<tr>
				<td colspan="2" width="100%"><b>1.</b> The Evaluation Committee submits hereunder its findings and recommendations:</td>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; <b>1.1 LEGAL REQUIREMENTS</b></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Supporting documents on the legal requirements</td>
				<td><b><?php echo $eval_legal; ?></b></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; <b>1.2 WORK EXPERIENCE</b></td>
				<td></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; No. of years of actual construction contract operation</td>
				<td>
					<b>
<?php
						if(EMPTY($evaluation_details['total_actual_constr_year'])):
							echo 'NONE';
						else:
							echo strtoupper(number_to_words($evaluation_details['total_actual_constr_year'])).' ( '.$evaluation_details['total_actual_constr_year'].' ) ';
						endif; 
?>
					</b>
				</td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; No. of successfully completed contracts during the period costing P10 million or greater &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; for construction contractors or P1 million or greater for specialized consultancy or P5 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  million or greater for specialized consultancy or P5 million or greater for specialty constractor (for the last 5 years)</td>
				<td>
					<b>
<?php
						if(EMPTY($evaluation_details['total_completed_contracts'])):
							echo 'NONE';
						else:
							echo strtoupper(number_to_words($evaluation_details['total_completed_contracts'])).' ( '.$evaluation_details['total_completed_contracts'].' ) ';
						endif; 
?>
					</b>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; No. of contracts rescinded by the owner for cause(s) not satisfactorily explained</td>
				<td>
					<b>
<?php
						if(EMPTY($evaluation_details['total_rescinded_contracts'])):
							echo 'NONE';
						else:
							echo strtoupper(number_to_words($evaluation_details['total_rescinded_contracts'])).' ( '.$evaluation_details['total_rescinded_contracts'].' ) ';
						endif; 
?>
					</b>
				
				</td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Work Experience Requirements for Registration</td>
				<td><b><?php echo $eval_work_exp; ?></b></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; <b>1.3 STAFFING</b></td>
				<td></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; No. of key management/technical personnel employed on a full time basis</td>
				<td>
					<b>
<?php
						if(EMPTY($evaluation_details['total_staff'])):
							echo 'NONE';
						else:
							echo strtoupper(number_to_words($evaluation_details['total_staff'])).' ( '.$evaluation_details['total_staff'].' ) ';
						endif; 
?>
					</b>
				</td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; Staffing Requirements for Registration</td>
				<td><b><?php echo $eval_staff; ?></b></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; <b>1.4 FINANCE</b></td>
				<td></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; As of <?php echo $statement_date; ?>, Paid-Up Capital is </td>
				<td><b>P <?php echo $paid_up; ?></b></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Networth is </td>
				<td><b>P <?php echo $networth; ?></b></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; <b>1.5 CLASSIFICATION</b></td>
				<td></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Annual average work volume for the last three (3) years Specialization: <br>
<?php 
				foreach($contractor_class as $key => $val):
					if($val['specialty_flag'] == 0) continue; 
				
					echo $val['license_classification_name'].'<br>';
				endforeach;
?>
				</td>
				<td><b>P <?php echo $work_volume; ?></b></td>
			</tr>
			<tr>
				<td><b>2.</b> In view of the foregoing, the Evaluation Committee recommends that the subject application be</td>
				<td><b><?php echo $eval_committee; ?></b></td>
			</tr>

		</tbody>
	</table>
</div>
<br><br>
	<div>
		<div align="center" style="float:left" colspan="2" >
			<div align="center" style="width:50%; float:left;">
				<b><i>Evaluated by:</i></b><br> <br><b>ROSALIE O. IGAY</b><br>  Marketing Specialist
			</div>
			<div align="center" style="width:50%; float:right;">
				<b><i>Reviewed by:</i></b><br> <br><b>LEILANI D.L. DEL PRADO</b><br> POCB & PDCB, Officer-in-Chanrge
			</div>
		</div>
	</div>
	<div>
		<div align="center" colspan="2">
			<b><i>Endorsed by:</i></b><br><br>
			<b>SONIA T. VALDEAVILLA</b><br>
			CIAP, OFFICER-in-Charge & POCB Board Secretary
		</div>
	</div>
<pagebreak />
<div>
	<div>
		<div align="center">
			<b>GENERAL INFORMATION</b>
			<br>
			<br>
		</div>
		<div>
			COMPANY: <b><u><?php echo $contractor_details['contractor_name']; ?></b>
		</div>
		<br>
		<div>
			ADDRESS: <?php echo $contractor_details['contractor_address']; ?>
		</div>
		<br>
		<div style="float:left;">
			<div style="width:50%; float:left;">
				SEC REGISTRATION NO: <?php echo $contractor_details['sec_reg_no']; ?><br><br><br>
				CONTRACTOR'S LICENSE NO.: <?php echo $contractor_details['license_no']; ?><br>
			</div>

			<div style="width:50%; float:left;">
				Date: <?php echo date('d F, Y', strtotime($contractor_details['license_issue_date']))?><br><br>
				Date: <?php echo date('d F, Y', strtotime($contractor_details['last_renewal_date'])); ?><br>
								
				&nbsp; &nbsp; (CFY <?php echo  $year; ?> - <?php echo ($year + 1); ?> )<br>
				Category: <?php echo $contractor_details['license_category_code']; ?>
			</div>
			<div>
				Classification: <br>
<?php 
				foreach($contractor_class as $key => $val):
					if($val['specialty_flag'] == 1) continue;
				
					echo $val['license_classification_name'].'<br>';
				endforeach;
?>
			</div>
		</div>
	</div>
	<br>
	<div>
		<b>CAPITALIZATION AS OF 30 September 2014</b><br>
		<div align="left" style="width:50%; float:left;">
			&nbsp; &nbsp; &nbsp;Authorized Capital Stock <?php echo $capital_stock; ?> shares with a &nbsp; &nbsp; &nbsp; <?php echo $par_value; ?> of Php1.00 per share<br>
			&nbsp; &nbsp; &nbsp;Paid-in Capital<br>
			&nbsp; &nbsp; &nbsp;Networth
		</div>
		<div align="center" style="width:50%; float:left;">
			P <?php echo $auth_cap_stock;?><br><br>
			P <?php echo $paid_up; ?><br>
			P <?php echo $networth; ?><br>
		</div>
	</div>
	<h5>STOCKHOLDERS</h5>
	<table border="1" style="width:100%;" align="center" cellpadding="3">
		<thead>
			<tr>
				<td align="center"><b>NAME</b></td>
				<td align="center"><b>NATIONALITY</b></td>
				<td align="center"><b>AMOUNT PAID-IN</b></td>
				<td align="center"><b>% TO TOTAL</b></td>
			</tr>
		</thead>

		<tbody>
<?php 
			//$tr = '<tr><td colspan="3">No records</td></tr>';
			$tr = '';
			$total_percent = 0;
			$total_amount  = 0;
			foreach($staff as $key => $val):
				if($val['staff_type'] != STAFF_TYPE_PRINCIPAL) continue;
			
				$percent = ( round(($val['paid_in_amount'] / $paid_up), 2) * 100);
				$tr .= '
				<tr>
					<td>'.$val['staff_name'].'</td>
					<td align="center">Filipino</td>
					<td align="right">P '.decimal_format($val['paid_in_amount']).'</td>
					<td align="right">'.$percent.'</td>
				</tr>';
				
				$total_amount  += $val['paid_in_amount'];
				$total_percent += $percent;
			endforeach;
			
			if(EMPTY($tr)) :
				$tr  = '<tr><td colspan="4">No stockholders</td></tr>';
			else:
				$tr .= '
				<tr>
					<td align="center"><b>TOTAL</b></td>
					<td align="center"><b></b></td>
					<td align="right"><b>P '.decimal_format($total_amount).'</b></td>
					<td align="right"><b>'.$total_percent.'%</b></td>
				</tr>';
			endif;
			
			echo $tr;
?>		
		</tbody>
	</table>
	<h5>DIRECTORS AND PRINCIPAL OFFICERS</h5>
	<table border="1" style="width:100%;" align="center" cellpadding="3">
		<thead>
			<tr>
				<td align="center"><b>NAME</b></td>
				<td align="center"><b>NATIONALITY</b></td>
				<td align="center"><b>POSITION</b></td>
			</tr>
		</thead>

		<tbody>
<?php 
			//$tr = '<tr><td colspan="3">No records</td></tr>';
			$tr = '';
			foreach($staff as $key => $val):
				if($val['staff_type'] != STAFF_TYPE_DIRECTOR) continue;
			
			
				$tr .= '
				<tr>
					<td>'.$val['staff_name'].'</td>
					<td align="center">Filipino</td>
					<td align="right">'.$val['position'].'</td>
				</tr>';
			endforeach;
			
			if(EMPTY($tr)) :
				$tr  = '<tr><td colspan="3">No directors</td></tr>';
			endif;
			
			echo $tr;
?>		
		</tbody>
	</table>
	<pagebreak />
	<div>
		<div align="left">
			<b>POCB FORM 01-E-a</b><br><br>
			<b>CHECKLIST FOR EVALUATING LEGAL CONSTRUCTION, INC.</b><br><br>
			<b>Application: <?php echo $contractor_details['contractor_name']; //kevin?></b><br><br>
		</div>
	</div>
	<div>
		<div align="left">
			A. Verified Data by comparing data on application form (POCB Form01) with supporting documents<br>
			(✓) Yes (&nbsp;&nbsp;) No<br><br>
		</div>
	</div>
	<div>
		<div align="left">
			B. Check the following:<br>
		</div>
	</div>
	<table style="width:100%;" align="center" cellpadding="3">
		<thead>
			<tr>
				<td></td>
				<td align="center"><b>Yes</b></td>
				<td align="center"><b>No</b></td>
			</tr>
		</thead>

		<tbody>
<?php 
			foreach($request_checklist as $key => $val):
				$yes = ($val['complied_flag'] == COMPLIED_YES_FLAG) ? 'X' : '';
				$no  = ( ! ISSET($val['complied_flag']) ||$val['complied_flag'] == COMPLIED_NO_FLAG) ?  'X' : '';
				echo '
					<tr>
						<td>'.$val['checklist'].'</td>
						<td>('.$yes.')</td>
						<td>('.$no.')</td>
					</tr>';
			endforeach;
?>
		</tbody>
	</table><br>
	<div>
		<div align="left">
			C. Comments and Observations:<br>
			The applicant-firm complies with the Board's legal requirements for registration.<br><br>
		</div>
		<div align="right">
			Prepared by: <b>ROSALIE O. IGAY</b><br>
		</div>
	</div>
	<pagebreak />
	<div align="left">
		<b>POCB FORM 01-E-b</b><br><br>
		<b>CHECKLIST FOR EVALUATING WORK EXPERIENCE</b>
		<br>
		<br>
	</div>
	<div>
		Name of Company: <b><u><?php echo $contractor_details['contractor_name']; ?></b>
	</div>
	<br>
	<div>
		NO. OF PROJECTS LISTED IN APPLICATION <b><u>12</u></b>
	</div>
	<h5>I. MAJOR OF PROJECT(S) COMPLETES FOR THE LAST FIVE YEARS</h5>
	<table border="1" style="width:100%;" align="center" cellpadding="3">
		<thead>
			<tr>
				<td><b>Name of Project/Location</b></td>
				<td><b>Client</b></td>
				<td><b>Contract Amount</b> (Php M)</td>
				<td><b>Date Completed</b><br> As od Dec. 2015</td>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td align="right">Cabulig Hydro Electric Project, Claveria, Misamis Oriental</td>
				<td align="right">Mindanao Energy Systems, Inc.</td>
				<td align="right"><b>2013</b></td>
				<td align="center"><b>2014</b></td>
			</tr>
			<tr>
				<td align="right">Cabulig Hydro Electric Project, Claveria, Misamis Oriental</td>
				<td align="right">Mindanao Energy Systems, Inc.</td>
				<td align="right"><b>2013</b></td>
				<td align="center"><b>2014</b></td>
			</tr>
			<tr>
				<td colspan="4" align="left"><b>OVERSEAS</b></td>
			</tr>
			<tr>
				<td align="right">Cabulig Hydro Electric Project, Claveria, Misamis Oriental</td>
				<td align="right">Mindanao Energy Systems, Inc.</td>
				<td align="right"><b>2013</b></td>
				<td align="center"><b>2014</b></td>
			</tr>
			<tr>
				<td align="right">Cabulig Hydro Electric Project, Claveria, Misamis Oriental</td>
				<td align="right">Mindanao Energy Systems, Inc.</td>
				<td align="right"><b>2013</b></td>
				<td align="center"><b>2014</b></td>
			</tr>
		</tbody>
	</table>
	<h5>II. ON-GOING PROJECTS</h5>
	<table border="1" style="width:100%;" align="center" cellpadding="3">
		<thead>
			<tr>
				<td><b>Project Title/Location</b></td>
				<td><b>Owner</b></td>
				<td><b>Contract Price</b><br>(Php Millions)</td>
				<td><b>Project Duration</b><br>Started/Completed</td>
				<td><b>% Accomp.</b><br>as of Dec. 2015</td>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td align="right">Cabulig Hydro Electric Project, Claveria, Misamis Oriental</td>
				<td align="right">Mindanao Energy Systems, Inc.</td>
				<td align="right"><b>2013</b></td>
				<td align="center"><b>2014</b></td>
				<td align="center"><b>2014</b></td>
			</tr>

			<tr>
				<td colspan="4" align="left"><b>OVERSEAS</b></td>
			</tr>
			<tr>
				<td align="left">Xayaburi Hydro-Electrical Power Project, Laos</td>
				<td align="center">CH Karnchang</td>
				<td align="right">Php 1.640 B</td>
				<td align="center">Feb. 2012/ May 2018</td>
				<td align="center">21%</td>
			</tr>
		</tbody>
	</table>
	<pagebreak />
	<div align="left">
		<b>POCB FORM 01-E-c</b><br><br>
		<b>CHECKLIST FOR EVALUATING KEY PERSONNEL OF ORGANIZATION</b>
		<br>
		<br>
	</div>
	<div>
		Name of Company: <b><u><?php echo $contractor_details['contractor_name']; ?></b>
	</div>
	<br>
	<div>
		NO. OF PROJECTS LISTED IN APPLICATION <b><u>7</u></b>
	</div>
	<h5>NO. OF KEY STAFF PERMANENTLY EMPLOYED: <b><u>7</u></b></h5>
	<table border="1" style="width:100%;" align="center" cellpadding="3">
		<thead>
			<tr>
				<td rowspan="2" align="center"><b>Name</b></td>
				<td rowspan="2" align="center"><b>Position</b></td>
				<td colspan="2" align="center"><b>Years of Experience</td>
			</tr>
			<tr>
				<td align="center"><b>Construction</b></td>
				<td align="center"><b>With Company</b></td>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td align="left">Edgardo N. Nakpil</td>
				<td align="left">Project Engineer</td>
				<td align="center">13</td>
				<td align="center">12</td>
			</tr>
			<tr>
				<td align="left">Edgardo N. Nakpil</td>
				<td align="left">Project Engineer</td>
				<td align="center">13</td>
				<td align="center">12</td>
			</tr>
			<tr>
				<td align="left">Edgardo N. Nakpil</td>
				<td align="left">Project Engineer</td>
				<td align="center">13</td>
				<td align="center">12</td>
			</tr>
			<tr>
				<td align="left">Edgardo N. Nakpil</td>
				<td align="left">Project Engineer</td>
				<td align="center">13</td>
				<td align="center">12</td>
			</tr>
		</tbody>
	</table>
	<h5>COMMENTS</h5>
	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley</p>
	<h5>RECOMMENDATION</h5>
	<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley</p>
	<div>
		<div align="right">
			Prepared by: <b>ROSALIE O. IGAY</b><br>
		</div>
	</div>
	<pagebreak />
	<div align="left">
		<b>POCB FORM 01-E-d</b><br><br>
		<b>CHECKLIST FOR EVALUATING KEY PERSONNEL OF ORGANIZATION</b>
		<br>
		<br>
	</div>
	<div>
		Applicant: <b><u>WHESSOE PHILIPPINES CONSTRUCTION, INC.</b>
	</div>
	<table style="width:100%;" align="center" cellpadding="3">
		<thead>
			<tr>
				<td></td>
				<td align="center"><b>Yes</b></td>
				<td align="center"><b>No</b></td>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td>A. Capitalization or Networth (whichever is lower)</td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;1. Construction Contractor - P5000000 or greater</td>
				<td align="center">(&nbsp;&nbsp;)</td>
				<td align="center">(&nbsp;&nbsp;)</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;2. Specialty Contractor - P1000000 or grearter</td>
				<td align="center">(✓)</td>
				<td align="center">(&nbsp;&nbsp;)</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;3. Specialized Consultancy Group - P250000 or greater</td>
				<td align="center">(&nbsp;&nbsp;)</td>
				<td align="center">(&nbsp;&nbsp;)</td>
			</tr>
			<br>
			<tr>
				<td>B. Word Volume (average work volume for the last three (3) years)</td>
				<td align="center"></td>
				<td align="center"></td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;1. Construction Contractor - work capability is US$2000000 or greater</td>
				<td align="center">(&nbsp;&nbsp;)</td>
				<td align="center">(&nbsp;&nbsp;)</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;2. Specialty Contractor - work capability is US$1000000 or grearter</td>
				<td align="center">(✓)</td>
				<td align="center">(&nbsp;&nbsp;)</td>
			</tr>
			<tr>
				<td>&nbsp;&nbsp;3. Specialized Consultancy Group - work capability is US$250000 or greater</td>
				<td align="center">(&nbsp;&nbsp;)</td>
				<td align="center">(&nbsp;&nbsp;)</td>
			</tr>
			<br>
			<tr>
				<td>C. Financial Highlights</td>
				<td align="center">(&nbsp;&nbsp;)</td>
				<td align="center">(&nbsp;&nbsp;)</td>
			</tr>
		</tbody>
	</table><br>
	<table border="1" style="width:100%;" align="center" cellpadding="3">
		<thead>
			<tr>
				<td rowspan="2" align="center"><b></b></td>
				<td colspan="3" align="center"><b>30 September (Audited)</b></td>
			</tr>
			<tr>
				<td align="center"><b><u>2012</u></b></td>
				<td align="center"><b><u>2013</u></b></td>
				<td align="center"><b><u>2014</u></b></td>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td align="left">Current Radio</td>
				<td align="center">1.19:1.00</td>
				<td align="center">8.76%</td>
				<td align="center">32.4%</td>
			</tr>
			<tr>
				<td align="left">Current Radio</td>
				<td align="center">1.19:1.00</td>
				<td align="center">8.76%</td>
				<td align="center">32.4%</td>
			</tr>
			<tr>
				<td align="left">Current Radio</td>
				<td align="center">1.19:1.00</td>
				<td align="center">8.76%</td>
				<td align="center">32.4%</td>
			</tr>
			<tr>
				<td align="left">Current Radio</td>
				<td align="center">1.19:1.00</td>
				<td align="center">8.76%</td>
				<td align="center">32.4%</td>
			</tr>
			<tr>
				<td align="left">Current Radio</td>
				<td align="center">1.19:1.00</td>
				<td align="center">8.76%</td>
				<td align="center">32.4%</td>
			</tr>
			<tr>
				<td align="left">Current Radio</td>
				<td align="center">1.19:1.00</td>
				<td align="center">8.76%</td>
				<td align="center">32.4%</td>
			</tr>
			<tr>
				<td align="left">Current Radio</td>
				<td align="center">1.19:1.00</td>
				<td align="center">8.76%</td>
				<td align="center">32.4%</td>
			</tr>
			<tr>
				<td align="left">Current Radio</td>
				<td align="center">1.19:1.00</td>
				<td align="center">8.76%</td>
				<td align="center">32.4%</td>
			</tr>
			<tr>
				<td align="left">Current Radio</td>
				<td align="center">1.19:1.00</td>
				<td align="center">8.76%</td>
				<td align="center">32.4%</td>
			</tr>
		</tbody>
	</table><br>
	<tr>
		<td>D. Comments and Observations:</td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<br>
	<tr>
		<td>&nbsp;&nbsp; The applicant-firm meets the Board's financial requirements for registration.</td>
		<td align="center"></td>
		<td align="center"></td>
	</tr>
	<div>
		<div align="right">
			Prepared by: <b>ROSALIE O. IGAY</b><br>
		</div>
	</div>
</div>


