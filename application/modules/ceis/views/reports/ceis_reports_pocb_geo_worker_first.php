<?php
	$total_onsite_manpower_temp 		= '';
	$total_serv_onsite_manpower_temp 	= '';
	$ctr 								= 1;


	foreach ($geo_region_proj_first_info as $geo_region_proj_first_info_temp):
		$total_onsite_manpower_temp += $geo_region_proj_first_info_temp['onsite_manpower'];

	endforeach;

	foreach ($geo_region_serv_first_info as $geo_region_serv_first_info_temp):
		$total_serv_onsite_manpower_temp += $geo_region_serv_first_info_temp['onsite_manpower'];

	endforeach;

	
?>
<div align="right">
	<label>
		<b>Annex "E-1"</b>
	</label>
</div>
<div>
	<p align="center">
		<b>PHILIPPINE OVERSEAS CONSTRUCTION BOARDS</b>
	</p>
	<p align="center">
		<b>Geographical Distribution of Workers<br>POCB Registered Contractors<br><b>
		<?php 
			switch ($quarter) {
			    case 1:
			        echo "as of March 31,";
			        break;
			    case 2:
			        echo "as of June 30,";
			        break;
			    case 3:
			        echo "as of September 30,";
			        break;
		        case 4:
		        	echo "as of December 31,";
		        	break;
		        case 5:
			        echo "January to December";
			        break;
			}
		?>
		<?php echo $year; ?></b>
	</p>
</div>

<div>
	<label><i><b>A. PROJECT CONTRACTS</b></i></label>
	<table width="100%" border="1" cellpadding="3" style="border-collapse: collapse;">
		<thead>
			<tr>
				<th width="35%" align="center">Name of Country</th>
				<th width="15%" align="center">Number of Contracts</th>
				<th width="25%" align="center">Manpower On-site</th>
				<th width="25%" align="center">Percent % Distribution</th>
			</tr>
		</thead>

		<tbody>
			<?php
				$total_number_contract = '';
				$total_onsite_manpower = '';
				$total_project_perc    = '';

				foreach ($geo_region_proj_first_info as $geo_region_proj_first_info):
			?>
				<tr>
					<td><?php echo $geo_region_proj_first_info['country_name']; ?></td>
					<td align="center" ><?php echo $geo_region_proj_first_info['number_of_contract']; ?></td>
					<td align="center" ><?php echo $geo_region_proj_first_info['onsite_manpower']; ?></td>
					<td align="center" >
					<?php 
						$project_perc = ($geo_region_proj_first_info['onsite_manpower']/$total_onsite_manpower_temp)*100;
						echo number_format($project_perc,2);
					?>	
					</td>
				</tr>
			<?php

				$total_number_contract 	+= $geo_region_proj_first_info['number_of_contract'];
				$total_onsite_manpower 	+= $geo_region_proj_first_info['onsite_manpower'];
				$total_project_perc		+= $project_perc;
				endforeach;
			?>
		</tbody>
		<tfoot>
			<tr>
				<td align="center" width="35%"><b>GRAND TOTAL</b></td>
				<td align="center"  width="15%"><b><?php echo $total_number_contract; ?></b></td>
				<td align="center"  width="25%"><b><?php echo $total_onsite_manpower; ?></b></td>
				<td align="center"  width="25%"><b><?php echo number_format($total_project_perc,2); ?></b></td>	
			</tr>
		</tfoot>
	</table>
</div><br>

<label><i><b>B. MANPOWER SERVICE CONTRACTS</b></i></label>
	<table width="100%" border="1" cellpadding="3" style="border-collapse: collapse;">
		<thead>
			<tr>
				<th width="35%" align="center">Name of Country</th>
				<th width="15%" align="center">Number of Contracts</th>
				<th width="25%" align="center">Manpower On-site</th>
				<th width="25%" align="center">Percent % Distribution</th>
			</tr>
		</thead>

		<tbody>
			<?php
			$total_number_contract = '';
			$total_onsite_manpower = '';
			$total_project_perc    = '';

			foreach ($geo_region_serv_first_info as $geo_region_serv_first_info):
			?>
				<tr>
					<td><?php echo $geo_region_serv_first_info['country_name']; ?></td>
					<td align="center" ><?php echo $geo_region_serv_first_info['number_of_contract']; ?></td>
					<td align="center" ><?php echo $geo_region_serv_first_info['onsite_manpower']; ?></td>
					<td align="center" >
					<?php 
						$service_perc = ($geo_region_serv_first_info['onsite_manpower']/$total_serv_onsite_manpower_temp)*100;
						echo number_format($service_perc,2);
					?>	
					</td>
				</tr>
			<?php

			$total_number_contract 	+= $geo_region_serv_first_info['number_of_contract'];
			$total_onsite_manpower 	+= $geo_region_serv_first_info['onsite_manpower'];
			$total_service_perc		+= $service_perc;

			endforeach;
			?>
		</tbody>
		<tfoot>
			<tr>
				<td align="center" width="35%"><b>GRAND TOTAL</b></td>
				<td align="center"  width="15%"><b><?php echo $total_number_contract; ?></b></td>
				<td align="center"  width="25%"><b><?php echo $total_onsite_manpower; ?></b></td>
				<td align="center"  width="25%"><b><?php echo number_format($total_service_perc,2); ?></b></td>	
			</tr>
		</tfoot>
	</table>
</div>