<?php 

define('TH', 'th');

define('TD', 'td');

define('TBL', 'tbl');


define('MODE', _ucfirst($mode_of_release));


// efine('MAIL' (MODE == 'Mail'));


if(MODE == 'Mail'){

	$label 	= 'Tracking Number';

	$key	= 'license_tracking_number';

}

if(MODE == 'Counter'){

	$label 	= 'Claimed By';

	$key 	= 'license_claimed_by';

}

function style($elem = '')

{

	$padding = '';

	$width 	 = '';

	if(!EMPTY($elem)){

		switch($elem){

			case TH:

				$padding = 'padding:15px;';		

			break;

			case TD:

				$padding = 'padding:5px;';

			break;

			case TBL:

				$padding = 'padding:5px;';

				$width 	 = 'width: 100%;';

			break;

		}

	}

	$style 	= $padding.$width;

	return $style;

}

//echo "<pre>"; echo var_export($releasing_details,TRUE) 

?>

<html>
<head>
<!-- 	<title>Releasing Sheet</title> -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url().PATH_CSS.'pdf.css'; ?>">
	<body>

		<div style="margin-bottom: 10px;width: 100%;">
			<div style="float:left;width:80%;">
			
				<div class="title">Releasing Sheet</div>
				<div class="title">Mode of Release: <?php echo MODE;  ?></div>
			</div>
		</div>

		<div style="padding: 5px;">
			<table style="<?php echo style(TBL); ?>">
				<tr>
					<th style="<?php echo style(TH); ?>width:35%;" >Name of Firm <br/><br/> Official Reference Number</th>
					<th style="<?php echo style(TH); ?>width:25%;">Authorzied Mananging Officer</th>
					<th style="<?php echo style(TH); ?>">Date of Release</th>
					<th style="<?php echo style(TH); ?>width:17%;"><?php echo $label ?></th>
					<th style="<?php echo style(TH); ?>width:20%;">Remarks</th>
				</tr>
				<?php 

					if(!EMPTY($releasing_details)){

						foreach ($releasing_details as $rd):

				?>
					<tr>
						<td style="<?php echo style(TD); ?>" ><?php echo $rd['firm_name'].' <br/><br/> '.$rd['official_ref_no'] ?></td>
						<td style="<?php echo style(TD); ?>" ><?php echo $rd['amo_name'] ?></td>
						<td style="<?php echo style(TD); ?>" ><?php echo date('m/d/Y',strtotime($rd['license_claimed_date'])); ?></td>
						<td style="<?php echo style(TD); ?>" ><?php echo $rd[$key] ?></td>
						<td style="<?php echo style(TD); ?>" ><?php echo $rd['license_release_remarks'] ?></td>
					</tr>

				<?php 
						endforeach;

					}else{

				?>

					<tr>
						<td style="<?php echo style(TD) ?>text-align:center" colspan="6">No records found/s</td>
					</tr>

				<?php } ?>
			</table>
		</div>
	</body>
</head>
</html>