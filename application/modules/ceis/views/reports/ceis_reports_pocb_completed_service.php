<div align="right">
	<label>
		<b>ANNEX "H"</b>
	</label>
</div>
<div>
	<p align="center">
		<b>PHILIPPINE OVERSEAS CONSTRUCTION BOARD</b>
	</p>
	<p align="center">
		<b>Completed Overseas Service Contract<br>
		<?php 
			switch ($quarter) {
			    case 1:
			        echo "as of March 31,";
			        break;
			    case 2:
			        echo "as of June 30,";
			        break;
			    case 3:
			        echo "as of September 30,";
			        break;
		        case 4:
		        	echo "as of December 31,";
		        	break;
		        case 5:
			        echo "January to December";
			        break;
			}
		?>
		<?php echo $year; ?></b>
	</p>
</div>

<div>
	<table width="100%" border="1" cellpadding="3" style="border-collapse: collapse;">
		<thead>
			<tr>
				<th width="30%" rowspan="2" align="center">NAME OF COMPANY/ PROJECT TITLE</th>
				<th width="15%" colspan="2" align="center">PROJECT LOCATION</th>
				<th width="15%" rowspan="2" align="center">PROJECT OWNER</th>
				<th width="15%" rowspan="2" align="center">NAME OF PRINCIPAL (S)</th>
			</tr>
			<tr>
				<th align="center">Country</th>
				<th align="center">Province</th>
			</tr>
		</thead>

		<tbody>
			<?php
			$prev_contractor = '';
			$ctr_contractor  = 1;
			
			foreach ($get_completed_service_info as $get_completed_service_info):
				if($prev_contractor != $get_completed_service_info['contractor_name'] ){
			?>	
				<tr>
					<td><b><?php echo $ctr_contractor.'. '. $get_completed_service_info['contractor_name'] ?></b></td>
					<td align="center" ></td>
					<td align="center" ></td>
					<td align="center" ></td>
					<td align="center" ></td>
				</tr>
			
			<?php
				$ctr_contractor++;
				$ctr_project  = 'a';
				}
			?>

				<tr>
					<td style="border-top:hidden;"> &nbsp; &nbsp; &nbsp; <?php echo $ctr_project.'. '; ?><?php echo $get_completed_service_info['project_title'] ?></td>
					<td style="border-top:hidden;" align="center" ><?php echo $get_completed_service_info['country_name']; ?></td>
					<td style="border-top:hidden;" align="center" ><?php echo $get_completed_service_info['province']; ?></td>
					<td style="border-top:hidden;" align="center" ><?php echo $get_completed_service_info['owner']; ?></td>
					<td style="border-top:hidden;" align="center" ><?php echo $get_completed_service_info['foreign_principal_name']; ?></td>
				</tr>
			<?php

				$prev_contractor = $get_completed_service_info['contractor_name'];
				$ctr_project++;
			endforeach;

			?>
		</tbody>
	</table>
</div><br>