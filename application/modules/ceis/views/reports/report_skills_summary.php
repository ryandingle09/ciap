<div>
	<div>
		<h4 align="center">
			<b><?php echo $contractor_name; ?></b><br>
			<b>Skill Summary<br>For the year <?php echo $year; ?></b>
		</h4>
			

	</div>

	<table width="100%" border="1" cellpadding="3" style="border-collapse: collapse;">
		<thead>
			<tr>
				<th width="25%" align="center" rowspan="2">Skill</th>
				<th width="25%" align="center" rowspan="2">Total Number</th>
				<th width="25%" align="center" colspan="2">Salary Rate</th>
				<th width="25%" align="center" rowspan="2">Average Salary Rate (US $)</th>
			</tr>
			<tr>
				<th align="center">Min.</th>
				<th align="center">Max.</th>
			</tr>
		</thead>
		<tbody>
			<?php
				
			$prev_country 	= $skill_summary['country_name'];

			foreach ($skill_summary as $skill_summary): 
			
				if($skill_summary['country_name']!=$prev_country){
				?>
					<tr>
						<td><b>&nbsp; <?php echo $skill_summary['country_name'] ?></b></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
					</tr>
			<?php
				$total_workers = 0;
				}
			?>
			<tr>
				<td style="border-top: hidden;">&nbsp; &nbsp; &nbsp; <?php  echo $skill_summary['license_classification_name']; ?></td>
				<td align="center" style="border-top: hidden;"><?php echo $skill_summary['total_number']; ?></td>
				<td align="center" style="border-top: hidden;"><?php echo $skill_summary['min_sal']; ?></td>
				<td align="center" style="border-top: hidden;"><?php echo $skill_summary['max_sal']; ?></td>
				<td align="center" style="border-top: hidden;"><?php echo round($skill_summary['avg_sal'],2); ?></td>
			</tr>
			<tr>
				<td><b>Total Number of Workers</b></td>
				<td align="center"><b><?php echo $total_workers += $skill_summary['total_number'] ?></b></td>
				<td colspan="3"></td>
			</tr>
			<?php
				$prev_country 	 = $skill_summary['country_name'];
				$grand_total	+= $skill_summary['total_number'];
			endforeach;
			?>

		</tbody>
	</table>
	<br>
	<label><b>Grand Total Number of Workers = <?php echo ISSET($grand_total) ? $grand_total : '0'; ?></b></label>
</div>