<div>
	<p align="center">
		<b>PHILIPPINE OVERSEAS CONSTRUCTION BOARD<br>DIRECTORY LIST</b>
	</p>
</div>

<div>
	<table width="100%" border="1" cellpadding="3" style="border-collapse:collapse;">
		<thead>
			<tr>
				<th width="20%" align="center"><font size="2">Contractor Name</font></th>
				<th width="20%" align="center"><font size="2">Address</font></th>
				<th width="10%" align="center"><font size="2">Telephone</font></th>
				<th width="10%" align="center"><font size="2">Email</font></th>
				<th width="10%" align="center"><font size="2">Website</font></th>
				<th width="10%" align="center"><font size="2">Specialization</font></th>
				<th width="10%" align="center"><font size="2">Overseas Experience in</font></th>
			</tr>
		</thead>

		<tbody>
			<?php
				$ctr = 1;
				foreach ($directory_list_info as $directory_list_info):
			?>
				<tr>
					<td><font size="2"><?php echo $ctr.'. '; echo $directory_list_info['contractor_name']; ?></font></td>
					<td><font size="2"><?php echo $directory_list_info['contractor_address']; ?></font></td>
					<td><font size="2"><?php echo $directory_list_info['tel_no']; ?></font></td>
					<td><font size="2"><?php echo $directory_list_info['email']; ?></font></td>
					<td><font size="2"><?php echo $directory_list_info['website']; ?></font></td>
					<td><font size="2"><?php echo $directory_list_info['license_classification_name']; ?></font></td>
					<td><font size="2"><?php echo $directory_list_info['country_name']; ?></font></td>
				</tr>
			<?php
				$ctr++;
				endforeach;
			?>
		</tbody>
		<!-- <tfoot>
			<tr>
				<td>Christian Aquino</td>
				<td>Makati City, Philippines</td>
				<td>476-322-4311</td>
				<td>christianaquino@yahoo.com</td>
				<td>christian@website.com</td>
				<td>General Engineering</td>
				<td>Brazil, Kenya, Philippines</td>
			</tr>
		</tfoot> -->
	</table>
</div>