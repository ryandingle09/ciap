<div>
	<div>
		<div align="center">
			<b>MEMORANDUM FOR THE CHAIRMAN AND THE MEMBERS OF THE BOARD</b>
			<br>
			<br>
		</div>
		<div style="float:left;">
			<div style="width:50%; float:left;">
				Subject: Summary Evaluation of the Application for Renewal of Registration of <b>ASIAN 	CONSTRUTION AND DEVELOPMENT CORPORATION</b><br>
				Classification: <b>General Contractor</b><br>
			</div>

			<div style="width:50%; float:left;">
				Date: 18 January 2016<br>
				Ref. No.: 2016-02<br>
				POCB REg. No.: 82152<br>
				Dated: 14 January 1982<br>
				Specialization: <b>Buildings, Roads, Electrical Work, Mechanical Work, Sewerage & Sewage Treatment</b>
			</div>
		</div>
	</div>
	<br>
	<br>
	<table width="100%" cellpadding="3">
		<thead>
			<tr>
				<td colspan="2" width="100%"><b>I.</b> The Evaluation Committee submits hereunder its findings and recommendations:</td>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; <b>A. LEGAL REQUIREMENTS</b></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Supporting documnets on the legal requirements</td>
				<td><b>COMPLETE</b></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Compliance with the legal requirements for renewal of registration</td>
				<td><b>COMPLETE</b></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; <b>B. TECHNICAL REQUIREMENTS</b></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; No. of full-time key management/technical personnel</td>
				<td><b>FOUR (4)</b></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; No. of contracts rescinded by the owner for cause(s) not satisfactorily explained</td>
				<td><b>NONE</b></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; On-going Projects</td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2">
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Number &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Value
				</td>
			</tr>
			<tr>
				<td colspan="2">
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Local  &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<b>6</b> 	 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <b>P39.60 M</b>
				</td>
			</tr>
			<tr>
				<td>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Overseas: &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				</td>
				<td></td>
			</tr>
			<tr>
				<td colspan="2">
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; Construction &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				</td>
			</tr>
			<tr>
				<td colspan="2">
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; Mapower Services &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
				</td>
			</tr>
			<tr>
				<td>
					&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Compliance with the technical requirements for renewal of registration
				</td>
				<td><b>COMPLETE</b></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; <b>C. FINANCIAL REQUIREMENTS As of 31 DECEMBER 2014</b></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Paid-Up Capital</td>
				<td><b>P<?php echo number_format(50000000,'2'); ?></b></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Networth</td>
				<td><b>P<?php echo number_format(268997608,'2'); ?></b></td>
			</tr>
			<tr>
				<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Compliance with the financial requirements for renewal of registration</td>
				<td><b>COMPLETE</b></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td><b>II.</b> In view of the foregoing, the staff recommends that the subject application be</td>
				<td><b>APPROVED</b></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td></td>
				<td></td>
			</tr>
		</tbody>
	</table>
</div>
<br><br>
	<div>
		<div align="center" style="float:left" colspan="2" >
			<div align="center" style="width:50%; float:left;">
				<b><i>Evaluated by:</i></b><br> <br><b>ROSALIE O. IGAY</b><br>  Marketing Specialist
			</div>
			<div align="center" style="width:50%; float:right;">
				<b><i>Reviewed by:</i></b><br> <br><b>LEILANI D.L. DEL PRADO</b><br> POCB & PDCB, Officer-in-Chanrge
			</div>
		</div>
	</div>
	<div>
		<div align="center" colspan="2">
			<b><i>Endorsed by:</i></b><br><br>
			<b>SONIA T. VALDEAVILLA</b><br>
			CIAP, OFFICER-in-Charge & POCB Board Secretary
		</div>
	</div>
<pagebreak />
<div>
	<div>
		<div align="center">
			<b>GENERAL INFORMATION</b>
			<br>
			<br>
		</div>
		<div>
			COMPANY: <b><u>ASIAN CONSTRUCTION AND DEVELOPMENT CORPORATION</u></b>
		</div>
		<br>
		<div>
			ADDRESS: 16 Antares Street corner Jupiter Street, Barangay Bel-Air, Makati City
		</div>
		<br>
		<div style="float:left;">
			<div style="width:50%; float:left;">
				SEC REGISTRATION NO: 97870<br><br>
				CONTRACTOR'S LICENSE NO.: 6119<br>
			</div>

			<div style="width:50%; float:left;">
				Date: 10 March 1981<br><br>
				Date Issued: 29 May 1981<br>
				Date Renewed: 14 May 2015<br>
				&nbsp; &nbsp; (License Validity - CFY 2015-2016)<br>
				Category: AAA
			</div>
			<div>
				CLASSIFICATION: General Engineering<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; General Building<br>
				&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  Specialty : Electrical Work, Mechanical Work
			</div>
		</div>
	</div>
	<br>
	<div>
		<b>CAPITALIZATION AS OF 31 DECEMBER 2014</b><br>
		<div align="left" style="width:50%; float:left;">
			&nbsp; &nbsp; &nbsp;Authorized Capital Stock of 50,000,000 shares with a &nbsp; &nbsp; &nbsp; par value of Php1.00 per share<br>
			&nbsp; &nbsp; &nbsp;Paid-in Capital<br>
			&nbsp; &nbsp; &nbsp;Networth
		</div>
		<div align="center" style="width:50%; float:left;">
			P 50,000,000.00<br><br>
			P 50,000,000.00<br>
			P 268,997,608.00<br>
		</div>
	</div>
	<h5>FINANCIAL HIGHLIGHTS</h5>
	<table border="1" style="width:100%;" align="center" cellpadding="3">
		<thead>
			<tr>
				<td></td>
				<td align="center" colspan="2"><b>31 December (Audited)</b></td>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td></td>
				<td align="center"><b>2013</b></td>
				<td align="center"><b>2014</b></td>
			</tr>
			<tr>
				<td>Current Ratio</td>
				<td align="center">1.93:1.00</td>
				<td align="center">1.91:1.00</td>
			</tr>
			<tr>
				<td>Long-Term Debts/Equity Ratio</td>
				<td align="center">82.61%</td>
				<td align="center">83.25%</td>
			</tr>
			<tr>
				<td>Total Debt/Equity Ratio</td>
				<td align="center">91.35%</td>
				<td align="center">91.74%</td>
			</tr>
			<tr>
				<td>Return on Investmemnts</td>
				<td align="center">-8.99%</td>
				<td align="center">-5.23%</td>
			</tr>
			<tr>
				<td>Book Value per Share, par P 1.00</td>
				<td align="center">P5.66</td>
				<td align="center">P5.38</td>
			</tr>
			<tr>
				<td>Earnings per Share</td>
				<td align="center">(P0.51)</td>
				<td align="center">(P0.28)</td>
			</tr>
			<tr>
				<td>Gross Revenue (P000)</td>
				<td align="center">P236,426</td>
				<td align="center">P110,604</td>
			</tr>
			<tr>
				<td>Net Income (P000)</td>
				<td align="center">(P25.441)</td>
				<td align="center">(P14,067)</td>
			</tr>
			<tr>
				<td>Net Income to Gross REvenue (P000)</td>
				<td align="center">-10.76%</td>
				<td align="center">-12.72%</td>
			</tr>
		</tbody>
	</table>
	<h5>STOCKHOLDERS</h5>
	<table border="1" style="width:100%;" align="center" cellpadding="3">
		<thead>
			<tr>
				<td align="center"><b>NAME</b></td>
				<td align="center"><b>NATIONALITY</b></td>
				<td align="center"><b>AMOUNT PAID-IN</b></td>
				<td align="center"><b>% TO TOTAL</b></td>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td>Edgardo H. Angeles, Sr.</td>
				<td align="center">Filipino</td>
				<td align="right">P30000000</td>
				<td align="right">60</td>
			</tr>
			<tr>
				<td>Zenaida P. Angeles</td>
				<td align="center">Filipino</td>
				<td align="right">P30000000</td>
				<td align="right">20</td>
			</tr>
			<tr>
				<td>Anthony P. Angeles</td>
				<td align="center">Filipino</td>
				<td align="right">P30000000</td>
				<td align="right">4</td>
			</tr>
			<tr>
				<td>Ma. Estelita A. Panlilio</td>
				<td align="center">Filipino</td>
				<td align="right">P30000000</td>
				<td align="right">4</td>
			</tr>
			<tr>
				<td>Edgardo P. Angeles, Jr.</td>
				<td align="center">Filipino</td>
				<td align="right">P30000000</td>
				<td align="right">4</td>
			</tr>
			<tr>
				<td>Ma. Zenaida A. Lejano</td>
				<td align="center">Filipino</td>
				<td align="right">P30000000</td>
				<td align="right">4</td>
			</tr>
			<tr>
				<td>Denis P. Angeles</td>
				<td align="center">Filipino</td>
				<td align="right">P30000000</td>
				<td align="right">4</td>
			</tr>
			<tr>
				<td align="center"><b>TOTAL</b></td>
				<td align="center"><b></b></td>
				<td align="right"><b>P500000000</b></td>
				<td align="right"><b>100.00%</b></td>
			</tr>
		</tbody>
	</table>
	<h5>MAJOR PROJECTS COMPLETED DURING THE LAST TWO YEARS</h5>
	<table border="1" style="width:100%;" align="center" cellpadding="3">
		<thead>
			<tr>
				<td align="left"><b>PROJECT TITLE/LOCATION</b></td>
				<td align="center"><b>OWNER</b></td>
				<td align="center"><b>CONTRACT PRICE</b></td>
				<td align="center"><b>DATE COMPLETED</b></td>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td>Eau De coco coconut Water Processing Plant, Lima Technology Center, Batangas</td>
				<td>HRD(s) Pte Ltd</td>
				<td align="right">Php 12.80 M</td>
				<td align="left">Nov. 2014</td>
			</tr>
			<tr>
				<td>Eau De coco coconut Water Processing Plant, Lima Technology Center, Batangas</td>
				<td>HRD(s) Pte Ltd</td>
				<td align="right">Php 12.80 M</td>
				<td align="left">Nov. 2014</td>
			</tr>
			<tr>
				<td>Eau De coco coconut Water Processing Plant, Lima Technology Center, Batangas</td>
				<td>HRD(s) Pte Ltd</td>
				<td align="right">Php 12.80 M</td>
				<td align="left">Nov. 2014</td>
			</tr>
			<tr>
				<td>Eau De coco coconut Water Processing Plant, Lima Technology Center, Batangas</td>
				<td>HRD(s) Pte Ltd</td>
				<td align="right">Php 12.80 M</td>
				<td align="left">Nov. 2014</td>
			</tr>
			<tr>
				<td>Eau De coco coconut Water Processing Plant, Lima Technology Center, Batangas</td>
				<td>HRD(s) Pte Ltd</td>
				<td align="right">Php 12.80 M</td>
				<td align="left">Nov. 2014</td>
			</tr>
			<tr>
				<td>Eau De coco coconut Water Processing Plant, Lima Technology Center, Batangas</td>
				<td>HRD(s) Pte Ltd</td>
				<td align="right">Php 12.80 M</td>
				<td align="left">Nov. 2014</td>
			</tr>
			<tr>
				<td>Eau De coco coconut Water Processing Plant, Lima Technology Center, Batangas</td>
				<td>HRD(s) Pte Ltd</td>
				<td align="right">Php 12.80 M</td>
				<td align="left">Nov. 2014</td>
			</tr>
		</tbody>
	</table>
	<h6><b>* Major Projects Only (Php 5.0 M and Above)</b></h6>
	<h5>ON-GOING MAJOR PROJECTS</h5>
	<table border="1" style="width:100%;" align="center" cellpadding="3">
		<thead>
			<tr>
				<td align="center"><b>PROJECTS TITLE/LOCATION</b></td>
				<td align="center"><b>OWNER</b></td>
				<td align="center"><b>CONTRACT PRICE</b></td>
				<td align="center"><b>PROJECT DURATION STARTED/COMPLETED</b></td>
				<td align="center"><b>% Accomplish</b><br>(as of Jan. 2016)</td>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td>Retrofitting Works, Mezzanine Slab and Installation of Styropor Ceiling for the first priority area (Grid lines X7 - X12/Y7 -Y15) at 11.5 Hectare Building Gate 5, CEZ II, Rosario Cavite</td>
				<td>House of Technology Industries Pte., Ltd. Yoshishito Hakamata</td>
				<td align="right">Php 7.00 M</td>
				<td align="left">Sept. 2015</td>
				<td align="left">29.18%</td>
			</tr>
			<tr>
				<td>Retrofitting Works, Mezzanine Slab and Installation of Styropor Ceiling for the first priority area (Grid lines X7 - X12/Y7 -Y15) at 11.5 Hectare Building Gate 5, CEZ II, Rosario Cavite</td>
				<td>House of Technology Industries Pte., Ltd. Yoshishito Hakamata</td>
				<td align="right">Php 7.00 M</td>
				<td align="left">Sept. 2015</td>
				<td align="left">29.18%</td>
			</tr>
			<tr>
				<td>Retrofitting Works, Mezzanine Slab and Installation of Styropor Ceiling for the first priority area (Grid lines X7 - X12/Y7 -Y15) at 11.5 Hectare Building Gate 5, CEZ II, Rosario Cavite</td>
				<td>House of Technology Industries Pte., Ltd. Yoshishito Hakamata</td>
				<td align="right">Php 7.00 M</td>
				<td align="left">Sept. 2015</td>
				<td align="left">29.18%</td>
			</tr>
			<tr>
				<td>Retrofitting Works, Mezzanine Slab and Installation of Styropor Ceiling for the first priority area (Grid lines X7 - X12/Y7 -Y15) at 11.5 Hectare Building Gate 5, CEZ II, Rosario Cavite</td>
				<td>House of Technology Industries Pte., Ltd. Yoshishito Hakamata</td>
				<td align="right">Php 7.00 M</td>
				<td align="left">Sept. 2015</td>
				<td align="left">29.18%</td>
			</tr>
			<tr>
				<td>Retrofitting Works, Mezzanine Slab and Installation of Styropor Ceiling for the first priority area (Grid lines X7 - X12/Y7 -Y15) at 11.5 Hectare Building Gate 5, CEZ II, Rosario Cavite</td>
				<td>House of Technology Industries Pte., Ltd. Yoshishito Hakamata</td>
				<td align="right">Php 7.00 M</td>
				<td align="left">Sept. 2015</td>
				<td align="left">29.18%</td>
			</tr>
			<tr>
				<td>Retrofitting Works, Mezzanine Slab and Installation of Styropor Ceiling for the first priority area (Grid lines X7 - X12/Y7 -Y15) at 11.5 Hectare Building Gate 5, CEZ II, Rosario Cavite</td>
				<td>House of Technology Industries Pte., Ltd. Yoshishito Hakamata</td>
				<td align="right">Php 7.00 M</td>
				<td align="left" colspan="2">Proposal submitted July 2015/Waiting for the advice of the owner to start the project</td>
			</tr>
			<tr>
				<td>Retrofitting Works, Mezzanine Slab and Installation of Styropor Ceiling for the first priority area (Grid lines X7 - X12/Y7 -Y15) at 11.5 Hectare Building Gate 5, CEZ II, Rosario Cavite</td>
				<td>House of Technology Industries Pte., Ltd. Yoshishito Hakamata</td>
				<td align="right">Php 7.00 M</td>
				<td align="left" colspan="2">Proposal submitted July 2015/Waiting for the advice of the owner to start the project</td>
			</tr>
			<tr>
				<td>Retrofitting Works, Mezzanine Slab and Installation of Styropor Ceiling for the first priority area (Grid lines X7 - X12/Y7 -Y15) at 11.5 Hectare Building Gate 5, CEZ II, Rosario Cavite</td>
				<td>House of Technology Industries Pte., Ltd. Yoshishito Hakamata</td>
				<td align="right">Php 7.00 M</td>
				<td align="left" colspan="2">Proposal submitted July 2015/Waiting for the advice of the owner to start the project</td>
			</tr>
			<tr>
				<td>Retrofitting Works, Mezzanine Slab and Installation of Styropor Ceiling for the first priority area (Grid lines X7 - X12/Y7 -Y15) at 11.5 Hectare Building Gate 5, CEZ II, Rosario Cavite</td>
				<td>House of Technology Industries Pte., Ltd. Yoshishito Hakamata</td>
				<td align="right">Php 7.00 M</td>
				<td align="left" colspan="2">Proposal submitted July 2015/Waiting for the advice of the owner to start the project</td>
			</tr>
			<tr>
				<td>Retrofitting Works, Mezzanine Slab and Installation of Styropor Ceiling for the first priority area (Grid lines X7 - X12/Y7 -Y15) at 11.5 Hectare Building Gate 5, CEZ II, Rosario Cavite</td>
				<td>House of Technology Industries Pte., Ltd. Yoshishito Hakamata</td>
				<td align="right">Php 7.00 M</td>
				<td align="left" colspan="2">Proposal submitted July 2015/Waiting for the advice of the owner to start the project</td>
			</tr>
		</tbody>
	</table>
	<h5>DIRECTORS AND PRINCIPAL OFFICERS</h5>
	<table border="1" style="width:100%;" align="center" cellpadding="3">
		<thead>
			<tr>
				<td align="center"><b>NAME</b></td>
				<td align="center"><b>NATIONALITY</b></td>
				<td align="center"><b>POSITION</b></td>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td align="left">Eduardo H. Angeles, Sr.</td>
				<td align="center">Filipino</td>
				<td align="left">Chairman, President & CEO</td>
			</tr>
			<tr>
				<td align="left">Eduardo H. Angeles, Sr.</td>
				<td align="center">Filipino</td>
				<td align="left">Chairman, President & CEO</td>
			</tr>
			<tr>
				<td align="left">Eduardo H. Angeles, Sr.</td>
				<td align="center">Filipino</td>
				<td align="left">Chairman, President & CEO</td>
			</tr>
			<tr>
				<td align="left">Eduardo H. Angeles, Sr.</td>
				<td align="center">Filipino</td>
				<td align="left">Chairman, President & CEO</td>
			</tr>
			<tr>
				<td align="left">Eduardo H. Angeles, Sr.</td>
				<td align="center">Filipino</td>
				<td align="left">Chairman, President & CEO</td>
			</tr>
			<tr>
				<td align="left">Eduardo H. Angeles, Sr.</td>
				<td align="center">Filipino</td>
				<td align="left">Chairman, President & CEO</td>
			</tr>
		</tbody>
	</table>
	<h5>KEY TECHNICAL STAFF</h5>
	<table border="1" style="width:100%;" align="center" cellpadding="3">
		<thead>
			<tr>
				<td align="center"><b>NAME</b></td>
				<td align="center"><b>POSITION</b></td>
				<td align="center" colspan="2"><b>NUMBER OR YEARS CONST. EXPERIENCE AGGREFATE W/ COMPANY</b></td>
			</tr>
		</thead>

		<tbody>
			<tr>
				<td>Arnel B. De Mesa</td>
				<td>VP Engineering & Operations</td>
				<td>37</td>
				<td>33</td>
			</tr>
			<tr>
				<td>Mariano B. Figueras</td>
				<td>structural Design Manager</td>
				<td>36</td>
				<td>27</td>
			</tr>
			<tr>
				<td>Fernando C. Burgos</td>
				<td>Project Control Manager</td>
				<td>28</td>
				<td>10</td>
			</tr>
			<tr>
				<td>Jose Pedro G. Cabrera</td>
				<td>Project Director</td>
				<td>19</td>
				<td>19</td>
			</tr>
		</tbody>
	</table>
	<h5>COMPANY PROFILE</h5>
	<p>ASIAN CONSTRUCTON & DEVELOPMENT CORP. was registered with the Securities and Exchange Comission on March 1981. It was issued a contractor's license on 29 May 1981. It obtained its registration with the Philippine Ocerseas Construction Board on 14 January 1982.</p>
	<p>During the last two years, the last two years, the company had completes five (5) major local construction projects, the largest of which was the Dream Homes Subdivision in Gen. Tris, Cavite with a contract amount of Qatar and Angola with an aggregate contract amount of US$ 89.50 million</p>
	<p>At present, the company is undertaking six (6) local projects. The largest among these is the 9.6 ha. Perometer Fence in Brgy. Ligtong, Rosario Cavite which has a contract amount of Php 23.00 millions. The comapny also has ten (10) contratcs on hand with an aggregate amount of Php 214.00 million.</p>
</div>