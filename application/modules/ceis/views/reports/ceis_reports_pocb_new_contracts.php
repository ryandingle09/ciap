<div align="right">
	<label>
		<b>ANNEX "B"</b>
	</label>
</div>
<div>
	<p align="center">
		<b>PHILIPPINE OVERSEAS CONSTRUCTION BOARD<br>NEW CONTRACTS</b>
	</p>
	<p align="center">
		<b>
		<?php 
			switch ($quarter) {
			    case 1:
			        echo "as of March 31";
			        break;
			    case 2:
			        echo "as of June 30";
			        break;
			    case 3:
			        echo "as of September 30";
			        break;
		        case 4:
		        	echo "as of December 31";
		        	break;
		        case 5:
			        echo "January to December";
			        break;
			}
		?>
		<?php echo $year; ?></b>
	</p>
</div>

<label><b><i>A. PROJECT CONTRACTS</i></b></label>
<div>
	<table width="100%" border="1" cellpadding="3" style="border-collapse: collapse;">
		<thead>
			<tr>
				<th width="25%" align="center">Name of Company/Project Title</th>
				<th width="25%" align="center">Project Location</th>
				<th width="15%" align="center">Contract Amount (US$)</th>
				<th width="25%" align="center">Project Owner</th>
				<th width="15%" align="center">Duration</th>
			</tr>
		</thead>

		<tbody>
			<?php
				$total_contract_amount	= 0;
				$prev_contractor 		= '';
				
				foreach ($projects_new_contracts_info as $projects_new_contracts_info):
			
					if($prev_contractor != $projects_new_contracts_info['contractor_name'])
					{
				?>
						<tr>
							<td><?php echo $projects_new_contracts_info['contractor_name']; ?></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
				<?php

					}
				?>	
					<tr>
						<td style='border-top-style:hidden'>&nbsp; &nbsp; &nbsp; <?php echo $projects_new_contracts_info['project_title']; ?></td>
						<td align="center" style='border-top-style:hidden'><?php echo $projects_new_contracts_info['project_location']; ?></td>
						<td align="right" style='border-top-style:hidden'><?php echo number_format($projects_new_contracts_info['contract_cost'],'2'); ?></td>
						<td align="center" style='border-top-style:hidden'><?php echo $projects_new_contracts_info['owner']; ?></td>
						<td align="center" style='border-top-style:hidden'><?php echo $projects_new_contracts_info['contract_duration']; ?></td>
					</tr>
			<?php
					$projects_new_contracts_value 	= $projects_new_contracts_info['contract_cost'];
					$total_projects_new_contracts 	+= $projects_new_contracts_value;
					$prev_contractor = $projects_new_contracts_info['contractor_name'];

				endforeach;
			?>
		</tbody>
		<tfoot>
			<tr>
				<td></td>
				<td align="center" ><b>GRAND TOTAL</b></td>
				<td align="right"  ><b><?php echo number_format($total_projects_new_contracts,'2'); ?></b></td>
				<td><b></b></td>
				<td><b></b></td>	
			</tr>
		</tfoot>
	</table>
</div>
<br>
<br>
<label><b><i>B. MANPOWER SERVICE CONTRACTS</i></b></label>
<div>
	<table width="100%" border="1" cellpadding="3" style="border-collapse: collapse;">
		<thead>
			<tr>
				<th width="25%" rowspan="2" align="center">Name of Company/Project Title</th>
				<th width="15%" colspan="2" align="center">Project Location</th>
				<th width="15%" rowspan="2" align="center">Project Owner</th>
				<th width="15%" rowspan="2" align="center">Name of Principal</th>
				<th width="15%" colspan="3" align="center">Total Manpower</th>
				<th width="15%" rowspan="2" align="center">Total</th>
			</tr>
		</thead>

		<tbody>
			<tr>
				<th>Country</th>
				<th>Prov./City</th>
				<th>Req</th>
				<th>Mob</th>
				<th>On-site</th>
			</tr>
			<?php
				$total_contract_amount	= 0;
				$prev_contractor 		= '';
				
				foreach ($services_new_contracts_info as $services_new_contracts_info):

					if($prev_contractor != $services_new_contracts_info['contractor_name'])
					{
				?>
						<tr>
							<td><?php echo $services_new_contracts_info['contractor_name']; ?></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
				<?php

					}
			?>
					<tr>
						<td style='border-top-style:hidden'>&nbsp; &nbsp; &nbsp; <?php echo $services_new_contracts_info['project_title']; ?></td>
						<td align="center" style='border-top-style:hidden'><?php echo $services_new_contracts_info['country_name']; ?></td>
						<td align="center" style='border-top-style:hidden'><?php echo $services_new_contracts_info['province']; ?></td>
						<td align="center" style='border-top-style:hidden'><?php echo $services_new_contracts_info['owner']; ?></td>
						<td align="center" style='border-top-style:hidden'><?php echo $services_new_contracts_info['foreign_principal_name']; ?></td>
						<td align="center" style='border-top-style:hidden'><?php echo $services_new_contracts_info['req_manpower']; ?></td>
						<td align="center" style='border-top-style:hidden'><?php echo $services_new_contracts_info['mob_manpower']; ?></td>
						<td align="center" style='border-top-style:hidden'><?php echo $services_new_contracts_info['onsite_manpower']; ?></td>
						<td align="center" style='border-top-style:hidden'><?php echo $services_new_contracts_info['onsite_manpower']; ?></td>
					</tr>
			<?php
					$services_new_contracts_value 	= $services_new_contracts_info['onsite_manpower'];
					$total_services_new_contracts 	+= $services_new_contracts_value;
					$prev_contractor = $services_new_contracts_info['contractor_name'];
				endforeach;
			?>
		</tbody>
		<tfoot>
			<tr>	
				<td colspan="8" align="center"><b>GRAND TOTAL<b></td>
				<td align="center" ><b><?php echo $total_services_new_contracts ?></b></td>	
			</tr>
		</tfoot>
	</table>
</div>