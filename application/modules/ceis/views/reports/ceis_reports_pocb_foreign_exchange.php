<div align="right">
	<label>
		<b>Annex "A"</b>
	</label>
</div>
<div>
	<p align="center">
		<b>PHILIPPINE OVERSEAS CONSTRUCTION BOARDS<br>Foreign Exchange Remittances and Manpower Report</b>
	</p>
	<p align="center">
		<b>POCB Registered Contractors<br>
		<?php 
			switch ($quarter) {
			    case 1:
			        echo "as of March 31";
			        break;
			    case 2:
			        echo "as of June 30";
			        break;
			    case 3:
			        echo "as of September 30";
			        break;
		        case 4:
		        	echo "as of December 31";
		        	break;
		        case 5:
			        echo "January to December";
			        break;
			}
		?>
		<?php echo $year; ?>
		</b>
	</p>
</div>

<div>
	<table width="100%" border="1" cellpadding="3" style="border-collapse: collapse;">
		<thead>
			<tr>
				<th width="50%" rowspan="2" align="center">Name of Company</th>
				<th width="20%" rowspan="2" align="center">Foreign Exchange Remittances (US$)</th>
				<th width="30%" colspan="2" align="center">Manpower</th>
			</tr>
			<tr>
				<th align="center">Onsite</th>
				<th align="center">Deployed</th>
			</tr>
		</thead>

		<tbody>
			<?php
			$total_remittances 	= 0;
			$total_onsite 		= 0;
			$total_deployed 	= 0;

			foreach ($foreign_exchange_info as $foreign_exchange_info):
			?>
				<tr>
					<td><?php echo $foreign_exchange_info['contractor_name']; ?></td>
					<td align="right" ><?php echo number_format($foreign_exchange_info['remittances'],'2'); ?></td>
					<td align="right" ><?php echo number_format($foreign_exchange_info['onsite'],'2'); ?></td>
					<td align="right" ><?php echo number_format($foreign_exchange_info['deployed'],'2'); ?></td>
				</tr>
			<?php
				$remittances_value 	= $foreign_exchange_info['remittances'];
				$onsite_value 		= $foreign_exchange_info['onsite'];
				$deployed_value 	= $foreign_exchange_info['deployed'];

	    		$total_remittances 	+= $remittances_value;
	    		$total_onsite 		+= $onsite_value;
	    		$total_deployed 	+= $deployed_value;
			endforeach;
			?>
		</tbody>
		<tfoot>
			<tr>
				<td align="center" width="40%"><b>GRAND TOTAL</b></td>
				<td align="right"  width="30%"><b><?php echo number_format($total_remittances,'2'); ?></b></td>
				<td align="right"  width="15%"><b><?php echo number_format($total_onsite,'2'); ?></b></td>
				<td align="right"  width="15%"><b><?php echo number_format($total_deployed,'2'); ?></b></td>	
			</tr>
		</tfoot>
	</table>
</div>