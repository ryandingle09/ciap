<?php

	foreach ($get_geo_region_second_info as $get_geo_region_second_info_temp):

		$region_second_info_temp 		= $get_geo_region_second_info_temp['onsite_manpower'];
		$total_region_second_info_temp 	+= $region_second_info_temp;

	endforeach;

?>

<div align="right">
	<label>
		<b>Annex "E-2"</b>
	</label>
</div>
<div>
	<p align="center">
		<b>Geographical Distribution of Workers</b>
	</p>
	<p align="center">
		<b>POCB Registered Contractors<br>
		<?php 
			switch ($quarter) {
			    case 1:
			        echo "as of March 31,";
			        break;
			    case 2:
			        echo "as of June 30,";
			        break;
			    case 3:
			        echo "as of September 30,";
			        break;
		        case 4:
		        	echo "as of December 31,";
		        	break;
		        case 5:
			        echo "January to December";
			        break;
			}
		?>
		<?php echo $year; ?></b>
	</p>
</div>

<div>
	<table width="100%" border="1" cellpadding="3" style="border-collapse: collapse;">
		<thead>
			<tr>
				<th width="35%" align="center">Name of Regions</th>
				<th width="15%" align="center">Number of Contracts</th>
				<th width="25%" align="center">Manpower On-site</th>
				<th width="25%" align="center">Percent % Distribution</th>
			</tr>
		</thead>

		<tbody>
			<?php
			$total_geo_region_no_contract 	= 0;
			$total_geo_region_onsite 		= 0;

			foreach ($get_geo_region_second_info as $get_geo_region_second_info) {
			?>
				<tr>
					<td align="center" ><?php echo $get_geo_region_second_info['region_name']; ?></td>
					<td align="center" ><?php echo $get_geo_region_second_info['no_of_contracts']; ?></td>
					<td align="center" ><?php echo $get_geo_region_second_info['onsite_manpower']; ?></td>
					<td align="center" >
					<?php 
						$perc_dist = ($get_geo_region_second_info['onsite_manpower']/$total_region_second_info_temp)*100;
					echo number_format($perc_dist,'2'); 
					?>
					</td>
				</tr>
			<?php
				$geo_region_no_contract_value 	= $get_geo_region_second_info['no_of_contracts'];

				$geo_region_onsite_value 		= $get_geo_region_second_info['onsite_manpower'];

				$total_geo_region_no_contract	+= $geo_region_no_contract_value;
				$total_geo_region_onsite		+= $geo_region_onsite_value;
				$total_perc_dist 				+= $perc_dist;
			}
			?>
		</tbody>
		<tfoot>
			<tr>
				<td align="center" width="35%"><b>GRAND TOTAL</b></td>
				<td align="center"  width="15%"><b><?php echo $total_geo_region_no_contract; ?></b></td>
				<td align="center"  width="25%"><b><?php echo $total_geo_region_onsite; ?></b></td>
				<td align="center"  width="25%"><b><?php echo number_format($total_perc_dist,2); ?></b></td>	
			</tr>
		</tfoot>
	</table>
</div>