<div align="right">
	<label>
		<b>ANNEX "F"</b>
	</label>
</div>
<div>
	<p align="center">
		<b>PHILIPPINE OVERSEAS CONSTRUCTION BOARD</b>
	</p>
	<p align="center">
		<b>REPORT ON OVERSEAS PROJECT CONTRACTS<br>
		<?php 
			switch ($quarter) {
			    case 1:
			        echo "as of March 31,";
			        break;
			    case 2:
			        echo "as of June 30,";
			        break;
			    case 3:
			        echo "as of September 30,";
			        break;
		        case 4:
		        	echo "as of December 31,";
		        	break;
		        case 5:
			        echo "January to December";
			        break;
			}
		?>
		<?php echo $year; ?></b>
	</p>
</div>

<label><b><i>A. POCB Registered Consultants</i></b></label>
<div>
	<table width="100%" border="1" cellpadding="3" style="border-collapse: collapse;">
		<thead>
			<tr>
				<th width="30%" align="center">Name of Company/ Project Title/ Type of Services</th>
				<th width="20%" align="center">Project Location</th>
				<th width="15%" align="center">Contract Amount (US$)</th>
				<th width="20%" align="center">Client</th>
				<th width="15%" align="center">Date Start (Mo/Year)</th>
			</tr>
		</thead>

		<tbody>
			<?php 
				$prev_company			= "";
				$total_contract_cost 	= 0;

				foreach($get_report_overseas_project_info as $get_report_overseas_project_info):
					if($prev_country != $get_report_overseas_project_info['contractor_name']){
			?>
						<tr>
							<td><?php echo $get_report_overseas_project_info['contractor_name']; ?></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
			<?php

					$prev_country = $get_report_overseas_project_info['contractor_name'];
					}
			?>

					<tr>
						<td style='border-top-style:hidden'>&nbsp; &nbsp; &nbsp; <?php echo $get_report_overseas_project_info['project_title'].' - '.$get_report_overseas_project_info['project_type_name']; ?></td>
						<td align="center" style='border-top-style:hidden'><?php echo $get_report_overseas_project_info['project_location'] ?></td>
						<td align="center" style='border-top-style:hidden'><?php echo number_format($get_report_overseas_project_info['contract_cost'],2) ?></td>
						<td align="center" style='border-top-style:hidden'><?php echo $get_report_overseas_project_info['owner'] ?></td>
						<td align="center" style='border-top-style:hidden'><?php echo $get_report_overseas_project_info['started_date'] ?></td>
					</tr>
			<?php
					$total_contract_cost += $get_report_overseas_project_info['contract_cost'];
				endforeach;
			?>
		</tbody>
		<tfoot>
			<tr>
				<td align="center" colspan="2"><b>GRAND TOTAL</b></td>
				<td align="center"><b><?php echo number_format($total_contract_cost,2); ?></b></td>
				<td align="center"></td>
				<td align="right" colspan="2" ></td>	
			</tr>
		</tfoot>
	</table>
</div>