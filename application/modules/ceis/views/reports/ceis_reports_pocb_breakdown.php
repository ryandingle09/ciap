<?php 
// GRAND TOTAL CONTRACT AMOUNT
foreach ($breakdown_info as $breakdown_info_temp):

	$contract_cost_value_temp 	= $breakdown_info_temp['contract_cost'];
	$total_contract_cost_temp 	+= $contract_cost_value_temp;

endforeach;

?>
<div align="right">
	<label>
		<b>ANNEX "C"</b>
	</label>
</div>
<div>
	<p align="center">
		<b>PHILIPPINE OVERSEAS CONSTRUCTION BOARDS</b>
	</p>
	<p align="center">
		<b>BREAKDOWN OF OUTSTANDING PROJECT CONTRACTS*<br>
		<?php 
			switch ($quarter) {
			    case 1:
			        echo "as of March 31,";
			        break;
			    case 2:
			        echo "as of June 30,";
			        break;
			    case 3:
			        echo "as of September 30,";
			        break;
		        case 4:
		        	echo "as of December 31,";
		        	break;
		        case 5:
			        echo "January to December";
			        break;
			}
		?>
		<?php echo $year; ?></b>
	</p>
</div>

<div>
	<table width="100%" border="1" cellpadding="3" style="border-collapse: collapse;">
		<thead>
			<tr>
				<th width="35%" align="center">COUNTRY</th>
				<th width="15%" align="center">NO. OF PROJECTS*</th>
				<th width="25%" align="center">TOTAL CONTRACT AMOUNT (US$ M)</th>
				<th width="25%" align="center">% DISTRIBUTION</th>
			</tr>
		</thead>

		<tbody>
			<?php
				$total_no_projects		= 0;
				$total_contract_cost	= 0;

				foreach ($breakdown_info as $breakdown_info):
			?>
					<tr>
						<td><?php echo $breakdown_info['country_name']; ?></td>
						<td align="center" ><?php echo $breakdown_info['no_projects']; ?></td>
						<td align="center" ><?php echo number_format($breakdown_info['contract_cost'],'2'); ?></td>
						<td align="center" ><?php 
							$breakdown = ($breakdown_info['contract_cost']/$total_contract_cost_temp)*100;
						echo number_format($breakdown,'2'); ?>
							
						</td>
					</tr>
			<?php
					$breakdown_info_value 	= $breakdown_info['no_projects'];
					$contract_cost_value 	= $breakdown_info['contract_cost'];

					$total_no_projects 		+= $breakdown_info_value;
					$total_contract_cost 	+= $contract_cost_value;
					$total_breakdown 		+= $breakdown;
				endforeach;
			?>
		</tbody>
		<tfoot>
			<tr>
				<td align="center" width="35%"><b>TOTAL</b></td>
				<td align="center"  width="15%"><b><?php echo $total_no_projects; ?></b></td>
				<td align="center"  width="25%"><b><?php echo number_format($total_contract_cost,'2'); ?></b></td>
				<td align="center"  width="25%"><b><?php echo number_format($total_breakdown,'2'); ?></b></td>	
			</tr>
		</tfoot>
	</table>
</div><br>

<div style="line-height: 3px;">
	<p><i>* Including Consultancy Services</i></p>
	<p><i>** With additional works</i></p>
</div><br>

<div>
	<p align="center">
		<b>GEOGRAPHICAL DISTRIBUTION OF OUTSTANDING PROJECT<br>CONTRACTS PER TYPE OF PROJECT</b>
	</p>
</div>

<?php
			$row 			= "";
			$prev_country	= "";
			$grand_total 	= 0;

			$sub_total		= array();
			$country_ctr	= 0;

			foreach ($geo_dist_info as $info) {

				$country 		= $info['country_name'];
				$project 		= $info['project_type_name'];
				$amount  		= $info['tot_contract'];
				$no_project	 	= $info['no_projects'];
				// $grand_total 	= $info['grand_total'];

				if($prev_country != $country)
				{
					
					$country_ctr++;

					$row.= "<tr>";
						$row.= "<td colspan='1'>".$country."</td>";
						$row.= "<td colspan='1'></td>";
						$row.= "<td colspan='1'></td>";
						$row.= "<td colspan='1' align='right'>%.2f</td>";
					$row.= "</tr>";	
				}

				//$grand_total += $amount;
				$sub_total[$country_ctr]  = ISSET($sub_total[$country_ctr]) ? $sub_total[$country_ctr] : 0;
				$sub_total[$country_ctr] += $amount;
				
				$row.= "<tr>";
					$row.= "<td align='center' style='border-top-style:hidden'>".'&nbsp; &nbsp; &nbsp;'.$project."</td>";
					$row.= "<td align='center' style='border-top-style:hidden' align='center'>".$no_project."</td>";
					$row.= "<td align='center' style='border-top-style:hidden' align='right'>".number_format($amount,2)."</td>";
					$row.= "<td align='center' style='border-top-style:hidden'>&nbsp;</td>";
				$row.= "</tr>";

				$prev_country = $country;

				$sum_grand_total += $amount;

				$contract_amt 			= $amount;
				$geo_no_projects 		= $no_project;

				$total_contract_amt 	+= $contract_amt;
				$total_geo_no_projects 	+= $geo_no_projects;
			}
			
			$tbody	= vsprintf($row, $sub_total);
		?>

<div>
	<table width="100%" border="1" cellpadding="3" style="border-collapse: collapse;">
		<thead>
			<tr>
				<th width="40%" align="center">NAME OF COUNTRY/ TYPE OF PROJECT/S</th>
				<th width="20%" align="center">NO. OF PROJECTS</th>
				<th width="20%" align="center">CONTRACT AMT./ TYPE (US$ M)</th>
				<th width="20%" align="center">TOTAL AMT./ COUNTRY (US$ M)</th>
			</tr>
		</thead>

		<tbody>
			<?php echo $tbody; ?>
		</tbody>
		<tfoot>
			<tr>
				<td align="center" width="40%"><b>TOTAL</b></td>
				<td align="center"  width="20%"><b><?php echo $total_geo_no_projects; ?></b></td>
				<td align="right"  width="20%"><b><?php echo number_format($total_contract_amt,2); ?></b></td>
				<td align="right"  width="20%"><b><?php echo number_format($sum_grand_total,2); ?></b></td>	
			</tr>
		</tfoot>
	</table>
</div>