<div class="page-title m-b-lg">
  	<ul id="breadcrumbs">
		<li><a href="#">Home</a></li>
		<li><a href="#" class="active">Equipment</a></li>
  	</ul>
  
  	<div class="row m-b-n">
		<div class="col s6 p-r-n">
	  		<h5>Equipment
				<span>Manage Equipment</span>
	  		</h5>
		</div>
		<div class="col s6 right-align p-r-md">
	  		<div class="btn-group">
	    		
	  		</div>
		</div>
  	</div>
</div>

<div class="pre-datatable"></div>

<div>
	<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="equipment_table">
		<thead>
		<tr>
			<th width="20%">Serial no.</th>
		    <th width="30%">Type of Equipment</th>
		    <th width="40%">Name of Registrant</th>
		    <th width="10%">Actions</th>
		</tr>
		</thead>
	</table>
</div>