
      <ul class="collapsible panel" data-collapsible="expandable">
        <li>
          <div class="collapsible-header active">Finance 
          </div>
          <div class="pull-right p-sm" ><a class="md-trigger btn" data-modal='modal_view_statement' onclick="modal_view_statement_init('<?php echo $modal_url; ?>')">View Statement</a></div>
          <div class="collapsible-body row" style="display: block">
            <table id="table_finance">
              <thead>
                <th>Statement Date</th>
                <th>Assets</th>
                <th>Liabilities</th>
                <th>Net Worth</th>
                <th>Paid Up Capital</th>
                <th>Action</th>
              </thead>
              <tbody>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tbody>
            </table>
          </div>
        </li>
      </ul>
    