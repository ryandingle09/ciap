<?php 
if(ISSET($project['contract_involvement'])):
	switch($project['contract_involvement']):
		case INVOLVEMENT_TYPE_PRIME : $contract_involvement = 'Prime Contractor'; break;
		case INVOLVEMENT_TYPE_SUB   : $contract_involvement = 'Sub Contractor'; break;
		case INVOLVEMENT_TYPE_OTHER : $contract_involvement = 'Others'; break;
	endswitch;
else:
	$contract_involvement = '';
endif;
/*
 * FINANCIAL ASSISTANCE
 */
switch($project['prime_fin_assistance_flag']):
		case FINANCIAL_ASSISTANCE_OTHER : $fin_assistance = 'Others'; break;
		case FINANCIAL_ASSISTANCE_PRIME : $fin_assistance = 'Prime'; break;
		case FINANCIAL_ASSISTANCE_NONE  : $fin_assistance = 'None'; break; 
endswitch;
?>
<div class="row">
  <div class="col s12">
    <ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active">Project Info 
        </div>
        <div class="collapsible-body row" style="display: block">
          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">library_books</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Project Title" data-required="true" value="<?php echo ISSET($project['project_title']) ? $project['project_title'] : "" ; ?>">
              <label class="grey-text-bold active">Project Title</label>
            </div>
          </div>
          
          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">location_on</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Project Location" data-required="true" value="<?php echo ISSET($project['project_location']) ? $project['project_location'] : "" ; ?>">
              <label class="grey-text-bold active">Project Location</label>
            </div>
          </div>

          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">location_on</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Province" data-required="true" value="<?php echo ISSET($project['province']) ? $project['province'] : "" ; ?>">
              <label class="grey-text-bold active">Province</label>
            </div>
          </div>

          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">location_on</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Country" data-required="true" value="<?php echo ISSET($project['country_name']) ? $project['country_name'] : "" ; ?>">
              <label class="grey-text-bold active">Country</label>
            </div>
          </div>

          <div class="input-field col s12">
            <div>
              <i class="material-icons prefix">library_books</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="General Description of the Project" data-required="true" value="<?php echo ISSET($project['project_description']) ? $project['project_description'] : "" ; ?>">
              <label class="grey-text-bold active">General Description of the Project</label>
            </div>
          </div>

          <div class="input-field col s12">
            <div>
              <i class="material-icons prefix">library_books</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="General Scope of Work" data-required="true" value="<?php echo ISSET($project['scope_work']) ? $project['scope_work'] : "" ; ?>">
              <label class="grey-text-bold active">General Scope of Work</label>
            </div>
          </div>
          
          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">monetization_on</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Estimated Contract Price (USD)" data-required="true" value="<?php echo ISSET($project['contract_price']) ? $project['contract_price'] : "" ; ?>">
              <label class="grey-text-bold active">Estimated Contract Price (USD)</label>
            </div>
          </div>
    
          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">date_range</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Contract Duration" data-required="true" value="<?php echo ISSET($project['contract_duration']) ? $project['contract_duration'] : "" ; ?>">
              <label class="grey-text-bold active">Contract Duration</label>
            </div>
          </div>

          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">monetization_on</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Estimated Manpower Required" data-required="true" value="<?php echo ISSET($project['est_min_manpower_required']) ? $project['est_min_manpower_required'] : "" ; ?>">
              <label class="grey-text-bold active">Estimated Manpower Required</label>
            </div>
          </div>

          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">monetization_on</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Maximum Required" data-required="true" value="<?php echo ISSET($project['est_max_manpower_required']) ? $project['est_max_manpower_required'] : "" ; ?>">
              <label class="grey-text-bold active">&nbsp;</label>
            </div>
          </div>

          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">monetization_on</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Outward" data-required="true" value="<?php echo ISSET($project['outward_amount']) ? $project['outward_amount'] : "" ; ?>">
              <label class="grey-text-bold active">Estimated Amount of FX Remittances</label>
            </div>
          </div>

          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">monetization_on</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Inward" data-required="true" value="<?php echo ISSET($project['inward_amount']) ? $project['inward_amount'] : "" ; ?>">
              <label class="grey-text-bold active">&nbsp;</label>
            </div>
          </div>

          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">library_books</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Construction Procurement" data-required="true" value="<?php echo ISSET($project['procument_type']) ? ($project['procument_type'] == PROCUREMENT_TYPE_BIDDING) ? 'Bidding' : 'Negotiation' : '' ; ?>">
              <label class="grey-text-bold active">Construction Procurement</label>
            </div>
          </div>

          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">library_books</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Involvement" data-required="true" value="<?php echo $contract_involvement; ?>">
              <label class="grey-text-bold active">Involvement</label>
            </div>
          </div>
          
<?php if($project['contract_involvement'] == INVOLVEMENT_TYPE_SUB) :?>
           <div class="input-field col s6">
            <div>
            <i class="material-icons prefix">account_circle</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Construction Procurement" data-required="true" value="<?php echo ISSET($project['prime_contractor_name']) ? $project['prime_contractor_name'] : "" ; ?>">
              <label class="grey-text-bold active">Prime Contractor</label>
            </div>
          </div>

          <div class="input-field col s6">
            <div>
              <i class="material-icons prefix">account_circle</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Construction Procurement" data-required="true" value="<?php echo ISSET($project['country_name']) ? $project['country_name'] : "" ; ?>">
              <label class="grey-text-bold active">Nationality</label>
            </div>
          </div>
<?php endif;?>

<?php if($project['contract_involvement'] == INVOLVEMENT_TYPE_OTHER) :?>
          <div class="input-field col s12">
            <div>
              <i class="material-icons prefix">account_circle</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Construction Procurement" data-required="true" value="<?php echo ISSET($project['other_contract_involvement']) ? $project['other_contract_involvement'] : '' ; ?>">
              <label class="grey-text-bold active">Others</label>
            </div>
          </div>
<?php endif;?>
      </div>
     
      <div class="collapsible-header active">BIDDING/NEGOTIATION
      </div>
      <div class="collapsible-body row" style="display: block">
        <div class="input-field col s4">
            <div>
              <i class="material-icons prefix">date_range</i>
              <input disabled="" type="text" readonly="" placeholder="Date" data-required="true" value="<?php echo ISSET($project['bidding_date']) ? date('F d, Y', strtotime($project['bidding_date'])) : "" ; ?>">
              <label class="grey-text-bold active">Date</label>
            </div>
          </div>

          <div class="input-field col s8">
            <div>
              <i class="material-icons prefix">location_on</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Place" data-required="true" value="<?php echo ISSET($project['bidding_place']) ? $project['bidding_place'] : "" ; ?>">
              <label class="grey-text-bold active">Place</label>
            </div>
          </div>

          <div class="input-field col s12">
            <div>
              <i class="material-icons prefix">library_books</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Place" data-required="true" value="<?php echo ISSET($project['contract_type_name']) ? $project['contract_type_name'] : "" ; ?>">
              <label class="grey-text-bold active">Type of Contract</label>
            </div>
          </div>

          <div class="input-field col s12">
            <div>
              <i class="material-icons prefix">library_books</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Place" data-required="true" value="<?php echo ISSET($project['other_filipino_companies']) ? $project['other_filipino_companies'] : "" ; ?>">
              <label class="grey-text-bold active">Other Filipino Companies Bidding/Negotiation for Project</label>
            </div>
          </div>

          <div class="input-field col s12">
            <div>
              <i class="material-icons prefix">library_books</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Place" data-required="true" value="<?php echo $fin_assistance; ?>">
              <label class="grey-text-bold active">Financial Assistance/Guarantee to be Secured From</label>
            </div>
          </div>

<?php if($project['prime_fin_assistance_flag'] == FINANCIAL_ASSISTANCE_OTHER): ?>          
          <div class="input-field col s12">
            <div>
              <i class="material-icons prefix">library_books</i>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Place" data-required="true" value="<?php echo  ISSET($project['other_fin_assistance']) ? $project['other_fin_assistance'] : "" ; ?>">
              <label class="grey-text-bold active">Others Specified</label>
            </div>
          </div>
<?php endif; ?>

        </div>

    <div class="collapsible-header active">FINANCIAL TERMS AND CONDITIONS
    </div>
    <div class="collapsible-body row" style="display: block">
       <div class="input-field col s6">
          <div>
            <i class="material-icons prefix">account_circle</i>
            <input disabled="" type="text" class="form-control" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($project['financed_by']) ? $project['financed_by'] : "Undefined" ; ?>">
            <label class="grey-text-bold active">Projects to be Financed by</label>
          </div>
        </div>

        <div class="input-field col s6">
          <div>
            <i class="material-icons prefix">library_books</i>
            <input disabled="" type="text" class="form-control" readonly="" placeholder="Enter Bid Bond" data-required="true" value="<?php echo ISSET($project['bid_bond']) ? $project['bid_bond'] : "Undefined" ; ?>">
            <label class="grey-text-bold active">Bid Bond</label>
          </div>
        </div>

        <div class="input-field col s6">
          <div>
            <i class="material-icons prefix">library_books</i>
            <input disabled="" type="text" class="form-control" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($project['performance_bond']) ? $project['performance_bond'] : "Undefined" ; ?>">
            <label class="grey-text-bold active">Performance Bond</label>
          </div>
        </div>

        <div class="input-field col s6">
          <div>
            <i class="material-icons prefix">library_books</i>
            <input disabled="" type="text" class="form-control" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($project['payment_guarantee']) ? $project['payment_guarantee'] : "Undefined" ; ?>">
            <label class="grey-text-bold active">Advance Payment Guarantee</label>
          </div>
        </div>

        <div class="input-field col s6">
          <div>
            <i class="material-icons prefix">library_books</i>
            <input disabled="" type="text" class="form-control" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($project['retentions']) ? $project['retentions'] : "Undefined" ; ?>">
            <label class="grey-text-bold active">Retentions</label>
          </div>
        </div>

        <div class="input-field col s6">
          <div>
            <i class="material-icons prefix">library_books</i>
            <input disabled="" type="text" class="form-control" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($project['progress_billing']) ? $project['progress_billing'] : "Undefined" ; ?>">
            <label class="grey-text-bold active">Progress Billings</label>
          </div>
        </div>

        <div class="input-field col s12">
          <div>
            <i class="material-icons prefix">library_books</i>
            <input disabled="" type="text" class="form-control" readonly="" placeholder="Other filipino companies bidding / Negotiation for the project" data-required="true" value="<?php echo ISSET($project['others']) ? $project['others'] : "Available upon request" ; ?>">
            <label class="grey-text-bold active">Others</label>
          </div>
        </div>
    </div>
  </li>
</ul>
