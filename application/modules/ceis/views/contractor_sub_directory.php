<div class="row">
	<div class="col s4">
		<ul class="collapsible panel">
		  	<li>
		    	<div class="collapsible-header active">ABOITIZ CONSTRUCTRION GROUP, INC</div>
				    <div style="display:block !important">
					    <div>
					        <div class="col s2">
					          	<i class="flaticon-home145"></i>
					        </div>
					        <div class="col s10">
					        	Unit 3-D, (3/F) Building 2, Salem International Commercial Complex, Domestic Road, Pasay City
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>
					    
					    <div>
					        <div class="col s2">
					          	<i class="flaticon-telephone102"></i>
					        </div>
					        <div class="col s10">
					        	(632) 5778732
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>

					    <div>
					        <div class="col s2">
					          	<i class="flaticon-message15"></i>
					        </div>
					        <div class="col s10">
					        	reaboitiz@aboitiz.com.ph
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>

					    <div>
					        <div class="col s2">
					          	<i class="flaticon-link56"></i>
					        </div>
					        <div class="col s10">
					        	www.aboitizconstruction.com;<br>
					        	www.metaphil.com
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>
						
						<div>
					        <div class="col s2">
					          	<i class="flaticon-man457"></i>
					        </div>
					        <div class="col s10">
					        	Roberto E. Aboitiz, Chairman<br>
					        	Napoleon R. Pe, Jr President/CEO
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>
						
					    <div>
					        <div class="col s2">
					          	<b>SPECIALIZATION</b>
					        </div>
					        <div class="col s10"><br><br>
					        	Steel Fabrication & Assembly/Erection, Earthmoving & Soil stabilization, Housing, Civil Works, Industrial Piping works
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>
						
					    <div>
					        <div class="col s2">
					          	<b>OVERSEAS&nbsp;EXPERIENCE&nbsp;IN</b>
					        </div>
					        <div class="col s10"><br><br>
					        	Saudi Arabia, Sri lanka
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>

					</div>
			</li>
		</ul>
	</div>

	<div class="col s4">
		<ul class="collapsible panel" data-collapsible="expandable">
		  	<li>
		    	<div class="collapsible-header active">ABOITIZ CONSTRUCTRION GROUP, INC</div>
				    <div style="display:block !important">
					    <div>
					        <div class="col s2">
					          	<i class="flaticon-home145"></i>
					        </div>
					        <div class="col s10">
					        	Unit 3-D, (3/F) Building 2, Salem International Commercial Complex, Domestic Road, Pasay City
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>
					    
					    <div>
					        <div class="col s2">
					          	<i class="flaticon-telephone102"></i>
					        </div>
					        <div class="col s10">
					        	(632) 5778732
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>

					    <div>
					        <div class="col s2">
					          	<i class="flaticon-message15"></i>
					        </div>
					        <div class="col s10">
					        	reaboitiz@aboitiz.com.ph
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>

					    <div>
					        <div class="col s2">
					          	<i class="flaticon-link56"></i>
					        </div>
					        <div class="col s10">
					        	www.aboitizconstruction.com;<br>
					        	www.metaphil.com
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>
						
						<div>
					        <div class="col s2">
					          	<i class="flaticon-man457"></i>
					        </div>
					        <div class="col s10">
					        	Roberto E. Aboitiz, Chairman<br>
					        	Napoleon R. Pe, Jr President/CEO
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>
						
					    <div>
					        <div class="col s2">
					          	<b>SPECIALIZATION</b>
					        </div>
					        <div class="col s10"><br><br>
					        	Steel Fabrication & Assembly/Erection, Earthmoving & Soil stabilization, Housing, Civil Works, Industrial Piping works
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>
						
					    <div>
					        <div class="col s2">
					          	<b>OVERSEAS&nbsp;EXPERIENCE&nbsp;IN</b>
					        </div>
					        <div class="col s10"><br><br>
					        	Saudi Arabia, Sri lanka
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>

					</div>
			</li>
		</ul>
	</div>
	<div class="col s4">
		<ul class="collapsible panel" data-collapsible="expandable">
		  	<li>
		    	<div class="collapsible-header active">ABOITIZ CONSTRUCTRION GROUP, INC</div>
				    <div style="display:block !important">
					    <div>
					        <div class="col s2">
					          	<i class="flaticon-home145"></i>
					        </div>
					        <div class="col s10">
					        	Unit 3-D, (3/F) Building 2, Salem International Commercial Complex, Domestic Road, Pasay City
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>
					    
					    <div>
					        <div class="col s2">
					          	<i class="flaticon-telephone102"></i>
					        </div>
					        <div class="col s10">
					        	(632) 5778732
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>

					    <div>
					        <div class="col s2">
					          	<i class="flaticon-message15"></i>
					        </div>
					        <div class="col s10">
					        	reaboitiz@aboitiz.com.ph
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>

					    <div>
					        <div class="col s2">
					          	<i class="flaticon-link56"></i>
					        </div>
					        <div class="col s10">
					        	www.aboitizconstruction.com;<br>
					        	www.metaphil.com
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>
						
						<div>
					        <div class="col s2">
					          	<i class="flaticon-man457"></i>
					        </div>
					        <div class="col s10">
					        	Roberto E. Aboitiz, Chairman<br>
					        	Napoleon R. Pe, Jr President/CEO
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>
						
					    <div>
					        <div class="col s2">
					          	<b>SPECIALIZATION</b>
					        </div>
					        <div class="col s10"><br><br>
					        	Steel Fabrication & Assembly/Erection, Earthmoving & Soil stabilization, Housing, Civil Works, Industrial Piping works
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>
						
					    <div>
					        <div class="col s2">
					          	<b>OVERSEAS&nbsp;EXPERIENCE&nbsp;IN</b>
					        </div>
					        <div class="col s10"><br><br>
					        	Saudi Arabia, Sri lanka
					        </div>
					    </div>

					    <div class="m-t-lg">&nbsp;</div>

					</div>
			</li>
		</ul>
	</div>
</div>
