<ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active">
          Basic Information
        </div>
        <div class="collapsible-body striped row" style="display: block">
        	
	        <div class="row">
		    	<div class="input-field col s4">
		    		  <i class="material-icons prefix">account_circle</i> 	
		    		  <input id="license_cat" class="black-text m-t-xs" type="text"  value="<?php echo $staff_info['first_name']; ?>" disabled>
		    		  <label for="license_cat" class="grey-text-bold active">First Name</label>
		    	</div>
		    	<div class="input-field col s4">
		    		  <i class="material-icons prefix">account_circle</i>	
		    		  <input class="black-text m-t-xs" type="text"   value="<?php echo $staff_info['middle_name']; ?>" disabled>
		    		  <label class="grey-text-bold active">Middle Name</label>
		    	</div>
		    	<div class="input-field col s4">
		    		  <i class="material-icons prefix">account_circle</i>	
		    		  <input class="black-text m-t-xs" type="text"   value="<?php echo $staff_info['last_name']; ?>" disabled>
		    		  <label class="grey-text-bold active">Last Name</label>
		    	</div>
	    	</div>
	    	
	    	<div class="row">
	    		<div class="input-field col s12">
	    		  <i class="material-icons prefix">location_on</i>	
	    		  <input class="black-text m-t-xs" type="text"  value="<?php echo $staff_info['address']; ?>" disabled>
	    		  <label class="grey-text-bold active">Address</label>
	    		</div>
	    	</div>
	    	
	    	
	    	<div class="row">
		    	<div class="input-field col s6">
		    		  <i class="material-icons prefix">date_range</i>	
		    		  <input class="black-text m-t-xs" type="text" value="<?php echo date('F d, Y', strtotime($staff_info['birth_date'])); ?>" disabled>
		    		  <label class="grey-text-bold active">Birthday</label>
		    	</div>
		    	<div class="input-field col s6">
		    		  <i class="material-icons prefix">location_on</i>	
		    		  <input class="black-text m-t-xs" type="text"  value="<?php echo$staff_info['birth_place']; ?>" disabled>
		    		  <label class="grey-text-bold active">Birth Place</label>
		    	</div>
	    	</div>
        </div>
      </li>
      <li>
        <div class="collapsible-header active"> 
          Educational Background
        </div>
        <div class="collapsible-body striped row" style="display: block">
        
        	<div class="row">
		    	<div class="input-field col s12">
		    		  <i class="material-icons prefix">library_books</i> 	
		    		  <input id="license_cat" class="black-text m-t-xs" type="text"  value="<?php echo $staff_info['educational_attainment']; ?>" disabled>
		    		  <label for="license_cat" class="grey-text-bold active">Highest Educational Attainment</label>
		    	</div>
	    	</div>
	    	
	    	<div class="row">
		    	<div class="input-field col s8">
		    		  <i class="material-icons prefix">library_books</i> 	
		    		  <input id="license_cat" class="black-text m-t-xs" type="text"  value="<?php echo $staff_info['school_attended']; ?>" disabled>
		    		  <label for="license_cat" class="grey-text-bold active">School/University Attended</label>
		    	</div>
		    	<div class="input-field col s4">
		    		  <i class="material-icons prefix">date_range</i> 	
		    		  <input id="license_cat" class="black-text m-t-xs" type="text"  value="<?php echo $staff_info['year_graduated']; ?>" disabled>
		    		  <label for="license_cat" class="grey-text-bold active">Year Graduated</label>
		    	</div>
	    	</div>
 
        </div>
      </li>
      <li>
        <div class="collapsible-header active"> 
          Professional Licenses
        </div>
        <div class="collapsible-body row" style="display: block">
          <table class="striped">
            <thead>
              <th width="30%">Profession</th>
              <th width="20%">License No.</th>
              <th width="20%">Issued Date</th>
              <th width="20%">Expiry Date</th>
            </thead>
            <tbody>
              <?php foreach ($staff_licenses as $data): ?>
              <tr>
                <td><?php echo ISSET($data['profession']) ? $data['profession'] : ""; ?></td>
                <td><?php echo ISSET($data['license_no']) ? $data['license_no'] : ""; ?></td>
                <td><?php echo ISSET($data['issued_date']) ? $data['issued_date'] : ""; ?></td>
                <td><?php echo ISSET($data['expiry_date']) ? $data['expiry_date'] : ""; ?></td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </li>
    </ul>
