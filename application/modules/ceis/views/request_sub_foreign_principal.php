<div class="row">
  <div class="col s12">
    <ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active">Foreign Principal
        </div>
        <div class="collapsible-body row" style="display: block">
          <div class="col s6">
            <div>
              <label>Name of Foreign Principal</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Name of Foreign Principal" data-required="true" value="<?php echo ISSET($foreign_principal['foreign_principal_name']) ? $foreign_principal['foreign_principal_name'] : "" ; ?>">
            </div>
          </div>
          
          <div class="col s6">
            <div>
              <label>Nationality</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Nationality" data-required="true" value="<?php echo ISSET($foreign_principal['nationality_code']) ? $foreign_principal['nationality_code'] : "" ; ?>">
            </div>
          </div>

          <div class="col s6">
            <div>
              <label>Address</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Address" data-required="true" value="<?php echo ISSET($foreign_principal['foreign_principal_address']) ? $foreign_principal['foreign_principal_address'] : "" ; ?>">
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
</div>