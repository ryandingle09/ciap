<input type="hidden" value="<?php echo ISSET($contractor_id) ? $contractor_id : "" ; ?>">
<input type="hidden" value="<?php echo $encoded_contractor_id; ?>">
<ul class="collapsible panel" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active">REMINDER:</div>
    <div class="collapsible-body row" style="display:block !important">
      <span class="red-text text-darken-2">
        <?php
          $var = $expiry_date;
          $exp_date = strtotime($var);
          if(time() > $exp_date){
            echo '<b>This registration is already EXPIRED. Please inform your contractor to renew their registration using REMIND link or tag this as DELISTED using Change Status link below.</b>';
          }
        ?>
      </span>
      <br><br>
      <div class="divider">
      </div>
      <div class="row left col s12">
        <div class="input-field col s12">
          <input disabled placeholder="Placeholder" id="registration_no" type="text" class="validate" value="<?php echo ISSET($registration_no) ? $registration_no : ""; ?>">
          <label class="active" for="registration_no">Registration No.</label>
        </div>
  
        <div class="input-field col s6">
          <input disabled placeholder="Placeholder" id="contractor_name" type="text" class="validate" value="<?php 
          echo ISSET($contractor_name) ? $contractor_name : "";
          ?>">
          <label class="active" for="contractor_name">Contractor Name</label>
          <button class="btn waves-effect waves-light md-trigger btn-success" onclick="remindContractor('<?php echo $encoded_contractor_id; ?>')">Remind Contractor</button>
        </div>
          
        <div class="input-field col s6">
          <input disabled placeholder="Placeholder" id="status" type="text" class="validate" value="<?php echo ISSET($registration_status) ? $registration_status : ""; ?>">
          <label class="active" for="status">Status</label>

          <button class="btn waves-effect waves-light md-trigger btn-success" onclick="changeStatus('<?php echo $encoded_contractor_id; ?>')">Change Status</button>

        </div>
      </div>
    </div>
  </li>
</ul>

<ul class="collapsible panel" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active">Renewal History 
    </div>
    <div class="collapsible-body row" style="display:block !important">
      <table id="table_order_of_payment" class="striped">
        <thead>
          <tr>
            <th>Registration Date</th>
            <th>Expiry Date</th>
            <th>Last Renewal Date</th>
            <th>Status</th>
          </tr>
        </thead>
        <tbody>
              <?php foreach ($registration_list_info as $data): ?>
              <tr>
                <td <?php if($data['registration_status'] == 'Active') { echo 'class="red-text yellow darken-2"'; } ?> ><?php echo ISSET($data['registration_date'])    ? $data['registration_date'] : ""; ?></td>
                <td <?php if($data['registration_status'] == 'Active') { echo 'class="red-text yellow darken-2"'; } ?> ><?php echo ISSET($data['expiry_date'])          ? $data['expiry_date'] : ""; ?></td>
                <td <?php if($data['registration_status'] == 'Active') { echo 'class="red-text yellow darken-2"'; } ?> ><?php echo ISSET($data['last_renewal_date'])    ? $data['last_renewal_date'] : ""; ?></td>
                <td <?php if($data['registration_status'] == 'Active') { echo 'class="red-text yellow darken-2"'; } ?> ><?php echo ISSET($data['registration_status'])  ? $data['registration_status'] : ""; ?></td>
              </tr>
              <?php endforeach; ?>
            </tbody>
      </table>
    </div>
  </li>
</ul>

<script>
/*var deleteObj = new handleData({ controller : 'ceis_contractors', method : 'change_status', module: '<?php echo PROJECT_CEIS ?>' });
*/
function changeStatus(contractor_id) {
  $('#confirm_modal').confirmModal({
      topOffset : 0,
      onOkBut : function() {
         //deleteObj.removeData({ param_1 : param_1, param_2 : param_2 });

        var url = '../../../change_status/' + contractor_id;

        // alert(url);

        $.get(url, function(result){

        }, 'json');  
      },
      onCancelBut : function() {},
      onLoad : function() {
        $('.confirmModal_content h4').html('Are you sure you want to change this Status?'); 
        $('.confirmModal_content p').html('This action will send request to your Contractor\'s Email Address.');
      },
      onClose : function() {}
  });
}

function remindContractor(contractor_id) {
  $('#confirm_modal').confirmModal({
      topOffset : 0,
      onOkBut : function() {
        
        //deleteObj.removeData({ param_1 : param_1, param_2 : param_2 });

        var url = '../../../remind_contractor/' + contractor_id;

        // alert(url);

        $.get(url, function(result){

        }, 'json');        

      
      },
      onCancelBut : function() {},
      onLoad : function() {
        $('.confirmModal_content h4').html('Are you sure you want to remind Contractor for your Registration Status?'); 
        $('.confirmModal_content p').html('This action will send request to your Contractor\'s Email Address.');
      },
      onClose : function() {}
  });
}
</script>