    <ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active"> 
        </div>
        <div class="collapsible-body striped row" style="display: block">
          <table id="request_staffing_experience">
            <thead>
              <th width="25%">Inclusive Dates</th>
              <th width="25%">Positions</th>
              <th width="25%">Job Description</th>
              <th width="10%">Status</th>
              <th width="10%">Actions</th>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </li>
    </ul>