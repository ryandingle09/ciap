<ul class="collapsible panel" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active">Projects 
    </div>
    <div class="collapsible-body row" style="display: block !important;">
      <table id="table_projects">
        <thead>
          <th>Title</th>
          <th>Location</th>
          <th>Owner</th>
          <th>Contract Cost</th>
          <th>Status</th>
          <th>Action</th>
        </thead>
        <tbody>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tbody>
      </table>
    </div>
  </li>
</ul>

<ul class="collapsible panel m-t-lg" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active">Manpower Services  
    </div>
    <div class="collapsible-body row" style="display: block !important;">
      <table id="table_manpower_services">
        <thead>
          <th>Title</th>
          <th>Location</th>
          <th>Owner</th>
          <th>Duration</th>
          <th>Status</th>
          <th>Action</th>
        </thead>
        <tbody>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tbody>
      </table>
    </div>
  </li>
</ul>