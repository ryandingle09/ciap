<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Country Profile</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Country Profile
		<span>Manage Country Profile</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	   <div class="input-field inline p-l-md p-r-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_country_profile" id="add_country_profile" name="add_country_profile" onclick="modal_init()">Country Profile</button>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="country_profile_table">
  <thead>
	<tr>
		<th width="30%">Country Name</th>
	    <th width="15%">Created By</th>
	    <th width="15%">Created Date</th>
	    <th width="15%">Last Modified By</th>
	    <th width="15%">Last Modified Date</th>
	    <th width="10%">Actions</th>
	</tr>
  </thead>
  </table>
</div>

<script type="text/javascript">
var modalObj = new handleModal({ controller : 'ceis_country_profile', modal_id: 'modal_country_profile', method: 'modal', module: '<?php echo PROJECT_CEIS ?>' });
	deleteObj = new handleData({ controller : 'ceis_country_profile', method : 'delete_country_profile', module: '<?php echo PROJECT_CEIS ?>' });
	updateObj = new handleData({ controller : 'ceis_country_profile', method : 'process' });
</script>