<ul class="collapsible panel" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active">
    </div>
    <div class="collapsible-body row" style="display:block !important">
      <div class="row">
        <div class="col s4">
          <label>Company Name</label>
          <input disabled type="text" class="form-control" value="<?php echo ISSET($contractor_name) ? $contractor_name : "" ; ?>" class="form-control" >
          <label>Project Title</label>
          <input disabled type="text" class="form-control" value="<?php echo ISSET($project_title) ? $project_title : "" ; ?>" class="form-control" >
          <label>Project Location</label>
          <input disabled type="text" class="form-control" value="<?php echo ISSET($project_location) ? $project_location : "" ; ?>" class="form-control" >
        </div>
        <div class="col s4">
          <label>Contract Amount</label>
          <input disabled type="text" class="form-control" value="<?php echo ISSET($contract_cost) ? number_format($contract_cost,2) : "" ; ?>" class="form-control" >
          <label>Type of Contract</label>
          <input disabled type="text" class="form-control" value="<?php echo ISSET($contract_type) ? $contract_type : "" ; ?>" class="form-control" >
          <label>Contract Duration</label>
          <input disabled type="text" class="form-control" value="<?php echo ISSET($contract_duration) ? $contract_duration : "" ; ?>" class="form-control" >
        </div>
        <div class="col s4">
        <label>Date Started</label>
          <input disabled type="text" class="form-control" value="<?php echo ISSET($started_date) ? $started_date : "" ; ?>" class="form-control" >
          <label>Original Completion Date</label>
          <input disabled type="text" class="form-control" value="<?php echo ISSET($completed_date) ? $completed_date : "" ; ?>" class="form-control" >
          <label>Revision Completion Date</label>
          <input disabled type="text" class="form-control" value="<?php echo ISSET($completed_date) ? $completed_date : "" ; ?>" class="form-control" >
        </div>
      </div>
    </div>
  </li>
</ul>

<ul class="collapsible panel m-t-lg" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active"> 
    </div>
    <div class="collapsible-body row" style="display: block">
      <table id="table_contracts_view">
        <thead>
          <th width="15%">Year</th>
          <th width="15%">Quarter</th>
          <th width="30%">Prepared By</th>
          <th width="20%">Prepared Date</th>
          <th width="10%">Status</th>
          <th width="10%">Actions</th>
        </thead>
        <tbody>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tbody>
      </table>
    </div>
  </li>
</ul>