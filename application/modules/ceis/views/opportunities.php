<div class="page-title m-b-lg">
  	<ul id="breadcrumbs">
		<li><a href="#">Home</a></li>
		<li><a href="#" class="active">Opportunities</a></li>
  	</ul>
  	<div class="row m-b-n">
		<div class="col s6 p-r-n">
		  	<h5>Opportunities
				<span>Manage Opportunities</span>
		  	</h5>
		</div>
		<div class="col s6 p-r-n right-align">
		  	<div class="btn-group">
		    	
		  	</div>
		  
		  	<div class="input-field inline p-l-md p-r-md">
				<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_opportunity" id="add_opportunity" name="add_opportunity" onclick="modal_opportunity_init()">Opportunity</button>
		  	</div>
		</div>
  	</div>
</div>

<div class="pre-datatable"></div>
<div>
  	<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="opportunity_table">
  	<thead>
		<tr>
		  	<th width="15%">Received Date</th>
		    <th width="15%">Reference No.</th>
		    <th width="15%">Source</th>
		    <th width="20%">Agency</th>
		    <th width="15%">Subject</th>
		    <th width="10%">Status</th>
		    <th width="10%">Actions</th>
		</tr>
  	</thead>
  	</table>
</div>

<script type="text/javascript">
var	deleteObj = new handleData({ controller : 'ceis_opportunities', method : 'delete_opportunity', module: '<?php echo PROJECT_CEIS ?>' });
</script>