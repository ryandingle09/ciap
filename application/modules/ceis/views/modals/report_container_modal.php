<form id="project_type_form">
	<div class="panel p-md" style="height: 600px;">
		<div class="col s12" style="height: 100%;">
			<?php if (!empty($pdf)): ?>
				<object width="100%" height="100%" type="application/pdf" data="data:application/pdf;base64,<?php echo base64_encode($pdf) ?>">
					Cant display PDF
				</object>
			<?php else:  ?>
				<div class="title"><h4>Cant display PDF</h4></div>
			<?php endif; ?>
		</div>
	</div>
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	</div>
</form>