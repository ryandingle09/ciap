<?php 
$present_company   = ( ISSET($work_exp['present_flag']) && ! EMPTY($work_exp['present_flag']) ) ? 'Yes' : 'No';
$start_date    	   = ( ISSET($work_exp['start_date']) && ! EMPTY($work_exp['start_date']) && $work_exp['start_date'] != DEFAULT_DATE ) ? date('F d,Y', strtotime($work_exp['start_date'])) : '';
$end_date    	   = ( ISSET($work_exp['end_date']) && ! EMPTY($work_exp['end_date']) && $work_exp['end_date'] != DEFAULT_DATE ) ? date('F d,Y', strtotime($work_exp['end_date'])) : '';
$company_name	   = ( ISSET($work_exp['company_name']) && ! EMPTY($work_exp['company_name']) ) ? $work_exp['company_name'] : '';
$company_addr	   = ( ISSET($work_exp['company_address']) && ! EMPTY($work_exp['company_address']) ) ? $work_exp['company_address'] : '';
$job_description   = ( ISSET($work_exp['job_description']) && ! EMPTY($work_exp['job_description']) ) ? $work_exp['job_description'] : '';
$position	       = ( ISSET($work_exp['position']) && ! EMPTY($work_exp['position']) ) ? $work_exp['position'] : '';
$stockholder       = ( ISSET($work_exp['stockholder']) && ! EMPTY($work_exp['stockholder']) ) ? 'Yes' : 'No';
$amount_paid       = ( ISSET($work_exp['paid_in_amount']) && ! EMPTY($work_exp['paid_in_amount']) ) ? decimal_format($work_exp['paid_in_amount']) : '';
?>
<form>
<div class="form-float-label font-normal">
  	
  	<div class="row row-special m-t-lg">
  	  <div class="input-field col s6">
	   <i class="material-icons prefix">library_books</i> 
	    <input type="text" value="<?php echo $present_company ?>" disabled name="statement_date" id="statement_date" class="black-text"/>
	    <label  class="grey-text-bold active">Present Company</label>
      </div>
    </div> 
    
    <div class="row row-special">
	 	<div class="input-field col s6">
		    <i class="material-icons prefix">date_range</i> 
		    <input type="text" value="<?php echo $start_date; ?>" disabled name="statement_date" id="statement_date" class="black-text datepicker dnb"/>
		    <label  class="grey-text-bold active">Start Date</label>
      	</div>
      	
<?php if( ! $work_exp['present_flag']): ?>
	  	<div class="input-field col s6">
		    <i class="material-icons prefix">date_range</i> 
		    <input type="text" value="<?php echo $end_date; ?>" disabled name="statement_date" id="statement_date" class="black-text datepicker dnb"/>
		    <label  class="grey-text-bold active">End Date</label>
      	</div>
<?php endif; ?>

    </div> 
    
    <div class="row row-special">
	  <div class="input-field col s12">
	   <i class="material-icons prefix">account_circle</i> 
	    <input type="text" disabled class="black-text" required="" aria-required="true" name="firm_name" id="firm_name" value="<?php echo $company_name; ?>"/>
	    <label for="project_title" class="grey-text-bold active">Company Name</label>
      </div>
  	</div>

	<div class="row row-special">
		<div class="input-field col s12">
		  	<i class="material-icons prefix">location_on</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="address" id="address" value="<?php echo $company_addr; ?>"/>
		    <label for="project_title" class="grey-text-bold active">Company Address</label>
        </div>
    </div> 
    
    <div class="row row-special">
		<div class="input-field col s12">
		  	<i class="material-icons prefix">person</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="firm_name" id="firm_name" value="<?php echo $position; ?>"/>
		    <label for="project_title" class="grey-text-bold active">Position</label>
        </div>
    </div> 
    
    <div class="row row-special">
		<div class="input-field col s12">
		    <i class="material-icons prefix">description</i> 
		    <textarea class="materialize-textarea black-text" disabled><?php echo $job_description; ?></textarea>
		    <label for="scheduled" class="grey-text-bold active">Job Description</label>
	   </div>
  	</div>
		
	<div class="row row-special">
	  	  <div class="input-field col s6">
			    <i class="material-icons prefix">library_books</i> 
			    <input type="text" value="<?php echo $stockholder; ?>" disabled name="statement_date" id="statement_date" class="black-text"/>
			    <label  class="grey-text-bold active">Stockholder</label>
	      </div>
	      
<?php if($work_exp['stockholder']): ?>
		  <div class="input-field col s6">
			    <i class="material-icons prefix">monetization_on</i> 
			    <input type="text" value="<?php echo $amount_paid; ?>" disabled name="statement_date" id="statement_date" class="black-text datepicker dnb"/>
			    <label  class="grey-text-bold active">Amount Paid - in</label>
	      </div>
<?php endif; ?>

    </div> 
  	
  	<div class="md-footer default">
	  <a class="waves-effect waves-teal btn-flat cancel_modal grey lighten-3">Cancel</a>
	</div>
  	

</div><!-- end of row -->

</form>
        


  
	