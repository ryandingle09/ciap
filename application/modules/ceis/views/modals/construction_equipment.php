<?php
$type      = ( ISSET($equip_details['equipment_type_name']) && ! EMPTY($equip_details['equipment_type_name']) ) ? $equip_details['equipment_type_name'] : '';
$serial    = ( ISSET($equip_details['serial_no']) && ! EMPTY($equip_details['serial_no']) ) ? $equip_details['serial_no'] : '';
$capacity  = ( ISSET($equip_details['capacity']) && ! EMPTY($equip_details['capacity']) ) ? $equip_details['capacity'] : '';
$brand	   = ( ISSET($equip_details['brand']) && ! EMPTY($equip_details['brand']) ) ? $equip_details['brand'] : '';
$model	   = ( ISSET($equip_details['model']) && ! EMPTY($equip_details['model']) ) ? $equip_details['model'] : '';
$cost	   = ( ISSET($equip_details['acquisition_cost']) && ! EMPTY($equip_details['acquisition_cost']) ) ? decimal_format($equip_details['acquisition_cost']) : '';
$condition = ( ISSET($equip_details['present_condition']) && ! EMPTY($equip_details['present_condition']) ) ? $equip_details['present_condition'] : '';
$location  = ( ISSET($equip_details['location']) && ! EMPTY($equip_details['location']) ) ? $equip_details['location'] : '';
$acquired  = ( ISSET($equip_details['acquired_date']) && ! EMPTY($equip_details['acquired_date']) && $equip_details['acquired_date'] != DEFAULT_DATE ) ? date('Y-m-d', strtotime($equip_details['acquired_date'])) : '';

$active    = ( ISSET($equip_details['active_flag']) && ! EMPTY($equip_details['active_flag']) ) ? 'Active' : 'Inactive';
$registration = ( ISSET($equip_details['for_registration_flag']) && ! EMPTY($equip_details['for_registration_flag']) ) ? 'Yes' : 'No';
?>
<form id="construction_equipment_form">
	
	<div class="form-float-label">

		<div class="row row-special m-t-lg">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">library_books</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="base" id="base" value="<?php echo $type; ?>"/>
		    <label for="base" class="grey-text-bold active">Type of Equipment</label>
	      </div>
	      
	       <div class="input-field col s6">
		   <i class="material-icons prefix">payment</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="base" id="base" value="<?php echo $serial; ?>"/>
		    <label for="base" class="grey-text-bold active">Serial No</label>
	      </div>
	  	</div>
	  	
	  	<div class="row row-special">
		  <div class="input-field col s12">
		   <i class="material-icons prefix">crop_free</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="base" id="base" value="<?php echo $capacity; ?>"/>
		    <label for="base" class="grey-text-bold active">Size/Capacity</label>
	      </div>	     
	  	</div>
	  	
	  	<div class="row row-special">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">local_offer</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="base" id="base" value="<?php echo $brand; ?>"/>
		    <label for="base" class="grey-text-bold active">Brand</label>
	      </div>
	      
	       <div class="input-field col s6">
		   <i class="material-icons prefix">local_offer</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="base" id="base" value="<?php echo $model; ?>"/>
		    <label for="base" class="grey-text-bold active">Model</label>
	      </div>
	  	</div>

		<div class="row row-special">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">date_range</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="base" id="base" value="<?php echo $acquired; ?>"/>
		    <label for="base" class="grey-text-bold active">Date Acquired</label>
	      </div>
	      
	       <div class="input-field col s6">
		   <i class="material-icons prefix">monetization_on</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="base" id="base" value="<?php echo $cost; ?>"/>
		    <label for="base" class="grey-text-bold active">Acquisition Cost</label>
	      </div>
	  	</div>

		<div class="row row-special">
		  <div class="input-field col s12">
		    <i class="material-icons prefix">description </i> 
		    <textarea class="materialize-textarea black-text" disabled><?php echo $condition; ?></textarea>
		    <label for="base" class="grey-text-bold active">Present Condition</label>
	      </div>
	  	</div>

	  	<div class="row row-special">
		  <div class="input-field col s12">
		   <i class="material-icons prefix">location_on</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="base" id="base" value="<?php echo $location; ?>"/>
		    <label for="base" class="grey-text-bold active">Equipment Present Location</label>
	      </div>	     
	  	</div>
	  	
		<div class="row row-special">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">hourglass_empty</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="base" id="base" value="<?php echo $active; ?>"/>
		    <label for="base" class="grey-text-bold active">Status</label>
	      </div>
	      
	       <div class="input-field col s6">
		   <i class="material-icons prefix">content_paste</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="base" id="base" value="<?php echo $registration; ?>"/>
		    <label for="base" class="grey-text-bold active">Subject for registration</label>
	      </div>
	  	</div>
	  
	<div class="md-footer default">
	  <a class="waves-effect waves-teal btn-flat cancel_modal grey lighten-3" id="cancel_construction_equipment">Cancel</a>
	</div>
</form>