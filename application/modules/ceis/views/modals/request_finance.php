<?php 
$statement_date    	   = ( ISSET($finance['statement_date']) && ! EMPTY($finance['statement_date']) && $finance['statement_date'] != DEFAULT_DATE ) ? date('Y-m-d', strtotime($finance['statement_date'])) : '';
$total_curr_assets 	   = ( ISSET($finance['total_current_assets']) && ! EMPTY($finance['total_current_assets']) ) ? decimal_format($finance['total_current_assets']) : '';
$total_non_curr_assets = ( ISSET($finance['total_non_current_assets']) && ! EMPTY($finance['total_non_current_assets']) ) ? decimal_format($finance['total_non_current_assets']) : '';
$total_non_liabilities = ( ISSET($finance['total_current_liabilities']) && ! EMPTY($finance['total_current_liabilities']) ) ? decimal_format($finance['total_current_liabilities']) : '';
$longterm_liabilities  = ( ISSET($finance['long_term_liabilities']) && ! EMPTY($finance['long_term_liabilities']) ) ? decimal_format($finance['long_term_liabilities']) : '';
$networth 			   = ( ISSET($finance['networth']) && ! EMPTY($finance['networth']) ) ? decimal_format($finance['networth']) : '';
$gross_sales 		   = ( ISSET($finance['gross_sales']) && ! EMPTY($finance['gross_sales']) ) ? decimal_format($finance['gross_sales']) : '';
$net_profit 		   = ( ISSET($finance['net_profit']) && ! EMPTY($finance['net_profit']) ) ? decimal_format($finance['net_profit']) : '';
$common_shares 		   = ( ISSET($finance['common_shares']) && ! EMPTY($finance['common_shares']) ) ? decimal_format($finance['common_shares']) : '';
$par_value 		       = ( ISSET($finance['par_value']) && ! EMPTY($finance['par_value']) ) ? $finance['par_value'] : '';
?>
<form>
<div class="form-float-label font-normal">
  	
  	<div class="row row-special m-t-lg">
  	  <div class="input-field col s6">
	   <i class="material-icons prefix">library_books</i> 
	    <input type="text" value="<?php echo ($finance['stmt_type'] == TYPE_AUDITED) ? 'Audited' : 'Interim'; ?>" disabled name="statement_date" id="statement_date" class="black-text"/>
	    <label  class="grey-text-bold active">Type</label>
      </div>
	  <div class="input-field col s6">
	   <i class="material-icons prefix">date_range</i> 
	    <input type="text" value="<?php echo $statement_date; ?>" disabled name="statement_date" id="statement_date" class="black-text datepicker dnb"/>
	    <label  class="grey-text-bold active">Statement Date</label>
      </div>
    </div> 
    
    <div class="row row-special">
	  <div class="input-field col s6">
	   <i class="material-icons prefix">monetization_on</i> 
	    <input type="text" disabled class="black-text" required="" aria-required="true" name="firm_name" id="firm_name" value="<?php echo $total_curr_assets; ?>"/>
	    <label for="project_title" class="grey-text-bold active">Total Current Assets</label>
      </div>
    
	  <div class="input-field col s6">
	  	<i class="material-icons prefix">monetization_on</i> 
	    <input type="text" disabled class="black-text" required="" aria-required="true" name="firm_name" id="firm_name" value="<?php echo $total_non_curr_assets; ?>"/>
	    <label for="project_title" class="grey-text-bold active">Total Non Current Assets</label>
      </div>
  	</div>

	
	<div class="row row-special">
	  <div class="input-field col s6">
	   <i class="material-icons prefix">monetization_on</i> 
	    <input type="text" disabled class="black-text" required="" aria-required="true" name="firm_name" id="firm_name" value="<?php echo $total_non_liabilities; ?>"/>
	    <label for="project_title" class="grey-text-bold active">Total Current Liabilities</label>
      </div>
    
	  <div class="input-field col s6">
	  	<i class="material-icons prefix">monetization_on</i> 
	    <input type="text" disabled class="black-text" required="" aria-required="true" name="firm_name" id="firm_name" value="<?php echo $longterm_liabilities; ?>"/>
	    <label for="project_title" class="grey-text-bold active">Long Term Liabilities</label>
      </div>
  	</div>

	<div class="row row-special">
	  <div class="input-field col s6">
	   <i class="material-icons prefix">monetization_on</i> 
	    <input type="text" disabled class="black-text" required="" aria-required="true" name="firm_name" id="firm_name" value="<?php echo $networth; ?>"/>
	    <label for="project_title" class="grey-text-bold active">Net Worth</label>
      </div>
    
	  <div class="input-field col s6">
	  	<i class="material-icons prefix">monetization_on</i> 
	    <input type="text" disabled class="black-text" required="" aria-required="true" name="firm_name" id="firm_name" value="<?php echo $gross_sales; ?>"/>
	    <label for="project_title" class="grey-text-bold active">Gross Sales/Revenues</label>
      </div>
  	</div>
	
	<div class="row row-special">
	  <div class="input-field col s6">
	   <i class="material-icons prefix">monetization_on</i> 
	    <input type="text" disabled class="black-text" required="" aria-required="true" name="firm_name" id="firm_name" value="<?php echo $net_profit; ?>"/>
	    <label for="project_title" class="grey-text-bold active">Net Profit after tax</label>
      </div>
    
	  <div class="input-field col s6">
	  	<i class="material-icons prefix">monetization_on</i> 
	    <input type="text" disabled class="black-text" required="" aria-required="true" name="firm_name" id="firm_name" value="<?php echo $common_shares; ?>"/>
	    <label for="project_title" class="grey-text-bold active">No. of common shares outstanding</label>
      </div>
  	</div>
  	
  	<div class="row row-special">
	  <div class="input-field col s6">
	   <i class="material-icons prefix">monetization_on</i> 
	    <input type="text" disabled class="black-text" required="" aria-required="true" name="firm_name" id="firm_name" value="<?php echo $par_value; ?>"/>
	    <label for="project_title" class="grey-text-bold active">PAR Value per common share</label>
      </div>
  	</div>
  	
  	<div class="md-footer default">
	  <a class="waves-effect waves-teal btn-flat cancel_modal grey lighten-3">Cancel</a>
	</div>
  	

</div><!-- end of row -->

</form>
        


  
	