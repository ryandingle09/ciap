<form id="geographical_distribution_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
	<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
	<div class="form-float-label">
		<div class="row m-n">
		    <div class="col s12">
			  	<div class="input-field">
				    <input type="text" class="validate" required="" aria-required="true" name="region_name" id="region_name" value="<?php echo ISSET($region_name) ? $region_name : "" ?>"/>
				    <label for="region_name">Region</label>
		      	</div>
		    </div>
		</div>
		<div class="row m-n">
		    <div class="col s12">
			  	<div class="input-field">
				    <label class="active" for="country_name">Country</label>
				    <select required="" aria-required="true" name="country_name[]" id="country_name" class="selectize" placeholder="Select Country" multiple="multiple">
					  	<?php foreach($countries as $country): ?>
                        <option
                        
                          <?php 
                            $rep = $country["country_code"];

                            for($i=0; $i<$selected_data_value; $i++){
                              if($geo_list_arr[$i] == $rep){
                                echo "selected"; 
                              }
                            }
                          ?> 

                        value="<?php echo $country["country_code"] ?>"><?php echo $country["country_name"] ?></option>      
                        <?php endforeach; ?>
					</select>
			  	</div>
	    	</div>
	  	</div>
	  </div>
	</div>
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal">Cancel</a>
	 	<?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	   		<button class="btn waves-effect waves-light" id="save_geographical_distribution" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<?php //endif; ?>
	  
	</div>
</form>
<script>
$(function(){
  //help_text("role_form");
  
  $('#geographical_distribution_form').parsley();
  $('#geographical_distribution_form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {
	  var data = $(this).serialize();
	  
	  button_loader('save_geographical_distribution', 1);
	  $.post("<?php echo base_url() . PROJECT_CEIS ?>/ceis_geographical_distribution/process/", data, function(result) {
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  button_loader('save_geographical_distribution', 0);
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  button_loader("save_geographical_distribution",0);
		  modalObj.closeModal();
		  
		  load_datatable('geographical_distribution_table', '<?php echo PROJECT_CEIS ?>/ceis_geographical_distribution/get_geographical_distribution_list/');
		}
	  }, 'json');       
    }
  });
  <?php if(ISSET($region_name)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>
});
</script>