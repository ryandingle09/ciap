<div class="modal-dialog">
  <div class="modal-body">
    <div class="row">
      <form data-validate="parsley">
        <section class="panel panel-default">
          <header class="panel-heading">
            <span class="h4">ADD RESOLUTIONS</span>
          </header>

          <div class="panel-body">
            <div class="col-sm-6">
              <div class="form-group">
                <label>Firm Name:</label>
                <label></label>  
              </div>                      
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Collection Group:</label>
                <label></label>  
              </div>
            </div>
            <div class="col-sm-12">
              <div class="form-group">
                <label>OP Date</label>
                <input type="text" class="input-sm input-s datepicker-input form-control" data-date-format="dd-mm-yyyy"  placeholder="Enter Resolution Title" >
                <input class="input-sm input-s datepicker-input form-control" id="sor_date_from" name="sor_date_from" style="width:150px" type="text" value="<?php echo date("m/d/Y", strtotime("-1 month", strtotime(date('m/d/Y'))));?>" data-date-format="mm/dd/yyyy">
              </div>
            </div>

            <div class="table-responsive">
              <table class="table table-striped m-b-none" data-ride="datatables">
                <thead>
                  <tr>
                    <th width="50%">Nature of Collection</th>
                    <th width="30%">Amount</th>
                    <th width="20%">Action</th>
                  </tr>
                </thead>

                <tbody>
                  <tr>
                    <td width="50%">
                      <div class="btn-group">
                        <select name="account" class="form-control">
                              <option>Application of Registration</option>
                              <option>Application of Renewal of Register</option>
                              <option>Authority to Bid/Undertake Project</option>
                              <option>Authority to Bid/Undertake Service</option>
                              <option>Equipment Registration</option>
                        </select>
                      </div>
                    </td>

                    <td width="30%">
                      <input type="text" placeholder="Enter Amount" class="form-control" data-type="phone"  data-required="true">
                    </td>

                    <td width="20%">
                      <a href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_request" class="btn btn-sm btn-icon btn-dark"><i class="fa fa-trash-o"></i></a>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>

            <div align="right">
              <div class="btn-group">
                <a class="btn btn-sm btn-info" data-toggle="modal" href="#modal-opportunities"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Another Collection</a>                  
              </div>
            </div>
          </div>

          <footer class="panel-footer text-right bg-light lter">
            <button type="btn" class="btn btn-dark btn-s-xs">Save</button>
            <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
          </footer>
        </section>
      </form>
    </div>
  </div>          
</div>