<form id="contract_involvement_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
	<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
	
	<div class="form-float-label">
	  	<div class="row m-n">
	    	<div class="col s6">
		  		<div class="input-field">
		    		<input type="text" class="validate" required="" maxlength="1" aria-required="true" name="involvement_code" id="involvement_code" value="<?php echo ISSET($involvement_code) ? $involvement_code : "" ?>"/>
		    		<label for="involvement_code">Code of Project Type</label>
	      		</div>
	    	</div>
		    <div class="col s6">
			  	<div class="input-field">
			    	<input type="text" class="validate" required="" aria-required="true" name="involvement_name" id="involvement_name" value="<?php echo ISSET($involvement) ? $involvement : "" ?>"/>
			    	<label for="involvement_name">Name of Project Type</label>
		      	</div>
		    </div>
	  	</div>
	</div>
	
	<div class="md-footer default">
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	    <button class="btn waves-effect waves-light" id="save_contract_involvement" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	</div>
</form>

<script>
$(function(){
  	$('#contract_involvement_form').parsley();
  	$('#contract_involvement_form').submit(function(e) {
    e.preventDefault();
    
		if ( $(this).parsley().isValid() ) {
	  		var data = $(this).serialize();
	  	
	  	button_loader('save_contract_involvement', 1);
	  	$.post("<?php echo base_url() . PROJECT_CEIS ?>/ceis_contract_involvement/process/", data, function(result) {
		if(result.flag == 0){
		  	notification_msg("<?php echo ERROR ?>", result.msg);
		  	button_loader('save_contract_involvement', 0);
		} else {
		  	notification_msg("<?php echo SUCCESS ?>", result.msg);
		  	button_loader("save_contract_involvement",0);
		  	modalObj.closeModal();
		  
		  	load_datatable('contract_involvement_table', '<?php echo PROJECT_CEIS ?>/ceis_contract_involvement/get_contract_involvement_list/');
		}
	  }, 'json');       
    }
  });
  
  <?php if(ISSET($involvement_code)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>
	})

</script>