<?php 
$start_date    	   = ( ISSET($project['start_date']) && ! EMPTY($project['start_date']) && $project['start_date'] != DEFAULT_DATE ) ? date('F d,Y', strtotime($project['start_date'])) : '';
$end_date    	   = ( ISSET($project['end_date']) && ! EMPTY($project['end_date']) && $project['end_date'] != DEFAULT_DATE ) ? date('F d,Y', strtotime($project['end_date'])) : '';
$project_title	   = ( ISSET($project['project_title']) && ! EMPTY($project['project_title']) ) ? $project['project_title'] : '';
$project_location  = ( ISSET($project['project_location']) && ! EMPTY($project['project_location']) ) ? $project['project_location'] : '';
$project_type      = ( ISSET($project['project_type_name']) && ! EMPTY($project['project_type_name']) ) ? $project['project_type_name'] : '';
$position	       = ( ISSET($project['positions']) && ! EMPTY($project['positions']) ) ? $project['positions'] : '';
$owner		       = ( ISSET($project['owner']) && ! EMPTY($project['owner']) ) ? $project['owner'] : '';
$project_cost      = ( ISSET($project['project_cost']) && ! EMPTY($project['project_cost']) ) ? decimal_format($project['project_cost']) : '';
?>
<form>
<div class="form-float-label font-normal">
    
    <div class="row row-special m-t-lg">
	 	<div class="input-field col s6">
		    <i class="material-icons prefix">description</i> 
		    <input type="text" value="<?php echo $project_title; ?>" disabled name="statement_date" id="statement_date" class="black-text datepicker dnb"/>
		    <label  class="grey-text-bold active">Project Title</label>
      	</div>
      	
	  	<div class="input-field col s6">
		    <i class="material-icons prefix">location_on</i> 
		    <input type="text" value="<?php echo $project_location; ?>" disabled name="statement_date" id="statement_date" class="black-text datepicker dnb"/>
		    <label  class="grey-text-bold active">Project Location</label>
      	</div>
    </div> 
    
    <div class="row row-special">
	 	<div class="input-field col s6">
		    <i class="material-icons prefix">account_circle</i> 
		    <input type="text" value="<?php echo $owner; ?>" disabled name="statement_date" id="statement_date" class="black-text datepicker dnb"/>
		    <label  class="grey-text-bold active">Owner</label>
      	</div>
      	
	  	<div class="input-field col s6">
		    <i class="material-icons prefix">monetization_on</i> 
		    <input type="text" value="<?php echo $project_cost; ?>" disabled name="statement_date" id="statement_date" class="black-text datepicker dnb"/>
		    <label  class="grey-text-bold active">Project Cost</label>
      	</div>
    </div> 
    
    <div class="row row-special">
	 	<div class="input-field col s12">
		    <i class="material-icons prefix">person</i> 
		    <input type="text" value="<?php echo $position; ?>" disabled name="statement_date" id="statement_date" class="black-text datepicker dnb"/>
		    <label  class="grey-text-bold active">Positions</label>
      	</div>
    </div>
       	
    <div class="row row-special">   	
	  	<div class="input-field col s12">
		    <i class="material-icons prefix">library_books</i> 
		    <input type="text" value="<?php echo $project_type; ?>" disabled name="statement_date" id="statement_date" class="black-text datepicker dnb"/>
		    <label  class="grey-text-bold active">Project Type</label>
      	</div>
    </div> 
    
    <div class="row row-special">
  		<div class="card-panel m-l-sm m-r-sm grey darken-3 p-md"><span class="font-semibold white-text">Percentage of Accomplishment</span></div>
  	</div>
    
     <div class="row row-special">
	 	<div class="input-field col s6">
		    <i class="material-icons prefix">date_range</i> 
		    <input type="text" value="<?php echo $start_date; ?>" disabled name="statement_date" id="statement_date" class="black-text datepicker dnb"/>
		    <label  class="grey-text-bold active">Start Date</label>
      	</div>
      	
	  	<div class="input-field col s6">
		    <i class="material-icons prefix">date_range</i> 
		    <input type="text" value="<?php echo $end_date; ?>" disabled name="statement_date" id="statement_date" class="black-text datepicker dnb"/>
		    <label  class="grey-text-bold active">End Date</label>
      	</div>
    </div> 
    
  	
  	<div class="md-footer default">
	  <a class="waves-effect waves-teal btn-flat cancel_modal grey lighten-3">Cancel</a>
	</div>
  	

</div><!-- end of row -->

</form>
        


  
	