<?php 
$request  			= (ISSET($task_details['request_type_name']) && ! EMPTY($task_details['request_type_name'])) ? $task_details['request_type_name'] : '';
$task    			= (ISSET($task_details['task_name']) && ! EMPTY($task_details['task_name'])) ? $task_details['task_name'] : '';
$resource_name  	= (ISSET($task_details['resource_name']) && ! EMPTY($task_details['resource_name'])) ? ' - '.$task_details['resource_name'] : '';
$task_status_id 	= (ISSET($task_details['task_status_id']) && ! EMPTY($task_details['task_status_id'])) ? $task_details['task_status_id'] : '';

$role_name	     	= (ISSET($task_details['role_name']) && ! EMPTY($task_details['role_name'])) ? $task_details['role_name'] : '';

$get_board_decision = (ISSET($task_details['get_board_decision']) && ! EMPTY($task_details['get_board_decision'])) ? $task_details['get_board_decision'] : 0;
?>
<form id="task_form" class="form-select">
	<input type="hidden" name="security" value="<?php echo $security;?>" />
	
	<div class="form-float-label font-normal">
		<div class="row row-special m-t-lg">
			<div class="input-field col s6">
		   		<i class="material-icons prefix">library_books</i> 
		    	<input type="text" disabled class="black-text" required="" aria-required="true" name="request" id="request" value="<?php echo $request; ?>"/>
		   	 	<label for="project_title" class="grey-text-bold active">Request</label>
	        </div>
	        
	        <div class="input-field col s6">
		   		<i class="material-icons prefix">gavel</i> 
		    	<input type="text" disabled class="black-text" required="" aria-required="true" name="task" id="task" value="<?php echo $task; ?>"/>
		   	 	<label for="project_title" class="grey-text-bold active">Task</label>
	        </div>
		</div>
		
		<div class="row row-special">
			<div class="input-field col s12">
		   		<i class="material-icons prefix">account_circle</i> 
		    	<input type="text" disabled class="black-text"  name="resource" id="resource" value="<?php echo $role_name.$resource_name; ?>"/>
		   	 	<label for="project_title" class="grey-text-bold active">Resource</label>
	        </div>
		</div>
		
		<div class="row row-special">
			<div class="input-field col s6">
		   		<i class="material-icons prefix select-icon">hourglass_empty</i> 
		    	<select name="task_status" id="task_status" class="selectize"  placeholder="Select Status" data-parsley-required="true">
					   			<option value="">Please Select</option>
					    <?php foreach($status as $key => $val) :
					    
					    		if($val['task_status_id'] == REQUEST_TASK_SKIP && $task_details['w_skip'] == 0) continue;
					    
					    		if($val['task_status_id'] == REQUEST_TASK_NOT_YET_STARTED) continue;
					    
					    		if($task_status_id == REQUEST_TASK_NOT_YET_STARTED && $val['task_status_id'] == REQUEST_TASK_DONE) continue;
					    		
					    		if($task_status_id == REQUEST_TASK_ONGOING && ($val['task_status_id'] == REQUEST_TASK_ONGOING || $val['task_status_id'] == REQUEST_TASK_SKIP)) continue;
					    			
					    		$selected = ($val['task_status_id'] == $task_details['task_status_id']) ? 'selected' : '';	
					    ?>	
					    		<option value="<?php echo $val['task_status_id']; ?>" <?php echo $selected; ?>><?php echo $val['task_status']?></option>
					    <?php endforeach; ?>
				</select>
		   	 	<label for="project_title" class="grey-text-bold active">Status</label>
	        </div>
	        
<?php if( ! EMPTY($get_board_decision) && $task_status_id == REQUEST_TASK_ONGOING) : ?>
	        <div class="input-field col s6">
		   		<i class="material-icons prefix select-icon">gavel</i> 
		    	<select name="board_decision" id="board_decision" class="selectize"  placeholder="Select Status" data-parsley-required="true">
					   			<option value="">Please Select</option>
					    <?php foreach($board_decision as $key => $val) :
					    
					    		if($val['request_status_id'] == REQUEST_TASK_NOT_YET_STARTED) continue;
					    			
					    		$selected = ($val['request_status_id'] == $task_details['request_status_id']) ? 'selected' : '';	
					    ?>	
					    		<option value="<?php echo $val['request_status_id']; ?>" <?php echo $selected; ?>><?php echo $val['request_status']?></option>
					    <?php endforeach; ?>
				</select>
		   	 	<label for="project_title" class="grey-text-bold active">Board Decision</label>
	        </div>
<?php endif; ?>

		</div>
		
	    <div class="md-footer default">
		  <button class="btn waves-effect waves-light"  id="save_task" name="action" type="button" value="Save">Save</button>
		  <a class="waves-effect waves-teal btn-flat cancel_modal grey lighten-3">Cancel</a>
		</div>
	
	</div>
</form>