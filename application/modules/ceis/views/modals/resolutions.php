<form id="resolutions_form">
  <input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
  <input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
  <input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
    <div class="row m-n">
    <div class="form-float-label">
      <form data-validate="parsley">
        
          <div class="row m-n">
           <div class="col s6">
              <div class="input-field">
                <label>Resolution no.</label>
                <input type="text" class="validate" required="" aria-required="true" name="resolution_no" value="<?php echo ISSET($resolution_no) ? $resolution_no : "" ?>" >
              </div>                      
            </div>
            <div class="col s6">
              <div class="input-field">
                <label>Resolution Date</label>
                <input type="text" class="validate datepicker" data-date-format="yyyy-mm-dd" required="" aria-required="true" name="resolution_date" value="<?php echo ISSET($resolution_date) ? $resolution_date : "" ?>">
              </div>
            </div>
          </div>

           <div class="row m-n">
           <div class="col s12">
              <div class="input-field">
                <label>Resolution Title</label>
                <input type="text" class="validate" required="" aria-required="true" name="title" value="<?php echo ISSET($title) ? $title : "" ?>" >
              </div>                      
            </div>
          </div>

          <div class="row m-n">
            <div class="col s6">
              <div class="input-field">
                <label>First Round</label>
                <input type="text" class="validate datepicker" required="" aria-required="true" name="first_round"  data-date-format="yyyy-mm-dd" value="<?php echo ISSET($first_round) ? $first_round : "" ; ?>">
              </div>                      
            </div>
            <div class="col s6">
              <div class="input-field">
                <label>Last Round</label>
                <input type="text" class="validate datepicker" required="" aria-required="true" name="last_round" data-date-format="yyyy-mm-dd" value="<?php echo ISSET($last_round) ? $last_round : "" ; ?>">
              </div>                      
            </div>
          </div>

          <div class="row m-n">
            <div class="col s12">
              <div class="input-field">
                <label>Offers</label>
                 <textarea name="offers" aria-required="true" required="" class="materialize-textarea"><?php echo ISSET($offers) ? $offers : "" ; ?></textarea>
              </div>                      
            </div>
          </div>
         
         <div class="row m-n">
            <div class="col s12">
              <div class="input-field">
                <label>Commitments</label>
                 <textarea name="commitments" aria-required="true" required="" class="materialize-textarea"><?php echo ISSET($commitments) ? $offers : "" ; ?></textarea>
              </div>                      
            </div>
          </div>

          <div class="md-footer default">
            <a class="waves-effect waves-teal btn-flat cancel_modal" ">Cancel</a>
            <button class="btn waves-effect waves-light" id="save_resolution" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
          </div>
      </form>
      </div>
    </div>  
</form> 
<script>
$(function(){
  
  $('#resolutions_form').parsley();
  $('#resolutions_form').submit(function(e) {
    e.preventDefault();
    
  if ( $(this).parsley().isValid() ) {
    var data = $(this).serialize();
    
    button_loader('save_resolution', 1);
    $.post("<?php echo base_url() . PROJECT_CEIS ?>/ceis_resolutions/process/", data, function(result) {
    if(result.flag == 0){
      notification_msg("<?php echo ERROR ?>", result.msg);
      button_loader('save_resolution', 0);
    } else {
      notification_msg("<?php echo SUCCESS ?>", result.msg);
      button_loader("save_resolution",0);
      modalObj.closeModal();
      
      load_datatable('table_resolution', '<?php echo PROJECT_CEIS ?>/ceis_resolutions/get_resolution_list/');
    }
    }, 'json');       
    }
  });
  
  <?php if(ISSET($resolution_no)){ ?>
  $('.input-field label').addClass('active');
  <?php } ?>
});
</script>