<?php 
$project_title   = ( ISSET($manpower_details['project_title']) && ! EMPTY($manpower_details['project_title'])) ? $manpower_details['project_title'] : '';
$project_loc     = ( ISSET($manpower_details['project_location']) && ! EMPTY($manpower_details['project_location'])) ? $manpower_details['project_location'] : '';

$province        = ( ISSET($manpower_details['province']) && ! EMPTY($manpower_details['province'])) ? $manpower_details['province'] : '';
$country         = ( ISSET($manpower_details['country_name']) && ! EMPTY($manpower_details['country_name'])) ? $manpower_details['country_name'] : '';
$status          = ( ISSET($manpower_details['contract_status_name']) && ! EMPTY($manpower_details['contract_status_name'])) ? $manpower_details['contract_status_name'] : '';
$foreign	     = ( ISSET($manpower_details['foreign_principal_name']) && ! EMPTY($manpower_details['foreign_principal_name'])) ? $manpower_details['foreign_principal_name'] : '';

$duration	     = ( ISSET($manpower_details['contract_duration']) && ! EMPTY($manpower_details['contract_duration'])) ? $manpower_details['contract_duration'] : '';
$project_cost    = ( ISSET($manpower_details['project_cost']) && ! EMPTY($manpower_details['project_cost'])) ? decimal_format($manpower_details['project_cost']) : '';
$contract_cost   = ( ISSET($manpower_details['contract_cost']) && ! EMPTY($manpower_details['contract_cost'])) ? decimal_format($manpower_details['contract_cost']) : '';

$start_date      = ( ISSET($manpower_details['started_date']) && ! EMPTY($manpower_details['started_date']) && $manpower_details['started_date'] != DEFAULT_DATE) ? date('M d,Y', strtotime($manpower_details['started_date'])) : '';
$completed_date  = ( ISSET($manpower_details['completed_date']) && ! EMPTY($manpower_details['completed_date']) && $manpower_details['completed_date'] != DEFAULT_DATE) ? date('M d,Y', strtotime($manpower_details['completed_date'])) : '';

$required  	 	 = ( ISSET($manpower_details['required_manpower']) && ! EMPTY($manpower_details['required_manpower'])) ? $manpower_details['required_manpower'] : '';
$mobilized	     = ( ISSET($manpower_details['mobilized_manpower']) && ! EMPTY($manpower_details['mobilized_manpower'])) ? $manpower_details['mobilized_manpower'] : '';
$onsite	 		 = ( ISSET($manpower_details['onsite_manpower']) && ! EMPTY($manpower_details['onsite_manpower'])) ? $manpower_details['onsite_manpower'] : '';

$fees  	 		 = ( ISSET($manpower_details['fees']) && ! EMPTY($manpower_details['fees'])) ? decimal_format($manpower_details['fees']) : '';
$workers_salary	 = ( ISSET($manpower_details['workers_salaries']) && ! EMPTY($manpower_details['workers_salaries'])) ? decimal_format($manpower_details['workers_salaries']) : '';
$nationality	 = ( ISSET($manpower_details['nationality_name']) && ! EMPTY($manpower_details['nationality_name'])) ? $manpower_details['nationality_name'] : '';
$remarks	 	 = ( ISSET($manpower_details['remarks']) && ! EMPTY($manpower_details['remarks'])) ? $manpower_details['remarks'] : '';
?>

<form id="manpower_form">
	<div class="form-float-label font-normal">
	
	<div class="row row-special  m-t-lg">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">description</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="project_title" id="project_title" value="<?php echo $project_title; ?>"/>
		    <label for="project_title" class="grey-text-bold active">Project Title</label>
	      </div>
	    
		  <div class="input-field col s6">
		  	<i class="material-icons prefix">location_on</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="project_location" id="project_location" value="<?php echo $project_loc; ?>"/>
		    <label for="project_location" class="grey-text-bold active">Project Location</label>
	      </div>
  	</div>


	<div class="row row-special">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">location_city</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="province" id="province" value="<?php echo $province; ?>"/>
		    <label for="province" class="grey-text-bold active">Province</label>
	      </div>
	    
		  <div class="input-field col s6">
		  	<i class="material-icons prefix">location_city</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="country" id="country" value="<?php echo $country; ?>"/>
		    <label for="country" class="grey-text-bold active">Country</label>
	      </div>
  	</div>
	
	<div class="row row-special">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">hourglass_empty</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="status" id="status" value="<?php echo $status; ?>"/>
		    <label for="status" class="grey-text-bold active">Status</label>
	      </div>
  	</div>

	<div class="row row-special">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">monetization_on</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="project_cost" id="project_cost" value="<?php echo $project_cost; ?>"/>
		    <label for="project_cost" class="grey-text-bold active">Project Cost (USD)</label>
	      </div>
	    
		  <div class="input-field col s6">
		  	<i class="material-icons prefix">monetization_on</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="contract_cost" id="contract_cost" value="<?php echo $contract_cost; ?>"/>
		    <label for="contract_cost" class="grey-text-bold active">Contract Cost (USD)</label>
	      </div>
  	</div>


<?php if($manpower_details['contract_status_id'] != CONTRACT_STATUS_ONHAND): ?>

	<div class="row row-special">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">account_circle</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="project_cost" id="project_cost" value="<?php echo $foreign; ?>"/>
		    <label for="project_cost" class="grey-text-bold active">Foreign Principal Name</label>

	      </div>

	    
		  <div class="input-field col s6">
		  	<i class="material-icons prefix">flag</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="contract_cost" id="contract_cost" value="<?php echo $nationality; ?>"/>
		    <label for="contract_cost" class="grey-text-bold active">Nationality</label>

	      </div>
  	</div>


	<div class="row row-special">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">access_alarm</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="contract_duration" id="contract_duration" value="<?php echo $duration; ?>"/>
		    <label for="contract_duration" class="grey-text-bold active">Contract Duration</label>

	      </div>

  	</div>

	<div class="row row-special">
		  <div class="input-field col s6">
		   	 <i class="material-icons prefix">date_range</i> 
		   	 <input type="text" disabled class="black-text" required="" aria-required="true" name="start_date" id="start_date" value="<?php echo $start_date; ?>"/>
		   	 <label for="start_date" class="grey-text-bold active">Start Date</label>

	      </div>


		  <div class="input-field col s6">
		  	 <i class="material-icons prefix">date_range</i> 
		     <input type="text" disabled class="black-text" required="" aria-required="true" name="end_date" id="end_date" value="<?php echo $completed_date; ?>"/>
		     <label for="end_date" class="grey-text-bold active">Completed Date</label>

	      </div>
  	</div>
 
	<div class="row row-special">
  		<div class="card-panel m-l-sm m-r-sm grey darken-3 p-md"><span class="font-semibold white-text">Manpower</span></div>
  	</div>


	<div class="row row-special">
		  <div class="input-field col s4">
		   	 <i class="material-icons prefix">gavel</i> 
		   	 <input type="text" disabled class="black-text" required="" aria-required="true" name="start_date" id="start_date" value="<?php echo $required; ?>"/>
		   	 <label for="start_date" class="grey-text-bold active">Required</label>

	      </div>

		  <div class="input-field col s4">
		  	 <i class="material-icons prefix">gavel</i> 
		     <input type="text" disabled class="black-text" required="" aria-required="true" name="end_date" id="end_date" value="<?php echo $mobilized; ?>"/>
		     <label for="end_date" class="grey-text-bold active">Mobilized</label>

	      </div>
	      
	      <div class="input-field col s4">
		  	 <i class="material-icons prefix">gavel</i> 
		     <input type="text" disabled class="black-text" required="" aria-required="true" name="end_date" id="end_date" value="<?php echo $onsite; ?>"/>
		     <label for="end_date" class="grey-text-bold active">Onsite</label>
	      </div>
  	</div>
  	
  	<div class="row row-special">
  		<div class="card-panel m-l-sm m-r-sm grey darken-3 p-md"><span class="font-semibold white-text">Total Payment Received</span></div>
  	</div>

	<div class="row row-special">
		<div class="input-field col s6">
		   	 <i class="material-icons prefix">monetization_on</i> 
		   	 <input type="text" disabled class="black-text" required="" aria-required="true" name="start_date" id="start_date" value="<?php echo $fees; ?>"/>
		   	 <label for="start_date" class="grey-text-bold active">Service Fee & Others</label>
	    </div>
	
		<div class="input-field col s6">
		  	 <i class="material-icons prefix">monetization_on</i> 
		     <input type="text" disabled class="black-text" required="" aria-required="true" name="end_date" id="end_date" value="<?php echo $worker_salary; ?>"/>
		     <label for="end_date" class="grey-text-bold active">Worker's Salaries</label>
	    </div>
	</div>


<?php endif; ?>

	 <div class="row row-special">
		  <div class="input-field col s12">
		   <i class="material-icons prefix">description</i> 
		    <textarea class="materialize-textarea black-text" disabled><?php echo $remarks; ?></textarea>
		    <label for="scheduled" class="grey-text-bold active">Remarks</label>
	      </div>
  	</div>



	<div class="md-footer default">
	  <a class="waves-effect waves-teal btn-flat cancel_modal grey lighten-3">Cancel</a>
	</div>
</form>