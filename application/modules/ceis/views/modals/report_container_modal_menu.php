<form id="project_type_form">
	<div class="panel p-md">
		<div class="col s12">
			<section>
				<div class="panel-body" style="height: 800px;">
					<div class="col-sm-12" style="height: 100%;">
						<table class="striped" width="100%" cellpadding="3">
							<thead>
								<tr>
									<th>REPORT TITLES</th>
									<th class="center-align">ACTIONS</th>
								</tr>
							</thead>

							<tbody>
								<tr>
									<td>MEMORANDUM</td>
									<td class="right-align">
										<a target="_blank" data-toggle="ajaxModalStack" href="ceis_reports/memorandum/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?>" class="waves-effect waves-light btn red lighter-5"><i class="flaticon-acrobat2" ></i>PDF</a>

										<a href="ceis_reports/memorandum_excel/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?> " class="waves-effect waves-light btn green lighter-5"><i class="flaticon-x16" ></i>Excel</a>
									</td>
								</tr>
								<tr>
									<td>OVERSEAS PERFORMANCE OF POCB REGISTERED COMPANIES</td>
									<td class="right-align">
										<a target="_blank" data-toggle="ajaxModalStack" href="ceis_reports/reg_companies/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?>" class="waves-effect waves-light btn red lighter-5"><i class="flaticon-acrobat2"></i>PDF</a>

										<a href="ceis_reports/reg_companies_excel/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?> " class="waves-effect waves-light btn green lighter-5"><i class="flaticon-x16" ></i>Excel</a></td>
								</tr>
								<tr>
									<td>ANNEX A: FOREIGN EXCHANGE REMMITTANCES AND MANPOWER REPORT</td>
									<td class="right-align">
										<a target="_blank" data-toggle="ajaxModalStack" href="ceis_reports/foreign_exchange/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?>" class="waves-effect waves-light btn red lighter-5"><i class="flaticon-acrobat2"></i>PDF</a>

										<a href="ceis_reports/foreign_exchange_excel/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?> " class="waves-effect waves-light btn green lighter-5"><i class="flaticon-x16"></i>Excel</a></td>
								</tr>
								<tr>
									<td>ANNEX B: NEW CONTRACTS</td>
									<td class="right-align">
										<a target="_blank" data-toggle="ajaxModalStack" href="ceis_reports/new_contracts/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?>" class="waves-effect waves-light btn red lighter-5"><i class="flaticon-acrobat2"></i>PDF</a>

										<a href="ceis_reports/new_contracts_excel/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?> " class="waves-effect waves-light btn green lighter-5"><i class="flaticon-x16" ></i>Excel</a></td>
								</tr>
								<tr>
									<td>ANNEX C: BREAKDOWN OF OUTSTANDING PROJECT CONTRACTS</td>
									<td class="right-align">
										<a target="_blank" data-toggle="ajaxModalStack" href="ceis_reports/breakdown/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?>" class="waves-effect waves-light btn red lighter-5"><i class="flaticon-acrobat2"></i>PDF</a>

										<a href="ceis_reports/breakdown_excel/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?> " class="waves-effect waves-light btn green lighter-5"><i class="flaticon-x16" ></i>Excel</a></td>
								</tr>
								<tr>
									<td>ANNEX D: MANPOWER REPORT ON SERVICE CONTRACTS</td>
									<td class="right-align">
									<a target="_blank" data-toggle="ajaxModalStack" href="ceis_reports/manpower_report/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?>" class="waves-effect waves-light btn red lighter-5" class="waves-effect waves-light btn green darken-2"><i class="flaticon-acrobat2"></i>PDF</a>

									<a href="ceis_reports/manpower_report_excel/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?> " class="waves-effect waves-light btn green lighter-5"><i class="flaticon-x16" ></i>Excel</a></td>
								</tr>
								<tr>
									<td>ANNEX E-1: GEOGRAPHICAL DISTRIBUTION OF WORKERS</td>
									<td class="right-align">
										<a target="_blank" data-toggle="ajaxModalStack" href="ceis_reports/geo_worker_first/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?>" class="waves-effect waves-light btn red lighter-5" class="waves-effect waves-light btn green darken-2"><i class="flaticon-acrobat2"></i>PDF</a>

										<a href="ceis_reports/geo_worker_first_excel/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?>" class="waves-effect waves-light btn green lighter-5"><i class="flaticon-x16" ></i>Excel</a></td>
								</tr>
								<tr>
									<td>ANNEX E-2: GEOGRAPHICAL DISTRIBUTION OF WORKERS</td>
									<td class="right-align">
										<a target="_blank" data-toggle="ajaxModalStack" href="ceis_reports/geo_worker_second/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?>" class="waves-effect waves-light btn red lighter-5"><i class="flaticon-acrobat2"></i>PDF</a>

										<a href="ceis_reports/geo_worker_second_excel/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?>" class="waves-effect waves-light btn green lighter-5"><i class="flaticon-x16" ></i>Excel</a></td>
								</tr>
								<tr>
									<td>ANNEX F: REPORT ON OVERSEAS PROJECT CONTRACTS</td>
									<td class="right-align">
										<a target="_blank" data-toggle="ajaxModalStack" href="ceis_reports/report_project/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?>" class="waves-effect waves-light btn red lighter-5"><i class="flaticon-acrobat2"></i>PDF</a>

										<a href="ceis_reports/report_project_excel/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?>" class="waves-effect waves-light btn green lighter-5"><i class="flaticon-x16" ></i>Excel</a></td>
								</tr>
								<tr>
									<td>ANNEX G: REPORT ON OVERSEAS SERVICE CONTRACT</td>
									<td class="right-align">
										<a target="_blank" data-toggle="ajaxModalStack" href="ceis_reports/report_service/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?>" class="waves-effect waves-light btn red lighter-5"><i class="flaticon-acrobat2"></i>PDF</a>

										<a href="ceis_reports/report_service_excel/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?>" class="waves-effect waves-light btn green lighter-5"><i class="flaticon-x16" ></i>Excel</a></td>
								</tr>
								<tr>
									<td>ANNEX H: COMPLETED OVERSEAS SERVICE CONTRACT</td>
									<td class="right-align">
										<a target="_blank" data-toggle="ajaxModalStack" href="ceis_reports/completed_service/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?>" class="waves-effect waves-light btn red lighter-5"><i class="flaticon-acrobat2"></i>PDF</a>

										<a href="ceis_reports/completed_service_excel/<?php echo $year.'/'.$quarter.'/'.$contractor_list ?>" class="waves-effect waves-light btn green lighter-5"><i class="flaticon-x16" ></i>Excel</a></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</section>
		</div>
		
	</div>
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	</div>
</form>

