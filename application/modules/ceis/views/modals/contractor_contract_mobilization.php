<form id="finance_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
	<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
	<div class="form-float-label">

    <div class="row m-n">
	    <div class="col s12">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="name" id="name" value="<?php echo ISSET($contr_staff_id) ? $contr_staff_id : "" ?>"/>
		    <label class="active" for="name">Name</label>
	      </div>
	    </div>
    </div>

	<div class="row m-n">
		<div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="position" id="position" value="<?php echo ISSET($position_id) ? $position_id : "" ?>"/>
		    <label class="active" for="position">Position</label>
	      </div>
	    </div>

	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="salary_rate" id="salary_rate" value="<?php echo ISSET($salary_in_usd) ? $salary_in_usd : "" ?>"/>
		    <label class="active" for="salary_rate">Salary Rate (USD)</label>
	      </div>
	    </div>
    </div>
    <div class="row p-l-sm p-t-sm p-b-sm">
    <label><b><i>CONTRACT DURATION</i></b></label>
    </div>
	<div class="row m-n">

		<div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate datepicker" required="" aria-required="true" name="start_date" id="start_date" value="<?php echo ISSET($start_duration) ? $start_duration : "" ?>"/>
		    <label class="active" for="start_date">Start Date</label>
	      </div>
	    </div>

	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate datepicker" required="" aria-required="true" name="end_date" id="end_date" value="<?php echo ISSET($end_duration) ? $end_duration : "" ?>"/>
		    <label class="active" for="end_date">End Date</label>
	      </div>
	    </div>

    </div>

	<div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate datepicker" required="" aria-required="true" name="departure" id="departure" value="<?php echo ISSET($mobilized_date) ? $mobilized_date : "" ?>"/>
		    <label class="active" for="departure">Departure (Mobilized)</label>
	      </div>
	    </div>
	
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate datepicker" required="" aria-required="true" name="arrival" id="arrival" value="<?php echo ISSET($demobilized_date) ? $demobilized_date : "" ?>"/>
		    <label class="active" for="arrival">Arrival (Demobilized)</label>
	      </div>
	    </div>
    </div>

    <div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="remarks1" id="remarks1" value="<?php echo ISSET($mobilized_remarks) ? $mobilized_remarks : "" ?>"/>
		    <label class="active" for="remarks1">Remarks</label>
	      </div>
	    </div>
	
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="remarks2" id="remarks2" value="<?php echo ISSET($demobilized_remarks) ? $demobilized_remarks : "" ?>"/>
		    <label class="active" for="remarks2">Remarks</label>
	      </div>
	    </div>
    </div>
	<div class="row">
		<div class="md-footer default">
		  <a class="waves-effect waves-teal btn-flat cancel_modal" >Close</a>
		</div>
	</div>
</form>