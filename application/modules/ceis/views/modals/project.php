<?php 
$base 		     = ( ISSET($contract_details['base']) && ! EMPTY($contract_details['base'])) ? ($contract_details['base'] == CONTRACT_OVERSEAS) ? 'Overseas' : 'Local' : '';
$project_title   = ( ISSET($contract_details['project_title']) && ! EMPTY($contract_details['project_title'])) ? $contract_details['project_title'] : '';
$project_loc     = ( ISSET($contract_details['project_location']) && ! EMPTY($contract_details['project_location'])) ? $contract_details['project_location'] : '';

$province        = ( ISSET($contract_details['province']) && ! EMPTY($contract_details['province'])) ? $contract_details['province'] : '';
$country         = ( ISSET($contract_details['country_name']) && ! EMPTY($contract_details['country_name'])) ? $contract_details['country_name'] : '';
$status          = ( ISSET($contract_details['contract_status_name']) && ! EMPTY($contract_details['contract_status_name'])) ? $contract_details['contract_status_name'] : '';
$owner	         = ( ISSET($contract_details['owner']) && ! EMPTY($contract_details['owner'])) ? $contract_details['owner'] : '';

$duration	     = ( ISSET($contract_details['contract_duration']) && ! EMPTY($contract_details['contract_duration'])) ? $contract_details['contract_duration'] : '';
$actual	     	 = ( ISSET($contract_details['actual_percent']) && ! EMPTY($contract_details['actual_percent'])) ? $contract_details['actual_percent'] : '';
$scheduled	     = ( ISSET($contract_details['scheduled_percent']) && ! EMPTY($contract_details['scheduled_percent'])) ? $contract_details['scheduled_percent'] : '';


$project_cost    = ( ISSET($contract_details['project_cost']) && ! EMPTY($contract_details['project_cost'])) ? decimal_format($contract_details['project_cost']) : '';
$contract_cost   = ( ISSET($contract_details['contract_cost']) && ! EMPTY($contract_details['contract_cost'])) ? decimal_format($contract_details['contract_cost']) : '';

$start_date      = ( ISSET($contract_details['started_date']) && ! EMPTY($contract_details['started_date']) && $contract_details['started_date'] != DEFAULT_DATE) ? date('M d, Y', strtotime($contract_details['started_date'])) : '';
$completed_date  = ( ISSET($contract_details['completed_date']) && ! EMPTY($contract_details['completed_date']) && $contract_details['completed_date'] != DEFAULT_DATE) ? date('M d, Y', strtotime($contract_details['completed_date'])) : '';

$project_type  	 = ( ISSET($contract_details['project_type_name']) && ! EMPTY($contract_details['project_type_name'])) ? $contract_details['project_type_name'] : '';
$remarks	     = ( ISSET($contract_details['remarks']) && ! EMPTY($contract_details['remarks'])) ? $contract_details['remarks'] : '';
$involvement	 = ( ISSET($contract_details['involvement']) && ! EMPTY($contract_details['involvement'])) ? $contract_details['involvement'] : '';
?>

<form id="project_form">
	<div class="form-float-label font-normal">

    <div class="row row-special m-t-lg">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">home</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="base" id="base" value="<?php echo $base; ?>"/>
		    <label for="base" class="grey-text-bold active">Base</label>
	      </div>
  	</div>

	<div class="row row-special">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">description</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="project_title" id="project_title" value="<?php echo $project_title; ?>"/>
		    <label for="project_title" class="grey-text-bold active">Project Title</label>
	      </div>
	    
		  <div class="input-field col s6">
		  	<i class="material-icons prefix">location_on</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="project_location" id="project_location" value="<?php echo $project_loc; ?>"/>
		    <label for="project_location" class="grey-text-bold active">Project Location</label>
	      </div>
  	</div>
  	
  	
  	<div class="row row-special">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">location_city</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="province" id="province" value="<?php echo $province; ?>"/>
		    <label for="province" class="grey-text-bold active">Province</label>
	      </div>
	    
		  <div class="input-field col s6">
		  	<i class="material-icons prefix">location_city</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="country" id="country" value="<?php echo $country; ?>"/>
		    <label for="country" class="grey-text-bold active">Country</label>
	      </div>
  	</div>
	
	<div class="row row-special">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">hourglass_empty</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="status" id="status" value="<?php echo $status; ?>"/>
		    <label for="status" class="grey-text-bold active">Status</label>
	      </div>
  	</div>

	<div class="row row-special">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">monetization_on</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="project_cost" id="project_cost" value="<?php echo $project_cost; ?>"/>
		    <label for="project_cost" class="grey-text-bold active">Project Cost (USD)</label>
	      </div>
	    
		  <div class="input-field col s6">
		  	<i class="material-icons prefix">monetization_on</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="contract_cost" id="contract_cost" value="<?php echo $contract_cost; ?>"/>
		    <label for="contract_cost" class="grey-text-bold active">Contract Cost (USD)</label>
	      </div>
  	</div>


<?php if($contract_details['contract_status_id'] != CONTRACT_STATUS_ONHAND): ?>

	<div class="row row-special">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">account_circle</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="owner" id="owner" value="<?php echo $owner; ?>"/>
		    <label for="owner" class="grey-text-bold active">Owner</label>
	      </div>
  	</div>
	
	<div class="row row-special">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">library_books</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="project_type" id="project_type" value="<?php echo $project_type ?>"/>
		    <label for="project_type" class="grey-text-bold active">Project Type</label>
	      </div>

		  <div class="input-field col s6">
		   <i class="material-icons prefix">group</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="involvement" id="involvement" value="<?php echo $involvement; ?>"/>
		    <label for="involvement" class="grey-text-bold active">Extend of Involvement</label>
	      </div>
  	</div>

	<div class="row row-special">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">access_alarm</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="contract_duration" id="contract_duration" value="<?php echo $duration; ?>"/>
		    <label for="contract_duration" class="grey-text-bold active">Contract Duration</label>
	      </div>
  	</div>

	<div class="row row-special">
		  <div class="input-field col s6">
		   	 <i class="material-icons prefix">date_range</i> 
		   	 <input type="text" disabled class="black-text" required="" aria-required="true" name="start_date" id="start_date" value="<?php echo $start_date; ?>"/>
		   	 <label for="start_date" class="grey-text-bold active">Start Date</label>
	      </div>

<?php if($contract_details['contract_status_id'] == CONTRACT_STATUS_COMPLETED): ?>
		  <div class="input-field col s6">
		  	 <i class="material-icons prefix">date_range</i> 
		     <input type="text" disabled class="black-text" required="" aria-required="true" name="end_date" id="end_date" value="<?php echo $completed_date; ?>"/>
		     <label for="end_date" class="grey-text-bold active">Completed Date</label>
	      </div>
<?php endif; ?>

  	</div>
 
 
<?php if($contract_details['contract_status_id'] == CONTRACT_STATUS_ONGOING): ?> 	
	<div class="row row-special">
  		<div class="card-panel m-l-sm m-r-sm grey darken-3 p-md"><span class="font-semibold white-text">Percentage of Accomplishment</span></div>
  	</div>
  	
  	<div class="row row-special">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">gavel</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="scheduled" id="scheduled" value="<?php echo $scheduled; ?>"/>
		    <label for="scheduled" class="grey-text-bold active">Scheduled</label>
	      </div>

		  <div class="input-field col s6">
		   <i class="material-icons prefix">gavel</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="actual" id="actual" value="<?php echo $actual; ?>"/>
		    <label for="actual" class="grey-text-bold active">Actual</label>
	      </div>
  	</div>
<?php endif; ?>  	
  	
<?php endif; //ONHAND ?> 	
  	
  	<div class="row row-special">
		  <div class="input-field col s12">
		   <i class="material-icons prefix">description</i> 
		    <textarea class="materialize-textarea black-text" disabled><?php echo $remarks; ?></textarea>
		    <label for="scheduled" class="grey-text-bold active">Remarks</label>
	      </div>
  	</div>
  	



	

	<div class="md-footer default">
	  <a class="waves-effect waves-teal btn-flat cancel_modal grey lighten-3" >Cancel</a>
	</div>
</form>