<form id="construction_equipment_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
	<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
	<div class="form-float-label">

    <div class="row m-n">
	    <div class="col s12">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="project_title" id="project_title" value="<?php echo ISSET($project_title) ? $project_title : "" ?>"/>
		    <label class="active" for="project_title">Project Title</label>
	      </div>
	    </div>
    </div>

    <div class="row m-n">
	    <div class="col s12">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="project_location" id="project_location" value="<?php echo ISSET($project_location) ? $project_location : "" ?>"/>
		    <label class="active" for="project_location">Project Location</label>
	      </div>
	    </div>
    </div>

    <div class="row m-n">
	    <div class="col s8">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="owner" id="owner" value="<?php echo ISSET($owner) ? $owner : "" ?>"/>
		    <label class="active" for="owner">Owner</label>
	      </div>
	    </div>
	    <div class="col s4">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="project_cost" id="project_cost" value="<?php echo ISSET($project_cost) ? number_format($project_cost,2) : "" ?>"/>
		    <label class="active" for="project_cost">Project Cost</label>
	      </div>
	    </div>
    </div>

	<div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="positions" id="positions" value="<?php echo ISSET($positions) ? $positions : "" ?>"/>
		    <label class="active" for="positions">Positions</label>
	      </div>
	    </div>
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="project_type_code" id="project_type_code" value="<?php echo ISSET($project_type_code) ? $project_type_code : "" ?>"/>
		    <label class="active" for="project_type_code">Project Type</label>
	      </div>
	    </div>
	</div>
	<div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate datepicker" required="" aria-required="true" name="start_date" id="start_date" value="<?php echo ISSET($start_date) ? $start_date : "" ?>"/>
		    <label class="active" for="start_date">Start Date</label>
	      </div>
	    </div>
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate datepicker" required="" aria-required="true" name="end_date" id="end_date" value="<?php echo ISSET($end_date) ? $end_date : "" ?>"/>
		    <label class="active" for="end_date">End Date</label>
	      </div>
	    </div>
	</div>

    <div class="row m-n">
    </div>
	<div class="md-footer default">
	  <a class="waves-effect waves-teal btn-flat cancel_modal">Cancel</a>
	</div>
</form>