<form id="country_profile_form">
	<div class="scroll-pane" style="height:400px;">
		<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
		<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
		<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">

		<div class="form-float-label">
		  	<div class="row m-n">
		    	<div class="col s12">
			  		<div class="input-field">
			  			<label class="active" for="Country Name">Country Name</label>
						  	<select name="country_name" id="country_name" class="selectize" placeholder="Select Country Name" data-parsley-required="true" required="" aria-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						    	<option value="">Select Country Name</option>
						    	<?php foreach ($countries as $country): ?>
						    	<option <?php if($country["country_code"]==$country_code){ echo selected; } ?> value="<?php echo $country["country_code"]?>"><?php echo ISSET($country["country_name"]) ? $country["country_name"] : "" ?></option>
						    	<?php endforeach; ?>
						  	</select>
		      		</div>
		    	</div>
		  	</div>

		  	<div class="row m-n">
			  	<div class="col s12">
					<div class="input-field">
					  	<label for="highlights"></label>
					  	<textarea name="highlights" required="" aria-required="true" id="highlights" class="materialize-textarea" style="min-height:60px!important"><?php echo ISSET($highlights) ? $highlights : "" ?></textarea>
					</div>
			  	</div>		 
			</div>

			<div class="row m-n">
				<div class="col s12">
				  	<div class="table-cell valign-top n-p n-m" style="width:65%;">
				  		<div <?php echo ISSET($file_name) ? 'class="white-text m-n p-sm teal lighten-2"' : "" ?> >
				  			<!-- <?php echo ISSET($file_name) ? "FILE: ".$file_name : "" ?> -->
				  			<?php echo ISSET($file_name) ? "UPLOADED FILE: "."<u><a class='white-text' target='_blank' href='../uploads/files/".$file_name."'>".$file_name."</a></u>" : "" ?> 
						</div>
						<div class="scroll-pane field-multi-attachment" style="height:230px">
				      		<a href="#" id="file_upload" required="" aria-required="true" class="tooltipped m-r-sm" data-position="bottom" data-delay="50" data-tooltip="Upload">Choose a file to upload</a>
				    	</div>
				 	</div>
				</div>
			</div>
		</div>

		<div class="md-footer default">
			<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
		    <button class="btn waves-effect waves-light" id="save_country_profile" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
		</div>
	</div>
</form>

<script>
$(function(){  

	$('#country_profile_form').parsley();
	$('#country_profile_form').submit(function(e) {
	    e.preventDefault();
	    
		if ( $(this).parsley().isValid() ) {


			for (instance in CKEDITOR.instances) {
				CKEDITOR.instances[instance].updateElement();
			}
			
		  	var data = $(this).serialize();
		  
		  	button_loader('save_country_profile', 1);
		  	$.post("<?php echo base_url() . PROJECT_CEIS ?>/ceis_country_profile/process/", data, function(result) {
			if(result.flag == 0){
			  	notification_msg("<?php echo ERROR ?>", result.msg);
			  	button_loader('save_country_profile', 0);
			} 
			else {
			  	notification_msg("<?php echo SUCCESS ?>", result.msg);
			  	button_loader("save_country_profile",0);
			  	modalObj.closeModal();
			  
			  	load_datatable('country_profile_table', '<?php echo PROJECT_CEIS ?>/ceis_country_profile/get_country_profile_list/');
				}
		  		}, 'json');       
	    	}
	  	});
	  
	  	<?php if(ISSET($highlights)){ ?>
			$('.input-field label').addClass('active');
	  	<?php } ?>

	  	CKEDITOR.replace('highlights');
	})
</script>