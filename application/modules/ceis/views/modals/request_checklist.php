<?php 
$item 	 = ( ISSET($checklist_details['checklist']) && ! EMPTY($checklist_details['checklist']) ) ? $checklist_details['checklist'] : '';
$caption = ( ISSET($checklist_details['caption']) && ! EMPTY($checklist_details['caption']) ) ? $checklist_details['caption'] : '';
?>
<form id="op_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">

	<div class="form-float-label font-normal">

	<div class="row row-special m-t-lg">
	  <div class="input-field col s12">
	   <i class="material-icons prefix">list</i> 
	   <textarea class="materialize-textarea black-text"  style="border:none;" disabled><?php echo $item; ?></textarea>
	    <label for="project_title" class="grey-text-bold active">Item</label>
      </div>
    
	 
  	</div>
  	
  	<div class="row row-special">
	  <div class="input-field col s12">
	   <i class="material-icons prefix">description</i> 
	   	<textarea class="materialize-textarea black-text" disabled><?php echo $caption; ?></textarea>
	    <label class="grey-text-bold active">Caption</label>
      </div>
    </div> 




	</div><!-- end of row -->
        
    
	<div class="md-footer default">
	  <a class="waves-effect waves-teal btn-flat cancel_modal grey lighten-3">Cancel</a>
	</div>
	
</form>  
