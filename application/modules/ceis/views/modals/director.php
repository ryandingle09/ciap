<form id="project_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
	<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
	<div class="form-float-label">

    <div class="row m-n">
	    <div class="col s12">
		  	<label>Base</label><br><br>
		      	<input name="group1" type="radio" id="test1" />
		      	<label for="test1">local</label>
		      	<input name="group1" type="radio" id="test2" />
		      	<label for="test2">overseas</label>
		    
	    </div>

  	</div>

    <div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" readonly class="validate" required="" aria-required="true" name="registrant_name" id="registrant_name" value="<?php echo ISSET($registrant_name) ? $registrant_name : "" ?>"/>
		    <label for="registrant_name">Project Title</label>
	      </div>
	    </div>
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" readonly class="validate" required="" aria-required="true" name="registrant_address" id="registrant_address" value="<?php echo ISSET($registrant_address) ? $registrant_address : "" ?>"/>
		    <label for="registrant_address">Project Location</label>
	      </div>
	    </div>
	</div>

	<div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" readonly class="validate" required="" aria-required="true" name="registrant_name" id="registrant_name" value="<?php echo ISSET($registrant_name) ? $registrant_name : "" ?>"/>
		    <label for="registrant_name">Province</label>
	      </div>
	    </div>
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" readonly class="validate" required="" aria-required="true" name="registrant_address" id="registrant_address" value="<?php echo ISSET($registrant_address) ? $registrant_address : "" ?>"/>
		    <label for="registrant_address">Country</label>
	      </div>
	    </div>
	</div>

	<div class="row m-n">
	    <div class="col s12">
		  <div class="input-field">
		    <input type="text" readonly class="validate" required="" aria-required="true" name="registrant_name" id="registrant_name" value="<?php echo ISSET($registrant_name) ? $registrant_name : "" ?>"/>
		    <label for="registrant_name">Status</label>
	      </div>
	    </div>
	</div>

	<div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" readonly class="validate" required="" aria-required="true" name="registrant_name" id="registrant_name" value="<?php echo ISSET($registrant_name) ? $registrant_name : "" ?>"/>
		    <label for="registrant_name">Project Cost (USD)</label>
	      </div>
	    </div>
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" readonly class="validate" required="" aria-required="true" name="registrant_address" id="registrant_address" value="<?php echo ISSET($registrant_address) ? $registrant_address : "" ?>"/>
		    <label for="registrant_address">Contract Cost (USD)</label>
	      </div>
	    </div>
	</div>

	<div class="row">
    <form class="col s12">
      <div class="row">
        <div class="input-field col s12">
          <textarea id="textarea1" class="materialize-textarea"></textarea>
          <label for="textarea1">Remarks</label>
        </div>
      </div>
    </form>
  </div>

	<div class="md-footer default">
	  <a class="waves-effect waves-teal btn-flat" id="cancel_construction_equipment">Cancel</a>
	</div>
</form>