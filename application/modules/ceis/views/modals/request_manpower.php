<form id="manpower_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">

	<div class="form-float-label font-normal">

	    <div class="row row-special m-t-lg">
	      <div class="input-field col s6">
		   <i class="material-icons prefix">list</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="category" id="category" value="<?php echo ($schedule['category'] == MANPOWER_CATEGORY_DIRECT) ? 'DIRECT' : 'INDIRECT'; ?>"/>
		    <label for="project_title" class="grey-text-bold active">Category</label>
	      </div>	
	  	</div>
	  	
	  	<div class="row row-special m-t-lg">
	  	  <div class="input-field col s12">
		  	<i class="material-icons prefix">library_books</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="description_of_skill" id="description_of_skill" value="<?php echo $schedule['skills_description']?>"/>
		   	<!--  <textarea class="materialize-textarea black-text" disabled><?php echo $schedule['skills_description']; ?></textarea> -->
		    <label for="scheduled" class="grey-text-bold active">Skills</label>
	      </div>
	  	</div>
	  	
	  	<div class="row row-special m-t-lg">
		  <div class="input-field col s6">
		   <i class="material-icons prefix">monetization_on</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="salary_rate" id="salary_rate" value="<?php echo $schedule['salary_rate']?>"/>
		    <label for="project_title" class="grey-text-bold active">Salary Rate</label>
	      </div>
	  
		  <div class="input-field col s6">
		   <i class="material-icons prefix">payment</i> 
		    <input type="text" disabled class="black-text" required="" aria-required="true" name="number_required" id="number_required" value="<?php echo $schedule['number_required']?>"/>
		    <label for="project_title" class="grey-text-bold active">Number Required</label>
	      </div>
    </div>

	<div class="md-footer default">	  
	  <a class="waves-effect waves-teal btn-flat cancel_modal grey lighten-3">Cancel</a>
	</div>
	
</form>  
