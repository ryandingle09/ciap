<?php 
$request  = (ISSET($task_details['request_type_name']) && ! EMPTY($task_details['request_type_name'])) ? $task_details['request_type_name'] : '';
$task     = (ISSET($task_details['task_name']) && ! EMPTY($task_details['task_name'])) ? $task_details['task_name'] : '';
$resource_name  = (ISSET($task_details['resource_name']) && ! EMPTY($task_details['resource_name'])) ? $task_details['resource_name'] : '';
$task_status_id = (ISSET($task_details['task_status_id']) && ! EMPTY($task_details['task_status_id'])) ? $task_details['task_status_id'] : '';
?>
<form id="attachment_form" class="m-t-md form-select">
<div class="table-display">
	<div class="table-cell p-md valign-top b-r" style="border-color:#eee!important;width:60%;">
		<div  id="ddt-scroll" class="row row-special  col s12 scroll-pane" style="height:450px;" >
			<table class="table table-advanced table-layout-auto" id="table_attachments">
				<thead>
				   	<tr>
					     <th width="15%">Date</th>
					     <th width="15%">Type</th>
					     <th width="35%">Description</th>
					     <th width="20%">By</th>
					     <th width="15%">Action</th>
				     </tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
	
	<div class="table-cell p-l-md p-r-md m-t-md valign-top font-normal" style="width:40%; position:relative;">
			<input type="hidden" name="security" value="<?php echo $security;?>" />
			<input type="hidden" id="task_attachment_id" name="task_attachment_id" />
	
			<div class="row m-b-n">
				<div class="input-field col s12">
					<i class="material-icons prefix">gavel</i> 
			    	<input type="text" disabled class="black-text" required="" aria-required="true" name="task" id="task" value="<?php echo $task; ?>"/>
			   	 	<label for="project_title" class="grey-text-bold active">Task</label>
		   	 	</div>
			</div>
			
			<div class="row m-b-n">
				<div class="input-field col s6">
					<i class="material-icons prefix">date_range</i> 
			    	<input type="text"  class="datepicker dnb" data-parsley-required="true" name="date_received" id="date_received" value="<?php //echo $task; ?>"/>
			   	 	<label for="project_title" class="grey-text-bold active">Date Received</label>
		   	 	</div>
			
				<div class="input-field col s6">
					<i class="material-icons prefix select-icon">library_books</i> 
			    	<select name="document_type" id="document_type" class="selectize"  placeholder="Select Type" data-parsley-required="true" >
					    	<option value="">Please Select</option>
					    	<option value="1">Client</option>
					    	<option value="2">Internal</option>					 
					 </select>
			   	 	<label for="document_type" class="grey-text-bold active p-b-xxs">Type of Document</label>
		   	 	</div>
			</div>
	
			<div class="row m-b-n">
			  <div class="input-field col s12">
			   <i class="material-icons prefix">description</i> 
			    <textarea style="max-height:45px;" name="description" id="description" class="materialize-textarea black-text" data-parsley-required="true"></textarea>
			    <label for="description" class="active">Description</label>
		      </div>
	  		</div>
		
			<div class="row row-special m-b-n" style="padding-left:10px;">
			
				<div class="table-cell valign-top n-p n-m">
					<div class="afu-container-mbn">
			      		<div id="file_upload" >Choose a file to upload</div>
			    	</div>
			 	</div>
	
			</div>
		
			<div class="default" style="width:100%;position:absolute; bottom:0;">
				<div class="md-footer default">
					<button class="btn waves-effect waves-light"  id="save_task_attachment" name="action" type="button" value="Save">Save</button>
			  		<a class="waves-effect waves-teal btn-flat cancel_modal grey lighten-3">Cancel</a>
			 	</div>
			</div>
			
		
	</div>
</div>

</form>

<script>
$(function(){
	$('#ddt-scroll').jScrollPane({autoReinitialise: true});
});
</script>



