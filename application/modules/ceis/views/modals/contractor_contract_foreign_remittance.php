<form id="finance_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
	<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
	<div class="form-float-label">

    <div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate datepicker" required="" aria-required="true" name="date" id="date" value="<?php echo ISSET($remitt_date) ? $remitt_date : "" ?>"/>
		    <label class="active" for="date">Date</label>
	      </div>
	    </div>
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="remit" id="remit" value="<?php echo ISSET($remitt_amount) ? number_format($remitt_amount,'2') : "" ?>"/>
		    <label class="active" for="remit">Amount Remitted</label>
	      </div>
	    </div>
    </div>

	<div class="row m-n">
		<div class="col s12">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="country" id="country" value="<?php echo ISSET($country_name) ? $country_name : "" ?>"/>
		    <label class="active" for="country">Country of Origin</label>
	      </div>
	    </div>
    </div>

    <div class="row m-n">
		<div class="col s12">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="bank" id="bank" value="<?php echo ISSET($receiving_bank) ? $receiving_bank : "" ?>"/>
		    <label class="active" for="bank">Receiving Bank</label>
	      </div>
	    </div>
    </div>
	<div class="row">
		<div class="md-footer default">
		  <a class="waves-effect waves-teal btn-flat cancel_modal" >Close</a>
		</div>
	</div>
</form>