<div class="modal-dialog" style="width: 95%;">
  <div class="modal-content">
    <div class="modal-header bg-dark">
      <button type="button" class="close" data-dismiss="modal">&times;</button>

      <h4 class="modal-title"><?php echo "NEW CONTRACTS (ANNEX B)" ?></h4>
    </div>

    <div class="modal-body">  
      <section>
        <div class="panel-body" style="height: 800px;">
          <div class="col-sm-12" style="height: 100%;">
          
              <?php if (!empty($pdf)): ?>
                <object width="100%" height="100%" type="application/pdf" data="data:application/pdf;base64,<?php echo base64_encode($pdf) ?>">
                  Cant display PDF
                </object>
              <?php else:  ?>
                <div class="title"><h4>Cant display PDF</h4></div>
              <?php endif; ?>

          </div>
        </div>
      </section>
    </div>
    
    <div class="modal-footer">
      <a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
    </div>
  </div>
</div>