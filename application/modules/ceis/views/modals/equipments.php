<form id="equipment_form">

	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
	<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
	
	<div class="form-float-label">
	  	<div class="row m-n">
	    	<div class="col s6">
		  		<div class="input-field">
		    		<input type="text" readonly class="validate" required="" aria-required="true" name="registrant_name" id="registrant_name" value="<?php echo ISSET($registrant_name) ? $registrant_name : "" ?>"/>
		    		<label for="registrant_name">Name of Registrant</label>
	      		</div>
	    	</div>

		    <div class="col s6">
			  	<div class="input-field">
			    	<input type="text" readonly class="validate" required="" aria-required="true" name="registrant_address" id="registrant_address" value="<?php echo ISSET($registrant_address) ? $registrant_address : "" ?>"/>
			    	<label for="registrant_address">Address of Registrant</label>
		      	</div>
		    </div>
	  	</div>

	  	<div class="row m-n">
	    	<div class="col s3">
		  		<div class="input-field">
			    	<input type="text" readonly class="validate" required="" aria-required="true" name="serial_no" id="serial_no" value="<?php echo ISSET($serial_no) ? $serial_no : "" ?>"/>
			    	<label for="serial_no">Serial Number</label>
	      		</div>
	    	</div>
	    	<div class="col s3">
		  		<div class="input-field">
		    		<input type="text" readonly class="validate" required="" aria-required="true" name="motor_no" id="motor_no" value="<?php echo ISSET($motor_no) ? $motor_no : "" ?>"/>
			    	<label for="motor_no">Motor Number</label>
		    	</div>
	    	</div>
		    <div class="col s3">
			  	<div class="input-field">
			    	<input type="text" readonly class="validate" required="" aria-required="true" name="brand" id="brand" value="<?php echo ISSET($brand) ? $brand : "" ?>"/>
			    	<label for="brand">Brand</label>
		      	</div>
		    </div>
		    <div class="col s3">
			  	<div class="input-field">
			    	<input type="text" readonly class="validate" required="" aria-required="true" name="color" id="color" value="<?php echo ISSET($color) ? $color : "" ?>"/>
			    	<label for="color">Color</label>
		      	</div>
		    </div>
	  	</div>

	  	<div class="row m-n">
		    <div class="col s3">
			  	<div class="input-field">
			    	<input type="text" readonly class="validate" required="" aria-required="true" name="model" id="model" value="<?php echo ISSET($model) ? $model : "" ?>"/>
			    	<label for="model">Model</label>
		      	</div>
		    </div>
		    <div class="col s3">
			  	<div class="input-field">
			    	<input type="text" readonly class="validate" required="" aria-required="true" name="year" id="year" value="<?php echo ISSET($year) ? $year : "" ?>"/>
			    	<label for="year">Year</label>
		      	</div>
		    </div>
		    <div class="col s3">
			  	<div class="input-field">
			    	<input type="text" readonly class="validate" required="" aria-required="true" name="capacity" id="capacity" value="<?php echo ISSET($capacity) ? $capacity : "" ?>"/>
			    	<label for="capacity">Size/Capacity</label>
		      	</div>
		    </div>
		    <div class="col s3">
			  	<div class="input-field">
			    	<input type="text" readonly class="validate" required="" aria-required="true" name="flywheel" id="flywheel" value="<?php echo ISSET($flywheel) ? $flywheel : "" ?>"/>
			    	<label for="flywheel">Flywheel HP</label>
		      	</div>
		    </div>
	 	</div>

	  	<div class="row m-n">
		    <div class="col s6">
			  	<div class="input-field">
			    	<input type="text" readonly class="validate" required="" aria-required="true" name="acquired_date" id="acquired_date" value="<?php echo ISSET($acquired_date) ?>"/>
			    	<label for="acquired_date">Date Acquired</label>
		      	</div>
		    </div>
		    <div class="col s6">
			  	<div class="input-field">
			    	<input type="text" readonly class="validate" required="" aria-required="true" name="acquisition_cost" id="acquisition_cost" value="<?php echo $acquisition_cost ?>"/>
			    	<label for="acquisition_cost">Acquisition Cost</label>
		      	</div>
		    </div>
	  	</div>

		<div class="row m-n">
		    <div class="col s6">
			  	<div class="input-field">
			    	<input type="text" readonly class="validate" required="" aria-required="true" name="acquired_from" id="acquired_from" value="<?php echo $acquired_from ?>"/>
			    	<label for="acquired_from">Acquired From</label>
		      	</div>
		    </div>
		    <div class="col s6">
			  	<div class="input-field">
			    	<input type="text" readonly class="validate" required="" aria-required="true" name="location" id="location" value="<?php echo $location ?>"/>
			    	<label for="location">Equipment Present Location</label>
		      	</div>
		    </div>
		</div>

	  	<div class="row m-n">
		    <div class="col s6">
			  	<div class="input-field">
			    	<input type="text" readonly class="validate" required="" aria-required="true" name="present_condition" id="present_condition" value="<?php echo $present_condition ?>"/>
			    	<label for="present_condition">Present Condition</label>
		      	</div>
		    </div>
		    <div class="col s6">
			  	<div class="input-field">
			    	<input type="text" readonly class="validate" required="" aria-required="true" name="ownership_type" id="ownership_type" value="<?php echo $ownership_type ?>"/>
			    	<label for="ownership_type"><?php echo $popo; ?> Type of Ownership</label>
		      	</div>
		    </div>
	    </div>

       	<div class="row m-n">
		    <div class="col s12">
			  	<div class="input-field">
			    	<input type="text" readonly class="validate" required="" aria-required="true" name="position_name" id="position_name" value="<?php echo $position_name ?>"/>
			    	<label for="position_name">Submitted Documents</label>
		      	</div>
		    </div>
	  	</div>
	<div class="md-footer default">
	  <a class="waves-effect waves-teal btn-flat cancel_modal" >Close</a>
	</div>
</form>

<script>
$(function(){	
	$('.input-field label').addClass('active');
});
</script>