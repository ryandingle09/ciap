<form id="finance_form">
	<ul class="collapsible panel p-n" data-collapsible="expandable">
      	<div class="row">
		    <div class="col s3 p-r-n">
		      	<table class="striped" id="statement_list_table"> 
		      		<thead>
		      			<tr><th>&nbsp;</th></tr>
		      		</thead>
					<tbody id="statement_list_tbody">	
						<tr><th>Total Current Assets</th></tr>	
						<tr><th>Total Non Current Assets</th></tr>
						<tr><th>Total Current Liabilities</th></tr>		
						<tr><th>Long Term Liabilities</th></tr>	
						<tr><th>Net Worth</th></tr>	
						<tr><th>Gross Sales/Revenues</th></tr>	
						<tr><th>Net Profit After Tax</th></tr>	
						<tr><th>No. of common shares outstanding</th></tr>	
						<tr><th>PAR Value per common share</th></tr>	
					<tbody id="statement_list_tbody"></tbody>
				</table>
			</div>
			<div class="col s9 p-l-n" style="overflow-x:auto;">
				<table class="striped" id="statement_list_table"> 
					<thead>
						<?php echo $view_statement['thead']; ?>
					</thead>
					<tbody id="statement_list_tbody">

						<?php echo $view_statement['tbody']; ?>
						
					</tbody>
				</table>
			</div>
		</div>
	</ul>

	<div class="row modal-fixed-footer">
		<div class="md-footer default ">
		  <a class="waves-effect waves-teal btn-flat cancel_modal" >Close</a>
		</div>
	</div>
</form>