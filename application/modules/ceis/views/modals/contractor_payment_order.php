<form id="construction_equipment_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
	<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
	<div class="form-float-label">

    <div class="row m-n">
	    <div class="col s12">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="contractor_name" id="contractor_name" value="<?php echo ISSET($contractor_name) ? $contractor_name : "" ?>"/>
		    <label class="active" for="contractor_name">Contractor Name</label>
	      </div>
	    </div>
    </div>

    <div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="pocb" id="pocb" value="POCB"/>
		    <label class="active" for="pocb">Collection Group</label>
	      </div>
	    </div>
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="datepicker" required="" aria-required="true" name="op_date" id="op_date" value="<?php echo ISSET($op_date) ? $op_date : "" ?>"/>
		    <label class="active" for="op_date">OP Date</label>
	      </div>
	    </div>
    </div>
	<div class="row m-n">
      	<table class="striped">
	        <thead>
	          	<tr>
	              	<th>Nature of Collection</th>
	              	<th>Amount</th>
	          	</tr>
	        </thead>
	        <tbody>
              <?php foreach ($collection_info as $data): ?>
              <tr>
                <td><?php echo ISSET($data['coll_nature_code']) ? $data['coll_nature_code'] : "" ; ?></td>
                <td><?php echo ISSET($data['amount']) ? number_format($data['amount'],2) : "" ; ?></td>
              </tr>
              <?php endforeach; ?>
            </tbody>
      	</table>
  	</div>
            
    <div class="row m-n">
    </div>
	<div class="md-footer default">
	  <a class="waves-effect waves-teal btn-flat cancel_modal">Cancel</a>
	</div>
</form>