<form id="project_type_form">
	<div class="panel p-md" style="height: 600px;">
		<div class="col s12" style="height: 100%;">
			<?php if (!empty($pdf)): ?>
				<object width="100%" height="100%" type="application/pdf" data="data:application/pdf;base64,<?php echo base64_encode($pdf) ?>">
					Cant display PDF
				</object>
			<?php else:  ?>
				<div class="title"><h4>Cant display PDF</h4></div>
			<?php endif; ?>
		</div>
	</div>
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	</div>
</form>
  <!-- <div class="md-overlay"></div> -->

<!-- <div class="modal-dialog" style="width: 95%;">
	<div class="modal-content">
		<div class="modal-header bg-dark">
			<button type="button" class="close" data-dismiss="modal">&times;</button>

			<h4 class="modal-title"><?php echo "SKILLS SUMMARY" ?></h4>
		</div>

		<div class="modal-body">	
			<section>
				<div class="panel-body" style="height: 800px;">
					<div class="col-sm-12" style="height: 100%;">
						<?php if (!empty($pdf)): ?>
							<object width="100%" height="100%" type="application/pdf" data="data:application/pdf;base64,<?php echo base64_encode($pdf) ?>">
								Cant display PDF
							</object>
						<?php else:  ?>
							<div class="title"><h4>Cant display PDF</h4></div>
						<?php endif; ?>
					</div>
				</div>
			</section>
		</div>
	
		<div class="modal-footer">
			<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
		</div>
	</div>
</div> -->