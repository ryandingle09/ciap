<form id="finance_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
	<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
	<div class="form-float-label">

    <div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate datepicker" required="" aria-required="true" name="statement_date" id="statement_date" value="<?php echo ISSET($statement_date) ? $statement_date : "" ?>"/>
		    <label for="statement_date">Statement Date</label>
	      </div>
	    </div>
	
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="total_current_assets" id="total_current_assets" value="<?php echo ISSET($total_current_assets) ? number_format($total_current_assets,2) : "" ?>"/>
		    <label for="total_current_assets">Total Current Assets</label>
	      </div>
	    </div>
    </div>

	<div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="total_non_current_assets" id="total_non_current_assets" value="<?php echo ISSET($total_non_current_assets) ? number_format($total_non_current_assets,2) : "" ?>"/>
		    <label for="total_non_current_assets">Total Non Current Assets</label>
	      </div>
	    </div>
	
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="total_current_liabilities" id="total_current_liabilities" value="<?php echo ISSET($total_current_liabilities) ? number_format($total_current_liabilities,2) : "" ?>"/>
		    <label for="total_current_liabilities">Total Current Liabilities</label>
	      </div>
	    </div>
    </div>

	<div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="long_term_liabilities" id="long_term_liabilities" value="<?php echo ISSET($long_term_liabilities) ? number_format($long_term_liabilities,2) : "" ?>"/>
		    <label for="long_term_liabilities">Long Term Liabilities</label>
	      </div>
	    </div>
	
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="networth" id="networth" value="<?php echo ISSET($networth) ? number_format($networth,2) : "" ?>"/>
		    <label for="networth">Net Worth</label>
	      </div>
	    </div>
    </div>

	<div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="gross_sales" id="gross_sales" value="<?php echo ISSET($gross_sales) ? number_format($gross_sales,2) : "" ?>"/>
		    <label for="gross_sales">Gross Sales/Revenues</label>
	      </div>
	    </div>
	
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="net_profit" id="net_profit" value="<?php echo ISSET($net_profit) ? number_format($net_profit,2) : "" ?>"/>
		    <label for="net_profit">Net Profit after tax</label>
	      </div>
	    </div>
    </div>
    
	<div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="common_shares" id="common_shares" value="<?php echo ISSET($common_shares) ? number_format($common_shares,2) : "" ?>"/>
		    <label for="common_shares">No. of common shares outstanding</label>
	      </div>
	    </div>
	
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="par_value" id="par_value" value="<?php echo ISSET($par_value) ? number_format($par_value,2) : "" ?>"/>
		    <label for="par_value">PAR Value per common share</label>
	      </div>
	    </div>
	</div>
	<div class="row">
		<div class="md-footer default">
		  <a class="waves-effect waves-teal btn-flat cancel_modal" >Close</a>
		</div>
	</div>
</form>

<?php if(ISSET($statement_date)){ ?>
	<script>
		$('.input-field label').addClass('active');
	</script>
<?php } ?>