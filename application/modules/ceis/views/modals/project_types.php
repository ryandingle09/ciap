<form id="project_type_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
	<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
	<div class="form-float-label">
	  	<div class="row m-n">
		    <div class="col s6">
			  <div class="input-field">
			    <input type="text" class="validate" <?php echo ISSET($readonly_value) ? $readonly_value : "" ?> required="" aria-required="true" name="project_type_code" id="project_type_code" value="<?php echo ISSET($project_type_code) ? $project_type_code : "" ?>"/>
			    <label for="project_type_code">Code of Project Type</label>
		      </div>
		    </div>
		    <div class="col s6">
			  <div class="input-field">
			    <input type="text" class="validate" required="" aria-required="true" name="project_type_name" id="project_type_name" value="<?php echo ISSET($project_type_name) ? $project_type_name : "" ?>"/>
			    <label for="project_type_name">Name of Project Type</label>
		      </div>
		    </div>
	    </div>
	    <div class="row m-n">
	    	<div class="col s12">
	    		<div class="m-t-lg"></div>
			  	<div class="input-field">
			  	<label class="active" for="classification">Classification</label>
				  	<select name="classification" id="classification" class="selectize" placeholder="Select Classification" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
				  		<option value="Select Classification">Select Classification</option>
				    	<option <?php if($classification=='H'){ echo 'selected'; }?> value="H">Horizontal Construction</option>
				    	<option <?php if($classification=='V'){ echo 'selected'; }?> value="V">Vertical Construction</option>
				    	<option <?php if($classification=='O'){ echo 'selected'; }?> value="O">Others</option>
				  	</select>
		      	</div>
	    	</div>
	  	</div>
	</div>
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	    <button class="btn waves-effect waves-light" id="save_project_type" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	</div>
</form>

<script>
$(function(){
  //help_text("role_form");
  
  $('#project_type_form').parsley();
  $('#project_type_form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {
	  var data = $(this).serialize();
	  
	  button_loader('save_project_type', 1);
	  $.post("<?php echo base_url() . PROJECT_CEIS ?>/ceis_project_types/process/", data, function(result) {
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  button_loader('save_project_type', 0);
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  button_loader("save_project_type",0);
		  modalObj.closeModal();
		  
		  load_datatable('project_type_table', '<?php echo PROJECT_CEIS ?>/ceis_project_types/get_project_type_list/');
		}
	  }, 'json');       
    }
  });
  
  <?php if(ISSET($project_type_code)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>
})

</script>