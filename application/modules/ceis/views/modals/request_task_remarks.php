<?php 
$request  = (ISSET($task_details['request_type_name']) && ! EMPTY($task_details['request_type_name'])) ? $task_details['request_type_name'] : '';
$task     = (ISSET($task_details['task_name']) && ! EMPTY($task_details['task_name'])) ? $task_details['task_name'] : '';
$resource_name  = (ISSET($task_details['resource_name']) && ! EMPTY($task_details['resource_name'])) ? $task_details['resource_name'] : '';
$task_status_id = (ISSET($task_details['task_status_id']) && ! EMPTY($task_details['task_status_id'])) ? $task_details['task_status_id'] : '';
?>

<div class="table-display">
	<div class="table-cell p-md valign-top b-r" style="border-color:#eee!important;">
		<div id="dt-scroll" class="row row-special  col s12 scroll-pane" style="min-height:300px;">
			<table class="table table-advanced table-layout-auto" id="table_remarks">
				<thead>
				   	<tr>
					     <th width="15%">Date</th>
					     <th width="50%" >Remarks</th>
					     <th width="25%">By</th>
					     <th width="10%">Action</th>
				     </tr>
				</thead>
				<tbody></tbody>
			</table>
		</div>
	</div>
	<div class="table-cell p-l-md p-r-md m-t-md valign-top font-normal" style="width:40%;">
		<form id="remarks_form" class="m-t-md">
			<input type="hidden" name="security" value="<?php echo $security;?>" />
			<input type="hidden" id="task_remark_id" name="task_remark_id" />
	
			<div class="row row-special">
				<div class="input-field col s12">
					<i class="material-icons prefix">gavel</i> 
			    	<input type="text" disabled class="black-text" required="" aria-required="true" name="task" id="task" value="<?php echo $task; ?>"/>
			   	 	<label for="project_title" class="grey-text-bold active">Task</label>
		   	 	</div>
			</div>
			
			<div class="row row-special">
			  <div class="input-field col s12">
			   <i class="material-icons prefix">description</i> 
			    <textarea style="max-height:45px;" name="remarks" id="remarks" class="materialize-textarea black-text" data-parsley-required="true"></textarea>
			    <label for="remarks" class="active">Remarks</label>
		      </div>
	  		</div>
			
			<div class="md-footer default">
			  <button class="btn waves-effect waves-light"  id="save_task_remarks" name="action" type="button" value="Save">Save</button>
			  <a class="waves-effect waves-teal btn-flat cancel_modal grey lighten-3">Cancel</a>
			</div>
		</form>
	</div>
</div>




<script>
$(function(){
	$('#dt-scroll').jScrollPane({autoReinitialise: true});
});
</script>



