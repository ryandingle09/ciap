<form id="opportunity_form">
	<input type="hidden" name="security" value="<?php echo $security ?>">
	<input type="hidden" name="salt" value="<?php echo $salt ?>">
	<input type="hidden" name="token" value="<?php echo $token ?>">
	<div class="form-float-label">
	  	<div class="row m-n">
		    <div class="col s6">
			  	<div class="input-field">
                      <label class="active" for="reference_no">Same as References</label>
                      <select name="reference_no" id="reference_no" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
                        <option value="0">Save as new opportunity</option>
                        <?php foreach($distinct_opportunities  as $opportunity): ?>
                        <option <?php if($opportunity["reference_no"]==$reference_no){ echo 'selected'; } ?> value="<?php echo $opportunity["reference_no"]?>"><?php echo ISSET($opportunity["reference_no"]) ? $opportunity["reference_no"] : "" ?></option>      
                        <?php endforeach; ?>
                      </select>   
			  	</div>
	    	</div>
	    	<div class="col s6">
			  	<div class="input-field">
			    	<label>Received Date <i class="fa fa-calendar"></i></label>
                    <input name="received_date" required="" aria-required="true" class="validate datepicker" size="16" type="text" value="<?php echo ISSET($received_date) ? $received_date : "" ; ?>" data-date-format="yyyy-mm-dd" >
                </div>
	  		</div>
  		</div>
	  	<div class="row m-n">
		    <div class="col s3">
		    	<div class="input-field">
		      	  	<label class="active">Source </label>
		          	<select name="source" id="source" required="" data-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
		                <option value="">Select Source</option>
		                <?php foreach($sources as $source): ?>
		                <option <?php if($source["opp_source_id"]==$opp_source_id){ echo 'selected'; } ?> value="<?php echo ISSET($source["opp_source_id"]) ? $source["opp_source_id"] : "" ?>"><?php echo ISSET($source["source"]) ? $source["source"] : "" ?></option>      
		                <?php endforeach; ?>
		          	</select>
	          	</div>
	        </div>

		    <div class="col s9">
		    	<div class="input-field">
		          	<label>Agency</label>
		          	<input type="text" name="source_agency" class="" required="" data-required="true" value="<?php echo ISSET($source_agency) ? $source_agency : "" ?>">
	          	</div>
	      	</div>
		</div>

		<div class="row m-n">
		    <div class="col s12">
			 	<div class="input-field">
					<textarea id="subject" class="materialize-textarea m-n" required="" name="subject" ><?php echo ISSET($subject) ? $subject : "" ; ?></textarea>
					<label for="subject">Subject</label>
				</div>
		    </div>
		</div>
	</div>
	<div class="row m-n">
		    <div class="col s6 p-t-md">
		 		<input type="checkbox" name="referrals" id="referrals" />
      			<label for="referrals">Send Referrals</label>
		    </div>
	    </div>
	<div class="card-action">
 		<div class="row m-n grey lighten-3">
		    <div class="col s6 p-t-sm">
		 		<h6>RECIPIENTS</h6>
		    </div>
		    <div class="pull-right col s6 p-t-sm p-b-sm">
		 		<a href="javascript:;" id="add_row" class="pull-right btn add_row">Add Contractor</a>
		    </div>
		</div>
	</div>

	<div class="row p-t-n" >
        <div class="col s12">
	        <div class="form-float-label">
		        <div class="card" >
			        
		            <div class="card-content white-text">
	              		  <table class="table table-striped" id="contractor">
	              			<thead>
	              				<tr>
		              				<th align="center">&nbsp;&nbsp; Contractors</th>
						            <th align="center">&nbsp;&nbsp; Email</th>                    
						            <th align="center">&nbsp;&nbsp; Action</th>
	              				</tr>
	              			</thead>
	              			<tbody>

	              				<tr style="<?php echo (count($recipient)) ?  'display:none;' : ''; ?> ">
		              				<td>
							          	<div >
								          	<select name="contractor_list[]" class="contractor_list browser-default" id="contractor_list" >
							                  	<option value="0">Not in the list</option>
							                  	<?php 
							                  		$not_in_the_list = 1;
							                  		foreach($contractors as $contractor)
							                  		{

							                  			$sel = "";
							                  			if($contractor['contractor_name'] == $recipient['contractor_name'])							                  				
							                  			{
							                  				$not_in_the_list = 0;
							                  				$sel =  "selected";
							                  			}
							                  			echo '<option '.$sel.' value="'.$contractor['contractor_name'].'">'.$contractor["contractor_name"].'</option>';
							                  		}

							                  	?>							                  	
							                </select>
							            </div>
							           
							          	<div id="new_contractor" class="input-field" style="<?php echo ($not_in_the_list) ?  '' : 'display:none;' ; ?> ">
								          	<input type="text" id="contractor_name" name="contractor_name[]" value="" placeholder="Enter contractor name">
							          	</div>
							          	
		              				</td>
		              				<td>
								    	<div class="input-field row">
								          	<input type="email" name="email[]" id="email" class="" data-type="email" placeholder="Enter Email" data-required="true" value="<?php echo $recipient['email'] ?>">
							          	</div>
		              				</td>
		              				<td>
							    		<a href="javascript:;" class='tooltipped' data-tooltip='View'><i class="flaticon-recycle69"></i></a>
		              				</td>
		              			</tr>



              				<?php
              					foreach($recipient as $recipient)
              					{	
              				?>
		              			<tr>
		              				<td>
							          	<div >

								          	<select name="contractor_list[]" class="contractor_list browser-default" id="contractor_list" >
							                  	<option value="0">Not in the list</option>
							                  	<?php 
							                  		$not_in_the_list = 1;
							                  		foreach($contractors as $contractor)
							                  		{

							                  			$sel = "";
							                  			if($contractor['contractor_name'] == $recipient['contractor_name'])							                  				
							                  			{
							                  				$not_in_the_list = 0;
							                  				$sel =  "selected";
							                  			}
							                  			echo '<option '.$sel.' value="'.$contractor['contractor_name'].'">'.$contractor["contractor_name"].'</option>';
							                  		}

							                  	?>							                  	
							                </select>
							            </div>
							            
							          	<div id="new_contractor" class="input-field" style="<?php echo ($not_in_the_list) ?  '' : 'display:none;' ; ?> ">
								          	<input type="text" id="contractor_name" name="contractor_name[]" class="" data-required="true" value="<?php echo $recipient['contractor_name'] ?>" placeholder="Enter contractor name">
							          	</div>
							        
		              				</td>
		              				<td>
								    	<div class="input-field row">
								          	<input type="email" name="email[]" id="email" class="" data-type="email" placeholder="Enter Email" data-required="true" value="<?php echo $recipient['email'] ?>">
							          	</div>
		              				</td>
		              				<td>
							    		<a href="javascript:;"><i class="flaticon-recycle69"></i></a>
		              				</td>
		              			</tr>

		              			<?php
		              				}
		              			?>



					<!-- sample -->					
						<!-- 
	              			<tr>
	              				<td>
						          	<div class="input-field row">
							          	<select name="contractor_list[]" class="contractor_list selectize" id="contractor_list" >
						                  	<option value="0">Not in the list</option>
						                  	<?php foreach($contractors as $contractor): ?>
						                  	<option value="<?php echo $contractor["contractor_name"]?>"><?php echo $contractor["contractor_name"] ?></option>      
						                  	<?php endforeach; ?>
						                </select>
						          	</div> -->
						          	<!-- <div id="new_contractor" class="input-field">
							          	<input type="text" id="contractor_name" name="contractor_name[]" class="" data-required="true" value="<?php echo $recipient['contractor_name']; ?>" placeholder="Enter contractor name">
						          	</div>
	              				</td>
	              				<td>
							    	<div class="input-field row">
							          	<input type="email" name="email[]" id="email" class="" data-type="email" placeholder="Enter Email" data-required="true" value="<?php echo $recipient['email']; ?>">
						          	</div>
	              				</td>
	              				<td>
						    		<a href="javascript:;"><i class="flaticon-recycle69"></i></a>
	              				</td>
	              			</tr> 
	              		--> 
					<!-- end sample -->
	              			</tbody>
	          			</table>
		            </div>
		        </div>
	        </div>
        </div>
    </div>
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	    <button class="btn waves-effect waves-light" id="save_opportunity" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	</div>
</form>

<script>
	<?php if(ISSET($reference_no)){ ?>
	$('.input-field label').addClass('active');
<?php } ?>
</script>