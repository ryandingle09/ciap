<?php 
$op_date   = ( ISSET($header['op_date']) && ! EMPTY($header['op_date']) && $header['op_date'] != DEFAULT_DATE ) ? date('m/d/Y', strtotime($header['op_date'])) : '';
$w_ref_no  = ( ISSET($request['reference_no']) && ! EMPTY($request['reference_no'])) ? TRUE : FALSE; 
$disabled  = ($w_ref_no) ? 'disabled' : '';
?>
<form id="op_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">

	<div class="form-float-label font-normal">

	<div class="row row-special m-t-lg">
	  <div class="input-field col s6">
	   <i class="material-icons prefix">account_circle</i> 
	    <input type="text" disabled class="black-text" required="" aria-required="true" name="firm_name" id="firm_name" value="<?php echo $contractor_name; ?>"/>
	    <label for="project_title" class="grey-text-bold active">Firm Name</label>
      </div>
    
	  <div class="input-field col s6">
	  	<i class="material-icons prefix">library_books</i> 
	    <input type="text" disabled class="black-text" required="" aria-required="true" name="collection_group" id="collection_group" value="POCB"/>
	    <label for="project_location" class="grey-text-bold active" >Collection Group</label>
      </div>
  	</div>
  	
  	<div class="row row-special">
	  <div class="input-field col s6">
	   <i class="material-icons prefix">date_range</i> 
	    <input type="text" <?php echo $disabled; ?> value="<?php echo $op_date; ?>" data-parsley-required="true"  name="op_date" id="op_date" class="datepicker dnb"/>
	    <label for="op_date">Order of Payment Date</label>
      </div>
    </div> 
    
<?php if($w_ref_no): ?>   
    <div class="row row-special">
	  <div class="input-field col s6">
	   <i class="material-icons prefix">date_range</i> 
	    <input type="text" value="<?php echo date('m/d/Y', strtotime($header['or_date'])); ?>" data-parsley-required="true"  name="or_date" id="or_date" class="datepicker dnb" <?php echo $disabled; ?>/>
	    <label for="op_date">OR Date</label>
      </div>
    </div> 
    
    <div class="row row-special">
	  <div class="input-field col s6">
	   <i class="material-icons prefix">payment</i> 
	    <input type="text" class="black-text" required="" aria-required="true" name="or_no" id="or_no" value="<?php echo $header['or_number']; ?>" <?php echo $disabled; ?>/>
	    <label for="project_title" class="grey-text-bold active">OR No.</label>
      </div>
    
	  <div class="input-field col s6">
	  	<i class="material-icons prefix">library_books</i> 
	    <input type="text" class="black-text" required="" aria-required="true" name="or_amount" id="or_amount" value="<?php echo decimal_format($header['or_amount']); ?>" <?php echo $disabled; ?>/>
	    <label for="project_location" class="grey-text-bold active" >OR Amount</label>
      </div>
  	</div>
<?php endif; ?>

<?php if($w_ref_no === FALSE): ?>
	<div style="margin:10px;" class=" right-align">
       		<a class="btn grey" id="add_nature_of_collection" ><i class="material-icons left">add</i>Add another collection</a>
   	</div>
<?php endif; ?>

	<div class="row row-special p-l-sm p-r-sm">
		<table class="table-default m-t-xxxs grey darken-3">
            <thead>
				<tr>
                    <th width="30%">Nature of Collection</th>
                    <th width="25%">Amount</th>
                    <th width="15%" class="center-align">Action</th>
                </tr>
			</thead>
         </table>
         
		<div class="scroll-pane">
		<table class="table-default">
             <tbody id="tbl_tbody_nature_of_collection">
				<tr id="tbl_row_nature_of_collection" class="hide">
					<td width="30%">
						<select name="collection" id="collection"   placeholder="Select Collection"   aria-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
					    	<option value="">Please Select</option>
					    	<?php foreach($collections as $key => $val):  ?>
					    		<option value="<?php echo $val['coll_nature_code']; ?>"><?php echo $val['collection_nature']; ?></option>
					    	<?php endforeach; ?>
					  	</select>
					</td>
					<td width="25%" class="p-t-lg">
						<input type="text" name=""amount"" id="amount" class=""/>
					</td>
					<td width="15%" class="table-actions p-t-lg">
                        <a id="delete_row" class="delete tooltipped" data-position="bottom" data-delay="50" data-tooltip="Save" style="cursor:pointer"></a>
                    </td>
				</tr>
<?php if(EMPTY($details)): ?>			
				<tr>
					<td width="30%">
						<select name="collection[]" id="collection" class="selectize"  placeholder="Select Collection" data-parsley-required="true" required="" aria-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
					    	<option value="">Please Select</option>
					    	<?php foreach($collections as $key => $val):  ?>
					    		<option value="<?php echo $val['coll_nature_code']; ?>"><?php echo $val['collection_nature']; ?></option>
					    	<?php endforeach; ?>
					  	</select>
					</td>
					<td width="25%" class="p-t-lg">
						<input type="text" data-parsley-required="true"  name="amount[]" id="amount" class=""/>
					</td>
					<td width="15%" class="table-actions p-t-lg">
                       &nbsp;
                    </td>
				</tr>
<?php else: ?>		
	<?php foreach($details as $key => $value): ?>
				<tr>
					<td width="30%">
						<select <?php echo $disabled; ?> name="collection[]" id="collection" class="selectize"  placeholder="Select Collection" data-parsley-required="true" required="" aria-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
					    	<option value="">Please Select</option>
					    	<?php foreach($collections as $key => $val):  ?>
					    	<?php $selected = ($val['coll_nature_code'] == $value['coll_nature_code']) ? 'selected' : '';  ?>
					    		<option value="<?php echo $val['coll_nature_code']; ?>" <?php echo $selected; ?>><?php echo $val['collection_nature']; ?></option>
					    	<?php endforeach; ?>
					  	</select>
					</td>
					<td width="25%" class="p-t-lg">
						<input <?php echo $disabled; ?> type="text" data-parsley-required="true"  name="amount[]" id="amount" class="" value="<?php echo $value['amount']; ?>"/>
					</td>

					<td width="15%" class="table-actions p-t-lg">
<?php if($w_ref_no === FALSE): ?>
                        <a id="delete_existing_row" class="delete tooltipped" data-position="bottom" data-delay="50" data-tooltip="Save" style="cursor:pointer"></a>
<?php endif; ?>
                    </td>

				</tr>
	<?php endforeach; ?>
<?php endif; ?>
			</tbody>
		</table>
		</div>

	</div><!-- end of row -->
        
    
	<div class="md-footer default">
<?php if($w_ref_no === FALSE): ?>
	  <button class="btn waves-effect waves-light"  id="save_op" name="action" type="button" value="Save">Save</button>
<?php endif; ?>	  
	  <a class="waves-effect waves-teal btn-flat cancel_modal grey lighten-3">Cancel</a>
	</div>
	
</form>  
