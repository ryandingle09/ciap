<form id="construction_equipment_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
	<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
	<div class="form-float-label">

    <div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate"  required="" aria-required="true" name="equipment_type" id="equipment_type" value="<?php echo ISSET($equipment_type) ? $equipment_type : "a" ?>"/>
		    <label class="active" for="equipment_type">Type of Equipment</label>
	      </div>
	    </div>
	
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="serial_no" id="serial_no" value="<?php echo ISSET($serial_no) ? $serial_no : "" ?>"/>
		    <label class="active" for="serial_no">Serial Number</label>
	      </div>
	    </div>
    </div>

	<div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="capacity" id="capacity" value="<?php echo ISSET($capacity) ? $capacity : "" ?>"/>
		    <label class="active" for="capacity">Size/Capacity</label>
	      </div>
	    </div>
	
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="brand" id="brand" value="<?php echo ISSET($brand) ? $brand : "" ?>"/>
		    <label class="active" for="brand">Brand</label>
	      </div>
	    </div>
    </div>

	<div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="model" id="model" value="<?php echo ISSET($model) ? $model : "" ?>"/>
		    <label class="active" for="model">Model</label>
	      </div>
	    </div>
	
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate datepicker" required="" aria-required="true" name="acquired_date" id="acquired_date" value="<?php echo ISSET($acquired_date) ? $acquired_date : "" ?>"/>
		    <label class="active" for="acquired_date">Date Acquired</label>
	      </div>
	    </div>
    </div>

	<div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="acquisition_cost" id="acquisition_cost" value="<?php echo ISSET($acquisition_cost) ? number_format($acquisition_cost,2) : "" ?>"/>
		    <label class="active" for="acquisition_cost">Acquisition Cost</label>
	      </div>
	    </div>

	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="location" id="location" value="<?php echo ISSET($location) ? $location : "" ?>"/>
		    <label class="active" for="location">Equipment Present Location</label>
	      </div>
	    </div>
    </div>

  	<div class="row m-n">
	    <div class="col s12">
		 	<div class="input-field">
				<textarea disabled id="present_condition" class="materialize-textarea" name="present_condition" style="height: 15px;"><?php echo ISSET($present_condition) ? $present_condition : "" ; ?></textarea>
				<label class="active" for="present_condition">Present Condition</label>
			</div>
	    </div>
	</div>

    <div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="active_flag" id="active_flag" value="<?php 	if($active_flag==1) 
		    			{
		    			echo "Active";
		    		}
		    		else{
		    			echo "Inactive";
	    			} 
	    	?>"/>
		    <label class="active" for="active_flag">Status</label>
	      </div>
	    </div>
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" disabled class="validate" required="" aria-required="true" name="for_registration_flag" id="for_registration_flag" value="<?php 	if($for_registration_flag==1) 
		    			{
		    			echo "Yes";
		    		}
		    		else{
		    			echo "No";
	    			} 
	    	?>"/>
		    <label class="active" for="for_registration_flag">For Registration</label>
	      </div>
	    </div>
    </div>
    <div class="row m-n">
    </div>
	<div class="md-footer default">
	  <a class="waves-effect waves-teal btn-flat cancel_modal">Cancel</a>
	</div>
</form>