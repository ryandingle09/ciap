<form id="position_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
	<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
	<div class="form-float-label">
	  <div class="row m-n">
	    <div class="col s12">
		  <div class="input-field">
		    <input type="text" class="validate" required="" aria-required="true" name="position_name" id="position_name" value="<?php echo ISSET($position_name) ? $position_name : "" ?>"/>
		    <label for="position_name">Name</label>
	      </div>
	    </div>
	  </div>
	</div>
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	  	<?php //if($this->permission->check_permission(MODULE_ROLE, ACTION_SAVE)):?>
	    	<button class="btn waves-effect waves-light" id="save_position" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<?php //endif; ?>
	</div>
</form>
<script>
$(function(){
  //help_text("role_form");
  
  $('#position_form').parsley();
  $('#position_form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {
	  var data = $(this).serialize();
	  
	  button_loader('save_position', 1);
	  $.post("<?php echo base_url() . PROJECT_CEIS ?>/ceis_positions/process/", data, function(result) {
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  button_loader('save_position', 0);
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  button_loader("save_position",0);
		  modalObj.closeModal();
		  
		  load_datatable('position_table', '<?php echo PROJECT_CEIS ?>/ceis_positions/get_position_list/');
		}
	  }, 'json');       
    }
  });
  
  <?php if(ISSET($position_name)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>
})
</script>