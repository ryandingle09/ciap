<form id="manpower_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
	<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
	<div class="form-float-label">

    <div class="row m-n">
	    <div class="col s12">
		  	<label>Category</label><br><br>
		      	<input name="group1" type="radio" id="test1" />
		      	<label for="test1">Direct</label>
		      	<input name="group1" type="radio" id="test2" />
		      	<label for="test2">Indirect</label>
	    </div>

  	</div>

  	<div class="row m-n">
	    <div class="col s12">
		 	<div class="input-field">
				<textarea id="subject" class="materialize-textarea" name="subject" style="height: 15px;"></textarea>
				<label for="subject">Description of Skills</label>
			</div>
	    </div>
	</div>

    <div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" readonly class="validate" required="" aria-required="true" name="registrant_name" id="registrant_name" value="<?php echo ISSET($registrant_name) ? $registrant_name : "" ?>"/>
		    <label for="registrant_name">Salary Rate (USD)</label>
	      </div>
	    </div>
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" readonly class="validate" required="" aria-required="true" name="registrant_address" id="registrant_address" value="<?php echo ISSET($registrant_address) ? $registrant_address : "" ?>"/>
		    <label for="registrant_address">Number Required</label>
	      </div>
	    </div>
	</div>

	<div class="row m-n">
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" readonly class="validate" required="" aria-required="true" name="registrant_name" id="registrant_name" value="<?php echo ISSET($registrant_name) ? $registrant_name : "" ?>"/>
		    <label for="registrant_name">Employment Contract</label>
	      </div>
	    </div>
	    <div class="col s6">
		  <div class="input-field">
		    <input type="text" readonly class="validate" required="" aria-required="true" name="registrant_address" id="registrant_address" value="<?php echo ISSET($registrant_address) ? $registrant_address : "" ?>"/>
		    <label for="registrant_address">Monthly Breakdown of Employment</label>
	      </div>
	    </div>
	</div>

	<div class="md-footer default">
	  <a class="waves-effect waves-teal btn-flat" id="cancel_construction_equipment">Cancel</a>
	</div>
</form>