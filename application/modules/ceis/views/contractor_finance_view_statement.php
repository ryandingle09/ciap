<ul class="collapsible panel" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active"> 
    </div>
    <div class="collapsible-body row" style="display:block !important">
      <table id="table_finance" class="striped">
        <thead>
          <th></th>
          <th>December 31, 2014</th>
          <th>December 31, 2015</th>
        </thead>
        <tbody>
          <tr>
            <td>TOTAL CURRENT ASSETS</td>
            <td><?php echo ISSET($total_current_assets) ? $total_current_assets : "" ; ?></td>
            <td></td>
          </tr>
          <tr>
            <td>TOTAL NON CURRENT ASSETS</td>
            <td><?php echo ISSET($total_non_current_assets) ? $total_non_current_assets : "" ; ?></td>
            <td></td>
          </tr>
          <tr>
            <td>TOTAL CURRENT LIABILITIES</td>
            <td><?php echo ISSET($total_current_liabilities) ? $total_current_liabilities : "" ; ?></td>
            <td></td>
          </tr>
          <tr>
            <td>LONG TERM LIABILITIES</td>
            <td><?php echo ISSET($long_term_liabilities) ? $long_term_liabilities : "" ; ?></td>
            <td></td>
          </tr>
          <tr>
            <td>NET WORTH</td>
            <td><?php echo ISSET($networth) ? $networth : "" ; ?></td>
            <td></td>
          </tr>
          <tr>
            <td>GROSS SALES/REVENUES</td>
            <td><?php echo ISSET($gross_sales) ? $gross_sales : "" ; ?></td>
            <td></td>
          </tr>
          <tr>
            <td>NET PROFIT AFTER TAX</td>
            <td><?php echo ISSET($net_profit) ? $net_profit : "" ; ?></td>
            <td></td>
          </tr>
          <tr>
            <td>NO. OF COMMON SHARES OUTSTANDING</td>
            <td><?php echo ISSET($common_shares) ? $common_shares : "" ; ?></td>
            <td></td>
          </tr>
          <tr>
            <td>PAR VALUE PER COMMON SHARE</td>
            <td><?php echo ISSET($par_value) ? $par_value : "" ; ?></td>
            <td></td>
          </tr>
        </tbody>
      </table>
    </div>
  </li>
</ul>