<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Positions</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Positions
		<span>Manage Positions</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p-l-md p-r-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_position" id="add_position" name="add_position" onclick="modal_init()">Position</button>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="position_table">
  <thead>
	<tr>
	  <th width="20%">Name of Position</th>
	  <th width="20%">Created By</th>
	  <th width="20%">Created Date</th>
	  <th width="15%">Last Modified By</th>
	  <th width="15%">Last Modified Date</th>
	  <th width="10%" class="text-center">Actions</th>
	</tr>
  </thead>
  </table>
</div>

<script>
var deleteObj = new handleData({ controller : 'ceis_positions', method : 'delete_position', module: '<?php echo PROJECT_CEIS ?>' });
</script>