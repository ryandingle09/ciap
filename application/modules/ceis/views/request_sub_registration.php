<?php
$registration_no 	= (ISSET($registration_details['registration_no'])) ? $registration_details['registration_no'] : '';
$registration_date  = (ISSET($registration_details['registration_date']) && ! EMPTY($registration_details['registration_date']) && $registration_details['registration_date'] != DEFAULT_DATE) ? date('m/d/Y', strtotime($registration_details['registration_date'])) : '';
//$registration_date  = (ISSET($registration_details['registration_date']) && ! EMPTY($registration_details['registration_date']) && $registration_details['registration_date'] != DEFAULT_DATE) ? date('m/d/Y', strtotime($registration_details['registration_date'])) : date('m/d/Y');

//$expiry_date	    = ( ISSET($registration_details['expiry_date']) && ! EMPTY($registration_details['expiry_date']) && $registration_details['expiry_date'] != DEFAULT_DATE) ? date('m/d/Y', strtotime($registration_details['expiry_date'])) : date('m/d/Y', strtotime('+ 2 years'));
$expiry_date	    = ( ISSET($registration_details['expiry_date']) && ! EMPTY($registration_details['expiry_date']) && $registration_details['expiry_date'] != DEFAULT_DATE) ? date('m/d/Y', strtotime($registration_details['expiry_date'])) : '';
$last_renewal	    = ( ISSET($registration_details['last_renewal_date']) && ! EMPTY($registration_details['last_renewal_date']) && $registration_details['last_renewal_date'] != DEFAULT_DATE) ? date('m/d/Y', strtotime($registration_details['last_renewal_date'])) : '';


$lr_readonly		= ($request_details['request_type_id'] ==  REQUEST_TYPE_REN) ? '' : 'readonly';
$lr_class		    = ($request_details['request_type_id'] ==  REQUEST_TYPE_REN) ? 'datepicker dnb' : '';
$reg_date_class		= ( ! ISSET($curr_reg)) ? 'datepicker dnb' : '';
$reg_readonly       = (ISSET($curr_reg)) ? 'readonly' : '';

if(ISSET($curr_reg)):
	$registration_no   = $curr_reg['registration_no'];
	$registration_date = date('m/d/Y', strtotime($curr_reg['registration_date']));
endif;

?>
<ul class="collapsible panel" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active">Registration
    </div>

    <div class="collapsible-body" style="display: block">
      <form id="registration_form" data-validate="parsley">
      
        <section class="panel panel-default">
          <header class="panel-heading"></header>
          
            <div class="row">
              <div class="input-field col s6 ">
              	<i class="material-icons prefix">payment</i>
                <input id="registration_no" name="registration_no" type="text" class="validate" value="<?php echo $registration_no; ?>" data-parsley-required="true" <?php echo $reg_readonly; ?>>
                <label for="registration_no">Registration No.</label>
              </div>
              <div class="input-field col s6 ">
                <i class="material-icons prefix">date_range</i>
                <input id="registration_date" name="registration_date" class="<?php echo $reg_date_class; ?>" value="<?php echo $registration_date; ?>" type="text" data-parsley-required="true" class="validate" <?php echo $reg_readonly; ?>>
                <label for="registration_date">Registration Date</label>
              </div>
            </div>
<?php //if($request_details['request_type_id'] ==  REQUEST_TYPE_REN): ?>
            <div class="row">
              <div class="input-field col s6 ">
              	<i class="material-icons prefix">date_range</i>
                <input id="expiry_date" name="expiry_date" type="text" class="datepicker dnb" value="<?php echo $expiry_date; ?>" data-parsley-required="true">
                <label for="expiry_date">Expiry Date</label>
              </div>
              <div class="input-field col s6 ">
                <i class="material-icons prefix">date_range</i>
                <input id="last_renewal_date" name="last_renewal_date" class="<?php echo $lr_class; ?>" value="<?php echo $last_renewal;  ?>" type="text"  class="validate" <?php echo $lr_readonly; ?>>
                <label for="last_renewal_date">Last Renewal Date</label>
              </div>
            </div>
<?php //endif; ?>
            <div style="margin:10px;" class="panel-footer right-align">
<?php if($show_save_btn): ?>
              <button class="btn waves-effect waves-light" id="save_registration" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
<?php endif; ?>
            </div>
        </section>
        
      </form>
    </div>
    
  </li>
</ul>

<script>
$(function(){

})
</script>
  