<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Project Types</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Project Types
		<span>Manage Project Types</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	  </div>
	  
	  <div class="input-field inline p-l-md p-r-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_project_type" id="add_project_type" name="add_project_type" onclick="modal_init()">Project Type</button>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="project_type_table">
  <thead>
	<tr>
	  	<th width="25%">Name of Project Types</th>
	    <th width="15%">Created By</th>
	    <th width="15%">Created Date</th>
	    <th width="15%">Last Modified By</th>
	    <th width="15%">Last Modified Date</th>
	    <th width="15%">Actions</th>
	</tr>
  </thead>
  </table>
</div>
<script>
var deleteObj = new handleData({ controller : 'ceis_project_types', method : 'delete_project_type', module: '<?php echo PROJECT_CEIS ?>' });
</script>