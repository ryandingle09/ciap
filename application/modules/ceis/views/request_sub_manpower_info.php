<div class="row">
  <div class="col s12">
    <ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active">Project Info 
        </div>
        <div class="collapsible-body row" style="display: block">
          <div class="col s6">
            <div>
              <label>Project Title</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Project Title" data-required="true" value="<?php echo ISSET($project_title) ? $project_title : "CIAP - CEIS Just a Sample Project" ; ?>">
            </div>
          </div>
          
          <div class="col s6">
            <div>
              <label>Project Location</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Project Location" data-required="true" value="<?php echo ISSET($project_location) ? $project_location : "Makati City Philippines" ; ?>">
            </div>
          </div>

          <div class="col s6">
            <div>
              <label>Province</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Province" data-required="true" value="<?php echo ISSET($province) ? $province : "Laguna City" ; ?>">
            </div>
          </div>

          <div class="col s6">
            <div>
              <label>Country</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Country" data-required="true" value="<?php echo ISSET($country) ? $country : "Philippines" ; ?>">
            </div>
          </div>

          <div class="col s12">
            <div>
              <label>General Description of the Project</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="General Description of the Project" data-required="true" value="<?php echo ISSET($project_description) ? $project_description : "A General Description of Project from Specific Company" ; ?>">
            </div>
          </div>

          <div class="col s12">
            <div>
              <label>General Scope of Work</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="General Scope of Work" data-required="true" value="<?php echo ISSET($work_scope) ? $work_scope : "Finding Leader of Black Society" ; ?>">
            </div>
          </div>
          
          <div class="col s6">
            <div>
              <label>Currency</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Estimated Contract Price (USD)" data-required="true" value="<?php echo ISSET($contract_price) ? $contract_price : "400,321" ; ?>">
            </div>
          </div>
    
          <div class="col s6">
            <div>
              <label>Exchange Rate to USD</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Contract Duration" data-required="true" value="<?php echo ISSET($contract_duration) ? $contract_duration : "27 Months" ; ?>">
            </div>
          </div>

          <div class="col s6">
            <div>
              <label>Estimated Contract Price</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Estimated Minimum Manpower Required" data-required="true" value="<?php echo ISSET($minimum_manpower) ? $minimum_manpower : "10" ; ?>">
            </div>
          </div>

          <div class="col s6">
            <div>
              <label>Contact Duration</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Estimated Outward Amount of FX Remittances" data-required="true" value="<?php echo ISSET($outward_remmittance) ? $outward_remmittance : "number_format(492332,'2')" ; ?>">
            </div>
          </div>

          <div class="col s6">
            <div>
              <label>Estimated Manpower Required</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Estimated Outward Amount of FX Remittances" data-required="true" value="<?php echo ISSET($outward_remmittance) ? $outward_remmittance : "number_format(492332,'2')" ; ?>">
            </div>
          </div>

          <div class="col s6">
            <div>
              <label>&nbsp;</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Estimated Inward Amount of FX Remittances" data-required="true" value="<?php echo ISSET($inward_remmittance) ? $inward_remmittance : "number_format(352542545,'2');" ; ?>">
            </div>
          </div>

          <div class="col s6">
            <div>
              <label>Estimated Amount of FX Remittances</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Estimated Inward Amount of FX Remittances" data-required="true" value="<?php echo ISSET($inward_remmittance) ? $inward_remmittance : "number_format(352542545,'2');" ; ?>">
            </div>
          </div>

          <div class="col s6">
            <div>
              <label>&nbsp;</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Estimated Inward Amount of FX Remittances" data-required="true" value="<?php echo ISSET($inward_remmittance) ? $inward_remmittance : "number_format(352542545,'2');" ; ?>">
            </div>
          </div>

          <div class="col s6">
            <div>
              <label>Construction Procurement</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Estimated Inward Amount of FX Remittances" data-required="true" value="<?php echo ISSET($inward_remmittance) ? $inward_remmittance : "Bidding" ; ?>">
            </div>
          </div>

          <div class="col s6">
            <div>
              <label>Ivolvement</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Estimated Inward Amount of FX Remittances" data-required="true" value="<?php echo ISSET($inward_remmittance) ? $inward_remmittance : "Prime Contractor" ; ?>">
            </div>
          </div>

          <div class="col s9">
            <div>
              <label>Prime Contractor</label>
              <input disabled="" type="text" readonly="" placeholder="Date" data-required="true" value="<?php echo ISSET($date) ? $date : "2016/12/12" ; ?>">
            </div>
          </div>

          <div class="col s3">
            <div>
              <label>Nationality</label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Place" data-required="true" value="<?php echo ISSET($place) ? $place : "Kochin cha, China" ; ?>">
            </div>
          </div>
          <div class="col s12 m-b-sm">
            <div>
              <label><b><i>Others</i></b></label>
            </div>
          </div>
          <div class="col s12">
            <div>
              <label></label>
              <input disabled="" type="text" class="form-control" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($contract_type) ? $contract_type : "Please Specify" ; ?>">
            </div>
          </div>
          <div class="col s12 m-b-sm">
            <div>
              <label><b><i>BIDDING/NEGOTIATION</i></b></label>
            </div>
          </div>
          <div class="col s3">
            <div>
              <label>Date</label>
              <input disabled="" type="text" class="datepicker" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($contract_type) ? $contract_type : "Please Specify" ; ?>">
            </div>
          </div>
          <div class="col s9">
            <div>
              <label>Place</label>
              <input disabled="" type="text" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($contract_type) ? $contract_type : "Please Specify" ; ?>">
            </div>
          </div>
          <div class="col s12">
            <div>
              <label>Type of Contract</label>
              <input disabled="" type="text" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($contract_type) ? $contract_type : "Please Specify" ; ?>">
            </div>
          </div>

          <div class="col s12">
            <div>
              <label>Other Filipino Companies Bidding/Negotiation for the Project</label>
              <input disabled="" type="text" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($contract_type) ? $contract_type : "Please Specify" ; ?>">
            </div>
          </div>
          <div class="col s12 m-b-sm">
            <div>
              <label><b><i>FINANCIAL ASSISTANCE/GUARANTEE TO BE SECURE FROM</i></b></label>
            </div>
          </div>
          <div class="col s6">
            <div>
              <label>Prime Contractor</label>
              <input disabled="" type="text" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($contract_type) ? $contract_type : "Please Specify" ; ?>">
            </div>
          </div>
          <div class="col s6">
            <div>
              <label>Others</label>
              <input disabled="" type="text" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($contract_type) ? $contract_type : "Please Specify" ; ?>">
            </div>
          </div>
          <div class="col s12 m-b-sm">
            <div>
              <label><b><i>FINANCIAL TERMS AND CONDITIONS</i></b></label>
            </div>
          </div>
          <div class="col s12">
            <div>
              <label>Project to be Financed By</label>
              <input disabled="" type="text" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($contract_type) ? $contract_type : "Please Specify" ; ?>">
            </div>
          </div>
          <div class="col s6">
            <div>
              <label>Bid Bond</label>
              <input disabled="" type="text" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($contract_type) ? $contract_type : "Please Specify" ; ?>">
            </div>
          </div>
          <div class="col s6">
            <div>
              <label>Performance Bond</label>
              <input disabled="" type="text" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($contract_type) ? $contract_type : "Please Specify" ; ?>">
            </div>
          </div>
          <div class="col s6">
            <div>
              <label>Advanced Payment Guarantee</label>
              <input disabled="" type="text" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($contract_type) ? $contract_type : "Please Specify" ; ?>">
            </div>
          </div>
          <div class="col s6">
            <div>
              <label>Retentions</label>
              <input disabled="" type="text" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($contract_type) ? $contract_type : "Please Specify" ; ?>">
            </div>
          </div>
          <div class="col s6">
            <div>
              <label>Progress Billings</label>
              <input disabled="" type="text" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($contract_type) ? $contract_type : "Please Specify" ; ?>">
            </div>
          </div>
          <div class="col s6">
            <div>
              <label>Others</label>
              <input disabled="" type="text" readonly="" placeholder="Type of Contract" data-required="true" value="<?php echo ISSET($contract_type) ? $contract_type : "Please Specify" ; ?>">
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
</div>
