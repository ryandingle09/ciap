<ul class="collapsible panel" data-collapsible="expandable">
  <li>
      <div class="collapsible-header active">Checklist 
      </div>

       <div class="row">
          <div class="col s12">
            <div class="card blue-grey grey darken-1 m-md">
              <div class="card-content white-text">
                <span class="font-lg font-bold"> LEGEND </span>
                <p class="m-t-sm"> <i style="color:#FFCC00; vertical-align:text-top;" class="flaticon-warning35 flat-icon-rml"></i> 
                Submission upon releasing of registration</p>
              </div>
            </div>
          </div>
        </div>

        <div class="collapsible-body" style="display: block">
            <table class="striped">
              <thead>    
               	<th width="2%" ></th>            
                <th width="58%" >Checklist</th>
                <th width="30%">Attachment</th>
                <th width="10%">Action</th>
              </thead>

              <tbody>
                <?php                	
                	$ctr = 1;

                 	foreach($checklist as $chk):
                 		$chk_key   = array_search($chk['checklist_id'], array_column($req_checklist, 'checklist_id'));
                 	
                 		$id 	   = $req_checklist[$chk_key]['request_id'].'/'.$chk['checklist_id'];
                 		$salt	   = gen_salt();
                 		$token     = in_salt($id, $salt);
                 		$modal_url = base64_url_encode($id).'/'.$salt.'/'.$token;
                 		
                 		$checked   = ($chk_key !== FALSE && $req_checklist[$chk_key]['complied_flag'] == COMPLIED_YES_FLAG) ? 'checked="checked"' : '';
                ?>
		                <tr>
		                
		                  <td width="2%">
		                  	<?php if($chk['for_submission_flag']){ ?>
		                     	<i style="color:#FFCC00;" class="flaticon-warning35 flat-icon-rml"></i>
		                    <?php } ?> 
		                     	<input type="checkbox" class="filled-in" id="check<?php echo $ctr; ?>" <?php echo $checked; ?> />
		      				 	<label id="check<?php echo $ctr; ?>"></label>
		      			  </td>
		      			   
		                  <td width="58%"><?php echo $chk['checklist']; ?></td>
		                  
		                  <td width="30%" style="vertical-align:text-top;">
		                  	<?php if($chk_key !== FALSE && ! EMPTY($req_checklist[$chk_key]['file_name']) ){ ?>
		                  		<a href="<?php echo base_url().'ceis/ceis_request_checklist/download_attachment/'.$url.'/'.$req_checklist[$chk_key]['file_name']; ?>" ><?php echo $req_checklist[$chk_key]['display_filename']; ?></a>
		                  	<?php } ?> 
		                  </td>
		                  
		                  <td width="10%" ><div class='table-actions'><a  href='javascript:;' data-tooltip='View Checklist' data-position='bottom' data-delay='50' style="cursor:pointer;" onclick="modal_checklist_init('<?php echo $modal_url; ?>');" data-modal="modal_checklist" class="md-trigger tooltipped"><i class="flaticon-visible9"></i></a></div></td>
		                  
		                </tr>
                <?php 
	                	$ctr++; 
	                 endforeach; 
                ?>
              </tbody>
          </table>
        </div>
        
        
      </li>
    </ul>
  