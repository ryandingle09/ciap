<div class="row">
  <div class="col s12 p-t-sm">
    <ul class="tabs" style="overflow-x: hidden">
      <li class="tab col s3"><a class="active" href="#personal_information">Personal Information</a></li>
      <li class="tab col s3"><a href="#work_experiences">Work Experiences</a></li>
      <li class="tab col s3"><a href="#projects">Projects</a></li>
    </ul>
  </div>
  <div id="personal_information" class="col s12">
    <ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active">
          Basic Information
        </div>
        <div class="collapsible-body striped row" style="display: block">
           <div class="col s4">
            <div>
              <label>First Name</label>
              <input disabled="" type="text" value="<?php echo ISSET($first_name) ? $first_name : "" ; ?>">
            </div>
          </div>
          <div class="col s4">
            <div>
              <label>Middle Name</label>
              <input disabled="" type="text" value="<?php echo ISSET($middle_name) ? $middle_name : "" ; ?>">
            </div>
          </div>
          <div class="col s4">
            <div>
              <label>Last Name</label>
              <input disabled="" type="text" value="<?php echo ISSET($last_name) ? $last_name : "" ; ?>">
            </div>
          </div>
          <div class="col s12">
            <div>
              <label>Address</label>
              <input disabled="" type="text" value="<?php echo ISSET($address) ? $address : "" ; ?>">
            </div>
          </div>
          <div class="col s6">
            <div>
              <label>Birth Date</label>
              <input disabled="" type="text" class="datepicker"  data-date-format="dd-mm-yyyy" value="<?php echo ISSET($birth_date) ? $birth_date : "" ; ?>">
            </div>
          </div>
          <div class="col s6">
            <div>
              <label>Birth Place</label>
              <input disabled="" type="text" value="<?php echo ISSET($birth_place) ? $birth_place : "" ; ?>">
            </div>
          </div>

        </div>
      </li>
      <li>
        <div class="collapsible-header active"> 
          Educational Background
        </div>
        <div class="collapsible-body striped row" style="display: block">
          <div class="col s12">
            <div>
              <label>Highest Educational Attainment</label>
              <input disabled="" type="text" value="<?php echo ISSET($educational_attainment) ? $educational_attainment : "" ; ?>">
            </div>
          </div>
          <div class="col s9">
            <div>
              <label>School/Univesity Attended</label>
              <input disabled="" type="text" value="<?php echo ISSET($school_attended) ? $school_attended : "" ; ?>">
            </div>
          </div>
          <div class="col s3">
            <div>
              <label>Year Graduated</label>
              <input disabled="" type="text" class="datepicker"  data-date-format="dd-mm-yyyy" value="<?php echo ISSET($year_graduated) ? $year_graduated : "" ; ?>">
            </div>
          </div>
        </div>
      </li>
      <li>
        <div class="collapsible-header active"> 
          Professional Licenses
        </div>
        <div class="collapsible-body row" style="display: block">
          <table class="striped">
            <thead>
              <th width="30%">Profession</th>
              <th width="20%">License No.</th>
              <th width="20%">Issued Date</th>
              <th width="20%">Expiry Date</th>
            </thead>
            <tbody>
              <?php foreach ($profession_info as $data): ?>
              <tr>
                <td><?php echo ISSET($data['profession']) ? $data['profession'] : ""; ?></td>
                <td><?php echo ISSET($data['license_no']) ? $data['license_no'] : ""; ?></td>
                <td><?php echo ISSET($data['issued_date']) ? $data['issued_date'] : ""; ?></td>
                <td><?php echo ISSET($data['expiry_date']) ? $data['expiry_date'] : ""; ?></td>
              </tr>
              <?php endforeach; ?>
            </tbody>
          </table>
        </div>
      </li>
    </ul>
  </div>
  <div id="work_experiences" class="col s12">
    <ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active"> 
        </div>
        <div class="collapsible-body striped row" style="display: block">
          <table id="contractor_staffing_experience">
            <thead>
              <th width="15%">Inclusive Dates</th>
              <th width="25%">Employer Name And Address</th>
              <th width="20%">Positions</th>
              <th width="20%">Job Description</th>
              <th width="10%">Status</th>
              <th width="10%">Actions</th>
            </thead>
            <tbody>
              <tr>
                <td>a</td>
                <td>b</td>
                <td>c</td>
                <td>d</td>
                <td>e</td>
                <td>f</td>
              </tr>
            </tbody>
          </table>
        </div>
      </li>
    </ul>
  </div>
  <div id="projects" class="col s12">
    <ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active">
        </div>
        <div class="collapsible-body row" style="display: block !important">
          <table class="striped" id="contractor_staffing_project">
            <thead>
              <th>Project Title</th>
              <th>Project Location</th>
              <th>Owner</th>
              <th>Project Cost</th>
              <th>Actions</th>
            </thead>
            <tbody>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
            </tbody>
          </table>
        </div>
      </li>
    </ul>
  </div>
</div>

<script>
  $(document).ready(function(){
    $('ul.tabs').tabs();
  });
</script>