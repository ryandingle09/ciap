<ul class="collapsible panel" data-collapsible="expandable">
	<li>
        <div class="collapsible-header active">Order of Payment List </div>
        <div class="right-align  m-t-sm m-r-sm">
<?php if(EMPTY($request_details['reference_no'])): ?>
       		<a class="btn md-trigger grey" data-tooltip='Add' data-modal='modal_op' onclick="modal_op_init('<?php echo $url; ?>');"  data-position='bottom' data-delay='50' id="add_order_of_payment" ><i class="material-icons left">add</i>Add Order of Payment</a>
    	</div>
<?php endif; ?>
        <div class="collapsible-body row" style="display: block">
	          <table id="table_order_of_payment" " class="table table-default table-layout-auto dataTable no-footer">
	            <thead>
	              <th>OP Date</th>
	              <th>OP Amount</th>
	              <th>OR No</th>
	              <th>OR Date</th>
	              <th>OR Amount</th>
	              <th>Action</th>
	            </thead>
	          </table>
        </div>
      </li>
</ul>
  
  