<input type="hidden" id="security" value="<?php echo $security ?>"> 

<div class="page-title">
	<ul id="breadcrumbs">
		<li><a href="#">Home</a></li>
		<li><a href="<?php echo base_url() ?>ceis/ceis_requests">Contractors</a></li>
    	<li><a href="#" class="active"><?php echo ISSET($contractor_name) ? $contractor_name : ""; ?></a></li>
  	</ul>

	<div class="row m-b-n">
		<div class="col s6 p-r-n">
      		<h5><?php echo ISSET($contractor_name) ? $contractor_name : ""; ?></h5>
	    </div>
  	</div>
</div>

<div class="row m-t-lg">
  	<div class="col s3 p-r-n">

  		<div class="list-basic collection">	
  		
			<!--CONTRACTOR-->
			<a id="menu_contractor" onclick="LoadTab.init('contractor')"  class="collection-item " href="javascript:;"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Contractor Info</a>
			
			<!--CONTRACTS-->
			<a id="menu_contracts" onclick="LoadTab.init('contracts')" class="collection-item " href="javascript:;"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Contracts</a>
			
			<!--STAFFING-->
  			<a id="menu_staffing" onclick="LoadTab.init('staffing')" class="collection-item " href="javascript:;"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Staffing</a>
			
			<!--FINANCE-->
  			<a id="menu_finance" onclick="LoadTab.init('finance')" class="collection-item "  href="javascript:;"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Finance</a>
			
			<!--WORK VOLUME-->
  			<a id="menu_work" onclick="LoadTab.init('work')" class="collection-item "  href="javascript:;"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work Volume</a>
  			
  			<!--CONSTRUCTION EQUIPMENT-->
  			<a id="menu_equipment" onclick="LoadTab.init('equipment')" class="collection-item "  href="javascript:;"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Construction Equipment</a>
  			
			<a id="menu_op" onclick="LoadTab.init('op')" class="collection-item "  href="javascript:;"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Order of Payment</a>	  	 
		  	
		  	<a id="menu_registration" onclick="LoadTab.init('registration')" class="collection-item "  href="javascript:;"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Registration Info</a>

  		</div>
	</div>
	<div class="tab-content col s9 p-l-sm">
  	</div>
</div>