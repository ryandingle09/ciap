    <ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active"> 
        </div>
        <div class="collapsible-body striped row" style="display: block">
          <table id="request_staffing_project">
            <thead>
                <th width="15%">Project Title</th>
				<th width="25%">Project Location</th>
				<th width="25%">Owner</th>
				<th width="15%">Project Cost</th>
				<th width="5%">Action</th>
            </thead>
            <tbody></tbody>
          </table>
        </div>
      </li>
    </ul>