<ul class="collapsible panel" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active">Finance 
    <?php
      $mydate = strtotime($highest);
      $high = date('Y', $mydate);
    ?>
    <input type="hidden" name="highest" value="<?php echo $high; ?>"/>
    </div>
    <div class="pull-right p-sm" >
      <!-- <a href="javascript:;" class="btn" onclick="LoadTab.init('finance_view_statement', '.$contractor_id.')">View Statement</a> -->
      <a href="javascript:;" class="btn md-trigger" data-position="bottom" data-delay="50" data-modal="modal_statement" onclick="modal_statement_init('<?php echo $url.'/'.$high; ?>')" >View Statement</a>
    </div>
    <div class="collapsible-body row" style="display:block !important">
      <table id="table_finance">
        <thead>
          <th>Statement Date</th>
          <th>Assets</th>
          <th>Liabilities</th>
          <th>Net Worth</th>
          <th>Paid Up Capital</th>
          <th>Actions</th>
        </thead>
        <tbody>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
        </tbody>
      </table>
    </div>
  </li>
</ul>