<?php 
$contractor_name  = ( ISSET($contractor_details['contractor_name']) && ! EMPTY($contractor_details['contractor_name'])) ? $contractor_details['contractor_name'] : '';
$contractor_addr  = ( ISSET($contractor_details['contractor_address']) && ! EMPTY($contractor_details['contractor_address'])) ? $contractor_details['contractor_address'] : '';
$contractor_cat   = ( ISSET($contractor_details['contractor_category_name']) && ! EMPTY($contractor_details['contractor_category_name'])) ? $contractor_details['contractor_category_name'] : '';

$license_category = ( ISSET($contractor_details['license_category_name']) && ! EMPTY($contractor_details['license_category_name'])) ? $contractor_details['license_category_name'] : '';
$license_no       = ( ISSET($contractor_details['license_no']) && ! EMPTY($contractor_details['license_no'])) ? $contractor_details['license_no'] : '';
$license_date     = ( ISSET($contractor_details['license_date']) && ! EMPTY($contractor_details['license_date']) && $contractor_details['license_date'] != DEFAULT_DATE ) ? date('M d,Y', strtotime($contractor_details['license_date'])) : '';
$renewal_date     = ( ISSET($contractor_details['last_renewal_date']) && ! EMPTY($contractor_details['last_renewal_date']) && $contractor_details['last_renewal_date'] != DEFAULT_DATE ) ? date('M d,Y', strtotime($contractor_details['last_renewal_date'])) : '';

$sec_no       	  = ( ISSET($contractor_details['sec_reg_no']) && ! EMPTY($contractor_details['sec_reg_no'])) ? $contractor_details['sec_reg_no'] : '';
$sec_date         = ( ISSET($contractor_details['sec_reg_date']) && ! EMPTY($contractor_details['sec_reg_date'])) ? date('M d,Y', strtotime($contractor_details['sec_reg_date'])) : '';

$bir_date         = ( ISSET($contractor_details['bir_clearance_date']) && ! EMPTY($contractor_details['bir_clearance_date'])) ? date('M d,Y', strtotime($contractor_details['bir_clearance_date'])) : '';
$custom_date      = ( ISSET($contractor_details['customs_clearance_date']) && ! EMPTY($contractor_details['customs_clearance_date'])) ? date('M d,Y', strtotime($contractor_details['customs_clearance_date'])) : '';
$court_date       = ( ISSET($contractor_details['court_clearance_date']) && ! EMPTY($contractor_details['court_clearance_date'])) ? date('M d,Y', strtotime($contractor_details['court_clearance_date'])) : '';

$tel_no       	  = ( ISSET($contractor_details['tel_no']) && ! EMPTY($contractor_details['tel_no'])) ? $contractor_details['tel_no'] : '';
$fax_no       	  = ( ISSET($contractor_details['fax_no']) && ! EMPTY($contractor_details['fax_no'])) ? $contractor_details['fax_no'] : '';
$email       	  = ( ISSET($contractor_details['email']) && ! EMPTY($contractor_details['email'])) ? $contractor_details['email'] : '';
$website       	  = ( ISSET($contractor_details['website']) && ! EMPTY($contractor_details['website'])) ? $contractor_details['website'] : '';

$rep_fname        = ( ISSET($contractor_details['rep_first_name']) && ! EMPTY($contractor_details['rep_first_name'])) ? $contractor_details['rep_first_name'] : '';
$rep_mname        = ( ISSET($contractor_details['rep_mid_name']) && ! EMPTY($contractor_details['rep_mid_name'])) ? $contractor_details['rep_mid_name'] : '';
$rep_lname        = ( ISSET($contractor_details['rep_last_name']) && ! EMPTY($contractor_details['rep_last_name'])) ? $contractor_details['rep_last_name'] : '';
$rep_addr         = ( ISSET($contractor_details['rep_address']) && ! EMPTY($contractor_details['rep_address'])) ? $contractor_details['rep_address'] : '';
$rep_contact      = ( ISSET($contractor_details['rep_contact_no']) && ! EMPTY($contractor_details['rep_contact_no'])) ? $contractor_details['rep_contact_no'] : '';
$rep_email        = ( ISSET($contractor_details['rep_email']) && ! EMPTY($contractor_details['rep_email'])) ? $contractor_details['rep_email'] : '';
?>
<ul class="collapsible panel" data-collapsible="expandable">
  <li>
    <div class="collapsible-header">PCAB License</div>
    <div class="collapsible-body" style="display: block !important;">

    	<div class="row">
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">library_books</i> 	
	    		  <input id="license_cat" class="black-text m-t-xs" type="text"  value="<?php echo $license_category; ?>" disabled>
	    		  <label for="license_cat" class="grey-text-bold active">License Category</label>
	    	</div>
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">payment</i>	
	    		  <input class="black-text m-t-xs" type="text"   value="<?php echo $license_no; ?>" disabled>
	    		  <label class="grey-text-bold active">License No</label>
	    	</div>
    	</div>
    	
    	
    	<div class="row">
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">date_range</i> 	
	    		  <input class="black-text m-t-xs" type="text"  value="<?php echo $license_date; ?>" disabled>
	    		  <label class="grey-text-bold active">Issued Date</label>
	    	</div>
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">date_range</i> 	
	    		  <input class="black-text m-t-xs" type="text"  value="<?php echo $renewal_date; ?>" disabled>
	    		  <label class="grey-text-bold active">Last Renewal Date</label>
	    	</div>
    	</div>

        <div class="row">
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">list</i> 
	    		  <label class="grey-text-bold active">Classification</label>
	    		  <div 	 class="m-t-md" style="margin-left:3rem;min-height:3rem;">
	    		  <?php foreach($classification as $key => $val): ?>
	    		  			<div class="chip m-b-sm"><?php echo $val['license_classification_name']; ?></div>
	    		  <?php endforeach; ?>	
	    		  </div>
	    	</div>
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">list</i> 	
	    		  <label class="grey-text-bold active">Specialization</label>
	    		  <div 	 class="m-t-md" style="margin-left:3rem;min-height:3rem;">	
	    		 	<?php foreach($specialization as $key => $val): ?>
	    		  			<div class="chip m-b-sm"><?php echo $val['license_classification_name']; ?></div>
	    		  	<?php endforeach; ?>
	    		  </div>
	    	</div>
    	</div>
    
    </div>
    
    <div class="collapsible-header">Contractor Details</div>
    <div class="collapsible-body" style="display: block !important;">
    	<div class="row">
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">account_circle</i> 	
	    		  <input class="black-text m-t-xs" type="text"  value="<?php echo $contractor_name; ?>" disabled>
	    		  <label class="grey-text-bold active">Contractor Name</label>
	    	</div>
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">location_on</i>	
	    		  <input class="black-text m-t-xs" type="text"  value="<?php echo $contractor_addr; ?>" disabled>
	    		  <label class="grey-text-bold active">Contractor Address</label>
	    	</div>
    	</div>
    	
    	<div class="row">
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">library_books</i> 	
	    		  <input class="black-text m-t-xs" type="text" value="<?php echo $contractor_cat; ?>" disabled>
	    		  <label class="grey-text-bold active">Category</label>
	    	</div>
    	</div>
    </div>
    
    
    <div class="collapsible-header">SEC Registration</div>
    <div class="collapsible-body" style="display: block !important;">
    	<div class="row">
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">payment</i> 	
	    		  <input class="black-text m-t-xs" type="text" value="<?php echo $sec_no; ?>" disabled>
	    		  <label class="grey-text-bold active">No</label>
	    	</div>
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">date_range</i>	
	    		  <input class="black-text m-t-xs" type="text" value="<?php echo $sec_date; ?>" disabled>
	    		  <label class="grey-text-bold active">Issued Date</label>
	    	</div>
    	</div>
    </div>
    
    <div class="collapsible-header">Clearances</div>
    <div class="collapsible-body" style="display: block !important;">
    	<div class="row">
	    	<div class="input-field col s4">
	    		  <i class="material-icons prefix">date_range</i>	
	    		  <input class="black-text m-t-xs" type="text" value="<?php echo $bir_date; ?>" disabled>
	    		  <label class="grey-text-bold active">BIR</label>
	    	</div>
	    	<div class="input-field col s4">
	    		  <i class="material-icons prefix">date_range</i>	
	    		  <input class="black-text m-t-xs" type="text" value="<?php echo $custom_date; ?>" disabled>
	    		  <label class="grey-text-bold active">Customs</label>
	    	</div>
	    	<div class="input-field col s4">
	    		  <i class="material-icons prefix">date_range</i>	
	    		  <input class="black-text m-t-xs" type="text" value="<?php echo $court_date; ?>" disabled>
	    		  <label class="grey-text-bold active">Court</label>
	    	</div>
    	</div>
    </div>
    
   	<div class="collapsible-header">Contact Details</div>
    <div class="collapsible-body" style="display: block !important;">
    	<div class="row">
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">contact_phone</i>	
	    		  <input class="black-text m-t-xs" type="text" value="<?php echo $tel_no; ?>" disabled>
	    		  <label class="grey-text-bold active">Telephone</label>
	    	</div>
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">contact_phone</i>	
	    		  <input class="black-text m-t-xs" type="text" value="<?php echo $fax_no; ?>" disabled>
	    		  <label class="grey-text-bold active">Fax</label>
	    	</div>
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">email</i>	
	    		  <input class="black-text m-t-xs" type="text" value="<?php echo $email; ?>" disabled>
	    		  <label class="grey-text-bold active">Email</label>
	    	</div>
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">language</i>	
	    		  <input class="black-text m-t-xs" type="text" value="<?php echo $website; ?>" disabled>
	    		  <label class="grey-text-bold active">Website</label>
	    	</div>
    	</div>
    </div>
    
    <div class="collapsible-header">Authorized Managing Officer</div>
    <div class="collapsible-body" style="display: block !important;">
    	<div class="row">
	    	<div class="input-field col s4">
	    		  <i class="material-icons prefix">account_circle</i> 	
	    		  <input class="black-text m-t-xs" type="text"  value="<?php echo $rep_fname; ?>" disabled>
	    		  <label class="grey-text-bold active">First Name</label>
	    	</div>
	    	<div class="input-field col s4">
	    		  <i class="material-icons prefix">account_circle</i> 	
	    		  <input class="black-text m-t-xs" type="text"  value="<?php echo $rep_mname; ?>" disabled>
	    		  <label class="grey-text-bold active">Middle Name</label>
	    	</div>
	    	<div class="input-field col s4">
	    		  <i class="material-icons prefix">account_circle</i> 	
	    		  <input class="black-text m-t-xs" type="text"  value="<?php echo $rep_lname; ?>" disabled>
	    		  <label class="grey-text-bold active">Last Name</label>
	    	</div>
    	</div>
    	
    	<div class="row">
	    	<div class="input-field col s12">
	    		  <i class="material-icons prefix">location_on</i> 	
	    		  <input class="black-text m-t-xs" type="text"  value="<?php echo $rep_addr; ?>" disabled>
	    		  <label class="grey-text-bold active">Address</label>
	    	</div>
    	</div>
    	
    	<div class="row">
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">contact_phone</i> 	
	    		  <input class="black-text m-t-xs" type="text"  value="<?php echo $rep_contact; ?>" disabled>
	    		  <label class="grey-text-bold active">Contact No</label>
	    	</div>
	    	<div class="input-field col s6">
	    		  <i class="material-icons prefix">email</i> 	
	    		  <input class="black-text m-t-xs" type="text"  value="<?php echo $rep_email; ?>" disabled>
	    		  <label class="grey-text-bold active">Email</label>
	    	</div>
    	</div>
    	
    </div>
 
  </li>
</ul>