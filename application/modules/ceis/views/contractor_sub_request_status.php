<?php 
  $url_holder = $this->uri->segment(4);
?>
<div class="page-title">
  <ul id="breadcrumbs">
    <li><a href="#">Home</a></li>
    <li><a href="#">Requests</a></li>
    <li><a href="#" class="active">Request Status</a></li>
  </ul>

  <div class="row m-b-n">
    <div class="col s6 p-r-n">
      <h5>Request Status
      <span></span>
      </h5>
    </div>
  </div>
</div>


<div class="row m-t-lg">
  <div class="col s3">
      <?php 
        $url_holder = $this->uri->segment(4);
        $url_cont   = $this->uri->segment(3);
      ?>
      <div class="list-basic collection">
        <?php
        if($request_type_id=='1' or $request_type_id=='2' or $url_cont == 'submenu_contractor'){
      ?>
      <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_request/<?php echo $url_holder; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Contractor Info</a>
        <?php 
          }
          if($request_type_id=='none_alele'){
        ?>
        <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_work_experience/<?php echo ISSET($url_holder) ? $url_holder : ""; ?> "><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work Experience
        </a>
        <?php 
          }
          if($request_type_id=='1' or $request_type_id=='2' or $request_type_id=='4' or $url_cont == "submenu_contractor"){
        ?>
        <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_contracts/<?php echo ISSET($url_holder) ? $url_holder : ""; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Contracts
      </a>
        <?php 
          }
          if($request_type_id=='1' or $request_type_id=='2' or $url_cont == 'submenu_contractor'){
        ?>
        <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_staffing/<?php echo ISSET($url_holder) ? $url_holder : ""; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Staffing
        </a>
        <?php 
          }
          if($request_type_id=='1' or $request_type_id=='2' or $url_cont == 'submenu_contractor'){
        ?>
        <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_finance/<?php echo ISSET($url_holder) ? $url_holder : ""; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Finance
        </a>
        <?php 
          }
          if($request_type_id=='1' or $request_type_id=='2' or $url_cont == 'submenu_contractor'){
        ?>
        <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_work_volume/<?php echo ISSET($url_holder) ? $url_holder : ""; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work Volume
        </a>
        <?php 
          }
          if($request_type_id=='3' or $request_type_id=='4' or $url_cont == 'submenu_contractor'){
        ?>
        <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_project_info/<?php echo ISSET($url_holder) ? $url_holder : ""; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Project Info
        </a>
        <?php 
          }
          if($request_type_id=='4' or $url_cont == 'submenu_contractor'){
        ?>
        <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_foreign_principal/<?php echo ISSET($url_holder) ? $url_holder : ""; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Foreign Principal
        </a>
        <?php 
          }
          if($request_type_id=='3' or $url_cont == 'submenu_contractor'){
        ?>
        <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_owner/<?php echo ISSET($url_holder) ? $url_holder : ""; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Owner
        </a>
        <?php 
          }
          if($request_type_id=='3' or $request_type_id=='4' or $url_cont == 'submenu_contractor'){
        ?>
        <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_manpower_schedule/<?php echo ISSET($url_holder) ? $url_holder : ""; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Manpower Schedule
        </a>
        <?php 
          }
          if($request_type_id=='1' or $request_type_id=='2' or $request_type_id=='5' or $url=='submenu_contractor'){
        ?>
        <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_construction_equipment/<?php echo ISSET($url_holder) ? $url_holder : ""; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Construction Equipment
        </a>
        <?php 
          }
          if($request_type_id=='1' or $request_type_id=='2' or $request_type_id=='3' or $request_type_id=='4' or $url_cont == 'submenu_contractor'){
        ?>
        <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_checklist/<?php echo ISSET($url_holder) ? $url_holder : ""; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Checklist
        </a>
        <?php 
          }
          if($request_type_id=='none_alele' or $url_cont == 'submenu_contractor'){
        ?>
        <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_order_of_payment/<?php echo ISSET($url_holder) ? $url_holder : ""; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Order of Payment
        </a>
        <?php 
          }
          if($request_type_id=='none_alele' or $url_cont == 'submenu_contractor'){
        ?>
        <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_evaluation/<?php echo ISSET($url_holder) ? $url_holder : ""; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Evaluation
        </a>
        <?php 
          }
          if($request_type_id=='1' or $request_type_id=='2' or $request_type_id=='3' or $request_type_id=='4' or $request_type_id=='5' or $url_cont == 'submenu_contractor'){
        ?>
        <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_request_status/<?php echo ISSET($url_holder) ? $url_holder : ""; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Request Status
        </a> 
        <?php
        }
        ?>  
      </div>
  </div>
  <div class="col s9">
    <ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active">Request Status 
        </div>
        <div class="collapsible-body row">
          <div class="col s6">
            <div>
              <label>Application Date:</label>
              <h6>Application Date</h6>
            </div>
          </div>
          <div class="col s6">
            <div>
              <label>Request No:</label>
              <h6>1</h6>
            </div>
          </div>

          <div class="col s6">
            <div>
              <label>Reference No:</label>
              <h6>upon Processing by POCB</h6>
            </div>
          </div>
          <div class="col s6">
            <div>
              <label>Status:</label>
              <h6>In Processing</h6>
            </div>
          </div>
        </div>
      </li>
    </ul>

    <ul class="collapsible panel m-t-lg" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active">
        </div>
        <div class="collapsible-body row">
        <section id="content" style="padding-top: 0px;">
          <section class="hbox stretch">
          <section class="vbox">
            <section class="scrollable padder">
              <div align="right">
                  <a class="waves-effect waves-light btn" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_request_status/<?php echo $url_holder; ?>"> &nbsp;&nbsp;Task</a>&nbsp;
                  <a class="waves-effect waves-light btn" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_request_status_remarks/<?php echo $url_holder; ?>">&nbsp;&nbsp;Remarks</a>&nbsp;
                  <a class="waves-effect waves-light btn" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_request_status_attachments/<?php echo $url_holder; ?>">&nbsp;&nbsp;Attachments</a>
              </div>

              <form data-validate="parsley">
                <section class="panel panel-default">
                  <header class="panel-heading">
                    
                  </header>

                  <div class="table-responsive">
                  <table class="table table-striped m-b-none" data-ride="datatables">
                    <thead>
                      <tr>
                        <th width="15%">Task</th>
                        <th width="15%">Resource</th>
                        <th width="15%">Start Date</th>
                        <th width="15%">End Date</th>
                        <th width="15%">Status</th>
                        <th width="15%">Action</th>
                      </tr>
                    </thead>

                    <tbody>
                      <tr>
                        <td width="15%">
                          <input type="text" class="form-control" data-type="phone"  data-required="true">
                        </td>

                        <td width="15%">
                          <input type="text" class="form-control" data-type="phone"  data-required="true">
                        </td>

                        <td width="15%">
                          <input type="text" class="form-control" data-type="phone"  data-required="true">
                        </td>

                        <td width="15%">
                          <input type="text" class="form-control" data-type="phone"  data-required="true">
                        </td>

                        <td width="15%">
                          <select class="browser-default">
                            <option value="" disabled selected>Choose your option</option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                            <option value="3">Option 3</option>
                          </select>
                        </td>

                        <td width="15%">
                          <a class="flaticon-search95" href="<?php echo base_url(); ?>ceis/ceis_requests/equipment_remarks" ><i ></i></a>
                          <a class="flaticon-arrows97" href="#"></a>
                          <a class="flaticon-recycle69" href="#"></a>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                  <footer class="panel-footer text-right bg-light lter">
                    
                  </footer>
                </section>
              </form>
            </section>
          </section>
          </section>
        </section>
        </div>
      </li>
    </ul>
  </div>
</div>