<div class="page-title">
  	<ul id="breadcrumbs">
		<li><a href="#">Home</a></li>
		<li><a href="#" class="active">Directory</a></li>
  	</ul>
  	<div class="row m-b-n">
		<div class="col s6 p-r-n">
	  	<h5>Directories
			<span>Manage Directories</span>
	  	</h5>
		</div>
		<div class="col s6 p-r-n right-align">
	  		<div class="btn-group">	
	  		</div>

	  		<div class="input-field inline p-l-md p-r-md">
				<a class="hide" id="gen_link" hre="#" target="_tab"></a>
          		<!-- <button class="btn waves-effect waves-light green lighter-5" onclick="CeisDirectory.initexcel();" name="action" type="button" value="Save"> <i class="flaticon-printing23"></i>Excel </button> -->
          		<a href="ceis_directory/get_directory_list_excel" class="waves-effect waves-light btn green lighter-5"><i class="flaticon-x16" ></i >Excel</a></td>

          		<button class="btn waves-effect waves-light red darken-1" onclick="CeisDirectory.initpdf();" name="action" type="button" value="Save"> <i class="flaticon-acrobat2"></i>PDF </button>
		  	</div>

		</div>
  	</div>
</div>

<div class="m-t-lg">
</div>

<div id="test-list" class="card-panel white lighten-3 p-t-sm p-r-n p-b-n p-l-n m-n">
	<div>
		<div class="row m-n">
			<div class="col s8">
				&nbsp;
				<ul class="pagination">
					<?php foreach ($links as $link) {
						echo "<li>". $link."</li>";
						} 
					?>
				</ul>
			</div>
			<div class="col s4 p-r-md">
				<input type="text" name="" class="search-box white search" placeholder="Search">
			</div>
		</div>
	</div>

	<div class="row grey lighten-4 m-n p-sm p-b-lg list">
		<?php 
			foreach ($results as $data):
		?>
		<div class="col s4">
			<ul class="collapsible panel">
			  	<li>
			    	<div class="collapsible-header active name"><?php echo $data['contractor_name']; ?></div>
					    <div style="display:block !important">
						    <div>
						        <div class="col s2">
						          	<i class="flaticon-home145"></i>
						        </div>
						        <div class="col s10">
						        	<?php echo $data['contractor_address']; ?>
						        </div>
						    </div>

						    <div class="m-t-lg">&nbsp;</div>
						    
						    <div>
						        <div class="col s2">
						          	<i class="flaticon-telephone102"></i>
						        </div>
						        <div class="col s10">
						        	<?php echo $data['tel_no']; ?>
						        </div>
						    </div>

						    <div class="m-t-lg">&nbsp;</div>

						    <div>
						        <div class="col s2">
						          	<i class="flaticon-message15"></i>
						        </div>
						        <div class="col s10">
						        	<?php echo $data['email']; ?>
						        </div>
						    </div>

						    <div class="m-t-lg">&nbsp;</div>

						    <div>
						        <div class="col s2">
						          	<i class="flaticon-link56"></i>
						        </div>
						        <div class="col s10">
						        	<?php echo $data['website']; ?>
						        </div>
						    </div>

						    <div class="m-t-lg">&nbsp;</div>
							
							<div>
						        <div class="col s2">
						          	<i class="flaticon-man457"></i>
						        </div>
						        <div class="col s10">
						        	<?php echo $data['rep_first_name'].' '.$data['rep_mid_name'].' '.$data['rep_last_name']; ?>
						        </div>
						    </div>

						    <div class="m-t-lg">&nbsp;</div>
							
						    <div>
						        <div class="col s2">
						          	<b>SPECIALIZATION</b>
						        </div>
						        <div class="col s10"><br><br>
						        	<?php echo $data['license_classification_name']; ?>
						        </div>
						    </div>

						    <div class="m-t-lg">&nbsp;</div>
							
						    <div>
						        <div class="col s2">
						          	<b>OVERSEAS&nbsp;EXPERIENCE&nbsp;IN</b>
						        </div>
						        <div class="col s10"><br><br>
						        	<?php echo $data['country_name']; ?>
						        </div>
						    </div>

						    <div class="m-t-lg">&nbsp;</div>

						</div>
				</li>
			</ul>
		</div>
		<?php
			endforeach;
		?>
	</div>
</div>