<input type="hidden" id="security" value="<?php echo $security ?>">

<div class="page-title">
	<ul id="breadcrumbs">
		<li><a href="#">Home</a></li>
		<li><a href="<?php echo base_url() ?>ceis/ceis_requests">Requests</a></li>
    	<li><a href="#" class="active"><?php echo $contractor_name; ?></a></li>
  	</ul>

	<div class="row m-b-n">
		<div class="col s6 p-r-n">
      		<h5><?php echo $contractor_name; ?></h5>
	    </div>
  	</div>
</div>

<input type="hidden" name="crid" id="crid" value ="<?php echo $request_id; ?>" />
<input type="hidden" name="cpage" id="cpage" value ="<?php echo $default_page; ?>" />
<input type="hidden" name="add_info" id="add_info" value ="" />

<div class="row m-t-lg">
  	<div class="col s3 p-r-n">
  	
  		<div class="list-basic collection">
  	
    		<?php foreach($submenu as $page => $page_name): ?>
    			<a id="menu_<?php echo $page;?>" onclick="LoadTab.getPage('<?php echo $page; ?>')" class="collection-item"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i><?php echo $page_name; ?></a>
    		<?php endforeach;?>

  		</div>
	</div>
	<!-- 
<div class="col s12 p-t-sm">
		    <ul class="tabs grey darken-3">
		      <li class="tab col s3"><a class="active" href="#personal_information">Personal Information</a></li>
		      <li class="tab col s3"><a href="#work_experiences">Work Experiences</a></li>
		      <li class="tab col s3"><a href="#projects">Projects</a></li>
		    </ul>
  		</div>
  		 -->
	<div class="tab-content col s9 ">
		 
  	</div>
</div>
