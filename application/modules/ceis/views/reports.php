<div class="page-title">
  <ul id="breadcrumbs">
    <li><a href="#">Home</a></li>
    <li><a href="#" class="active">Reports</a></li>
  </ul>
  <div class="row m-b-n">
    <div class="col s6 p-r-n">
      <h5>Reports
        <span>Manage Reports</span>
      </h5>
    </div>
    <div class="col s6 p-r-n right-align">
      <label></label>
    </div>
  </div>
</div>

<div class="m-t-lg">
</div>

<ul class="collapsible panel" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active">Generate Report</div>
    <div class="collapsible-body" style="display: block !important;" >
      <form id="report_form">
          <div class="form-basic">
            <div class="row m-n">
              <div class="col s12">
                <h6>Report</h6>
                <div class="input-field">
                  <select name="report" id="report_list" class="selectize"  placeholder="Select sector">
                        <option value="1">Overseas Performance Reports</option>
                        <option value="2">Skills Summary/Salary Rate</option>
                  </select> 
                </div>
              </div>
            </div>
            <div class="row m-n">
              <div class="col s6">
                <h6>Year</h6>
                <div class="input-field">
                   <select required name="year" id="year_list" class="selectize">
                      <?php
                      for($i=date("Y")-10;$i<=date("Y");$i++) {
                          $sel = ($i == date('Y')) ? 'selected' : '';
                          echo "<option value=".$i." ".$sel.">".date("Y", mktime(0,0,0,0,1,$i+1))."</option>";
                      }
                      ?>
                    </select>
                </div>
              </div>

              <div id="contractor" class="col s6" style="display: none;">
                <h6>Contractors</h6>
                <div class="input-field">
                  <select required name="contractor_list" class="selectize" id="contractor_list" data-required="true">
                    <option value="0">Select Contractor</option>
                    <?php foreach($contractors as $contractor): ?>
                    <option value="<?php echo ISSET($contractor["contractor_id"]) ? $contractor["contractor_id"] : "" ?>"><?php echo ISSET($contractor["contractor_name"]) ? $contractor["contractor_name"] : "" ?></option>      
                    <?php endforeach; ?>
                  </select>
                </div>
              </div>  

              <div id="quarter" class="col s6">
                <h6>Quarter</h6>
                  <div class="input-field">
                    <select required name="quarter" id="quarter_list" class="selectize">
                      <!-- <option value="">Select Quarter</option> -->
                      <option value="1"> First Quarter</option>
                      <option value="2"> Second Quarter</option>
                      <option value="3"> Third Quarter</option>
                      <option value="4"> Fourth Quarter</option>
                      <option value="5"> Year End</option>
                    </select>                     
                  </div>
              </div>
            </div>
            <div class="row m-n">
              <div id="gen_type" class="col s12">
                <h6>Generate As</h6>

                <div class="col s12">
                    <div class="col s6">
                      <input checked name="generate" type="radio" id="report_type_pdf" value="PDF"/>
                      <label for="report_type_pdf">PDF</label>
                    </div>
                    <div id="excel" style="display:none;" class="col s6">
                      <input name="generate" type="radio" id="report_type_excel" value="Excel"/>
                      <label for="report_type_excel">Excel</label>
                    </div>
                </div>                  
              </div>
            </div>
          </div>
          <div class="panel-footer right-align">
            <a class="hide" id="gen_link" hre="#" target="_tab"></a>
            <button class="btn waves-effect waves-light"  id="generate_report" name="action" type="button" value="Save">Generate Report</button>
          </div>
      </form>
    </div>
  </li>
</ul>