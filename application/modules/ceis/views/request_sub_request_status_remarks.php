 <!--

<div class="row blue-teal lighten-5 m-b-md m-t-md ">

<div class="table-display">
<div class="table-cell p-md b-r valign-top" style="width:15%; border-style:dashed!important; border-right-color:#d2d2d2!important;">
                  <h6>Filter by criteria</h6>
                  <small>Use filters to limit results</small>
                  </div>
	<div class="table-cell valign-middle p-sm center-align" style="width:30%;"></div>
    <div class="table-cell valign-middle p-sm">
         <div style="display:inline-block;width:80%;"><input type="text"></div>
         <a class="waves-effect waves-light btn p-l-sm  p-r-sm"><i class="material-icons left m-r-xs">search</i>Search</a>
    </div>
</div>

</div>
                 -->

<div id="remarks_body" class="m-t-lg" style="height:550px;">

<?php foreach($remarks as $key => $val): ?>

	<blockquote style="border-left: 5px solid #64b5f6; padding:10px; background-color:#F5F5F0;">
		<span class="pull-left" style="margin:20px;"> &nbsp;<img src="<?php echo base_url(); ?>static/images/avatar_default.jpg" class="circle z-depth-4" ></span>
        <div class="media-body">
			<span class="pull-right"><b><?php echo $val['task_name']; ?><i class="flaticon-clipboard95"></i></b></span> 
			<b>REMARKS</b> <br><label>Task Step <?php echo $val['sequence_no']; ?></label>
			<p><?php echo $val['remarks']; ?></p>
			<i class="flaticon-weekly18"></i> <?php echo date('F d, Y', strtotime($val['remark_date'])); ?>&nbsp; &nbsp; &nbsp; &nbsp;  <i class="flaticon-user153"></i> <?php echo $val['resource']; ?>
			<i class="pull-right"><?php echo date('h:i a', strtotime($val['remark_date'])); ?></i>
		</div>
	</blockquote>
	<hr>
	
<?php endforeach; ?>
                   
      
<script>
	$(function(){
		$('#remarks_body').jScrollPane({autoReinitialise: true});
	});
</script>