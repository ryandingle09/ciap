<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Contract Involvement</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Contract Involvement
		<span>Manage Contract Involvement</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p-l-md p-r-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_contract_involvement" id="add_contract_involvement" name="add_contract_involvement" onclick="modal_init()">Contract Involvement</button>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="contract_involvement_table">
  <thead>
	<tr>
	  	<th width="25%">Contact Involvement</th>
	    <th width="20%">Created By</th>
	    <th width="15%">Created Date</th>
	    <th width="15%">Last Modified By</th>
	    <th width="15%">Last Modified Date</th>
	    <th width="10%">Actions</th>
	</tr>
  </thead>
  </table>
</div>
<script>
var deleteObj = new handleData({ controller : 'ceis_contract_involvement', method : 'delete_contract_involvement', module: '<?php echo PROJECT_CEIS ?>' });
</script>