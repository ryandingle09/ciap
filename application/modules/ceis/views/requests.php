<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
  <li><a href="#">Home</a></li>
  <li><a href="#" class="active">Requests</a></li>
  </ul>
  <div class="row m-b-n">
  <div class="col s6 p-r-n">
    <h5>Requests
    <span>Manage Requests</span>
    </h5>
  </div>
  <div class="col s6 p-r-md right-align">
    <div class="btn-group">
      
    </div>
  </div>
  </div>
</div>

<div class="pre-datatable">&nbsp;</div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="request_table">
  <thead>
    <tr>
      <th width="10%">App No</th>
      <th width="10%">App Date</th>
      <th width="15%">App Type</th>
      <th width="15%">Ref No.</th>
      <th width="15%">Company Name</th>
      <th width="15%">Project Name</th>
      <th width="10%">Status</th>
      <th width="10%">Action</th>
    </tr>
  </thead>
  </table>
</div>