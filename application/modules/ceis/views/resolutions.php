<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
  <li><a href="#">Home</a></li>
  <li><a href="#" class="active">Resolutions</a></li>
  </ul>
  <div class="row m-b-n">
  <div class="col s6 p-r-n">
    <h5>Resolutions
    <span>Manage Resolutions</span>
    </h5>
  </div>
  <div class="col s6 p-r-n right-align">
    <div class="btn-group">
      
    </div>

    
    <div class="input-field inline p-l-md p-r-md">
    <button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_resolution" id="add_resolution" name="add_resolution" onclick="modal_init()">Resolution</button>
    </div>
  </div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="table_resolution">
  <thead>
  <tr>
      <th width="15%">BR No</th>
      <th width="20%">BR Date</th>
      <th width="25%">BR Title</th>
      <th width="15%">First Round</th>
      <th width="15%">Last Round</th>
      <th width="10%">Actions</th>
  </tr>
  </thead>
  </table>
</div>

<!-- Modal -->
<div id="modal_resolution" class="md-modal md-effect-<?php echo MODAL_EFFECT ?>">
  <div class="md-content">
  <a class="md-close icon">&times;</a>
  <h3 class="md-header">Resolution</h3>
  <div id="modal_resolution_content"></div>
  </div>
</div>
<div class="md-overlay"></div>

<script type="text/javascript">
  var deleteObj = new handleData({ controller : 'ceis_resolutions', method : 'delete_resolution', module: '<?php echo PROJECT_CEIS ?>' });
</script>