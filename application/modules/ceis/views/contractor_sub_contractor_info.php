<ul class="collapsible panel" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active">Contractor Info 
    </div>
    <div class="collapsible-body row" style="display: block !important;">
      <div class="col s12">
        <div>
          <label>Company Name</label>
          <input disabled="" type="text" class="form-control" readonly="" placeholder="Company Name" data-required="true" value="<?php echo ISSET($contractor_name) ? $contractor_name : "" ; ?>">
        </div>
      </div>
      <div class="col s12">
        <div>
          <label>Company Address</label>
          <input disabled="" type="text" class="form-control" placeholder="Company Address" data-required="true" value="<?php echo ISSET($contractor_address) ? $contractor_address : "" ; ?>">
        </div>
      </div>
      <div class="col s6">
        <div>
          <label><b>PCBA LICENSE</b></label>
        </div>
      </div>
      <div class="col s6">
        <div>
          <label><b>SEC REGISTRATION</b></label>
        </div>
      </div>
      <div class="col s12"><br><br></div>
      <div class="col s6">
        <div>
            <label>Category</label>
            <select disabled="" class="browser-default"> 
              <option disabled="" value="">Select Country Name</option>
                <?php foreach ($contractor_category as $cont): ?>
                <option <?php if($cont["contractor_category_id"]==$contractor_category_id){ echo 'selected'; } ?> value="<?php echo $cont["contractor_category_id"]?>"><?php echo $cont["contractor_category_name"]?></option>
                <?php endforeach; ?>
            </select>
        </div>
      </div>
      <div class="col s3">
        <div>
          <label>No</label>
          <input disabled="" type="text" placeholder="Enter No." data-required="true" value="<?php echo ISSET($license_no) ? $license_no : "" ; ?>">
        </div>
      </div>
      <div class="col s3">
        <div>
          <label>Issued Date</label>
          <input disabled="" type="text" class="datepicker"  data-date-format="dd-mm-yyyy" value="<?php echo ISSET($sec_reg_date) ? $sec_reg_date : "" ; ?>">
        </div>
      </div>
      <div class="col s6">
        <div>
          <label><i>If not specialized consultancy, please enter PCAB license</i></label>
        </div>
      </div>
      <div class="col s6">
        <div>
          <label><b><i>CLEARANCES</i></b></label>
        </div>
      </div>
      <div class="col s12"><br><br></div>
      <div class="col s3">
        <div>
          <label>No.</label>
          <input disabled="" type="text" placeholder="" data-required="true" value="<?php echo ISSET($license_no) ? $license_no : "" ; ?>">
        </div>
      </div>
      <div class="col s3">
        <div>
          <label>Issued Date</label>
          <input disabled="" readonly="" type="text" class="datepicker" data-date-format="yyyy-mm-dd" placeholder="" value="<?php echo ISSET($license_date) ? $license_date : "" ; ?>">
        </div>
      </div>
      <div class="col s6">
        <div>
          <label>BIR</label>
          <input disabled="" readonly="" type="text" placeholder="" data-date-format="yyyy-mm-dd" data-required="true" value="<?php echo ISSET($bir_clearance_date) ? $bir_clearance_date : "" ; ?>">
        </div>
      </div>
      <div class="col s6">
        <div>
          <label class="active" for="classification">Classification</label>
          <div class="input-field">
            <select disabled name="classification[]" id="classification" class="selectize" placeholder="" multiple="multiple">
              <option selected value="classification1">Select Classification1</option>
              <option value="classification2">Select Classification2</option>
              <option value="classification3">Select Classification3</option>
              <option value="classification4">Select Classification4</option>
              <option value="classification5">Select Classification5</option>
            </select>
          </div>
        </div>
      </div>
      <div class="col s6">
        <div>
          <label>Customs</label>
          <input disabled="" readonly="" type="text" placeholder="" data-date-format="yyyy-mm-dd" data-required="true" value="<?php echo ISSET($customs_clearance_date) ? $customs_clearance_date : "" ; ?>">
        </div>
      </div>
      <div class="col s6">
        <div>
          <label class="active" for="specialization">Specialization</label>
          <div class="input-field">
            <select disabled name="specialization[]" id="specialization" class="selectize" placeholder="" multiple="multiple">
              <option value="specialization1">Select Specialization1</option>
              <option value="specialization2">Select Specialization2</option>
              <option value="specialization3">Select Specialization3</option>
              <option value="specialization4">Select Specialization4</option>
              <option value="specialization5">Select Specialization5</option>
            </select>
          </div>
        </div>
      </div>
      <div class="col s6">
        <div>
          <label>Court</label>
          <input disabled="" readonly="" type="text" placeholder="" data-date-format="yyyy-mm-dd" data-required="true" value="<?php echo ISSET($court_clearance_date) ? $court_clearance_date : "" ; ?>">
        </div>
      </div>
      <div class="col s12">
        <div>
          <label><b><i>AUTHORIZED REPRESENTATIVE</i></b></label>
        </div>
      </div>
      <div class="col s12"><br><br></div>
      <div class="col s4">
        <div>
          <label>Name</label>
          <input disabled="" type="text" placeholder="Enter First Name" data-required="true" value="<?php echo ISSET($rep_first_name) ? $rep_first_name : "" ; ?>">
        </div>
      </div>
      <div class="col s4">
        <div>
          <label><br></label>
          <input disabled="" type="text" placeholder="Enter Middle Name" data-required="true" value="<?php echo ISSET($rep_mid_name) ? $rep_mid_name : "" ; ?>">
        </div>
      </div>
      <div class="col s4">
        <div>
          <label><br></label>
          <input disabled="" type="text" placeholder="Enter Last Name" data-required="true" value="<?php echo ISSET($rep_last_name) ? $rep_last_name : "" ; ?>">
        </div>
      </div>
      <div class="col s12">
        <div>
          <label>Address</label>
          <input disabled="" type="text" placeholder="Enter Address" data-required="true" value="<?php echo ISSET($rep_address) ? $rep_address : "" ; ?>">
        </div>
      </div>
      <div class="col s6">
        <div>
          <label>Contact No.</label>
          <input disabled="" type="text" placeholder="" data-required="true" value="<?php echo ISSET($rep_contact_no) ? $rep_contact_no : "" ; ?>">
        </div>
      </div>
      <div class="col s6">
        <div>
          <label>Email</label>
          <input disabled="" type="text" placeholder="Enter Email" data-required="true" value="<?php echo ISSET($rep_email) ? $rep_email : "" ; ?>">
        </div>
      </div>
    </div>
  </li>
</ul>