
      <ul class="collapsible panel" data-collapsible="expandable">
        <li>
          <div class="collapsible-header active">Directors 
          </div>
          <div class="collapsible-body row"  style="display: block">
            <table id="table_director_staffing">
              <thead>
                <th width="45%">Name<?php echo ISSET($contr_staff_id) ? $contr_staff_id : ""; ?></th>
                <th width="25%">Years with Company</th>
                <th width="15%">Status</th>
                <th width="15%">Actions</th>
              </thead>
              <tbody>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tbody>
            </table>
          </div>
        </li>
      </ul>

      <ul class="collapsible panel m-t-lg" data-collapsible="expandable">
        <li>
          <div class="collapsible-header active">Principal Officers  
          </div>
          <div class="collapsible-body row" style="display: block">
            <table id="table_principal_officer_staffing">
              <thead>
                <th width="45%">Name</th>
                <th width="25%">Years with Company</th>
                <th width="15%">Status</th>
                <th width="15%">Actions</th>
              </thead>
              <tbody>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tbody>
            </table>
          </div>
        </li>
      </ul>

      <ul class="collapsible panel m-t-lg" data-collapsible="expandable">
        <li>
          <div class="collapsible-header active">Key Technical Staff  
          </div>
          <div class="collapsible-body row" style="display: block">
            <table id="table_tech_staff_staffing">
              <thead>
                <th width="45%">Name</th>
                <th width="25%">Years with Company</th>
                <th width="15%">Status</th>
                <th width="15%">Actions</th>
              </thead>
              <tbody>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
              </tbody>
            </table>
          </div>
        </li>
      </ul>
