<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Geographical Distribution</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Geographical Distribution
		<span>Manage Geographical Distribution</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	  </div>
	  
	  <div class="input-field inline p-l-md p-r-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_geographical_distribution" id="add_geographical_distribution" name="add_geographical_distribution" onclick="modal_init()">Geographical Distribution</button>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="geographical_distribution_table">
  <thead>
	<tr>
	    <th width="15%">Region</th>
        <th width="15%">No. of Countries</th>
        <th width="15%">Created By</th>
        <th width="15%">Created Date</th>
        <th width="15%">Last Modified By</th>
        <th width="15%">Last Modified Date</th>
        <th width="10%">Actions</th>
	</tr>
  </thead>
  </table>
</div>

<!-- Modal -->
<div id="modal_geographical_distribution" class="md-modal md-effect-<?php echo MODAL_EFFECT ?>">
  <div class="md-content">
	<a class="md-close icon">&times;</a>
	<h3 class="md-header">Geographical Distribution</h3>
	<div id="modal_geographical_distribution_content"></div>
  </div>
</div>
<div class="md-overlay"></div>

<script type="text/javascript">
	var	deleteObj = new handleData({ controller : 'ceis_geographical_distribution', method : 'delete_geographical_distribution', module: '<?php echo PROJECT_CEIS ?>' });
</script>