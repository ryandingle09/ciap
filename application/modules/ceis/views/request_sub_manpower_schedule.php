<div class="row">
  <div class="col s12">
    <ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active">Manpower Schedule 
        </div>
        <div class="collapsible-body row" style="display: block">
          <table id="table_manpower_schedule">
            <thead>
              <th>Description of Skills</th>
              <th>Salary Rate</th>
              <th>Number Required</th>
              <th>Action</th>
            </thead>
            <tbody>
            </tbody>
          </table>
        </div>
      </li>
    </ul>
  </div>
</div>