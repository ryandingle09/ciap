<ul class="collapsible panel" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active">Work volume during the latest three (3) years 
    </div>
    <div class="collapsible-body row" style="display: block"> 
      <!-- <div class="col s12">
          <div>
            <label>Year <?php echo $highest_year -2; ?></label>
            <input disabled="" type="text" value="<?php echo ISSET($amount) ? number_format($amount,2) : "" ; ?>">
          </div>
      </div> -->
      
      <?php 
        foreach($contractor_work_volume_info as $contractor_work_volume_info):
      ?>
        <div class="col s12">
            <div>
              <label>Year <?php echo $contractor_work_volume_info['year']; ?></label>
              <input disabled="" type="text" value="<?php echo ISSET($contractor_work_volume_info['amount']) ? number_format($contractor_work_volume_info['amount'],2) : "" ; ?>">
            </div>
        </div>
      <?php 
          
        endforeach;
      ?>


      <div class="col s12">
          <div>
            <label>THREE YEAR TOTAL</label>
            <input disabled="" type="text" value="<?php echo $total_work_volume[0]['sum(amount)'] ; ?>">
          </div>
      </div>
      <div class="col s12">
          <div>
            <label>AVG TOTAL</label>
            <input disabled="" type="text" value="<?php echo $avg_work_volume[0]['avg(amount)'] ; ?>">
          </div>
      </div>
    </div>
  </li>
</ul>
  