<ul class="collapsible panel" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active">Work volume during the latest three (3) years 
    </div>
    <div class="collapsible-body row  p-lg" style="display: block"> 
    
    <table class="striped">
         <tbody>
         	<?php foreach($work_volume as $key => $val):
         			$style 		 = ($key == 0) ? 'style="width:35%;"' : '';
         			$year_total += $val['gross_sales'];
         	?>
         			<tr>
						<th <?php echo $style; ?> >YEAR <?php echo date('Y', strtotime($val['statement_date'])); ?></th>
						<td><?php echo decimal_format($val['gross_sales'])?></td>
					</tr>
         	<?php endforeach; ?>
         			<tr>
						<th>THREE YEAR TOTAL</th>
						<td><?php echo decimal_format($year_total); ?></td>
					</tr>
					<tr>
						<th>AVG TOTAL</th>
						<td><?php echo decimal_format(($year_total / 3)); ?></td>
					</tr>
		 </tbody>
    </table>           
    
    <!-- 
      <div class="col s6">
          <div>
            <label>Year 2015</label>
            <input disabled="" type="text" value="<?php echo ISSET($current_year) ? $current_year : "11,996.02" ; ?>">
          </div>
      </div>
      <div class="col s6">
          <div>
            <label>Year 2016</label>
            <input disabled="" type="text" value="<?php echo ISSET($prev_year) ? $prev_year : "32,323.90" ; ?>">
          </div>
      </div>
      <div class="col s6">
          <div>
            <label>AVG TOTAL</label>
            <input disabled="" type="text" value="<?php echo ISSET($avg_total) ? $avg_total : "16,217,821.00" ; ?>">
          </div>
      </div>
      <div class="col s6">
          <div>
            <label>THREE YEAR TOTAL</label>
            <input disabled="" type="text" value="<?php echo ISSET($three_year_total) ? $three_year_total : "142,602.00" ; ?>">
          </div>
      </div>
       -->
       
       
    </div>
  </li>
</ul>