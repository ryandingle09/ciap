<?php
$statement_date = ( ISSET($financial['statement_date']) && ! EMPTY($financial['statement_date']) && $financial['statement_date'] != DEFAULT_DATE) ? date('d F Y', strtotime($financial['statement_date'])) : '';
$paid_up  = (ISSET($financial['paid_up']) && ! EMPTY($financial['paid_up'])) ? decimal_format($financial['paid_up']) : '0.00';
$networth = (ISSET($financial['networth']) && ! EMPTY($financial['networth'])) ? decimal_format($financial['networth']) : '0.00';


$legal_requirement 	   	 = (ISSET($legal_requirement)) ? $legal_requirement: '';
$construction_contract 	 = ( ! EMPTY($construction_contract) || $construction_contract === '0' ) ? $construction_contract : '';
$completed_contract		 = ( ! EMPTY($completed_contract) || $completed_contract === '0') ? $completed_contract : '';
$rescinded_contract 	 = ( ! EMPTY($rescinded_contract) || $rescinded_contract === '0') ? $rescinded_contract : '';
$work_experience 	 	 = (ISSET($work_experience)) ? $work_experience : '';
$total_staff 	 		 = ( ! EMPTY($total_staff) || $total_staff === '0' )  ? $total_staff : '';
$staff_reqmt 	 		 = (ISSET($staff_reqmt)) 	 ? $staff_reqmt : '';
$recommendation 	 	 = (ISSET($recommendation))  ? $recommendation : '';

$header_class  = 'font-md font-semibold black-text';
$label_class   = 'font-sm font-semibold grey-text';
?>

<ul class="collapsible panel" data-collapsible="expandable">
  <li>
    <div class="collapsible-header active">I. The Evaluation Commitee submits hereunder its findings and recommendations: 
    </div>

    <div class="collapsible-body row" style="display: block">
      <form id="evaluation_form" data-validate="parsley">
      
        <section class="panel panel-default">
          <header class="panel-heading">
          </header>
          
          <div class="table-responsive">
            <table class="table table-striped m-b-none">
              <thead>
              </thead>
              <tbody>
                <tr>
                  <td>
                    <label class="<?php echo $header_class; ?>">A. LEGAL REQUIREMENTS</label>
                  </td>
                  <td>
                  </td>
                </tr>
                <tr>
                  <td width="50%"><label class="<?php echo $label_class; ?>">Supporting documents on the legal requirements for renewal of registration.</label></td>
                  <td width="50%">
                      	<input name="legal_requirements" type="radio" value="<?php echo COMPLETE_FLAG; ?>"	id="legal_complete" data-parsley-required="true"  <?php echo ($legal_requirement == COMPLETE_FLAG) ? 'checked' : ''; ?> />
                      <label for="legal_complete">Complete</label>
                    	<input name="legal_requirements" type="radio" value="<?php echo INCOMPLETE_FLAG; ?>" id="legal_incomplete" <?php echo ( EMPTY($legal_requirement) || $legal_requirement == INCOMPLETE_FLAG) ? 'checked' : ''; ?> />
                      <label for="legal_incomplete">Incomplete</label>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label class="<?php echo $header_class; ?>">B. WORK EXPERIENCE</label>
                  </td>
                  <td>
                  </td>
                </tr>
                <tr>
                  <td width="50%"><label class="<?php echo $label_class; ?>">No. of years of actual construction contract operation</label></td>
                  <td width="50%"><input text="text" name="construction_contract" id="construction_contract" placeholder="Enter years"  value="<?php echo $construction_contract; ?>" data-parsley-required="true" data-parsley-type="number"></td>
                </tr>
                <tr>
                  <td width="50%"><label class="<?php echo $label_class; ?>">No. of successfully completed contracts during the period costing PhP 10M or greater for construction contractors or PhP 1M or greater for specialized consultancy or Php 5M or greater for specialty contractor (for the last 5 years)</label></td>
                  <td width="50%"><input text="text" placeholder="Enter no. of completed contracts" name="completed_contracts" id="completed_contracts"  value="<?php echo $completed_contract; ?>" data-parsley-required="true" data-parsley-type="digits"></td>
                </tr>
                <tr>
                  <td width="50%"><label class="<?php echo $label_class; ?>">No. of contracts rescinded by the owner for cause(s) not satifactorily explained</label></td>
                  <td width="50%"><input text="text" placeholder="Enter no. of rescinded contracts" name="rescinded_contracts" id="rescinded_contracts"  value="<?php echo $rescinded_contract; ?>" data-parsley-required="true" data-parsley-type="digits"></td>
                </tr>
                <tr>
                  <td width="50%"><label class="<?php echo $label_class; ?>">Work Experience Requirements for Registration</label></td>
                  <td width="50%">
                    	<input name="work_experience"  type="radio"  value="<?php echo COMPLETE_FLAG; ?>" id="work_exp_complete" data-parsley-required="true" <?php echo ($work_experience == COMPLETE_FLAG) ? 'checked' : ''; ?> />
                    <label for="work_exp_complete">Complete</label>
                   		 <input name="work_experience" type="radio"   value="<?php echo INCOMPLETE_FLAG; ?>" id="work_exp_incomplete" <?php echo ( EMPTY($work_experience) || $work_experience == INCOMPLETE_FLAG) ? 'checked' : ''; ?> />
                    <label for="work_exp_incomplete">Incomplete</label>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label class="<?php echo $header_class; ?>">C. STAFFING</label>
                  </td>
                  <td>
                  </td>
                </tr>
                <tr>
                  <td width="50%"><label class="<?php echo $label_class; ?>">No. of key management/technical personnel employed on a full time basis</label></td>
                  <td width="50%"><input type="text"  placeholder="Enter no. of personnel" name="no_staff" value="<?php echo $total_staff; ?>" data-parsley-required="true" data-parsley-type="digits"> </td>
                </tr>
                <tr>
                  <td width="50%"><label class="<?php echo $label_class; ?>">Staffing Requirements for Registration</label></td>
                  <td width="50%">
                    	<input name="staff_requirements" type="radio"  value="<?php echo COMPLETE_FLAG; ?>" id="staff_complete" data-parsley-required="true" <?php echo ($staff_reqmt == COMPLETE_FLAG) ? 'checked' : ''; ?>/>
                    <label for="staff_complete">Complete</label>
                   		<input name="staff_requirements" type="radio"  value="<?php echo INCOMPLETE_FLAG; ?>" id="staff_incomplete" <?php echo (EMPTY($staff_reqmt) || $staff_reqmt == INCOMPLETE_FLAG) ? 'checked' : ''; ?>/>
                    <label for="staff_incomplete">Incomplete</label>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label class="<?php echo $header_class; ?>">D. FINANCE</label>
                  </td>
                  <td>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label class="font-md font-normal black-text">As of <?php echo $statement_date; ?></label>
                  </td>
                  <td>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label class="<?php echo $label_class; ?>"> Paid-up Capital </label>
                  </td>
                  <td>
                   <!--  <label><b>PHP 50,000,000.00</b></label> -->
                   <input name="paid_up_capital" placeholder="Paid up capital" type="text" id="paid_up_capital" value="<?php echo $paid_up; ?>" data-parsley-required="true" data-parsley-type="number" readonly/>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label class="<?php echo $label_class; ?>"> Net worth </label>
                  </td>
                  <td>
                    <!-- <label><b>PHP 268,997,608.00</b></label>  -->
                    <input name="networth" placeholder="Networth" type="text" id="networth" value="<?php echo $networth; ?>" data-parsley-required="true" data-parsley-type="number" readonly/>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label class="<?php echo $header_class; ?>">E. CLASSIFICATION</label>
                  </td> 
                  <td>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label class="<?php echo $label_class; ?>">Annual average work volume for the last three (3) years</label>
                  </td>
                  <td>
                    <!-- <label><b>PHP 50,000,000.00</b></label>-->
                    <input name="work_volume" placeholder="Networth" type="text" id="work_volume" value="<?php echo $work_volume; ?>" data-parsley-required="true" data-parsley-type="number" readonly/>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label class="<?php echo $label_class; ?>">Specialization:</label>
                  </td>
                  <td>
                  	<?php foreach($specialization as $key => $val): ?>
                  		<div class="chip m-b-sm"><?php echo $val['license_classification_name']; ?></div>
                  	<?php endforeach;?>
                  </td>
                </tr>
                <tr>
                  <td>
                    <label class="<?php echo $header_class; ?>">II. In view of the foregoing, the Evaluation Committee recommends that the subject application be</label>
                  </td>
                  <td>
                    <input name="application_result" type="radio"  value="<?php echo APPROVE_FLAG; ?>" id="application_approved" data-parsley-required="true" <?php echo ($recommendation == APPROVE_FLAG) ? 'checked' : ''; ?>/>
                    <label for="application_approved">Approved</label>
                    <input name="application_result" type="radio"  value="<?php echo DISAPPROVE_FLAG; ?>" id="application_disapproved" <?php echo ( EMPTY($recommendation) || $recommendation == DISAPPROVE_FLAG) ? 'checked' : ''; ?>/>
                    <label for="application_disapproved">Disapproved</label>
                  </td>
                  
                </tr>
              </tbody>
            </table>
            <div style="margin:10px;" class="panel-footer right-align">
<?php if($show_save_btn): ?>
              <button class="btn waves-effect waves-light"  id="save_evaluation" name="action" type="button" value="Save">Save</button>
<?php endif; ?>
<!-- 
              <button class="btn waves-effect waves-light" onclick="CeisRequests.initpdf();"  type="button" value="Save"> <i class="flaticon-printing23"></i>Print </button>
              -->
              <button class="btn waves-effect waves-light"  id="print_eval_form" name="print_eval_form" type="button" > <i class="flaticon-printing23"></i>Print</button>
            </div>
          </div>
        </section>
      </form>
    </div>
  </li>
</ul>
  