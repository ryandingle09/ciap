<div id="attachments_body" class="m-t-lg" style="height:550px;">

<?php foreach($attachments as $key => $val): ?>

	<blockquote style="border-left: 5px solid #64b5f6; padding:10px; background-color:#F5F5F0;">
		<span class="pull-left" style="margin:20px;"> &nbsp;<img src="<?php echo base_url(); ?>static/images/avatar_default.jpg" class="circle z-depth-4" ></span>
        <div class="media-body">
			<span class="pull-right"><b><?php echo $val['task_name']; ?><i class="flaticon-clipboard95"></i></b></span> 
			<b><?php echo $val['display_filename']; ?></b> <br><label class="m-t-xs">Task Step <?php echo $val['sequence_no']; ?></label>
			<p><?php echo $val['description']; ?></p>
			<i class="flaticon-weekly18"></i> <?php echo date('F d, Y', strtotime($val['received_date'])); ?>&nbsp; &nbsp; &nbsp; &nbsp;  <i class="flaticon-user153"></i> <?php echo $val['resource']; ?>
			&nbsp; &nbsp; &nbsp; &nbsp;<i class="flaticon-clipboard93"></i> <?php echo $val['attachment_type']; ?>
			&nbsp; &nbsp; &nbsp; &nbsp;<a href="<?php echo base_url().'ceis/ceis_request_tasks/download_attachment/'.$filepath.'/'.$val['file_name']; ?>" style="color:#039be5;cursor:pointer;"><i class="flaticon-download157"></i>&nbsp;Download</a>
			<i class="pull-right"><?php echo date('h:i a', strtotime($val['received_date'])); ?></i>
		</div>
	</blockquote>
	<hr>
	
<?php endforeach; ?>
</div>

<script>
	$(function(){
		$('#attachments_body').jScrollPane({autoReinitialise: true});
	});
</script>

                    