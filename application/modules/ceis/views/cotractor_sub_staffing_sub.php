<div class="page-title">
  <ul id="breadcrumbs">
    <li><a href="#">Home</a></li>
    <li><a href="#">Requests</a></li>
    <li><a href="#" class="active">Staffing</a></li>
  </ul>

  <div class="row m-b-n">
    <div class="col s6 p-r-n">
      <h5>Staffing
      <span></span>
      </h5>
    </div>
  </div>
</div>

<div class="row m-t-lg">
  <div class="col s3">
    <?php 
      $url_holder = $this->uri->segment(4);
      $url_cont = $this->uri->segment(3);
    ?>
    <div class="list-basic collection">
      <?php
      if($request_type_id=='1' or $request_type_id=='2' or $url_cont == 'submenu_contractor'){
    ?>
    <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_request/<?php echo $url_holder; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Contractor Info</a>
      <?php 
        }
        if($request_type_id=='none_alele'){
      ?>
      <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_work_experience/<?php echo $url_holder; ?> "><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work Experience
      </a>
      <?php 
        }
        if($request_type_id=='1' or $request_type_id=='2' or $request_type_id=='4' or $url_cont == "submenu_contractor"){
      ?>
      <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_contracts/<?php echo $url_holder; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Contracts
    </a>
      <?php 
        }
        if($request_type_id=='1' or $request_type_id=='2' or $url_cont == 'submenu_contractor'){
      ?>
      <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_staffing/<?php echo $url_holder; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Staffing
      </a>
      <?php 
        }
        if($request_type_id=='1' or $request_type_id=='2' or $url_cont == 'submenu_contractor'){
      ?>
      <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_finance/<?php echo $url_holder; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Finance
      </a>
      <?php 
        }
        if($request_type_id=='1' or $request_type_id=='2' or $url_cont == 'submenu_contractor'){
      ?>
      <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_work_volume/<?php echo $url_holder; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Work Volume
      </a>
      <?php 
        }
        if($request_type_id=='3' or $request_type_id=='4' or $url_cont == 'submenu_contractor'){
      ?>
      <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_project_info/<?php echo $url_holder; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Project Info
      </a>
      <?php 
        }
        if($request_type_id=='4' or $url_cont == 'submenu_contractor'){
      ?>
      <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_foreign_principal/<?php echo $url_holder; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Foreign Principal
      </a>
      <?php 
        }
        if($request_type_id=='3' or $url_cont == 'submenu_contractor'){
      ?>
      <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_owner/<?php echo $url_holder; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Owner
      </a>
      <?php 
        }
        if($request_type_id=='3' or $request_type_id=='4' or $url_cont == 'submenu_contractor'){
      ?>
      <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_manpower_schedule/<?php echo $url_holder; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Manpower Schedule
      </a>
      <?php 
        }
        if($request_type_id=='1' or $request_type_id=='2' or $request_type_id=='5' or $url=='submenu_contractor'){
      ?>
      <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_construction_equipment/<?php echo $url_holder; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Construction Equipment
      </a>
      <?php 
        }
        if($request_type_id=='1' or $request_type_id=='2' or $request_type_id=='3' or $request_type_id=='4' or $url_cont == 'submenu_contractor'){
      ?>
      <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_checklist/<?php echo $url_holder; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Checklist
      </a>
      <?php 
        }
        if($request_type_id=='none_alele' or $url_cont == 'submenu_contractor'){
      ?>
      <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_order_of_payment/<?php echo $url_holder; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Order of Payment
      </a>
      <?php 
        }
        if($request_type_id=='none_alele' or $url_cont == 'submenu_contractor'){
      ?>
      <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_evaluation/<?php echo $url_holder; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Evaluation
      </a>
      <?php 
        }
        if($request_type_id=='1' or $request_type_id=='2' or $request_type_id=='3' or $request_type_id=='4' or $request_type_id=='5' or $url_cont == 'submenu_contractor'){
      ?>
      <a class="collection-item grey darken-1 white-text" href="<?php echo base_url(); ?>ceis/ceis_requests/submenu_request_status/<?php echo $url_holder; ?>"><i class="fa fa-chevron-right pull-right m-t-xs text-xs icon-muted"></i>Request Status
      </a> 
      <?php
      }
      ?>  
    </div>
  </div>

  <div class="col s9">
    <ul class="collapsible panel" data-collapsible="expandable">
      <li>
        <div class="collapsible-header active">Directors 
        </div>
        <div class="collapsible-body row">
          <div class="row">
            <div class="col s12">
              <ul class="tabs">
                <li class="tab col s3"><a href="#personal"  class="active">Personal Information</a></li>
                <li class="tab col s3"><a href="#experience"              >Work Experience</a></li>
                <li class="tab col s3"><a href="#projects"                >Projects </a> </li>
              </ul>
            </div>
            <div id="personal" class="col s12">
              <ul class="collapsible panel m-t-lg" data-collapsible="expandable">
                <li>
                  <div class="collapsible-header active">Basic Information 
                  </div>
                  <div class="collapsible-body" style="background-color:#F6F6F6; margin:0;">
                    <div class="row">
                      <div class="col s4">
                        <div>
                          <label>First Name</label>
                          <input readonly="" type="text" class="form-control" readonly="" placeholder="First Name" data-required="true" value="<?php echo ISSET($contractor_name) ? $contractor_name : "" ; ?>">
                        </div>
                      </div>
                      <div class="col s4">
                        <div>
                          <label>Middle Name</label>
                          <input readonly="" type="text" class="form-control" readonly="" placeholder="Middle Name" data-required="true" value="<?php echo ISSET($contractor_name) ? $contractor_name : "" ; ?>">
                        </div>
                      </div>
                      <div class="col s4">
                        <div>
                          <label>Last Name</label>
                          <input readonly="" type="text" class="form-control" readonly="" placeholder="Last Name" data-required="true" value="<?php echo ISSET($contractor_name) ? $contractor_name : "" ; ?>">
                        </div>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col s12">
                        <div>
                          <label>Address</label>
                          <input readonly="" type="text" class="form-control" readonly="" placeholder="Address" data-required="true" value="<?php echo ISSET($contractor_name) ? $contractor_name : "" ; ?>">
                        </div>
                      </div>
                    </div>
                    
                    <div class="row">
                      <div class="col s6">
                        <div>
                          <label>Birth Date</label>
                          <input readonly="" type="text" class="form-control" readonly="" placeholder="Birth Date" data-required="true" value="<?php echo ISSET($contractor_name) ? $contractor_name : "" ; ?>">
                        </div>
                      </div>
                    </div>

                    <div class="row">
                      <div class="col s6">
                        <div>
                          <label>Birth Place</label>
                          <input readonly="" type="text" class="form-control" readonly="" placeholder="Birth Place" data-required="true" value="<?php echo ISSET($contractor_name) ? $contractor_name : "" ; ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="collapsible-header active">Educational Background 
                  </div>
                  <div class="collapsible-body" style="background-color:#F6F6F6; margin:0;">
                    <div class="row">
                      <div class="col s12">
                        <div>
                          <label>School/University Attended</label>
                          <input readonly="" type="text" class="form-control" readonly="" placeholder="School/University Attended" data-required="true" value="<?php echo ISSET($contractor_name) ? $contractor_name : "" ; ?>">
                        </div>
                      </div>
                    </div>
                    <div class="row">
                      <div class="col s6">
                        <div>
                          <label>Highest Educational Attainment</label>
                          <input readonly="" type="text" class="form-control" readonly="" placeholder="Highest Educational Attainment" data-required="true" value="<?php echo ISSET($contractor_name) ? $contractor_name : "" ; ?>">
                        </div>
                      </div>
                      <div class="col s4">
                        <div>
                          <label>Year Graduated</label>
                          <input readonly="" type="text" class="form-control" readonly="" placeholder="Year Graduated" data-required="true" value="<?php echo ISSET($contractor_name) ? $contractor_name : "" ; ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="collapsible-header active">Professional License 
                  </div>
                  <div class="collapsible-body" style="background-color:#F6F6F6; margin:0;">
                    <div class="m-t-lg">
                      <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="staffing_professional_table">
                      <thead>
                      <tr>
                        <th width="20%">Name of Position</th>
                        <th width="20%">Created By</th>
                        <th width="20%">Created Date</th>
                        <th width="15%">Last Modified By</th>
                        <th width="15%">Last Modified Date</th>
                        <th width="10%" class="text-center">Actions</th>
                      </tr>
                      </thead>
                      </table>
                    </div>
                  </div>
                </li>
              </ul>
            </div>
            <div id="experience"  class="col s12">
              <ul class="collapsible panel m-t-lg" data-collapsible="expandable">
                  <li>
                    <div class="collapsible-header active"></div>
                    <div class="collapsible-body" style="background-color:#F6F6F6; margin:0;">
                      <div class="m-t-lg">
                        <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="staffing_project_table">
                        <thead>
                        <tr>
                          <th width="20%">Name of Position</th>
                          <th width="20%">Created By</th>
                          <th width="20%">Created Date</th>
                          <th width="15%">Last Modified By</th>
                          <th width="15%">Last Modified Date</th>
                          <th width="10%" class="text-center">Actions</th>
                        </tr>
                        </thead>
                        </table>
                      </div>
                    </div>
                  </li>
              </ul>
            </div>
            <div id="projects"    class="col s12">
              <ul class="collapsible panel m-t-lg" data-collapsible="expandable">
                <li>
                  <div class="collapsible-header active"></div>
                  <div class="collapsible-body" style="background-color:#F6F6F6; margin:0;">
                      <div class="m-t-lg">
                        <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="staffing_project_table">
                        <thead>
                        <tr>
                          <th width="20%">Name of Position</th>
                          <th width="20%">Created By</th>
                          <th width="20%">Created Date</th>
                          <th width="15%">Last Modified By</th>
                          <th width="15%">Last Modified Date</th>
                          <th width="10%" class="text-center">Actions</th>
                        </tr>
                        </thead>
                        </table>
                      </div>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </li>
    </ul>
  </div>
</div>