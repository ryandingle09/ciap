<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_request_foreign_principal_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	

	public function get_request_foreign_principal($request_id)
	{
		try
		{
			$query = <<<EOS
				
					SELECT *
					FROM %s
					WHERE request_id = ? 
EOS;
			$query = sprintf($query, CEIS_Model::tbl_request_aut_services);
			return $this->query($query, array($request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
	}
}

/* End of file Requests_model.php */
/* Location: ./application/modules/ceis/models/Requests_model.php */