<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Opportunities_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	
	
	public function get_opportunity_list($aColumns, $bColumns, $params)
	{
		try
		{
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));
			
			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
		
			
			$query 	  = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields
					FROM %s a 
					LEFT JOIN %s b 
					ON a.opp_source_id = b.opp_source_id
					LEFT JOIN %s c
					ON a.opp_status_id = c.opp_status_id
					LEFT JOIN %s d
					ON a.created_by = d.employee_no
					LEFT JOIN %s e
					ON a.modified_by = e.employee_no
					$filter_str
				GROUP BY a.opportunity_id
	        	$sOrder
	        	$sLimit;
EOS;
			$query 	= sprintf($query, CEIS_Model::tbl_opportunities, CEIS_Model::tbl_opportunity_sources, CEIS_Model::tbl_param_opportunity_status, CEIS_Model::tbl_users, CEIS_Model::tbl_users);
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	public function get_contractor_email_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("opportunity_id" => $id);
				
			return $this->select_one($fields, CEIS_Model::tbl_opportunity_recipients, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}	
	
	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_opportunity_list($aColumns, $bColumns, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	public function get_opportunity_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("opportunity_id" => $id);
				
			return $this->select_one($fields, CEIS_Model::tbl_opportunities, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}

	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(opportunity_id) cnt");
				
			return $this->select_one($fields, CEIS_Model::tbl_opportunities);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_specific_opportunity($where){

		try
		{
			$fields = array("*");
			
			return $this->select_one($fields, CEIS_Model::tbl_opportunities, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_specific_opportunity_recipient($where){

		try
		{
			$fields = array("*");
			
			return $this->select_all($fields, CEIS_Model::tbl_opportunity_recipients, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_sources(){

		try{
			$fields = array("*");
			return $this->select_all($fields, CEIS_Model::tbl_opportunity_sources);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_contractors(){

		try{
			$fields = array("*");
			return $this->select_all($fields, CEIS_Model::tbl_contractors);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_opportunities(){

		try{
			$fields = array("*");
			return $this->select_all($fields, CEIS_Model::tbl_opportunities);
		}

		catch(PDOException $e)
		{
			$this->rlog_error($e);	
			throw $e;
		}
	}

	public function get_distinct_opportunities(){
		
		try{
			$query 	  = <<<EOS
			SELECT DISTINCT reference_no
			FROM %s
EOS;
		$query	= sprintf($query, CEIS_Model::tbl_opportunities);	
		return $this->query($query);

		}

		
		catch(PDOException $e){
			$this->rlog_error($e);
			throw $e;	
		}
		catch(Exception $e){
			$this->rlog_error($e);
			throw $e;	
		}
	}

	public function delete_opportunity($opportunity_id)
	{
		try
		{		
			$this->delete_data(CEIS_Model::tbl_opportunity_recipients, array('opportunity_id'=>$opportunity_id));	
			$this->delete_data(CEIS_Model::tbl_opportunities, array('opportunity_id'=>$opportunity_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}


	public function delete_opportunity_recipients($opportunity_id)
	{
		try
		{		
			$this->delete_data(CEIS_Model::tbl_opportunity_recipients, array('opportunity_id'=>$opportunity_id));				
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function insert_opportunity($params){
			
		try
		{
			$val 					= array();
			$val["reference_no"] 	= $params['reference_no'];
			$val["received_date"] 	= date('Y-m-d', strtotime(filter_var($params['received_date'], FILTER_SANITIZE_STRING)));
			$val["opp_source_id"] 	= $params['source'];
			$val["source_agency"] 	= filter_var($params['source_agency'], FILTER_SANITIZE_STRING);
			$val["subject"] 		= filter_var($params['subject'], FILTER_SANITIZE_STRING);
			$val["opp_status_id"] 	= $params['source'];
			$val["created_by"] 		= $this->session->userdata("employee_no");
			$val["created_date"] 	= date('Y-m-d H:i:s');
			$count_number			= $this->get_reference_number();
			$count_number			= $count_number['count_number'];

			//FOR REFERENCE NUMBER
			if($params['reference_no']=='0'){

				$data_value          = date('Y');
				$count_number 		 = $count_number+1;        	
	          	$new_count           = str_pad($count_number,3,"0", STR_PAD_LEFT);
	          	$generated_val       = $data_value.'-'.$new_count;
	          	$val["reference_no"] = $generated_val;
			}	
				
          	$val['reference_no'];

			return $this->insert_data(CEIS_Model::tbl_opportunities, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function insert_opportunity_recipients($params){
			
		try
		{
			if(!EMPTY($params['recipient_name']) or !EMPTY($params['email']))
				{
					$val 					= array();
					$val['opportunity_id']  = $params['opportunity_id'];

					$val["contractor_name"] = $params['recipient_name'];
					$val["email"] 			= $params['email'];
					$val["created_by"] 		= $this->session->userdata("employee_no");


					/*if($params['contractor_list'] == "0" || $params['contractor_list'] == ''){
						$val['contractor_name'] = $params['contractor_name'];
					}
					else{
						$val['contractor_name'] = $params['contractor_list'];	
					}*/

					$val["created_date"] 	= date('Y-m-d H:i:s');

					$this->insert_data(CEIS_Model::tbl_opportunity_recipients, $val, TRUE);

					/*
					if(!EMPTY($id) && !EMPTY($params["email"]))
						$this->_insert_email($params["email"], $id);

					return $id;
					*/

					/*foreach ($params['country_name'] as $value):
						$val	 				= array();
						$val["geo_region_id"]	= $geo_region_id;
						$val["country_code"] 	= $value;
						$this->insert_data($this->tbl_opportunity_recipients, $val);
					endforeach;*/
				}
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_reference_number(){
		
		try{
			$query 	  = <<<EOS
			SELECT COUNT(DISTINCT reference_no) count_number
			FROM %s
EOS;
		$query = sprintf($query, CEIS_Model::tbl_opportunities);
		return $this->query($query, NULL, FALSE);

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function update_opportunity($params)
	{
		try
		{	


			$opportunity_id 			= $params["opportunity_id"];
			$val 						= array();	
			$val['opportunity_id'] 		= $params['opportunity_id'];
			$val['reference_no'] 		= $params['reference_no'];
			$val['received_date']		= date('Y-m-d', strtotime($params['received_date']));
			$val['opp_source_id'] 		= $params['source'];
			$val['source_agency'] 		= $params['source_agency'];
			$val['subject'] 			= $params['subject'];
			// $val['contractor_list']		= $params['recipient_name'];
			// $val['email']				= $params['email'];
			$val["modified_by"] 		= $this->session->userdata("employee_no");
			$val["modified_date"]		= date('Y-m-d H:i:s');
			
			$where 						= array();
			$where["opportunity_id"]	= $opportunity_id;
			// echo '<pre>'; print_r($val); echo '</pre>';

			$this->update_data(CEIS_Model::tbl_opportunities, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
}

/* End of file Opportunities_model.php */
/* Location: ./application/modules/ceis/models/Opportunities_model.php */