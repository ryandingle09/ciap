<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Directory_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}


	public function fetch_data()
	{
		try
		{	
			$contract_type = CONTRACT_OVERSEAS;

			$query = <<<EOS
				SELECT 	a.*,
						GROUP_CONCAT(DISTINCT if(c.specialty_flag, license_classification_name ,null) separator ', ')license_classification_name,
						GROUP_CONCAT(DISTINCT(e.country_name)) country_name
					FROM  %s a
						JOIN %s b
							ON a.contractor_id = b.contractor_id
						JOIN %s c
							ON b.license_classification_code = c.license_classification_code
						LEFT JOIN contractor_contracts d
							ON a.contractor_id = d.contractor_id
						LEFT JOIN param_countries e
							ON d.country_code = e.country_code
						WHERE d.base = '$contract_type'
						GROUP BY a.contractor_id
						ORDER BY a.contractor_id ASC
EOS;
			$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_license_classifications, CEIS_Model::tbl_param_license_classifications);	
			$info = $this->query($query);

			$count 	= count($info);
		
			if ($count) {
			foreach ($info as $info) {
				$data[] = $info;
			}



			return $data;
		}
		return false;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function record_count()
	{
		try
		{
			$fields = array("COUNT(contractor_id) cnt");
				
			$info =  $this->select_one($fields, 'contractor');

			return $info['cnt'];
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_directory_list()
	{
		try
		{	
			$contract_type = CONTRACT_OVERSEAS;

			$query = <<<EOS
				SELECT 	a.*,
						GROUP_CONCAT(DISTINCT if(c.specialty_flag, license_classification_name ,null) separator ', ')license_classification_name,
						GROUP_CONCAT(DISTINCT(e.country_name)) country_name
					FROM  %s a
						JOIN %s b
							ON a.contractor_id = b.contractor_id
						JOIN %s c
							ON b.license_classification_code = c.license_classification_code
						LEFT JOIN contractor_contracts d
							ON a.contractor_id = d.contractor_id
						LEFT JOIN param_countries e
							ON d.country_code = e.country_code
						WHERE d.base = '$contract_type'
						GROUP BY a.contractor_id
						ORDER BY a.contractor_id DESC
EOS;
			$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_license_classifications, CEIS_Model::tbl_param_license_classifications);	
			return $this->query($query);

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
}

/* End of file Directory_model.php */
/* Location: ./application/modules/ceis/models/Directory_model.php */