<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contractors_model extends CEIS_Model
{
	public function __construct() {
		parent::__construct();	
	}

	public function get_contractor_list($aColumns, $bColumns, $params)
	{
		try
		{	
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];

			$query 	  = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields
					FROM %s a 
					LEFT JOIN %s b
					ON a.contractor_category_id = b.contractor_category_id
					$filter_str
				GROUP BY a.contractor_id
	        	$sOrder
	        	$sLimit;
EOS;
			$query = sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_categories);
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_contractor_statement($contractor_id, $statement_date=NULL, $fields=array('*'))
	{
		try
		{	
			$where = array('contractor_id' => $contractor_id);
			
			if( ! EMPTY($statement_date))
				$where['statement_date'] = date('Y-m-d', strtotime($statement_date));
			
			return $this->select_all($fields, CEIS_Model::tbl_contractor_statement, $where,null,null,"LIMIT 0,3");
		}
		catch(PDOException $e)
		{
			throw new Exception($e, EXCEPTION_CUSTOM);
		}
	}

	public function get_statement_list($year, $contractor_id)
	{
		try
		{	

			$query = <<<EOS
				SELECT $year
				FROM  %s a
				WHERE contractor_id = $contractor_id
				order by statement_date desc limit 3
EOS;

			$query	= sprintf($query, CEIS_Model::tbl_contractor_statement);	
			return $this->query($query);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	public function get_contract_mobilization_list($aColumns, $bColumns, $params, $contractor_id, $contract_id)
	{
		try
		{	

			$fields = str_replace(" , ", " ", implode(", ", $aColumns));
			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str 		= $sWhere["search_str"];
			$filter_params 		= $sWhere["search_params"];
			$filter_params[] 	= $contractor_id;
			$filter_params[] 	= $contract_id;
			$filter_params[] 	= $contract_id;

			$query 	  = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields
					FROM %s a 
					LEFT JOIN %s b
					ON a.progress_report_id = b.progress_report_id
					LEFT JOIN %s c
					ON b.contract_id = c.contract_id
					LEFT JOIN %s d
					ON c.contractor_id = d.contractor_id
					LEFT JOIN %s e
					ON e.contr_staff_id = a.contr_staff_id
					LEFT JOIN %s f
					ON f.position_id = a.position_id
					WHERE d.contractor_id = ? AND 
						c.contract_id = ? AND 
						b.contract_id = ?
					$filter_str
				GROUP BY a.report_manpower_id
	        	$sOrder
	        	$sLimit;
EOS;
			$query = sprintf($query, CEIS_Model::tbl_progress_report_manpower, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_staff,CEIS_Model::tbl_positions);
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_construction_payment_list($aColumns, $bColumns, $params, $contractor_id=NULL)
	{
		try
		{	
			
			/*$cColumns = array(
					'a.rqst_payment_id',
					'a.request_id',
					'a.op_date',
					'a.op_amount',
					'a.created_by',
					'a.created_date',
					'a.modified_by',
					'a.modified_date',
					'b.coll_nature_code',
					'b.amount');*/
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
			$filter_params[] = $contractor_id;
			
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a 
					LEFT JOIN %s b 
					ON a.contractor_id = b.contractor_id
					WHERE a.contractor_id = ? 
EOS;
			$query = sprintf($query, CEIS_Model::tbl_contractor_payments, CEIS_Model::tbl_contractors
);
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	public function get_staffing_experience_list($aColumns, $bColumns, $params, $contractor_id, $contr_staff_id)
	{
		try
		{	
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);

			$filter_str 		= $sWhere["search_str"];
			$filter_params 		= $sWhere["search_params"];
			$filter_params[] 	= $contractor_id;
			$filter_params[] 	= $contr_staff_id;

			$query 	  = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields
					FROM %s a 
					LEFT JOIN %s b
					ON a.contr_staff_id = b.contr_staff_id
					LEFT JOIN %s c
					ON c.contractor_id = b.contractor_id
					WHERE b.contractor_id = ?
					AND b.contr_staff_id = ?

EOS;
			$query = sprintf($query, CEIS_Model::tbl_contractor_staff_experiences, CEIS_Model::tbl_contractor_staff, CEIS_Model::tbl_contractors );
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_staffing_project_list($aColumns, $bColumns, $params, $contractor_id, $contr_staff_id)
	{
		try
		{	
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			

			$filter_str 		= $sWhere["search_str"];
			$filter_params 		= $sWhere["search_params"];
			$filter_params[] 	= $contr_staff_id;
		
			$query 	  = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields
					FROM %s a 
					LEFT JOIN %s b
					ON a.contr_staff_id = b.contr_staff_id
					WHERE a.contr_staff_id = ?
					GROUP BY a.contr_staff_proj_id;
EOS;
			$query 	= sprintf($query, CEIS_Model::tbl_contractor_staff_projects, CEIS_Model::tbl_contractor_staff );
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_staffing_director_list($aColumns, $bColumns, $params, $staff_type=NULL, $contractor_id=NULL)
	{
		try
		{	
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str 		= $sWhere["search_str"];
			$filter_params 		= $sWhere["search_params"];
			$filter_params[] 	= $staff_type;
			$filter_params[] 	= $contractor_id;
	
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields
					FROM %s a 
					LEFT JOIN %s b
						ON a.contractor_id = b.contractor_id 
					LEFT JOIN %s c
						ON a.contr_staff_id = c.contr_staff_id
					WHERE a.staff_type 	 = ? AND b.contractor_id = ?
						$filter_str
					GROUP BY a.contr_staff_id
			        	$sOrder
			        	$sLimit;
EOS;

			$query  = sprintf($query, CEIS_Model::tbl_contractor_staff, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_staff_experiences);
			$stmt 	= $this->query($query, $filter_params);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_progress_report_list($aColumns, $bColumns, $params, $contractor_id, $contract_id)
	{
		try
		{	

			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere	= $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str 		= $sWhere["search_str"];
			$filter_params 		= $sWhere["search_params"];
			$filter_params[] 	=  $contract_id;
			$filter_params[] 	=  $contractor_id;

			$query 	  = <<<EOS
				SELECT * 
					FROM %s a
					LEFT JOIN %s b
					ON a.contract_id = b.contract_id
					LEFT JOIN %s c
					ON b.contractor_id = c.contractor_id
					LEFT JOIN %s d
					ON d.contract_status_id = a.contract_status_id
					WHERE b.contract_id = ?
					AND c.contractor_id = ?
EOS;

			$query    	= sprintf($query,CEIS_Model::tbl_progress_reports,  CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractors, CEIS_Model::tbl_param_contract_status);
			$stmt 		= $this->query($query, $filter_params);
			
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}


	//Gets Country list from table param_contractor_categories
	public function get_category(){

		try{
			$fields = array("*");
			return $this->select_all($fields, CEIS_Model::tbl_contractor_categories);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_contract_project_list($aColumns, $bColumns, $params, $contractor_id=NULL)
	{
		try
		{	
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str 		= $sWhere["search_str"];
			$filter_params 		= $sWhere["search_params"];
			$filter_params[] 	= $contractor_id;
			
			$query 	  = <<<EOS
                SELECT SQL_CALC_FOUND_ROWS $fields
					FROM %s a
						LEFT JOIN %s b
							ON a.contract_id = b.contract_id
						LEFT JOIN %s c
							ON b.contractor_id = c.contractor_id
						LEFT JOIN %s d
							ON b.contract_status_id = d.contract_status_id
						WHERE c.contractor_id = ?
						$filter_str
					GROUP BY a.contract_id
		        	$sOrder
		        	$sLimit;
EOS;

			$query = sprintf($query, CEIS_Model::tbl_contractor_projects, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractors,  CEIS_Model::tbl_param_contract_status);

			$stmt = $this->query($query, $filter_params);

			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_contract_service_list($aColumns, $bColumns, $params, $contractor_id=NULL)
	{
		try
		{	
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
			$filter_params[] = $contractor_id;
			
			$query 	  = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields
					FROM %s a
						LEFT JOIN %s b
							ON a.contract_id = b.contract_id
						LEFT JOIN %s c
							ON b.contractor_id = c.contractor_id
						LEFT JOIN %s d
							ON b.contract_status_id = d.contract_status_id
						WHERE c.contractor_id = ?
						$filter_str
					GROUP BY a.contract_id
		        	$sOrder
		        	$sLimit;

EOS;

			$query = sprintf($query,CEIS_Model::tbl_contractor_services, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractors, CEIS_Model::tbl_param_contract_status);

		
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	public function get_specific_contract_project($where)
	{
		try
		{	
			$fields = array("*");

			$query 	= <<<EOS
				SELECT $fields
					FROM %s a, %s b, %s c
					WHERE a.contractor_id = b.contractor_id 
					AND b.contract_id = c.contract_id

EOS;

			$query = sprintf($query,CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractor_projects, $where, TRUE);
			$smtp = $this->query($query);

			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_specific_contractor_staff($contr_staff_id)
	{
		try
		{	
			$fields = '*';
			// $where	= array("contr_staff_id" => $contr_staff_id);

			$query 	= <<<EOS

				SELECT $fields from %s a
					LEFT JOIN %s b
					ON a.contractor_id = b.contractor_id
					LEFT JOIN %s c
					ON a.contr_staff_id = c.contr_staff_id
					WHERE a.contr_staff_id = ?
EOS;

			$query = sprintf($query,CEIS_Model::tbl_contractor_staff, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_staff_projects);
			$stmt 	= $this->query($query, array($contr_staff_id),FALSE);

			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_specific_staff_experience($contr_staff_exp_id)
	{
		try
		{	
			$fields = '*';
			// $where	= array("contr_staff_id" => $contr_staff_id);

			$query 	= <<<EOS

				SELECT $fields from %s a
					WHERE a.contr_staff_exp_id = ?
EOS;

			$query = sprintf($query,CEIS_Model::tbl_contractor_staff_experiences);
			$stmt 	= $this->query($query, array($contr_staff_exp_id),FALSE);

			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	public function get_specific_staff_projects($contr_staff_proj_id)
	{
		try
		{	
			$fields = '*';
			// $where	= array("contr_staff_id" => $contr_staff_id);

			$query 	= <<<EOS

				SELECT $fields from %s a
					WHERE a.contr_staff_proj_id = ?
EOS;

			$query = sprintf($query,CEIS_Model::tbl_contractor_staff_projects);
			$stmt 	= $this->query($query, array($contr_staff_proj_id),FALSE);

			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_specific_work_experience($contr_staff_id)
	{
		try
		{	
			$fields = '*';
			// $where	= array("contr_staff_id" => $contr_staff_id);

			$query 	= <<<EOS

				SELECT SQL_CALC_FOUND_ROWS $fields
				FROM  %s a
                LEFT JOIN %s b
                ON a.contr_staff_id = b.contr_staff_id
				WHERE a.contr_staff_id = ?
				GROUP BY a.contr_staff_exp_id;
EOS;

			$query = sprintf($query,CEIS_Model::tbl_contractor_staff_experiences, CEIS_Model::tbl_contractor_staff );
			$stmt 	= $this->query($query, array($contr_staff_id),FALSE);

			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_specific_contract_progress_pro($progress_report_id){

		try
		{
			$fields 		= "*";
			

			//select query
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a
					LEFT JOIN %s b
					ON a.progress_report_id = b.progress_report_id
					LEFT JOIN %s c
					ON b.contract_id = c.contract_id
					LEFT JOIN contractor d
					ON c.contractor_id = d.contractor_id
					WHERE a.progress_report_id = ? AND a.type = 'P';
EOS;

			$query 	= sprintf($query,CEIS_Model::tbl_progress_report_info, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractors); 
			$stmt 	= $this->query($query, array($progress_report_id),FALSE);
		
			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_specific_contract_progress_semi($progress_report_id){

		try
		{
			$fields 		= "*";
			

			//select query
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a
					LEFT JOIN %s b
					ON a.progress_report_id = b.progress_report_id
					LEFT JOIN %s c
					ON b.contract_id = c.contract_id
					LEFT JOIN contractor d
					ON c.contractor_id = d.contractor_id
					WHERE a.progress_report_id = ? AND a.type = 'S';
EOS;

			$query 	= sprintf($query,CEIS_Model::tbl_progress_report_info, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractors); 
			$stmt 	= $this->query($query, array($progress_report_id),FALSE);
		
			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_work_volume_list($aColumns, $bColumns, $params)
	{
		try
		{	
			
			/*$cColumns = array('a.contractor_id',
				'a.year',
				'a.amount',
				'a.created_by',
				'a.created_date',
				'a.modified_by',
				'a.modified_date');*/
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
		
			
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a
EOS;
			$query = sprintf($query, CEIS_Model::tbl_contractor_work_volume);
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function filtered_length($aColumns, $bColumns, $params, $function_name="get_contractor_list", $contractor_id=NULL, $staff_type=NULL, $contract_id=NULL, $contr_staff_id=NULL)
	{
		try
		{
			// echo "alele:".$contractor_id;
			switch($function_name)
			{
				case "get_staffing_director_list":
					$this->{$function_name}($aColumns, $bColumns, $params, $staff_type, $contractor_id);
				break;	

				case "get_progress_report_list":
					$this->{$function_name}($aColumns, $bColumns, $params, $contractor_id, $contract_id);
				break;

				case "get_contract_remittance_list":
					$this->{$function_name}($aColumns, $bColumns, $params, $contractor_id, $contract_id);
				break;

				case "get_contract_mobilization_list":
					$this->{$function_name}($aColumns, $bColumns, $params, $contractor_id, $contract_id);
				break;

				case "get_staffing_experience_list":
					$this->{$function_name}($aColumns, $bColumns, $params, $contractor_id, $contr_staff_id);
				break;

				case "get_staffing_project_list":
					$this->{$function_name}($aColumns, $bColumns, $params, $contractor_id, $contr_staff_id);
				break;

				default:
					$this->{$function_name}($aColumns, $bColumns, $params, $contractor_id);
				break;

			}
			
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
			$stmt = $this->query($query, NULL, FALSE);

			// print_r($stmt);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	public function get_equipment_list($aColumns, $bColumns, $params)
	{
		try
		{	
			
			/*$cColumns = array(
						"a.serial_no",
						"a.registrant_name", 
						"a.registrant_address", 
						"a.equipment_type", 
						"a.capacity", 
						"a.brand",
						"a.model",
						"a.acquired_date",
						"a.acquisition_cost",
						"a.present_condition",
						"a.location",
						"a.motor_no",
						"a.color",
						"a.year",
						"a.flywheel",
						"a.acquired_from",
						"a.ownership_type",
						"a.active_flag",
						"a.created_by",
						"a.created_date",
						"a.modified_by",
						"a.modified_date");*/
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
		
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM  %s a
				$filter_str
				GROUP BY a.serial_no
	        	$sOrder
	        	$sLimit
EOS;

			
			$query	= sprintf($query, CEIS_Model::tbl_equipment);			
			$stmt	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_construction_equipment_list($aColumns, $bColumns, $params, $contractor_id=NULL)
	{
		try
		{	
			
			$aColumns = array(
						"a.serial_no",
						"a.registrant_name", 
						"a.registrant_address", 
						/*"a.equipment_type", */
						"a.capacity", 
						"a.brand",
						"a.model",
						"a.acquired_date",
						"a.acquisition_cost",
						"a.present_condition",
						"a.location",
						"a.motor_no",
						"a.color",
						"a.year",
						"a.flywheel",
						"a.acquired_from",
						"a.ownership_type",
						"a.active_flag",
						"a.created_by",
						"a.created_date",
						"a.modified_by",
						"a.modified_date");
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str 	= $sWhere["search_str"];
			$filter_params 	= $sWhere["search_params"];
		
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM  %s a
				LEFT JOIN %s b
				ON a.contractor_id = b.contractor_id
				WHERE a.contractor_id = '$contractor_id'
				$filter_str
				GROUP BY a.serial_no
	        	$sOrder
	        	$sLimit
EOS;

			
			$query	= sprintf($query, CEIS_Model::tbl_equipment, CEIS_Model::tbl_contractors);			
			$stmt	= $this->query($query, array('serial_no'), TRUE);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	public function get_profession_list($contr_staff_id)
	{
		try
		{	
			$fields 		= '*';	
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM  %s a
				WHERE a.contr_staff_id = ?
				GROUP BY a.license_no
EOS;

			
			$query	= sprintf($query, CEIS_Model::tbl_contractor_staff_licenses);
			$stmt 	= $this->query($query, array($contr_staff_id),TRUE);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	

	public function get_registration_list($contractor_id)
	{
		try
		{	
			$fields 		= '*';	
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM  %s a
				LEFT JOIN %s b
				ON a.registration_status_id = b.registration_status_id
				WHERE a.contractor_id = ?
EOS;

			$query	= sprintf($query, CEIS_Model::tbl_contractor_registration, CEIS_Model::tbl_param_registration_status);
			$stmt 	= $this->query($query, array($contractor_id),TRUE);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	

	public function get_collection_list($contractor_payment_id)
	{
		try
		{	
			$fields 		= '*';	
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM  %s a
				WHERE a.contractor_payment_id = ?
EOS;

			
			$query	= sprintf($query, CEIS_Model::tbl_contractor_payment_collections);
			$stmt 	= $this->query($query, array($contractor_payment_id),TRUE);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	

	public function get_contractors_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("contractor_id" => $id);
				
			return $this->select_one($fields, CEIS_Model::tbl_contractors, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}

	public function total_length($field="contractor_id", $table=CEIS_Model::tbl_contractors, $contractor_id=NULL, $staff_type=NULL,  $contract_id=NULL, $progress_report_id=NULL, $contr_staff_id=NULL)
	{
		try
		{
			$fields = array("COUNT(".$field.") cnt ");
			$where  = array();

			if($contractor_id)
				$where["contractor_id"] 		= $contractor_id;	

			if($staff_type)
				$where["staff_type"] 			= $staff_type;	

			if($contract_id)
				$where["contract_id"] 			= $contract_id;	

			if($progress_report_id)
				$where["progress_report_id"] 	= $progress_report_id;	

			if($contr_staff_id)
				$where["contr_staff_id"] 		= $contr_staff_id;	

			

			return $this->select_one($fields, $table);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_finance_contractor_statements_list($aColumns, $bColumns, $params, $contractor_id=NULL)
	{
		try
		{	
			$aColumns = array(/*'a.statement_id',*/
					'a.contractor_id',
					'a.statement_date',
					'a.stmt_type',
					'a.total_current_assets',
					'a.total_non_current_assets',
					'a.total_current_liabilities',
					'a.long_term_liabilities',
					'a.networth',
					'a.gross_sales',
					'a.net_profit',
					'a.common_shares',
					'a.par_value',
					'a.created_by',
					'a.created_date',
					'b.contractor_name',
					'b.contractor_address'
					);
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));
			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str 		= $sWhere["search_str"];
			$filter_params 		= $sWhere["search_params"];
			$filter_params[] 	= $contractor_id;
		
			
			//select query
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a 
					LEFT JOIN %s b 
					ON a.contractor_id = b.contractor_id
                    WHERE a.contractor_id = ?

EOS;
			$query 	= sprintf($query,CEIS_Model::tbl_contractor_statement, CEIS_Model::tbl_contractors); 
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_specific_contractor($where){

		try
		{
			$fields 			 = array("*");

			return $this->select_one($fields, CEIS_Model::tbl_contractors, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_specific_mobilization($progress_report_id){

		try
		{
			$fields = "*";
			$where['progress_report_id'] 	= $progress_report_id;

			return $this->select_one($fields, CEIS_Model::tbl_progress_report_manpower, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_specific_finance($statement_date){

		try
		{
			$fields = "*";
			$where['statement_date'] 	= $statement_date;

			return $this->select_one($fields, CEIS_Model::tbl_contractor_finance, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_specific_remittance($report_remitt_id){

		try
		{
			$fields = "*";

			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
					FROM  %s a
					LEFT JOIN %s b
					ON a.country = b.country_code
					WHERE a.report_remitt_id = ?
EOS;

			$query 	= sprintf($query,CEIS_Model::tbl_progress_report_remittances, CEIS_Model::tbl_countries); 
			$stmt 	= $this->query($query, array($report_remitt_id),FALSE);

			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_contract_remittance_list($aColumns, $bColumns, $params, $contractor_id, $contract_id)
	{
		try
		{	

			$fields = str_replace(" , ", " ", implode(", ", $aColumns));
			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
			$filter_params[] = $contractor_id;
			$filter_params[] = $contract_id;
			$filter_params[] = $contract_id;
			

			$query 	  = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields
					FROM %s a 
					LEFT JOIN %s b
					ON a.progress_report_id = b.progress_report_id
					LEFT JOIN %s c
					ON b.contract_id = c.contract_id
					LEFT JOIN %s d
					ON c.contractor_id = d.contractor_id
					LEFT JOIN %s e
					ON a.country = e.country_code
					WHERE d.contractor_id = ? AND c.contract_id = ? AND b.contract_id = ?
					$filter_str
				GROUP BY a.report_remitt_id
	        	$sOrder
	        	$sLimit;
EOS;
			$query = sprintf($query, CEIS_Model::tbl_progress_report_remittances, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractors, CEIS_Model::tbl_countries);
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_countries(){

		try{
			$fields = array("*");
			return $this->select_all($fields, CEIS_Model::tbl_countries);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_status(){

		try{
			$fields = array("*");
			return $this->select_all($fields, CEIS_Model::tbl_param_contract_status);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_specific_contracts($where){

		try
		{
			$fields 			 = array("*");
			
			return $this->select_one($fields, CEIS_Model::tbl_contractor_contracts, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_specific_equipment($serial_no){

		try
		{
			$fields 			 	= array("*");
			$where['serial_no'] 	= $serial_no;
			
			return $this->select_one($fields, CEIS_Model::tbl_equipment, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}


	public function get_specific_order_payment($contractor_payment_id){

		try
		{
			$fields 			 			= array("*");
			$where['contractor_payment_id'] = $contractor_payment_id;
			
			return $this->select_one($fields, CEIS_Model::tbl_contractor_payments, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	public function get_specific_contracts_project($where){

		try
		{
			$fields 				= array("*");

			return $this->select_one($fields, CEIS_Model::tbl_contractor_projects, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_specific_contract_info($contract_id){

		try
		{
			$fields = "*";
			
			//select query
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a
					LEFT JOIN %s b
					ON a.contractor_id = b.contractor_id
					WHERE a.contract_id = ?;
EOS;

			$query 	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractors); 
			$stmt 	= $this->query($query, array($contract_id),FALSE);
		
			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	
	public function get_specific_statement($contractor_id){

		try
		{
			$fields 		= "*";
			

			//select query
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a
					LEFT JOIN %s b
					ON a.contractor_id = b.contractor_id
					WHERE a.contractor_id = ?;
EOS;

			$query 	= sprintf($query,CEIS_Model::tbl_contractor_statement, CEIS_Model::tbl_contractors); 
			$stmt 	= $this->query($query, array($contractor_id),FALSE);
		
			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_first_statement($year, $contractor_id){

		try
		{
			$fields 		= "*";
			

			//select query
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a
					LEFT JOIN %s b
					ON a.contractor_id = b.contractor_id
					WHERE a.contractor_id = ?
					AND CONTAINS(a.statement_date, $year);
EOS;

			$query 	= sprintf($query,CEIS_Model::tbl_contractor_statement, CEIS_Model::tbl_contractors); 
			$stmt 	= $this->query($query, array($contractor_id),FALSE);
		
			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_max_date_statement($contractor_id){

		try
		{
			/*$fields 		= "*";
			*/

			//select query
			$query 	  = <<<EOS
				select MAX(statement_date) from %s a
					LEFT JOIN %s b
					ON a.contractor_id = b.contractor_id
					WHERE a.contractor_id = ?;
EOS;

			$query 	= sprintf($query,CEIS_Model::tbl_contractor_statement, CEIS_Model::tbl_contractors); 
			$stmt 	= $this->query($query, array($contractor_id),FALSE);
		
			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}



	public function get_max_date_work_volume($contractor_id){

		try
		{
			/*$fields 		= "*";
			*/

			//select query
			$query 	  = <<<EOS
				select MAX(year) from %s a
					LEFT JOIN %s b
					ON a.contractor_id = b.contractor_id
					WHERE a.contractor_id = ?;
EOS;

			$query 	= sprintf($query,CEIS_Model::tbl_contractor_work_volume, CEIS_Model::tbl_contractors); 
			$stmt 	= $this->query($query, array($contractor_id),FALSE);
		
			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_specific_work_volume($contractor_id){

		try
		{
			$fields 	= 	"year, 
							amount
							";
			

			//select query
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a
					WHERE a.contractor_id = ? 
					order by year desc limit 3
EOS;

			$query 	= sprintf($query,CEIS_Model::tbl_contractor_work_volume); 
			$stmt 	= $this->query($query, array($contractor_id),TRUE);
		
			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_total_work_volume($contractor_id){

		try
		{
			$fields 	= 	"*";
			

			//select query
			$query 	  = <<<EOS
				SELECT sum(amount)
					FROM (SELECT $fields
					FROM %s a
					WHERE a.contractor_id = ? 
					order by year desc limit 3) as total
EOS;

			$query 	= sprintf($query,CEIS_Model::tbl_contractor_work_volume); 
			$stmt 	= $this->query($query, array($contractor_id),TRUE);
		
			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_avg_work_volume($contractor_id){

		try
		{
			$fields 	= 	"*";
			

			//select query
			$query 	  = <<<EOS
				SELECT avg(amount)
					FROM (SELECT $fields
					FROM %s a
					WHERE a.contractor_id = ? 
					order by year desc limit 3) as total
EOS;

			$query 	= sprintf($query,CEIS_Model::tbl_contractor_work_volume); 
			$stmt 	= $this->query($query, array($contractor_id),TRUE);
		
			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_registration($contractor_id){

		try
		{
			$fields 		= "*";
			
			//select query
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a
					LEFT JOIN %s b
					ON a.contractor_id = b.contractor_id
					LEFT JOIN %s c
					ON a.registration_status_id = c.registration_status_id
					WHERE a.contractor_id = 1 order by a.registration_date desc limit 1;
EOS;

			$query 	= sprintf($query,CEIS_Model::tbl_contractor_registration, CEIS_Model::tbl_contractors, CEIS_Model::tbl_param_registration_status); 
			$stmt 	= $this->query($query, array($contractor_id),FALSE);
		
			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_workexperience_project_list($params){

		try{
			//columns you want to be selected
			$cColumns = array(
				'a.rqst_staff_proj_id',
				'a.rqst_staff_id',
				'a.project_title',
				'a.project_location',
				'a.owner',
				'a.positions',
			);
			//adding ',' between values
			$fields   = implode(',', $aColumns);

			//select query
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a
EOS;

			$query 	= sprintf($query,CEIS_Model::tbl_contractor_staff_projects); 
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_workexperience_services_list($params){

		try{
			//columns you want to be selected
			/*$aColumns = array(
				'a.rqst_contract_id',
				'a.foreign_principal_name',
				'a.nationality_code',
				'a.required_manpower',
				'a.mobilized_manpower',
				'a.onsite_manpower',
				'a.fees',
				'a.workers_salaries',
				'a.remarks',
			);*/

			//adding ',' between values
			$fields   = implode(',', $aColumns);

			$fields = str_replace(" , ", " ", implode(", ", $aColumns));
			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];

			//select query
			$query 	  = <<<EOS
				SELECT $fields
					FROM $this->tbl_contractor_services a
EOS;
			//setting datatable
			return $this->set_datatable($query, $params, $aColumns);
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_workexperience_remarks_list($params){

		try{
			//columns you want to be selected
			$aColumns = array(
				'a.task_remark_id',
				'a.contractor_task_id',
				'a.contractor_id',
				'a.remarks',
				'a.created_by',
				'a.created_date',
				'a.modified_by',
				'a.modified_date',
			);
			//adding ',' between values
			$fields   = implode(',', $aColumns);

			//select query
			$query 	  = <<<EOS
				SELECT $fields
					FROM $this->tbl_contractor_task_remarks a
EOS;
			//setting datatable
			return $this->set_datatable($query, $params, $aColumns);
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}


	public function get_checklist($contractor_type_id)
	{
		try
		{

			$fields = array("*");
			$where['contractor_type_id'] = $contractor_type_id;
			
			return $this->select_all($fields, CEIS_Model::tbl_checklist, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}				
	}
}

/* End of file Contractors_model.php */
/* Location: ./application/modules/ceis/models/Contractors_model.php */