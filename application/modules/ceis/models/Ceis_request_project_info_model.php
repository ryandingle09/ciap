<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_request_project_info_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	

	public function get_project_services($request_id)
	{
		try
		{
			$query = <<<EOS
				SELECT a.*,b.country_name
					FROM %s a
						LEFT JOIN %s b ON a.country_code = b.country_code
					WHERE request_id = ?
EOS;
			$query = sprintf($query, self::tbl_request_aut_services, self::tbl_countries);
				
			return $this->query($query, array($request_id), FALSE);
			//return $this->select_one($fields, self::tbl_request_aut_services, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
	}
	
	public function get_project_projects($request_id)
	{
		try
		{
			$query = <<<EOS
				SELECT a.*,b.country_name, c.nationality_name, d.contract_type_name
					FROM %s a
						LEFT JOIN %s b ON a.country_code = b.country_code
						LEFT JOIN %s c ON a.prime_contractor_nationality = c.nationality_code
						LEFT JOIN %s d ON a.contract_type_id = d.contract_type_id
					WHERE request_id = ?
EOS;
			$query = sprintf($query, self::tbl_request_aut_projects, self::tbl_countries, self::tbl_param_nationalities, self::tbl_param_contract_types);
			
			return $this->query($query, array($request_id), FALSE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
}

/* End of file Requests_model.php */
/* Location: ./application/modules/ceis/models/Requests_model.php */