<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Requests_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	public function get_request_list($aColumns, $bColumns, $params)
	{
		try
		{	
			
/* 			$fields = str_replace(" , ", " ", implode(", ", $aColumns));
			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
		
			
			//select query
			$query 	  = <<<EOS
				SELECT $fields
				FROM %s a
				JOIN %s b ON a.request_type_id    = b.request_type_id 
				JOIN %s c ON a.request_status_id  = c.request_status_id 
				LEFT JOIN %s d ON a.request_id    = d.request_id
				LEFT JOIN %s e ON a.request_id    = e.request_id
EOS;
			$query 	= sprintf($query,CEIS_Model::tbl_requests, CEIS_Model::tbl_param_request_types, CEIS_Model::tbl_param_request_status, CEIS_Model::tbl_request_aut_projects, CEIS_Model::tbl_request_aut_services); 
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
			
			
			
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns     = array(
					'CONCAT(a.first_name, " ", a.middle_name, " ", a.last_name) as staff_name',
					"( PERIOD_DIFF(DATE_FORMAT(NOW(),'%%Y%%m'), DATE_FORMAT(b.start_date,'%%Y%%m')) / 12 ) as no_years",
					"'Connected' as status", 'a.rqst_staff_id', 'a.staff_type'
			);
			 */
			// APPEARS ON TABLE
			// APPEARS ON TABLE
			//$bColumns = array('request_id', 'requested_date', 'request_type_name', 'reference_no', 'reference_name', 'reference_name', 'request_status');
			$query_params = array(ACTIVE_FLAG, $request_id);
			$fields    	  = implode(',', $aColumns);
			
			//select query
			$query 	  = <<<EOS
				SELECT $fields
				FROM %s a
				JOIN %s b ON a.request_type_id    = b.request_type_id 
				JOIN %s c ON a.request_status_id  = c.request_status_id 
				LEFT JOIN %s d ON a.request_id    = d.request_id
				LEFT JOIN %s e ON a.request_id    = e.request_id
EOS;
			$query 	= sprintf($query,CEIS_Model::tbl_requests, CEIS_Model::tbl_param_request_types, CEIS_Model::tbl_param_request_status, CEIS_Model::tbl_request_aut_projects, CEIS_Model::tbl_request_aut_services); 
				
			
			return $this->set_datatable($query, $aColumns, $bColumns, $params, array());
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	

	//Gets Country list from table param_countries
	public function get_request_type(){

		try{
			$fields = array("*");
			return $this->select_all($fields, CEIS_Model::tbl_requests);
		}

		catch(PDOException $e)
		{
			$this->rlog_error($e);	
			throw $e;
		}
	}

	public function insert_registration($params){
			
		try
		{
			$val 						= array();
			$val["request_id"]			= 1; //Sample data
			$val["registration_no"] 	= filter_var($params['registration_no'], FILTER_SANITIZE_STRING);
			$val["registration_date"] 	= filter_var($params['registration_date'], FILTER_SANITIZE_STRING);

			$this->insert_data(CEIS_Model::tbl_request_registration, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_order_of_payment_list($aColumns, $bColumns, $params)
	{
		try
		{	
			$cColumns = array("a.rqst_payment_id", 
							"a.request_id", 
							"a.op_date", 
							"a.op_amount", 
							"a.created_by", 
							"a.created_date", 
							"a.modified_by", 
							"a.modified_date");
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));
			$sWhere = $this->filtering($cColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
		
			
			//select query
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a
EOS;
			$query 	= sprintf($query,CEIS_Model::tbl_request_payments); 
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	

	public function get_finance_request_statements_list($aColumns, $bColumns, $params)
	{
		try
		{	
			$cColumns = array('a.request_id',
					'a.statement_date',
					'a.stmt_type',
					'a.total_current_assets',
					'a.total_non_current_assets',
					'a.total_current_liabilities',
					'a.long_term_liabilities',
					'a.networth',
					'a.gross_sales',
					'a.net_profit',
					'a.common_shares',
					'a.par_value',
					'a.created_by',
					'a.created_date',
					'a.modified_by',
					'a.modified_date',
					'a.exist_in_pcab'
					);
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));
			$sWhere = $this->filtering($cColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
		
			
			//select query
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a
EOS;
			$query 	= sprintf($query,CEIS_Model::tbl_request_statements); 
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	

	public function filtered_length($aColumns, $bColumns, $params, $function_name="get_request_list")
	{
		try
		{
			$this->{$function_name}($aColumns, $bColumns, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length($field="request_id", $table=CEIS_Model::tbl_requests)
	{
		try
		{
			$fields = array("COUNT(".$field.") cnt");
				
			return $this->select_one($fields, $table);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_requests_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("request_id" => $id);
				
			return $this->select_one($fields, CEIS_Model::tbl_requests, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}
	
	public function get_specific_order_payment($rqst_payment_id){

		try
		{
			$fields 			 		= array("*");
			$where['rqst_payment_id'] 	= $rqst_payment_id;
			
			return $this->select_one($fields, CEIS_Model::tbl_request_payments, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	/*
	 * Excepts a hash key 
	 */
	public function get_specific_request($where){

		try
		{
			$key = key($where);
			
			$fields = array('*');
				
			$query  = <<<EOS
				SELECT a.*, b.request_status as request_status_name
				FROM %s a
					JOIN %s b ON a.request_status_id = b.request_status_id
				WHERE %s = ?
EOS;
			$query  = sprintf($query, CEIS_Model::tbl_requests, CEIS_Model::tbl_param_request_status, $key);
				
			return $this->query($query, array($where[$key]), FALSE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	//Gets Country list from table param_contractor_categories
	public function get_category(){

		try{
			$fields = array("*");
			return $this->select_all($fields, CEIS_Model::tbl_contractor_categories);
		}

		catch(PDOException $e)
		{
			$this->rlog_error($e);	
			throw $e;
		}
	}

	public function get_contract_project_list($aColumns, $bColumns, $params, $request_id)
	{
		try
		{	
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
			$filter_params[] = $request_id;

			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a, %s b, %s c, %s d
					WHERE a.request_id = ?
					AND b.request_id = a.request_id
                    AND c.rqst_contract_id = b.rqst_contract_id
                    AND d.request_status_id = b.rqst_contract_id;
EOS;

			$query = sprintf($query,CEIS_Model::tbl_requests, CEIS_Model::tbl_request_contracts, CEIS_Model::tbl_request_projects, CEIS_Model::tbl_param_request_status);

			
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_request_payment_list($aColumns, $bColumns, $params)
	{
		try
		{	
			
			$cColumns = array(
					'a.rqst_payment_id',
					'a.request_id',
					'a.op_date',
					'a.op_amount',
					'a.created_by',
					'a.created_date',
					'a.modified_by',
					'a.modified_date',
					'b.coll_nature_code',
					'b.amount');
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($cColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
		
			
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a LEFT JOIN %s b ON a.rqst_payment_id = b.rqst_payment_id 
EOS;
			$query = sprintf($query, CEIS_Model::tbl_request_payments, CEIS_Model::tbl_request_payment_collections);
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_staffing_experience_list($aColumns, $bColumns, $params)
	{
		try
		{	
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
		
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a 
EOS;
			$query = sprintf($query, CEIS_Model::tbl_request_staff_experiences);
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_staffing_project_list($aColumns, $bColumns, $params)
	{
		try
		{	
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
		
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a 
EOS;
			$query = sprintf($query, CEIS_Model::tbl_request_staff_projects);
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_staffing_director_list($aColumns, $bColumns, $params, $staff_type)
	{
		try
		{	

			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
			$filter_params[] = $staff_type;

			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a 
					LEFT JOIN %s b
						ON a.rqst_staff_id = b.rqst_staff_id 
						WHERE a.staff_type 	 = ? 
EOS;

			$query    	= sprintf($query, CEIS_Model::tbl_request_staff, CEIS_Model::tbl_request_staff_projects);
			$stmt 		= $this->query($query, $filter_params);
			
			/*echo $query;*/
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}


	public function get_contract_service_list($aColumns, $bColumns, $params, $request_id)
	{
		try
		{	
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
			$filter_params[] = $request_id;
			$filter_params[] = $request_id;
			$query 	  = <<<EOS
				SELECT *
					FROM %s a, %s b, %s c, %s d
					WHERE a.request_id = ?
					AND b.request_id = ?
                    AND c.rqst_contract_id = b.rqst_contract_id
                    AND d.request_status_id = b.rqst_contract_id;
EOS;

			$query = sprintf($query,CEIS_Model::tbl_requests, CEIS_Model::tbl_request_contracts, CEIS_Model::tbl_request_services, CEIS_Model::tbl_param_request_status);

			
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_specific_request_contractor($request_id){

		try
		{
			$fields = array('*');
			
			$query  = <<<EOS
				SELECT a.*, b.contractor_category_name, c.license_category_name
				FROM %s a
					JOIN %s b ON a.contractor_category_id = b.contractor_category_id
					LEFT JOIN %s c ON a.license_category_code = c.license_category_code
				WHERE a.request_id = ?
EOS;
			$query  = sprintf($query, CEIS_Model::tbl_request_contractor, CEIS_Model::tbl_contractor_categories, CEIS_Model::tbl_param_license_category);
			
			return $this->query($query, array($request_id), FALSE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	
	public function get_request_license_classifications($request_id, $special_flag=0, $fields=array('*'))
	{
		try 
		{
			/* $where['request_id'] 	 = $request_id;
			$where['specialty_flag'] = $request_id;
				
			return $this->select_all($fields, CEIS_Model::tbl_request_license_classifications, $where); */
			$query = <<<EOS
				SELECT a.*, b.license_classification_name
				FROM %s a
					JOIN %s b ON a.license_classification_code = b.license_classification_code
				WHERE a.request_id 	   = ?
				AND   a.specialty_flag = ?
EOS;
			$query  = sprintf($query, CEIS_Model::tbl_request_license_classifications, CEIS_Model::tbl_param_license_classifications);
				
			return $this->query($query, array($request_id, $special_flag));
		}
		catch(PDOException $e)
		{
			throw $e;
		}
	}
	
	public function get_specific_contractor($contractor_id){

		try
		{
			$fields 			 	= array("*");
			$where['contractor_id'] = $contractor_id;
			
			return $this->select_one($fields, CEIS_Model::tbl_contractors, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	public function get_workexperience_project_list($params){

		try{
			//columns you want to be selected
			$cColumns = array(
				'a.rqst_staff_proj_id',
				'a.rqst_staff_id',
				'a.project_title',
				'a.project_location',
				'a.owner',
				'a.positions',
			);
			//adding ',' between values
			$fields   = implode(',', $aColumns);

			//select query
			$query 	  = <<<EOS
				SELECT $fields
					FROM %s a
EOS;

			$query 	= sprintf($query,CEIS_Model::tbl_request_staff_projects); 
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
			
		}
		catch(PDOException $e){
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_workexperience_services_list($params){

		try{
			//columns you want to be selected
			$aColumns = array(
				'a.rqst_contract_id',
				'a.foreign_principal_name',
				'a.nationality_code',
				'a.required_manpower',
				'a.mobilized_manpower',
				'a.onsite_manpower',
				'a.fees',
				'a.workers_salaries',
				'a.remarks',
			);

			//adding ',' between values
			$fields   = implode(',', $aColumns);

			$fields = str_replace(" , ", " ", implode(", ", $aColumns));
			$sWhere = $this->filtering($cColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];

			//select query
			$query 	  = <<<EOS
				SELECT $fields
					FROM $this->tbl_request_services a
EOS;
			//setting datatable
			return $this->set_datatable($query, $params, $aColumns);
			
		}
		catch(PDOException $e){
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_workexperience_remarks_list($params){

		try{
			//columns you want to be selected
			$aColumns = array(
				'a.task_remark_id',
				'a.request_task_id',
				'a.request_id',
				'a.remarks',
				'a.created_by',
				'a.created_date',
				'a.modified_by',
				'a.modified_date',
			);
			//adding ',' between values
			$fields   = implode(',', $aColumns);

			//select query
			$query 	  = <<<EOS
				SELECT $fields
					FROM $this->tbl_request_task_remarks a
EOS;
			//setting datatable
			return $this->set_datatable($query, $params, $aColumns);
			
		}
		catch(PDOException $e){
			$this->rlog_error($e);
			throw $e;
		}
	}


	public function get_checklist($request_type_id)
	{
		try
		{

			$fields = array("*");
			$where['request_type_id'] = $request_type_id;
			
			return $this->select_all($fields, CEIS_Model::tbl_checklist, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}				
	}
	
	public function get_request_task($where, $fields=array('*'))
	{
		try
		{
			return $this->select_one($fields, CEIS_Model::tbl_request_tasks, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
	}
	
	public function get_current_task($request_id)
	{
		try
		{
			$ongoing = REQUEST_TASK_ONGOING;
			$not_yet = REQUEST_TASK_NOT_YET_STARTED;
			$query   = <<<EOS
				SELECT *
					FROM %s a
					WHERE a.request_id = ?
						 AND a.task_status_id IN ($ongoing, $not_yet)
					ORDER BY a.sequence_no ASC
					LIMIT 1
EOS;
			$query  = sprintf($query, self::tbl_request_tasks);
	
			return $this->query($query, array($request_id), FALSE);
		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
}

/* End of file Requests_model.php */
/* Location: ./application/modules/ceis/models/Requests_model.php */