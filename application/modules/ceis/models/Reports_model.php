<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reports_model extends CEIS_Model
{
	public function __construct() {
		parent::__construct();	
	}

	//Gets Country list from table param_countries
	public function get_contractors(){

		try{
			$fields = array("*");
			

			return $this->select_all($fields, CEIS_Model::tbl_contractors);
		}

		catch(PDOException $e)
		{
			$this->rlog_error($e);	
			throw $e;
		}
	}

	public function get_foreign_exchange($year,$quarter){
	
		$cond 			= '';
		$contract_type 	= CONTRACT_TYPE_MANPOWER;
		$base 			= CONTRACT_OVERSEAS;


		if($quarter == 5 ){
			$cond = '(c.year = '.$year.')';
		}else{
			$cond = '((c.year <= '.$year.' AND c.quarter <= '.$quarter.') OR c.year < '.$year.')';
		}

			$query = <<<EOS
				SELECT a.contractor_name, a.contractor_id, SUM(e.remitt_amount) as remittances, SUM(d.onsite_manpower) as onsite, SUM(d.mob_manpower) as deployed FROM %s a
				LEFT JOIN %s b
					ON a.contractor_id = b.contractor_id
				LEFT JOIN %s c
					ON b.contract_id = c.contract_id
				LEFT JOIN %s d
					ON c.progress_report_id = d.progress_report_id
				LEFT JOIN %s e
					ON c.progress_report_id = e.progress_report_id
			WHERE b.contract_type = '$contract_type' AND $cond AND b.base = '$base'
			GROUP BY a.contractor_id;
EOS;


	$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_progress_report_info, CEIS_Model::tbl_progress_report_remittances);	
	return $this->query($query);
	}

	public function get_projects_new_contracts($year,$quarter){
		
		$cond = '';
		$contract_type 	= CONTRACT_TYPE_PROJECT;
		$base 			= CONTRACT_OVERSEAS;

		if($quarter == 5 ){
			$cond = '(d.year = '.$year.')';
		}else{
			$cond = '((d.year <= '.$year.' AND d.quarter <= '.$quarter.') OR d.year < '.$year.')';
		}

		$query = <<<EOS
			SELECT a.contractor_name, b.project_title, b.project_location, b.contract_cost , b.owner, b.contract_duration FROM %s a
				JOIN %s b
					ON a.contractor_id = b.contractor_id
				JOIN %s c 
					ON b.contract_id = c.contract_id
	        	LEFT JOIN %s d
	        		ON b.contract_id = d.contract_id
			WHERE b.contract_type = '$contract_type' AND $cond AND b.base = '$base';
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractor_projects, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);
	}

	public function get_services_new_contracts($year,$quarter){
		
		$cond = '';
		$contract_type 	= CONTRACT_TYPE_MANPOWER;
		$base 			= CONTRACT_OVERSEAS;

		if($quarter == 5 ){
			$cond = '(d.year = '.$year.')';
		}else{
			$cond = '((d.year <= '.$year.' AND d.quarter <= '.$quarter.') OR d.year < '.$year.')';
		}

		$query = <<<EOS
			SELECT a.contractor_name, b.project_title, f.country_name, b.province, b.owner,c.foreign_principal_name, e.req_manpower, e.mob_manpower, e.onsite_manpower  FROM %s a
		JOIN %s b
			ON a.contractor_id = b.contractor_id
	        JOIN %s c 
	        ON b.contract_id = c.contract_id
	        LEFT JOIN %s d
	        ON b.contract_id = d.contract_id
	        LEFT JOIN %s e
	        ON d.progress_report_id = e.progress_report_id
	        LEFT JOIN %s f
	        ON b.country_code = f.country_code
	        WHERE b.contract_type = '$contract_type' AND $cond AND '$base';
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractor_services, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_progress_report_info, CEIS_Model::tbl_countries);	
	return $this->query($query);
	}

	public function get_breakdown($year,$quarter){
		
		$cond = '';
		$contract_type 		= CONTRACT_TYPE_PROJECT;
		$base 				= CONTRACT_OVERSEAS;
		$contract_status 	= CONTRACT_STATUS_ONGOING;

		if($quarter == 5 ){
			$cond = '(c.year = '.$year.')';
		}else{
			$cond = '((c.year <= '.$year.' AND c.quarter <= '.$quarter.') OR c.year < '.$year.')';
		}
			 
			$query = <<<EOS
			SELECT a.country_code, a.country_name, b.contract_status_id, COUNT(b.project_title) as no_projects,SUM(b.contract_cost) as contract_cost, c.year FROM %s a
				JOIN %s b 
					ON a.country_code = b.country_code
				JOIN %s c
					ON b.contract_id = c.contract_id 
			WHERE b.contract_type = '$contract_type' AND b.contract_status_id = $contract_status AND $cond AND b.base = '$base'
			GROUP BY a.country_name;        
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_countries, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);
	}

	public function get_dist($year,$quarter){
			
			$cond = '';
			$contract_type 		= CONTRACT_TYPE_PROJECT;
			$base 				= CONTRACT_OVERSEAS;
			$contract_status 	= CONTRACT_STATUS_ONGOING;

			if($quarter == 5 ){
				$cond = '(c.year = '.$year.')';
			}else{
				$cond = '((c.year <= '.$year.' AND c.quarter <= '.$quarter.') OR c.year < '.$year.')';
			}

			$query = <<<EOS
		      SELECT *, SUM(tot_contract) as grand_total FROM (SELECT a.country_name, e.project_type_name, COUNT(b.project_title) as no_projects,b.contract_type, b.contract_status_id, SUM(b.contract_cost) as tot_contract, c.year 
				    FROM %s a
						LEFT JOIN %s b 
							ON a.country_code = b.country_code 
						JOIN %s c 
							ON b.contract_id = c.contract_id
						JOIN %s d
							ON b.contract_id = d.contract_id
						LEFT JOIN %s e
							ON d.project_type_code = e.project_type_code
					WHERE b.contract_type = '$contract_type' AND b.contract_status_id = $contract_status AND b.base='$base' AND $cond 
				    GROUP BY country_name, project_type_name) new_tot group by country_name,project_type_name;
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_countries, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_contractor_projects, CEIS_Model::tbl_project_types);	
	return $this->query($query);
	}

	public function get_manpower($year,$quarter){

			$cond = '';
			$contract_type 		= CONTRACT_TYPE_MANPOWER;
			$base 				= CONTRACT_OVERSEAS;

			if($quarter == 5 ){
				$cond = '(c.year = '.$year.')';
			}else{
				$cond = '((c.year <= '.$year.' AND c.quarter <= '.$quarter.') OR c.year < '.$year.')';
			}

			$query = <<<EOS

		    SELECT a.contractor_name, COUNT(b.project_title) as number_of_contract, SUM(d.onsite_manpower) as onsite_manpower  
		    FROM %s a
				LEFT JOIN %s b
					ON a.contractor_id = b.contractor_id
				LEFT JOIN %s c 
					ON b.contract_id=c.contract_id
				LEFT JOIN %s d
					ON c.progress_report_id = d.progress_report_id
			WHERE b.contract_type = '$contract_type' AND b.base = '$base' AND $cond
			GROUP BY a.contractor_id;	
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_progress_report_info);	
	return $this->query($query);
	}

	public function get_geo_region_proj_first($year,$quarter){
			
			$cond = '';
			$contract_type 				= CONTRACT_TYPE_PROJECT;
			$base 						= CONTRACT_OVERSEAS;
			$contract_status_ongoing 	= CONTRACT_STATUS_ONGOING;
			$contract_status_completed 	= CONTRACT_STATUS_COMPLETED;

			if($quarter == 5 ){
				$cond = '(c.year = '.$year.')';
			}else{
				$cond = '((c.year <= '.$year.' AND c.quarter <= '.$quarter.') OR c.year < '.$year.')';
			}

			$query = <<<EOS
			SELECT a.country_code, b.country_name, COUNT(*) as number_of_contract, SUM(d.onsite_manpower) as onsite_manpower 
			FROM %s a
				JOIN %s b
					ON a.country_code = b.country_code
				LEFT JOIN %s c
					ON a.contract_id = c.contract_id
				JOIN %s d
					ON c.progress_report_id = d.progress_report_id
				WHERE (a.contract_status_id = $contract_status_ongoing OR a.contract_status_id = $contract_status_completed) AND a.contract_type = '$contract_type' AND a.base= '$base' AND $cond
				GROUP BY a.country_code;
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_countries, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_progress_report_info);	
	return $this->query($query);

	}

	public function get_geo_region_serv_first($year,$quarter){
			
		$cond = '';
		$contract_type 				= CONTRACT_TYPE_MANPOWER;
		$base 						= CONTRACT_OVERSEAS;
		$contract_status_ongoing 	= CONTRACT_STATUS_ONGOING;
		$contract_status_completed 	= CONTRACT_STATUS_COMPLETED;

		if($quarter == 5 ){
			$cond = '(c.year = '.$year.')';
		}else{
			$cond = '((c.year <= '.$year.' AND c.quarter <= '.$quarter.') OR c.year < '.$year.')';
		}

		$query = <<<EOS
			SELECT a.country_code, b.country_name, COUNT(*) as number_of_contract, SUM(d.onsite_manpower) as onsite_manpower 
			FROM %s a
				JOIN %s b
					ON a.country_code = b.country_code
				LEFT JOIN %s c
					ON a.contract_id = c.contract_id
				JOIN %s d
					ON c.progress_report_id = d.progress_report_id
			WHERE (a.contract_status_id = $contract_status_ongoing OR a.contract_status_id = $contract_status_completed) AND a.contract_type = '$contract_type' AND a.base = '$base' AND $cond
			GROUP BY a.country_code;
EOS;
		

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_countries, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_progress_report_info);	
	return $this->query($query);

	}

	public function get_geo_region_second($year,$quarter){
			
		$cond = '';

		if($quarter == 5 ){
			$cond = '(d.year = '.$year.')';
		}else{
			$cond = '((d.year <= '.$year.' AND d.quarter <= '.$quarter.') OR d.year < '.$year.')';
		}

		$query = <<<EOS
			SELECT a.region_name, count(*) as no_of_contracts, SUM(onsite_manpower) as onsite_manpower 
			FROM %s a 
				JOIN %s b
					ON a.geo_region_id = b.geo_region_id
				JOIN %s c 
					ON b.country_code = c.country_code
				JOIN %s d 
					ON c.contract_id = d.contract_id 
				JOIN %s e 
					ON d.progress_report_id = e.progress_report_id
				WHERE $cond
			GROUP BY a.region_name;
EOS;
		

	$query	= sprintf($query, CEIS_Model::tbl_geo_regions, CEIS_Model::tbl_geo_region_countries, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_progress_report_info);	
	return $this->query($query);

	}
	
	public function get_report_overseas_project($year,$quarter){
			
		$cond = '';
		$contract_type 				= CONTRACT_TYPE_PROJECT;
		$base 						= CONTRACT_OVERSEAS;
		$contract_status_ongoing 	= CONTRACT_STATUS_ONGOING;

		if($quarter == 5 ){
			$cond = '(e.year = '.$year.')';
		}else{
			$cond = '((e.year <= '.$year.' AND e.quarter <= '.$quarter.') OR e.year < '.$year.')';
		}

		$query = <<<EOS
			SELECT a.contractor_name, b.project_title, d.project_type_name, b.project_location, b.contract_cost, b.owner, b.started_date, e.year, e.quarter
			FROM %s a
				JOIN %s b
					ON a.contractor_id = b.contractor_id
				JOIN %s c
					ON b.contract_id = c.contract_id
				JOIN %s d
					ON c.project_type_code = d.project_type_code
				JOIN %s e
					ON b.contract_id = e.contract_id
			WHERE b.contract_type = '$contract_type' AND b.contract_status_id = $contract_status_ongoing AND b.base = '$base' AND $cond;
EOS;
		

	$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractor_projects, CEIS_Model::tbl_project_types, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}

	public function get_report_overseas_service($year,$quarter){
			
		$cond = '';
		$contract_type 				= CONTRACT_TYPE_MANPOWER;
		$base 						= CONTRACT_OVERSEAS;
		$contract_status_ongoing 	= CONTRACT_STATUS_ONGOING;

		if($quarter == 5 ){
			$cond = '(d.year = '.$year.')';
		}else{
			$cond = '((d.year <= '.$year.' AND d.quarter <= '.$quarter.') OR d.year < '.$year.')';
		}

		$query = <<<EOS
		SELECT a.contractor_name, b.project_title, e.country_name, b.province, b.owner, c.foreign_principal_name, c.required_manpower, c.mobilized_manpower, c.onsite_manpower, d.year, d.quarter
		FROM %s a
			JOIN %s b
				ON a.contractor_id = b.contractor_id
			JOIN %s c
				ON b.contract_id = c.contract_id
			JOIN %s d
				ON b.contract_id = d.contract_id
			JOIN %s e
			ON b.country_code = e.country_code
		WHERE b.contract_type = '$contract_type' AND b.contract_status_id = $contract_status_ongoing AND b.base = '$base' AND $cond
		ORDER BY a.contractor_name;
EOS;
		

	$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractor_services, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_countries);	
	return $this->query($query);

	}

	public function get_reg_companies($year,$quarter){

		$query = <<<EOS

			SELECT COUNT(DISTINCT a.contractor_id) as reg_count FROM %s a
			WHERE (YEAR(a.registration_date) <= $year AND QUARTER(a.registration_date) <= $quarter) OR (YEAR(a.registration_date) < $year) ;
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_registration);	
	return $this->query($query);

	}
	
	public function get_active_companies($year,$quarter){

		$query = <<<EOS

			SELECT COUNT(DISTINCT a.contractor_id) as reg_count 
			FROM %s a
			WHERE ((YEAR(a.registration_date) <= $year AND QUARTER(a.registration_date) <= $quarter) OR (YEAR(a.registration_date) < $year)) AND a.registration_status_id = 1 ;
EOS;

		$query	= sprintf($query, CEIS_Model::tbl_contractor_registration);	
	return $this->query($query);

	}

	public function get_remittances_companies($year,$quarter){

		$query = <<<EOS

			SELECT SUM(remitt_amount)as remit_tot 
			FROM %s a
				JOIN %s b
					ON a.progress_report_id = b.progress_report_id 
			WHERE ((a.year <= $year AND a.quarter <= $quarter) OR a.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_progress_reports,CEIS_Model::tbl_progress_report_remittances);	
	return $this->query($query);

	}

	public function get_not_started_contract($year,$quarter){

		$query = <<<EOS

		SELECT COUNT(*) as not_yet_started_count_project 
		FROM %s a
			LEFT JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'M' AND a.contract_status_id = 2 AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}

	public function get_not_started_amount_project_contract($year,$quarter){

		$query = <<<EOS

		SELECT COUNT(*) as not_yet_started_count_project, SUM(a.contract_cost) as tot_amt_not_project 
		FROM %s a
			LEFT JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'P' AND a.contract_status_id = 2 AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}

	public function get_not_started_service_contract($year,$quarter){

		$query = <<<EOS

		SELECT COUNT(*) as not_yet_started_count_service 
		FROM %s a
			LEFT JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'M' AND a.contract_status_id = 2 AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}

	public function get_not_started_amount_service_contract($year,$quarter){

		$query = <<<EOS

		SELECT COUNT(*) as not_yet_started_count_service, SUM(a.contract_cost) as tot_amt_not_service 
		FROM %s a
			LEFT JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'M' AND a.contract_status_id = 2 AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}

	//ongoing

	public function get_ongoing_contract($year,$quarter){

		$query = <<<EOS

		SELECT COUNT(*) as ongoing_count 
		FROM %s a
			LEFT JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'P' AND a.contract_status_id = 5 AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}

	public function get_ongoing_amount_project_contract($year,$quarter){

		$query = <<<EOS

		SELECT COUNT(*) as ongoing_count, SUM(a.contract_cost) as ongoing_amt_project 
		FROM %s a
			LEFT JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'P' AND a.contract_status_id = 5 AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}

	public function get_ongoing_service_contract($year,$quarter){

		$query = <<<EOS

		SELECT COUNT(*) as ongoing_count_service 
		FROM %s a
			LEFT JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'M' AND a.contract_status_id = 5 AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}
	
	public function get_ongoing_amount_service_contract($year,$quarter){

		$query = <<<EOS

		SELECT COUNT(*) as ongoing_count_service, SUM(a.contract_cost) as ongoing_amt_service 
		FROM %s a
			LEFT JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'M' AND a.contract_status_id = 5 AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}

	//completed

	public function get_completed_contract($year,$quarter){

		$query = <<<EOS

		SELECT COUNT(*) as completed_count 
		FROM %s a
			LEFT JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'P' AND a.contract_status_id = 1 AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}

	public function get_completed_amount_project_contract($year,$quarter){

		$query = <<<EOS

		SELECT COUNT(*) as completed_count, SUM(a.contract_cost) as completed_amt_project 
		FROM %s a
			LEFT JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'P' AND a.contract_status_id = 1 AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}

	public function get_completed_service_contract($year,$quarter){

		$query = <<<EOS

		SELECT COUNT(*) as completed_count_service 
		FROM %s a
			LEFT JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'M' AND a.contract_status_id = 1 AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}
	
	public function get_completed_amount_service_contract($year,$quarter){

		$query = <<<EOS

		SELECT COUNT(*) as completed_count_service, SUM(a.contract_cost) as completed_amt_service 
		FROM %s a
			LEFT JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'M' AND a.contract_status_id = 1 AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}

	//terminated

	public function get_terminated_contract($year,$quarter){

		$query = <<<EOS

		SELECT COUNT(*) as terminated_count 
		FROM %s a
			LEFT JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'P' AND a.contract_status_id = 4 AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}

	public function get_terminated_amount_project_contract($year,$quarter){

		$query = <<<EOS

		SELECT COUNT(*) as terminated_count, SUM(a.contract_cost) as terminated_amt_project 
		FROM %s a
			LEFT JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'P' AND a.contract_status_id = 4 AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}

	public function get_terminated_service_contract($year,$quarter){

	$query = <<<EOS

	SELECT COUNT(*) as terminated_count_service 
	FROM %s a
			LEFT JOIN %s b
				ON a.contract_id = b.contract_id
	WHERE a.contract_type = 'M' AND a.contract_status_id = 4 AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}
	
	public function get_terminated_amount_service_contract($year,$quarter){

		$query = <<<EOS

		SELECT COUNT(*) as terminated_count_service, SUM(a.contract_cost) as terminated_amt_service 
		FROM %s a
			LEFT JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'M' AND a.contract_status_id = 4 AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year);
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}

	public function get_total_manpower_onsite($year,$quarter){

		$query = <<<EOS
		SELECT SUM(onsite_manpower)as total_manpower_onsite 
		FROM %s a
			JOIN %s b
				ON a.contract_id = b.contract_id
			JOIN %s c
				ON b.progress_report_id = c.progress_report_id
		WHERE a.contract_type = 'M' AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year)
		GROUP BY b.year AND b.quarter;
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_progress_report_info);	
	return $this->query($query);

	}

	public function get_total_manpower_deployed($year,$quarter){

		$query = <<<EOS
		SELECT SUM(mob_manpower) as total_manpower_deployed 
		FROM %s a
			JOIN %s b
				ON a.contract_id = b.contract_id
			JOIN %s c
				ON b.progress_report_id = c.progress_report_id
		WHERE a.contract_type = 'M' AND ((b.year <= $year AND b.quarter <= $quarter) OR b.year < $year)
		GROUP BY b.year AND b.quarter;
EOS;

	$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_progress_report_info);	
	return $this->query($query);

	}

	public function get_completed_service($year,$quarter){
			
		$cond = '';
		$contract_type 		= CONTRACT_TYPE_MANPOWER;
		$base 				= CONTRACT_OVERSEAS;
		$contract_status 	= CONTRACT_STATUS_COMPLETED;

		if($quarter == 5 ){
			$cond = '(d.year = '.$year.')';
		}else{
			$cond = '((d.year <= '.$year.' AND d.quarter <= '.$quarter.') OR d.year < '.$year.')';
		}

		$query = <<<EOS
			SELECT a.contractor_name, b.project_title, e.country_name, b.province, b.owner, c.foreign_principal_name, f.contract_status_name
			FROM %s a
				JOIN %s b
					ON a.contractor_id = b.contractor_id
				JOIN %s c
					ON b.contract_id = c.contract_id
				JOIN %s d
					ON b.contract_id = d.contract_id
				JOIN %s  e
					ON b.country_code = e.country_code
				JOIN %s f
					ON b.contract_status_id = f.contract_status_id
			WHERE b.contract_type = '$contract_type' AND f.contract_status_id = $contract_status AND b.base = '$base' AND $cond
			ORDER BY a.contractor_name;
EOS;
		

	$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractor_services, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_countries, CEIS_Model::tbl_param_contract_status);	
	return $this->query($query);

	}

	public function get_submitted_foreign_exchange($year,$quarter){
		$cond = '';

		if($quarter == 5 ){
			$cond = '(c.year = '.$year.')';
		}else{
			$cond = '((c.year <= '.$year.' AND c.quarter <= '.$quarter.') OR c.year < '.$year.')';
		}

		$query = <<<EOS
			SELECT COUNT(NULLIF(TRIM(remittances), '')) as companies_remitted_count , SUM(remittances) as total_remitted_earning
			FROM (
				SELECT a.contractor_name, a.contractor_id, SUM(e.remitt_amount) as remittances, SUM(d.onsite_manpower) as onsite, SUM(d.mob_manpower) as deployed FROM %s a
				LEFT JOIN %s b
					ON a.contractor_id = b.contractor_id
				JOIN %s c
					ON b.contract_id = c.contract_id
				JOIN %s d
					ON c.progress_report_id = d.progress_report_id
				JOIN %s e
					ON c.progress_report_id = e.progress_report_id
			WHERE b.contract_type = 'M' AND $cond
			GROUP BY a.contractor_id) remitted_number;
EOS;
		

	$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_progress_report_info, CEIS_Model::tbl_progress_report_remittances);	
	return $this->query($query);

	}

	public function get_total_foreign_exchange($year,$quarter){
		$cond = '';

		if($quarter == 5 ){
			$cond = '(c.year = '.$year.')';
		}else{
			$cond = '((c.year <= '.$year.' AND c.quarter <= '.$quarter.') OR c.year < '.$year.')';
		}

		$query = <<<EOS
			SELECT COUNT(*) as all_companies_remitted_count , SUM(remittances) as all_total_earning
			FROM (	SELECT a.contractor_name, a.contractor_id, SUM(e.remitt_amount) as remittances, SUM(d.onsite_manpower) as onsite, SUM(d.mob_manpower) as deployed FROM %s a
						LEFT JOIN %s b
							ON a.contractor_id = b.contractor_id
						LEFT JOIN %s c
							ON b.contract_id = c.contract_id
						LEFT JOIN %s d
							ON c.progress_report_id = d.progress_report_id
						LEFT JOIN %s e
							ON c.progress_report_id = e.progress_report_id
					WHERE b.contract_type = 'M' AND $cond
					GROUP BY a.contractor_id) total_number;
EOS;
		

	$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_progress_report_info, CEIS_Model::tbl_progress_report_remittances);	
	return $this->query($query);

	}

	public function get_project_new_contract_memorandum($year,$quarter){
		$cond = '';

		if($quarter == 5 ){
			$cond = '(d.year = '.$year.')';
		}else{
			$cond = '((d.year <= '.$year.' AND d.quarter <= '.$quarter.') OR d.year < '.$year.')';
		}

		$query = <<<EOS
		SELECT COUNT(*) as project_contract_count 
		FROM (	SELECT a.contractor_name, b.project_title, b.project_location, b.contract_cost , b.owner, b.contract_duration FROM %s a
					JOIN %s b
						ON a.contractor_id = b.contractor_id
					JOIN %s c 
						ON b.contract_id = c.contract_id
	        		LEFT JOIN %s d
	        			ON b.contract_id = d.contract_id
				WHERE b.contract_type = 'P' AND $cond) total ;
EOS;
		$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractor_projects, CEIS_Model::tbl_progress_reports);	
		return $this->query($query);
	}
	
	public function get_service_new_contract_memorandum($year,$quarter){
		$cond = '';

		if($quarter == 5 ){
			$cond = '(d.year = '.$year.')';
		}else{
			$cond = '((d.year <= '.$year.' AND d.quarter <= '.$quarter.') OR d.year < '.$year.')';
		}

		$query = <<<EOS
		SELECT COUNT(*) as service_contract_count 
		FROM (	SELECT a.contractor_name, b.project_title, b.project_location, b.contract_cost , b.owner, b.contract_duration FROM %s a
					JOIN %s b
						ON a.contractor_id = b.contractor_id
					JOIN %s c 
						ON b.contract_id = c.contract_id
	        		LEFT JOIN %s d
	        			ON b.contract_id = d.contract_id
				WHERE b.contract_type = 'M' AND $cond) total ;
EOS;
		$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractor_projects, CEIS_Model::tbl_progress_reports);	
		return $this->query($query);
	}

	public function get_service_new_contract_reg_memorandum($year,$quarter){
		$cond = '';

		if($quarter == 5 ){
			$cond = '(d.year = '.$year.')';
		}else{
			$cond = '((d.year <= '.$year.' AND d.quarter <= '.$quarter.') OR d.year < '.$year.')';
		}

		$query = <<<EOS
		SELECT COUNT(*) as reg_service_contract_count 
		FROM (	SELECT a.contractor_name, a.contractor_id, b.project_title, b.project_location, b.contract_cost , b.owner, b.contract_duration FROM %s a
					JOIN %s b
						ON a.contractor_id = b.contractor_id
					JOIN %s c 
						ON b.contract_id = c.contract_id
	        		LEFT JOIN %s d
	        			ON b.contract_id = d.contract_id
				WHERE b.contract_type = 'M' AND $cond 
				GROUP BY a.contractor_id) total ;
EOS;
		$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractor_projects, CEIS_Model::tbl_progress_reports);	
		return $this->query($query);
	}

	public function get_manpower_completed_contract_memorandum($year,$quarter){
		$cond = '';

		if($quarter == 5 ){
			$cond = '(b.year = '.$year.')';
		}else{
			$cond = '((b.year <= '.$year.' AND b.quarter <= '.$quarter.') OR b.year < '.$year.')';
		}

		$query = <<<EOS
		SELECT COUNT(*) as completed_contract 
		FROM %s a
			JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'M' AND a.contract_status_id = 1 AND $cond; 
EOS;
		$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts,  CEIS_Model::tbl_progress_reports);	
		return $this->query($query);
	}

	public function get_project_completed_contract_memorandum($year,$quarter){
		$cond = '';

		if($quarter == 5 ){
			$cond = '(b.year = '.$year.')';
		}else{
			$cond = '((b.year <= '.$year.' AND b.quarter <= '.$quarter.') OR b.year < '.$year.')';
		}

		$query = <<<EOS
		SELECT COUNT(*) as completed_contract 
		FROM %s a
			JOIN %s b
				ON a.contract_id = b.contract_id
		WHERE a.contract_type = 'P' AND a.contract_status_id = 1 AND $cond; 
EOS;
		$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts,  CEIS_Model::tbl_progress_reports);	
		return $this->query($query);
	}
	
	public function get_manpower_deployed_memorandum($year,$quarter){
		$cond = '';

		if($quarter == 5 ){
			$cond = '(a.year = '.$year.')';
		}else{
			$cond = '((a.year <= '.$year.' AND a.quarter <= '.$quarter.') OR a.year < '.$year.')';
		}

		$query = <<<EOS
		SELECT SUM(b.mob_manpower) as total_workers
		FROM %s a
			JOIN %s b
				ON a.progress_report_id = b.progress_report_id
		WHERE $cond;

EOS;
		$query	= sprintf($query, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_progress_report_info);	
		return $this->query($query);
	}

	public function get_workers_count_memorandum($year,$quarter){
		$cond = '';

		if($quarter == 5 ){
			$cond = '(b.year = '.$year.')';
		}else{
			$cond = '((b.year <= '.$year.' AND b.quarter <= '.$quarter.') OR b.year < '.$year.')';
		}

		$query = <<<EOS
		SELECT SUM(onsite_manpower) as worker 
		FROM progress_report_info a
			LEFT JOIN progress_reports b
				ON a.progress_report_id = b.progress_report_id
		WHERE $cond;

EOS;
		$query	= sprintf($query, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_progress_report_info);	
		return $this->query($query);
	}

	
	public function get_specific_contractor($contractor_name)
	{
		try
		{	
			$fields = '*';
			// $where	= array("contr_staff_id" => $contr_staff_id);

			$query 	= <<<EOS

			SELECT * 
			FROM contractor
			WHERE contractor_id = ?
EOS;

			$query = sprintf($query,CEIS_Model::tbl_contractors);
			$stmt 	= $this->query($query, array($contractor_name),FALSE);

			return $stmt;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_skill_summary_list($year, $contractor_id)
	{
		try
		{	
			if($contractor_id == 0){
				$where = '';
			}
			else
				$where	= 'AND a.contractor_id = '.$contractor_id;
			$query 	= <<<EOS

			SELECT *, MAX(g.salary_in_usd) as max_sal, MIN(g.salary_in_usd) as min_sal, AVG(g.salary_in_usd) as avg_sal, count(*) as total_number
			FROM contractor a
				LEFT JOIN contractor_contracts b 
					ON a.contractor_id = b.contractor_id 
				JOIN param_countries c 
					ON b.country_code = c.country_code 
				LEFT JOIN contractor_license_classifications d 
					ON b.contractor_id = d.contractor_id 
				JOIN param_license_classifications e 
					ON d.license_classification_code = e.license_classification_code
				JOIN progress_reports f 
			        ON b.contract_id = f.contract_id
		        LEFT JOIN progress_report_manpower g
	            	ON f.progress_report_id = g.progress_report_id
				WHERE f.year = $year $where
			GROUP BY c.country_code, e.license_classification_code

EOS;

			$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_countries, CEIS_Model::tbl_contractor_license_classifications, CEIS_Model::tbl_param_license_classifications, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_progress_report_manpower );	
			return $this->query($query);

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_sum_country_onsite($year,$quarter)
	{
		try
		{	
			$cond = '';

			if($quarter == 5 ){
				$cond = '(b.year = '.$year.')';
			}else{
				$cond = '((b.year <= '.$year.' AND b.quarter <= '.$quarter.') OR b.year < '.$year.')';
			}

			$query 	= <<<EOS

			SELECT SUM(sum_onsite) as grand_total 
			FROM (	SELECT sum(onsite_manpower) as sum_onsite FROM %s a 
						JOIN %s b 
							ON a.contract_id = b.contract_id
						JOIN %s c 
							ON b.progress_report_id = c.progress_report_id
						JOIN %s d 
							ON a.country_code = d.country_code
						LEFT JOIN %s e 
							ON d.geo_region_id = e.geo_region_id 
						WHERE a.contract_type = 'M' AND $cond
					GROUP BY e.geo_region_id LIMIT 2) granded;
EOS;

			$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_progress_report_info, CEIS_Model::tbl_geo_region_countries, CEIS_Model::tbl_geo_regions);	
			return $this->query($query);

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_ind_country_onsite($year,$quarter)
	{
		try
		{	
			$cond = '';

			if($quarter == 5 ){
				$cond = '(b.year = '.$year.')';
			}else{
				$cond = '((b.year <= '.$year.' AND b.quarter <= '.$quarter.') OR b.year < '.$year.')';
			}

			$query 	= <<<EOS

			SELECT sum(onsite_manpower) as sum_onsite, e.region_name
			FROM %s a 
				JOIN %s b 
					ON a.contract_id = b.contract_id
				JOIN %s c 
					ON b.progress_report_id = c.progress_report_id
				JOIN %s d 
					ON a.country_code = d.country_code
				LEFT JOIN %s e 
					ON d.geo_region_id = e.geo_region_id 
			WHERE a.contract_type = 'M'
			GROUP BY e.geo_region_id 
			ORDER BY sum_onsite DESC LIMIT 2;
EOS;

			$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_progress_report_info, CEIS_Model::tbl_geo_region_countries, CEIS_Model::tbl_geo_regions);	
			return $this->query($query);

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_companies_outstanding_count($year,$quarter)
	{
		try
		{	$cond = '';

			if($quarter == 5 ){
				$cond = '(b.year = '.$year.')';
			}else{
				$cond = '((b.year <= '.$year.' AND b.quarter <= '.$quarter.') OR b.year < '.$year.')';
			}

			$query 	= <<<EOS

			SELECT DISTINCT count(*) as project_count
			FROM %s a
			JOIN %s b
			ON a.contract_id = b.contract_id
			WHERE a.contract_status_id = 5
			GROUP BY contractor_id;
EOS;

			$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_progress_reports);	
			return $this->query($query);

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_project_outstanding_count($year,$quarter)
	{
		try
		{	
			$cond = '';

			if($quarter == 5 ){
				$cond = '(c.year = '.$year.')';
			}else{
				$cond = '((c.year <= '.$year.' AND c.quarter <= '.$quarter.') OR c.year < '.$year.')';
			}

			$query 	= <<<EOS

			SELECT COUNT(*) as project_company_count
			FROM (	SELECT a.* 
					FROM %s a
						LEFT JOIN %s b 
							ON a.contractor_id = b.contractor_id
						LEFT JOIN %s c
							ON a.contract_id = c.contract_id
						WHERE a.contract_type = 'P' AND a.contract_status_id = 5 AND $cond
					GROUP BY a.contractor_id) companies_project_count ;  
EOS;

			$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractors, CEIS_Model::tbl_progress_reports);	
			return $this->query($query);

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_service_outstanding_count($year,$quarter)
	{
		try
		{	
			$cond = '';

			if($quarter == 5 ){
				$cond = '(c.year = '.$year.')';
			}else{
				$cond = '((c.year <= '.$year.' AND c.quarter <= '.$quarter.') OR c.year < '.$year.')';
			}

			$query 	= <<<EOS

			SELECT COUNT(*) as service_company_count
			FROM (	SELECT a.* 
					FROM %s a
						LEFT JOIN %s b 
							ON a.contractor_id = b.contractor_id
						LEFT JOIN %s c 
							ON a.contract_id = c.contract_id
						WHERE a.contract_type = 'M' AND a.contract_status_id = 5 AND $cond
					GROUP BY a.contractor_id) companies_service_count ;  
EOS;

			$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractors, CEIS_Model::tbl_progress_reports);	
			return $this->query($query);

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_project_consultant_outstanding_count($year,$quarter)
	{
		try
		{	
			$cond = '';

			if($quarter == 5 ){
				$cond = '(c.year = '.$year.')';
			}else{
				$cond = '((c.year <= '.$year.' AND c.quarter <= '.$quarter.') OR c.year < '.$year.')';
			}

			$query 	= <<<EOS

			SELECT COUNT(*) as project_consultant_count 
			FROM %s a
				LEFT JOIN %s b 
					ON a.contractor_id = b.contractor_id
				LEFT JOIN %s c
					ON a.contract_id = c.contract_id
			WHERE a.contract_type = 'P' AND a.contract_status_id = 5 AND $cond
			GROUP BY a.contractor_id;
EOS;

			$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractors, CEIS_Model::tbl_progress_reports);	
			return $this->query($query);

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
		public function get_service_consultant_outstanding_count($year,$quarter)
	{
		try
		{	
			$cond = '';

			if($quarter == 5 ){
				$cond = '(c.year = '.$year.')';
			}else{
				$cond = '((c.year <= '.$year.' AND c.quarter <= '.$quarter.') OR c.year < '.$year.')';
			}

			$query 	= <<<EOS

			SELECT COUNT(*) as service_consultant_count 
			FROM %s a
				LEFT JOIN %s b 
					ON a.contractor_id = b.contractor_id
				LEFT JOIN %s c
					ON a.contract_id = c.contract_id
			WHERE a.contract_type = 'M' AND a.contract_status_id = 5;
			GROUP BY a.contractor_id;
EOS;

			$query	= sprintf($query, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractors, CEIS_Model::tbl_progress_reports);	
			return $this->query($query);

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_project_ongoing_count($year,$quarter){
			
		$cond = '';

		if($quarter == 5 ){
			$cond = '(e.year = '.$year.')';
		}else{
			$cond = '((e.year <= '.$year.' AND e.quarter <= '.$quarter.') OR e.year < '.$year.')';
		}

		$query = <<<EOS
			SELECT total_sum 
			FROM (	SELECT DISTINCT count(*), a.contractor_name, a.contractor_id, b.project_title, d.project_type_name, b.project_location, SUM(b.contract_cost) as total_sum , b.owner, b.started_date, e.year, e.quarter
					FROM %s a
						JOIN %s b
							ON a.contractor_id = b.contractor_id
						JOIN %s c
							ON b.contract_id = c.contract_id
						JOIN %s d
							ON c.project_type_code = d.project_type_code
						JOIN %s e
							ON b.contract_id = e.contract_id
					WHERE b.contract_type = 'P' AND b.contract_status_id = 5 AND $cond ) grand_tot;
EOS;
		
	$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractor_projects, CEIS_Model::tbl_project_types, CEIS_Model::tbl_progress_reports);	
	return $this->query($query);

	}

/*	public function get_service_ongoing_count($year,$quarter){
			
		$cond = '';

		if($quarter == 5 ){
			$cond = '(d.year = '.$year.')';
		}else{
			$cond = '((d.year <= '.$year.' AND d.quarter <= '.$quarter.') OR d.year < '.$year.')';
		}

		$query = <<<EOS
		SELECT total_sum_service FROM (
			SELECT DISTINCT count(*), a.contractor_name, a.contractor_id, b.project_title, d.project_type_name, b.project_location, SUM(b.contract_cost) as total_sum , b.owner, b.started_date, e.year, e.quarter
					FROM %s a
						JOIN %s b
							ON a.contractor_id = b.contractor_id
						JOIN %s c
							ON b.contract_id = c.contract_id
						JOIN %s d
							ON c.project_type_code = d.project_type_code
						JOIN %s e
							ON b.contract_id = e.contract_id
					WHERE b.contract_type = 'M' AND b.contract_status_id = 5 AND $cond ) grand_tot;
EOS;
		

	$query	= sprintf($query, CEIS_Model::tbl_contractors, CEIS_Model::tbl_contractor_contracts, CEIS_Model::tbl_contractor_services, CEIS_Model::tbl_progress_reports, CEIS_Model::tbl_countries);	
	return $this->query($query);

	}*/
	
}



/* End of file Reports_model.php */
/* Location: ./application/modules/ceis/models/Reports_model.php */