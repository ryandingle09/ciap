<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Geographical_distribution_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}		
	
	public function get_geographical_distribution_list($aColumns, $bColumns, $params)
	{
		try
		{	
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$cColumns = array(
				"a.geo_region_id", 
				"a.region_name", 
				"a.created_by", 
				"a.created_date", 
				"a.modified_by", 
				"a.modified_date");

			$sWhere = $this->filtering($cColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
		
			$query 	  = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields
					FROM %s a 
					LEFT JOIN %s b
					ON a.geo_region_id = b.geo_region_id
					LEFT JOIN %s c
					ON a.created_by = c.employee_no
					LEFT JOIN %s d
					ON a.modified_by = d.employee_no
					$filter_str
				GROUP BY a.geo_region_id
	        	$sOrder
	        	$sLimit;
EOS;
			$query = sprintf($query, CEIS_Model::tbl_geo_regions, CEIS_Model::tbl_geo_region_countries, CEIS_Model::tbl_users, CEIS_Model::tbl_users);
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_geographical_distribution_list($aColumns, $bColumns, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(geo_region_id) cnt");
				
			return $this->select_one($fields, CEIS_Model::tbl_geo_regions);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	public function get_geographical_distributions_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("geo_region_id" => $id);
				
			return $this->select_one($fields, CEIS_Model::tbl_geo_region_countries, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}

	public function get_specific_geographical_distribution($where){

		try
		{
			$fields = array("*");
			
			return $this->select_one($fields, CEIS_Model::tbl_geo_regions, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	//Gets Country list from table param_countries
	public function get_countries(){

		try{
			return $this->select_all("*", CEIS_Model::tbl_countries);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_selected_geo_list($where)
	{
		try
		{
			return $this->select_all("*", CEIS_Model::tbl_geo_region_countries, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}				
	}

	public function insert_geographical_distribution($params){
			
		try
		{
			$val 					= array();
			$val["region_name"] 	= filter_var($params['region_name'], FILTER_SANITIZE_STRING);
			$val["created_by"] 		= $this->session->userdata("employee_no");
			$val["created_date"] 	= date('Y-m-d H:i:s');

			$id = $this->insert_data(CEIS_Model::tbl_geo_regions, $val, TRUE);

			foreach ($params['country_name'] as $value):
				$val	 				= array();
				$val["geo_region_id"]	= $id;
				$val["country_code"] 	= $value;
				$this->insert_data(CEIS_Model::tbl_geo_region_countries, $val);
			endforeach;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function delete_geographical_distribution($geo_region_id)
	{
		try
		{			
			$this->delete_data(CEIS_Model::tbl_geo_region_countries, array('geo_region_id'=>$geo_region_id));
			$this->delete_data(CEIS_Model::tbl_geo_regions, array('geo_region_id'=>$geo_region_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function update_geographical_distribution($params)
	{
		try
		{
			$geo_region_id 				= filter_var($params["geo_region_id"], FILTER_SANITIZE_STRING);

			$val 						= array();			
			$val["region_name"] 		= filter_var($params['region_name'], FILTER_SANITIZE_STRING);
			$val["modified_by"] 		= $this->session->userdata("employee_no");
			$val["modified_date"]		= date('Y-m-d H:i:s');
			
			$where 						= array();
			$where["geo_region_id"]		= $geo_region_id;
			
			$this->update_data(CEIS_Model::tbl_geo_regions, $val, $where);
			$this->delete_data(CEIS_Model::tbl_geo_region_countries, array('geo_region_id'=>$geo_region_id));
			
			foreach ($params['country_name'] as $value):
				$val	 				= array();
				$val["geo_region_id"]	= $geo_region_id;
				$val["country_code"] 	= $value;
				$this->insert_data(CEIS_Model::tbl_geo_region_countries, $val);
			endforeach;

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
}

/* End of file Geographical_distribution_model.php */
/* Location: ./application/modules/ceis/models/Geographical_distribution_model.php */