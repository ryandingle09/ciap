<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_params_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	public function get_param_checklist($where, $fields=array('*'))
	{
		try
		{
			return $this->select_all($fields, self::tbl_checklist, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}				
	}
	
	public function get_param_task_status($where=array(), $fields=array('*'))
	{
		try
		{
			return $this->select_all( $fields , self::tbl_param_request_task_status, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_param_request_status($where=array(), $fields=array('*'))
	{
		try
		{
			return $this->select_all( $fields , self::tbl_param_request_status, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
}

/* End of file Requests_model.php */
/* Location: ./application/modules/ceis/models/Requests_model.php */