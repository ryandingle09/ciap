<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_request_registration_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	public function get_registration_details($request_id){
		try
		{
			
			return $this->select_one(array('*'), CEIS_Model::tbl_request_registration, array('request_id' => $request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function insert_request_registration($params){
			
		try
		{
			$val = array();
			$val["request_id"]			= $params['request_id'];
			$val["registration_no"] 	= filter_var($params['registration_no'], FILTER_SANITIZE_NUMBER_INT);
			$val["registration_date"] 	= date('Y-m-d', strtotime($params['registration_date']));

			$this->insert_data(CEIS_Model::tbl_request_registration, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	public function update_request_registration($params){
			
		try
		{
			$val = array();
			$val["registration_no"] 	= filter_var($params['registration_no'], FILTER_SANITIZE_NUMBER_INT);
			$val["registration_date"] 	= date('Y-m-d', strtotime($params['registration_date']));
			
			$this->update_data(CEIS_Model::tbl_request_registration, $val, array('request_id' => $params['request_id']));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}



}

/* End of file Requests_model.php */
/* Location: ./application/modules/ceis/models/Requests_model.php */