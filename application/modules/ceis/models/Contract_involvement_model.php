<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Contract_involvement_model extends CEIS_Model
{
	public function __construct() {
		parent::__construct();	
	}

	public function get_contract_involvement_list($aColumns, $bColumns, $params){

		try{
			
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($aColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
		
			
			$query 	  = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields from %s a
					LEFT JOIN %s b
					ON a.created_by = b.employee_no
					LEFT JOIN %s c
					ON a.modified_by = c.employee_no
					$filter_str
				GROUP BY a.involvement_code
	        	$sOrder
	        	$sLimit;
EOS;
			$query = sprintf($query, CEIS_Model::tbl_contract_involvement, CEIS_Model::tbl_users, CEIS_Model::tbl_users);
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function total_length()
	{
		try
		{
			$fields = array("COUNT(involvement_code) cnt");
				
			return $this->select_one($fields, CEIS_Model::tbl_contract_involvement);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_contract_involvement_list($aColumns, $bColumns, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_contract_involvement_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("involvement_code" => $id);
				
			return $this->select_one($fields, CEIS_Model::tbl_contract_involvement, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}

	public function get_specific_contract_invovement($where){

		try
		{
			$fields = array("*");
			
			
			return $this->select_one($fields, CEIS_Model::tbl_contract_involvement, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function delete_contract_involvement($involvement_code)
	{
		try
		{			
			$this->delete_data(CEIS_Model::tbl_contract_involvement, array('involvement_code'=>$involvement_code));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function insert_contract_involvement($params){
			
		try
		{
			$val 						= array();
			$val["involvement_code"] 	= filter_var($params['involvement_code'], FILTER_SANITIZE_STRING);
			$val["involvement"] 		= filter_var($params['involvement_name'], FILTER_SANITIZE_STRING);
			$val["created_by"] 			= $this->session->userdata("employee_no");
			$val["created_date"] 		= date('Y-m-d H:i:s');

			$this->insert_data(CEIS_Model::tbl_contract_involvement, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function update_contract_involvement($params)
	{
		try
		{
			$involvement_code 				= $params["involvement_code"];
			$val 							= array();			
			$val["involvement"] 			= filter_var($params['involvement_name'], FILTER_SANITIZE_STRING);
			$val["modified_by"] 			= $this->session->userdata("employee_no");
			$val["modified_date"]			= date('Y-m-d H:i:s');
			
			$where 							= array();
			$where["involvement_code"]		= $involvement_code;

			$this->update_data(CEIS_Model::tbl_contract_involvement, $val, $where);

		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
}

/* End of file Contract_involvement_model.php */
/* Location: ./application/modules/ceis/models/Contract_involvement_model.php */