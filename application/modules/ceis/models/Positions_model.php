<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Positions_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
		
	public function get_position_list($aColumns, $bColumns, $params)
	{
		try
		{	
			
			$cColumns = array(
				"a.position_id", 
				"a.position_name", 
				"a.created_by", 
				"a.created_date", 
				"a.modified_by", 
				"a.modified_date");
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($cColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
		
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields from %s a
					LEFT JOIN %s b
					ON a.created_by = b.employee_no
					LEFT JOIN %s c
					ON a.modified_by = c.employee_no
				$filter_str
				GROUP BY a.position_id
	        	$sOrder
	        	$sLimit;
EOS;
			$query	= sprintf($query, CEIS_Model::tbl_positions, CEIS_Model::tbl_users, CEIS_Model::tbl_users);	
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_position_list($aColumns, $bColumns, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(position_id) cnt");
				
			return $this->select_one($fields, CEIS_Model::tbl_positions);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_position($position_id)
	{
		try
		{			
			$this->delete_data(CEIS_Model::tbl_positions, array('position_id'=>$position_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_positions_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("position_id" => $id);
				
			return $this->select_one($fields, CEIS_Model::tbl_positions, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}
	
	public function insert_positions($params){
			
		try
		{
			$val 					= array();
			$val["position_name"] 	= filter_var($params['position_name'], FILTER_SANITIZE_STRING);
			$val["created_by"] 		= $this->session->userdata("employee_no");
			$val["created_date"] 	= date('Y-m-d H:i:s');

			$this->insert_data(CEIS_Model::tbl_positions, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}


	public function get_specific_position($where){

		try
		{
			$fields = array("*");
			
			return $this->select_one($fields, CEIS_Model::tbl_positions, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function update_position($params)
	{
		try
		{
			$position_id 				= $params["position_id"];
			$val 						= array();			
			$val["position_name"] 		= filter_var($params['position_name'], FILTER_SANITIZE_STRING);
			$val["modified_by"] 		= $this->session->userdata("employee_no");
			$val["modified_date"]		= date('Y-m-d H:i:s');
			
			$where 						= array();
			$where["position_id"]		= $position_id;

			$this->update_data(CEIS_Model::tbl_positions, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
}

/* End of file Position_model.php */
/* Location: ./application/modules/ceis/models/Position_model.php */