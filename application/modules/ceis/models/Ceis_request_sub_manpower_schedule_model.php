<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_request_sub_manpower_schedule_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	
	public function get_specific_schedule($rqst_sched_id)
	{
		try
		{
			return $this->select_one(array('*'), CEIS_Model::tbl_request_schedule, array('rqst_sched_id' => $rqst_sched_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_request($request_id)
	{
		try
		{
			return $this->select_one(array('*'), CEIS_Model::tbl_requests, array('request_id' => $request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	/*public function get_order_of_payment_header($where)
	{
		try
		{
			return $this->select_one(array('*'), CEIS_Model::tbl_request_payments, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}*/
	
	/*public function get_order_of_payment_details($where)
	{
		try
		{
			return $this->select_all(array('*'), CEIS_Model::tbl_request_payment_collections, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}*/

	/*public function insert_order_of_payment($params){
			
		try
		{
			$val = array();
			$val["request_id"]	 = $params['request_id'];
			$val["op_amount"] 	 = filter_var($params['total_amount'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$val["op_date"] 	 = date('Y-m-d', strtotime($params['op_date']));
			$val['created_date'] = date('Y-m-d H:i:s');
			$val['created_by']   = $params['user_id'];

			return $this->insert_data(CEIS_Model::tbl_request_payments, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}*/
	
	/*public function update_order_of_payment($params){
			
		try
		{
			$val = array();
			
			if(ISSET($params['total_amount']))
				$val["op_amount"] = filter_var($params['total_amount'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			
			if(ISSET($params['op_date']))
				$val["op_date"]   = date('Y-m-d', strtotime($params['op_date']));
			
			if(ISSET($params['or_date']))
				$val["or_date"]   = date('Y-m-d', strtotime($params['or_date']));
						
			if(ISSET($params['or_amount']))
				$val["or_amount"] = filter_var($params['or_amount'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			
			if(ISSET($params['or_no']))
				$val["or_number"] = $params['or_no'];
				
			$val['modified_date'] = date('Y-m-d H:i:s');
			$val['modified_by']   = $params['user_id'];
	
			return $this->update_data(CEIS_Model::tbl_request_payments, $val, array('rqst_payment_id' => $params['rqst_payment_id']));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}*/
	
	/*public function delete_order_of_payment_details($rqst_payment_id){
			
		try
		{
			return $this->delete_data(CEIS_Model::tbl_request_payment_collections,  array('rqst_payment_id' => $rqst_payment_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}*/
	

	/*public function delete_order_of_payment($rqst_payment_id){
			
		try
		{
			return $this->delete_data(CEIS_Model::tbl_request_payments,  array('rqst_payment_id' => $rqst_payment_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}*/
	
	
	/*public function insert_order_of_payment_details($params){
			
		try
		{
			$val = array();
			
			foreach($params['collection'] as $key => $value):
				$val[] = array('rqst_payment_id' => $params['rqst_payment_id'], 'coll_nature_code' => $value, 'amount' => $params['amount'][$key]);
			endforeach;
	
			$this->insert_data(CEIS_Model::tbl_request_payment_collections, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}*/
	
	/*public function update_request_registration($params){
			
		try
		{
			$val = array();
			$val["registration_no"] 	= filter_var($params['registration_no'], FILTER_SANITIZE_NUMBER_INT);
			$val["registration_date"] 	= date('Y-m-d', strtotime($params['registration_date']));
			
			$this->update_data(CEIS_Model::tbl_request_registration, $val, array('request_id' => $params['request_id']));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}*/

	public function get_manpower_schedule_list($aColumns, $bColumns, $params, $request_id)
	{
		try
		{
			$query_params = array($request_id);
			$fields 	  = str_replace(" , ", " ", implode(", ", $aColumns));
			$query  	  = <<<EOS
				SELECT $fields
					FROM %s a
					WHERE a.request_id  = ?
EOS;
			$query = sprintf($query, CEIS_Model::tbl_request_schedule);
				
			return $this->set_datatable($query, $aColumns, $bColumns, $params, $query_params);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	

}

/* End of file Requests_model.php */
/* Location: ./application/modules/ceis/models/Requests_model.php */