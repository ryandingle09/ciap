<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_request_contracts_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	public function get_contract_details($rqst_contract_id, $contract_type)
	{
		try 
		{
			$fields = '';
			$join   = '';
			$query  = <<<EOS
				SELECT a.*, b.country_name, f.contract_status_name %s
					FROM %s a 
						JOIN %s b ON a.country_code 	  = b.country_code 
						JOIN %s f ON a.contract_status_id = f.contract_status_id
EOS;
			
			if($contract_type == CONTRACT_TYPE_PROJECT):
				$fields  = ', c.scheduled_percent, c.actual_percent, d.project_type_name, e.involvement';
				$join    = <<<EOS
					LEFT JOIN %s c ON a.rqst_contract_id  = c.rqst_contract_id   
					LEFT JOIN %s d ON c.project_type_code = d.project_type_code 
					LEFT JOIN %s e ON c.involvement_code  = e.involvement_code  
EOS;
				$join 	 = sprintf($join, CEIS_Model::tbl_request_projects, CEIS_Model::tbl_project_types, CEIS_Model::tbl_contract_involvement);	
			else:
				$fields  = ', c.foreign_principal_name, c.required_manpower, c.mobilized_manpower, c.onsite_manpower, c.fees, c.workers_salaries, d.nationality_name';
				$join    = <<<EOS
					LEFT JOIN %s c ON a.rqst_contract_id = c.rqst_contract_id 
					LEFT JOIN %s d ON c.nationality_code = d.nationality_code 
EOS;
				$join 	 = sprintf($join, CEIS_Model::tbl_request_services, CEIS_Model::tbl_param_nationalities);
			endif;
				
			$query .= $join;
			$query .= <<<EOS
				WHERE a.rqst_contract_id = ?
					AND a.contract_type  = ?
EOS;
			$query  = sprintf($query, $fields, CEIS_Model::tbl_request_contracts, CEIS_Model::tbl_countries, CEIS_Model::tbl_param_contract_status);
			
			return $this->query($query, array($rqst_contract_id, $contract_type), FALSE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}
	}
	
	
	public function get_contract_project_list($aColumns, $bColumns, $params, $request_id)
	{
		try
		{
		 	$query_params = array($request_id, CONTRACT_TYPE_PROJECT);
		 	$fields 	  = str_replace(" , ", " ", implode(", ", $aColumns));
		 	$query  	  = <<<EOS
				SELECT $fields
					FROM %s a
						JOIN  %s b ON b.contract_status_id = a.contract_status_id
					WHERE a.request_id 		= ?
						AND a.contract_type = ?
EOS;
		 	$query = sprintf($query, CEIS_Model::tbl_request_contracts,  CEIS_Model::tbl_param_contract_status);
			
			return $this->set_datatable($query, $aColumns, $bColumns, $params, $query_params); 
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}
	}
	
	

	public function get_contract_manpower_list($aColumns, $bColumns, $params, $request_id)
	{
		try
		{
			$query_params = array($request_id, CONTRACT_TYPE_MANPOWER);
			$fields 	  = str_replace(" , ", " ", implode(", ", $aColumns));
			$query  	  = <<<EOS
				SELECT $fields
					FROM %s a
						JOIN  %s b ON b.contract_status_id	  = a.contract_status_id
						LEFT JOIN  %s c ON a.rqst_contract_id = c.rqst_contract_id  AND a.request_id = c.request_id
					WHERE a.request_id 		= ?
						AND a.contract_type = ?
EOS;
			$query = sprintf($query, CEIS_Model::tbl_request_contracts,  CEIS_Model::tbl_param_contract_status, CEIS_Model::tbl_request_services);
				
			return $this->set_datatable($query, $aColumns, $bColumns, $params, $query_params);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}
	}
}

/* End of file */
