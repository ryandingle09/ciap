<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_request_construction_equipment_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	public function get_equipment_list($aColumns, $bColumns, $params, $request_id)
	{
		try
		{
			$query_params = array($request_id);
			$fields 	  = str_replace(" , ", " ", implode(", ", $aColumns));
			$query  	  = <<<EOS
				SELECT $fields
					FROM  %s a
					WHERE a.request_id = ?
EOS;
			$query = sprintf($query, self::tbl_request_equipment);
	
			return $this->set_datatable($query, $aColumns, $bColumns, $params, $query_params);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}

	public function get_equipment_details($rqst_equip_id, $fields=array('*'))
	{
		try
		{
			$query =<<<EOS
				SELECT a.*, b.equipment_type_name
					FROM %s a
						JOIN %s b ON a.equipment_type_id = b.equipment_type_id
						WHERE rqst_equip_id = ?
EOS;
			$query = sprintf($query, self::tbl_request_equipment, self::tbl_param_equipment_type);
			
			return $this->query($query, array($rqst_equip_id), FALSE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}
	}

	public function get_equipment_details_by_request_id($request_id, $fields=array('*'))
	{
		try
		{
			$query =<<<EOS
				SELECT a.*, b.equipment_type_name
					FROM %s a
						JOIN %s b ON a.equipment_type_id = b.equipment_type_id
						WHERE request_id = ?
EOS;
			$query = sprintf($query, self::tbl_request_equipment, self::tbl_param_equipment_type);
				
			return $this->query($query, array($request_id), FALSE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}

}

/* End of file Requests_model.php */
/* Location: ./application/modules/ceis/models/Requests_model.php */