<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Project_types_model extends CEIS_Model
{
	public function __construct() {
		parent::__construct();	
	}

	public function get_project_type_list($aColumns, $bColumns, $params)
	{
		try
		{	
			
			$cColumns = array(
				"a.project_type_code",
				"a.project_type_name",
				"a.classification",
				"a.created_by",
				"a.created_date",
				"a.modified_by",
				"a.modified_date"
				);
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($cColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
		
			
			$query 	  = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields from %s a
					LEFT JOIN %s b
					ON a.created_by = b.employee_no
					LEFT JOIN %s c
					ON a.modified_by = c.employee_no
					$filter_str
				GROUP BY a.project_type_code
	        	$sOrder
	        	$sLimit;
EOS;
			$query	= sprintf($query, CEIS_Model::tbl_project_types, CEIS_Model::tbl_users, CEIS_Model::tbl_users);	
			$stmt = $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_project_type_list($aColumns, $bColumns, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function total_length()
	{
		try
		{
			$fields = array("COUNT(project_type_code) cnt");
				
			return $this->select_one($fields, CEIS_Model::tbl_project_types);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_project_types_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("project_type_code" => $id);
				
			return $this->select_one($fields, CEIS_Model::tbl_project_types, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}

	public function get_specific_project_type($where){

		try
		{
			$fields = array("*");
			
			
			return $this->select_one($fields, CEIS_Model::tbl_project_types, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function delete_project_type($project_type_code)
	{
		try
		{			
			$this->delete_data(CEIS_Model::tbl_project_types, array('project_type_code'=>$project_type_code));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function insert_project_type($params){
			
		try
		{
			$val 						= array();
			$val["project_type_name"] 	= filter_var($params['project_type_name'], FILTER_SANITIZE_STRING);
			$val["project_type_code"]	= $params['project_type_code'];
			$val["classification"] 		= $params['classification'];
			$val["created_by"] 			= $this->session->userdata("employee_no");
			$val["created_date"] 		= date('Y-m-d H:i:s');

			$this->insert_data(CEIS_Model::tbl_project_types, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function update_project_type($params)
	{
		try
		{
			$project_type_code 				= $params["project_type_code"];
			$val 							= array();			
			$val["project_type_name"] 		= filter_var($params['project_type_name'], FILTER_SANITIZE_STRING);
			$val["classification"] 			= $params['classification'];
			$val["modified_by"] 			= $this->session->userdata("employee_no");
			$val["modified_date"]			= date('Y-m-d H:i:s');
			
			$where 							= array();
			$where["project_type_code"]		= $project_type_code;

			$this->update_data(CEIS_Model::tbl_project_types, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
}

/* End of file Project_types_model.php */
/* Location: ./application/modules/ceis/models/Project_types_model.php */
