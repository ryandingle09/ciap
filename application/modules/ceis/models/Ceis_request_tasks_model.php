<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_request_tasks_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	/* CONTRACTOR REGISTRATION NOT REQUEST REGISTRATION */
	public function get_registration_details($where, $fields=array('*'))
	{
		try
		{
			return $this->select_one($fields, self::tbl_registration, $where, array('expiry_date' => 'DESC'));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	/* REQUEST REGISTRATION */
	public function get_request_registration($where, $fields=array('*'))
	{
		try
		{
			return $this->select_one($fields, self::tbl_request_registration, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function get_task($where=array())
	{
		try
		{
			return $this->select_one(array('*'), self::tbl_request_tasks, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);	
			throw $e;
		}
	}
	

	public function get_current_task($request_id)
	{
		try
		{
			$query =<<<EOS
				SELECT *
					FROM %s
					WHERE request_id = ?
					AND   task_status_id  IN (%d, %d)
					ORDER BY sequence_no ASC
EOS;
			$query = sprintf($query, self::tbl_request_tasks, REQUEST_TASK_NOT_YET_STARTED, REQUEST_TASK_ONGOING);
			
			return $this->query($query, array($request_id), FALSE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function get_task_remark($task_remark_id){
		try
		{
			return $this->select_one(array('*'), self::tbl_request_task_remarks, array('task_remark_id' => $task_remark_id));
		}
	
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function get_task_attachment($task_attachment_id){
		try
		{
			return $this->select_one(array('*', 'REPLACE(file_name, SUBSTR(file_name, LOCATE(\'_\', file_name), LOCATE(\'.\', file_name) - LOCATE(\'_\', file_name)), \'\') as display_filename'), self::tbl_request_task_attachments, array('task_attachment_id' => $task_attachment_id));
		}
	
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function get_connected_users($request_id, $fields=array('*'))
	{
		try
		{
			return $this->select_all($fields, self::tbl_request_tasks, array('request_id' => $request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function get_param_attachment_types(){
		try
		{
			return $this->select_all(array('*'), self::tbl_param_attachment_types);
		}
	
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function update_request_task($params){
			
		try
		{
			$val = array();
			$val['task_status_id'] = $params['task_status'];
			$val['assigned_to']	   = $params['assigned_to'];

			switch($val['task_status_id']):
				case REQUEST_TASK_ONGOING:
						$val['start_date'] = date('Y-m-d H:i:s');
					break;
				case REQUEST_TASK_DONE:
						$val['end_date']   = date('Y-m-d H:i:s');
					break;
				case REQUEST_TASK_SKIP:
						$val['start_date'] = date('Y-m-d H:i:s');
						$val['end_date']   = date('Y-m-d H:i:s');
					break;
				default:
					throw new PDOException(sprintf($this->lang->line('invalid_data'), 'Task Status'));
			endswitch;
			
			$this->update_data(self::tbl_request_tasks, $val, array('request_task_id' => $params['request_task_id']));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function update_request_status($params){
			
		try
		{
			$val = array();
			$val['request_status_id'] = $params['board_decision'];
					
			$this->update_data(self::tbl_requests, $val, array('request_id' => $params['request_id']));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_request_tasks($aColumns, $bColumns, $params, $request_id)
	{
		try
		{
			$query_params = array($request_id);
			$fields 	  = str_replace(" , ", " ", implode(", ", $aColumns));
			$query  	  = <<<EOS
				SELECT $fields 
					FROM  %s a 
						JOIN  %s b ON a.task_status_id = b.task_status_id
						JOIN  %s c ON a.role_code 	   = c.role_code
						LEFT JOIN  %s d ON a.assigned_to    = d.employee_no
					WHERE a.request_id = ?
					ORDER BY a.sequence_no
EOS;
			$query = sprintf($query, self::tbl_request_tasks,  self::tbl_param_request_task_status, self::tbl_param_role, self::tbl_users);
				
			return $this->set_datatable($query, $aColumns, $bColumns, $params, $query_params);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_task_remark_list($aColumns, $bColumns, $params, $task_remark_id, $user_id)
	{
		try
		{
			$query_params = array($task_remark_id, $user_id);
			$fields 	  = str_replace(" , ", " ", implode(", ", $aColumns));
			$query  	  = <<<EOS
				SELECT $fields
					FROM  %s a
						JOIN  %s d ON a.created_by = d.employee_no
					WHERE a.request_task_id = ?
						AND a.created_by 	= ?
					ORDER BY a.created_date DESC
EOS;
			$query = sprintf($query, self::tbl_request_task_remarks, self::tbl_users);
	
			return $this->set_datatable($query, $aColumns, $bColumns, $params, $query_params);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_task_attachment_list($aColumns, $bColumns, $params, $task_remark_id, $user_id)
	{
		try
		{
			$query_params = array($task_remark_id, $user_id);
			$fields 	  = str_replace(" , ", " ", implode(", ", $aColumns));
			$query  	  = <<<EOS
				SELECT $fields
					FROM  %s a
						JOIN  %s d ON a.created_by = d.employee_no
						JOIN %s  e ON a.attachment_type_id =  e.attachment_type_id
					WHERE a.request_task_id = ?
						AND a.created_by 	= ?
					ORDER BY a.created_date DESC
EOS;
			$query = sprintf($query, self::tbl_request_task_attachments, self::tbl_users, self::tbl_param_attachment_types);
	
			return $this->set_datatable($query, $aColumns, $bColumns, $params, $query_params);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	

	
	public function get_task_details($request_task_id)
	{
		try
		{
			$query = <<<EOS
				SELECT a.request_id, a.task_name, a.task_status_id, CONCAT(b.first_name, ' ', b.last_name) as resource_name, d.request_type_name,
					   a.generate_reference_flag, a.get_task_flag, a.get_board_decision, c.request_status_id, a.w_skip, e.role_name
					FROM %s a
						LEFT JOIN %s b ON a.assigned_to = b.employee_no
						JOIN %s c ON a.request_id  	    = c.request_id
						JOIN %s d ON c.request_type_id  = d.request_type_id
						JOIN %s e ON a.role_code	    = e.role_code
					WHERE a.request_task_id = ?
EOS;
			$query = sprintf($query, self::tbl_request_tasks, self::tbl_users, self::tbl_requests, self::tbl_param_request_types, self::tbl_param_role);
			
			return $this->query($query, array($request_task_id), FALSE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
	}
	
	public function insert_task_attachments($params)
	{
		try
		{
			$val = array();
			$val['request_task_id']	 	= $params['request_task_id'];
			$val['request_id'] 	 	 	= $params['request_id'];
			$val['received_date'] 	 	= date('Y-m-d', strtotime($params['date_received']));
			$val['attachment_type_id'] 	= $params['document_type'];
			$val['description'] 	 	= filter_var($params['description'], FILTER_SANITIZE_STRING);
			$val['file_name'] 			= $params['filename'];
			$val['created_by']   	 	= $params['created_by'];
			$val['created_date'] 	 	= date('Y-m-d H:i:s');
			
			return $this->insert_data(self::tbl_request_task_attachments, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
	}
	
	public function insert_task_remarks($params){
		try
		{
			$val = array();
			$val['request_id'] 	 	 = $params['request_id'];
			$val['request_task_id']	 = $params['request_task_id'];
			$val['remarks'] 	 	 = filter_var($params['remarks'], FILTER_SANITIZE_STRING);
			$val['created_date'] 	 = date('Y-m-d H:i:s');
			$val['created_by']   	 = $params['created_by'];
	
			return $this->insert_data(self::tbl_request_task_remarks, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function update_task_attachments($params)
	{
		try
		{
			$val = array();
			$val['received_date'] 	 	= date('Y-m-d', strtotime($params['date_received']));
			$val['attachment_type_id'] 	= $params['document_type'];
			$val['description'] 	 	= filter_var($params['description'], FILTER_SANITIZE_STRING);
			$val['file_name'] 			= $params['filename'];
			$val['modified_date'] 	    = date('Y-m-d H:i:s');
			$val['modified_by']   	    = $params['created_by'];
			
			return $this->update_data(self::tbl_request_task_attachments, $val, array('task_attachment_id' => $params['task_attachment_id']));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
	}
	
	public function update_task_remarks($params)
	{
		try
		{
			$val = array();
			$val['remarks'] 	 	 = filter_var($params['remarks'], FILTER_SANITIZE_STRING);
			$val['modified_date'] 	 = date('Y-m-d H:i:s');
			$val['modified_by']   	 = $params['created_by'];
	
			return $this->update_data(self::tbl_request_task_remarks, $val, array('task_remark_id' => $params['task_remark_id']));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function delete_task_remark($task_remark_id){
			
		try
		{
			return $this->delete_data(self::tbl_request_task_remarks,  array('task_remark_id' => $task_remark_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}

	public function delete_task_attachment($task_attachment_id){
			
		try
		{
			return $this->delete_data(self::tbl_request_task_attachments,  array('task_attachment_id' => $task_attachment_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_remarks($request_id)
	{
		try
		{
			$query =<<<EOS
				SELECT IFNULL(a.modified_date, a.created_date) as remark_date, c.task_name,
					   CONCAT(b.first_name, ' ', b.last_name) as resource, a.remarks, c.sequence_no
					FROM %s a
						JOIN %s b ON b.employee_no = IFNULL(a.modified_by, a.created_by)
						JOIN %s c ON a.request_task_id = c.request_task_id
					WHERE a.request_id = ?
					ORDER BY remark_date DESC
EOS;
			$query = sprintf($query, self::tbl_request_task_remarks, self::tbl_users, self::tbl_request_tasks);
			
			return $this->query($query, array($request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	
	public function get_attachments($request_id)
	{
		try
		{
			$query =<<<EOS
				SELECT CONCAT(b.first_name, ' ', b.last_name) as resource, a.received_date,
					   a.description, c.sequence_no, a.file_name, d.attachment_type, c.task_name,
					   REPLACE(a.file_name, SUBSTR(a.file_name, LOCATE('_', a.file_name), LOCATE('.', a.file_name) - LOCATE('_', a.file_name)), '') as display_filename
					FROM %s a
						JOIN %s b ON  b.employee_no  	   = IFNULL(a.modified_by, a.created_by)
						JOIN %s c ON  a.request_task_id    = c.request_task_id
						JOIN %s d ON  a.attachment_type_id = d.attachment_type_id
					WHERE a.request_id = ?
					ORDER BY a.received_date DESC
EOS;
			$query = sprintf($query, self::tbl_request_task_attachments, self::tbl_users, self::tbl_request_tasks, self::tbl_param_attachment_types);
				
			return $this->query($query, array($request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function get_email_and_add_data($params)
	{
		try
		{
			$query =<<<EOS
				SELECT a.request_id, b.request_type_name, c.rep_email, LOWER(d.request_status) as request_status
					FROM %s a 
					JOIN %s b ON a.request_type_id 	  = b.request_type_id
					JOIN %s c ON a.request_id 		  = c.request_id
					JOIN %s d ON a.request_status_id  = d.request_status_id
					WHERE a.request_id = ?
EOS;
			$query = sprintf($query, self::tbl_requests, self::tbl_param_request_types, self::tbl_request_contractor, self::tbl_param_request_status);
				
			return $this->query($query, array($params['request_id']), FALSE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function get_order_of_payment_header($params)
	{
		try
		{
			$query =<<<EOS
				SELECT a.op_date, a.op_amount, b.contractor_name, b.contractor_address, b.license_no, a.created_by, a.rqst_payment_id
					FROM %s a 
						LEFT JOIN %s b ON a.request_id = b.request_id
					WHERE a.request_id = ?
EOS;
			$query = sprintf($query, self::tbl_request_payments, self::tbl_request_contractor);
				
			return $this->query($query, array($params['request_id']));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
/* 	
	
	SELECT a.op_date, a.op_amount, b.registrant_name, b.registrant_address, NULL as license_no, a.created_by, a.rqst_payment_id
	FROM request_payments a
	LEFT JOIN request_equipment b ON a.request_id = b.request_id
	WHERE a.request_id = 55; */
	
	public function get_order_of_payment_details($request_id)
	{
		try
		{
			$query =<<<EOS
					SELECT a.*
						FROM %s a
						WHERE a.rqst_payment_id IN ( SELECT rqst_payment_id FROM %s WHERE request_id = ? )
EOS;
			$query = sprintf($query, self::tbl_request_payment_collections,  self::tbl_request_payments);
			
			return $this->query($query, array($request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function update_reference_no($request_id, $reference_no)
	{
		try
		{
			//$val = array('reference_no' => date('Y').'-'.$request_id);
			$val = array('reference_no' => $reference_no);
			
			$this->update_data(self::tbl_requests, $val, array('request_id' => $request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function get_new_reference_no()
	{
		try 
		{
			$query = <<<EOS
				SELECT COUNT(1) as no_request, CONCAT( YEAR(NOW()) , '-', LPAD(SUBSTR(MAX(reference_no), 6)  + 1, 2, 0)) as new_reference_no
					FROM requests 
						WHERE SUBSTR(reference_no,1 , 4) = YEAR(NOW())
EOS;
			return $this->query($query, array(), FALSE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
	}
	
	public function get_request_details($request_id){
		try
		{
			return $this->select_one(array('*'), self::tbl_requests, array('request_id' => $request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function _get_columns($table)
	{
		try
		{
			$query = <<<EOS
				SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = ? AND TABLE_NAME = ?;
EOS;
			return $this->query($query, array(DB_CEIS, $table));				
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function insert_contractor($params)
	{
		try
		{
			$dsn    = self::get_connection();
			$query = <<<EOS
				INSERT INTO %s 
				(contractor_name, contractor_address, sec_reg_no, sec_reg_date, contractor_category_id, license_no, license_date, last_renewal_date,
				 bir_clearance_date, customs_clearance_date, court_clearance_date, tel_no, fax_no, email, website, rep_first_name, rep_mid_name,
				 rep_last_name, rep_address, rep_contact_no, rep_email, created_by, created_date, license_category_code, exist_in_pcab)
					
				SELECT contractor_name, contractor_address, sec_reg_no, sec_reg_date, contractor_category_id, license_no, license_date, last_renewal_date,
					   bir_clearance_date, customs_clearance_date, court_clearance_date, tel_no, fax_no, email, website, rep_first_name, rep_mid_name,
					   rep_last_name, rep_address, rep_contact_no, rep_email, ?, NOW(), license_category_code, exist_in_pcab
				FROM   %s
				WHERE  request_id = ?	
EOS;
			$query = sprintf($query, self::tbl_contractors, self::tbl_request_contractor);
			$stmt  = $dsn->prepare($query);
		
			self::rlog_info('QUERY ' . $query);
			
			$stmt->execute(array($params['user_id'], $params['request_id']));
			
			return $dsn->lastInsertId();	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}	
	}
	
	public function insert_contractor_data($from, $to, $select_columns, $request_id)
	{
		try
		{	
			$dsn   = self::get_connection();
			$query = <<<EOS
				INSERT INTO %s
				SELECT %s
					FROM  %s a
						WHERE a.request_id = ?
EOS;
		 	$query = sprintf($query, $to, $select_columns, $from);			
			$stmt  = $dsn->prepare($query);
/* 			
			if($from == self::tbl_request_payments):
				throw new Exception('KEVIN : '.$query.' REQUEST ID : '.$request_id.' END');
			endif; */
			
			self::rlog_info('QUERY ' . $query);
				
			$stmt->execute(array($request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function insert_contractor_data_joined_request($from, $to, $select_columns, $request_id, $joined_params)
	{
		try
		{
			$dsn   		= self::get_connection();
			$parent_tbl = $joined_params['parent_tbl'];
			$column	    = $joined_params['column'];
			$add_and	= (ISSET($joined_params['add_and'])) ? $joined_params['add_and'] : '';
			$query 		= <<<EOS
				INSERT INTO %s
				SELECT %s
					FROM  %s a
						JOIN  %s b ON a.%s = b.%s $add_and
						WHERE b.request_id = ?
EOS;
			$query = sprintf($query, $to, $select_columns, $from, $parent_tbl, $column, $column);
			
			$stmt  = $dsn->prepare($query);
				
			self::rlog_info('QUERY ' . $query);
			
			$stmt->execute(array($request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function get_order_of_payments($where, $fields=array('*'))
	{
		try
		{
			return $this->select_all($fields, self::tbl_contractor_payments, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function get_order_of_payment_collections($contractor_id)
	{
		try
		{
			$query = <<<EOS
				SELECT a.*
					FROM %s a
						JOIN %s b ON a.contractor_payment_id = b.contractor_payment_id
							WHERE b.contractor_id = ?
EOS;
			$query = sprintf($query, self::tbl_contractor_payment_collections, self::tbl_contractor_payments);
			
			return $this->query($query, array($contractor_id), TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	
	public function delete_contractor_data($table, $where){
		try
		{
			return $this->delete_data($table, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function delete_contractor_data_joined($from, $to, $column, $contractor_id)
	{
		try
		{
			$dsn   		= self::get_connection();
			$query 		= <<<EOS
				DELETE FROM %s WHERE %s IN 
				(
					SELECT %s
						FROM  %s 
							WHERE contractor_id = ?
			 	)
EOS;
			$query = sprintf($query, $to, $column, $column, $from);
			
			$stmt  = $dsn->prepare($query);
				
			self::rlog_info('QUERY ' . $query);
			
			$stmt->execute(array($contractor_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
	}
	
	public function insert_registration($request_id, $contractor_id)
	{
		try
		{
			$dsn    = self::get_connection();
			$active = REG_STATUS_ACTIVE; 
			$query  = <<<EOS
				INSERT INTO %s
				(registration_no, contractor_id, registration_date, expiry_date, last_renewal_date, registration_status_id)
				SELECT registration_no, $contractor_id, registration_date,  expiry_date, last_renewal_date, {$active}
					FROM  %s a
						WHERE a.request_id = ?
EOS;
			$query = sprintf($query, self::tbl_registration, self::tbl_request_registration);
			$stmt  = $dsn->prepare($query);
				
			self::rlog_info('QUERY ' . $query);
	
			$stmt->execute(array($request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function update_registration($params, $where)
	{
		try
		{
			$this->update_data(self::tbl_registration, $params, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}	
	
	public function get_contractor_contract($where, $fields=array('*'))
	{
		try
		{
			return $this->select_one($fields, self::tbl_contractor_contracts, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function get_contractor($where, $fields=array('*'))
	{
		return $this->select_one($fields, self::tbl_contractors, $where);
	}
	
	public function get_request_contract($where, $fields=array('*'))
	{
		try
		{
			return $this->select_one($fields, self::tbl_request_contracts, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function insert_contractor_services($contract_id, $request_id)
	{
		try
		{
			$dsn    = self::get_connection();
			$active = REG_STATUS_ACTIVE;
			$query  = <<<EOS
				INSERT INTO %s
				(contract_id, foreign_principal_name, nationality_code, required_manpower)
				SELECT $contract_id, foreign_principal_name, nationality_code, est_manpower_required
					FROM %s
					WHERE request_id = ?
EOS;
			$query = sprintf($query, self::tbl_contractor_services, self::tbl_request_aut_services);
			$stmt  = $dsn->prepare($query);
			
			self::rlog_info('QUERY ' . $query);
			
			$stmt->execute(array($request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function insert_contractor_services_dtl($contract_id, $request_id)
	{
		try
		{
			$dsn    = self::get_connection();
			$active = REG_STATUS_ACTIVE;
			$query  = <<<EOS
				INSERT INTO %s
				(contract_id, project_description, service_scope, est_manpower_required, bidding_date, bidding_place, financed_by, guarantees_required, foreign_principal_address)
				SELECT $contract_id, project_description, service_scope, est_manpower_required, bidding_date, bidding_place, financed_by, guarantees_required, foreign_principal_address
					FROM %s
					WHERE request_id = ?
EOS;
			$query = sprintf($query, self::tbl_contractor_services_dtl, self::tbl_request_aut_services);
			$stmt  = $dsn->prepare($query);
				
			self::rlog_info('QUERY ' . $query);
				
			$stmt->execute(array($request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function update_contractor_contract($fields, $where_arr)
	{
		try
		{
			$this->update_data(self::tbl_contractor_contracts, $fields, $where_arr);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function insert_contractor_contracts($contract_id, $request_id, $contractor_id)
	{
		try
		{
			$dsn    = self::get_connection();
			$active = REG_STATUS_ACTIVE;
			$query  = <<<EOS
				INSERT INTO %s
				SELECT rqst_contract_id, $contractor_id, contract_type, base, with_authorization_flag, project_title, project_location, province, country_code, 
					   contract_status_id, project_cost, contract_cost, owner, contract_duration, started_date, completed_date, created_by, created_date, 
					   modified_by, modified_date, remarks
					FROM %s
						WHERE request_id  	   = ?
						AND   rqst_contract_id = ?
EOS;
			$query = sprintf($query, self::tbl_contractor_contracts, self::tbl_request_contracts);
			$stmt  = $dsn->prepare($query);
			//throw new Exception('QUERY ' . $query. ' REQUEST_ID : '.$request_id.' CONTRACT_ID : '.$contract_id);
			self::rlog_info('QUERY ' . $query. ' REQUEST_ID : '.$request_id.' CONTRACT_ID : '.$contract_id);
				
			$stmt->execute(array($request_id, $contract_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function insert_contractor_projects($contract_id, $request_id)
	{
		try
		{
			$dsn    = self::get_connection();
			$active = REG_STATUS_ACTIVE;
			$query  = <<<EOS
				INSERT INTO %s
				(contract_id, project_type_code, involvement_code, scheduled_percent, actual_percent)
				SELECT $contract_id, foreign_principal_name, nationality_code, est_manpower_required
					FROM %s
					WHERE request_id = ?
EOS;
			$query = sprintf($query, self::tbl_contractor_projects, self::tbl_request_aut_projects);
			$stmt  = $dsn->prepare($query);
				
			self::rlog_info('QUERY ' . $query);
				
			$stmt->execute(array($request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function insert_contractor_projects_dtl($contract_id, $request_id)
	{
		try
		{
			$dsn    = self::get_connection();
			$active = REG_STATUS_ACTIVE;
			$query  = <<<EOS
				INSERT INTO %s
				SELECT $contract_id,project_description,scope_work,est_min_manpower_required,est_max_manpower_required,outward_amount,inward_amount,procument_type,
					   contract_involvement,prime_contractor_name,prime_contractor_nationality,other_contract_involvement,bidding_date,bidding_place,contract_type_id,
					   other_filipino_companies,prime_fin_assistance_flag,other_fin_assistance,financed_by,bid_bond,performance_bond,payment_guarantee,retentions,
					   progress_billing,others,owner,owner_address,owner_background
					FROM %s
					WHERE request_id = ?
EOS;
			$query = sprintf($query, self::tbl_contractor_projects_dtl, self::tbl_request_aut_projects);
			$stmt  = $dsn->prepare($query);
	
			self::rlog_info('QUERY ' . $query);
	
			$stmt->execute(array($request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	public function insert_contractor_schedules($contract_id, $request_id)
	{
		try
		{
			$dsn    = self::get_connection();
			$active = REG_STATUS_ACTIVE;
			$query  = <<<EOS
				INSERT INTO %s
				SELECT rqst_sched_id, $contract_id, category, skills_description, salary_rate, number_required, employment_contract, monthly_breakdown, 
					   created_by, created_date, modified_by, modified_date 
					FROM %s
					WHERE request_id = ?
EOS;
			$query = sprintf($query, self::tbl_contractor_contracts_schedule, self::tbl_request_schedule);
			$stmt  = $dsn->prepare($query);
		
			self::rlog_info('QUERY ' . $query);
		
			$stmt->execute(array($request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
}
	


/* End of file Requests_model.php */
/* Location: ./application/modules/ceis/models/Requests_model.php */