<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Country_profile_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}
	
	
	public function get_country_profile_list($aColumns, $bColumns, $params)
	{
		try
		{
			$cColumns = array(
				'a.country_code',
				'b.country_name',
				'a.created_by',
				'a.created_date',
				'a.modified_by',
				'a.modified_date',
			);
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));
			
			$sWhere = $this->filtering($cColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str 	= $sWhere["search_str"];
			$filter_params 	= $sWhere["search_params"];
		
			
			$query 	  = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields
					FROM %s a
					LEFT JOIN %s b 
					ON a.country_code = b.country_code
					LEFT JOIN %s c
					ON a.created_by = c.employee_no
					LEFT JOIN %s d
					ON a.modified_by = d.employee_no
					$filter_str
				GROUP BY a.country_code
	        	$sOrder
	        	$sLimit;
EOS;
			$query	= sprintf($query, CEIS_Model::tbl_country_profile, CEIS_Model::tbl_countries, CEIS_Model::tbl_users, CEIS_Model::tbl_users);
			$stmt	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	public function get_country_profile_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("country_code" => $id);
				
			return $this->select_one($fields, CEIS_Model::tbl_country_profile, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}

	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_country_profile_list($aColumns, $bColumns, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(country_code) cnt");
				
			return $this->select_one($fields, CEIS_Model::tbl_country_profile);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	//Gets Country list from table param_countries
	public function get_countries(){

		try{
			$fields = array("*");
			return $this->select_all($fields, CEIS_Model::tbl_countries);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_specific_country_profile($where){

		try
		{
			$fields = array("*");
			
			return $this->select_one($fields, CEIS_Model::tbl_country_profile,$where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	
	public function insert_country_profile($params){
			
		try
		{
			$val 					= array();
			$val["country_code"] 	= filter_var($params['country_name'], FILTER_SANITIZE_STRING);
			$val["highlights"] 		= filter_var($params['highlights'], FILTER_SANITIZE_STRING);
			$val["file_name"] 		= $params['multiple_file_name'][0];
			$val["created_by"] 		= $this->session->userdata("employee_no");
			$val["created_date"]	= date('Y-m-d H:i:s');

			$this->insert_data(CEIS_Model::tbl_country_profile, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function delete_country_profile($country_code)
	{
		try
		{			
			$this->delete_data(CEIS_Model::tbl_country_profile, array('country_code'=>$country_code));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
		
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function update_country_profile($params)
	{
		try
		{
			$country_code 				= filter_var($params["country_code"], FILTER_SANITIZE_STRING);

			$val 						= array();	
			$val["country_code"]		= filter_var($params["country_name"], FILTER_SANITIZE_STRING);		
			$val["highlights"] 			= filter_var($params['highlights'], FILTER_SANITIZE_STRING);

			IF($params['multiple_file_name'][0]){
				$val["file_name"] 		= $params['multiple_file_name'][0];
			}

			$val["modified_by"] 		= $this->session->userdata("employee_no");
			$val["modified_date"]		= date('Y-m-d H:i:s');

			$where 						= array();
			$where["country_code"]		= $country_code;

			$this->update_data(CEIS_Model::tbl_country_profile, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
}

/* End of file Country_profile_model.php */
/* Location: ./application/modules/ceis/models/Country_profile_model.php */