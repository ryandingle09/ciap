<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_request_evaluation_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	public function get_financial_data($request_id){
		try
		{
			return $this->select_one(array('statement_date', 'networth', '(common_shares * par_value) as paid_up'), CEIS_Model::tbl_request_statements, array('request_id' => $request_id), array('statement_date' => 'DESC'));
		}

		catch(PDOException $e)
		{
			$this->rlog_error($e);	
			throw $e;
		}
	}

	public function insert_request_evaluation($params){
			
		try
		{
			$val = array();
			$val["request_id"]				    = $params['request_id'];
			$val["legal_requirement"] 			= filter_var($params['legal_requirements'], FILTER_SANITIZE_STRING);
			$val["total_actual_constr_year"] 	= filter_var($params['construction_contract'], FILTER_SANITIZE_NUMBER_INT);
			$val["total_completed_contracts"] 	= filter_var($params['completed_contracts'], FILTER_SANITIZE_NUMBER_INT);
			$val["total_rescinded_contracts"] 	= filter_var($params['rescinded_contracts'], FILTER_SANITIZE_NUMBER_INT);
			$val["work_experience"] 			= filter_var($params['work_experience'], FILTER_SANITIZE_STRING);
			$val["total_staff"] 			    = filter_var($params['no_staff'], FILTER_SANITIZE_NUMBER_INT);
			$val["staff_reqmt"] 			 	= filter_var($params['staff_requirements'], FILTER_SANITIZE_STRING);
			$val["committee_recommend"] 		= filter_var($params['application_result'], FILTER_SANITIZE_STRING);

			$this->insert_data(CEIS_Model::tbl_request_evaluation, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	
	public function update_request_evaluation($params){
			
		try
		{
			$val = array();
			$val["legal_requirement"] 			= filter_var($params['legal_requirements'], FILTER_SANITIZE_STRING);
			$val["total_actual_constr_year"] 	= filter_var($params['construction_contract'], FILTER_SANITIZE_NUMBER_INT);
			$val["total_completed_contracts"] 	= filter_var($params['completed_contracts'], FILTER_SANITIZE_NUMBER_INT);
			$val["total_rescinded_contracts"] 	= filter_var($params['rescinded_contracts'], FILTER_SANITIZE_NUMBER_INT);
			$val["work_experience"] 			= filter_var($params['work_experience'], FILTER_SANITIZE_STRING);
			$val["total_staff"] 			    = filter_var($params['no_staff'], FILTER_SANITIZE_NUMBER_INT);
			$val["staff_reqmt"] 			 	= filter_var($params['staff_requirements'], FILTER_SANITIZE_STRING);
			$val["committee_recommend"] 		= filter_var($params['application_result'], FILTER_SANITIZE_STRING);
	
			$this->update_data(CEIS_Model::tbl_request_evaluation, $val, array('request_id' => $params['request_id']));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_work_volume($request_id, $number=3)
	{
		try
		{
			$query = <<<EOS
				SELECT (SUM(a.gross_sales) / {$number}) as work_volume
				FROM
				(
					SELECT *
					FROM %s
					WHERE request_id = ? ORDER BY statement_date DESC
				) a ORDER BY statement_date DESC LIMIT {$number}
EOS;
/* 
			$query = <<<EOS
				SELECT (SUM(a.gross_sales) / {$number}) as work_volume
				FROM
				(
					SELECT *
					FROM %s
					WHERE request_id = ? ORDER BY statement_date DESC
				) a  GROUP BY YEAR(a.statement_date) ORDER BY statement_date DESC LIMIT {$number}
EOS;
			 */
			$query  = sprintf($query, self::tbl_request_statements);
			
			$result = $this->query($query, array($request_id));
			
			return $result[0]['work_volume'];
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}


	public function get_request_evaluation($where, $fields=array('*')){

		try
		{
			return $this->select_one($fields, CEIS_Model::tbl_request_evaluation, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	
	public function get_completed_contracts($where)
	{
		try
		{
			$result = $this->select_one(array('COUNT(1) as no_contracts'), CEIS_Model::tbl_request_contracts, $where);
			
			return $result['no_contracts'];
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_contract_per_status($request_id, $contract_status_id)
	{
		try
		{
			$result =  $this->select_one(array('COUNT(1) as no_contracts'), CEIS_Model::tbl_request_contracts, array('request_id' => $request_id, 'contract_status_id' => $contract_status_id));
			
			return $result['no_contracts'];
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}

	public function get_specialization($request_id)
	{
		try
		{
			$query = <<<EOS
				SELECT a.license_classification_code, b.license_classification_name
				FROM %s a
				JOIN %s b ON a.license_classification_code = b.license_classification_code
				WHERE  request_id = ?  AND	a.specialty_flag = ?
EOS;
			$query = sprintf($query, CEIS_MODEL::tbl_request_license_classifications, CEIS_MODEL::tbl_param_license_classifications);

			return $this->query($query, array($request_id, ACTIVE_FLAG), TRUE);	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_key_staff_number($request_id)
	{
		try 
		{
			$query  = <<<EOS
				SELECT COUNT(1) as staff_no 
				 FROM  %s a
					JOIN  %s b ON a.rqst_staff_id = b.rqst_staff_id AND a.request_id = b.request_id AND b.present_flag = ?
				 WHERE  a.request_id = ?	
					AND a.staff_type = ?
EOS;
			$query  = sprintf($query, CEIS_MODEL::tbl_request_staff, CEIS_MODEL::tbl_request_staff_experiences);
			
			$result = $this->query($query, array(ACTIVE_FLAG, $request_id, STAFF_TYPE_TECHNICAL), FALSE);
			
			return $result['staff_no'];
		} 
		catch (PDOException $e) 
		{
			$this->rlog_error($e);
			
			throw $e;
		}
	}
	
	public function get_actual_construction_years($request_id)
	{
		try
		{
			$query  = <<<EOS
				SELECT YEAR(NOW()) - MIN(YEAR(started_date)) as actual_years FROM %s WHERE request_id = ?
EOS;
			$query  = sprintf($query, CEIS_MODEL::tbl_request_contracts);
				
			$result = $this->query($query, array($request_id), FALSE);
				
			return $result['actual_years'];
		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}
	
	
	public function get_request_contractor_details($where, $fields=array('*'))
	{
		try
		{
			return $this->select_one($fields, CEIS_Model::tbl_request_contractor, $where);
		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_request_details($request_id){
	
		try
		{
			$query  = <<<EOS
				SELECT a.*, b.request_status as request_status_name, c.request_type_name
				FROM %s a
					JOIN %s b ON a.request_status_id = b.request_status_id
					JOIN %s c ON a.request_type_id   = c.request_type_id
				WHERE a.request_id = ?
EOS;
			$query  = sprintf($query, self::tbl_requests, self::tbl_param_request_status, self::tbl_param_request_types);
	
			return $this->query($query, array($request_id), FALSE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_request_contractor_class($request_id, $specialty_flag=2)
	{
		try 
		{
			$where = '';
			$query_params = array($request_id);
			
			if($specialty_flag != 2):
				$where = ' AND a.specialty_flag = ?';
				$query_params[] = $specialty_flag; 
			endif;
				
			
			$query  = <<<EOS
				SELECT a.*, b.license_classification_name
				FROM %s a
					JOIN %s b ON a.license_classification_code = b.license_classification_code
				WHERE a.request_id = ?
					$where
				ORDER BY b.sort_order ASC	
EOS;
			$query  = sprintf($query, self::tbl_request_license_classifications, self::tbl_param_license_classifications);
			
			return $this->query($query, $query_params);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_request_total_capital_stock($request_id)
	{
		$params = array(ACTIVE_FLAG, ACTIVE_FLAG, $request_id);
		
		$query  = <<<EOS
				SELECT SUM(b.paid_in_amount) as total_capital_stock
					FROM %s a
						JOIN %s b ON a.rqst_staff_id = b.rqst_staff_id AND b.stockholder_flag = ? AND b.present_flag = ?
					WHERE a.request_id = ?
EOS;
		$query  = sprintf($query, self::tbl_request_staff, self::tbl_request_staff_experiences);
			
		$result =  $this->query($query, $params, FALSE);
		
		return $result['total_capital_stock'];
	}
	
	public function get_request_stockholders_directors($request_id)
	{
		try
		{
			$stockholder = STAFF_TYPE_PRINCIPAL;
			$director    = STAFF_TYPE_DIRECTOR;
			
			$params = array(ACTIVE_FLAG, $request_id);
			
			$query  = <<<EOS
				SELECT CONCAT(a.first_name, ' ', a.middle_name, ' ', a.last_name) as staff_name, b.present_flag, b.paid_in_amount, 
						b.position, a.staff_type
					FROM %s a
						JOIN %s b ON a.rqst_staff_id = b.rqst_staff_id  AND b.present_flag = ?
					WHERE a.request_id = ?
						AND a.staff_type IN ('$stockholder', '$director')
EOS;
			$query  = sprintf($query, self::tbl_request_staff, self::tbl_request_staff_experiences);
				
			$result =  $this->query($query, $params);
		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_request_key_staffs($request_id)
	{
		try
		{
			$params = array(ACTIVE_FLAG, $request_id);
				
			$query  = <<<EOS
				SELECT CONCAT(a.first_name, ' ', a.middle_name, ' ', a.last_name) as staff_name, b.present_flag, b.paid_in_amount,
						b.position, a.staff_type
					FROM %s a
						JOIN %s b ON a.rqst_staff_id = b.rqst_staff_id  AND b.present_flag = ?
					WHERE a.request_id = ?
						AND a.staff_type IN ('$stockholder', '$director')
EOS;
			$query  = sprintf($query, self::tbl_request_staff, self::tbl_request_staff_experiences);
	
			$result =  $this->query($query, $params);
		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_request_checklist($request_type_id, $request_id)
	{
		try
		{
			$params = array($request_id, $request_type_id);
				
			$query  = <<<EOS
			SELECT a.checklist_id, a.checklist, b.complied_flag 
				FROM %s a
					LEFT JOIN %s b ON a.checklist_id = b.checklist_id AND b.request_id = ?
						WHERE a.request_type_id = ?
EOS;
			$query  = sprintf($query, self::tbl_checklist, self::tbl_request_checklist);
	
			return  $this->query($query, $params);
		}
		catch (PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
}

/* End of file Requests_model.php */
/* Location: ./application/modules/ceis/models/Requests_model.php */