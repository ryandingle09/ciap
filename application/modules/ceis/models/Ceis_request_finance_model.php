<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_request_finance_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	public function get_statement_list($aColumns, $bColumns, $params, $request_id)
	{
		try
		{
			$query_params = array($request_id);
			$fields 	  = str_replace(" , ", " ", implode(", ", $aColumns));
			$query  	  = <<<EOS
				SELECT $fields
					FROM  %s a
					WHERE a.request_id = ?
					ORDER BY a.statement_date DESC
EOS;
			$query = sprintf($query, self::tbl_request_statements);
	
			return $this->set_datatable($query, $aColumns, $bColumns, $params, $query_params);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}

	public function get_financial_statement($where, $fields=array('*'))
	{
		try
		{
			return $this->select_one($fields, self::tbl_request_statements, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
	}

	public function get_request_top_statements($request_id, $number=3)
	{
		try
		{
			$query = <<<EOS
				SELECT *
				FROM
				(
					SELECT *
					FROM %s
					WHERE request_id = ? ORDER BY statement_date DESC
				) a  GROUP BY YEAR(a.statement_date) ORDER BY statement_date DESC LIMIT {$number}
EOS;
			$query = sprintf($query, self::tbl_request_statements);
			return $this->query($query, array($request_id));
		}
		catch(PDOException $e)
		{
			throw new Exception($e, EXCEPTION_CUSTOM);
		}
	}
	
	public function get_request_statement($request_id, $statement_date=NULL, $fields=array('*'))
	{
		try
		{
			$where = array('request_id' => $request_id);
				
			if( ! EMPTY($statement_date))
				$where['statement_date'] = date('Y-m-d', strtotime($statement_date));
					
				return $this->select_all($fields, self::tbl_request_statements, $where, array('statement_date' => 'DESC'), null, 'LIMIT 0,3');
		}
		catch(PDOException $e)
		{
			throw new Exception($e, EXCEPTION_CUSTOM);
		}
	}

}

/* End of file Requests_model.php */
/* Location: ./application/modules/ceis/models/Requests_model.php */