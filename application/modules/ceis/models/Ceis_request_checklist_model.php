<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_request_checklist_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	public function get_request_checklist($request_id)
	{
		try
		{
			$query =<<<EOS
				SELECT  a.complied_flag, a.checklist_id, b.file_name, a.request_id,
						REPLACE(SUBSTRING_INDEX(b.file_name, '_', -2), SUBSTR(SUBSTRING_INDEX(b.file_name, '_', -2), LOCATE('_', SUBSTRING_INDEX(b.file_name, '_', -2)), LOCATE('.', SUBSTRING_INDEX(b.file_name, '_', -2)) - LOCATE('_', SUBSTRING_INDEX(b.file_name, '_', -2))), '') as display_filename
					FROM %s a
						LEFT JOIN %s b ON a.request_id = b.request_id AND a.checklist_id = b.checklist_id
					WHERE a.request_id = ?
EOS;
			$query = sprintf($query, self::tbl_request_checklist, self::tbl_request_attachments);
			
			return $this->query($query, array($request_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}				
	}
	
	public function get_checklist($request_id, $checklist_id)
	{
		try
		{
/* 			$query =<<<EOS
				SELECT c.caption, b.checklist
					FROM %s a
						JOIN %s b      ON a.checklist_id = b.checklist_id
						LEFT JOIN %s c ON a.checklist_id = c.checklist_id AND a.request_id = c.request_id
					WHERE a.request_id = ?
						AND a.checklist_id = ?
EOS;
			$query = sprintf($query, self::tbl_request_checklist, self::tbl_checklist, self::tbl_request_attachments);
			return $this->query($query, array($request_id, $checklist_id)); */
			
			$query =<<<EOS
				SELECT c.caption, a.checklist
					FROM %s a
						LEFT JOIN %s b      ON a.checklist_id = b.checklist_id AND b.request_id = ?
						LEFT JOIN %s c ON a.checklist_id = c.checklist_id AND b.request_id = c.request_id
					WHERE a.checklist_id = ?
						
EOS;
			$query = sprintf($query, self::tbl_checklist, self::tbl_request_checklist, self::tbl_request_attachments);
				
			return $this->query($query, array($request_id, $checklist_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
}

/* End of file Requests_model.php */
/* Location: ./application/modules/ceis/models/Requests_model.php */