<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_request_staffing_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	public function get_staff_list($params, $request_id)
	{
		try
		{
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns     = array(
					'CONCAT(a.first_name, " ", a.middle_name, " ", a.last_name) as staff_name',
					"( PERIOD_DIFF(DATE_FORMAT(NOW(),'%%Y%%m'), DATE_FORMAT(b.start_date,'%%Y%%m')) / 12 ) as no_years",
					"'Connected' as status", 'a.rqst_staff_id', 'a.staff_type'
			);
				
			// APPEARS ON TABLE
			$bColumns 	  = array('staff_name', 'no_years', 'status');
			$query_params = array(ACTIVE_FLAG, $request_id);
			$fields    	  = implode(',', $aColumns);
			$query 	  	  = <<<EOS
				SELECT $fields
					FROM %s a
						LEFT JOIN %s b
							ON a.rqst_staff_id = b.rqst_staff_id AND a.request_id = b.request_id AND b.present_flag = ?
						WHERE a.request_id = ?
EOS;
			$query    = sprintf($query, self::tbl_request_staff, self::tbl_request_staff_experiences);
			
		
			return $this->set_datatable($query, $aColumns, $bColumns, $params, $query_params);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}

	public function get_staff_work_exp_list($params, $staff_id)
	{
		try
		{
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array(
					"CONCAT(DATE_FORMAT(a.start_date, ' %%b %%d,%%Y '), IF( a.end_date IS NULL, '', CONCAT('to ',DATE_FORMAT(a.end_date, ' %%b %%d,%%Y '))) ) as inclusive_dates",
					'CONCAT(a.company_name, \' -  \', a.company_address) as company_details',
					'a.job_description',
					'IF(a.present_flag = 0, \' Resigned \', \' Connected \') as status',
					'a.rqst_staff_exp_id'
			);
			// APPEARS ON TABLE
			$bColumns 	  = array('inclusive_dates', 'company_details', 'job_description', 'status');			
			$query_params = array($staff_id);
			$fields    	  = implode(',', $aColumns);
			$query 	  	  = <<<EOS
				SELECT $fields
					FROM %s a
				    WHERE a.rqst_staff_id = ? 	
EOS;
			$query    = sprintf($query, self::tbl_request_staff_experiences);
				
			return $this->set_datatable($query, $aColumns, $bColumns, $params, $query_params);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_staff_project_list($params, $staff_id)
	{
		try
		{
			$aColumns  = array(
					'a.project_title',
					'a.project_location',
					'a.owner',
					'FORMAT(a.project_cost, '.DECIMAL_PLACES.') as project_cost',
					'a.rqst_staff_proj_id'
			);
				
			$bColumns     = array('project_title', 'project_location', 'owner', 'project_cost');
			$query_params = array($staff_id);
			$fields    	  = implode(',', $aColumns);
			$query 	   	  = <<<EOS
				SELECT $fields
					FROM %s a
					 	WHERE a.rqst_staff_id = ?
EOS;
			$query    = sprintf($query, self::tbl_request_staff_projects);
			
			return $this->set_datatable($query, $aColumns, $bColumns, $params, $query_params);
		}
		catch(PDOException $e)
		{
			throw new Exception($e, EXCEPTION_CUSTOM);
		}
	}
	
	
	
	public function get_staff_work_exp($where=array(), $fields=array('*'))
	{
		try
		{
			return $this->select_one($fields, self::tbl_request_staff_experiences, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_staff_project($where=array(), $fields=array('*'))
	{
		try
		{
			//return $this->select_one($fields, self::tbl_request_staff_projects, $where);
			$fields = implode(',', $fields);
			$query  = <<<EOS
				SELECT a.*, b.project_type_name
					FROM %s a 
						JOIN %s b ON a.project_type_code = b.project_type_code
EOS;
			$query  = sprintf($query, self::tbl_request_staff_projects, self::tbl_param_project_types);
			
			return $this->query($query, $where, FALSE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
}
	


/* End of file Requests_model.php */
/* Location: ./application/modules/ceis/models/Requests_model.php */