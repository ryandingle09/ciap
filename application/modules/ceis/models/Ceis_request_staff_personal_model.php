<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_request_staff_personal_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	public function get_staff_info($where, $fields=array('*'))
	{
		try
		{
			return $this->select_one($fields, self::tbl_request_staff, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);	
			throw $e;
		}
	}
	
	public function get_staff_license($where, $fields=array('*'))
	{
		try
		{
			return $this->select_all($fields, self::tbl_request_staff_licenses, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

}
	


/* End of file Requests_model.php */
/* Location: ./application/modules/ceis/models/Requests_model.php */