<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Resolution_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}		
	
	public function get_resolution_list($aColumns, $bColumns, $params)
	{
		try
		{	
			
			$cColumns = array(
				'a.resolution_no',
				'a.resolution_date',
				'a.title',
				'a.first_round',
				'a.last_round',
				'a.offers',
				'a.commitments',
				'a.created_by',
				'a.created_date',
				'a.modified_by',
				'a.modified_date',
			);
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($cColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
		
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM  %s a
				$filter_str
				GROUP BY a.resolution_no
	        	$sOrder
	        	$sLimit
EOS;

			
			$query	= sprintf($query, CEIS_Model::tbl_resolutions);			
			$stmt	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	


	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_resolution_list($aColumns, $bColumns, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(resolution_no) cnt");
				
			return $this->select_one($fields, CEIS_Model::tbl_resolutions);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function get_resolutions_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("resolution_no" => $id);
				
			return $this->select_one($fields, CEIS_Model::tbl_resolutions, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}

	public function insert_resolutions($params){
			
		try
		{
			$val 					= array();
			$val["resolution_no"] 	= filter_var($params['resolution_no'], FILTER_SANITIZE_STRING);
			$val["resolution_date"] = date('Y-m-d', strtotime($params['resolution_date']));
			$val["title"]			= filter_var($params['title'], FILTER_SANITIZE_STRING);
			$val["first_round"] 	= date('Y-m-d', strtotime($params['first_round']));
			$val["last_round"] 		= date('Y-m-d', strtotime($params['last_round']));
			$val["offers"] 			= filter_var($params['offers'], FILTER_SANITIZE_STRING);
			$val["commitments"] 	= filter_var($params['commitments'], FILTER_SANITIZE_STRING);
			$val["created_by"] 		= $this->session->userdata("employee_no");
			$val["created_date"] 	= date('Y-m-d H:i:s');
			

			$this->insert_data(CEIS_Model::tbl_resolutions, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
	}

	public function delete_resolution($resolution_no)
	{
		try
		{			
			$this->delete_data(CEIS_Model::tbl_resolutions, array('resolution_no'=>$resolution_no));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_specific_resolution($where){

		try
		{
			$fields = array("*");
			
			return $this->select_one($fields, CEIS_Model::tbl_resolutions, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}


	public function update_resolutions($params)
	{
		try
		{
			$resolution_no 				= $params["resolution_no"];

			$val 						= array();			
			$val["resolution_date"]		= date('Y-m-d', strtotime($params['resolution_date']));
			$val["title"]				= $params['title'];
			$val["first_round"] 		= date('Y-m-d', strtotime($params['first_round']));
			$val["last_round"] 			= date('Y-m-d', strtotime($params['last_round']));
			$val["offers"]				= $params['offers'];
			$val["commitments"]			= $params['commitments'];
			$val["modified_by"] 		= $this->session->userdata("employee_no");
			$val["modified_date"]		= date('Y-m-d H:i:s');

			$where 						= array();
			$where["resolution_no"]		= $resolution_no;

			$this->update_data(CEIS_Model::tbl_resolutions, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);

			throw $e;
		}
	}
}
/* End of file Resolution_model.php */
/* Location: ./application/modules/ceis/models/Resolution_model.php */