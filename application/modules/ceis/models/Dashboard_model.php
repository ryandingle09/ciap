<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}
}

/* End of file Directory_model.php */
/* Location: ./application/modules/ceis/models/Directory_model.php */