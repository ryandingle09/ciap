<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ceis_request_tasks extends Ceis_Controller
{
	private $module = MODULE_CEIS_REQUESTS;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('file');
		
		$this->load->model('ceis_request_tasks_model', 'crtm');
		$this->load->model('ceis_params_model', 'cpm');
		
		$this->load->library('email_template');
		
		$this->load->library('rest', array(
				'server' => PORTAL_API_SERVER
		),'ceis_portal_api');
	}
	
	public function fetch_data()
	{
		try
		{
			$flag   = 0;
			$data   = array();
			$params = get_params();
			$type   = $params['type'];
			$tokens = explode('/', $params['security']);
			
			$id 	= base64_url_decode($tokens[0]);
			$salt   = $tokens[1];
			$token  = $tokens[2];
			
			check_salt($id, $salt, $token);
			
			switch($type):
				case 'remarks':
						$remarks = $this->crtm->get_task_remark($id);	
						$data['remarks'] = $remarks['remarks'];
					break;
				case 'attachment':
						$attachment = $this->crtm->get_task_attachment($id);
						$data['description']   		= $attachment['description'];
						$data['received_date'] 		= date('m/d/Y', strtotime($attachment['received_date']));
						$data['attachment_type_id'] = $attachment['attachment_type_id'];
						$data['file_details']		= array('display_filename' => $attachment['display_filename'], 'filename' => $attachment['file_name'], 'path' => PATH_UPLOADS_REQUESTS.$attachment['request_id']);
					break;
			endswitch;
			
			$msg = 'Fetch Successful';
		}
		catch(PDOException $e)
		{
			$msg =  $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			$msg =  $this->rlog_error($e, TRUE);
		}
		
		echo json_encode( array('flag' => $flag, 'msg' => $msg, 'data' => $data) );
	}
	
	
	public function get_task_attachment_list($req_id, $salt, $token)
	{
		try 
		{
			$rows		    = array();
			$params	 		= get_params();
			$task_remark_id = base64_url_decode($req_id);
				
			check_salt($task_remark_id, $salt, $token);
				
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns 	= array('a.task_attachment_id', 'a.request_task_id', 'a.request_id', 'e.attachment_type', 'a.description', 'a.file_name', 'CONCAT(d.first_name, \' \', d.last_name) as resource', 'DATE_FORMAT(a.received_date, \'%%b %%d,%%Y\' ) as received_date');
			// APPEARS ON TABLE
			$bColumns 	= array('received_date', 'attachment_type', 'description', 'resource');
		
			$projects 	= $this->crtm->get_task_attachment_list($aColumns, $bColumns, $params, $task_remark_id, $this->user_id);
		
			foreach($projects['aaData'] as $aRow):
				$salt     = gen_salt();
				$token    = in_salt($aRow['task_attachment_id'], $salt);
				$url      = base64_url_encode($aRow['task_attachment_id'])."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("attachment", "'.$url.'")';
				
				$action   = "<div class='table-actions'>";
				$action  .= "<a href='javascript:void(0);' id='edit_attachment'   data-url='".$url."' class='edit tooltipped'   data-tooltip='Edit Attachment'    data-position='bottom' data-delay='50' ><i class='material-icons'></i></a>";
				$action  .= "<a onclick='".$delete_action."' href='javascript:void(0);' id='delete_attachment' data-url='".$url."' class='delete tooltipped' data-tooltip='Delete Attachment'  data-position='bottom' data-delay='50' ><i class='material-icons'></i></a>";
				$action  .= "</div>";
			
				$rows[]   = array($aRow['received_date'], $aRow['attachment_type'], string_min($aRow['description']), $aRow['resource'], $action);
			endforeach;
		
			$projects['aaData'] = $rows;
		
			echo json_encode($projects);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
	}
	
	public function get_task_remarks_list($req_id, $salt, $token)
	{
		try{
			$rows		    = array();
			$params	 		= get_params();
			$task_remark_id = base64_url_decode($req_id);
				
			check_salt($task_remark_id, $salt, $token);
				
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns 	= array('a.task_remark_id', 'a.request_task_id', 'a.remarks', 'CONCAT(d.first_name, \' \', d.last_name) as resource', 'DATE_FORMAT(a.created_date, \'%%b %%d,%%Y\' ) as created_date');
			// APPEARS ON TABLE
			$bColumns 	= array('created_date', 'remarks', 'resource');
		
			$projects 	= $this->crtm->get_task_remark_list($aColumns, $bColumns, $params, $task_remark_id, $this->user_id);
		
			foreach($projects['aaData'] as $aRow):
				$salt     = gen_salt();
				$token    = in_salt($aRow['task_remark_id'], $salt);
				$url      = base64_url_encode($aRow['task_remark_id'])."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("remarks", "'.$url.'")';
				
				$action   = "<div class='table-actions'>";
				$action  .= "<a href='javascript:void(0);' id='edit_remarks'   data-url='".$url."' class='edit tooltipped'   data-tooltip='Edit Remark'    data-position='bottom' data-delay='50' ><i class='material-icons'></i></a>";
				$action  .= "<a onclick='".$delete_action."' href='javascript:void(0);' id='delete_remarks' data-url='".$url."' class='delete tooltipped' data-tooltip='Delete Remark'  data-position='bottom' data-delay='50' ><i class='material-icons'></i></a>";
				$action  .= "</div>";
			
				$rows[]   = array($aRow['created_date'], string_min($aRow['remarks']), $aRow['resource'], $action);
			endforeach;
		
			$projects['aaData'] = $rows;
		
			echo json_encode($projects);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
	}
	
	/*
	 * $curr_task : Refers to the current available task.
	 * $loop  	  : Refers to the current task in the loop. 
	 */
	private function _check_action_access($curr_task, $loop, $cusers)
	{
		/*
		 * If 		 : If the task in the loop is also the current task.
		 * Elseif 	 : If the task in the loop is already done or skipped and the current user is already in the "loop".
		 * Else      : no access should be given.
		 */
		if($curr_task['request_task_id'] == $loop['request_task_id']):
			/* Check if the current task has a designated personnel already */
			if( ! EMPTY($curr_task['assigned_to']))
			{
				if($curr_task['assigned_to'] == $this->user_id):
					return array('change_status' => TRUE, 'others' => TRUE);
				elseif(in_array($this->user_id, $cusers)):
					return array('change_status' => FALSE, 'others' => TRUE);
				else: 
					return array('change_status' => FALSE, 'others' => FALSE);
				endif;
			}
			else
			{   
				/* If current task has no designated personnel check if the currently logged user has the right role for the task */
				return (in_array($curr_task['role_code'], $this->user_roles)) ? array('change_status' => TRUE, 'others' => FALSE) : array('change_status' => FALSE, 'others' => FALSE);
			}
		elseif($curr_task['request_task_id'] > $loop['request_task_id'] && (in_array($this->user_id, $cusers))):
			return array('change_status' => FALSE, 'others' => TRUE);
		else:
			return array('change_status' => FALSE, 'others' => FALSE);	
		endif;
	}
	
	public function get_request_task_list($req_id, $salt, $token)
	{
		try{
			$rows		    = array();
			$params	 		= get_params();
			$request_id 	= base64_url_decode($req_id);
			
			check_salt($request_id, $salt, $token);
		
			$curr_task 		= $this->crtm->get_current_task($request_id);
			$raw_users 		= $this->crtm->get_connected_users($request_id, array('GROUP_CONCAT(DISTINCT assigned_to) as assigned_to'));
			$cusers    		= explode(',', $raw_users[0]['assigned_to']);
			
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns 		= array('a.request_task_id', 'a.task_name', 'a.task_status_id',
									'CONCAT(c.role_name,IF(d.employee_no IS NULL, \'\', CONCAT(\' - \', d.first_name, \' \', d.last_name))) as resource', 
									'DATE_FORMAT(a.start_date, \'%%b %%d,%%Y %%h:%%i %%p\' ) as start_date', 'DATE_FORMAT(a.end_date, \'%%b %%d,%%Y %%h:%%i %%p\' ) as end_date', 'b.task_status');
			// APPEARS ON TABLE
			$bColumns 		= array('task_name', 'resource', 'start_date', 'end_date', 'task_status');
	
			$tasks 			= $this->crtm->get_request_tasks($aColumns, $bColumns, $params, $request_id);
				
			foreach($tasks['aaData'] as $aRow):
				$salt     = gen_salt();
				$token    = in_salt($aRow['request_task_id'], $salt);
				$url      = base64_url_encode($aRow['request_task_id'])."/".$salt."/".$token;
				$action   = "<div class='table-actions'>";
				
				$access   = $this->_check_action_access($curr_task, $aRow, $cusers);
				
				if($access['change_status'] === TRUE):
					$action  .= "<a href='javascript:;' class='md-trigger  tooltipped' data-tooltip='Change Status'  data-position='bottom' data-delay='50' data-modal='modal_task' onclick=\"modal_task_init('".$url."')\" ><i class='material-icons'>update</i></a>";
				endif;
				
				if($access['others'] === TRUE):
					$action  .= "<a href='javascript:;' class='md-trigger  tooltipped' data-tooltip='Add Remarks'    data-position='bottom' data-delay='50' data-modal='modal_remarks' onclick=\"modal_remarks_init('".$url."')\" ><i class='material-icons'>insert_comment</i></a>";
					$action  .= "<a href='javascript:;' class='md-trigger  tooltipped' data-tooltip='Add Attachment' data-position='bottom' data-delay='50' data-modal='modal_attachments' onclick=\"modal_attachments_init('".$url."')\" ><i class='material-icons'>cloud_upload</i></a>";
				endif;
				
				$action  .= "</div>";
				$rows[]   = array($aRow['task_name'], $aRow['resource'], $aRow['start_date'] , $aRow['end_date'], $aRow['task_status'], $action);
			endforeach;
				
			$tasks['aaData'] = $rows;
				
			echo json_encode($tasks);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
	}
	
	
	public function modal_task($encoded_id, $salt, $token)
	{
		try
		{	
			$id = base64_url_decode($encoded_id);
			
			check_salt($id, $salt, $token);
			
			$resources['load_css'] 	 = array(CSS_SELECTIZE);
			$resources['load_js'] 	 = array(JS_SELECTIZE);
			$resources['load_init']  = array('CeisRequestSubRequestStatusTasks.initModal();');
			
			$data['status'] 		 = $this->cpm->get_param_task_status();
			$data['board_decision']  = $this->cpm->get_param_request_status(array('board_flag' => ACTIVE_FLAG));
			$data['task_details'] 	 = $this->crtm->get_task_details($id);
			$data['security']		 = $encoded_id.'/'.$salt.'/'.$token;
			//echo '<pre>'; print_r($data['task_details']); echo '</pre>'; 
			$html  = $this->load->view('modals/request_task',$data, TRUE);
			$html .= $this->load_resources->get_resource($resources, TRUE);
				
			echo $html;
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
	}
	
	public function modal_remarks($encoded_id, $salt, $token)
	{
		try
		{
			$id  = base64_url_decode($encoded_id);
			$url = $encoded_id.'/'.$salt.'/'.$token;
			
			check_salt($id, $salt, $token);
				
			$resources['load_css'] 	  = array(CSS_SELECTIZE);
			$resources['load_js'] 	  = array(JS_SELECTIZE);
			$resources['load_init'][] = 'CeisRequestSubRequestStatusTasks.RemarksModal(\''.$url.'\');';
			$resources['load_init'][] = "var deleteObj = new handleData({controller:'ceis_request_tasks', method:'delete_remarks', module:'".PROJECT_CEIS."'});";
			
			$data['status'] 		  = $this->cpm->get_param_task_status();
			$data['task_details'] 	  = $this->crtm->get_task_details($id);
			$data['security']		  = $url;
			$html  = '';
			$html  = $this->load->view('modals/request_task_remarks',$data, TRUE);
			$html .= $this->load_resources->get_resource($resources, TRUE);
			
		
			echo $html;
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
	}
	
	public function modal_attachments($encoded_id, $salt, $token)
	{
		try
		{
			$id  = base64_url_decode($encoded_id);
			$url = $encoded_id.'/'.$salt.'/'.$token;
		 	
			check_salt($id, $salt, $token);
		
			$data['type'] 		 	 = $this->crtm->get_param_attachment_types();
			$data['security']        = $url;
			$data['task_details'] 	 = $this->crtm->get_task_details($id);
			
			$path = PATH_UPLOADS_REQUESTS.$data['task_details']['request_id'].'/';
			
			$resources['load_css'] 	 = array(CSS_SELECTIZE, CSS_DATETIMEPICKER, CSS_UPLOAD);
			$resources['load_js'] 	 = array(JS_SELECTIZE, JS_DATETIMEPICKER, JS_UPLOAD);
			$resources['load_init'][] = 'CeisRequestSubRequestStatusTasks.AttachmentsModal(\''.$url.'\', \''.$path.'\');';
			$resources['load_init'][] = "var deleteObj = new handleData({controller:'ceis_request_tasks', method:'delete_attachment', module:'".PROJECT_CEIS."'});";
			
			$html  = $this->load->view('modals/request_task_attachments',$data, TRUE);
			$html .= $this->load_resources->get_resource($resources, TRUE);
		
			echo $html;
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
	}
	
	public function save_task()
	{
		
		try
		{	
			$flag 	= 0;
			$msg  	= '';
			$params = get_params();

			$this->_validate($params);
			
			CEIS_Model::beginTransaction();
			
			$where			 = array('request_task_id' => $params['request_task_id']);	
			$task_details	 = $this->crtm->get_task($where);
			$audit_table[]	 = CEIS_MODEL::tbl_request_tasks;
			$prev_detail[]	 = array($task_details);
			$audit_action[]	 = AUDIT_UPDATE;
			$audit_schema[]	 = DB_CEIS;
			$activity		 = "Request task has been updated.";
			
			$this->crtm->update_request_task($params);
			
			if($params['task_status'] == REQUEST_TASK_DONE)
			{
				
				if($task_details['generate_reference_flag']):
					$new_ref_details = $this->crtm->get_new_reference_no();
					$new_ref_no      = $new_ref_details['new_reference_no'];
					$this->crtm->update_reference_no($task_details['request_id'], $new_ref_no);
					
					$this->load->library('rest', array('server' => PORTAL_API_SERVER), 'portal_api');
					
					$api      = array();
					$api['params'] = array('reference_no' => $new_ref_no);
					$api['where']  = array('request_id'   => $params['request_id']);
					$wsData   = $this->portal_api->post('update_request.json', $api);
					$response = convert_object_to_array($wsData);
						
					if($response == NULL || $response['status'] == 0) throw new Exception('Reference API Error!'.var_export($response, TRUE));
				endif;
				
	 			if($task_details['generate_order_of_payment']):
					$this->load->library('rest', array('server' => CS_API_SERVER), 'cs_api');
				
					$api 			   = array();
					$where_arr  	   = array('request_id' => $task_details['request_id']);
					$api['ref_prefix'] = 'CEIS-'.$task_details['request_id'].'-';
					$api['op_prefix']  = date('Y-m').'-'.$this->session->userdata('office_code').'-';
					$api['header']     = $this->crtm->get_order_of_payment_header($where_arr);
					$api['details']    = $this->crtm->get_order_of_payment_details($task_details['request_id']);
				 	
					
					if(EMPTY($api['header']) || EMPTY($api['details'])) throw new Exception('Please create at least one order of payment.');
					
					$wsData   = $this->cs_api->post('insert_op.json', $api);
					$response = convert_object_to_array($wsData); 
					
					if($response == NULL || $response['status'] == 0) throw new Exception('Order of Payment API Error!'.var_export($response, TRUE));
				endif; 
				
				if($task_details['get_board_decision']):
					$this->crtm->update_request_status($params);
					
					$request_details = $this->crtm->get_request_details($task_details['request_id']);
					
					if(in_array($request_details['request_type_id'], array(REQUEST_TYPE_NEW, REQUEST_TYPE_REN)))
					{
						$where_arr 	= array('request_id' => $task_details['request_id']);
						$request_registration = $this->crtm->get_request_registration($where_arr);
						
						if(EMPTY($request_registration)) throw new Exception('Please update registration first!');
					}
					
					if($params['board_decision'] == REQUEST_APPROVED)
					{	
						$contractor_id   = $this->_transfer_data_to_contractor($request_details);

						switch($request_details['request_type_id']):
							case REQUEST_TYPE_NEW :
									$where_arr 	= array('contractor_id' => $contractor_id, 'registration_status_id' => REG_STATUS_ACTIVE);
						
									$this->crtm->insert_registration($task_details['request_id'], $contractor_id);
									
									$data['registration'] = $this->crtm->get_registration_details($where_arr);
								break;
							case REQUEST_TYPE_REN :
									$where_arr 	= array('contractor_id' => $contractor_id, 'registration_status_id' => REG_STATUS_ACTIVE);
									$upd_params = array('registration_status_id' => REG_STATUS_EXPIRED);
									
									$this->crtm->update_registration($upd_params, $where_arr);
									$this->crtm->insert_registration($task_details['request_id'], $contractor_id);
									
									$data['registration'] = $this->crtm->get_registration_details($where_arr);
								break;
							case REQUEST_TYPE_PC  :
									$where_arr 		  = array('request_id' => $task_details['request_id'], 'contract_status_id' => CONTRACT_STATUS_AUTHORIZATION);
									$contract_details = $this->crtm->get_request_contract($where_arr);
									
									//$this->crtm->insert_contractor_contracts($contract_details['rqst_contract_id'], $task_details['request_id'], $contractor_id);
									//$this->crtm->insert_contractor_projects($contract_details['contract_id'], $task_details['request_id']);
									$this->crtm->insert_contractor_projects_dtl($contract_details['rqst_contract_id'], $task_details['request_id']);
									$this->crtm->insert_contractor_schedules($contract_details['rqst_contract_id'], $task_details['request_id']);	
									
									
									$fields    = array('contract_status_id' => CONTRACT_STATUS_AUTHORIZED);
									$where_arr = array('contractor_id' => $contractor_id, 'contract_id' => $contract_details['rqst_contract_id']);
										
									$this->crtm->update_contractor_contract($fields, $where_arr);
								break;
							case REQUEST_TYPE_MSC :
									$where_arr 		  = array('contractor_id' => $contractor_id, 'contract_status_id' => CONTRACT_STATUS_AUTHORIZATION);
									$contract_details = $this->crtm->get_contractor_contract($where_arr);
									
									$this->crtm->insert_contractor_services($contract_details['contract_id'], $task_details['request_id']);
									$this->crtm->insert_contractor_services_dtl($contract_details['contract_id'], $task_details['request_id']);
									$this->crtm->insert_contractor_schedules($contract_details['contract_id'], $task_details['request_id']);
									
									$fields    = array('contract_status_id' => CONTRACT_STATUS_AUTHORIZED);
									$where_arr = array('contractor_id' => $contractor_id, 'contract_id' => $contract_details['contract_id']);
									
									$this->crtm->update_contractor_contract($fields, $where_arr); 
								break;
						endswitch;
						
						$where_arr = array('contractor_id' => $contractor_id);
						
						$data['contractor_id'] 	   = $contractor_id;
						$data['request_id']    	   = $request_details['request_id'];
						$data['request_type_id']   = $request_details['request_type_id'];
						$data['request_status_id'] = $params['board_decision'];
						
						$data['op_header']         = $this->crtm->get_order_of_payments($where_arr);
						$data['op_details']    	   = $this->crtm->get_order_of_payment_collections($contractor_id);
						$data['email_data']  	   = $this->crtm->get_email_and_add_data($params);
						
						
						$api = $this->ceis_portal_api->post('transfer_contractor_details.json', $data);
						$response = convert_object_to_array($api);
						
						if($api == NULL || $response['status'] == 0):
							throw new Exception('API ERROR'.var_export($response, TRUE));
						endif;
					
					}
					
					  
/* 					$add_data  = $this->crtm->get_email_and_add_data($params);
					
					$this->_email_client($add_data['rep_email'], $add_data); */
					
				endif;
			}	
		
			$task_details 	 = $this->crtm->get_task($where);
			$curr_detail[]	 = array($task_details);
			
			$this->audit_trail->log_audit_trail(
					$activity,
					$this->module,
					$prev_detail,
					$curr_detail,
					$audit_action,
					$audit_table,
					$audit_schema
					);
				
			CEIS_Model::commit();
	
			$flag = 1;
			$msg  = sprintf($this->lang->line('data_spec_saved'), 'Task');
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
			
			$ms  = $this->rlog_error($e, TRUE);
			$msg = $this->lang->line('err_internal_server').$ms;
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
				"flag" => $flag,
				"msg" => $msg
		);
		
		echo json_encode($info);
	}
	

	private function _transfer_data_to_contractor($request_details)
	{
		try
		{
			$request_id = $request_details['request_id'];
			$params 	= array('user_id' => $this->user_id, 'request_id' => $request_id);
			
			//$request_details = $this->crtm->get_request_details($request_id);
			$request_type_id = $request_details['request_type_id'];
			$contractor_name = $request_details['reference_name'];
			
			//GETS CONTRACTOR ID
			switch($request_type_id):
				case REQUEST_TYPE_NEW:
					$contractor_id = $this->crtm->insert_contractor($params);
				break;
				case REQUEST_TYPE_REN:
					$registration_details = $this->crtm->get_registration_details(array('registration_no' => $request_details['pocb_registration_no']));
			   		$contractor_id  	  = $registration_details['contractor_id'];
				break;
				case REQUEST_TYPE_PC:
				case REQUEST_TYPE_MSC:
					$contractor_details   = $this->crtm->get_contractor(array('contractor_name' => $contractor_name), array('contractor_id'));
					$contractor_id = $contractor_details['contractor_id'];
					
					
				break;
				case REQUEST_TYPE_EQP:
					$contractor_id = $this->crtm->insert_contractor($params);
				break;
			endswitch;
			/* if(REQUEST_TYPE_NEW == $request_type_id):
				  $contractor_id = $this->crtm->insert_contractor($params);
			else:
				$registration_details = $this->crtm->get_registration_details(array('registration_no' => $request_details['pocb_registration_no']));
			
			   	$contractor_id  = $registration_details['contractor_id']; 
			endif; */
						
			$tables  = $this->_get_table_per_application_type($request_type_id);
			
			foreach($tables as $index => $val):
				$actions  = $val['actions'];
				$from_tbl = $val['from'];
				$to_tbl   = $val['to'];

				if(in_array(ACTION_DELETE, $actions)):
					$where = array('contractor_id' => $contractor_id);
				
		/* 			if(ISSET($val['joined_w_request']))
					{
						$parent_tbl = $val['joined_w_request']['parent_tbl'];
						$parent_col = $val['joined_w_request']['column'];
						
						$this->crtm->delete_contractor_data_joined($parent_tbl, $to_tbl, $parent_col, $contractor_id);
					}
					else
					{ */
						$this->crtm->delete_contractor_data($to_tbl, $where);
					/* } */
				endif; 
				
				if(in_array(ACTION_ADD, $actions)):
					$tbl_columns = $this->crtm->_get_columns($from_tbl);
					$columns 	 = array_column($tbl_columns, 'COLUMN_NAME');
					
					array_walk($columns, function(&$val, $key, $prefix){
							$val = $prefix.$val;
					}, 'a.');
					
					$rid_key 	 = array_search('a.request_id', $columns);
						
					if( ! ISSET($val['joined_w_request']) && $rid_key !== FALSE)
						$columns[$rid_key] = $contractor_id;
					elseif(ISSET($val['joined_w_request']) && $rid_key !== FALSE)
						unset($columns[$rid_key]);
					
						
					//throw new Exception($contractor_id.$rid_key.'<pre>'.var_export($columns, TRUE).'</pre>');	
					$select_columns    = implode(',', $columns);

					if(ISSET($val['joined_w_request']))
					{
						$this->crtm->insert_contractor_data_joined_request($from_tbl, $to_tbl, $select_columns, $request_id, $val['joined_w_request']);
					}
					else
					{
						$this->crtm->insert_contractor_data($from_tbl, $to_tbl, $select_columns, $request_id);
					}
				endif;
				
				/* if(in_array(ACTION_EDIT, $actions)):
					$this->crtm->update_contractor_data($from, $to);
				endif; */
			endforeach;
			
			return $contractor_id;
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _get_table_per_application_type($request_type)
	{
		//Try lang to hehehe. Object instance ginamit ko.
		$rsm    = $this->crtm;
		$tables = array();

		switch($request_type):
			case REQUEST_TYPE_NEW:
				$options  = array('actions' => array(ACTION_ADD));
				//Contractor Information
				$tables[] = array_merge($options, array('from' => $rsm::tbl_request_license_classifications, 'to' => $rsm::tbl_contractor_license_classifications)); 
				//Contracts
				$tables[] = array_merge($options, array('from' => $rsm::tbl_request_contracts, 'to' => $rsm::tbl_contractor_contracts));
				$tables[] = array_merge($options, array('from' => $rsm::tbl_request_projects,  'to' => $rsm::tbl_contractor_projects, 'joined_w_request' => array('parent_tbl' => $rsm::tbl_request_contracts , 'column' => 'rqst_contract_id', 'add_and' => 'AND a.request_id = b.request_id')));
				$tables[] = array_merge($options, array('from' => $rsm::tbl_request_services,  'to' => $rsm::tbl_contractor_services, 'joined_w_request' => array('parent_tbl' => $rsm::tbl_request_contracts , 'column' => 'rqst_contract_id', 'add_and' => 'AND a.request_id = b.request_id')));
				//Staffing
				$tables[] = array_merge($options, array('from' => $rsm::tbl_request_staff, 'to' => $rsm::tbl_contractor_staff));
				$tables[] = array_merge($options, array('from' => $rsm::tbl_request_staff_licenses, 'to' => $rsm::tbl_contractor_staff_licenses, 'joined_w_request' => array('parent_tbl' => $rsm::tbl_request_staff , 'column' => 'rqst_staff_id', 'add_and' => 'AND a.request_id = b.request_id')));
				$tables[] = array_merge($options, array('from' => $rsm::tbl_request_staff_projects, 'to' => $rsm::tbl_contractor_staff_projects, 'joined_w_request' => array('parent_tbl' => $rsm::tbl_request_staff , 'column' => 'rqst_staff_id', 'add_and' => 'AND a.request_id = b.request_id')));
				$tables[] = array_merge($options, array('from' => $rsm::tbl_request_staff_experiences, 'to' => $rsm::tbl_contractor_staff_experiences, 'joined_w_request' => array('parent_tbl' => $rsm::tbl_request_staff , 'column' => 'rqst_staff_id', 'add_and' => 'AND a.request_id = b.request_id')));
				//Finance
				$tables[] = array_merge($options, array('from' => $rsm::tbl_request_statements, 'to' => $rsm::tbl_contractor_statement));
				//Construction Equipment
				$tables[] = array_merge($options, array('from' => $rsm::tbl_request_equipment, 'to' => $rsm::tbl_equipment));
				//Payments
				$tables[] = array_merge($options, array('from' => $rsm::tbl_request_payments, 'to' => $rsm::tbl_contractor_payments));
				$tables[] = array_merge($options, array('from' => $rsm::tbl_request_payment_collections, 'to' => $rsm::tbl_contractor_payment_collections, 'joined_w_request' => array('parent_tbl' => $rsm::tbl_request_payments , 'column' => 'rqst_payment_id')));
				break;
			case REQUEST_TYPE_REN :
				$add_only  = array('actions' => array(ACTION_ADD));
				$options   = array('actions' => array(ACTION_DELETE, ACTION_ADD));
				
				$tables[]  = array_merge($options, array('from' => $rsm::tbl_request_license_classifications, 'to' => $rsm::tbl_contractor_license_classifications));
				//Contracts
				$tables[]  = array_merge($options, array('from' => $rsm::tbl_request_contracts, 'to' => $rsm::tbl_contractor_contracts));
				$tables[]  = array_merge($add_only, array('from' => $rsm::tbl_request_projects,  'to' => $rsm::tbl_contractor_projects, 'joined_w_request' => array('parent_tbl' => $rsm::tbl_request_contracts , 'column' => 'rqst_contract_id', 'add_and' => 'AND a.request_id = b.request_id')));
				$tables[]  = array_merge($add_only, array('from' => $rsm::tbl_request_services,  'to' => $rsm::tbl_contractor_services, 'joined_w_request' => array('parent_tbl' => $rsm::tbl_request_contracts , 'column' => 'rqst_contract_id', 'add_and' => 'AND a.request_id = b.request_id')));
				//Staffing
				$tables[]  = array_merge($options, array('from' => $rsm::tbl_request_staff, 'to' => $rsm::tbl_contractor_staff));
				$tables[]  = array_merge($add_only, array('from' => $rsm::tbl_request_staff_licenses, 'to' => $rsm::tbl_contractor_staff_licenses, 'joined_w_request' => array('parent_tbl' => $rsm::tbl_request_staff , 'column' => 'rqst_staff_id', 'add_and' => 'AND a.request_id = b.request_id')));
				$tables[]  = array_merge($add_only, array('from' => $rsm::tbl_request_staff_projects, 'to' => $rsm::tbl_contractor_staff_projects, 'joined_w_request' => array('parent_tbl' => $rsm::tbl_request_staff , 'column' => 'rqst_staff_id', 'add_and' => 'AND a.request_id = b.request_id')));
				$tables[]  = array_merge($add_only, array('from' => $rsm::tbl_request_staff_experiences, 'to' => $rsm::tbl_contractor_staff_experiences, 'joined_w_request' => array('parent_tbl' => $rsm::tbl_request_staff , 'column' => 'rqst_staff_id', 'add_and' => 'AND a.request_id = b.request_id')));
				//Finance
				$tables[]  = array_merge($options, array('from' => $rsm::tbl_request_statements, 'to' => $rsm::tbl_contractor_statement));
				//Construction Equipment
				$tables[]  = array_merge($options, array('from' => $rsm::tbl_request_equipment, 'to' => $rsm::tbl_equipment));
				//Payments
				$tables[]  = array_merge($add_only, array('from' => $rsm::tbl_request_payments, 'to' => $rsm::tbl_contractor_payments));
				$tables[]  = array_merge($add_only, array('from' => $rsm::tbl_request_payment_collections, 'to' => $rsm::tbl_contractor_payment_collections, 'joined_w_request' => array('parent_tbl' => $rsm::tbl_request_payments , 'column' => 'rqst_payment_id')));
				break;
			case REQUEST_TYPE_PC :
				$add_only  = array('actions' => array(ACTION_ADD));
				$options   = array('actions' => array(ACTION_DELETE, ACTION_ADD));
				$tables[]  = array_merge($add_only, array('from' => $rsm::tbl_request_contracts, 'to' => $rsm::tbl_contractor_contracts));
				//Manpower Schedule 
				//$tables[]  = array_merge($add_only, array('from' => $rsm::tbl_request_schedule, 'to' => $rsm::tbl_contractor_contracts_schedule));
				//Payments
				$tables[]  = array_merge($add_only, array('from' => $rsm::tbl_request_payments, 'to' => $rsm::tbl_contractor_payments));
				$tables[]  = array_merge($add_only, array('from' => $rsm::tbl_request_payment_collections, 'to' => $rsm::tbl_contractor_payment_collections, 'joined_w_request' => array('parent_tbl' => $rsm::tbl_request_payments , 'column' => 'rqst_payment_id')));
				break;
			case REQUEST_TYPE_MSC :
				$add_only  = array('actions' => array(ACTION_ADD));
				$options   = array('actions' => array(ACTION_DELETE, ACTION_ADD));
				//Contracts
				$tables[]  = array_merge($options, array('from' => $rsm::tbl_request_contracts, 'to' => $rsm::tbl_contractor_contracts));
				$tables[]  = array_merge($add_only, array('from' => $rsm::tbl_request_projects,  'to' => $rsm::tbl_contractor_projects, 'joined_w_request' => array('parent_tbl' => $rsm::tbl_request_contracts , 'column' => 'rqst_contract_id', 'add_and' => 'AND a.request_id = b.request_id')));
				$tables[]  = array_merge($add_only, array('from' => $rsm::tbl_request_services,  'to' => $rsm::tbl_contractor_services, 'joined_w_request' => array('parent_tbl' => $rsm::tbl_request_contracts , 'column' => 'rqst_contract_id', 'add_and' => 'AND a.request_id = b.request_id')));
				//Manpower Schedule 
				//$tables[]  = array_merge($add_only, array('from' => $rsm::tbl_request_schedule, 'to' => $rsm::tbl_contractor_contracts_schedule));
				//Payments
				$tables[]  = array_merge($add_only, array('from' => $rsm::tbl_request_payments, 'to' => $rsm::tbl_contractor_payments));
				$tables[]  = array_merge($add_only, array('from' => $rsm::tbl_request_payment_collections, 'to' => $rsm::tbl_contractor_payment_collections, 'joined_w_request' => array('parent_tbl' => $rsm::tbl_request_payments , 'column' => 'rqst_payment_id')));
				break;
			case REQUEST_TYPE_EQP :
				//Construction Equipment
				$tables[] =  $rsm::tbl_ceis_request_equipment;
				//Checklist
				$tables[] =  $rsm::tbl_ceis_request_checklist;
				$tables[] =  $rsm::tbl_ceis_request_attachments;
				break;
			default:
				throw new Exception('No request type defined!');
			endswitch;
	
			return $tables;
	}
	
	private function _validate(&$params)
	{
		
		if( ! ISSET($params['task_status']) || EMPTY($params['task_status'])):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Task Status'));
		endif;
		
	
		$tokens = explode('/', $params['security']);
		
		$params['request_task_id'] = base64_url_decode($tokens[0]);
		$params['salt'] 	 	   = $tokens[1];
		$params['token'] 	  	   = $tokens[2];
		
		check_salt($params['request_task_id'], $params['salt'], $params['token']);
		
		
		$tokens = explode('/', $params['request_id']);
		
		$params['request_id']  = base64_url_decode($tokens[0]);
		$params['salt'] 	   = $tokens[1];
		$params['token'] 	   = $tokens[2];
		
		check_salt($params['request_id'], $params['salt'], $params['token']);
		
		$params['assigned_to'] = $this->user_id; 
	}
	
	
	public function save_task_remark()
	{
		try
		{
			$flag 	= 0;
			$msg  	= '';
			$params = get_params();
				
			$this->_validate_remarks($params);
	
			CEIS_Model::beginTransaction();
			
			$audit_table[]	 = CEIS_MODEL::tbl_request_tasks;
			$audit_schema[]	 = DB_CEIS;
			
			if( ! EMPTY($params['task_remark_id'])):
				$remarks_details = $this->crtm->get_task_remark($params['task_remark_id']);
				$prev_detail[]	 = array($remarks_details);
				$audit_action[]	 = AUDIT_UPDATE;
				$activity		 = "Request task remark has been updated.";
				
				$this->crtm->update_task_remarks($params);
			else:
				$prev_detail[]	 = array();
				$audit_action[]	 = AUDIT_INSERT;
				$activity		 = "Request task remark has been inserted.";
			
				$params['task_remark_id'] = $this->crtm->insert_task_remarks($params);
			endif;
			
			$remark_details = $this->crtm->get_task_remark($params['task_remark_id']);
			$curr_detail[]  = array($remark_details);
	
			$this->audit_trail->log_audit_trail(
					$activity,
					$this->module,
					$prev_detail,
					$curr_detail,
					$audit_action,
					$audit_table,
					$audit_schema
				);
			
			CEIS_Model::commit();
	
			$flag = 1;
			$msg  = sprintf($this->lang->line('data_spec_saved'), 'Task');
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
				
			$this->rlog_error($e);
			$msg = $this->lang->line('err_internal_server');
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();
				
			$msg = $this->rlog_error($e, TRUE);
		}
	
		$info = array(
				"flag" => $flag,
				"msg" => $msg
		);
	
		echo json_encode($info);
	}
	
	private function _validate_remarks(&$params)
	{
	
		if( ! ISSET($params['remarks']) || EMPTY($params['remarks'])):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Remarks'));
		endif;
	
		/* SECURITY */
		$tokens = explode('/', $params['security']);
	
		$params['request_task_id'] = base64_url_decode($tokens[0]);
		$params['salt'] 	 	   = $tokens[1];
		$params['token'] 	  	   = $tokens[2];
	
		check_salt($params['request_task_id'], $params['salt'], $params['token']);

		/* REQUEST ID */
	 	$tokens = explode('/', $params['request_id']);

		$params['request_id']  = base64_url_decode($tokens[0]);
		$params['salt'] 	   = $tokens[1];
		$params['token'] 	   = $tokens[2];
	
		check_salt($params['request_id'], $params['salt'], $params['token']);
	
		/* TASK REMARK ID */
		if( ! EMPTY($params['task_remark_id'])):
			$tokens = explode('/', $params['task_remark_id']);
			
			$params['task_remark_id'] = base64_url_decode($tokens[0]);
			$params['salt'] 	 	   = $tokens[1];
			$params['token'] 	  	   = $tokens[2];
			
			check_salt($params['task_remark_id'], $params['salt'], $params['token']);
		endif;
		
		$params['created_by'] = $this->user_id; 
	}
	
	
	public function delete_remarks()
	{
		try 
		{
			$flag  	     = 0;
			$msg    	 = '';
			$params 	 = get_params();
			$tokens 	 = explode('/', $params['param_1']);
			$id 		 = base64_url_decode($tokens[0]); 
			
			check_salt($id, $tokens[1], $tokens[2]);
			
			CEIS_Model::beginTransaction();
			
			$audit_table	 = array(CEIS_MODEL::tbl_request_task_remarks);
			$audit_schema	 = array(DB_CEIS);
			$audit_action    = array(AUDIT_DELETE);
			$remark_details  = $this->crtm->get_task_remark($id);
			
			$prev_detail[]   = array($remark_details);
			
			$this->crtm->delete_task_remark($id);
			
			$activity	     = "Request task remarks has been deleted.";
			$curr_detail[]   = array();
				
			$this->audit_trail->log_audit_trail(
					$activity,
					$this->module,
					$prev_detail,
					$curr_detail,
					$audit_action,
					$audit_table,
					$audit_schema
			);

		
			CEIS_Model::commit();
			
			$flag  = 1;
			$msg   = sprintf($this->lang->line('data_spec_deleted'), 'Remarks');
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
				
			$this->rlog_error($e);
			
			$msg = $this->lang->line('err_internal_server');
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();
				
			$msg = $this->rlog_error($e, TRUE);
		}
		
		echo json_encode( array('flag' => $flag, 'msg' => $msg, 'reload' => 'fnreload', 'table_id' => 'table_remarks') );
	}
	
	public function delete_attachment()
	{
		try
		{
			$flag  	     = 0;
			$msg    	 = '';
			$params 	 = get_params();
			$tokens 	 = explode('/', $params['param_1']);
			$id 		 = base64_url_decode($tokens[0]);
				
			check_salt($id, $tokens[1], $tokens[2]);
				
			CEIS_Model::beginTransaction();
				
			$audit_table	 = array(CEIS_MODEL::tbl_request_task_attachments);
			$audit_schema	 = array(DB_CEIS);
			$audit_action    = array(AUDIT_DELETE);
			$attachment_details = $this->crtm->get_task_attachment($id);
				
			$prev_detail[]   = array($attachment_details);
			
			$filepath 		 = PATH_UPLOADS_REQUESTS.$attachment_details['request_id'].'/'.$attachment_details['file_name']; 
			$this->file->delete_file($filepath);
			
			$this->crtm->delete_task_attachment($id);
				
			$activity	     = "Request task attachment has been deleted.";
			$curr_detail[]   = array();
	
			$this->audit_trail->log_audit_trail(
					$activity,
					$this->module,
					$prev_detail,
					$curr_detail,
					$audit_action,
					$audit_table,
					$audit_schema
					);
	
	
			CEIS_Model::commit();
				
			$flag  = 1;
			$msg   = sprintf($this->lang->line('data_spec_deleted'), 'Attachment');
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
	
			$this->rlog_error($e);
				
			$msg = $this->lang->line('err_internal_server');
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();
	
			$msg = $this->rlog_error($e, TRUE);
		}
	
		echo json_encode( array('flag' => $flag, 'msg' => $msg, 'reload' => 'fnreload', 'table_id' => 'table_attachments') );
	}
	
	public function save_task_attachment()
	{
		try
		{
			$flag 	= 0;
			$msg  	= '';
			$params = get_params();
			
			$this->_validate_attachment($params);
			
			CEIS_Model::beginTransaction();
				
			$audit_table[]	 = CEIS_MODEL::tbl_request_task_attachments;
			$audit_schema[]	 = DB_CEIS;
				
			if( ! EMPTY($params['task_attachment_id'])):
				$attachment_details = $this->crtm->get_task_attachment($params['task_attachment_id']);
			
				if($attachment_details['file_name'] != $params['filename']):
					$filepath  = PATH_UPLOADS_REQUESTS.$attachment_details['request_id'].'/'.$attachment_details['file_name'];
					$this->file->delete_file($filepath);
				endif;
				
				$prev_detail[]	 	= array($attachment_details);
				$audit_action[]	 	= AUDIT_UPDATE;
				$activity		 	= "Request task attachment has been updated.";
		
				$this->crtm->update_task_attachments($params);
			else:
				$prev_detail[]	 = array();
				$audit_action[]	 = AUDIT_INSERT;
				$activity		 = "Request task attachment has been inserted.";
					
				$params['task_attachment_id'] = $this->crtm->insert_task_attachments($params);
			endif;
				
			$attachment_details = $this->crtm->get_task_attachment($params['task_attachment_id']);
			$curr_detail[]  	= array($attachment_details);
	
			$this->audit_trail->log_audit_trail(
					$activity,
					$this->module,
					$prev_detail,
					$curr_detail,
					$audit_action,
					$audit_table,
					$audit_schema
					);
				
			CEIS_Model::commit();
	
			$flag = 1;
			$msg  = sprintf($this->lang->line('data_spec_saved'), 'Attachment');
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
	
			$this->rlog_error($e);
			$msg = $this->lang->line('err_internal_server');
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();
	
			$msg = $this->rlog_error($e, TRUE);
		}
	
		$info = array(
				"flag" => $flag,
				"msg" => $msg
		);
	
		echo json_encode($info);
	}
	
	private function _validate_attachment(&$params)
	{
 		if( ! ISSET($params['description']) || EMPTY($params['description'])):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Description'));
		endif;

		if( ! ISSET($params['date_received']) || EMPTY($params['date_received'])):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Date Received'));
		endif;
		
		if( ! ISSET($params['document_type']) || EMPTY($params['document_type'])):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Document Type'));
		endif;
		
		if( (! ISSET($params['filename']) || EMPTY($params['filename'])) ):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Filename'));
		endif;
		
		/* SECURITY */
		$tokens = explode('/', $params['security']);
	
		$params['request_task_id'] = base64_url_decode($tokens[0]);
		$params['salt'] 	 	   = $tokens[1];
		$params['token'] 	  	   = $tokens[2];
	
		check_salt($params['request_task_id'], $params['salt'], $params['token']);
	
		/* REQUEST ID */
		$tokens = explode('/', $params['request_id']);
	
		$params['request_id']  = base64_url_decode($tokens[0]);
		$params['salt'] 	   = $tokens[1];
		$params['token'] 	   = $tokens[2];
	
		check_salt($params['request_id'], $params['salt'], $params['token']);
	
		/* TASK ATTACHMENT ID */
		if( ! EMPTY($params['task_attachment_id'])):
			$tokens = explode('/', $params['task_attachment_id']);
				
			$params['task_attachment_id'] = base64_url_decode($tokens[0]);
			$params['salt'] 	 	   = $tokens[1];
			$params['token'] 	  	   = $tokens[2];
				
			check_salt($params['task_attachment_id'], $params['salt'], $params['token']);
		endif;
	
		$params['created_by'] = $this->user_id;

	}
	
	public function download_attachment($encoded_id, $salt, $token, $filename)
	{
		try
		{
			$id = base64_url_decode($encoded_id);
			
			check_salt($id, $salt, $token);
			
			$filepath = PATH_UPLOADS_REQUESTS.$id.'/'.$filename;
			
			$this->file->download_file($filepath, $filename);
		}
		catch(Exception $e)
		{
			$msg =  $this->rlog_error($e, TRUE);
			
			echo $msg;
		}
	}
	
	private function _email_client($email, $add_data=array())
	{
		try
		{
			$params = array();
			$params["fields"] 	 = array('sys_param_name', 'sys_param_value');
			$params["where"] 	 = array('sys_param_type' => SYS_PARAM_TYPE_SMTP);
			$params["multiple"] = TRUE;
			$email_params 		 = get_values('sys_param_model', 'get_sys_param', $params, PROJECT_CORE);
			
			//Ni try ko lang para hindi na ko may for loop :) :*
			$formatted_params   = array_column($email_params, 'sys_param_value', 'sys_param_name');
		
			$email_data = array();
			$email_data['subject']    = 'Result of application';
			$email_data['to_email']   = array($email);
			$email_data['from_name']  = $formatted_params[SYS_PARAM_TYPE_SMTP_REPLY_NAME];
			$email_data['from_email'] = $formatted_params[SYS_PARAM_TYPE_SMTP_REPLY_EMAIL];
	
			$this->email_template->send_email_template($email_data, 'request_result', $add_data);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
}

/* End of file Ceis_dashboard.php */
/* Location: ./application/modules/ceis/controllers/Ceis_dashboard.php */