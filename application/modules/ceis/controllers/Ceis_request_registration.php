<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ceis_request_registration extends Ceis_Controller
{
	private $module = MODULE_CEIS_REQUESTS;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ceis_request_registration_model', 'crrm');
	
	}

	public function save_registration()
	{
		try
		{	
			$flag 	= 0;
			$msg  	= '';
			$params = get_params();
			
			$this->_validate($params);
			
			$registration_details = $this->crrm->get_registration_details($params['request_id']);
			
			CEIS_Model::beginTransaction();
			
			$audit_table[]	= CEIS_MODEL::tbl_request_registration;
			$audit_schema[]	= DB_CEIS;
			$prev_detail[]	= $evaluation_details;
			
			//$request_details  = $this->crtm->get_request_details($params['request_id']);
		/* 	switch($request_details['request_type_id']):
				case REQUEST_TYPE_NEW :
						$params['expiry_date'] = date('Y-m-d', strtotime($params['registration_date'].' +2 year'));
					break;
				case REQUEST_TYPE_REN :
						$params['expiry_date'] = date('Y-m-d', strtotime($params['expiry_date'].' +2 year'));
					break;
			endswitch; */
			
			if( ! EMPTY($registration_details)):
				$audit_action[]	= AUDIT_UPDATE;
			
				$this->crrm->update_request_registration($params, $where);
				
				$activity	= "Request registration has been updated.";
			else:
				$audit_action[]	= AUDIT_INSERT;
			
				$this->crrm->insert_request_registration($params, $where);
				
				$activity	= "Request registration has been added.";
			endif;

			$evaluation_details = $this->crrm->get_registration_details($params['request_id']);
			$curr_detail[]		= array($evaluation_details);
			
			$this->audit_trail->log_audit_trail(
					$activity,
					$this->module,
					$prev_detail,
					$curr_detail,
					$audit_action,
					$audit_table,
					$audit_schema
			);
			
			CEIS_Model::commit();
			
			$flag = 1;
			$msg  = sprintf($this->lang->line('data_spec_saved'), 'Evaluation');
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
			
			$this->rlog_error($e);
			$msg = $this->lang->line('err_internal_server');
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
				"flag" => $flag,
				"msg" => $msg
		);
		
		echo json_encode($info);
	}
	

	
	private function _validate(&$params)
	{
		if( ! ISSET($params['registration_no']) || EMPTY($params['registration_no'])):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Registration No'));
		elseif( filter_var($params['registration_no'], FILTER_VALIDATE_INT) === FALSE ):
			throw new Exception(sprintf($this->lang->line('invalid_data'), 'Registration No'));
		endif;
		
		if( ! ISSET($params['registration_date']) || EMPTY($params['registration_date']) ):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Registration Date'));
		endif;
		
		if( ! ISSET($params['expiry_date']) || EMPTY($params['expiry_date'])):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Expiry Date'));
		endif;
		
		//print_r($params);
		//if(EMPTY($params['last_renewal_date'])) $params['last_renewal_date'] = DEFAULT_DATE;
		//print_r($params);
		$tokens = explode('/', $params['request_id']);
		
		$params['request_id'] = base64_url_decode($tokens[0]);
		$params['salt'] 	  = $tokens[1];
		$params['token'] 	  = $tokens[2];
		
		check_salt($params['request_id'], $params['salt'], $params['token']);
	}
}

/* End of file Ceis_dashboard.php */
/* Location: ./application/modules/ceis/controllers/Ceis_dashboard.php */