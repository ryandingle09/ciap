<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ceis_dashboard extends Ceis_Controller
{
	private $module = MODULE_CEIS_DASHBOARD;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('dashboard_model', 'dashboard');
	
	}

	public function index()
	{
		try
		{	
			$data 					= array(); 
			$resources 				= array();

		}

		catch(Exception $e)
		{
			$this->rlog_error($e);

			echo $e->getMessage();
		}
		catch(PDOException $e){

			$this->rlog_error($e);
			
			$this->get_user_message($e);
		}

		$this->template->load( 'dashboard', $data, $resources);
	}
}

/* End of file Ceis_dashboard.php */
/* Location: ./application/modules/ceis/controllers/Ceis_dashboard.php */