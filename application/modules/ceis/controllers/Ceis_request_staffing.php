<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ceis_request_staffing extends Ceis_Controller
{
	private $module = MODULE_CEIS_REQUESTS;

	public function __construct()
	{
		parent::__construct();
	
		$this->load->model('ceis_request_staffing_model', 'crsm');
	}
	
	public function get_staff_list($req_id, $salt, $token)
	{
		try 
		{
			$rows		 = array();
			$params	 	 = get_params();
			$request_id  = base64_url_decode($req_id);
				
			check_salt($request_id, $salt, $token);
		
		
			$staffs 	= $this->crsm->get_staff_list($params, $request_id);
			
			foreach($staffs['aaData'] as $aRow):
				$salt    = gen_salt();
				$token   = in_salt($aRow['rqst_staff_id'], $salt);
				$info    = base64_url_encode(base64_url_encode($aRow['rqst_staff_id'])."/".$salt."/".$token);
				 
				$action  = "<div class='table-actions'>";
				$action .= "<a href='javascript:;' data-info='".$info."' class='edit tooltipped view-staff-info' data-tooltip='View' data-position='bottom' data-delay='50'><i class='flaticon-search95'></i></a>";
				$action .= "</div>";
				
				switch($aRow['staff_type']):
					case STAFF_TYPE_DIRECTOR  :
						$staff_type = 'Director';
						break;
					case STAFF_TYPE_PRINCIPAL :
						$staff_type = 'Principal Officer';
						break;
					case STAFF_TYPE_TECHNICAL :
						$staff_type = 'Key Technical Staff';
						break;
					default:
						$staff_type = '';
				endswitch; 
				
				$rows[]   = array($aRow['staff_name'], $staff_type, $aRow['no_years'], $aRow['status'], $action);
			endforeach;
		
			$staffs['aaData'] = $rows;
		
			echo json_encode($staffs);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
	}
	
	public function get_staff_work_exp_list($staff_id_tokens)
	{
		try 
		{
			$params   = get_params();
			$tokens   = explode('/', base64_url_decode($staff_id_tokens));
			$staff_id = base64_url_decode($tokens[0]);

			check_salt($staff_id, $tokens[1], $tokens[2]);
			
			$work_exp = $this->crsm->get_staff_work_exp_list($params, $staff_id);
			
			foreach($work_exp['aaData'] as $aRow):
				$salt    = gen_salt();
				$token   = in_salt($aRow['rqst_staff_exp_id'], $salt);
				$url     = base64_url_encode($aRow['rqst_staff_exp_id'])."/".$salt."/".$token;
				
				$action  = "<div class='table-actions'>";
				$action .= "<a href='javascript:;' class='md-trigger  tooltipped' data-tooltip='View Staff Work Exp' data-position='bottom' data-delay='50' data-modal='modal_staff_work_exp' onclick=\"modal_staff_work_exp_init('".$url."')\" ><i class='flaticon-search95'></i></a>";
				$action .= "</div>";
				
				
				$rows[]   = array($aRow['inclusive_dates'], $aRow['company_details'], $aRow['job_description'], $aRow['status'], $action);
			endforeach;
		
			$work_exp['aaData'] = $rows;
		
			echo json_encode($work_exp);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
	}

	public function get_staff_project_list($staff_id_tokens)
	{
		try
		{
			$params   = get_params();
			$tokens   = explode('/', base64_url_decode($staff_id_tokens));
			$staff_id = base64_url_decode($tokens[0]);
	
			check_salt($staff_id, $tokens[1], $tokens[2]);
				
			$project = $this->crsm->get_staff_project_list($params, $staff_id);
				
			foreach($project['aaData'] as $aRow):
				$salt    = gen_salt();
				$token   = in_salt($aRow['rqst_staff_proj_id'], $salt);
				$url     = base64_url_encode($aRow['rqst_staff_proj_id'])."/".$salt."/".$token;
		
				$action  = "<div class='table-actions'>";
				$action .= "<a href='javascript:;' class='md-trigger  tooltipped' data-tooltip='View Staff Project' data-position='bottom' data-delay='50' data-modal='modal_staff_project' onclick=\"modal_staff_project_init('".$url."')\" ><i class='flaticon-search95'></i></a>";
				$action .= "</div>"; 
		
				$rows[]   = array($aRow['project_title'], $aRow['project_location'], $aRow['owner'], $aRow['project_cost'], $action);
			endforeach;
	
			$project['aaData'] = $rows;
	
			echo json_encode($project);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
	}
	
	
	public function modal_staff_work_exp($encoded_id, $salt, $token)
	{
		try
		{
			$data = array();
			$staff_work_exp_id = base64_url_decode($encoded_id);

			check_salt($staff_work_exp_id, $salt, $token);
		
			$data['work_exp'] = $this->crsm->get_staff_work_exp(array('rqst_staff_exp_id' => $staff_work_exp_id));
			
			$html = $this->load->view('modals/request_staffing_experience',$data, TRUE);

			echo $html; 
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
	}
	
	public function modal_staff_project($encoded_id, $salt, $token)
	{
		try
		{
			
			$data = array();
			$staff_project_id = base64_url_decode($encoded_id);
	
			check_salt($staff_project_id, $salt, $token);
	
			$data['project'] = $this->crsm->get_staff_project(array('rqst_staff_proj_id' => $staff_project_id));
			
			/* echo '<pre>';
			print_r($data);
			die();  */
			
			$html = $this->load->view('modals/request_staffing_project',$data, TRUE);
	
			echo $html;
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
	}
}

/* End of file Ceis_dashboard.php */
/* Location: ./application/modules/ceis/controllers/Ceis_dashboard.php */