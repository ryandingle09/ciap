<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ceis_request_sub_manpower_schedule extends Ceis_Controller
{
	private $module = MODULE_CEIS_REQUESTS;
	private $collection_group = array();

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('ceis_request_sub_manpower_schedule_model', 'crsmsm');
		
		
	}
	
	public function modal_manpower($request_id, $encoded_id=NULL, $salt=NULL, $token=NULL)
	{
		try
		{

			$data    = array();
			$rid     = base64_url_decode($request_id);
			$rqst_sched_id = base64_url_decode($encoded_id);
			
			$data['schedule'] 	 = $this->crsmsm->get_specific_schedule($rqst_sched_id);

			$id      = base64_url_decode($encoded_id);
			
			// CHECK THE SECURITY VARIABLES
			if( $encoded_id !== NULL && $salt !== NULL && $token !== NULL) check_salt($id, $salt, $token);
			
			$data['security'] = $encoded_id.'/'.$salt.'/'.$token;

			$html  = $this->load->view('modals/request_manpower',$data, TRUE);
	
			echo $html;
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
	public function get_manpower_list($req_id)
	{
		try{
			$rows		     = array();
			$params	 		 = get_params();
			$request_id 	 = base64_url_decode($req_id);
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns 		 = array('a.request_id', 'a.skills_description','a.rqst_sched_id', 'a.salary_rate', 'a.number_required', 'a.employment_contract', 'a.monthly_breakdown');
			// APPEARS ON TABLE
			$bColumns 		 = array('skills_description','salary_rate', 'number_required', 'employment_contract', 'monthly_breakdown');
	
			$schedules 		 = $this->crsmsm->get_manpower_schedule_list($aColumns, $bColumns, $params, $request_id);
			$request_details = $this->crsmsm->get_request($request_id);
			
			foreach($schedules['aaData'] as $aRow):
				$salt    = gen_salt();
				$token   = in_salt($aRow['rqst_sched_id'], $salt);
				$url     = base64_url_encode($aRow['rqst_sched_id'])."/".$salt."/".$token;
				
				$url     = base64_url_encode($aRow['request_id'])."/".$url;
				

				$action  = "<div class='table-actions'>";
				$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_manpower' onclick=\"modal_manpower_init('".$url."')\" ><i class='flaticon-search95'></i></a>";
				
				$action .= "</div>"; 
			
				$rows[]  = array(	$aRow['skills_description'], 
									$aRow['salary_rate'], 
									$aRow['number_required'], 
									$action);
			endforeach;
				
			$schedules['aaData'] = $rows;

		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
					
		echo json_encode($schedules);
	}
}

/* End of file Ceis_request_sub_manpower_schedule.php */
/* Location: ./application/modules/ceis/controllers/Ceis_dashboard.php */