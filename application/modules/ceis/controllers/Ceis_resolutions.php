<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_resolutions extends CEIS_Controller {
	
	private $module = MODULE_CEIS_RESOLUTIONS;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('resolution_model', 'resolutions');
	}
	
	public function index()
	{	
		try{
			$data = $resources = $modal = array();
			
			$modal = array(
				'modal_resolution' => array(
					'controller'	=> __CLASS__,
					'module'		=> PROJECT_CEIS,
					'height'		=> '550px'
				)
			);

			$resources['load_css'] 		= array(CSS_DATATABLE);
			$resources['load_js'] 		= array(JS_DATATABLE);
			$resources['load_modal'] 	= $modal;
			$resources['datatable'] 	= array();
			$resources['datatable'][] 	= array('table_id' => 'table_resolution', 'path' => PROJECT_CEIS.'/ceis_resolutions/get_resolution_list');
			
			$this->template->load('resolutions', $data, $resources);
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

	}
	
	public function get_resolution_list()
	{
		try
		{
			$params = get_params();
			$cnt = 0;
			
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array(
				"a.resolution_no",
				"a.resolution_date",
				"a.title",
				"a.first_round",
				"a.last_round",
				"a.offers",
				"a.commitments",
				"a.created_by",
				"a.created_date",
				"a.modified_by",
				"a.modified_date",
			);

			// APPEARS ON TABLE
			$bColumns = array(
				"resolution_no", 
				"resolution_date", 
				"title",
				"first_round",
				"last_round");
		
			$resolutions 	= $this->resolutions->get_resolution_list($aColumns, $bColumns, $params);
			$iTotal 		= $this->resolutions->total_length();
			$iFilteredTotal = $this->resolutions->filtered_length($aColumns, $bColumns, $params);
		
			$output = array(
				"sEcho" => intval($_POST['sEcho']),
				"iTotalRecords" => $iTotal["cnt"],
				"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
				"aaData" => array()
			);
		
			foreach ($resolutions as $data):

				$action 		= "";
			
				$resolution_no 	= $data["resolution_no"];
				$hash_id 		= $this->hash($resolution_no);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("resolution", "'.$url.'")';
				
				$action = "<div class='table-actions'>";

				if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
					$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_resolution' onclick=\"modal_init('".$url."')\"></a>";
				endif;

				if (($this->permission->check_permission(MODULE_ROLE, ACTION_DELETE))) : 	  
					$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
				endif;
				
				$action .= '</div>';

				$row	= array();
				$row[] 	= $data['resolution_no'];
				$row[] 	= $data['resolution_date'];
				$row[] 	= $data['title'];
				$row[] 	= $data['first_round'];
				$row[] 	= $data['last_round'];
				$row[] 	= $action;

				$output['aaData'][] = $row;
			endforeach;
		
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		echo json_encode( $output );
	}

	public function process()
	{
		try
		{
			$flag 				= 0;
			$params 			= get_params();

			// SERVER VALIDATION
			$this->_validate($params);

	
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('resolution_no');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->resolutions->get_specific_resolution($where);
			$resolution_no	= $info['resolution_no'];

			CEIS_Model::beginTransaction();

			$resolution_title 	= $params['title'];
			IF(EMPTY($resolution_no)){
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= CEIS_Model::tbl_resolutions;
				$audit_schema[]	= DB_CEIS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				// SAVES THE RESOLUTIONS RECORD
				$this->resolutions->insert_resolutions($params);

				$activity	= "%s has been added in resolution.";
				$activity 	= sprintf($activity, $resolution_title);

				$msg  = $this->lang->line('data_saved');
			}

			else{

				$params['resolution_no']	= $resolution_no;

				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= CEIS_Model::tbl_resolutions;
				$audit_schema[]	= DB_CEIS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				// SAVES THE PROJECT TYPES RECORD
				$this->resolutions->update_resolutions($params);

				$activity	= "%s has been updated in resolution.";
				$activity 	= sprintf($activity, $resolution_title);

				$msg  = $this->lang->line('data_updated');
			}


			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table, 
				$audit_schema
			);
			
			CEIS_Model::commit();

			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
			
			$this->rlog_error($e);

			$msg = $this->get_user_message($e);
			
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();
			$msg = $this->rlog_error($e);
		}
		
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	
	}
	
	public function modal($encoded_id, $salt, $token)
	{
		try{
			$data 						= array();
			$resources 					= array();
			$resources['load_css'] 		= array(CSS_DATETIMEPICKER);
			$resources['load_js'] 		= array(JS_DATETIMEPICKER);

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('resolution_no');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->resolutions->get_specific_resolution($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['resolution_no'] 			= $info['resolution_no'];
				$data['resolution_date'] 		= $info['resolution_date'];
				$data['title'] 					= $info['title'];
				$data['first_round']			= $info['first_round'];
				$data['last_round']				= $info['last_round'];
				$data['offers']					= $info['offers'];
				$data['commitments']			= $info['commitments'];
				$data['created_by'] 			= $info['created_by'];	
				$data['created_date'] 			= $info['created_date'];
				$data["readonly_value"]			= "readonly";	

				// CREATE NEW SECURITY VARIABLES
				$resolution_no	= $info['resolution_no'];
				$hash_id 		= $this->hash($resolution_no);
			}

			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/resolutions',$data);
		$this->load_resources->get_resource( $resources );
	}
	
	public function delete_resolution($params)
	{
		
		try 
		{
			$flag				= 0;
			$msg				= "Error";
			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->_validate_security($params);

			$key 			= $this->get_hash_key('resolution_no');
			$where			= array();
			$where[$key]	= $params['hash_id'];
			
			$info 			= $this->resolutions->get_specific_resolution($where);
			
			$resolution_no 	= $info['resolution_no'];

			if(EMPTY($info['resolution_no']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			CEIS_Model::beginTransaction();
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= CEIS_Model::tbl_resolutions;
			$audit_schema[]	= DB_CEIS;			
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL

			$this->resolutions->delete_resolution($info['resolution_no']);

			// SET AS AN ARRAY
			$curr_detail[]	= array();
			
			// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $resolution_no);
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			CEIS_Model::commit();

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			CEIS_Model::rollback();

			$this->rlog_error($e);

			$msg	= $this->get_user_message($e); 	

		}
		catch(Exception $e)
		{	
			CEIS_Model::rollback();

			$this->rlog_error($e);

			$msg	= $e->getMessage(); 	
		}
		
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);
		echo json_encode($info);
	}

	//VALIDATIONS FOR FIELDS
	private function _validate(&$params)
	{ 
		try{

			$this->_validate_security($params);
		
			//VALIDATION
			if(EMPTY($params['resolution_no']))
				throw new Exception( sprintf($this->lang->line('err_data_required'), 'Resolution number') );

			if(EMPTY($params['resolution_date']))
				throw new Exception( sprintf($this->lang->line('err_data_required'), 'Resolution date') );
			
			if(EMPTY($params['title']))
				throw new Exception( sprintf($this->lang->line('err_data_required'), 'Title') );

			if(EMPTY($params['first_round']))
				throw new Exception( sprintf($this->lang->line('err_data_required'), 'First round') );

			if(EMPTY($params['last_round']))
				throw new Exception( sprintf($this->lang->line('err_data_required'), 'Last Round') );

			if(EMPTY($params['offers']))
				throw new Exception( sprintf($this->lang->line('err_data_required'), 'Offers') );

			if(EMPTY($params['commitments']))
				throw new Exception( sprintf($this->lang->line('err_data_required'), 'Commitments') );
		
		}catch(Exception $e){
				throw $e;

		}catch(PDOException $e){
			throw $e;
		}
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
		catch(PDOException $e){
			throw $e;
		}
	}
}


/* End of file Ceis_resolutions.php */
/* Location: ./application/modules/ceis/controllers/Ceis_resolutions.php */