<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_geographical_distribution extends CEIS_Controller {
	
	private $module = MODULE_CEIS_GEOGRAPHICAL_DISTRIBUTIONS;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('geographical_distribution_model', 'geographical_distribution');
	}
	
	public function index()
	{	
		try{
		$data = $resources = $modal = array();

		$modal = array(
			'modal_geographical_distribution' => array(
				'controller'	=> __CLASS__,
				'module'		=> PROJECT_CEIS
			)
		);
		
		$resources['load_css'] 		= array(CSS_DATATABLE, CSS_SELECTIZE);
		$resources['load_js'] 		= array(JS_DATATABLE, JS_SELECTIZE);
		$resources['load_modal'] 	= $modal;
		$resources['datatable'] 	= array();
		$resources['datatable'][] 	= array('table_id' => 'geographical_distribution_table', 'path' => PROJECT_CEIS.'/ceis_geographical_distribution/get_geographical_distribution_list');
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e;
		}
		
		$this->template->load('geographical_distribution', $data, $resources);
	}
	
	
	public function get_geographical_distribution_list()
	{
		try{
			$params = get_params();
			$cnt = 0;

			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array(
				"a.geo_region_id", 
				"a.region_name", 
				"a.created_by", 
				"a.created_date", 
				"a.modified_by", 
				"a.modified_date",
				"COUNT(b.geo_region_id) cnt",
				'CONCAT(c.last_name, \',  \', c.first_name) as created_name',
				'CONCAT(d.last_name, \',  \', d.first_name) as modified_name');

			// APPEARS ON TABLE
			$bColumns = array(
				"region_name", 
				'cnt',
				"created_name", 
				"created_date", 
				"modified_name", 
				"modified_date"
				);
		
			$geographical_distributions = $this->geographical_distribution->get_geographical_distribution_list($aColumns, $bColumns, $params);


			$iTotal = $this->geographical_distribution->total_length();
			$iFilteredTotal = $this->geographical_distribution->filtered_length($aColumns, $bColumns, $params);
		
			$output = array(
				"sEcho" => intval($_POST['sEcho']),
				"iTotalRecords" => $iTotal["cnt"],
				"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
				"aaData" => array()
			);
		
			foreach ($geographical_distributions as $data):
			
				$geo_region_id	= $data["geo_region_id"];

				$hash_id 		= $this->hash($geo_region_id);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$geo_cnt 		= $this->geographical_distribution->get_geographical_distributions_count($geo_region_id);
				$ctr 			= $geo_cnt['cnt'];

				$delete_action 	= 'content_delete("geographical distribution", "'.$url.'")';

				$action = "<div class='table-actions'>";

				if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
					$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-geographical_distribution='bottom' data-delay='50' data-modal='modal_geographical_distribution' onclick=\"modal_init('".$url."')\"></a>";
				endif;

				if (($this->permission->check_permission(MODULE_ROLE, ACTION_DELETE))) : 	  
					$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-geographical_distributions='bottom' data-delay='50'></a>";
				endif;
			
				$action .= '</div>';
				$row 	= array();
				$row[] 	= $data['region_name'];
				$row[] 	= $data['cnt'];
				$row[] 	= $data['created_name'];
				$row[] 	= $data['created_date'];
				$row[] 	= $data['modified_name'];
				$row[] 	= $data['modified_date'];
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			

		}
		catch(PDOException $e){
			
			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){
			
			$this->rlog_error($e);

			echo $e->getMessage();
		}
		
		echo json_encode( $output );
	}
	
	public function modal($encoded_id, $salt, $token)
	{
		try{
			$data = array();
			$resources 				= array();
			$resources['load_css'] 	= array(CSS_SELECTIZE);
			$resources['load_js'] 	= array('jquery.jscrollpane',JS_SELECTIZE);

			$data["countries"] 		= $this->geographical_distribution->get_countries();

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('geo_region_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->geographical_distribution->get_specific_geographical_distribution($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	
				
				$geo_selected_list = $this->geographical_distribution->get_selected_geo_list($where);

				
				if($geo_selected_list){
					for($i=0; $i<count($geo_selected_list); $i++){
					$geo_list_arr[] = $geo_selected_list[$i]["country_code"];
					}
				}

				$data['geo_region_id'] 			= $info['geo_region_id'];
				$data['region_name'] 			= $info['region_name'];
				$data['created_by'] 			= $info['created_by'];	
				$data['created_date'] 			= $info['created_date'];
				$data["selected_data_value"]	= count($geo_selected_list);
				$data['modified_by'] 			= $info['modified_by'];	
				$data['modified_date'] 			= $info['modified_date'];
		
				// CREATE NEW SECURITY VARIABLES
				$geo_region_id	= $info['geo_region_id'];
				$hash_id 		= $this->hash($geo_region_id);			

			}

			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			$data['geo_list_arr'] = $geo_list_arr;
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}
		
		$this->load->view('modals/geographical_distribution',$data);
		$this->load_resources->get_resource($resources);
	}
	
	public function process()
	{
		try
		{
			$flag 				= 0;
			$params 			= get_params();
			
			// SERVER VALIDATION
			$this->_validate($params);

			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('geo_region_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->geographical_distribution->get_specific_geographical_distribution($where);
			$geo_region_id	= $info['geo_region_id'];



			CEIS_Model::beginTransaction();

			$region_name = $params['region_name'];

			IF(EMPTY($geo_region_id)){
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= CEIS_Model::tbl_geo_regions;
				$audit_schema[]	= DB_CEIS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				// SAVES THE RESOLUTIONS RECORD
				$this->geographical_distribution->insert_geographical_distribution($params);

				$activity	= "%s has been added in region.";
				$activity 	= sprintf($activity, $region_name);

				$msg  = $this->lang->line('data_saved');
			}

			else{

				$params['geo_region_id']	= $geo_region_id;

				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= CEIS_Model::tbl_geo_regions;
				$audit_schema[]	= DB_CEIS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				// SAVES THE PROJECT TYPES RECORD
				$this->geographical_distribution->update_geographical_distribution($params);

				$activity	= "%s has been updated in region .";
				$activity 	= sprintf($activity, $region_name);

				$msg  = $this->lang->line('data_updated');
			}


			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table, 
				$audit_schema
			);
			
			CEIS_Model::commit();

			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
			
			$this->rlog_error($e, TRUE);
			$msg = $this->get_user_message($e);
			
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();

			$this->rlog_error($e, TRUE);
			$msg = $this->get_user_message($e);
		}
		
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	
	}
	
	private function _validate(&$params)
	{
		try
		{
			
			$this->_validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields = array();
			$fields['region_name']			= 'Region';

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _validate_input($params)
	{
		try
		{
			$validation['region_name'] = array(
					'data_type' => 'string',
					'name'		=> 'Region Name',
					'max_len'	=> 100
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function delete_geographical_distribution($params)
	{
		try 
		{

			$flag	= 0;
			$msg	= "Error";

			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->_validate_security($params);

			$key 			= $this->get_hash_key('geo_region_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->geographical_distribution->get_specific_geographical_distribution($where);

			$geo_region_id = $info['geo_region_id'];

			if(EMPTY($info['geo_region_id']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			CEIS_Model::beginTransaction();
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= CEIS_Model::tbl_geo_regions;
			$audit_schema[]	= DB_CEIS;			
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL

			$this->geographical_distribution->delete_geographical_distribution($info['geo_region_id']);

			// SET AS AN ARRAY
			$curr_detail[]	= array();
			
			// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $geo_region_id);
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			CEIS_Model::commit();

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			CEIS_Model::rollback();
			
			$this->rlog_error($e);

			$msg	= $this->get_user_message($e);	
		}
		catch(Exception $e)
		{	
			CEIS_Model::rollback();
			
			$this->rlog_error($e);
			
			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
	}
}


/* End of file Ceis_geographical_distribution.php */
/* Location: ./application/modules/sysad/controllers/Ceis_geographical_distribution.php */