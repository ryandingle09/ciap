<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ceis_request_finance extends Ceis_Controller
{
	private $module = MODULE_CEIS_REQUESTS;

	public function __construct()
	{
		parent::__construct();
		$this->load->library('file');
		
		$this->load->model('ceis_request_finance_model', 'crfm');
	}


	public function get_request_statement_list($req_id, $salt, $token)
	{
		try{
			$rows		    = array();
			$params	 		= get_params();
			$request_id 	= base64_url_decode($req_id);
			
			check_salt($request_id, $salt, $token);
	
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns 	 = array('a.statement_date', 'a.request_id', 'a.networth', 'FORMAT((a.common_shares * a.par_value), 2) as paid_up',
									'FORMAT((a.total_current_assets + a.total_non_current_assets),2) as assets', 'FORMAT((a.total_current_liabilities + a.long_term_liabilities), 2) as liabilities', 
									'DATE_FORMAT(a.statement_date, \'%%b %%d, %%Y\' ) as display_date');
			// APPEARS ON TABLE
			$bColumns 	 = array('display_date', 'assets', 'liabilities', 'networth', 'paid_up');
	
			$statements  = $this->crfm->get_statement_list($aColumns, $bColumns, $params, $request_id);
			
			foreach($statements['aaData'] as $aRow):
				$id   	  = $aRow['request_id'].'/'.$aRow['statement_date']; 
				$salt     = gen_salt();
				$token	  = in_salt($id, $salt);      
				$url      = base64_url_encode($id)."/".$salt."/".$token; 
				$action   = "<div class='table-actions'>";
				$action  .= "<a href='javascript:;' class='md-trigger  tooltipped' data-tooltip='View Finance Statement' data-position='bottom' data-delay='50' data-modal='modal_finance' onclick=\"modal_finance_init('".$url."')\" ><i class='flaticon-search95'></i></a>";
				$action  .= "</div>";
				
				$rows[]   = array($aRow['display_date'], decimal_format($aRow['assets']), decimal_format($aRow['liabilities']), decimal_format($aRow['networth']), decimal_format($aRow['paid_up']), $action);
			endforeach;
				
			$statements['aaData'] = $rows;
				
			echo json_encode($statements);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
	}
	
	
	public function modal_finance($encoded_id, $salt, $token)
	{
		try
		{	
			$id   = base64_url_decode($encoded_id);
			$ids  = explode('/', $id);
			
			$request_id     = $ids[0];
			$statement_date = $ids[1];
			
			$where = array('request_id' => $request_id, 'statement_date' => $statement_date);
			$data['finance'] = $this->crfm->get_financial_statement($where);
		
			$html = $this->load->view('modals/request_finance',$data, TRUE);
			
			echo $html; 
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
	}
	

	public function modal_view_statement($encoded_id, $salt, $token)
	{
		try
		{
			$request_id   = base64_url_decode($encoded_id);
			
			check_salt($request_id, $salt, $token);
		
				
			/* $request_id     = $ids[0];
			$statement_date = $ids[1];
				
			$where = array('request_id' => $request_id, 'statement_date' => $statement_date);
			$data['finance'] = $this->crfm->get_financial_statement($where);
	*/
			$data['view_statement'] = $this->_construct_view_statement($request_id);
			
			$html = $this->load->view('modals/view_statement',$data, TRUE);
				
			echo $html; 
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
	}
	
	
	public function _construct_view_statement($request_id)
	{
		try
		{
			$view = array();
			$statement_details = $this->crfm->get_request_statement($request_id);
				
			if( ! EMPTY($statement_details))
			{
				$thead = '<tr>';
				$tbody = array();
	
				foreach($statement_details as $sd):
					$thead 	  .= '<th class="right-align" width="20%">'.date('M d,Y', strtotime($sd['statement_date'])).'</th>';
					$tbody[0] .= '<td class="right-align">'.decimal_format($sd['total_current_assets']).'</td>';
					$tbody[1] .= '<td class="right-align">'.decimal_format($sd['total_non_current_assets']).'</td>';
					$tbody[2] .= '<td class="right-align">'.decimal_format($sd['total_current_liabilities']).'</td>';
					$tbody[3] .= '<td class="right-align">'.decimal_format($sd['long_term_liabilities']).'</td>';
					$tbody[4] .= '<td class="right-align">'.decimal_format($sd['networth']).'</td>';
					$tbody[5] .= '<td class="right-align">'.decimal_format($sd['gross_sales']).'</td>';
					$tbody[6] .= '<td class="right-align">'.decimal_format($sd['net_profit']).'</td>';
					$tbody[7] .= '<td class="right-align">'.decimal_format($sd['common_shares']).'</td>';
					$tbody[8] .= '<td class="right-align">'.decimal_format($sd['par_value']).'</td>';
				endforeach;
	
				$tbody = '<tr>'.rtrim(implode('</tr><tr>', $tbody), '<tr>');
				$view = array('thead' => $thead, 'tbody' => $tbody);
			}
				
			return $view;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
}

/* End of file Ceis_dashboard.php */
/* Location: ./application/modules/ceis/controllers/Ceis_dashboard.php */