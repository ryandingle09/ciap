<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_country_profile extends CEIS_Controller {
	
	private $module = MODULE_COUNTRY_PROFILE;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('country_profile_model', 'country_profiles');
	}
	
	public function index()
	{	
		try{
			$data = $resources = array();
			
			$modal = array(
				'modal_country_profile' => array(
					'controller'	=> __CLASS__,
					'module'		=> PROJECT_CEIS
				)
			);

			$resources['load_css'] 		= array(CSS_DATATABLE, CSS_DATETIMEPICKER, CSS_SELECTIZE, CSS_MODAL_COMPONENT, CSS_UPLOAD);
			$resources['load_js'] 		= array(JS_DATATABLE, JS_DATETIMEPICKER, JS_SELECTIZE, JS_MODAL_CLASSIE, JS_MODAL_EFFECTS, JS_UPLOAD, JS_EDITOR);
			$resources['load_modal'] 	= $modal;
			$resources['datatable'] 	= array();
			$resources['datatable'][] 	= array('table_id' => 'country_profile_table', 'path' => PROJECT_CEIS.'/ceis_country_profile/get_country_profile_list');
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e;
		}
		
		$this->template->load('country_profiles', $data, $resources);
	}
	
	
	public function get_country_profile_list()
	{
		try{
			$params = get_params();
			$cnt = 0;

			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array(
				"a.country_code", 
				"b.country_name", 
				"a.highlights", 
				"a.file_name", 
				"a.created_by", 
				"a.created_date", 
				"a.modified_by", 
				"a.modified_date",
				'CONCAT(c.last_name, \',  \', c.first_name) as created_name',
				'CONCAT(d.last_name, \',  \', d.first_name) as modified_name');

			// APPEARS ON TABLE
			$bColumns = array(
				"country_name", 
				"created_name", 
				"created_date", 
				"modified_name", 
				"modified_date");
		
			$country_profiles 	= $this->country_profiles->get_country_profile_list($aColumns, $bColumns, $params);
			$iTotal 			= $this->country_profiles->total_length();
			$iFilteredTotal 	= $this->country_profiles->filtered_length($aColumns, $bColumns, $params);
		
			$output 	= array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
	
		foreach ($country_profiles as $data):
		
			$country_code	= $data["country_code"];
			$hash_id 		= $this->hash($country_code);
			$encoded_id 	= base64_url_encode($hash_id);
			$salt 			= gen_salt();			
			$token 			= in_salt($encoded_id, $salt);			
			$url 			= $encoded_id."/".$salt."/".$token;
			
			$country_prof_cnt 		= $this->country_profiles->get_country_profile_count($country_code);
			$ctr 					= $country_prof_cnt['cnt'];
			
			$delete_action 	= 'content_delete("country profile", "'.$url.'")';
			
			$action = "<div class='table-actions'>";

			//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
				$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-country_profile='bottom' data-delay='50' data-modal='modal_country_profile' onclick=\"modal_init('".$url."')\"></a>";
			//endif;

			//if (($this->permission->check_permission(MODULE_ROLE, ACTION_DELETE))) : 	  
				$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-country_profile='bottom' data-delay='50'></a>";
			//endif;
		
			$action .= '</div>';
				$row 			= array();
				$row[] 			= $data['country_name'];
				$row[] 			= $data['created_name'];
				$row[] 			= $data['created_date'];
				$row[] 			= $data['modified_name'];
				$row[] 			= $data['modified_date'];
				$row[] 			= $action;
				
			$output['aaData'][] = $row;
		endforeach;
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}
	
		echo json_encode( $output );
	}
	
	public function modal($encoded_id, $salt, $token)
	{
		try{
			$data 					= array();
			$data["countries"] 		= $this->country_profiles->get_countries();
			$resources 				= array();
			$resources['load_css'] 	= array(CSS_SELECTIZE);
			$load_js 				= array('jquery.jscrollpane',JS_SELECTIZE);

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('country_code');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->country_profiles->get_specific_country_profile($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['country_code'] 		= $info['country_code'];
				$data['highlights']		 	= $info['highlights'];
				$data['file_name']			= $info['file_name'];
				$data['modified_by']		= $info['modified_by'];
				$data['modified_date']		= $info['modified_date'];
				$data['readonly_value']		= 'readonly';
				$data['selected_value']		= 'selected';	

				// CREATE NEW SECURITY VARIABLES
				$country_code	= $info['country_code'];
				$hash_id 		= $this->hash($country_code);
			}

			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
			}



			$load_js[] = 'jquery.uploadfile';

				$resources['upload'] = array(
						array(
							'id' 					=> 'file', 
							'page' 					=> 'files', 
							'form_name' 			=> 'country_profile_form' ,
							'path' 					=> PATH_FILE_UPLOADS, 
							'allowed_types' 		=> 'doc,docx,xls,xlsx,ppt,pptx,pdf,jpeg,jpg,png,gif', 
							'default_img_preview' 	=> 'image_preview.png', 
							'multiple' 				=> 0, 
							'drag_drop' 			=> 1, 
							'show_preview' 			=> 1
							)
					);
				
			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}
		$resources['load_js'] = $load_js;
		$this->load->view('modals/country_profiles',$data);
		$this->load_resources->get_resource($resources);
	}
	
	public function process()
	{
		try
		{
			$flag 				= 0;
			$params 			= get_params();

			// SERVER VALIDATION
			$this->_validate($params);

			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('country_code');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->country_profiles->get_specific_country_profile($where);
			$country_code	= $info['country_code'];

			CEIS_Model::beginTransaction();

			$country_name = $params['country_name'];

			IF(EMPTY($country_code)){
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= CEIS_Model::tbl_country_profile;
				$audit_schema[]	= DB_CEIS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				// SAVES THE RESOLUTIONS RECORD
				$this->country_profiles->insert_country_profile($params);

				$activity	= "%s has been added in Country Profile.";
				$activity 	= sprintf($activity, $country_name);

				$msg  = $this->lang->line('data_saved');
			}

			else{

				$params['country_code']	= $country_code;

				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= CEIS_Model::tbl_country_profile;
				$audit_schema[]	= DB_CEIS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				// SAVES THE PROJECT TYPES RECORD
				$this->country_profiles->update_country_profile($params);

				$activity	= "%s has been updated in country profile.";
				$activity 	= sprintf($activity, $country_name);

				$msg  = $this->lang->line('data_updated');
			}


			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table, 
				$audit_schema
			);
			
			CEIS_Model::commit();

			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
			
			$this->rlog_error($e);
			/*$arr_field_names = array(
				'country_code' 		=> 'Country Name'
	  		);


			$this->rlog_error($e, TRUE);*/
			$msg = $this->get_user_message($e, $arr_field_names);
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	
	}
	
	private function _validate(&$params)
	{
		try
		{
			
			$this->_validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields = array();
			$fields['country_name']			= 'Country Name';
			$fields['highlights']			= 'Highlights';
			// $fields['photo']				= 'Photo';

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _validate_input($params)
	{
		try
		{
			$validation['country_name'] = array(
					'data_type' => 'string',
					'name'		=> 'Country Name',
					'max_len'	=> 100
			);

			$validation['highlights'] = array(
					'data_type' => 'string',
					'name'		=> 'Highlights',
					'max_len'	=> 100
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function delete_country_profile($params)
	{

		try 
		{
			$flag	 			= 0;
			$msg 				= "Error";
			$params 			= get_params();
			$params['security']	= $params['param_1'];


			$this->_validate_security($params);

			$key 			= $this->get_hash_key('country_code');
			$where			= array();
			$where[$key]	= $params['hash_id'];
			$info 			= $this->country_profiles->get_specific_country_profile($where);
			$country_code 	= $info['country_code'];

			if(EMPTY($info['country_code']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			CEIS_Model::beginTransaction();
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= CEIS_Model::tbl_country_profile;
			$audit_schema[]	= DB_CEIS;			
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL

			$this->country_profiles->delete_country_profile($info['country_code']);

			// SET AS AN ARRAY
			$curr_detail[]	= array();
			
			// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $country_code);
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			CEIS_Model::commit();

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');

		}		
		catch(PDOException $e)
		{		
			CEIS_Model::rollback();

			$this->rlog_error($e);

			$msg	= $this->get_user_message($e);	
		}
		catch(Exception $e)
		{	
			CEIS_Model::rollback();
			
			$this->rlog_error($e);

			$msg	= $e->getMessage(); 	
		}
		
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);
		echo json_encode($info);
	}
}


/* End of file Ceis_country_profile.php */
/* Location: ./application/modules/sysad/controllers/Ceis_country_profile.php */