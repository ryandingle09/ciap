<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_equipment extends CEIS_Controller {
	
	private $module = MODULE_CEIS_EQUIPMENTS;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('equipment_model', 'equipment');
	}
	
	public function index()
	{	
		try{
			$data = $resources = $modal = array();
		
			$modal = array(
				'modal_equipment' => array(
					'controller'	=> __CLASS__,
					'module'		=> PROJECT_CEIS
				)
			);

			$resources['load_css'] 		= array(CSS_DATATABLE);
			$resources['load_js'] 		= array(JS_DATATABLE);
			$resources['load_modal'] 	= $modal;
			$resources['datatable'] 	= array();
			$resources['datatable'][] 	= array('table_id' => 'equipment_table', 'path' => PROJECT_CEIS.'/ceis_equipment/get_equipment_list');	
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e;
		}
		
		$this->template->load('equipment', $data, $resources);
	}
	
	public function get_equipment_list()
	{
		try
		{
			$params = get_params();
			$cnt = 0;
			
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array(
							"a.serial_no",
							"a.registrant_name", 
							"a.registrant_address", 
							"a.equipment_type", 
							"a.capacity", 
							"a.brand",
							"a.model",
							"a.acquired_date",
							"a.acquisition_cost",
							"a.present_condition",
							"a.location",
							"a.motor_no",
							"a.color",
							"a.year",
							"a.flywheel",
							"a.acquired_from",
							"a.ownership_type",
							"a.active_flag",
							"a.created_by",
							"a.created_date",
							"a.modified_by",
							"a.modified_date");

			// APPEARS ON TABLE
			$bColumns = array(
							"serial_no", 
							"equipment_type", 
							"registrant_name");
		
			$equipment 		= $this->equipment->get_equipment_list($aColumns, $bColumns, $params);
			$iTotal 		= $this->equipment->total_length();
			$iFilteredTotal = $this->equipment->filtered_length($aColumns, $bColumns, $params);
		
			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
		
			foreach ($equipment as $data):
		
				$action 		= "";
			
				$serial_no 		= $data["serial_no"];
				$hash_id 		= $this->hash($serial_no);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;

				$action = "<div class='table-actions'>";

				//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
					$action .= "<a href='javascript:;' class='md-trigger view tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_equipment' onclick=\"modal_init('".$url."')\"></a>";
				//endif;
		
				$action .= '</div>';

				$row 	= array();
				$row[] 	= $data['serial_no'];
				$row[] 	= $data['equipment_type'];
				$row[] 	= $data['registrant_name'];
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e, TRUE);

			$msg = $this->get_user_message($e);

		}
		catch(Exception $e)
		{
			$msg = $this->rlog_error($e, TRUE);
		}

		echo $msg;
		echo json_encode( $output );
	}
	
	public function modal($encoded_id, $salt, $token)
	{
		try{
			$data = array();
			$resources = array();

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('serial_no');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->equipment->get_specific_equipment($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	
				// echo '<pre>'; print_r($info); echo '</pre>';

				$data['serial_no'] 				= $info['serial_no'];
				$data['registrant_name'] 		= $info['registrant_name'];
				$data['registrant_address']		= $info['registrant_address'];
				$data['equipment_type']			= $info['equipment_type'];
				$data['capacity']				= $info['capacity'];
				$data['brand']					= $info['brand'];
				$data['model']					= $info['model'];
				$data['acquired_date']			= $info['acquired_date'];
				$data['acquisition_cost']		= $info['acquisition_cost'];
				$data['present_condition'] 		= $info['present_condition'];
				$data['location']				= $info['location'];
				$data['motor_no'] 				= $info['motor_no'];
				$data['color']	 				= $info['color'];
				$data['year'] 					= $info['year'];
				$data['flywheel'] 				= $info['flywheel'];
				$data['acquired_from'] 			= $info['acquired_from'];
				$data['ownership_type'] 		= $info['ownership_type'];
				$data['active_flag'] 			= $info['active_flag'];
				$data['created_by'] 			= $info['created_by'];
				$data['created_date'] 			= $info['created_date'];
				$data['modified_by'] 			= $info['modified_by'];
				$data['modified_date']			= $info['modified_date'];


				// CREATE NEW SECURITY VARIABLES
				$serial_no		= $info['serial_no'];
				$hash_id 		= $this->hash($serial_no);			

			}

			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);
			
			echo $e->getMessage();
		}

		$this->load->view('modals/equipment',$data);
	}
		
}


/* End of file Ceis_equipment.php */
/* Location: ./application/modules/sysad/controllers/Ceis_equipment.php */