<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_directory extends CEIS_Controller {
	
	private $module = MODULE_CEIS_DIRECTORY;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('directory_model', 'directories');
		$this->load->library('pagination');
	}
	
	public function index()
	{	
		try{
			$data 						= array(); 
			$resources					= array();
			$modal 						= array();

			$modal = array(
				'modal_directory_report' => array(
					'controller'		=> __CLASS__,
					'module'			=> PROJECT_CEIS,
					'multiple'			=> true,
					'height'			=> '450px;',
					'ajax'				=> true,
					'size'				=> 'xl'
				)
			);

			$resources['load_js'] 		= array(JS_LIST,JS_LIST_PAGINATE,JS_LIST_CALL_PAGINATE,'modules/ceis/ceis_directory');
			$resources['load_css']		= array(CSS_LIST);
			
			$resources['load_modal'] 	= $modal;

			$data["results"] 	= $this->directories->fetch_data();

		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		catch(Exception $e){
			
			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->template->load('directory', $data, $resources);
	}	

	public function get_directory_list_excel(){
		try{
			$success 				= 0;
			$data 	 				= array();
			$modal_data 			= array();

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.xls';

			$directory_list_info			= $this->directories->get_directory_list();
			$data['directory_list_info'] 	=	$directory_list_info;

			$html.= $this->load->view('reports/report_directory_list',$data,TRUE);

			$html.= $this->set_report_footer();

			$this->convert_excel( $html, $filename , '');

			$success 	= 1;
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){	

			$this->rlog_error($e);

			echo $e;
		}
	
	}

	public function generate_report()
	
	{
		try{

			$success 				= 0;
			$msg 	 				= "";
			$data 	 				= array();
			$link 	 				= "";
			$val_msg 				= array();
			$modal_data 			= array();
			$report 				= '';
			$modal 					= '';
			$html 					= '';
			$params     			= get_params();
			$data['report']			= 2;
			$report 				= 2;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'pdf';
			$modal 					= 'report_container_modal';

			$directory_list_info			= $this->directories->get_directory_list();
			$data['directory_list_info'] 	= $directory_list_info;

			$html  .= $this->load->view('reports/report_directory_list', $data,TRUE);
			$html  .= $this->set_report_footer();


			$data['generate'] = 'PDF';

			$pdf = $this->pdf( $filename, $html, TRUE, 'S' );

			$modal_data['pdf'] 			= $pdf;
			$modal_data['report_title']	= $data['report'];

			$modal = $this->load->view('modals/report_container_modal', $modal_data, TRUE);	
			$success 	= 1;
		

		}catch(PDOException $e){

			$msg 	= $e->getMessage().' -Line '.$e->getLine().' - '.__FUNCTION__;

		}catch(Exception $e){

			$msg 	= $e->getMessage().' -Line '.$e->getLine().' - '.__FUNCTION__;

			if(!EMPTY($val_msg['flag'])) $msg = $e->getMessage();

		}

		echo $msg;

		if( $data['generate'] == 'PDF' )
		{
	
			$ajaxData 	= array(

					'success'=> $success,
					'msg'	=> $msg,
					'modal' => $modal,
					'data' => $data
			);

			echo  json_encode($ajaxData);

		}
	}

	public function generate_report_excel()
	
	{
		try{
			$success 				= 0;
			$msg 	 				= "";
			$data 	 				= array();
			$link 	 				= "";
			$val_msg 				= array();
			$modal_data 			= array();
			$report 				= '';
			$modal 					= '';
			$html 					= '';
			$params     			= get_params();
			$data['report']			= 2;
			$report 				= 2;
			$data['generate']		= 1;
			// $data['contractor_list']= $params['contractor_list'];
			// $data['quarter']		= $params['quarter'];

			$filename 				= 'ceis_reports_'.date("Y_m_d");
			$modal 					= 'report_container_modal';


			$directory_list_info			= $this->directories->get_directory_list();
			$data['directory_list_info'] 	= $directory_list_info;

			$html  .= $this->load->view('reports/report_directory_list', $data,TRUE);
			$html  .= $this->set_report_footer();

			$modal_page = "report_container_modal";
			
			$this->convert_excel( $html, $filename , '');
			$success 	= 1;

		}catch(PDOException $e){

			$msg 	= $e->getMessage().' -Line '.$e->getLine().' - '.__FUNCTION__;

		}catch(Exception $e){

			$msg 	= $e->getMessage().' -Line '.$e->getLine().' - '.__FUNCTION__;

			if(!EMPTY($val_msg['flag'])) $msg = $e->getMessage();

		}

		echo $msg;

		if( $data['generate'] == 'PDF' )
		{
	
			$ajaxData 	= array(

					'success'=> $success,
					'msg'	=> $msg,
					'modal' => $modal,
					'data' => $data
			);

			echo  json_encode($ajaxData);

		}
	}
}


/* End of file Ceis_directory.php */
/* Location: ./application/modules/ceis/controllers/Ceis_directory.php */
/*
?>
<script>

var value_test = 1;
get_first_val();
get_second_val();
 
 function get_first_val(){
 	value_test = 2;
 	console.log("1st call value: "+ value_test);

 }
function get_second_val(){
 	
 	console.log("2nd call value: "+ value_test);
 }
</script>*/