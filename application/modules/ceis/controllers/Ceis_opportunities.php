<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_opportunities extends CEIS_Controller {
	
	private $module = MODULE_CEIS_OPPORTUNITIES;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('opportunities_model', 'opportunities');
		$this->load->library( 'Email_template' );
	}
	
	public function index()
	{	
		try{	
			$data = $resources = $modal = array();

			$modal = array(
				'modal_opportunity' => array(
					'module'		=> PROJECT_CEIS,
					'controller'	=> __CLASS__,
					'method'		=> 'modal',
					'height'		=> '400px',
					'multiple'		=> TRUE
					
				)
			);
			
			$resources['load_css'] 		= array(CSS_DATATABLE,CSS_DATETIMEPICKER, CSS_LABELAUTY, CSS_SELECTIZE,CSS_MODAL_COMPONENT);
			$resources['load_js'] 		= array(JS_DATATABLE, JS_DATETIMEPICKER, JS_LABELAUTY,JS_MODAL_CLASSIE, JS_SELECTIZE,'common/add_row');
			$resources['load_modal'] 	= $modal;
			$resources['datatable'] 	= array();
			$resources['datatable'][] 	= array('table_id' => 'opportunity_table', 'path' => PROJECT_CEIS.'/ceis_opportunities/get_opportunity_list');
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e, TRUE);

			echo $e;
		}
				
		$this->template->load('opportunities', $data, $resources);
	}
	
	public function get_opportunity_list()
	{
		try{
			$params = get_params();
			$cnt = 0;
		
			$aColumns = array(
				"a.opportunity_id", 
				"a.reference_no", 
				"a.received_date",
				"a.opp_source_id", 
				"a.source_agency", 
				"a.subject",
				"a.opp_status_id", 
				"a.created_by", 
				"a.created_date",
				"a.modified_by",
				"a.modified_date",
				"b.source",
				"c.opp_status",
				'CONCAT(d.last_name, \',  \', d.first_name) as created_name',
				'CONCAT(e.last_name, \',  \', e.first_name) as modified_name');

			$bColumns = array(
				"received_date", 
				"reference_no", 
				"source", 
				"source_agency",
				"subject",
				"opp_status"
				);
		
			$opportunities 	= $this->opportunities->get_opportunity_list($aColumns, $bColumns, $params);
			$iTotal 		= $this->opportunities->total_length();
			$iFilteredTotal = $this->opportunities->filtered_length($aColumns, $bColumns, $params);
		
			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
		
			foreach ($opportunities as $data):
			
				$opportunity_id = $data["opportunity_id"];

				$has_id 		= $this->hash($opportunity_id);
				$encoded_id 	= base64_url_encode($has_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("opportunity", "'.$url.'")';
				
				$action = "<div class='table-actions'>";

				if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
					$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-opportunity='bottom' data-delay='50' data-modal='modal_opportunity' onclick=\"modal_opportunity_init('".$url."')\"></a>";
				endif;

				if (($this->permission->check_permission(MODULE_ROLE, ACTION_DELETE))) : 	  
					$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-opportunity='bottom' data-delay='50'></a>";
				endif;
			
				$action .= '</div>';

				
				$row 	= array();
				$row[] 	= $data['received_date'];
				$row[] 	= $data['reference_no'];
				$row[] 	= $data['source'];
				$row[] 	= $data['source_agency'];
				$row[] 	= $data['subject'];
				$row[] 	= $data['opp_status'];
				$row[] 	= $action;
				$output['aaData'][] = $row;
			endforeach;
		
			
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e, TRUE);

			echo $e->getMessage();
		}
		
		echo json_encode( $output );
	}

	public function modal($encoded_id, $salt, $token){

		$data 							= array();
		$resources 						= array();
		$resources['load_css'] 			= array(CSS_SELECTIZE, CSS_DATETIMEPICKER);
		$load_js 						= array('jquery.jscrollpane',JS_SELECTIZE,JS_DATETIMEPICKER,'modules/ceis/ceis_opportunities','common/add_row');

		$data["sources"] 				= $this->opportunities->get_sources();
		$data["contractors"] 			= $this->opportunities->get_contractors();
		$data["opportunities"] 			= $this->opportunities->get_opportunities();
		$data["distinct_opportunities"] = $this->opportunities->get_distinct_opportunities();

		try{

			if(!EMPTY($encoded_id)){

				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('opportunity_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info 			= $this->opportunities->get_specific_opportunity($where);
				$recipient 		= $this->opportunities->get_specific_opportunity_recipient($where);
				
				$inf 					= $info['opportunity_id'];  
				$email_count 			= $this->opportunities->get_contractor_email_count($inf);
				$c_count 				= $email_count['cnt'];

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));

				$data['opportunity_id'] 		= $info['opportunity_id'];
				$data['reference_no'] 			= $info['reference_no'];
				$data['received_date']  		= $info['received_date'];
				$data['opp_source_id']  		= $info['opp_source_id'];
				$data['source_agency']  		= $info['source_agency'];
				$data['subject'] 				= $info['subject'];
				$data['recipient']				= $recipient;


				// CREATE NEW SECURITY VARIABLES
				$opportunity_id	= $info['opportunity_id'];
				$hash_id 		= $this->hash($opportunity_id);
			}
			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e, TRUE);

			echo $e->getMessage();
		}

		$resources['load_js'] = $load_js;
		$this->load->view('modals/opportunities',$data);
		$this->load_resources->get_resource($resources);	
	}
	
	
	public function process()
	{
		try
		{
			$flag 				= 0;
			$params 			= get_params();

			$security 			= $params['security'];
			$opportunity_id		= $params['opportunity_id'];
			$reference_no 		= $params['reference_no'];
			$received_date		= $params['received_date'];
			$opp_source_id		= $params['source'];
			$source_agency		= $params['source_agency'];
			$subject			= $params['subject'];
			$opp_status_id		= $params['opp_status_id'];
			$created_by			= $params['created_by'];

			$contractor_lists	= $params['contractor_list'];
			$contractor_names	= $params['contractor_name'];
			$emails 			= $params['email'];
						
			// SERVER VALIDATION
			$this->_validate($params);

			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('opportunity_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->opportunities->get_specific_opportunity($where);
			$opportunity_id	= $info['opportunity_id'];

			CEIS_Model::beginTransaction();

			$subject = $params['subject'];

			IF(EMPTY($opportunity_id)){
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= CEIS_Model::tbl_opportunities;
				$audit_schema[]	= DB_CEIS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				// SAVES THE OPPORTUNITY RECORD
				$new_opportunity_id = $this->opportunities->insert_opportunity($params);

				$params['opportunity_id'] = $new_opportunity_id;
				
				$activity	= "%s has been added in opportunity.";
				$activity 	= sprintf($activity, $subject);

				$msg  = $this->lang->line('data_saved');

			}

			else{

				$params['opportunity_id'] = $opportunity_id;

				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= CEIS_Model::tbl_opportunities;
				$audit_schema[]	= DB_CEIS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				// SAVES THE PROJECT TYPES RECORD
				$this->opportunities->update_opportunity($params);

				$activity	= "%s has been updated in opportunity.";
				$activity 	= sprintf($activity, $subject);

				$msg  = $this->lang->line('data_updated');
			}

			// DELETE
			IF(!EMPTY($contractor_lists))
			{
				$this->opportunities->delete_opportunity_recipients($params['opportunity_id']);
			
				// SAVING
				for($i = 0; $i < count($contractor_lists); $i++)
				{
					$contractor_list 			= $contractor_lists[$i];
					$contractor_name 			= $contractor_names[$i];

					if(EMPTY($contractor_list) AND EMPTY($contractor_name))
						continue;

					$recipient_name				= !EMPTY($contractor_list) ? $contractor_list : $contractor_name;

					$contractor_email 			= $emails[$i];

					$params['email'] 			= $contractor_email;	
					$params['recipient_name']	= $recipient_name;

					// SAVES THE OPPORTUNITIES RECORD
					$this->opportunities->insert_opportunity_recipients($params);
				}
			}

			// SENDING EMAIL
			if($params['referrals']=='on'){
				for($i = 0; $i < count($contractor_lists); $i++)
				{

					// $email_data 				= array();
					$email_data['from_email'] 	= "rms@asiagate.com";
					$email_data['from_name']	= "CIAP webmaster";
					$contractor_email 			= $emails[$i];
					$email_data['to_email']		= array($contractor_email);
					$email_data['subject']		= "Opportunities";
					$template 					= "../emails/opportunities_email";
					
					$template_data 				= array(
						'name'		=> 'Christian'
					);

					$this->email_template->send_email_template($email_data, $template, $template_data);

					$email_errors 	= $this->email_template->get_email_errors();
					
					if( !EMPTY( $email_errors ) )
					{
						throw new Exception( var_export( $email_errors, TRUE ) );
						
					}
				}
			}

			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table, 
				$audit_schema
			);
			
			CEIS_Model::commit();
			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
			
			$this->rlog_error($e, TRUE);

			$msg = $this->get_user_message($e);
			
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	
	}

	public function delete_opportunity($params)
	{
		try 
		{

			$flag	= 0;
			$msg	= "Error";

			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->_validate_security($params);

			$key 			= $this->get_hash_key('opportunity_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];


			$info 			= $this->opportunities->get_specific_opportunity($where);
			
			$opportunity_id = $info['opportunity_id'];
			
			if(EMPTY($info['opportunity_id']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			CEIS_Model::beginTransaction();
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= CEIS_Model::tbl_opportunities;
			$audit_schema[]	= DB_CEIS;			
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL

			$this->opportunities->delete_opportunity($info['opportunity_id']);

			// SET AS AN ARRAY
			$curr_detail[]	= array();
			
			// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $opportunity_id);
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			CEIS_Model::commit();

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			CEIS_Model::rollback();

			$this->rlog_error($e);

			$msg	= $this->get_user_message($e);
		}
		catch(Exception $e)
		{	
			CEIS_Model::rollback();
			
			$this->rlog_error($e);

			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
		catch(PDOException $e)
		{
			throw $e;
		}
	}

	private function _validate(&$params)
	{
		try
		{
			
			$this->_validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields = array();
			$fields['received_date']			= 'Received Date';

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_input($params)
	{
		try
		{
			$validation['reference_no'] = array(
					'data_type' => 'string',
					'name'		=> 'Reference No',
					'max_len'	=> 100
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
}


/* End of file Ceis_opportunities.php */
/* Location: ./application/modules/ceis/controllers/Ceis_opportunities.php */