<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_requests extends CEIS_Controller {
	
	private $module 	= MODULE_CEIS_REQUESTS;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('requests_model', 'requests');
	}
	
	public function index()
	{	
		$data = $resources = $modal = array();

		$modal = array(
			'modal_request' 		=> array(
				'module'			=> PROJECT_CEIS,
				'controller'		=> __CLASS__
			)
		);

		$resources['load_css'] 		= array(CSS_DATATABLE);
		$resources['load_js'] 		= array(JS_DATATABLE);
		$resources['load_modal'] 	= $modal;
		$resources['datatable'][] 	= array();
		$resources['datatable'][] 	= array('table_id' => 'request_table', 'path' => PROJECT_CEIS.'/ceis_requests/get_request_list');
		
		$this->template->load('requests', $data, $resources);
	}

	public function process_registration()
	{
		try
		{
			$flag 				= 0;
			$params 			= get_params();

			// SERVER VALIDATION
			// $this->_validate($params);


			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('registration_no');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			/*$info 			= $this->requests->get_specific_registration($where);
			$registration_no	= $info['registration_no'];*/

			CEIS_Model::beginTransaction();

			IF(EMPTY($registration_no)){
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= $this->requests->tbl_request_registration;
				$audit_schema[]	= DB_CEIS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				// SAVES THE RESOLUTIONS RECORD
				$this->requests->insert_registration($params);

				$activity	= "%s has been added in registration.";
				$activity 	= sprintf($activity, $registration_no);

				$msg  = $this->lang->line('data_saved');
			}

			else{

				$params['registration_no']	= $registration_no;

				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= $this->requests->tbl_request_registration;
				$audit_schema[]	= DB_CEIS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				// SAVES THE PROJECT TYPES RECORD
				$this->requests->update_registration($params);

				$activity	= "%s has been updated in position.";
				$activity 	= sprintf($activity, $registration_no);

				$msg  = $this->lang->line('data_updated');
			}


			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table, 
				$audit_schema
			);
			
			CEIS_Model::commit();

			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			SYSAD_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
			
		}
		catch(Exception $e)
		{
			SYSAD_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	
	}
	
	public function get_request_list()
	{
		try
		{
			$params	= get_params();
			$row 	= array();
			$cnt	= 0;
		
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array(
					'a.request_id',
					'a.requested_date',
					'a.request_type_id',
					'a.reference_no',
					'a.reference_name',
					'a.request_status_id',
					'a.pocb_registration_no',
					'a.equip_serial_no',
					'a.created_by',
					'a.created_date',
					'a.modified_by',
					'a.modified_date',
					'b.request_type_name',
					'c.request_status',
					'd.project_title as proj_title',
					'e.project_title as srv_title',
					'CASE a.request_type_id WHEN '.REQUEST_TYPE_PC.' THEN d.project_title WHEN '.REQUEST_TYPE_MSC.' THEN e.project_title ELSE \'\' END as project_name'
			);
		
			$bColumns = array('request_id', 'requested_date', 'request_type_name', 'reference_no', 'reference_name', 'project_name', 'request_status');

			$data["request_type"] 	= $this->requests->get_request_type();
			
			$requests 				= $this->requests->get_request_list($aColumns, $bColumns, $params);

			foreach ($requests['aaData'] as $key => $aRow):
				$cnt++;
				$action 	= "";
				// START: SET THE SECURITY VARIABLES
				$request_id	= $aRow["request_id"];
				$hash_id	= $this->hash($request_id);
				$encoded_id = base64_url_encode($hash_id);
				$salt		= gen_salt();
				$token		= in_salt($encoded_id, $salt);
				$url		= $encoded_id.'/'.$salt.'/'.$token;
				$action 	= "<div class='table-actions'>";
				//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
				$action    .= '<a href="ceis_requests/view_request/'.$url.'"  class="view tooltipped" data-tooltip="View"  data-position="bottom"></a>';

				$row[] = array($aRow['request_id'], $aRow['requested_date'], $aRow['request_type_name'], $aRow['reference_no'], $aRow['reference_name'], $aRow['project_name'], $aRow['request_status'], $action);
					
			endforeach;

			$requests['aaData'] = $row;

			echo json_encode( $requests );
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}

	public function get_order_of_payment_list()
	{
		try{

			$params = get_params();
			$cnt = 0;
		
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array('a.rqst_payment_id',
					'a.request_id',
					'a.op_date',
					'a.op_amount',
					'a.created_by',
					'a.created_date',
					'a.modified_by',
					'a.modified_date');

			// APPEARS ON TABLE
			$bColumns = array('rqst_payment_id',
					'request_id',
					'op_date',
					'op_amount'
					);
		
			$requests = $this->requests->get_order_of_payment_list($aColumns, $bColumns, $params);
			$iTotal = $this->requests->total_length("rqst_payment_id", CEIS_Model::tbl_request_payments);
			$iFilteredTotal = $this->requests->filtered_length($aColumns, $bColumns, $params,"get_order_of_payment_list");
		
			$output = array(
				"sEcho" => intval($_POST['sEcho']),
				"iTotalRecords" => $iTotal["cnt"],
				"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
				"aaData" => array()
			);
		
			foreach ($requests as $aRow):

				$cnt++;
				$row 			= array();
				$action 		= "";
			
				$rqst_payment_id		= $aRow["rqst_payment_id"];
				$hash_id 		= $this->hash($rqst_payment_id);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$requests_cnt 	= $this->requests->get_requests_count($rqst_payment_id);
				$ctr 			= $requests_cnt['cnt'];
				
				$delete_action 	= 'content_delete("request", "'.$url.'")';
				
				for ( $i=0 ; $i<count($bColumns) ; $i++ )
				{
					$row[] = $aRow[ $bColumns[$i] ];
				}
				
				$action = "<div class='table-actions'>";

				$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_manpower_schedule' onclick=\"modal_manpower_schedule_init()\" ><i class='flaticon-search95' ></i></a>";
			
				$action .= '</div>';

				$row[] = $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}

		catch(PDOException $e){
			echo $e->getMessage();
		}

		catch(Exception $e){
			echo $e->getMessage();
		}

	}

	public function get_staffing_principal_list()
	{
		try{

			$params = get_params();
			$cnt = 0;
		
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array('a.request_id',
					'a.requested_date',
					'a.request_type_id',
					'a.reference_no',
					'a.reference_name',
					'a.request_status_id',
					'a.pocb_registration_no',
					'a.equip_serial_no',
					'a.created_by',
					'a.created_date',
					'a.modified_by',
					'a.modified_date');

			// APPEARS ON TABLE
			$bColumns = array('request_id',
					'requested_date',
					'request_type_id'
					);
		
			$requests = $this->requests->get_request_list($aColumns, $bColumns, $params);
			$iTotal = $this->requests->total_length();
			$iFilteredTotal = $this->requests->filtered_length($aColumns, $bColumns, $params);
		
			$output = array(
				"sEcho" => intval($_POST['sEcho']),
				"iTotalRecords" => $iTotal["cnt"],
				"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
				"aaData" => array()
			);
		
			foreach ($requests as $aRow):

				$cnt++;
				$row 			= array();
				$action 		= "";
			
				$request_id		= $aRow["request_id"];
				$hash_id 		= $this->hash($request_id);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$requests_cnt 	= $this->requests->get_requests_count($request_id);
				$ctr 			= $requests_cnt['cnt'];
				
				$delete_action 	= 'content_delete("request", "'.$url.'")';
				
				for ( $i=0 ; $i<count($bColumns) ; $i++ )
				{
					$row[] = $aRow[ $bColumns[$i] ];
				}
				
				$action = "<div class='table-actions'>";

				//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
		
	            // $action .='<a href="" class="md-trigger" data-opportunity="button" data-modal="modal_opportunity"></a>';
	           $action .= "<a href='../submenu_staffing_principal' class='edit tooltipped' data-tooltip='View' ><i class='flaticon-search95' ></i></a>";
			
				$action .= '</div>';

				$row[] = $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}

		catch(PDOException $e){
			echo $e->getMessage();
		}

		catch(Exception $e){
			echo $e->getMessage();
		}

	}

	public function get_contract_service_list($req_id)
	{
		try{

			$params = get_params();
			$cnt = 0;
			
			// START: GET THE CONTRACTOR INFORMATION USING HASH ID
			$key 			= $this->get_hash_key('request_id');
			$where			= array();
			$where[$key]	= $req_id;
			$info 			= $this->requests->get_specific_request($where);
			// END: GET THE CONTRACTOR INFORMATION USING HASH ID
			
			$request_id = $info['request_id'];

			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array('a.request_id',
					'a.requested_date',
					'a.request_type_id',
					'a.reference_no',
					'a.reference_name',
					'a.request_status_id',
					'a.pocb_registration_no',
					'a.equip_serial_no',
					'a.created_by',
					'a.created_date',
					'a.modified_by',
					'a.modified_date',
					'b.project_title',
					'b.project_location',
					'b.owner',
					'b.contract_cost',
					'd.request_status'
					);

			// APPEARS ON TABLE
			$bColumns = array('project_title',
					'project_location',
					'owner',
					'contract_cost',
					'request_status'
					);
		
			$requests 		= $this->requests->get_contract_service_list($aColumns, $bColumns, $params, $request_id);
			$iTotal 		= $this->requests->total_length("rqst_contract_id", CEIS_Model::tbl_request_services);
			$iFilteredTotal = $this->requests->filtered_length($aColumns, $bColumns, $params,"get_contract_service_list");
		
			$output = array(
				"sEcho" => intval($_POST['sEcho']),
				"iTotalRecords" => $iTotal["cnt"],
				"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
				"aaData" => array()
			);
		
			foreach ($requests as $aRow):

				$cnt++;
				$row 			= array();
				$action 		= "";
			
				$request_id		= $aRow["request_id"];
				$hash_id 		= $this->hash($request_id);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$requests_cnt 	= $this->requests->get_requests_count($request_id);
				$ctr 			= $requests_cnt['cnt'];
				
				$delete_action 	= 'content_delete("request", "'.$url.'")';
				
				for ( $i=0 ; $i<count($bColumns) ; $i++ )
				{
					$row[] = $aRow[ $bColumns[$i] ];
				}
				
				$action = "<div class='table-actions'>";

				//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
					// $action .= '<a href="ceis_requests/submenu_request"><i class="flaticon-search95"></i>
     //        </a>';	
            // $action .='<a href="" class="md-trigger" data-opportunity="button" data-modal="modal_opportunity"></a>';
           		$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_manpower' onclick=\"modal_manpower_init()\" ><i class='flaticon-search95' ></i></a>";
				
			
				$action .= '</div>';

				$row[] = $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}

		catch(PDOException $e){
			echo $e->getMessage();
		}

		catch(Exception $e){
			echo $e->getMessage();
		}

	}

	public function get_construction_equipment_list()
	{
		try{

			$params = get_params();
			$cnt = 0;
		
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array('a.request_id',
					'a.requested_date',
					'a.request_type_id',
					'a.reference_no',
					'a.reference_name',
					'a.request_status_id',
					'a.pocb_registration_no',
					'a.equip_serial_no',
					'a.created_by',
					'a.created_date',
					'a.modified_by',
					'a.modified_date');

			// APPEARS ON TABLE
			$bColumns = array('request_id',
					'requested_date',
					'request_type_id',
					'reference_no',
					'reference_name'
					);
		
			$requests = $this->requests->get_request_list($aColumns, $bColumns, $params);
			$iTotal = $this->requests->total_length();
			$iFilteredTotal = $this->requests->filtered_length($aColumns, $bColumns, $params);
		
			$output = array(
				"sEcho" => intval($_POST['sEcho']),
				"iTotalRecords" => $iTotal["cnt"],
				"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
				"aaData" => array()
			);
		
			foreach ($requests as $aRow):

				$cnt++;
				$row 			= array();
				$action 		= "";
			
				$request_id		= $aRow["request_id"];
				$hash_id 		= $this->hash($request_id);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$requests_cnt 	= $this->requests->get_requests_count($request_id);
				$ctr 			= $requests_cnt['cnt'];
				
				$delete_action 	= 'content_delete("request", "'.$url.'")';
				
				for ( $i=0 ; $i<count($bColumns) ; $i++ )
				{
					$row[] = $aRow[ $bColumns[$i] ];
				}
				
				$action = "<div class='table-actions'>";

				//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
					// $action .= '<a href="ceis_requests/submenu_request"><i class="flaticon-search95"></i>
     //        </a>';	
            // $action .='<a href="" class="md-trigger" data-opportunity="button" data-modal="modal_opportunity"></a>';
           		$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_construction_equipment' onclick=\"modal_construction_equipment_init()\" ><i class='flaticon-search95' ></i></a>";
				
			
				$action .= '</div>';

				$row[] = $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}

		catch(PDOException $e){
			echo $e->getMessage();
		}

		catch(Exception $e){
			echo $e->getMessage();
		}

	}

	public function modal($encoded_id, $salt, $token)
	{
		try{
			$data = array();
			$resources = array();

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('request_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->requests->get_specific_request($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['request_id'] 			= $info['request_id'];

				// CREATE NEW SECURITY VARIABLES
				$request_id		= $info['request_id'];
				$hash_id 		= $this->hash($request_id);			

			}

			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}

		$this->load->view('modals/requests',$data);
	}


	public function modal_manpower($encoded_id, $salt, $token)
	{
		try{
			$data = array();
			$resources = array();

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('request_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->requests->get_specific_request($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['request_id'] 			= $info['request_id'];

				// CREATE NEW SECURITY VARIABLES
				$request_id		= $info['request_id'];
				$hash_id 		= $this->hash($request_id);			

			}

			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}

		$this->load->view('modals/manpower',$data);
	}

	public function modal_manpower_schedule($encoded_id, $salt, $token)
	{
		try{
			$data = array();
			$resources = array();

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('request_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->requests->get_specific_request($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['request_id'] 			= $info['request_id'];

				// CREATE NEW SECURITY VARIABLES
				$request_id		= $info['request_id'];
				$hash_id 		= $this->hash($request_id);			

			}

			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}

		$this->load->view('modals/manpower_schedule',$data);
	}

	public function modal_construction_equipment($encoded_id, $salt, $token)
	{
		try{
			$data = array();
			$resources = array();

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('request_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->requests->get_specific_request($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['request_id'] 			= $info['request_id'];

				// CREATE NEW SECURITY VARIABLES
				$request_id		= $info['request_id'];
				$hash_id 		= $this->hash($request_id);			

			}

			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}

		$this->load->view('modals/construction_equipment',$data);
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function view_request($encoded_id, $salt, $token)
	{
		
		try 
		{
			$data = array();
			
			check_salt($encoded_id, $salt, $token);
			
			// SET NEW SECURITY VARIABLE
			$salt	= gen_salt();
			$token	= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$hash_id = base64_url_decode($encoded_id);
			
			$key 							= $this->get_hash_key('request_id');
			$where							= array();
			$where[$key]					= $hash_id;
			$info 							= $this->requests->get_specific_request($where);
			$contractor 					= $this->requests->get_specific_request_contractor($where);
		
			// GET CONTRACTOR NAME
			$data['contractor_name']		= $contractor['contractor_name'];
			
			$submenu = $this->_set_request_submenu($info['request_type_id']);
			
			$data['submenu'] 	  = $submenu['submenu'];
			$data['default_page'] = $submenu['default_page'];
			
			/* SALT FOR REQUEST ID */
			$salt   = gen_salt();
			$token	= in_salt($info['request_id'], $salt);
			
			$data['request_id']   = base64_url_encode($info['request_id']).'/'.$salt.'/'.$token;
		}
		catch(PDOException $e)
		{
			$msg = $e->getMessage();
		}
		catch(Exception $e)
		{
			$msg = $e->getMessage();
		}
				
		
		$resources				= array();
		$resources['load_js']	= array('modules/ceis/ceis_view_request');
		$resources['load_init'] = array("LoadTab.init('".$data['default_page']."');");
		
		$this->template->load('request_sub_menu', $data, $resources);
	}
	
	
	private function _set_request_submenu($request_type_id)
	{
		$submenu 	  = array();
		$default_page = '';
		
		switch ($request_type_id):
			case REQUEST_TYPE_NEW:
					$default_page = 'request_sub_contractor_info';
					$submenu      = array(
							'request_sub_contractor_info'   => 'Contractor Info',
							'request_sub_contracts'   	    => 'Contracts',
							'request_sub_staffing'     		=> 'Staffing',
							'request_sub_finance'           => 'Finance',
							'request_sub_work_volume'       => 'Work Volume',
							'request_sub_construction_equipment'    => 'Construction Equipment',
							'request_sub_checklist'    		=> 'Checklist',
							'request_sub_order_of_payment' 	=> 'Order of Payment',
							'request_sub_evaluation'   		=> 'Evaluation',
							'request_sub_registration' 		=> 'Registration',
							'request_sub_request_status' 	=> 'Request Status',
					);
				break;
		
			case REQUEST_TYPE_REN:
					$default_page = 'request_sub_contractor_info';
					$submenu      = array(
							'request_sub_contractor_info'    => 'Contractor Info',
							'request_sub_contracts'    		 => 'Contracts',
							'request_sub_staffing'     		 => 'Staffing',
							'request_sub_finance'      		 => 'Finance',
							'request_sub_work_volume'        => 'Work Volume',
							'request_sub_construction_equipment' => 'Construction Equipment',
							'request_sub_checklist'    		 => 'Checklist',
							'request_sub_order_of_payment' 	 => 'Order of Payment',
							'request_sub_evaluation'   		 => 'Evaluation',
							'request_sub_registration' 		 => 'Registration',
							'request_sub_request_status' 	 => 'Request Status',
					);
				break;
					
			case REQUEST_TYPE_PC:
					$default_page = 'request_sub_project_info';
					$submenu      = array(
							'request_sub_project_info'       => 'Project Info',
							'request_sub_owner'   	   		 => 'Owner',
							'request_sub_manpower_schedule'  => 'Manpower Schedule',
							'request_sub_checklist'    		 => 'Checklist',
							//'request_sub_order_of_payment' 	 => 'Order of Payment',
							'request_sub_request_status' 	 => 'Request Status',
					);

				break;
		
			case REQUEST_TYPE_MSC:
					$default_page = 'request_sub_service_info';
					$submenu      = array(
							'request_sub_service_info'       => 'Project Info',
							'request_sub_foreign_principal'  => 'Foreign',
							'request_sub_manpower_schedule'  => 'Manpower Schedule',
							'request_sub_checklist'    		 => 'Checklist',
							//'request_sub_order_of_payment' 	 => 'Order of Payment',
							'request_sub_request_status' 	 => 'Request Status',
					);
				break;
		
			case REQUEST_TYPE_EQP:
					$default_page = 'request_sub_equipment';
					$submenu      = array(
							'request_sub_equipment'     	 => 'Equipment Info',
							'request_sub_order_of_payment'   => 'Order of Payment',
							'request_sub_checklist'   		 => 'Checklist',
							'request_sub_request_status' 	 => 'Request Status',
					);
				break;
		endswitch;
		
		return 	array('submenu' => $submenu, 'default_page' => $default_page);
	}
	
	
	private function _set_data_per_page($page, &$data, $request_details, &$resources, $add_info)
	{
		try
		{
			$request_id  	 		 = $request_details['request_id'];
			$request_type_id 		 = $request_details['request_type_id'];
			$salt	 				 = gen_salt();
			$token					 = in_salt($request_id, $salt);
			$encoded_rid 	 		 = base64_url_encode($request_id);
			$add_info 				 = base64_url_decode($add_info);
			$data['request_details'] = $request_details;
			$salt_link 				 = $encoded_rid.'/'.$salt.'/'.$token;
			
			switch($page):
				case 'request_sub_contractor_info':
						$data['no_js'] 			    = TRUE;		
						$data['contractor_details']	= $this->requests->get_specific_request_contractor( $request_id );
						$data['classification'] 	= $this->requests->get_request_license_classifications($request_id, 0);
						$data['specialization'] 	= $this->requests->get_request_license_classifications($request_id, 1);
					break;
				case 'request_sub_contracts' :
						$data['no_js'] 			    = TRUE;
						$resources['load_js'] 		= array(JS_DATATABLE);
						$resources['load_css'] 		= array(CSS_DATATABLE);
						$resources['datatable'][]	= array(
								'table_id' 	=> 'table_projects',
								'path'		=> PROJECT_CEIS.'/ceis_request_contracts/get_contract_project_list/'.$encoded_rid
						);
						$resources['datatable'][]	= array(
								'table_id' 	=> 'table_manpower_services',
								'path'		=> PROJECT_CEIS.'/ceis_request_contracts/get_contract_manpower_list/'.$encoded_rid
						);
						$resources['load_modal']	= array(
								'modal_project' => array(
										'controller'	=> 'ceis_request_contracts',
										'module'		=> PROJECT_CEIS,
										'multiple'		=> true,
										'method'		=> 'modal_project',
										'height'   	    => '500px',
										'size' 			=> 'lg'
								),
						
								'modal_manpower' => array(
										'controller'	=> 'ceis_request_contracts',
										'module'		=> PROJECT_CEIS,
										'multiple'		=> true,
										'method'		=> 'modal_manpower',
										'height'   	    => '500px',
										'size' 			=> 'lg'
								)
						);
					break;	
				case 'request_sub_staffing':
						$resources['load_js'] 		= array(JS_DATATABLE);
						$resources['load_css'] 		= array(CSS_DATATABLE);
					break;
				case 'request_staff_tabs':
					break;
				case 'request_staffing_work_experience':
						$data['container'] 	   = '#work_exp_container';
						$resources['load_js']  = array(JS_DATATABLE);
						$resources['load_css'] = array(CSS_DATATABLE);
						
						$resources['load_modal']	= array(
								'modal_staff_work_exp' => array(
										'controller'	=> 'ceis_request_staffing',
										'module'		=> PROJECT_CEIS,
										'multiple'		=> true,
										'size'			=> 'lg',
										'method'		=> 'modal_staff_work_exp',
										'title'			=> 'View Staff Work Experience',
										'height'        => '500px'
								),
						);
					break;
				case 'request_staffing_project':
						$data['container'] 	   = '#project_container';
						$resources['load_js']  = array(JS_DATATABLE);
						$resources['load_css'] = array(CSS_DATATABLE);
						
						$resources['load_modal']	= array(
								'modal_staff_project' => array(
										'controller'	=> 'ceis_request_staffing',
										'module'		=> PROJECT_CEIS,
										'multiple'		=> true,
										'size'			=> 'lg',
										'method'		=> 'modal_staff_project',
										'title'			=> 'View Staff Project',
										'height'        => '500px'
								),
						);
					break;
				case 'request_staffing_personal':
						$data['no_js'] 	   = TRUE;
						$data['container'] = '#personal_info_container';
						$add_info = explode('/', $add_info);
						$staff_id = base64_url_decode($add_info[0]);
						
						check_salt($staff_id, $add_info[1], $add_info[2]);
						
						$this->load->model('ceis_request_staff_personal_model', 'crspm');
						
						$data['staff_info']     = $this->crspm->get_staff_info(array('rqst_staff_id' => $staff_id));
						$data['staff_licenses'] = $this->crspm->get_staff_license(array('rqst_staff_id' => $staff_id));
					break;
				case 'request_sub_finance':
						$resources['load_js'] 		= array(JS_DATATABLE);
						$resources['load_css'] 		= array(CSS_DATATABLE);
						$data['modal_url']	        = $encoded_rid.'/'.$salt.'/'.$token;
						$resources['load_modal']	= array(
								'modal_finance' => array(
										'controller'	=> 'ceis_request_finance',
										'module'		=> PROJECT_CEIS,
										'multiple'		=> true,
										'method'		=> 'modal_finance',
										'title'			=> 'View Finance Statement',
										'size'			=> 'lg',
										'height'        => '500px'
								),
								'modal_view_statement' => array(
										'controller'	=> 'ceis_request_finance',
										'module'		=> PROJECT_CEIS,
										'multiple'		=> true,
										'method'		=> 'modal_view_statement',
										'title'			=> 'View Finance Statement',
										'size'			=> 'xl',
										'height'        => '500px'
								),
						);
						
					
					break;
				case 'request_sub_work_volume':
						$this->load->model('ceis_request_finance_model', 'crfm');

						$data['no_js']  = TRUE;
						$data['work_volume'] = $this->crfm->get_request_top_statements($request_id);
					break;
				case 'request_sub_construction_equipment':
						$resources['load_js']  = array(JS_DATATABLE);
						$resources['load_css'] = array(CSS_DATATABLE);
						
						$resources['load_modal']  = array(
								'modal_equipment' => array(
										'controller'	=> 'ceis_request_construction_equipment',
										'module'		=> PROJECT_CEIS,
										'multiple'		=> true,
										'method'		=> 'modal_equipment',
										'title'			=> 'View Equipment',
										'height'        => '500px'
								),
						);
					break;
				case 'request_sub_checklist':
						$this->load->model('ceis_params_model', 'cpm');
						$this->load->model('ceis_request_checklist_model', 'crcm');
					
						
						$data['no_js'] 			= TRUE;
						$data['checklist']		= $this->cpm->get_param_checklist(array('request_type_id' => $request_type_id));
						$data['req_checklist']	= $this->crcm->get_request_checklist($request_id);
						//$data['url']			= $encoded_rid.'/'.$salt.'/'.$token;
						$data['url']			= $salt_link;
						
						$resources['load_modal']	= array(
								'modal_checklist' => array(
										'controller'	=> 'ceis_request_checklist',
										'module'		=> PROJECT_CEIS,
										'multiple'		=> true,
										'method'		=> 'modal_checklist',
										'title'			=> 'View Checklist',
								),
						);
						
						$resources['loaded_init']	= array('ModalEffects.re_init();');
					break;
				case 'request_sub_order_of_payment':
						$where = array('request_id' => $request_id, 'generate_order_of_payment' => 1);
						$task_details = $this->requests->get_request_task($where);
						
						if($task_details['task_status_id'] != REQUEST_TASK_NOT_YET_STARTED)
						{
/* 							$resources['load_js'] 		= array(JS_DATATABLE, JS_DATETIMEPICKER, JS_MODAL_CLASSIE, JS_MODAL_EFFECTS);
							$resources['load_css'] 		= array(CSS_DATATABLE, CSS_DATETIMEPICKER, CSS_MODAL_COMPONENT); */
							$resources['load_js'] 		= array(JS_DATATABLE, JS_MODAL_CLASSIE, JS_MODAL_EFFECTS);
							$resources['load_css'] 		= array(CSS_DATATABLE, CSS_MODAL_COMPONENT);
							$resources['datatable'][]	= array(
									'table_id' 	=> 'table_order_of_payment',
									'path'		=> PROJECT_CEIS.'/ceis_request_sub_order_of_payment/get_op_list/'.$encoded_rid
							);
						 	$resources['load_modal']	= array(
									'modal_op' => array(
											'controller'	=> 'ceis_request_sub_order_of_payment',
											'module'		=> PROJECT_CEIS,
											'multiple'		=> true,
											'method'		=> 'modal_op',
											'height'   	    => '500px',
											'size' 			=> 'lg',
											'title'         => 'Order of Payment'
									),
							); 
						 	$data['url'] = base64_url_encode(ACTION_ADD).'/'.$encoded_rid;
						 	$delete_arr  = array('controller' => 'ceis_request_sub_order_of_payment', 'method' => 'delete_op');
						}
						else
						{
							$access['title']    = 'Ooooops.';
							$access['message']  = 'This page will be available when we reach the order of payment task.';
							$data['html']       = $this->load->view('errors/html/error_custom', $access, TRUE);
						}
					break;
				case 'request_sub_evaluation':
						$where = array('request_id' => $request_id, 'evaluation_flag' => 1);
						$task_details = $this->requests->get_request_task($where);						
						$current_task = $this->requests->get_current_task($request_id);

/* 						if($task_details['task_status_id'] != REQUEST_TASK_NOT_YET_STARTED)
						{ */
							$this->load->model('ceis_request_evaluation_model', 'crem');
						
							$contractor_details		= $this->requests->get_specific_request_contractor( $request_id );
							$contractor_category    = $contractor_details['contractor_category_id'];
							
							$resources['load_js']   = array(PATH_JS_MODULES.PROJECT_CEIS.'/ceis_requests', JS_MODAL_CLASSIE);
							$resources['load_css'] 	= array(CSS_MODAL_COMPONENT);
							$data['financial']   	= $this->crem->get_financial_data($request_id);
							
							$work_volume_years   	= ($request_type_id == REQUEST_TYPE_NEW) ? 3 : 2;
							$work_volume			= $this->crem->get_work_volume($request_id, $work_volume_years);
							
							$data['work_volume']    = ( ! EMPTY($work_volume)) ? decimal_format($work_volume) : 0.00;
							
							$evaluation_details  	= $this->crem->get_request_evaluation(array('request_id' => $request_id));
							$data['specialization'] = $this->crem->get_specialization($request_id);
					
							
							if(EMPTY($evaluation_details)):
								$data['construction_contract'] 	 = $this->crem->get_actual_construction_years($request_id);
								$data['rescinded_contract'] 	 = $this->crem->get_contract_per_status($request_id, CONTRACT_STATUS_RESCINDED);
								$data['total_staff']			 = $this->crem->get_key_staff_number($request_id);
								
								$where = array('request_id' => $request_id, 'contract_status_id' => CONTRACT_STATUS_COMPLETED); 
								switch($contractor_category):
									case CONTRACTOR_CONSTRUCTION : 
											$where['contract_cost'] = array('10000000' , array('>='));
										break;
									case CONTRACTOR_SPECIALTY :
											$where['contract_cost'] = array('5000000' , array('>='));
										break;
									case CONTRACTOR_CONSULTANCY :
											$where['contract_cost'] = array('1000000' , array('>='));
										break;
								endswitch;
								
								$data['completed_contract']		 = $this->crem->get_completed_contracts($where);
								
							else:
								$data['legal_requirement'] 	   	 = $evaluation_details['legal_requirement'];
								$data['construction_contract'] 	 = $evaluation_details['total_actual_constr_year'];
								$data['completed_contract']		 = $evaluation_details['total_completed_contracts'];
								$data['rescinded_contract'] 	 = $evaluation_details['total_rescinded_contracts'];
								$data['work_experience'] 	 	 = $evaluation_details['work_experience'];
								$data['total_staff'] 	 		 = $evaluation_details['total_staff'];
								$data['staff_reqmt'] 	 		 = $evaluation_details['staff_reqmt'];
								$data['recommendation'] 	 	 = $evaluation_details['committee_recommend'];
							endif;
							
							$resources['load_modal']	= array(
									'modal_evaluation_report' => array(
											'controller'	=> __CLASS__,
											'module'		=> PROJECT_CEIS,
											'multiple'		=> true,
											'ajax'		    => true,
											//'method'		=> 'modal_op',
											'height'   	    => '450px',
											'size' 			=> 'xl',
											'title'         => 'Evaluation'
									),
							);
							
							$data['show_save_btn'] = ($current_task['evaluation_flag'] == 1 && $current_task['task_status_id'] == REQUEST_TASK_ONGOING && in_array($current_task['role_code'], $this->user_roles)) ? TRUE : FALSE;
							
/* 						}
						else 
						{
							$access['title']    = 'Ooooops.';
							$access['message']  = 'This page will be available when we reach the evaluation task.';
							$data['html']       = $this->load->view('errors/html/error_custom', $access, TRUE);
						} */
					break;
				case 'request_sub_registration':
						$this->load->model('ceis_request_tasks_model', 'crtm');
						
						$where		  = array('request_id' => $request_id, 'get_board_decision' => 1);
						$task_details = $this->requests->get_request_task($where);
						$current_task = $this->requests->get_current_task($request_id);
						
						if($task_details['task_status_id'] != REQUEST_TASK_NOT_YET_STARTED)
						{
							$this->load->model('ceis_request_registration_model', 'crrm');
	
							$data['registration_details'] = $this->crrm->get_registration_details($request_id);
							
							if($data['request_details']['request_type_id'] == REQUEST_TYPE_REN):
								$where = array('registration_no' => $data['request_details']['pocb_registration_no']);
							
								$data['curr_reg'] = $this->crtm->get_registration_details($where);
							endif;
							
							$data['show_save_btn'] = ($current_task['get_board_decision'] == 1 && $current_task['task_status_id'] == REQUEST_TASK_ONGOING && in_array($current_task['role_code'], $this->user_roles)) ? TRUE : FALSE;
						}	
						else 
						{
							$access['title']    = 'Ooooops.';
							$access['message']  = 'This page will be available when we reach the board result task.';
							$data['html']       = $this->load->view('errors/html/error_custom', $access, TRUE);
						}
					break;
				case 'request_sub_request_status':
					break;
				case 'request_sub_request_status_tasks':
						$resources['load_js'] 	  = array(JS_DATATABLE, JS_DATATABLE_RELOAD);
						$resources['load_css'] 	  = array(CSS_DATATABLE);
						$resources['load_modal']	= array(
								'modal_task' => array(
										'controller'	=> 'ceis_request_tasks',
										'module'		=> PROJECT_CEIS,
										'multiple'		=> true,
										'method'		=> 'modal_task',
										'title'         => 'Change Status'
								),
								'modal_remarks' => array(
										'controller'	=> 'ceis_request_tasks',
										'module'		=> PROJECT_CEIS,
										'multiple'		=> true,
										'method'		=> 'modal_remarks',
										'title'         => 'Add Remarks',
										'size'			=> 'xl',
								),
								'modal_attachments' => array(
										'controller'	=> 'ceis_request_tasks',
										'module'		=> PROJECT_CEIS,
										'multiple'		=> true,
										'method'		=> 'modal_attachments',
										'title'         => 'Add Attachments',
										'size'			=> 'xl',
										//'height'  	    => '380px'
								),
						);
						
						$data['container'] = '#tasks_container';
						//$delete_arr        = array('controller' => 'ceis_request_tasks', 'method' => 'delete_remarks');
					break;
				case 'request_sub_request_status_remarks':
						$this->load->model('ceis_request_tasks_model', 'crtm');
						
						$data['no_js'] 	     = TRUE;
						$data['container']   = '#remarks_container';
						$data['remarks'] 	 = $this->crtm->get_remarks($request_id);
					break;
				case 'request_sub_request_status_attachments':
						$this->load->model('ceis_request_tasks_model', 'crtm');
						
						$data['no_js'] 	     = TRUE;
						$data['container']   = '#attachements_container';
						$data['attachments'] = $this->crtm->get_attachments($request_id);
						//$data['filepath']	 = $encoded_rid.'/'.$salt.'/'.$token;
						$data['filepath']	 = $salt_link;
					break;
				case 'request_sub_project_info' :
						$this->load->model('ceis_request_project_info_model', 'crpim');
						
						$data['no_js']    = TRUE;
						$data['project']  = $this->crpim->get_project_projects($request_id);
						//print_r($data['project']);
					break;
				case 'request_sub_service_info' :
						$this->load->model('ceis_request_project_info_model', 'crpim');
						
						$data['no_js']   = TRUE;
						$data['project'] = $this->crpim->get_project_services($request_id);
					break;
				case 'request_sub_owner' :
						$this->load->model('ceis_request_project_info_model', 'crpim');
						
						$data['no_js']  = TRUE;
						$data['owner']  = $this->crpim->get_project_projects($request_id);
					break;
				case 'request_sub_manpower_schedule' :
						$resources['load_js'] 		= array(JS_DATATABLE, JS_MODAL_CLASSIE, JS_MODAL_EFFECTS);
						$resources['load_css'] 		= array(CSS_DATATABLE, CSS_MODAL_COMPONENT);
						$resources['datatable'][]	= array(
								'table_id' 	=> 'table_manpower_schedule',
								'path'		=> PROJECT_CEIS.'/ceis_request_sub_manpower_schedule/get_manpower_list/'.$encoded_rid
						);
					 	$resources['load_modal']	= array(
								'modal_manpower' => array(
										'controller'	=> 'ceis_request_sub_manpower_schedule',
										'module'		=> PROJECT_CEIS,
										'multiple'		=> true,
										'method'		=> 'modal_manpower',
										'size' 			=> 'lg',
										'title'         => 'Manpower Schedule'
								),
						); 
					 	$data['url'] = base64_url_encode(ACTION_ADD).'/'.$encoded_rid;
					 	$delete_arr  = array('controller' => 'ceis_request_sub_manpower_schedule', 'method' => 'delete_manpower');
				 	break;
				 case 'request_sub_foreign_principal' :
				 		$this->load->model('ceis_request_project_info_model', 'crpim');
				 		
						$data['no_js']  = TRUE;
						$data['foreign_principal'] = $this->crpim->get_project_services($request_id);
					break;
				 case 'request_sub_equipment':
				 		$this->load->model('ceis_request_construction_equipment_model', 'crcem');
				 		
				 		$data['no_js']  = TRUE;
				 		$data['equip_details'] = $this->crcem->get_equipment_details_by_request_id($request_id);
				 		
				 		//print_r($data['equip_details']);
				 	break;
				default :
					throw new Exception($this->lang->line('err_invalid_request'));
			endswitch;
			
			$resources['load_js'][]  = JS_DATETIMEPICKER;
			$resources['load_css'][] = CSS_DATETIMEPICKER;
			
			if( ! EMPTY($delete_arr))
				$resources['load_init'][] = "var deleteObj = new handleData({controller:'".$delete_arr['controller']."', method:'".$delete_arr['method']."', module:'".PROJECT_CEIS."'});";
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function load_tab()
	{
		try 
		{
			$view       = '';
			$params 	= get_params();
			//$tab		= $params['page'];
			$page		= $params['page'];
			$security	= $params['security'];
			$add_info   = $params['additional_info'];
			$modal 		= array();
			$data 		= array();	
			$resources 	= array();
			$html       = '';
			$container  = '.tab-content';

			IF(EMPTY($security)) throw new Exception($this->lang->line('err_invalid_request'));
								
			$parsed_id	= explode('/', $security);
			
			$encoded_id	= $parsed_id[0]; 
			$salt		= $parsed_id[1];
			$token		= $parsed_id[2];
			
			// CHECK SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);
			
			// DECODE 
			$hash_id	= base64_url_decode($encoded_id);
			
			/*// GET SPECIFIC REQUEST
			$info 				= $this->requests->get_specific_request($decoded_id);
			$request_type_id	= $info['request_type_id'];*/

			// START: GET THE CONTRACTOR INFORMATION USING HASH ID
			$key 	= $this->get_hash_key('request_id');
			$where	= array( $key => $hash_id);
			//$info 	= $this->requests->get_specific_request($where);
			$request_details = $this->requests->get_specific_request($where);
			
			$this->_set_data_per_page($page, $data, $request_details, $resources, $add_info);
			
			if( ! ISSET($data['no_js'])):
				$js_file = 'ceis_'.$page;
				$uc_page = implode('', array_map('_ucfirst',explode('_', $js_file)));
			
				$resources['load_js'][]   = PATH_JS_MODULES.PROJECT_CEIS.'/'.$js_file;
				$resources['loaded_init'][] = $uc_page.'.init();';
			endif;
			
			if(ISSET($data['container'])):
				$container = $data['container'];
			endif;
			
			if(ISSET($data['html'])):
				$html = $data['html'];
			
				$resources = array('load_js' => array(JS_DATETIMEPICKER), 'load_css' => array(CSS_DATETIMEPICKER));
			else:
				$html = $this->load->view($page, $data, TRUE);
			endif;
			
			if( ! EMPTY($resources))
				$html.= $this->load_resources->get_resource( $resources , TRUE);
			 
			//$req_cont	= $this->requests->get_specific_request_contractor($where);
			// END: GET THE CONTRACTOR INFORMATION USING HASH ID
			
			//CONTRACTOR ID
			$req_id 	= $hash_id;

			/*
			switch($tab)
			{
				case 'contractor':
					$resources['load_js']			= array(JS_SELECTIZE);	
					$resources['load_css']			= array(CSS_SELECTIZE);
					$data["contractor_category"] 	= $this->requests->get_category();

					$data['contractor_name'] 		= $req_cont['contractor_name'];
					$data['contractor_address'] 	= $req_cont['contractor_address'];
					$data['contractor_category_id'] = $req_cont['contractor_category_id'];
					$data['sec_reg_no'] 			= $req_cont['sec_reg_no'];
					$data['sec_reg_date'] 			= $req_cont['sec_reg_date'];
					$data['license_no'] 			= $req_cont['license_no'];
					$data['license_date'] 			= $req_cont['license_date'];
					$data['bir_clearance_date'] 	= $req_cont['bir_clearance_date'];
					$data['customs_clearance_date'] = $req_cont['customs_clearance_date'];
					$data['court_clearance_date'] 	= $req_cont['court_clearance_date'];
					$data['rep_first_name'] 		= $req_cont['rep_first_name'];
					$data['rep_mid_name'] 			= $req_cont['rep_mid_name'];
					$data['rep_last_name'] 			= $req_cont['rep_last_name'];
					$data['rep_address'] 			= $req_cont['rep_address'];
					$data['rep_contact_no'] 		= $req_cont['rep_contact_no'];
					$data['rep_email'] 				= $req_cont['rep_email'];

					$this->load->view('request_sub_contractor_info',$data);
					$this->load_resources->get_resource( $resources );
				break;
				
				case 'contracts':
					$modal = array(
						'modal_project' => array(
							'controller'	=> __CLASS__,
							'module'		=> PROJECT_CEIS,
							'multiple'		=> true,
							'method'		=> 'modal_project'
						),

						'modal_manpower' => array(
							'controller'	=> __CLASS__,
							'module'		=> PROJECT_CEIS,
							'multiple'		=> true,
							'method'		=> 'modal_manpower'
						)
					);

					$resources['load_modal'] 	= $modal;
					$resources['load_css'] 		= array(CSS_DATATABLE);
					$resources['load_js'] 		= array(JS_DATATABLE);
					$resources['datatable'] 	= array();
					$resources['datatable'][]	= array(
						'table_id' 	=> 'table_projects', 
						'path'		=> PROJECT_CEIS.'/ceis_requests/get_contract_project_list/'.$req_id.''
					);

					$resources['datatable'][]	= array(
						'table_id' 	=> 'table_manpower_services', 
						'path'		=> PROJECT_CEIS.'/ceis_requests/get_contract_service_list/'.$req_id.''
					);		
					

					$this->load->view('request_sub_contracts');
					$this->load_resources->get_resource( $resources );
				break;
				
				case 'staffing':

					$modal = array(
						'modal_director' => array(
							'module'			=> PROJECT_CEIS,
							'controller'		=> __CLASS__,
							'multiple'			=> true,
							'method'			=> 'modal_director',
						),

						'modal_principal' 	=> array(
							'module'			=> PROJECT_CEIS,
							'controller'		=> __CLASS__,
							'multiple'			=> true,
							'method'			=> 'modal_principal',
						),

						'modal_technical' 	=> array(
							'module'			=> PROJECT_CEIS,
							'controller'		=> __CLASS__,
							'multiple'			=> true,
							'method'			=> 'modal_technical',
						)
					);

					$resources['load_modal'] 	= $modal;			
					$resources['load_css'] 		= array(CSS_DATATABLE);
					$resources['load_js'] 		= array(JS_DATATABLE);
					$resources['datatable'] 	= array();

					$resources['datatable'][]	= array(
						'table_id' 	=> 'table_director_staffing', 
						'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_director_list/D'
					);		

					$resources['datatable'][]	= array(
						'table_id' 	=> 'table_principal_officer_staffing', 
						'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_director_list/P'
					);	

					$resources['datatable'][]	= array(
						'table_id' 	=> 'table_tech_staff_staffing', 
						'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_director_list/T'
					);	

					$this->load->view('request_sub_staffing');
					$this->load_resources->get_resource( $resources );
				break;
					
					case 'staffing_director':
						$modal 		= array(
								'modal_request_staffing_experience'	=> array(
									'module'		=> PROJECT_CEIS,
									'controller'	=> __CLASS__,
									'multiple'		=> true,
									'method'		=> 'modal_request_staffing_experience'
											),
								'modal_request_staffing_project'	=> array(
									'module'		=> PROJECT_CEIS,
									'controller'	=> __CLASS__,
									'multiple'		=> true,
									'method'		=> 'modal_request_staffing_project'
											)
								);
						$resources['load_modal']	= $modal;
						$resources['load_css'] 		= array(CSS_DATATABLE);
						$resources['load_js'] 		= array(JS_DATATABLE);
						$resources['datatable'][]	= array(
							'table_id' 	=> 'contractor_staffing_experience', 
							'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_experience_list'
						);
						$resources['datatable'][]	= array(
							'table_id' 	=> 'contractor_staffing_project', 
							'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_project_list'
						);

						$this->load->view('request_staffing_personal',$data);
						$this->load_resources->get_resource($resources);
					break;

				case 'finance':

					$modal = array(
						'modal_finance' => array(
							'module'			=> PROJECT_CEIS,
							'controller'		=> __CLASS__,
							'multiple'		=> true,
							'method'		=> 'modal_finance',
						)
					);

					$resources['load_modal'] 	= $modal;
					$resources['load_css'] 		= array(CSS_DATATABLE);
					$resources['load_js'] 		= array(JS_DATATABLE);
					$resources['datatable'][]	= array(
						'table_id' 	=> 'table_finance', 
						'path'		=> PROJECT_CEIS.'/ceis_requests/get_finance_request_statements_list'
					);

					$this->load->view('request_sub_finance');
					$this->load_resources->get_resource( $resources );
				break;
				
				case 'work':
					$this->load->view('request_sub_work_volume');
				break;
				
				case 'project':
					$this->load->view('request_sub_project_info');
				break;
				
				case 'foreign':
					$this->load->view('request_sub_foreign_principal');
				break;
				
				case 'owner':
					$this->load->view('request_sub_owner');
				break;
				
				case 'manpower':
					$this->load->view('request_sub_manpower_schedule');
				break;
				
				case 'equipment':

					$modal = array(
						'modal_construction_equipment' => array(
							'module'		=> PROJECT_CEIS,
							'controller'	=> __CLASS__,
							'multiple'		=> true,
							'method'		=> 'modal_construction_equipment',
							'height'		=> '450px;'
						)
					);

					$resources['load_modal'] 	= $modal;
					$resources['load_css'] 		= array(CSS_DATATABLE);
					$resources['load_js'] 		= array(JS_DATATABLE);
					$resources['datatable'][]	= array(
						'table_id' 	=> 'table_construction_equipment', 
						'path'		=> PROJECT_CEIS.'/ceis_requests/get_construction_equipment_list'
					);

					$this->load->view('request_sub_construction_equipment');
					$this->load_resources->get_resource( $resources );
				break;
				
				case 'eqp_info':
					$this->load->view('request_sub_equipment');
				break;
				
				case 'checklist':
									
					$data['checklist']	= $this->requests->get_checklist($request_type_id);
					
					
					$this->load->view('request_sub_checklist', $data);
				break;
				
				case 'op':
					$modal = array(
						'modal_request_order_payment' => array(
							'module'		=> PROJECT_CEIS,
							'controller'	=> __CLASS__,
							'multiple'		=> true,
							'method'		=> 'modal_request_order_payment',
							'height'		=> '450px;'
						)
					);

					$resources['load_modal'] 	= $modal;
					$resources['load_css'] 		= array(CSS_DATATABLE);
					$resources['load_js'] 		= array(JS_DATATABLE);
					$resources['datatable'][]	= array(
						'table_id' 	=> 'table_order_of_payment', 
						'path'		=> PROJECT_CEIS.'/ceis_requests/get_request_payment_list'
					);	

					$this->load->view('request_sub_order_of_payment');
					$this->load_resources->get_resource( $resources );
				break;
				
				case 'evaluation':
					$data 		= array();
					$resources 	= array();
					$modal 		= array(
						'modal_evaluation_report' => array(
							'controller'	=> __CLASS__,
							'module'		=> PROJECT_CEIS,
							'multiple'		=> true,
							'height'		=> '450px;',
							'ajax'			=> true,
							'size'			=> 'xl'
						)
					);

					$resources['load_modal'] 	= $modal;
					$resources['load_css'] 		= array(CSS_SELECTIZE,CSS_MODAL_COMPONENT);
					$resources['load_js'] 		= array(JS_SELECTIZE,JS_MODAL_CLASSIE,JS_MODAL_EFFECTS,'modules/ceis/'.'ceis_requests');
					
					$data['request_id'] 		= $info['request_id'];
					$data['reference_name']		= $info['reference_name'];
					$data['request_type_id'] 	= $info['request_type_id'];	
					$encoded_id					= base64_url_encode($info['request_id']);

					$this->load->view('request_sub_evaluation',$data);
					$this->load_resources->get_resource($resources);
				break;

				case 'registration':
					$resources['load_css'] 		= array(CSS_DATETIMEPICKER);
					$resources['load_js'] 		= array(JS_DATETIMEPICKER);
					$this->load->view('request_sub_registration');
					$this->load_resources->get_resource( $resources );
				break;
				
				case 'status':
					
					$resources['load_js']	= array('modules/ceis/ceis_view_request');
					$resources['load_init'] = array("LoadTab.init_status_tab('tasks');");
					
					$this->load->view('request_sub_request_status');
					$this->load_resources->get_resource($resources);
				break;
				
				case 'tasks':
					$this->load->view('request_sub_request_status_tasks');
				break;
				
				case 'remarks':
					$this->load->view('request_sub_request_status_remarks');
				break;
				
				case 'attachments':
					$this->load->view('request_sub_request_status_attachments');
				break;
			}
			
			*/
		}
		catch(PDOException $e)
		{
			$html =  $e->getMessage();
		}
		catch(Exception $e)
		{
			$html =  $e->getMessage();
		}
		
		echo json_encode(array('html' => $html, 'container' => $container));
	}
	
	
	public function submenu_request($encoded_id)
	{
		try
		{
			$data 					= array();
			$resources 				= array();			
			$resources['load_js']  	= array(JS_DATETIMEPICKER);
			$resources['load_css'] 	= array(CSS_DATETIMEPICKER);
			// $data['url_value'] 		= $this->get_url_value($url_value);

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			// CHECK THE SECURITY VARIABLES
			// check_salt($id, $salt, $token);	
			
			$info 							= $this->requests->get_specific_request($id);
			$contractor_info				= $this->requests->get_specific_contractor($id);
			
			// $category_info					= $this->requests->get_specific_category($id);

			$data['contractor_name']		= $contractor_info['contractor_name'];
			$data['contractor_address']		= $contractor_info['contractor_address'];
			$data['sec_reg_no']				= $contractor_info['sec_reg_no'];
			$data['sec_reg_date']			= $contractor_info['sec_reg_date'];
			$data['contractor_category_id']	= $contractor_info['contractor_category_id'];
			$data['license_no']				= $contractor_info['license_no'];
			$data['license_date']			= $contractor_info['license_date'];
			$data['rep_first_name']			= $contractor_info['rep_first_name'];
			$data['rep_mid_name']			= $contractor_info['rep_mid_name'];
			$data['rep_last_name']			= $contractor_info['rep_last_name'];
			$data['rep_address']			= $contractor_info['rep_address'];
			$data['rep_contact_no']			= $contractor_info['rep_contact_no'];
			$data['rep_email']				= $contractor_info['rep_email'];
			$data['bir_clearance_date']		= $contractor_info['bir_clearance_date'];
			$data['customs_clearance_date']	= $contractor_info['customs_clearance_date'];
			$data['court_clearance_date']	= $contractor_info['court_clearance_date'];

			$data['request_id'] 			= $info['request_id'];
			$data['reference_name'] 		= $info['reference_name'];
			$data['request_type_id'] 		= $info['request_type_id'];

			$encoded_id						= base64_url_encode($info['request_id']);

			$salt 							= gen_salt();
			$in_salt						= in_salt($encoded_id, $salt);
			$data['security']				= $encoded_id . '/' . $salt . '/' . $token;


		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_contractor_info', $data, $resources);
	}

	public function submenu_work_experience($encoded_id)
	{
		try
		{
			$data 					= array();
			$resources 				= array();			
			$resources['load_js']  	= array(JS_DATETIMEPICKER, JS_DATATABLE, 'modules/ceis/'.'ceis_requests');
			$resources['load_css'] 	= array(CSS_DATETIMEPICKER, CSS_DATATABLE);
			$resources['datatable'] = array('table_id' => 'tbl_work_experience_projects', 'path' => PROJECT_CEIS.'/ceis_requests/get_workexperience_project_list');

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 					= base64_url_decode($encoded_id);

			// CHECK THE SECURITY VARIABLES
			// check_salt($encoded_id, $salt, $token);	
			
			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];

			$encoded_id				= base64_url_encode($info['request_id']);

			$salt 					= gen_salt();
			$in_salt				= in_salt($encoded_id, $salt);
			$data['security']		= $encoded_id . '/' . $salt . '/' . $token;

		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_work_experience', $data, $resources);	
	}

	public function submenu_work_experience_manpower($encoded_id)
	{
		try
		{
			$data 					= array();
			$resources 				= array();			
			$resources['load_js']  	= array(JS_DATETIMEPICKER, JS_DATATABLE, 'modules/ceis/'.'ceis_requests');
			$resources['load_css'] 	= array(CSS_DATETIMEPICKER, CSS_DATATABLE);
			$resources['datatable'] = array('table_id' => 'tbl_work_experience_projects', 'path' => PROJECT_CEIS.'/ceis_requests/get_workexperience_project_list');

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);
		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_work_experience_manpower', $data, $resources);	
	}
	
	public function get_workexperience_project_list(){

		try{

		$user 	    					= array();
		$params     					= get_params();
		$workexperience_project_list	= $this->requests->get_workexperience_project_list($params);
		
		$aColumns = array(
				'a.rqst_staff_proj_id',
				'a.rqst_staff_id',
				'a.project_title',
				'a.project_location',
				'a.owner',
				'a.positions',
			);

		// APPEARS ON TABLE
		$bColumns = array('request_id',
				'requested_date',
				'request_type_id',
				'reference_no',
				'reference_name',
				'reference_name',
				'request_status_id');

		$requests 		= $this->requests->get_request_list($aColumns, $bColumns, $params);
		$iTotal 		= $this->requests->total_length();
		$iFilteredTotal = $this->requests->filtered_length($aColumns, $bColumns, $params);


		if(!empty($workexperience_project_list['aaData']))
		{
			foreach($workexperience_project_list['aaData'] as $result)
			{				
				$user[] = array(
						$result['project_title'],
						$result['project_location'],
						$result['owner'],
						$result[''],
						$result[''],						
						'<a href="equipment_remarks" data-toggle="ajaxModalStack"><i class="fa fa-search"></i></a>'
				);
			}
		}

		$workexperience_project_list['aaData'] = $user;
		echo json_encode($workexperience_project_list);

		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}


	public function submenu_evaluation($encoded_id)
	{
		try
		{
			$data 		= array();
			$resources 	= array();
			$modal 		= array(
				'modal_evaluation_report' => array(
					'controller'	=> __CLASS__,
					'module'		=> PROJECT_CEIS,
					'multiple'		=> true,
					'height'		=> '450px;',
					'ajax'			=> true,
					'size'			=> 'xl'
				)
			);

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 						= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$resources['load_css'] 		= array( CSS_SELECTIZE, CSS_MODAL_COMPONENT);
			$resources['load_js'] 		= array( JS_SELECTIZE,JS_MODAL_CLASSIE,JS_MODAL_EFFECTS, 'modules/ceis/'.'ceis_requests');
			$resources['load_modal'] 	= $modal;

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);		
		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_evaluation', $data, $resources);	
		
	}

	//SKILLS SUMMARY
	public function generate_report()
	
	{
		try{

			$success 				= 0;
			$msg 	 				= "";
			$data 	 				= array();
			$link 	 				= "";
			$val_msg 				= array();
			$modal_data 			= array();
			$report 				= '';
			$modal 					= '';
			$html 					= '';
			$params     			= get_params();
			$data['report']			= 2;
			$report 				= 2;
			$data['generate']		= 1;
			// $data['contractor_list']= $params['contractor_list'];
			// $data['quarter']		= $params['quarter'];

			$filename 				= 'ceis_reports_'.date("Y_m_d");

			$reports 				= array(
				'1' 				=> 'report_evaluation',
				'2' 				=> 'report_evaluation'
			);

			$modals 				= array(
				'1' 				=> 'report_container_modal_menu',
				'2'					=> 'report_container_modal'
			);

			//report_evaluation, report_container_modal
			$modal 					= $modals[ $data['report'] ];
			$report 				= $reports[ $data['report'] ];



			$html  .= $this->load->view('reports/'.$report, $data,TRUE);

			$html.= $this->set_report_footer();

			// if(!EMPTY($data['report'])){	
				$data['generate'] = 'PDF';
				$modal_page	= "report_container_modal";

				$filename 	= $filename.'.pdf';

				$pdf = $this->pdf( $filename, $html, TRUE, 'S' );

				$modal_data['pdf'] 			= $pdf;
				$modal_data['report_title']	= $data['report'];

				$modal = $this->load->view('modals/' . $modal_page, $modal_data, TRUE);	
				$success 	= 1;
			// }
			/*
			else{
				$modal_page = "report_container_modal";
				if( $data['generate'] == 'PDF' )
				{
					$filename 	= $filename.'.pdf';

					$pdf = $this->pdf( $filename, $html, FALSE, 'S' );

					$modal_data['pdf'] 			= $pdf;
					$modal_data['report_title']	= $data['report'];

					$modal = $this->load->view('modals/' . $modal_page, $modal_data, TRUE);	
					$success 	= 1;
				}
				else if( $data['generate'] == 'Excel' )
				{
					$this->convert_excel( $html, $filename , '');
					$success 	= 1;
				}	
			}*/
			
		
		}catch(PDOException $e){

			$msg 	= $e->getMessage().' -Line '.$e->getLine().' - '.__FUNCTION__;

		}catch(Exception $e){

			$msg 	= $e->getMessage().' -Line '.$e->getLine().' - '.__FUNCTION__;

			if(!EMPTY($val_msg['flag'])) $msg = $e->getMessage();

		}

		echo $msg;

		if( $data['generate'] == 'PDF' )
		{
	
			$ajaxData 	= array(

					'success'=> $success,
					'msg'	=> $msg,
					'modal' => $modal,
					'data' => $data
			);

			echo  json_encode($ajaxData);

		}
	}

	/* public function submenu_request_status($encoded_id)
	{
		try
		{
			$data 		= array();	
			$resources 	= array();

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);		
		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_request_status', $data, $resources);	
		
	} */

	public function submenu_staffing($encoded_id)
	{
		try
		{
			$data 						= array();	
			$resources 					= array();	
			$resources['load_css'] 		= array(CSS_DATATABLE);
			$resources['load_js'] 		= array(JS_DATATABLE);
			$resources['datatable'] 	= array();

			$modal = array(
				'modal_director' => array(
					'module'			=> PROJECT_CEIS,
					'controller'		=> __CLASS__,
					'multiple'			=> true,
					'method'			=> 'modal_director',
				),

				'modal_principal' 	=> array(
					'module'			=> PROJECT_CEIS,
					'controller'		=> __CLASS__,
					'multiple'			=> true,
					'method'			=> 'modal_principal',
				),

				'modal_technical' 	=> array(
					'module'			=> PROJECT_CEIS,
					'controller'		=> __CLASS__,
					'multiple'			=> true,
					'method'			=> 'modal_technical',
				)
			);

			$resources['load_modal'] 	= $modal;

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 						= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);

			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_director_staffing', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_director_list'
			);		

			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_principal_officer_staffing', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_principal_list'
			);	

			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_tech_staff_staffing', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_technical_list'
			);	

		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_staffing', $data, $resources);	
		
	}

	public function submenu_staffing_director($encoded_id)
	{
		try
		{
			$data 						= array();	
			$resources 					= array();	
			$resources['load_css'] 		= array(CSS_DATATABLE);
			$resources['load_js'] 		= array(JS_DATATABLE);
			$resources['datatable'] 	= array();

			$modal = array(
				'modal_director' => array(
					'module'			=> PROJECT_CEIS,
					'controller'		=> __CLASS__,
					'multiple'			=> true,
					'method'			=> 'modal_director',
				),

				'modal_principal' 	=> array(
					'module'			=> PROJECT_CEIS,
					'controller'		=> __CLASS__,
					'multiple'			=> true,
					'method'			=> 'modal_principal',
				),

				'modal_technical' 	=> array(
					'module'			=> PROJECT_CEIS,
					'controller'		=> __CLASS__,
					'multiple'			=> true,
					'method'			=> 'modal_technical',
				)
			);

			$resources['load_modal'] 	= $modal;

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);

			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_director_staffing', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_director_list'
			);		

			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_principal_officer_staffing', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_principal_list'
			);	

			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_tech_staff_staffing', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_technical_list'
			);	

		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_staffing_director', $data, $resources);	
		
	}

	public function submenu_staffing_principal($encoded_id)
	{
		try
		{
			$data 						= array();	
			$resources 					= array();	
			$resources['load_css'] 		= array(CSS_DATATABLE);
			$resources['load_js'] 		= array(JS_DATATABLE);
			$resources['datatable'] 	= array();

			$modal = array(
				'modal_director' => array(
					'module'			=> PROJECT_CEIS,
					'controller'		=> __CLASS__,
					'multiple'			=> true,
					'method'			=> 'modal_director',
				),

				'modal_principal' 	=> array(
					'module'			=> PROJECT_CEIS,
					'controller'		=> __CLASS__,
					'multiple'			=> true,
					'method'			=> 'modal_principal',
				),

				'modal_technical' 	=> array(
					'module'			=> PROJECT_CEIS,
					'controller'		=> __CLASS__,
					'multiple'			=> true,
					'method'			=> 'modal_technical',
				)
			);

			$resources['load_modal'] 	= $modal;

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);

			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_director_staffing', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_director_list'
			);		

			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_principal_officer_staffing', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_principal_list'
			);	

			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_tech_staff_staffing', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_technical_list'
			);	

		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_staffing_principal', $data, $resources);	
		
	}

	public function submenu_staffing_technical($encoded_id)
	{
		try
		{
			$data 						= array();	
			$resources 					= array();	
			$resources['load_css'] 		= array(CSS_DATATABLE);
			$resources['load_js'] 		= array(JS_DATATABLE);
			$resources['datatable'] 	= array();

			$modal = array(
				'modal_director' => array(
					'module'			=> PROJECT_CEIS,
					'controller'		=> __CLASS__,
					'multiple'			=> true,
					'method'			=> 'modal_director',
				),

				'modal_principal' 	=> array(
					'module'			=> PROJECT_CEIS,
					'controller'		=> __CLASS__,
					'multiple'			=> true,
					'method'			=> 'modal_principal',
				),

				'modal_technical' 	=> array(
					'module'			=> PROJECT_CEIS,
					'controller'		=> __CLASS__,
					'multiple'			=> true,
					'method'			=> 'modal_technical',
				)
			);

			$resources['load_modal'] 	= $modal;

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);

			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_director_staffing', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_director_list'
			);		

			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_principal_officer_staffing', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_principal_list'
			);	

			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_tech_staff_staffing', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_staffing_technical_list'
			);	

		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_staffing_technical', $data, $resources);	
		
	}
	public function submenu_finance($encoded_id)
	{
		try
		{
			$data 						= array();	
			$resources 					= array();	
			$resources['load_css'] 		= array(CSS_DATATABLE);
			$resources['load_js'] 		= array(JS_DATATABLE);
			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_finance', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_finance_request_statements_list'
			);

			$modal = array(
				'modal_finance' => array(
					'module'			=> PROJECT_CEIS,
					'controller'		=> __CLASS__,
					'multiple'		=> true,
					'method'		=> 'modal_finance',
				)
			);

			$resources['load_modal'] 	= $modal;

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);	
		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_finance', $data, $resources);	
		
	}

	public function submenu_construction_equipment($encoded_id)
	{
		try
		{
			$data 						= array();	
			$resources 					= array();	
			$resources['load_css'] 		= array(CSS_DATATABLE);
			$resources['load_js'] 		= array(JS_DATATABLE);
			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_construction_equipment', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_construction_equipment_list'
			);

			$modal = array(
				'modal_construction_equipment' => array(
					'module'		=> PROJECT_CEIS,
					'controller'	=> __CLASS__,
					'multiple'		=> true,
					'method'		=> 'modal_construction_equipment',
					'height'		=> '450px;'
				)
			);

			$resources['load_modal'] 	= $modal;

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);	
		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_construction_equipment', $data, $resources);	
		
	}

	public function submenu_work_volume($encoded_id)
	{
		try
		{
			$data 		= array();	
			$resources 	= array();

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);			
		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_work_volume', $data, $resources);	
		
	}


	public function submenu_checklist($encoded_id)
	{
		try
		{
			$data 				= array();	
			$resources 			= array();
			$params 			= get_params();
			$info 				= $this->requests->get_specific_request($where);

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 						= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$request_type_id 			= $data['request_type_id'];
			$encoded_id					= base64_url_encode($info['request_id']);

			$data['checklist']			= $this->requests->get_checklist($request_type_id);
		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_checklist', $data, $resources);	
		
	}

	public function submenu_order_of_payment($encoded_id)
	{
		try
		{
			$data 						= array();	
			$resources 					= array();	
			$resources['load_css'] 		= array(CSS_DATATABLE);
			$resources['load_js'] 		= array(JS_DATATABLE);
			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_order_of_payment', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_order_of_payment_list'
			);	

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			$info 							= $this->requests->get_specific_order_payment($id);

			$data['rqst_payment_id'] 		= $info['rqst_payment_id'];
			$data['op_date']				= $info['op_date'];
			$data['request_id'] 			= $info['request_id'];
			$data['request_type_id'] 		= $info['request_type_id'];	
			$request_type_id 				= $data['request_type_id'];

			$encoded_id						= base64_url_encode($info['rqst_payment_id']);		
		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_order_of_payment', $data, $resources);	
		
	}

	public function submenu_contracts($encoded_id)
	{
		try
		{
			$data 						= array();	
			$resources 					= array();	
			$modal 						= array();
			$resources['load_css'] 		= array(CSS_DATATABLE);
			$resources['load_js'] 		= array(JS_DATATABLE);
			


			$modal = array(
				'modal_project' => array(
					'controller'	=> __CLASS__,
					'module'		=> PROJECT_CEIS,
					'multiple'		=> true,
					'method'		=> 'modal_project'
				),

				'modal_manpower' => array(
					'controller'	=> __CLASS__,
					'module'		=> PROJECT_CEIS,
					'multiple'		=> true,
					'method'		=> 'modal_manpower'
				)
			);

			$resources['load_modal'] 	= $modal;

			//projects
			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_project', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_contract_project_list'
			);	

			//manpower services
			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_manpower_services', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_contract_manpower_list'
			);	

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);

		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_contracts', $data, $resources);	
		
	}

	public function submenu_project_info($encoded_id)
	{
		try
		{
			$data 		= array();	
			$resources 	= array();	

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);		
		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_project_info', $data, $resources);	
		
	}

	public function submenu_owner($encoded_id)
	{
		try
		{
			$data 		= array();	
			$resources 	= array();	

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);		
		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_owner', $data, $resources);	
		
	}

	public function submenu_manpower_schedule($encoded_id)
	{
		try
		{
			$data 						= array();	
			$resources 					= array();	
			$modal 						= array();
			$resources['load_css'] 		= array(CSS_DATATABLE);
			$resources['load_js'] 		= array(JS_DATATABLE);
			$resources['datatable'] 	= array();

			$resources['datatable'][]	= array(
				'table_id' 	=> 'table_manpower_schedule', 
				'path'		=> PROJECT_CEIS.'/ceis_requests/get_manpower_schedule_list'
			);	
			
			$modal 	= array(
					'modal_manpower_schedule' => array(
					'controller'	=> __CLASS__,
					'module'		=> PROJECT_CEIS,
					'multiple'		=> true,
					'method'		=> 'modal_manpower_schedule'
				)
			);

			$resources['load_modal'] 	= $modal;

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);		
		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_manpower_schedule', $data, $resources);	
		
	}

	public function submenu_foreign_principal($encoded_id)
	{
		try
		{
			$data 		= array();	
			$resources 	= array();	

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);		
		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_foreign_principal', $data, $resources);		
	}

	public function submenu_request_status_remarks($encoded_id)
	{
		try
		{
			$data 		= array();	
			$resources 	= array();

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);		
		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_request_status_remarks', $data, $resources);	
		
	}

	public function submenu_request_status_attachments($encoded_id)
	{
		try
		{
			$data 		= array();	
			$resources 	= array();

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$id 							= base64_url_decode($encoded_id);

			$info 						= $this->requests->get_specific_request($id);

			$data['request_id'] 		= $info['request_id'];
			$data['reference_name']		= $info['reference_name'];
			$data['request_type_id'] 	= $info['request_type_id'];	
			$encoded_id					= base64_url_encode($info['request_id']);			
		}

		catch(Exception $e)
		{
			echo $e->getMessage();
		}

		$this->template->load('request_sub_request_status_attachments', $data, $resources);	
		
	}

	public function get_url_value($uri_val){

		return $this->uri->segment(4);
	}
}



/* End of file Ceis_requests.php */
/* Location: ./application/modules/ceis/controllers/Ceis_requests.php */