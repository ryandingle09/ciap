<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ceis_request_construction_equipment extends Ceis_Controller
{
	private $module = MODULE_CEIS_REQUESTS;
	private $collection_group = array();

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('ceis_request_construction_equipment_model', 'crcem');
	}
	
	public function modal_equipment($encoded_id=NULL, $salt=NULL, $token=NULL)
	{
		try
		{
			$data = array();
			$rqst_equip_id = base64_url_decode($encoded_id);

			check_salt($rqst_equip_id, $salt, $token);
			
			$data['equip_details']  = $this->crcem->get_equipment_details($rqst_equip_id);
			
			$html  = $this->load->view('modals/construction_equipment',$data, TRUE);
			
			$html .= $this->load_resources->get_resource($resources, TRUE);
			
			echo $html;
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
	
	public function get_equipment_list()
	{
		try{
			$rows		= array();
			$params	 	= get_params();
			$ids     	= explode('/', $params['request_id']);
			$request_id = base64_url_decode($ids[0]);
			$salt 		= $ids[1];
			$token 		= $ids[2];
			
			check_salt($request_id, $salt, $token);
			
			
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns 		= array('a.rqst_equip_id', 'a.request_id', 'a.serial_no', 'a.capacity', 'YEAR(NOW()) - YEAR(a.acquired_date) as years_of_service', 'a.present_condition', 'FORMAT(a.acquisition_cost, 2) as acquisition_cost');
			// APPEARS ON TABLE
			$bColumns 		= array('serial_no', 'capacity', 'years_of_service', 'present_condition', 'acquisition_cost');
	
			$equipments 	= $this->crcem->get_equipment_list($aColumns, $bColumns, $params, $request_id);
					
			foreach($equipments['aaData'] as $aRow):
				$salt    = gen_salt();
				$token   = in_salt($aRow['rqst_equip_id'], $salt);
				$url     = base64_url_encode($aRow['rqst_equip_id'])."/".$salt."/".$token;
				
				$action  = "<div class='table-actions'>";
				$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_equipment' onclick=\"modal_equipment_init('".$url."')\" ><i class='flaticon-search95'></i></a>";
				$action .= "</div>";  
			
				$rows[]  = array($aRow['serial_no'], $aRow['capacity'], $aRow['years_of_service'], $aRow['present_condition'], $aRow['acquisition_cost'], $action);
			endforeach;
				
			$equipments['aaData'] = $rows;

		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
					
		echo json_encode($equipments);
	}
	

	public function save_order_payment()
	{
		try
		{	
			$flag 	= 0;
			$msg  	= '';
			$url    = '';
			$params = get_params();
			
			$this->_validate($params);
			
			CEIS_Model::beginTransaction();
		
			$prev_detail[]  = array();
			$prev_detail[]  = array();
			$audit_table[]	= CEIS_MODEL::tbl_request_payments;
			$audit_table[]	= CEIS_MODEL::tbl_request_payment_collections;
			$audit_schema[]	= DB_CEIS;
			$audit_schema[]	= DB_CEIS;
		
			if( ! EMPTY($params['rqst_payment_id'])):
				$audit_action[]	= AUDIT_UPDATE;
				$audit_action[]	= AUDIT_UPDATE;
				$prev_detail[]  = $this->crsoopm->get_order_of_payment_header($params['rqst_payment_id']);
				$prev_detail[]  = $this->crsoopm->get_order_of_payment_details($params['rqst_payment_id']);
			
				$this->crsoopm->update_order_of_payment($params);
				
				$this->crsoopm->delete_order_of_payment_details($params['rqst_payment_id']);
				
				$this->crsoopm->insert_order_of_payment_details($params);
				
				$activity	= "Request order of payment has been updated.";
			else:
				$audit_action[]	= AUDIT_INSERT;
				$audit_action[]	= AUDIT_INSERT;
				
				$params['rqst_payment_id'] = $this->crsoopm->insert_order_of_payment($params);
				
				$this->crsoopm->insert_order_of_payment_details($params);
				
				$activity	= "Request order of payment has been added.";
			endif;
			
			$op_header  = $this->crsoopm->get_order_of_payment_header($params['rqst_payment_id']);
			$op_details = $this->crsoopm->get_order_of_payment_details($params['rqst_payment_id']);
			
			$url 		= PROJECT_CEIS.'/ceis_request_sub_order_of_payment/get_op_list/'.base64_url_encode($op_header['request_id']);
			
			$curr_detail[]		= array($op_header);
			$curr_detail[]		= $op_details;
			
			$this->audit_trail->log_audit_trail(
					$activity,
					$this->module,
					$prev_detail,
					$curr_detail,
					$audit_action,
					$audit_table,
					$audit_schema
			);
			
			CEIS_Model::commit();
			
			$flag = 1;
			$msg  = sprintf($this->lang->line('data_spec_saved'), 'Order of Payment');
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
			
			$this->rlog_error($e);
			$msg = $this->lang->line('err_internal_server');
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
				"flag" => $flag,
				"msg"  => $msg,
				"url"  => $url
		);
		
		echo json_encode($info);
	}
	
	public function delete_op()
	{
		try 
		{
			$flag  	     = 0;
			$msg    	 = '';
			$params 	 = get_params();
			$tokens 	 = explode('/', $params['param_1']);
			$id 		 = base64_url_decode($tokens[0]); 
			
			check_salt($id, $tokens[1], $tokens[2]);
			
			CEIS_Model::beginTransaction();
			
			$audit_table	 = array(CEIS_MODEL::tbl_request_payments, CEIS_MODEL::tbl_request_payment_collections);
			$audit_schema	 = array(DB_CEIS, DB_CEIS);
			$payment_details = $this->crsoopm->get_order_of_payment_header($id);
			
			$encoded_rid     = base64_url_encode($payment_details['request_id']);
			
			$prev_detail[]   = array($payment_details);
			$prev_detail[]   = $this->crsoopm->get_order_of_payment_details($id);
			$audit_action    = array(AUDIT_DELETE, AUDIT_DELETE);
			
			$activity	= "Request order of payment has been deleted.";
			
			$this->crsoopm->delete_order_of_payment_details($id);
			$this->crsoopm->delete_order_of_payment($id);

			$curr_detail  = array(array(), array());
			
			$this->audit_trail->log_audit_trail(
					$activity,
					$this->module,
					$prev_detail,
					$curr_detail,
					$audit_action,
					$audit_table,
					$audit_schema
					);
				
			CEIS_Model::commit();
			
			$flag  = 1;
			$msg   = sprintf($this->lang->line('data_spec_deleted'), 'Order of Payment');
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
				
			$this->rlog_error($e);
			
			$msg = $this->lang->line('err_internal_server');
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();
				
			$msg = $this->rlog_error($e, TRUE);
		}
		
		echo json_encode( array('flag' => $flag, 'msg' => $msg, 'reload' => 'datatable', 'table_id' => 'table_order_of_payment', 'path' => PROJECT_CEIS.'/ceis_request_sub_order_of_payment/get_op_list/'.$encoded_rid) );
	}
	
	private function _validate(&$params)
	{
		if( ! ISSET($params['op_date']) || EMPTY($params['op_date'])):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Order of Payment Date'));
		endif;
		
		if( ! ISSET($params['amount']) || EMPTY($params['amount'])):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Amount'));
		endif;
		
		if( ! ISSET($params['collection']) || EMPTY($params['collection'])):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Collection'));
		endif;
		
		$collection_values = array_count_values($params['collection']);
		
		foreach($collection_values as $key => $value):
			if($value > 1) throw new Exception(sprintf($this->lang->line('err_duplicate_data_spec'), 'Collection'));
		endforeach;
		
		foreach($params['amount'] as $key => $value):
			if( ! is_valid_number($value)) throw new Exception(sprintf($this->lang->line('invalid_data'), 'Amount'));
		endforeach;
		
		$params['total_amount'] = array_sum($params['amount']);
		$params['user_id']		= $this->user_id;
		
		$tokens = explode('/', $params['request_id']);
		
		$params['request_id'] = base64_url_decode($tokens[0]);
		$params['salt'] 	  = $tokens[1];
		$params['token'] 	  = $tokens[2];
		
		check_salt($params['request_id'], $params['salt'], $params['token']);
		
		if( ! EMPTY($params['security'])):
			$tokens = explode('/', $params['security']);
		
			$rqst_payment_id = base64_url_decode($tokens[0]);
			
			check_salt($rqst_payment_id, $tokens[1], $tokens[2]);
			
			$params['rqst_payment_id'] = $rqst_payment_id;
		endif;
	}
}

/* End of file Ceis_dashboard.php */
/* Location: ./application/modules/ceis/controllers/Ceis_dashboard.php */