<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_contractors extends CEIS_Controller {
	
	private $module = MODULE_CEIS_CONTRACTORS;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('contractors_model', 'contractors');
		$this->load->library( 'Email_template' );
	}
	
	public function index()
	{	
		try{
			$data = $resources = $modal = array();

			$modal = array(
				'modal_contractor' 	=> array(
					'controller'	=> __CLASS__,
					'module'		=> PROJECT_CEIS
				)
			);
			
			$resources['load_css'] 		= array(CSS_DATATABLE);
			$resources['load_js'] 		= array(JS_DATATABLE);
			$resources['load_modal'] 	= $modal;
			$resources['datatable'] 	= array();
			$resources['datatable'][] 	= array('table_id' => 'contractor_table', 'path' => PROJECT_CEIS.'/ceis_contractors/get_contractor_list');
		}
		catch(Exception $e){
			
			$this->rlog_error($e);

			echo $e;
		}
		catch(PDOException $e){
			
			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		
		$this->template->load('contractors', $data, $resources);
	}
	
	public function get_contractor_list()
	{
		try{

			$params = get_params();
			$cnt = 0;
			
			// FIELDS TO BE SELECTED FROM TABLE
			$select_fields = array('a.contractor_id', 'a.contractor_name', 'a.contractor_address', 'a.sec_reg_no', 'a.sec_reg_date', 'a.contractor_category_id', 'a.license_no', 'a.license_date', 'a.bir_clearance_date', 'a.customs_clearance_date', 'a.court_clearance_date', 'a.tel_no', 'a.fax_no', 'a.email', 'a.website', 'a.rep_first_name', 'a.rep_mid_name', 'a.rep_last_name', 'a.rep_address', 'a.rep_contact_no', 'a.rep_email', 'a.created_by', 'a.created_date', 'a.modified_by', 'a.modified_date', 'b.contractor_category_name', "'Completed' as stat");

			// APPEARS ON TABLE, USED IN WHERE AND ORDER BY CONDITION
			$where_fields = array('contractor_name', 'contractor_category_name', 'sec_reg_no', 'bir_clearance_date', 'created_date', 'license_date', 'stat');
			
			$contractors 	= $this->contractors->get_contractor_list($select_fields, $where_fields, $params);
			$iTotal 		= $this->contractors->total_length(); // TOTAL COUNT OF RECORDS
			$iFilteredTotal = $this->contractors->filtered_length($select_fields, $where_fields, $params); // TOTAL COUNT OF RECORDS PER PAGE
		
			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
		
			foreach ($contractors as $data):

				// PRIMARY KEY
				$contractor_id 	= $data["contractor_id"];

				// CONSTRUCT SECURITY VARIABLES
				$hash_id		= $this->hash($contractor_id);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;

				// CONSTRUCT ACTION ICONS
				$action = "<div class='table-actions'>";
				//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
					$action .= '<a class="tooltipped" data-tooltip="View" data-position="bottom" data-delay="50" href="ceis_contractors/view_contractor/'.$url.'"><i class="flaticon-search95"></i>
	            </a>';
				$action .= '</div>';

				$row 	= array();
				$row[]	= $data['contractor_name'];
				$row[]	= $data['contractor_category_name'];
				$row[]	= $data['sec_reg_no'];
				$row[]	= $data['bir_clearance_date'];
				$row[]	= $data['created_date'];
				$row[]	= $data['license_date'];
				$row[]	= $data['stat'];
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
			endforeach;	
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		echo json_encode( $output );
	}

	public function get_progress_report_list($contractor_id,$contract_id)
	{
		try{
			
			$params = get_params();
			$cnt 	= 0;
			
			// START: GET THE CONTRACTOR INFORMATION USING HASH ID
			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $contractor_id;
			$info 			= $this->contractors->get_specific_contractor($where);
			// END: GET THE CONTRACTOR INFORMATION USING HASH ID

			$contractor_id = $info['contractor_id'];

			$aColumns = array('a.progress_report_id',
					'a.contract_id',
					'a.year',
					'a.quarter',
					'a.submitted_flag',
					'a.started_date',
					'a.orig_completion_date',
					'a.rev_completion_date',
					'a.official_completion_date',
					'a.contract_status_id',
					'a.created_by',
					'a.created_date',
					'a.modified_by',
					'a.modified_date',
					'd.contract_status_name'
					);

			$bColumns = array(
					'year',
					'quarter',
					'created_by',
					'created_date',
					'contract_status_name');

			$contractors 	= $this->contractors->get_progress_report_list($aColumns, $bColumns, $params, $contractor_id, $contract_id);
			$iTotal 		= $this->contractors->total_length("progress_report_id", CEIS_Model::tbl_progress_reports, $contractor_id, null, $contract_id);
			$iFilteredTotal = $this->contractors->filtered_length($aColumns, $bColumns, $params, "get_progress_report_list", $contractor_id, null, $contract_id);
		
			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
		
			foreach ($contractors as $data):

				$action 			= "";
				$progress_report_id = $data["progress_report_id"];
				$hash_id			= $this->hash($progress_report_id);
				$encoded_id 		= base64_url_encode($hash_id);
				$salt 				= gen_salt();
				$token 				= in_salt($encoded_id, $salt);			
				$url 				= $encoded_id."/".$salt."/".$token;
				
				$action = "<div class='table-actions'>";
				$action .= "<a id='menu_contracts_manpower' href='javascript:;' onclick=\"LoadTab.init('contracts_manpower','".$contract_id."','".$progress_report_id."')\" class='tooltipped' data-tooltip='View' data-position='bottom' data-delay='50'  ><i class='flaticon-search95' ></i></a>";

				$action .= '</div>';
				
				$row 	= array();
				$row[] 	= $data['year'];
				$row[] 	= $data['quarter'];
				$row[] 	= $data['created_by'];
				$row[] 	= $data['created_date'];
				$row[] 	= $data['contract_status_name'];
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}
	}

	public function get_construction_payment_list($contractor_id)
	{
		try{
			$params = get_params();
			$cnt 	= 0;
			
			// START: GET THE CONTRACTOR INFORMATION USING HASH ID
			$key 					= $this->get_hash_key('contractor_id');
			$where					= array();
			$where[$key]			= $contractor_id;
			$info 					= $this->contractors->get_specific_contractor($where);
			
			// END: GET THE CONTRACTOR INFORMATION USING HASH ID
			$contractor_id = $info['contractor_id'];

			$aColumns = array('a.contractor_payment_id',
					'a.contractor_id',
					'a.op_date',
					'a.op_amount',
					'a.or_number',
					'a.or_date',
					'a.or_amount',
					'a.created_by',
					'a.created_date',
					'a.modified_by',
					'a.modified_date',
					'b.contractor_name',
					'b.contractor_address',
					'b.sec_reg_no',
					'b.sec_reg_date',
					'b.contractor_category_id',
					'b.license_no',
					'b.license_date',
					'b.bir_clearance_date',
					'b.customs_clearance_date',
					'b.court_clearance_date',
					'b.tel_no',
					'b.fax_no',
					'b.email',
					'b.website',
					'b.rep_first_name',
					'b.rep_mid_name',
					'b.rep_last_name',
					'b.rep_address',
					'b.rep_contact_no',
					'b.rep_email',
					'b.created_by',
					'b.created_date');

			$bColumns = array(
					'op_date',
					'op_amount',
					'or_number',
					'or_date',
					'or_amount');
			
			$order_payment 	= $this->contractors->get_construction_payment_list($aColumns, $bColumns, $params, $contractor_id);
			$iTotal 		= $this->contractors->total_length("contractor_payment_id", CEIS_Model::tbl_contractor_payments, $contractor_id);
			$iFilteredTotal = $this->contractors->filtered_length($aColumns, $bColumns, $params,"get_construction_payment_list", $contractor_id);
		
			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
		
			foreach ($order_payment as $data):
			
				$contractor_payment_id 	= $data["contractor_payment_id"];
				$hash_id 				= $this->hash($contractor_payment_id);
				$encoded_id 			= base64_url_encode($hash_id);
				$salt 					= gen_salt();
				$token 					= in_salt($encoded_id, $salt);	
				$url 					= $encoded_id."/".$salt."/".$token;
				
				$action = "<div class='table-actions'>";
				$action .= "<a href='javascript:;' class='md-trigger view tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_contractor_order_payment' onclick=\"modal_contractor_order_payment_init('".$url."/".$contractor_payment_id."')\" ><i class='flaticon-search95' ></i></a>";
				$action .= '</div>';
				
				$row 	= array();
				$row[] 	= $data['op_date'];
				$row[] 	= number_format($data['op_amount'],2);
				$row[] 	= $data['or_number'];
				$row[] 	= $data['or_date'];
				$row[] 	= number_format($data['or_amount'],2);
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}
	}

	public function get_finance_contractor_statements_list($contractor_id)
	{
		try{

			$params = get_params();
			$cnt 	= 0;
			$modal 	= array();

			// START: GET THE CONTRACTOR INFORMATION USING HASH ID
			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $contractor_id;
			$info 			= $this->contractors->get_specific_contractor($where);
			// END: GET THE CONTRACTOR INFORMATION USING HASH ID

			$contractor_id = $info['contractor_id'];

			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array(
					/*'b.statement_id',*/
					'b.contractor_id',
					'b.contractor_name',
					'b.contractor_address',
					'b.sec_reg_no',
					'b.sec_reg_date',
					'b.contractor_category_id',
					'b.license_no',
					'b.license_date',
					'b.bir_clearance_date',
					'b.customs_clearance_date',
					'b.court_clearance_date',
					'b.tel_no',
					'b.fax_no',
					'b.email',
					'b.website',
					'b.rep_first_name',
					'b.rep_mid_name',
					'b.rep_last_name',
					'b.rep_address',
					'b.rep_contact_no',
					'b.rep_email',
					'b.created_by',
					'b.created_date',
					'a.statement_date',
					'a.stmt_type',
					'a.total_current_assets',
					'a.total_non_current_assets',
					'a.total_current_liabilities',
					'a.long_term_liabilities',
					'a.networth',
					'a.gross_sales',
					'a.net_profit',
					'a.common_shares',
					'a.par_value',
					'a.created_by',
					'a.created_date'
					);

			// APPEARS ON TABLE
			$bColumns = array('statement_date',
					'total_non_current_assets',
					'total_current_liabilities',
					'networth',
					'gross_sales'
					);
		
			$statements 	= $this->contractors->get_finance_contractor_statements_list($aColumns, $bColumns, $params, $contractor_id);
			$iTotal 		= $this->contractors->total_length("contractor_id", CEIS_Model::tbl_contractor_statement, $contractor_id);
			$iFilteredTotal = $this->contractors->filtered_length($aColumns, $bColumns, $params, "get_finance_contractor_statements_list", $contractor_id);
		
			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);

			foreach ($statements as $data):
			
				$statement_id		= $data["statement_id"];
				$hash_id 			= $this->hash($statement_id);
				$encoded_id 		= base64_url_encode($hash_id);
				$salt 				= gen_salt();			
				$token 				= in_salt($encoded_id, $salt);			
				$url 				= $encoded_id."/".$salt."/".$token;
				$statement_date 	= $data['statement_date'];
				
				$action = "<div class='table-actions'>";

				/*$action .= "<a id='menu_finance_view_statement' href='javascript:;' onclick=\"LoadTab.init('finance_view_statement', '".$contractor_id."')\" class='edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50'  ><i class='flaticon-search95' ></i></a>";*/

				$action .= "<a href='javascript:;' class='md-trigger view tooltipped' data-tooltip='View'  data-position='bottom' data-delay='50' data-modal='modal_finance_statement' onclick=\"modal_finance_statement_init('".$url."/".$statement_date."')\" ><i class='flaticon-search95' ></i></a>";
			
				$action .= '</div>';

				$row 	= array();
				$row[]  = $data['statement_date'];
				$row[]  = number_format($data['total_non_current_assets'],2);
				$row[]  = number_format($data['total_current_liabilities'],2);
				$row[]  = number_format($data['networth'],2);
				$row[]  = number_format($data['gross_sales'],2);
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}

		catch(PDOException $e){
			echo $e;
			/*$this->rlog_error($e);

			echo $this->get_user_message($e);*/
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

	}

	public function get_staffing_director_list($staff_type,$contractor_id)
	{
		try{

			$params = get_params();
			$cnt 	= 0;
			
			// START: GET THE CONTRACTOR INFORMATION USING HASH ID
			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $contractor_id;
			$info 			= $this->contractors->get_specific_contractor($where);
			// END: GET THE CONTRACTOR INFORMATION USING HASH ID

			$contractor_id = $info['contractor_id'];

			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array('a.contr_staff_id',
					'a.contractor_id',
					'a.staff_type',
					'a.first_name',
					'a.middle_name',
					'a.last_name',
					'a.address',
					'a.birth_date',
					'a.birth_place',
					'a.educational_attainment',
					'a.school_attended',
					'a.year_graduated',
					'a.created_by',
					'a.created_date',
					'CONCAT(a.first_name, \' \', a.middle_name, \' \', a.last_name) as staff_name',/*
					"( PERIOD_DIFF(DATE_FORMAT(NOW(),'%%Y%%m'), DATE_FORMAT(b.issued_date,'%%Y%%m')) / 12 ) as no_years",*/
					/*"'Undefined' as no_years",*/
					/*"'Resigned' as status",*/
					'b.contractor_name as contractor_name',
					'c.company_name as company_name',
					'c.start_date as start_date',
					'c.end_date as end_date',
					"IF (b.contractor_name = c.company_name, 'Connected', 'Resigned') as status",
					"PERIOD_DIFF(DATE_FORMAT(c.end_date,'%%Y%%m'), DATE_FORMAT(c.start_date,'%%Y%%m') ) / 12 as no_years"
					/*,
					'b.license_no',
					'b.contr_staff_id',
					'b.profession',
					'b.issued_date',
					'b.expiry_date'*/
					);

			// APPEARS ON TABLE
			$bColumns = array('staff_name',
					'no_years',
					'status'
					);
		
			$staffing 		= $this->contractors->get_staffing_director_list($aColumns, $bColumns, $params, $staff_type, $contractor_id);
			$iTotal 		= $this->contractors->total_length("contr_staff_id", CEIS_Model::tbl_contractor_staff, $contractor_id, $staff_type);
			$iFilteredTotal = $this->contractors->filtered_length($aColumns, $bColumns, $params,"get_staffing_director_list", $contractor_id,$staff_type);
		
			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
			

			foreach ($staffing as $data):
				
				$contr_staff_id	= $data["contr_staff_id"];
				$hash_id 		= $this->hash($contr_staff_id);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$action = "<div class='table-actions'>";
	          	
	          	$action .= "<a id='menu_staffing_director' href='javascript:;' onclick=\"LoadTab.init('staffing_director','0' , '0', '".$contr_staff_id."')\" class='edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50'  ><i class='flaticon-search95' ></i></a>";
			
				$action .= '</div>';

				$row 	= array();
				$row[] 	= $data['staff_name'];
				$row[] 	= $data['no_years'];
				$row[] 	= $data['status'];
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

	}

	public function get_manpower_schedule_list()
	{
		try{

			$params = get_params();
			$cnt = 0;
		
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array('a.contractor_id',
					'a.contractor_date',
					'a.contractor_type_id',
					'a.reference_no',
					'a.reference_name',
					'a.contractor_status_id',
					'a.pocb_registration_no',
					'a.equip_serial_no',
					'a.created_by',
					'a.created_date',
					'a.modified_by',
					'a.modified_date');

			// APPEARS ON TABLE
			$bColumns = array('contractor_id',
					'contractor_date',
					'contractor_type_id',
					'contractor_id',
					'contractor_date'
					);
		
			$contractors 	= $this->contractors->get_contractor_list($aColumns, $bColumns, $params);
			$iTotal 		= $this->contractors->total_length();
			$iFilteredTotal = $this->contractors->filtered_length($aColumns, $bColumns, $params);
		
			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
		
			foreach ($contractors as $data):
			
				$contractor_id	= $data["contractor_id"];
				$hash_id 		= $this->hash($contractor_id);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$action = "<div class='table-actions'>";

	          	/*$action .= "<a href='' class='edit tooltipped' data-tooltip='View' ><i class='flaticon-search95' ></i></a>";*/

	          	//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
					$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_manpower_schedule' onclick=\"modal_manpower_schedule_init()\" ><i class='flaticon-search95' ></i></a>";

	          	/*$action .= "<a href='../submenu_staffing_director' class='md-trigger edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_director' onclick=\"modal_director_init()\" ><i class='flaticon-search95' ></i></a>";*/
			
				$action .= '</div>';

				$row 	= array();
				$row[] 	= $data['contractor_id'];
				$row[] 	= $data['contractor_date'];
				$row[] 	= $data['contractor_type_id'];
				$row[] 	= $data['contractor_id'];
				$row[] 	= $data['contractor_date'];
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

	}

	public function get_staffing_experience_list($contractor_id, $contr_staff_id)
	{
		try{

			$params = get_params();
			$cnt = 0;
			
			// START: GET THE CONTRACTOR INFORMATION USING HASH ID
			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $contractor_id;
			$info 			= $this->contractors->get_specific_contractor($where);
			// END: GET THE CONTRACTOR INFORMATION USING HASH ID

			$contractor_id = $info['contractor_id'];
		
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array('a.contr_staff_exp_id',
					'a.contr_staff_id',
					'a.present_flag',
					'CONCAT(a.start_date, \' to \', a.end_date) as inclusive_date',
					'a.start_date',
					'a.end_date',
					'a.company_name as company_name',
					'a.company_address',
					'a.position',
					'a.job_description',
					'a.stockholder_flag',
					'a.paid_in_amount',
					'a.created_by',
					'a.created_date',
					'b.first_name',
					'CONCAT(b.first_name, \' \', b.middle_name, \' \', b.last_name, \' \', b.address) as employee_name',
					'b.address',
					/*'"Connected" as status'*/
					'c.contractor_name as contractor_name',
					"IF (c.contractor_name = a.company_name, 'Connected', 'Resigned') as status"

					);

			// APPEARS ON TABLE
			$bColumns = array('inclusive_date',
					'employee_name',
					'position',
					'job_description',
					'status'
					);
		
			$staff_experience 	= $this->contractors->get_staffing_experience_list($aColumns, $bColumns, $params, $contractor_id, $contr_staff_id);
			$iTotal 			= $this->contractors->total_length("contr_staff_exp_id", CEIS_Model::tbl_contractor_staff_experiences, $contractor_id, null, null, null, $contr_staff_id);
			$iFilteredTotal 	= $this->contractors->filtered_length($aColumns, $bColumns, $params, 'get_staffing_experience_list', $contractor_id, null, null, $contr_staff_id );
		
			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
		
			foreach ($staff_experience as $data):
			
				$contr_staff_exp_id	= $data["contr_staff_exp_id"];
				$hash_id 			= $this->hash($contr_staff_exp_id);
				$encoded_id 		= base64_url_encode($hash_id);
				$salt 				= gen_salt();			
				$token 				= in_salt($encoded_id, $salt);			
				$url 				= $encoded_id."/".$salt."/".$token;
				
				$action = "<div class='table-actions'>";

				$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_contractor_staffing_experience' onclick=\"modal_contractor_staffing_experience_init('".$url."/".$contr_staff_exp_id."')\" ><i class='flaticon-search95' ></i></a>";
			
				$action .= '</div>';
				$row 	= array();
				$row[] 	= $data['inclusive_date'];
				$row[] 	= $data['employee_name'];
				$row[] 	= $data['position'];
				$row[] 	= $data['job_description'];
				$row[] 	= $data['status'];
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}
	}

	public function get_staffing_project_list($contractor_id, $contr_staff_id)
	{
		try{

			$params = get_params();
			$cnt = 0;
		
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array(
					'a.contr_staff_proj_id',
					'a.contr_staff_id',
					'a.project_title',
					'a.project_location',
					'a.project_cost',
					'a.owner',
					'a.positions',
					'a.project_type_code',
					'a.start_date',
					'a.end_date',
					'a.created_by',
					'a.created_date',
					'a.modified_by',
					'a.modified_date');

			// APPEARS ON TABLE
			$bColumns = array(
					'project_title',
					'project_location',
					'owner',
					'project_cost'
					);
		
			$projects 	= $this->contractors->get_staffing_project_list($aColumns, $bColumns, $params, $contractor_id, $contr_staff_id);
			$iTotal 		= $this->contractors->total_length("contr_staff_proj_id", CEIS_Model::tbl_contractor_staff_projects, $contractor_id, null, null, null, $contr_staff_id);
			$iFilteredTotal = $this->contractors->filtered_length($aColumns, $bColumns, $params, "get_staffing_project_list", $contractor_id, null, null, $contr_staff_id);
		
			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
		
			foreach ($projects as $data):
			
				$contr_staff_proj_id	= $data["contr_staff_proj_id"];

				$hash_id 				= $this->hash($contr_staff_proj_id);
				$encoded_id 			= base64_url_encode($hash_id);
				$salt 					= gen_salt();			
				$token 					= in_salt($encoded_id, $salt);			
				$url 					= $encoded_id."/".$salt."/".$token;
				
				$action = "<div class='table-actions'>";
				$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_contractor_staffing_project' onclick=\"modal_contractor_staffing_project_init('".$url."/".$contr_staff_proj_id."')\" ><i class='flaticon-search95' ></i></a>";
				$action .= '</div>';

				$row 	= array();
				$row[] 	= $data['project_title'];
				$row[] 	= $data['project_location'];
				$row[] 	= $data['owner'];
				$row[] 	= number_format($data['project_cost'],2);
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}

		catch(PDOException $e){
			
			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

	}

	public function get_contract_project_list($contractor_id)
	{
		try{

			$params = get_params();
			$cnt = 0;
			
			// START: GET THE CONTRACTOR INFORMATION USING HASH ID
			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $contractor_id;
			$info 			= $this->contractors->get_specific_contractor($where);
			// END: GET THE CONTRACTOR INFORMATION USING HASH ID

			$contractor_id = $info['contractor_id'];
			
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array(
				'c.contractor_id',
				'c.contractor_name',
				'c.contractor_address',
				'c.sec_reg_no',
				'c.sec_reg_date',
				'c.contractor_category_id',
				'c.license_no',
				'c.license_date',
				'c.bir_clearance_date',
				'c.customs_clearance_date',
				'c.court_clearance_date',
				'c.tel_no',
				'c.fax_no',
				'c.email',
				'c.website',
				'c.rep_first_name',
				'c.rep_mid_name',
				'c.rep_last_name',
				'c.rep_address',
				'c.rep_contact_no',
				'c.rep_email',
				'c.created_by',
				'c.created_date',
				'b.contract_id',
				'b.contractor_id',
				'b.contract_type',
				'b.base',
				"IF (b.with_authorization_flag = 1, 'Yes', 'No') as with_flag",
				'b.project_title',
				'b.project_location',
				'b.province',
				'b.country_code',
				'b.contract_status_id',
				'b.project_cost',
				'b.contract_cost',
				'b.owner',
				'b.contract_duration',
				'b.started_date',
				'b.completed_date',
				'b.created_by',
				'b.created_date',
				'a.contract_id',
				'a.project_type_code',
				'a.involvement_code',
				'a.scheduled_percent',
				'a.actual_percent',
				'd.contract_status_id',
				'd.contract_status_name'
				);

			// APPEARS ON TABLE
			$bColumns = array('project_title',
					'project_location',
					'owner',
					'contract_cost',
					'contract_status_name',
					'with_flag',
					'contract_duration'
					);
		
			$contract_projects 	= $this->contractors->get_contract_project_list($aColumns, $bColumns, $params, $contractor_id );
			$iTotal 			= $this->contractors->total_length("contract_id", CEIS_Model::tbl_contractor_projects, $contractor_id);
			
			$iFilteredTotal 	= $this->contractors->filtered_length($aColumns, $bColumns, $params, "get_contract_project_list", $contractor_id);

			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
			
			foreach ($contract_projects as $data):
				
				$contract_id	= $data["contract_id"];
				$hash_id 		= $this->hash($contract_id);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;

				$action = "<div class='table-actions'>";
				$action .= "<a id='menu_contracts_view' href='javascript:;' onclick=\"LoadTab.init('contracts_view','".$contract_id."')\" class='edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50'  ><i class='flaticon-search95' ></i></a>";
				$action .= '</div>';

				$row   = array();
				$row[] = $data['project_title'];
				$row[] = $data['project_location'];
				$row[] = $data['owner'];
				$row[] = number_format($data['contract_cost'],2);
				$row[] = $data['contract_status_name'];
				$row[] = $data['with_flag'];
				$row[] = $data['contract_duration'];
				$row[] = $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

	}

	public function remind_contractor($hash_contractor_id){

		try{

				// START: GET THE CONTRACTOR INFORMATION USING HASH ID
				$key 			= $this->get_hash_key('contractor_id');
				$where			= array();
				$where[$key]	= $hash_contractor_id;
				$info 			= $this->contractors->get_specific_contractor($where);
				// END: GET THE CONTRACTOR INFORMATION USING HASH ID

				$rep_email 					= $info['rep_email'];
				
				//start of sending email
				$email_data['from_email'] 	= "rms@asiagate.com";
				$email_data['from_name']	= "CIAP webmaster";
				$contractor_email 			= $emails[$i];
				$email_data['to_email']		= array($rep_email);
				$email_data['subject']		= "Reminders";
				$template 					= "../emails/remind_contractor";
				
				$template_data 				= array(
					'name'		=> 'Christian'
				);

				$this->email_template->send_email_template($email_data, $template, $template_data);

				$email_errors 	= $this->email_template->get_email_errors();
				
				if( !EMPTY( $email_errors ) )
				{
					throw new Exception( var_export( $email_errors, TRUE ) );
					
				}
				//end of sending email

				$info = array(
					'first_name'	=> 'kokin',
					'address'		=> 'ecija'
				);

				echo json_encode($info);

			}

			catch(PDOException $e){

				$this->rlog_error($e);

				echo $this->get_user_message($e);
			}

			catch(Exception $e){

				$this->rlog_error($e);

				echo $e->getMessage();
			}

	}

	public function change_status($hash_contractor_id){

		try{
				

				// START: GET THE CONTRACTOR INFORMATION USING HASH ID
				$key 			= $this->get_hash_key('contractor_id');
				$where			= array();
				$where[$key]	= $hash_contractor_id;
				$info 			= $this->contractors->get_specific_contractor($where);
				// END: GET THE CONTRACTOR INFORMATION USING HASH ID

				$rep_email 					= $info['rep_email'];

				//start of sending email
				$email_data['from_email'] 	= "rms@asiagate.com";
				$email_data['from_name']	= "CIAP webmaster";
				$contractor_email 			= $emails[$i];
				$email_data['to_email']		= array($rep_email);
				$email_data['subject']		= "Change Status Request";
				$template 					= "../emails/change_status";
				
				$template_data 				= array(
					'name'		=> 'Christian'
				);

				$this->email_template->send_email_template($email_data, $template, $template_data);

				$email_errors 	= $this->email_template->get_email_errors();
				
				if( !EMPTY( $email_errors ) )
				{
					throw new Exception( var_export( $email_errors, TRUE ) );
					
				}
				//end of sending email

				$info = array(
					'first_name'	=> 'kokin',
					'address'		=> 'ecija'
				);

				echo json_encode($info);

			}

			catch(PDOException $e){

				$this->rlog_error($e);

				echo $this->get_user_message($e);
			}

			catch(Exception $e){

				$this->rlog_error($e);

				echo $e->getMessage();
			}

	}

	public function get_contract_service_list($contractor_id)
	{
		try{

			$params = get_params();
			$cnt = 0;
			
			// START: GET THE CONTRACTOR INFORMATION USING HASH ID
			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $contractor_id;
			$info 			= $this->contractors->get_specific_contractor($where);
			// END: GET THE CONTRACTOR INFORMATION USING HASH ID

			$contractor_id = $info['contractor_id'];

			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array(
				'c.contractor_id',
				'c.contractor_name',
				'c.contractor_address',
				'c.sec_reg_no',
				'c.sec_reg_date',
				'c.contractor_category_id',
				'c.license_no',
				'c.license_date',
				'c.bir_clearance_date',
				'c.customs_clearance_date',
				'c.court_clearance_date',
				'c.tel_no',
				'c.fax_no',
				'c.email',
				'c.website',
				'c.rep_first_name',
				'c.rep_mid_name',
				'c.rep_last_name',
				'c.rep_address',
				'c.rep_contact_no',
				'c.rep_email',
				'c.created_by',
				'c.created_date',
				'b.contract_id',
				'b.contractor_id',
				'b.contract_type',
				'b.base',
				"IF (b.with_authorization_flag = 1, 'Yes', 'No') as with_flag",
				'b.project_title',
				'b.project_location',
				'b.province',
				'b.country_code',
				'b.contract_status_id',
				'b.project_cost',
				'b.contract_cost',
				'b.owner',
				'b.contract_duration',
				'b.started_date',
				'b.completed_date',
				'b.created_by',
				'b.created_date',
				'a.contract_id',
				'a.foreign_principal_name',
				'a.nationality_code',
				'a.required_manpower',
				'a.mobilized_manpower',
				'a.onsite_manpower',
				'a.fees',
				'a.workers_salaries',
				'd.contract_status_id',
				'd.contract_status_name'
				);

			// APPEARS ON TABLE
			$bColumns = array('project_title',
					'project_location',
					'owner',
					'contract_cost',
					'contract_status_id',
					'with_flag',
					'contract_duration'
					);

			$contract_services 	= $this->contractors->get_contract_service_list($aColumns, $bColumns, $params, $contractor_id);
			$iTotal 			= $this->contractors->total_length("contract_id", CEIS_Model::tbl_contractor_services, $contractor_id);
			$iFilteredTotal 	= $this->contractors->filtered_length($aColumns, $bColumns, $params,"get_contract_service_list", $contractor_id);
		
			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
		
			foreach ($contract_services as $data):
			
				$contract_id	= $data["contract_id"];
				$hash_id 		= $this->hash($contract_id);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$action = "<div class='table-actions'>";
				
				$action .= "<a id='menu_contracts_view' href='javascript:;' onclick=\"LoadTab.init('contracts_view','".$contract_id."')\" class='edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50'  ><i class='flaticon-search95' ></i></a>";
				$action .= '</div>';

				$row   = array();
				$row[] = $data['project_title'];
				$row[] = $data['project_location'];
				$row[] = $data['owner'];
				$row[] = number_format($data['contract_cost'],2);
				$row[] = $data['contract_status_name'];
				$row[] = $data['with_flag'];
				$row[] = $data['contract_duration'];
				$row[] = $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

	}

	public function get_contract_mobilization_list($contractor_id, $contract_id)
	{
		try{

			$params = get_params();
			$cnt = 0;
			
			// START: GET THE CONTRACTOR INFORMATION USING HASH ID
			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $contractor_id;
			$info 			= $this->contractors->get_specific_contractor($where);
			// END: GET THE CONTRACTOR INFORMATION USING HASH ID

			$contractor_id = $info['contractor_id'];

			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array(
				'a.report_manpower_id',
				'a.progress_report_id',
				'a.contr_staff_id',
				'a.position_id',
				'a.salary_in_usd',
				'a.start_duration',
				'a.end_duration',
				'DATEDIFF(a.end_duration,a.start_duration) as as_diff',
				'a.mobilized_date',
				'a.mobilized_remarks',
				'a.demobilized_date',
				'a.demobilized_remarks',
				'a.created_by',
				'a.created_date',
				'a.modified_by',
				'a.modified_date',
				'b.contract_id',
				'b.year',
				'b.quarter',
				'b.submitted_flag',
				'b.started_date',
				'b.orig_completion_date',
				'b.rev_completion_date',
				'b.official_completion_date',
				'b.contract_status_id',
				'b.created_by',
				'b.created_date',
				'b.modified_by',
				'b.modified_date',
				'b.started_date',
				'c.contract_id',
				'c.contractor_id',
				'c.contract_type',
				'c.base',
				'c.with_authorization_flag',
				'c.project_title',
				'c.project_location',
				'c.province',
				'c.country_code',
				'c.contract_status_id',
				'c.project_cost',
				'c.contract_cost',
				'c.owner',
				'c.contract_duration',
				'c.started_date',
				'c.completed_date',
				'c.created_by',
				'c.created_date',
				'c.modified_by',
				'c.modified_date',
				'd.contractor_id',
				'd.contractor_name',
				'd.contractor_address',
				'd.sec_reg_no',
				'd.sec_reg_date',
				'd.contractor_category_id',
				'd.license_no',
				'd.license_date',
				'd.bir_clearance_date',
				'd.customs_clearance_date',
				'd.court_clearance_date',
				'd.tel_no',
				'd.fax_no',
				'd.email',
				'd.website',
				'd.rep_first_name',
				'd.rep_mid_name',
				'd.rep_last_name',
				'd.rep_address',
				'd.rep_contact_no',
				'd.rep_email',
				'd.created_by',
				'd.created_date',
				'd.modified_by',
				'd.modified_date',
				'e.first_name',
				'e.middle_name',
				'e.last_name',
				'f.position_name'
				);

			/*$date1=date_create($aColumns['start_duration']);
			$date2=date_create($aColumns['end_duration']);
			$diff=date_diff($date1,$date2);
			$diff = $diff->format("%R%a days");*/
			
			// APPEARS ON TABLE
			$bColumns = array('first_name',
					'position_name',
					'salary_in_usd',
					'as_diff',
					'mobilized_date',
					'demobilized_date'
					);

			$mobilization 	= $this->contractors->get_contract_mobilization_list($aColumns, $bColumns, $params, $contractor_id, $contract_id);
			$iTotal 		= $this->contractors->total_length("progress_report_id", CEIS_Model::tbl_progress_report_manpower, $contractor_id, null, $contract_id, null, null);
			$iFilteredTotal = $this->contractors->filtered_length($aColumns, $bColumns, $params,"get_contract_mobilization_list", $contractor_id, null, $contract_id, null);
		
			$output	= array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
		
			foreach ($mobilization as $data):
			
				$report_manpower_id	= $data["report_manpower_id"];
				$hash_id 			= $this->hash($report_manpower_id);
				$encoded_id 		= base64_url_encode($hash_id);
				$salt 				= gen_salt();			
				$token 				= in_salt($encoded_id, $salt);			
				$url 				= $encoded_id."/".$salt."/".$token;
				
				$action = "<div class='table-actions'>";
				
				$action .= "<a href='javascript:;' class='md-trigger view tooltipped' data-tooltip='View'  data-position='bottom' data-delay='50' data-modal='modal_contract_mobilization' onclick=\"modal_contract_mobilization_init('".$url."/".$report_manpower_id."')\" ><i class='flaticon-search95' ></i></a>";

				$action .= '</div>';

				$row 	= array();
				$row[] 	= $data['last_name'].', '.$data['first_name'].' '.$data['middle_name'];
				$row[] 	= $data['position_name'];
				$row[] 	= $data['salary_in_usd'];
				$row[] 	= $data['as_diff'];
				$row[] 	= $data['mobilized_date'];
				$row[] 	= $data['demobilized_date'];
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

	}

	public function get_contract_foreign_remittance_list($contractor_id, $contract_id)
	{
		try{

			$params = get_params();
			$cnt = 0;
			
			// START: GET THE CONTRACTOR INFORMATION USING HASH ID
			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $contractor_id;
			$info 			= $this->contractors->get_specific_contractor($where);
			// END: GET THE CONTRACTOR INFORMATION USING HASH ID

			$contractor_id = $info['contractor_id'];
		
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array(
				'a.report_remitt_id',
				'a.progress_report_id',
				'a.remitt_date',
				'a.remitt_amount',
				'a.country',
				'a.receiving_bank',
				'a.created_by',
				'a.created_date',
				'a.modified_by',
				'a.modified_date',
				'b.contract_id',
				'b.year',
				'b.quarter',
				'b.submitted_flag',
				'b.started_date',
				'b.orig_completion_date',
				'b.rev_completion_date',
				'b.official_completion_date',
				'b.contract_status_id',
				'b.created_by',
				'b.created_date',
				'b.modified_by',
				'b.modified_date',
				'b.started_date',
				'c.contract_id',
				'c.contractor_id',
				'c.contract_type',
				'c.base',
				'c.with_authorization_flag',
				'c.project_title',
				'c.project_location',
				'c.province',
				'c.country_code',
				'c.contract_status_id',
				'c.project_cost',
				'c.contract_cost',
				'c.owner',
				'c.contract_duration',
				'c.started_date',
				'c.completed_date',
				'c.created_by',
				'c.created_date',
				'c.modified_by',
				'c.modified_date',
				'd.contractor_id',
				'd.contractor_name',
				'd.contractor_address',
				'd.sec_reg_no',
				'd.sec_reg_date',
				'd.contractor_category_id',
				'd.license_no',
				'd.license_date',
				'd.bir_clearance_date',
				'd.customs_clearance_date',
				'd.court_clearance_date',
				'd.tel_no',
				'd.fax_no',
				'd.email',
				'd.website',
				'd.rep_first_name',
				'd.rep_mid_name',
				'd.rep_last_name',
				'd.rep_address',
				'd.rep_contact_no',
				'd.rep_email',
				'd.created_by',
				'd.created_date',
				'd.modified_by',
				'd.modified_date',
				'e.country_name'
				);

			// APPEARS ON TABLE
			$bColumns = array('remitt_date',
					'remitt_amount',
					'country_name',
					'receiving_bank'
					);

			$remittances 	= $this->contractors->get_contract_remittance_list($aColumns, $bColumns, $params, $contractor_id, $contract_id);
			$iTotal 		= $this->contractors->total_length("progress_report_id", CEIS_Model::tbl_progress_report_remittances, $contractor_id, null, $contract_id, null);
			$iFilteredTotal = $this->contractors->filtered_length($aColumns, $bColumns, $params,"get_contract_remittance_list", $contractor_id , null, $contract_id);
		
			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
		
			foreach ($remittances as $data):
			
				$report_remitt_id	= $data["report_remitt_id"];
				$hash_id 			= $this->hash($report_remitt_id);
				$encoded_id 		= base64_url_encode($hash_id);
				$salt 				= gen_salt();			
				$token 				= in_salt($encoded_id, $salt);			
				$url 				= $encoded_id."/".$salt."/".$token;
				
				$action = "<div class='table-actions'>";

           		/*$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_project' onclick=\"modal_project_init('".$url."')\" ><i class='flaticon-search95' ></i></a>";*/
				
				$action .= "<a href='javascript:;' class='md-trigger view tooltipped' data-tooltip='View'  data-position='bottom' data-delay='50' data-modal='modal_contract_foreign_remittance' onclick=\"modal_contract_foreign_remittance_init('".$url."/".$report_remitt_id."')\" ><i class='flaticon-search95' ></i></a>";

				$action .= '</div>';
				
				$row 	= array();
				$row[] 	= $data['remitt_date'];
				$row[] 	= number_format($data['remitt_amount'],2);
				$row[] 	= $data['country_name'];
				$row[] 	= $data['receiving_bank'];
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

	}

	public function get_contract_manpower_list()
	{
		try{

			$params = get_params();
			$cnt 	= 0;
		
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array('a.contractor_id',
					'a.contractor_date',
					'a.contractor_type_id',
					'a.reference_no',
					'a.reference_name',
					'a.contractor_status_id',
					'a.pocb_registration_no',
					'a.equip_serial_no',
					'a.created_by',
					'a.created_date',
					'a.modified_by',
					'a.modified_date');

			// APPEARS ON TABLE
			$bColumns = array('contractor_id',
					'contractor_date',
					'contractor_type_id',
					'reference_no',
					'reference_name'
					);
		
			$contractors 	= $this->contractors->get_contractor_list($aColumns, $bColumns, $params);
			$iTotal 		= $this->contractors->total_length();
			$iFilteredTotal = $this->contractors->filtered_length($aColumns, $bColumns, $params);
		
			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
		
			foreach ($contractors as $data):
			
				$contractor_id	= $data["contractor_id"];
				$hash_id 		= $this->hash($contractor_id);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$action = "<div class='table-actions'>";

				//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
					// $action .= '<a href="ceis_contractors/submenu_contractor"><i class="flaticon-search95"></i>
     //        </a>';	
            // $action .='<a href="" class="md-trigger" data-opportunity="button" data-modal="modal_opportunity"></a>';
           		$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_manpower' onclick=\"modal_manpower_init()\" ><i class='flaticon-search95' ></i></a>";	
			
				$action .= '</div>';

				$row 	= array();
				$row[] 	= $data['contractor_id'];
				$row[] 	= $data['contractor_date'];
				$row[] 	= $data['contractor_type_id'];
				$row[] 	= $data['reference_no'];
				$row[] 	= $data['reference_name'];
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

	}

	public function get_construction_equipment_list($contractor_id, $serial_no=NULL)
	{
		try{

			$params = get_params();
			$cnt = 0;
			
			// START: GET THE CONTRACTOR INFORMATION USING HASH ID
			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $contractor_id;
			$info 			= $this->contractors->get_specific_contractor($where);
			// END: GET THE CONTRACTOR INFORMATION USING HASH ID

			$contractor_id = $info['contractor_id'];
		
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array(
							"a.serial_no",
							"a.registrant_name", 
							"a.registrant_address", 
							/*"a.equipment_type", */
							"a.capacity", 
							"a.brand",
							"a.model",
							"a.acquired_date",
							"a.acquisition_cost",
							"a.present_condition",
							"a.location",
							"a.motor_no",
							"a.color",
							"a.year",
							"a.flywheel",
							"a.acquired_from",
							"a.ownership_type",
							"a.active_flag",
							"a.created_by",
							"a.created_date",
							"a.modified_by",
							"a.modified_date");

			// APPEARS ON TABLE
			$bColumns = array(
							"serial_no", 
							"capacity", 
							"year",
							"present_condition", 
							"acquisition_cost");
		
			$equipment 		= $this->contractors->get_construction_equipment_list($aColumns, $bColumns, $params, $contractor_id);
			$iTotal 		= $this->contractors->total_length("serial_no", CEIS_Model::tbl_equipment, $contractor_id);
			$iFilteredTotal = $this->contractors->filtered_length($aColumns, $bColumns, $params,"get_construction_equipment_list", $contractor_id);
		
			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
		
			foreach ($equipment  as $data):
			
				$serial_no		= $data["serial_no"];
				$hash_id 		= $this->hash($serial_no);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$action = "<div class='table-actions'>";

				//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
					// $action .= '<a href="ceis_contractors/submenu_contractor"><i class="flaticon-search95"></i></a>';	
            		// $action .='<a href="" class="md-trigger" data-opportunity="button" data-modal="modal_opportunity"></a>';
           		$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_construction_equipment' onclick=\"modal_construction_equipment_init('".$url."/".$serial_no."')\" ><i class='flaticon-search95' ></i></a>";
				
				$action .= '</div>';

				$row 	= array();
				$row[] 	= $data['serial_no'];
				$row[] 	= $data['capacity'];
				$row[] 	= $data['year'];
				$row[] 	= $data['present_condition'];
				$row[] 	= number_format($data['acquisition_cost'],2);
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

	}

	public function _construct_view_statement($contractor_id)
	{
		try 
		{
			$view = array();
			$statement_details = $this->contractors->get_contractor_statement($contractor_id);
			
			if( ! EMPTY($statement_details))
			{
				$thead = '<tr>';
				$tbody = array();

				foreach($statement_details as $sd):
					$thead 	  .= '<th class="right-align">'.date('M d,Y', strtotime($sd['statement_date'])).'</th>';
					$tbody[0] .= '<td class="right-align">'.$sd['total_current_assets'].'</td>';
					$tbody[1] .= '<td class="right-align">'.$sd['total_non_current_assets'].'</td>';
					$tbody[2] .= '<td class="right-align">'.$sd['total_current_liabilities'].'</td>';
					$tbody[3] .= '<td class="right-align">'.$sd['long_term_liabilities'].'</td>';
					$tbody[4] .= '<td class="right-align">'.$sd['networth'].'</td>';
					$tbody[5] .= '<td class="right-align">'.$sd['gross_sales'].'</td>';
					$tbody[6] .= '<td class="right-align">'.$sd['net_profit'].'</td>';
					$tbody[7] .= '<td class="right-align">'.$sd['common_shares'].'</td>';
					$tbody[8] .= '<td class="right-align">'.$sd['par_value'].'</td>';
				endforeach;
				
				$tbody = '<tr>'.rtrim(implode('</tr><tr>', $tbody), '<tr>');
				$view = array('thead' => $thead, 'tbody' => $tbody);
			}
			
			return $view;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function modal($encoded_id, $salt, $token)
	{
		try{
			$data 		= array();
			$resources 	= array();

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('contractor_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->contractors->get_specific_contractor($where);

				/*if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_contractor'));	*/

				$data['contractor_id'] 	= $info['contractor_id'];

				// CREATE NEW SECURITY VARIABLES
				$contractor_id	= $info['contractor_id'];
				$hash_id 		= $this->hash($contractor_id);			

			}

			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/contractors',$data);
	}
	
	public function modal_statement($encoded_id, $salt, $token, $year)
	{
		try{
			$data 		= array();
			$resources 	= array();
			$params 	= get_params();

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);	
			
			$hash_id 		= base64_url_decode($encoded_id);

			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $hash_id;

			$info 						= $this->contractors->get_specific_contractor($where);
			$contractor_id 				= $info['contractor_id'];
			$contractor_statement_info 	= $this->contractors->get_specific_statement($contractor_id);

			/*if(EMPTY($info))
				throw new Exception($this->lang->line('err_invalid_contractor'));*/	

			$data['contractor_id'] 						= $info['contractor_id'];
			$data['statement_date'] 					= $info['statement_date'];
			$data['total_current_assets'] 				= $info['total_current_assets'];
			$data['total_non_current_assets'] 			= $info['total_non_current_assets'];
			$data['total_current_liabilities'] 			= $info['total_current_liabilities'];
			$data['long_term_liabilities'] 				= $info['long_term_liabilities'];
			$data['networth'] 							= $info['networth'];
			$data['gross_sales'] 						= $info['gross_sales'];
			$data['net_profit'] 						= $info['net_profit'];
			$data['common_shares'] 						= $info['common_shares'];
			$data['par_value'] 							= $info['par_value'];
			$data['created_by'] 						= $info['created_by'];
			$data['created_date'] 						= $info['created_date'];	

			$data['statement_date'] 					= $contractor_statement_info['statement_date'];
			$data['stmt_type'] 							= $contractor_statement_info['stmt_type'];
			$data['total_current_assets'] 				= $contractor_statement_info['total_current_assets'];
			$data['total_non_current_assets'] 			= $contractor_statement_info['total_non_current_assets'];
			$data['total_current_liabilities'] 			= $contractor_statement_info['total_current_liabilities'];
			$data['long_term_liabilities'] 				= $contractor_statement_info['long_term_liabilities'];
			$data['networth'] 							= $contractor_statement_info['networth'];
			$data['gross_sales'] 						= $contractor_statement_info['gross_sales'];
			$data['net_profit'] 						= $contractor_statement_info['net_profit'];
			$data['common_shares'] 						= $contractor_statement_info['common_shares'];
			$data['par_value'] 							= $contractor_statement_info['par_value'];
			$data['created_by'] 						= $contractor_statement_info['created_by'];
			$data['created_date'] 						= $contractor_statement_info['created_date'];

			$data['year'] 								= $year;


			$statement_info 							= $this->contractors->get_statement_list($year, $contractor_id);
			$data['statement_info'] 					= $statement_info;
			
			$data['view_statement'] = $this->_construct_view_statement($contractor_id);
		}

		catch(PDOException $e){
			
			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/finance',$data);
	}

	public function modal_contract_mobilization($encoded_id, $salt, $token, $progress_report_id)
	{
		try{
			$data 		= array();
			$resources 	= array();
			$params 	= get_params();
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);	
			
			$hash_id 		= base64_url_decode($encoded_id);

			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $hash_id;

			$info = $this->contractors->get_specific_mobilization($progress_report_id);

			// $pos_name = $this->contractors->get_specific_position($info['position_id']);

			if(EMPTY($info))
				throw new Exception($this->lang->line('err_invalid_contractor'));	

			$data['progress_report_id'] 	= $info['progress_report_id'];
			$data['contr_staff_id'] 		= $info['contr_staff_id'];
			$data['position_id'] 			= $info['position_id'];
			$data['salary_in_usd'] 			= $info['salary_in_usd'];
			$data['start_duration'] 		= $info['start_duration'];
			$data['end_duration'] 			= $info['end_duration'];
			$data['mobilized_date'] 		= $info['mobilized_date'];
			$data['mobilized_remarks'] 		= $info['mobilized_remarks'];
			$data['demobilized_date'] 		= $info['demobilized_date'];
			$data['demobilized_remarks']	= $info['demobilized_remarks'];
			$data['created_by'] 			= $info['created_by'];
			$data['created_date'] 			= $info['created_date'];

		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/contractor_contract_mobilization',$data);
	}

	public function modal_finance_statement($encoded_id, $salt, $token, $statement_date)
	{
		try{

			$data = array();
			$resources = array();
			$params = get_params();
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);	
			
			$hash_id 		= base64_url_decode($encoded_id);

			$key 			= $this->get_hash_key('statement_date');
			$where			= array();
			$where[$key]	= $hash_id;

			$info = $this->contractors->get_specific_finance($statement_date);

			if(EMPTY($info))
				throw new Exception($this->lang->line('err_invalid_contractor'));	

			$data['statement_date'] 			= $info['statement_date'];
			$data['stmt_type'] 					= $info['stmt_type'];
			$data['total_current_assets'] 		= $info['total_current_assets'];
			$data['total_non_current_assets'] 	= $info['total_non_current_assets'];
			$data['total_current_liabilities'] 	= $info['total_current_liabilities'];
			$data['long_term_liabilities'] 		= $info['long_term_liabilities'];
			$data['networth'] 					= $info['networth'];
			$data['gross_sales'] 				= $info['gross_sales'];
			$data['net_profit'] 				= $info['net_profit'];
			$data['common_shares']				= $info['common_shares'];
			$data['par_value'] 					= $info['par_value'];
			$data['created_by'] 				= $info['created_by'];
			$data['created_date'] 				= $info['created_date'];
			$data['modified_by'] 				= $info['modified_by'];
			$data['modified_date'] 				= $info['modified_date'];

		}

		catch(PDOException $e){
			echo $e;
			/*$this->rlog_error($e);

			echo $this->get_user_message($e);*/
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/finance_statement',$data);
	}
	
	public function modal_contract_foreign_remittance($encoded_id, $salt, $token, $report_remitt_id)
	{
		try{
			$data = array();
			$resources = array();
			$params = get_params();

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);	
			
			$hash_id 		= base64_url_decode($encoded_id);

			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $hash_id;

			$info = $this->contractors->get_specific_remittance($report_remitt_id);

			if(EMPTY($info))
				throw new Exception($this->lang->line('err_invalid_contractor'));	

			$data['report_remitt_id'] 	= $info['report_remitt_id'];
			$data['progress_report_id'] = $info['progress_report_id'];
			$data['remitt_date'] 		= $info['remitt_date'];
			$data['remitt_amount'] 		= $info['remitt_amount'];
			$data['country'] 			= $info['country'];
			$data['country_name'] 		= $info['country_name'];
			$data['receiving_bank'] 	= $info['receiving_bank'];
			$data['created_by'] 		= $info['created_by'];
			$data['created_date'] 		= $info['created_date'];
			$data['modified_by'] 		= $info['modified_by'];		

		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/contractor_contract_foreign_remittance',$data);
	}

	public function modal_contractor_order_payment($encoded_id, $salt, $token, $contractor_payment_id)
	{
		try{

				$data 		= array();
				$resources 	= array();
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('contractor_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info 					= $this->contractors->get_specific_contractor($where);
				$order_payment_info 	= $this->contractors->get_specific_order_payment($contractor_payment_id);
				$collection_info 		= $this->contractors->get_collection_list($contractor_payment_id);

				/*if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_contractor'));*/

				$data['contractor_id'] 						= $info['contractor_id'];
				$data['contractor_name'] 					= $info['contractor_name'];
				$data['total_current_assets'] 				= $info['total_current_assets'];
				$data['total_non_current_assets'] 			= $info['total_non_current_assets'];
				$data['total_current_liabilities'] 			= $info['total_current_liabilities'];
				$data['long_term_liabilities'] 				= $info['long_term_liabilities'];
				$data['networth'] 							= $info['networth'];
				$data['gross_sales'] 						= $info['gross_sales'];
				$data['net_profit'] 						= $info['net_profit'];
				$data['common_shares'] 						= $info['common_shares'];
				$data['par_value'] 							= $info['par_value'];

				$data['contractor_payment_id'] 				= $order_payment_info['contractor_payment_id'];
				$data['op_date'] 							= $order_payment_info['op_date'];
				$data['created_by'] 						= $order_payment_info['created_by'];
				$data['created_date'] 						= $order_payment_info['created_date'];
				$data['collection_info']					= $collection_info;


				// CREATE NEW SECURITY VARIABLES
				$contractor_id		= $info['contractor_id'];
				$hash_id 			= $this->hash($contractor_id);			

				$encoded_id			= base64_url_encode($hash_id);
				$salt 				= gen_salt();
				$token				= in_salt($encoded_id, $salt);
				$data['security']	= $encoded_id . '/' . $salt . '/' . $token;

		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/contractor_payment_order',$data);
	}

	public function modal_contractor_staffing_experience($encoded_id, $salt, $token, $contr_staff_exp_id)
	{
		try{
			$data = array();
			$resources = array();
			$params = get_params();

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);	
			
			$hash_id 		= base64_url_decode($encoded_id);

			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $hash_id;

			$info 					= $this->contractors->get_specific_contractor($where);
			$staff_experience_info 	= $this->contractors->get_specific_staff_experience($contr_staff_exp_id);

			/*if(EMPTY($info))
				throw new Exception($this->lang->line('err_invalid_contractor'));	*/

			$data['contractor_id'] 						= $info['contractor_id'];
			$data['statement_date'] 					= $info['statement_date'];
			$data['total_current_assets'] 				= $info['total_current_assets'];
			$data['total_non_current_assets'] 			= $info['total_non_current_assets'];
			$data['total_current_liabilities'] 			= $info['total_current_liabilities'];
			$data['long_term_liabilities'] 				= $info['long_term_liabilities'];
			$data['networth'] 							= $info['networth'];
			$data['gross_sales'] 						= $info['gross_sales'];
			$data['net_profit'] 						= $info['net_profit'];
			$data['common_shares'] 						= $info['common_shares'];
			$data['par_value'] 							= $info['par_value'];
			$data['created_by'] 						= $info['created_by'];
			$data['created_date'] 						= $info['created_date'];

			$data['company_name']						= $staff_experience_info['company_name'];
			$data['start_date']							= $staff_experience_info['start_date'];
			$data['end_date']							= $staff_experience_info['end_date'];
			$data['company_name']						= $staff_experience_info['company_name'];
			$data['company_address']					= $staff_experience_info['company_address'];
			$data['job_description']					= $staff_experience_info['job_description'];
			$data['stockholder_flag']					= $staff_experience_info['stockholder_flag'];
			$data['paid_in_amount']						= $staff_experience_info['paid_in_amount'];
			
			// CREATE NEW SECURITY VARIABLES
			$contractor_id		= $info['contractor_id'];
			$hash_id 			= $this->hash($contractor_id);			

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/contractor_staffing_experience',$data);
	}

	public function modal_contractor_staffing_project($encoded_id, $salt, $token, $contr_staff_proj_id)
	{
		try{
			$data = array();
			$resources = array();
			$params = get_params();

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);	
			
			$hash_id 		= base64_url_decode($encoded_id);

			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $hash_id;

			$info 					= $this->contractors->get_specific_contractor($where);
			$staff_projects_info 	= $this->contractors->get_specific_staff_projects($contr_staff_proj_id);

			/*if(EMPTY($info))
				throw new Exception($this->lang->line('err_invalid_contractor'));*/	

			$data['contractor_id'] 						= $info['contractor_id'];
			$data['statement_date'] 					= $info['statement_date'];
			$data['total_current_assets'] 				= $info['total_current_assets'];
			$data['total_non_current_assets'] 			= $info['total_non_current_assets'];
			$data['total_current_liabilities'] 			= $info['total_current_liabilities'];
			$data['long_term_liabilities'] 				= $info['long_term_liabilities'];
			$data['networth'] 							= $info['networth'];
			$data['gross_sales'] 						= $info['gross_sales'];
			$data['net_profit'] 						= $info['net_profit'];
			$data['common_shares'] 						= $info['common_shares'];
			$data['par_value'] 							= $info['par_value'];
			$data['created_by'] 						= $info['created_by'];
			$data['created_date'] 						= $info['created_date'];

			$data['project_title'] 						= $staff_projects_info['project_title'];
			$data['project_location'] 					= $staff_projects_info['project_location'];
			$data['owner'] 								= $staff_projects_info['owner'];
			$data['project_cost'] 						= $staff_projects_info['project_cost'];
			$data['positions'] 							= $staff_projects_info['positions'];
			$data['project_type_code'] 					= $staff_projects_info['project_type_code'];
			$data['start_date'] 						= $staff_projects_info['start_date'];
			$data['end_date'] 							= $staff_projects_info['end_date'];

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/contractor_staffing_project',$data);
	}

	public function modal_project($encoded_id, $salt, $token)
	{
		try{

			$data 				= array();
			$resources 			= array();
			$params 			= get_params();

			IF(EMPTY($encoded_id))
				throw new Exception($this->lang->line('err_invalid_request'));
		

			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);	
			
			$hash_id 		= base64_url_decode($encoded_id);

			$key 			= $this->get_hash_key('contract_id');
			$where			= array();
			$where[$key]	= $hash_id;


			$data["countries"] 	= $this->contractors->get_countries();
			$data["status"] 	= $this->contractors->get_status();

			$contracts 			= $this->contractors->get_specific_contracts($where);
			// $contractors 		= $this->contractors->get_specific_contractor($where);
			$contracts_project 	= $this->contractors->get_specific_contracts_project($where);

			/*if(EMPTY($contracts))
				throw new Exception($this->lang->line('err_invalid_request'));*/

			$data['contract_id'] 		= $contracts['contract_id'];
			$data['base'] 				= $contracts['base'];
			$data['project_location'] 	= $contracts['project_location'];
			$data['project_title'] 		= $contracts['project_title'];
			$data['province'] 			= $contracts['province'];
			$data['country_code'] 		= $contracts['country_code'];
			$data['contract_status_id'] = $contracts['contract_status_id'];
			$data['project_cost'] 		= $contracts['project_cost'];
			$data['contract_cost'] 		= $contracts['contract_cost'];

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/project',$data);
	}

	public function modal_director($encoded_id, $salt, $token)
	{
		try{
			$data = array();
			$resources = array();

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('contractor_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->contractors->get_specific_contractor($where);

				/*if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_contractor'));	*/

				$data['contractor_id'] 			= $info['contractor_id'];

				// CREATE NEW SECURITY VARIABLES
				$contractor_id		= $info['contractor_id'];
				$hash_id 		= $this->hash($contractor_id);			

			}

			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/director',$data);
	}

	public function modal_principal($encoded_id, $salt, $token)
	{
		try{
			$data = array();
			$resources = array();

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('contractor_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->contractors->get_specific_contractor($where);

				/*if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_contractor'));*/	

				$data['contractor_id'] 			= $info['contractor_id'];

				// CREATE NEW SECURITY VARIABLES
				$contractor_id		= $info['contractor_id'];
				$hash_id 		= $this->hash($contractor_id);			

			}

			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/principal',$data);
	}

	public function modal_technical($encoded_id, $salt, $token)
	{
		try{
			$data = array();
			$resources = array();

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('contractor_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->contractors->get_specific_contractor($where);

				/*if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_contractor'));*/	

				$data['contractor_id'] 			= $info['contractor_id'];

				// CREATE NEW SECURITY VARIABLES
				$contractor_id		= $info['contractor_id'];
				$hash_id 		= $this->hash($contractor_id);			

			}

			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/technical',$data);
	}

	public function modal_manpower($encoded_id, $salt, $token)
	{
		try{
			$data = array();
			$resources = array();

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('contractor_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->contractors->get_specific_contractor($where);

				/*if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_contractor'));	*/

				$data['contractor_id'] 			= $info['contractor_id'];

				// CREATE NEW SECURITY VARIABLES
				$contractor_id		= $info['contractor_id'];
				$hash_id 		= $this->hash($contractor_id);			

			}

			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/manpower',$data);
	}

	public function modal_manpower_schedule($encoded_id, $salt, $token)
	{
		try{
			$data = array();
			$resources = array();

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('contractor_id');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->contractors->get_specific_contractor($where);

				/*if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_contractor'));	*/

				$data['contractor_id'] 			= $info['contractor_id'];

				// CREATE NEW SECURITY VARIABLES
				$contractor_id		= $info['contractor_id'];
				$hash_id 		= $this->hash($contractor_id);			

			}

			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/manpower_schedule',$data);
	}

	public function modal_construction_equipment($encoded_id, $salt, $token, $serial_no)
	{
		try{
			$data = array();
			$resources = array();

			// DECODE THE ID TO GET THE ORIGINAL VALUE
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);	
			
			$hash_id 		= base64_url_decode($encoded_id);

			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $hash_id;

			$info 				= $this->contractors->get_specific_contractor($where);
			$equipment_info 	= $this->contractors->get_specific_equipment($serial_no);

			$data['equipment_type'] 		= $equipment_info['equipment_type'];
			$data['serial_no'] 				= $equipment_info['serial_no'];
			$data['capacity'] 				= $equipment_info['capacity'];
			$data['brand'] 					= $equipment_info['brand'];
			$data['model'] 					= $equipment_info['model'];
			$data['acquired_date'] 			= $equipment_info['acquired_date'];
			$data['acquisition_cost'] 		= $equipment_info['acquisition_cost'];
			$data['present_condition'] 		= $equipment_info['present_condition'];
			$data['location'] 				= $equipment_info['location'];
			$data['ownership_type'] 		= $equipment_info['ownership_type'];
			$data['active_flag']			= $equipment_info['active_flag'];
			$data['for_registration_flag']	= $equipment_info['for_registration_flag'];	

			/*if(EMPTY($info))
				throw new Exception($this->lang->line('err_invalid_contractor'));	*/

			$data['contractor_id'] 			= $info['contractor_id'];

		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/contractor_construction_equipment',$data);
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_contractor'));
			}


			$security = explode('/', $params['security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function view_contractor($encoded_id, $salt, $token)
	{
		try 
		{
			
			$data 					= array();

			// $params 	= get_params();
				
			check_salt($encoded_id, $salt, $token);
			
			// DECODE THE ID TO GET THE ORIGINAL VALUE
			$hash_id 		= base64_url_decode($encoded_id);
		
			// CONSTRUCT SECURITY VARIABLES
			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $hash_id;
			
			// GET SPECIFIC REQUEST
			$info	= $this->contractors->get_specific_contractor($where);
			
			// GET CONTRACTOR NAME
			$data['contractor_name']		= $info['contractor_name'];
			
			$data['default_tab']			= 'contractor';
			
			$data['menu']['contractor']		= 1;
			$data['menu']['contracts']		= 1;
			$data['menu']['staffing']		= 1;
			$data['menu']['finance']		= 1;
			$data['menu']['work']			= 1;
			$data['menu']['equipment']		= 1;
			$data['menu']['op']				= 1;
			$data['menu']['registration']	= 1;


			// START: CREATE NEW SECURITY VARIABLES
			
			$encoded_id = base64_url_encode($hash_id);
			$salt 		= gen_salt();
			$token 		= in_salt($encoded_id, $salt);
			
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			// END: CREATE NEW SECURITY VARIABLES

		}
		catch(PDOException $e){

			$this->rlog_error($e);

			$msg = $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			$msg = $e->getMessage();
		}
				
		$resources				= array();
		$resources['load_js']	= array('modules/ceis/ceis_view_contractor');
		$resources['load_init'] = array("LoadTab.init('".$data['default_tab']."');");
		
		$this->template->load('contractor_sub_menu', $data, $resources);

	}

	public function load_tab()
	{
		try 
		{
			$params 	= get_params();
			$tab		= $params['tab'];
			$security	= $params['security'];
			$modal 		= array();
			$data 		= array();	
			$resources 	= array();	

			if(EMPTY($params['tab']))
				throw new Exception('Invalid request');

			if(EMPTY($params['security']))
				throw new Exception('Invalid request');

			// PARSE SECURITY VARIABLES
			$security	= explode('/', $params['security']);
			$encoded_id	= $security[0];
			$salt		= $security[1];
			$token		= $security[2];

			// DECODE
			$hash_id	= base64_url_decode($encoded_id);


			// START: GET THE CONTRACTOR INFORMATION USING HASH ID
			$key 			= $this->get_hash_key('contractor_id');
			$where			= array();
			$where[$key]	= $hash_id;
			$info 			= $this->contractors->get_specific_contractor($where);
			// END: GET THE CONTRACTOR INFORMATION USING HASH ID

			$tab					= ISSET($params['tab'])	 				? $params['tab'] : "";
			$contract_id			= ISSET($params['contract_id']) 		? $params['contract_id'] : "";
			$progress_report_id 	= ISSET($params['progress_report_id']) 	? $params['progress_report_id'] : ""; 
			$contr_staff_id 		= ISSET($params['contr_staff_id']) 		? $params['contr_staff_id'] : "";
			$contr_staff_exp_id 	= ISSET($params['contr_staff_exp_id']) 	? $params['contr_staff_exp_id'] : "";
			$contr_staff_proj_id 	= ISSET($params['contr_staff_proj_id']) ? $params['contr_staff_proj_id'] : "";
			$serial_no 				= ISSET($params['serial_no']) 			? $params['serial_no'] : "";

			//PRINT_R
			// echo '<pre>';print_r($params); echo '</pre>';
			
			//CONTRACTOR ID
			$contractor_id 	= $hash_id;

			switch($tab)
			{
				case 'contractor':
					$resources['load_js']			= array(JS_SELECTIZE);	
					$resources['load_css']			= array(CSS_SELECTIZE);
					$data["contractor_category"] 	= $this->contractors->get_category();

					$data['contractor_name'] 		= $info['contractor_name'];
					$data['contractor_address'] 	= $info['contractor_address'];
					$data['sec_reg_no'] 			= $info['sec_reg_no'];
					$data['sec_reg_date'] 			= $info['sec_reg_date'];
					$data['contractor_category_id'] = $info['contractor_category_id'];
					$data['license_no'] 			= $info['license_no'];
					$data['license_date'] 			= $info['license_date'];
					$data['bir_clearance_date'] 	= $info['bir_clearance_date'];
					$data['customs_clearance_date'] = $info['customs_clearance_date'];
					$data['court_clearance_date'] 	= $info['court_clearance_date'];
					$data['tel_no'] 				= $info['tel_no'];
					$data['fax_no'] 				= $info['fax_no'];
					$data['email'] 					= $info['email'];
					$data['website'] 				= $info['website'];
					$data['rep_first_name'] 		= $info['rep_first_name'];
					$data['rep_mid_name'] 			= $info['rep_mid_name'];
					$data['rep_last_name'] 			= $info['rep_last_name'];
					$data['rep_address'] 			= $info['rep_address'];
					$data['rep_contact_no'] 		= $info['rep_contact_no'];
					$data['rep_email'] 				= $info['rep_email'];
					$data['created_by'] 			= $info['created_by'];
					$data['created_date'] 			= $info['created_date'];
					$data['modified_by'] 			= $info['modified_by'];
					$data['modified_date'] 			= $info['modified_date'];

					$this->load->view('contractor_sub_contractor_info',$data);
					$this->load_resources->get_resource( $resources );
				break;
				
				case 'contracts':

					$modal 		= array(
						'modal_project'				=> array(
							'module'				=> PROJECT_CEIS,
							'controller'			=> __CLASS__,
							'multiple'				=> true,
							'method'				=> 'modal_project'
						)
					);

					$resources['load_modal'] 	= $modal;
					$resources['load_css'] 		= array(CSS_DATATABLE);
					$resources['load_js'] 		= array(JS_DATATABLE,'modules/ceis/ceis_view_contractor');
					$resources['datatable'][]	= array(
						'table_id' 	=> 'table_contractors_projects', 
						'path'		=> PROJECT_CEIS.'/ceis_contractors/get_contract_project_list/'.$contractor_id.''
					);	

					$resources['datatable'][]	= array(
						'table_id' 	=> 'table_contractors_services', 
						'path'		=> PROJECT_CEIS.'/ceis_contractors/get_contract_service_list/'.$contractor_id.''
					);	
					

					$this->load->view('contractor_sub_contracts');
					$this->load_resources->get_resource( $resources );
				break;

					case 'contracts_view':

						$resources['load_css'] 		= array(CSS_DATATABLE);
						$resources['load_js'] 		= array(JS_DATATABLE);
						$resources['datatable'][]	= array(
							'table_id' 	=> 'table_contracts_view', 
							'path'		=> PROJECT_CEIS.'/ceis_contractors/get_progress_report_list/'.$contractor_id.'/'.$contract_id.''
						);

						$contractor_id 				= $info['contractor_id'];
						$data['contractor_name'] 	= $info['contractor_name'];

						$contract_info 	= $this->contractors->get_specific_contract_info($contract_id);

						$data['contractor_name'] 	= $contract_info['contractor_name'];
						$data['project_title'] 		= $contract_info['project_title'];
						$data['project_location'] 	= $contract_info['project_location'];
						$data['contract_cost'] 		= $contract_info['contract_cost'];
						$data['contract_type'] 		= $contract_info['contract_type'];
						$data['contract_duration'] 	= $contract_info['contract_duration'];
						$data['started_date'] 		= $contract_info['started_date'];
						$data['completed_date'] 	= $contract_info['completed_date'];

						$this->load->view('contractor_sub_project_view',$data);
						$this->load_resources->get_resource( $resources );
					break;

					case 'contracts_manpower':

						$resources['load_css'] 		= array(CSS_DATATABLE);
						$resources['load_js'] 		= array(JS_DATATABLE);
						$resources['datatable'][]	= array(
							'table_id' 	=> 'table_contract_mobilization', 
							'path'		=> PROJECT_CEIS.'/ceis_contractors/get_contract_mobilization_list/'.$contractor_id.'/'.$contract_id.''
						);	

						$resources['datatable'][]	= array(
							'table_id' 	=> 'table_contract_foreign_remittance', 
							'path'		=> PROJECT_CEIS.'/ceis_contractors/get_contract_foreign_remittance_list/'.$contractor_id.'/'.$contract_id.''
						);	

						$modal 		= array(
								'modal_contract_mobilization'	=> array(
									'module'		=> PROJECT_CEIS,
									'controller'	=> __CLASS__,
									'multiple'		=> true,
									'method'		=> 'modal_contract_mobilization'
											),
								'modal_contract_foreign_remittance'	=> array(
									'module'		=> PROJECT_CEIS,
									'controller'	=> __CLASS__,
									'multiple'		=> true,
									'method'		=> 'modal_contract_foreign_remittance'
											)
								);
						$resources['load_modal'] 	= $modal;
						
						$contractor_id 				= $info['contractor_id'];

						$progress_report_pro_info 	= $this->contractors->get_specific_contract_progress_pro($progress_report_id);
						$progress_report_semi_info 	= $this->contractors->get_specific_contract_progress_semi($progress_report_id);

						//MANPOWER REQUIREMENTS
						$data['p_req_manpower'] 	= $progress_report_pro_info['req_manpower'];
						$data['s_req_manpower'] 	= $progress_report_semi_info['req_manpower'];

						$data['p_mob_manpower'] 	= $progress_report_pro_info['mob_manpower'];
						$data['s_mob_manpower'] 	= $progress_report_semi_info['mob_manpower'];

						$data['p_onsite_manpower'] 	= $progress_report_pro_info['onsite_manpower'];
						$data['s_onsite_manpower'] 	= $progress_report_semi_info['onsite_manpower'];

						//FINANCIAL
						$data['p_payroll_amount'] 	= $progress_report_pro_info['payroll_amount'];
						$data['s_payroll_amount'] 	= $progress_report_semi_info['payroll_amount'];

						$data['p_service_fee'] 		= $progress_report_pro_info['service_fee'];
						$data['s_service_fee'] 		= $progress_report_semi_info['service_fee'];

						$data['p_other_fee'] 		= $progress_report_pro_info['other_fee'];
						$data['s_other_fee'] 		= $progress_report_semi_info['other_fee'];

						$this->load->view('contractor_contracts_manpower',$data);
						$this->load_resources->get_resource( $resources );
					break;

				case 'staffing':

					$resources['load_css'] 		= array(CSS_DATATABLE);
					$resources['load_js'] 		= array(JS_DATATABLE);
					$resources['datatable'][]	= array(
						'table_id' 	=> 'table_director_staffing', 
						'path'		=> PROJECT_CEIS.'/ceis_contractors/get_staffing_director_list/D/'.$contractor_id.''
					);		

					$resources['datatable'][]	= array(
						'table_id' 	=> 'table_principal_officer_staffing', 
						'path'		=> PROJECT_CEIS.'/ceis_contractors/get_staffing_director_list/P/'.$contractor_id.''
					);	

					$resources['datatable'][]	= array(
						'table_id' 	=> 'table_tech_staff_staffing', 
						'path'		=> PROJECT_CEIS.'/ceis_contractors/get_staffing_director_list/T/'.$contractor_id.''
					);	


					$this->load->view('contractor_sub_staffing',$data);
					$this->load_resources->get_resource( $resources );
				break;

					case 'staffing_director':

						$modal 		= array(
								'modal_contractor_staffing_experience'	=> array(
									'module'		=> PROJECT_CEIS,
									'controller'	=> __CLASS__,
									'multiple'		=> true,
									'method'		=> 'modal_contractor_staffing_experience'
											),
								'modal_contractor_staffing_project'	=> array(
									'module'		=> PROJECT_CEIS,
									'controller'	=> __CLASS__,
									'multiple'		=> true,
									'method'		=> 'modal_contractor_staffing_project'
											)
								);
						$resources['load_modal']	= $modal;
						$resources['load_css'] 		= array(CSS_DATATABLE);
						$resources['load_js'] 		= array(JS_DATATABLE);
						$resources['datatable'][]	= array(
							'table_id' 	=> 'contractor_staffing_experience', 
							'path'		=> PROJECT_CEIS.'/ceis_contractors/get_staffing_experience_list/'.$contractor_id.'/'.$contr_staff_id.''
						);
						$resources['datatable'][]	= array(
							'table_id' 	=> 'contractor_staffing_project', 
							'path'		=> PROJECT_CEIS.'/ceis_contractors/get_staffing_project_list/'.$contractor_id.'/'.$contr_staff_id.''
						);
						

						$contractor_staff_info 	= $this->contractors->get_specific_contractor_staff($contr_staff_id);
						$profession_info 		= $this->contractors->get_profession_list($contr_staff_id);
						// $work_experience_info 	= $this->contractors->get_specific_work_experience($contr_staff_id);
						
						$data['first_name'] 			= $contractor_staff_info['first_name'];	
						$data['middle_name'] 			= $contractor_staff_info['middle_name'];	
						$data['last_name'] 				= $contractor_staff_info['last_name'];	
						$data['address'] 				= $contractor_staff_info['address'];	
						$data['birth_date'] 			= $contractor_staff_info['birth_date'];	
						$data['birth_place'] 			= $contractor_staff_info['birth_place'];	
						$data['educational_attainment'] = $contractor_staff_info['educational_attainment'];	
						$data['school_attended'] 		= $contractor_staff_info['school_attended'];
						$data['year_graduated'] 		= $contractor_staff_info['year_graduated'];	
						$data['profession_info']		= $profession_info;
						
						$this->load->view('contractor_staffing_personal',$data);
						$this->load_resources->get_resource($resources);
					break;

				case 'finance':

					/*// START: GET THE CONTRACTOR INFORMATION USING HASH ID
					$key 			= $this->get_hash_key('contractor_id');
					$where			= array();
					$where[$key]	= $contractor_id;
					$info 			= $this->contractors->get_specific_contractor($where);
					// END: GET THE CONTRACTOR INFORMATION USING HASH ID

					$contractor_id = $info['contractor_id'];*/

					$modal 		= array(
						'modal_statement'			=> array(
							'module'				=> PROJECT_CEIS,
							'controller'			=> __CLASS__,
							'multiple'				=> true,
							'method'				=> 'modal_statement',
							'size'					=> 'xl',
							'height'	 			=> '500px;'

						),
						'modal_finance_statement'	=> array(
							'module'				=> PROJECT_CEIS,
							'controller'			=> __CLASS__,
							'multiple'				=> true,
							'method'				=> 'modal_finance_statement'
						)
					);

					$resources['load_modal'] 	= $modal;
					$resources['load_css'] 		= array(CSS_DATATABLE,CSS_DATETIMEPICKER);
					$resources['load_js'] 		= array(JS_DATATABLE,JS_DATETIMEPICKER);
					$resources['datatable'] 	= array();
					$resources['datatable'][]	= array(
						'table_id' 	=> 'table_finance', 
						'path'		=> PROJECT_CEIS.'/ceis_contractors/get_finance_contractor_statements_list/'.$contractor_id.''
					);	

					// START: GET THE CONTRACTOR INFORMATION USING HASH ID
					$key 			= $this->get_hash_key('contractor_id');
					$where			= array();
					$where[$key]	= $contractor_id;
					$info 			= $this->contractors->get_specific_contractor($where);
					// END: GET THE CONTRACTOR INFORMATION USING HASH ID

					$contractor_id 			= $info['contractor_id'];
					$data['contractor_id'] 	= $contractor_id;

					$contractor_statement_info 	= $this->contractors->get_max_date_statement($contractor_id);
					$data['highest']  			= $contractor_statement_info['MAX(statement_date)'];

					// CONSTRUCT SECURITY VARIABLES
					$hash_id		= $this->hash($contractor_id);
					$encoded_id 	= base64_url_encode($hash_id);
					$salt 			= gen_salt();
					$token 			= in_salt($encoded_id, $salt);			
					$url 			= $encoded_id."/".$salt."/".$token;

					$data['url'] 	= $url;

					$this->load->view('contractor_sub_finance',$data);
					$this->load_resources->get_resource( $resources );
				break;

					case 'finance_view_statement':
					
						// START: GET THE CONTRACTOR INFORMATION USING HASH ID
						$key 			= $this->get_hash_key('contractor_id');
						$where			= array();
						$where[$key]	= $contractor_id;
						$info 			= $this->contractors->get_specific_contractor($where);
						// END: GET THE CONTRACTOR INFORMATION USING HASH ID

						$contractor_id = $info['contractor_id'];

						$contractor_statement_info 	= $this->contractors->get_specific_statement($contractor_id);

						$data['statement_date'] 			= $contractor_statement_info['statement_date'];
						$data['stmt_type'] 					= $contractor_statement_info['stmt_type'];
						$data['total_current_assets'] 		= $contractor_statement_info['total_current_assets'];
						$data['total_non_current_assets'] 	= $contractor_statement_info['total_non_current_assets'];
						$data['total_current_liabilities'] 	= $contractor_statement_info['total_current_liabilities'];
						$data['long_term_liabilities'] 		= $contractor_statement_info['long_term_liabilities'];
						$data['networth'] 					= $contractor_statement_info['networth'];
						$data['gross_sales'] 				= $contractor_statement_info['gross_sales'];
						$data['net_profit'] 				= $contractor_statement_info['net_profit'];
						$data['common_shares'] 				= $contractor_statement_info['common_shares'];
						$data['par_value'] 					= $contractor_statement_info['par_value'];
						$data['created_by'] 				= $contractor_statement_info['created_by'];
						$data['created_date'] 				= $contractor_statement_info['created_date'];

						$this->load->view('contractor_finance_view_statement',$data);
					
					break;

				case 'work':
					
					// START: GET THE CONTRACTOR INFORMATION USING HASH ID
					$key 			= $this->get_hash_key('contractor_id');
					$where			= array();
					$where[$key]	= $contractor_id;
					$info 			= $this->contractors->get_specific_contractor($where);
					// END: GET THE CONTRACTOR INFORMATION USING HASH ID

					$contractor_id = $info['contractor_id'];

					$contractor_work_volume_info 			= $this->contractors->get_specific_work_volume($contractor_id);
					$data['contractor_work_volume_info'] 	= $contractor_work_volume_info;

					//total
					$total_work_volume 						= $this->contractors->get_total_work_volume($contractor_id);
					$data['total_work_volume'] 				= $total_work_volume;

					//average
					$avg_work_volume 						= $this->contractors->get_avg_work_volume($contractor_id);
					$data['avg_work_volume'] 				= $avg_work_volume;

					/*$data['amount'] 				= $contractor_work_volume_info['amount'];

					$contractor_work_volume_info 	= $this->contractors->get_max_date_work_volume($contractor_id);
					$data['highest_year']  			= $contractor_work_volume_info['MAX(year)'];*/

					$this->load->view('contractor_sub_work_volume',$data);
				break;

				case 'equipment':
					$modal 		= array(
						'modal_construction_equipment'				=> array(
							'module'				=> PROJECT_CEIS,
							'controller'			=> __CLASS__,
							'multiple'				=> true,
							'height'				=> '450px;',
							'method'				=> 'modal_construction_equipment'
						)
					);
					$resources['load_modal'] 	= $modal;
					$resources['load_css'] 		= array(CSS_DATATABLE);
					$resources['load_js'] 		= array(JS_DATATABLE);
					$resources['datatable'][]	= array(
						'table_id' 	=> 'table_construction_equipment', 
						'path'		=> PROJECT_CEIS.'/ceis_contractors/get_construction_equipment_list/'.$contractor_id.''
					);

					$this->load->view('contractor_sub_construction_equipment');
					$this->load_resources->get_resource( $resources );
				break;

				case 'op':
					$modal 		= array(
							'modal_contractor_order_payment'	=> array(
								'module'						=> PROJECT_CEIS,
								'controller'					=> __CLASS__,
								'multiple'						=> true,
								'method'						=> 'modal_contractor_order_payment'
							)
						);
					$resources['load_modal']	= $modal;
					$resources['load_css'] 		= array(CSS_DATATABLE);
					$resources['load_js'] 		= array(JS_DATATABLE);
					$resources['datatable'][]	= array(
						'table_id' 	=> 'table_order_of_payment', 
						'path'		=> PROJECT_CEIS.'/ceis_contractors/get_construction_payment_list/'.$contractor_id.''
					);

					$this->load->view('contractor_sub_order_of_payment');
					$this->load_resources->get_resource( $resources );
				break;

				case 'registration':
					$modal = array(
						'modal_change_status' 	=> array(
							'controller'		=> __CLASS__,
							'module'			=> PROJECT_CEIS,
							'multiple'			=> true,
							'method'			=> 'modal_change_status'
						)
					);
					$resources['load_modal']	= $modal;
					$resources['load_css'] 		= array();
					$resources['load_js'] 		= array();

					$encoded_contractor_id 		= $contractor_id;

					// START: GET THE CONTRACTOR INFORMATION USING HASH ID
					$key 			= $this->get_hash_key('contractor_id');
					$where			= array();
					$where[$key]	= $contractor_id;
					$info 			= $this->contractors->get_specific_contractor($where);
					// END: GET THE CONTRACTOR INFORMATION USING HASH ID

					$contractor_id 				= $info['contractor_id'];
					$registration_info 			= $this->contractors->get_registration($contractor_id);
					$registration_list_info 	= $this->contractors->get_registration_list($contractor_id);

					$data['contractor_name']		= $info['contractor_name'];
					$data['expiry_date']			= $registration_info['expiry_date'];
					$data['registration_no'] 		= $registration_info['registration_no'];
					$data['registration_status']	= $registration_info['registration_status'];
					$data['rep_email']				= $registration_info['rep_email'];
					$data['registration_list_info']	= $registration_list_info;
					$data['contractor_id']			= $contractor_id;
					$data['encoded_contractor_id']	= $encoded_contractor_id;
					
					$this->load->view('contractor_sub_registration_info',$data);
					$this->load_resources->get_resource( $resources );

				break;
			}
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);
			
			echo $e->getMessage();	
		}
	}
}

/* End of file Ceis_contractors.php */
/* Location: ./application/modules/ceis/controllers/Ceis_contractors.php */