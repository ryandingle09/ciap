<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ceis_request_sub_order_of_payment extends Ceis_Controller
{
	private $module = MODULE_CEIS_REQUESTS;
	private $collection_group = array();

	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('ceis_request_sub_order_of_payment_model', 'crsoopm');
		$this->load->model('ceis_request_tasks_model', 'crtm');
	}
	
	public function modal_op($action_code, $request_id, $encoded_id=NULL, $salt=NULL, $token=NULL)
	{
		try
		{
			$data    = array();
			$action  = base64_url_decode($action_code);
			$rid     = base64_url_decode($request_id);
			
			
			$data['collections'] 	 = $this->crsoopm->get_param_collection_nature();
			$data['request']		 = $this->crsoopm->get_request($rid);
			
			
		 	if($data['request']['request_type_id'] == REQUEST_TYPE_EQP):
				$equip_details = $this->crsoopm->get_request_equip($rid);
		 		
				$data['contractor_name'] = $equip_details['registrant_name'];
			else:
				$request_details = $this->crsoopm->get_request_details($rid);
				$data['contractor_name'] = $request_details['contractor_name'];
			endif;
			
			
			switch($action):
				case ACTION_ADD  :
					break;
				case ACTION_EDIT :	
						$id      = base64_url_decode($encoded_id);
						
						// CHECK THE SECURITY VARIABLES
						if( $encoded_id !== NULL && $salt !== NULL && $token !== NULL) check_salt($id, $salt, $token);
						
						$data['header']   = $this->crsoopm->get_order_of_payment_header(array('rqst_payment_id' => $id));
						$data['details']  = $this->crsoopm->get_order_of_payment_details(array('rqst_payment_id' => $id));
						$data['security'] = $encoded_id.'/'.$salt.'/'.$token;
					break;
				default :
					throw new Exception($this->lang->line('invalid_action'));
			endswitch;
			
			$resources['load_css'] 	 = array(CSS_SELECTIZE);
			$resources['load_js'] 	 = array(JS_SELECTIZE);
			
			$resources['load_init']  = array('CeisRequestSubOrderOfPayment.initModal();');
			
			$html  = $this->load->view('modals/request_payment_order',$data, TRUE);
			
			$html .= $this->load_resources->get_resource($resources, TRUE);
			
			echo $html;
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
	
	public function get_op_list($req_id)
	{
		try{
			$rows		     = array();
			$params	 		 = get_params();
			$request_id 	 = base64_url_decode($req_id);
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns 		 = array('a.rqst_payment_id', 'a.request_id', 'a.op_date', 'a.op_amount', 'a.or_number', 'a.or_date', 'a.or_amount');
			// APPEARS ON TABLE
			$bColumns 		 = array('op_date', 'op_amount', 'or_no', 'or_date', 'or_amount');
	
			$payments 		 = $this->crsoopm->get_order_of_payment_list($aColumns, $bColumns, $params, $request_id);
			$request_details = $this->crsoopm->get_request($request_id);
			
			foreach($payments['aaData'] as $aRow):
				$salt    = gen_salt();
				$token   = in_salt($aRow['rqst_payment_id'], $salt);
				$url     = base64_url_encode($aRow['rqst_payment_id'])."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("order_payment", "'.$url.'")';
				
				$url     = base64_url_encode(ACTION_EDIT)."/".base64_url_encode($aRow['request_id'])."/".$url;
				
				$action  = "<div class='table-actions'>";
				$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_op' onclick=\"modal_op_init('".$url."')\" ></a>";
				
				if(EMPTY($request_details['reference_no']))
					$action .= "<a href='javascript:;' onclick='".$delete_action."' class='delete tooltipped' data-order-payment='bottom' data-tooltip='Delete'  data-delay='50'></a>";
				
				$action .= "</div>"; 
				
				
				$or_no     = ( ! EMPTY($aRow['or_number'])) ? $aRow['or_number'] : '';
				$or_date   = ( ! EMPTY($aRow['or_date']) && $aRow['or_date'] != DEFAULT_DATE ) ? $aRow['or_date'] : '';
				$or_amount = ( ! EMPTY($aRow['or_amount']) && $aRow['or_amount'] != '0.00') ? decimal_format($aRow['or_amount']) : '';
			
				$rows[]    = array($aRow['op_date'], $aRow['op_amount'],  $or_no, $or_date, $or_amount, $action);
			endforeach;
				
			$payments['aaData'] = $rows;

		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
					
		echo json_encode($payments);
	}
	

	public function save_order_payment()
	{
		try
		{	
			$flag 	= 0;
			$msg  	= '';
			$url    = '';
			$params = get_params();
			
			$this->_validate($params);
			
			CEIS_Model::beginTransaction();
			
			$audit_table[]	= CEIS_MODEL::tbl_request_payments;
			$audit_table[]	= CEIS_MODEL::tbl_request_payment_collections;
			$audit_schema[]	= DB_CEIS;
			$audit_schema[]	= DB_CEIS;
		
			if( ! EMPTY($params['rqst_payment_id'])):
				$audit_action[]	= AUDIT_UPDATE;
				$audit_action[]	= AUDIT_UPDATE;
				$op_header	    = $this->crsoopm->get_order_of_payment_header(array('rqst_payment_id' => $params['rqst_payment_id']));
				$prev_detail[]  = array($op_header);
				$prev_detail[]  = $this->crsoopm->get_order_of_payment_details(array('rqst_payment_id' => $params['rqst_payment_id']));
			
				$this->crsoopm->update_order_of_payment($params);
				
				if( ! ISSET($params['or_no'])):
					$this->crsoopm->delete_order_of_payment_details($params['rqst_payment_id']);
					
					$this->crsoopm->insert_order_of_payment_details($params);
				endif;
				
				$activity	= "Request order of payment has been updated.";
			else:
				$audit_action[]	 = AUDIT_INSERT;
				$audit_action[]	 = AUDIT_INSERT;
				$prev_detail[]   = array();
				$prev_detail[]   = array();
				
				$request_details = $this->crtm->get_request_details($params['request_id']);
				
				$params['request_type_id'] = $request_details['request_type_id'];
				$params['rqst_payment_id'] = $this->crsoopm->insert_order_of_payment($params);
				
				$this->crsoopm->insert_order_of_payment_details($params);
				
				$activity	= "Request order of payment has been added.";
			endif;
			
			$op_header  = $this->crsoopm->get_order_of_payment_header(array('rqst_payment_id' => $params['rqst_payment_id']));
			$op_details = $this->crsoopm->get_order_of_payment_details(array('rqst_payment_id' => $params['rqst_payment_id']));
			
			$url 		= PROJECT_CEIS.'/ceis_request_sub_order_of_payment/get_op_list/'.base64_url_encode($op_header['request_id']);
			
			$curr_detail[]		= array($op_header);
			$curr_detail[]		= $op_details;

			$this->audit_trail->log_audit_trail(
					$activity,
					$this->module,
					$prev_detail,
					$curr_detail,
					$audit_action,
					$audit_table,
					$audit_schema
			);
			
			CEIS_Model::commit();
			
			$flag = 1;
			$msg  = sprintf($this->lang->line('data_spec_saved'), 'Order of Payment');
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
			
			$this->rlog_error($e);
			$msg = $this->lang->line('err_internal_server');
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
				"flag" => $flag,
				"msg"  => $msg,
				"url"  => $url
		);
		
		echo json_encode($info);
	}
	
	public function delete_op()
	{
		try 
		{
			$flag  	     = 0;
			$msg    	 = '';
			$params 	 = get_params();
			$tokens 	 = explode('/', $params['param_1']);
			$id 		 = base64_url_decode($tokens[0]); 
			
			check_salt($id, $tokens[1], $tokens[2]);
			
			CEIS_Model::beginTransaction();
			
			$audit_table	 = array(CEIS_MODEL::tbl_request_payments, CEIS_MODEL::tbl_request_payment_collections);
			$audit_schema	 = array(DB_CEIS, DB_CEIS);
			$payment_details = $this->crsoopm->get_order_of_payment_header(array('rqst_payment_id' => $id));
			
			$encoded_rid     = base64_url_encode($payment_details['request_id']);
			
			$prev_detail[]   = array($payment_details);
			$prev_detail[]   = $this->crsoopm->get_order_of_payment_details(array('rqst_payment_id' => $id));
			$audit_action    = array(AUDIT_DELETE, AUDIT_DELETE);
			
			$activity	= "Request order of payment has been deleted.";
			
			$this->crsoopm->delete_order_of_payment_details($id);
			$this->crsoopm->delete_order_of_payment($id);

			$curr_detail  = array(array(), array());
			
			$this->audit_trail->log_audit_trail(
					$activity,
					$this->module,
					$prev_detail,
					$curr_detail,
					$audit_action,
					$audit_table,
					$audit_schema
					);
				
			CEIS_Model::commit();
			
			$flag  = 1;
			$msg   = sprintf($this->lang->line('data_spec_deleted'), 'Order of Payment');
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
				
			$this->rlog_error($e);
			
			$msg = $this->lang->line('err_internal_server');
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();
				
			$msg = $this->rlog_error($e, TRUE);
		}
		
		echo json_encode( array('flag' => $flag, 'msg' => $msg, 'reload' => 'datatable', 'table_id' => 'table_order_of_payment', 'path' => PROJECT_CEIS.'/ceis_request_sub_order_of_payment/get_op_list/'.$encoded_rid) );
	}
	
	private function _validate(&$params)
	{
		if( ! ISSET($params['or_no']))
		{
			if( ! ISSET($params['op_date']) || EMPTY($params['op_date'])):
				throw new Exception(sprintf($this->lang->line('is_required'), 'Order of Payment Date'));
			endif;
			
			if( ! ISSET($params['amount']) || EMPTY($params['amount'])):
				throw new Exception(sprintf($this->lang->line('is_required'), 'Amount'));
			endif;
			
			if( ! ISSET($params['collection']) || EMPTY($params['collection'])):
				throw new Exception(sprintf($this->lang->line('is_required'), 'Collection'));
			endif;
		}
		
		$collection_values = array_count_values($params['collection']);
		
		foreach($collection_values as $key => $value):
			if($value > 1) throw new Exception(sprintf($this->lang->line('err_duplicate_data_spec'), 'Collection'));
		endforeach;
		
		foreach($params['amount'] as $key => $value):
			if( ! is_valid_number($value)) throw new Exception(sprintf($this->lang->line('invalid_data'), 'Amount'));
		endforeach;
		
		$params['total_amount'] = array_sum($params['amount']);
		$params['user_id']		= $this->user_id;
		
		$tokens = explode('/', $params['request_id']);
		
		$params['request_id'] = base64_url_decode($tokens[0]);
		$params['salt'] 	  = $tokens[1];
		$params['token'] 	  = $tokens[2];
		
		check_salt($params['request_id'], $params['salt'], $params['token']);
		
		if( ! EMPTY($params['security'])):
			$tokens = explode('/', $params['security']);
		
			$rqst_payment_id = base64_url_decode($tokens[0]);
			
			check_salt($rqst_payment_id, $tokens[1], $tokens[2]);
			
			$params['rqst_payment_id'] = $rqst_payment_id;
		endif;
	}
}

/* End of file Ceis_dashboard.php */
/* Location: ./application/modules/ceis/controllers/Ceis_dashboard.php */