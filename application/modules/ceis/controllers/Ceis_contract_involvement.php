<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_contract_involvement extends CEIS_Controller {
	
	private $module = MODULE_CEIS_CONTRACT_INVOLVEMENTS;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('contract_involvement_model', 'contract_involvement');
	}
	
	public function index()
	{	
		try{
			$data = $resources = $modal = array();

			$modal = array(
				'modal_contract_involvement' => array(
					'controller'	=> __CLASS__,
					'module'		=> PROJECT_CEIS
				)
			);
			
			$resources['load_css'] 		= array(CSS_DATATABLE);
			$resources['load_js'] 		= array(JS_DATATABLE);
			$resources['load_modal'] 	= $modal;
			$resources['datatable'] 	= array();
			$resources['datatable'][] 	= array('table_id' => 'contract_involvement_table', 'path' => PROJECT_CEIS.'/ceis_contract_involvement/get_contract_involvement_list');
			
			
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}
		
		$this->template->load('contract_involvement', $data, $resources);
	}
	
	
	public function get_contract_involvement_list()
	{
		try{
			$params = get_params();
			
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array(
				"a.involvement_code",
				"a.involvement",
				"a.created_by",
				"a.created_date",
				"a.modified_by",
				"a.modified_date",
				'CONCAT(b.last_name, \',  \', b.first_name) as created_name',
				'CONCAT(c.last_name, \',  \', c.first_name) as modified_name');

			// APPEARS ON TABLE
			$bColumns = array(
				"involvement_code",
				"created_name",
				"created_date",
				"modified_name",
				"modified_date");
			
			$contract_involvements 	= $this->contract_involvement->get_contract_involvement_list($aColumns, $bColumns, $params);
			$iTotal 				= $this->contract_involvement->total_length(); // TOTAL COUNT OF RECORDS
			$iFilteredTotal 		= $this->contract_involvement->filtered_length($aColumns, $bColumns, $params); // TOTAL COUNT OF RECORDS PER PAGE

			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
			
			foreach ($contract_involvements as $data):

				// PRIMARY  KEY
				$involvement_code	= $data["involvement_code"];
			
				// CONSTRUCT SECURITY VARIABLES
				$hash_id 			= $this->hash($involvement_code);
				$encoded_id 		= base64_url_encode($hash_id);
				$salt 				= gen_salt();			
				$token 				= in_salt($encoded_id, $salt);			
				$url 				= $encoded_id."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("contract involvement", "'.$url.'")';

				$action = "<div class='table-actions'>";

				if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
					$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-contract_involvement='bottom' data-delay='50' data-modal='modal_contract_involvement' onclick=\"modal_init('".$url."')\"></a>";
				endif;

				if (($this->permission->check_permission(MODULE_ROLE, ACTION_DELETE))) : 	  
					$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-contract_involvement='bottom' data-delay='50'></a>";
				endif;
				$action .= '</div>';
				
				$row 	= array();
				$row[] 	= $data['involvement_code'];
				$row[] 	= $data['created_name'];
				$row[] 	= $data['created_date'];
				$row[] 	= $data['modified_name'];
				$row[] 	= $data['modified_date'];
				$row[] 	= $action;
				$output['aaData'][] = $row;
			endforeach;
		}

		catch(PDOException $e){
			
			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		echo json_encode( $output );
	}
	
	public function modal($encoded_id, $salt, $token)
	{
		try{
			$data 		= array();
			$resources 	= array();

			// CHECK IF THE ACTION IS UPDATE/INSERT
			if(!EMPTY($encoded_id)){
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);
				$key 			= $this->get_hash_key('involvement_code');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->contract_involvement->get_specific_contract_invovement($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['involvement_code'] 	= $info['involvement_code'];
				$data['involvement'] 		= $info['involvement'];
				$data['created_by'] 		= $info['created_by'];
				$data['created_date'] 		= $info['created_date'];
				$data["readonly_value"]		= "readonly";

				// CREATE NEW SECURITY VARIABLES
				$involvement_code			= $info['involvement_code'];
				$hash_id 					= $this->hash($involvement_code);			
			}

			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

		$this->load->view('modals/contract_involvement',$data);
	}
	
	public function process()
	{
		try
		{
			$flag 				= 0;
			$params 			= get_params();
			
			// SERVER VALIDATION
			$this->_validate($params);
	
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 				= $this->get_hash_key('involvement_code');
			$where				= array();
			$where[$key]		= $params['hash_id'];

			$info 				= $this->contract_involvement->get_specific_contract_invovement($where);
			$involvement_code	= $info['involvement_code'];
			

			CEIS_Model::beginTransaction();

			$involvement 		= $params['involvement_name'];
			
			// IF EMPTY MEANS INSERT/SAVE RECORD
			IF(EMPTY($involvement_code)){
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= CEIS_Model::tbl_contract_involvement;
				$audit_schema[]	= DB_CEIS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				// SAVES THE RESOLUTIONS RECORD
				$this->contract_involvement->insert_contract_involvement($params);
				
				$activity		= "%s has been added in contract involvement.";
				$activity 		= sprintf($activity, $involvement);

				$msg  = $this->lang->line('data_saved');
			}

			else{

				$params['involvement_code']	= $involvement_code;

				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= CEIS_Model::tbl_contract_involvement;
				$audit_schema[]	= DB_CEIS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				// SAVES THE PROJECT TYPES RECORD
				$this->contract_involvement->update_contract_involvement($params);

				$activity	= "%s has been updated in contract involvement.";
				$activity 	= sprintf($activity, $involvement);

				$msg  = $this->lang->line('data_updated');
			}


			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table, 
				$audit_schema
			);
			
			CEIS_Model::commit();

			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
			
			$this->rlog_error($e);

			$mg = $this->get_user_message($e);
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			CEIS_Model::rollback();
		}
		
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	
	}
	
	private function _validate(&$params)
	{
		try
		{
			
			$this->_validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields = array();
			$fields['involvement_code']			= 'Involvemnet Code';
			$fields['involvement_name']			= 'Involvement Name';

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
		catch(PDOException $e){
			throw $e;
		}
	}
	
	private function _validate_input($params)
	{
		try
		{
			$validation['involvement_code'] = array(
					'data_type' => 'string',
					'name'		=> 'Involvement Code',
					'max_len'	=> 100
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
		catch(PDOException $e){
			throw $e;
		}
	}

	public function delete_contract_involvement($params)
	{
		try 
		{
			$flag				= 0;
			$msg				= "Error";
			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->_validate_security($params);

			$key 			= $this->get_hash_key('involvement_code');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->contract_involvement->get_specific_contract_invovement($where);
			
			$involvement_code = $info['involvement_code'];
			
			if(EMPTY($info['involvement_code']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			CEIS_Model::beginTransaction();
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= CEIS_Model::tbl_contract_involvement;
			$audit_schema[]	= DB_CEIS;			
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL

			$this->contract_involvement->delete_contract_involvement($info['involvement_code']);

			// SET AS AN ARRAY
			$curr_detail[]	= array();
			
			// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $involvement_code);
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			CEIS_Model::commit();

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			CEIS_Model::rollback();

			$this->rlog_error($e);

			$msg	= $this->get_user_message($e);
		}
		catch(Exception $e)
		{	
			CEIS_Model::rollback();

			$this->rlog_error($e);
						
			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
	}
}

/* End of file Ceis_contract_involvement.php */
/* Location: ./application/modules/ceis/controllers/Ceis_contract_involvement.php */