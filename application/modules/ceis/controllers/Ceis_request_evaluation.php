<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ceis_request_evaluation extends Ceis_Controller
{
	private $module = MODULE_CEIS_REQUESTS;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('ceis_request_evaluation_model', 'crem');
	
	}
	
	public function generate_report()
	{
		try
		{
			$status = 0;
			$msg	= '';
			$modal  = '';
			$data   = array();
			$params = get_params();
			
			$rids   = explode('/', $params['request_id']);
			
			$request_id = base64_url_decode($rids[0]);
			
			check_salt($request_id, $rids[1], $rids[2]);

			$request_details     = $this->crem->get_request_details($request_id);
			$evaluation_details   = $this->crem->get_request_evaluation(array('request_id' => $request_id));
			$contractor_details   = $this->crem->get_request_contractor_details(array('request_id' => $request_id));
			$financial_details    = $this->crem->get_financial_data($request_id);
			$contractor_class     = $this->crem->get_request_contractor_class($request_id);		
			//$contractor_contracts = $this->cream->get_request_contractor_contracts($request_id);
			
			$work_volume_years   	= ($$request_details['request_type_id'] == REQUEST_TYPE_NEW) ? 3 : 2;
			$work_volume			= $this->crem->get_work_volume($request_id, $work_volume_years);
			

			$data['request_details'] 	= $request_details;
			$data['evaluation_details'] = $evaluation_details;
			$data['contractor_details'] = $contractor_details;
			$data['contractor_class']	= $contractor_class;
			$data['financial_details']  = $financial_details;
			$data['work_volume']		= decimal_format($work_volume);
			//$data['auth_capital_stock'] = $this->crem->get_request_total_capital_stock($request_id);
			$data['staffs']  		    = $this->crem->get_request_stockholders_directors($request_id);
			$data['key_staffs']			= $this->crem->get_request_key_staffs($request_id);
			$data['request_checklist']	= $this->crem->get_request_checklist($request_details['request_type_id'], $request_id);
			
			
			//print_r($data['staffs']);
			$html   = $this->load->view('reports/report_evaluation', $data,TRUE);
			$html  .= $this->set_report_footer();
			
			$pdf = $this->pdf( 'POCB_FORM_01-E.pdf', $html, TRUE, 'S' );
			
			$data['pdf'] 			= $pdf;

			$modal = $this->load->view('modals/report_container_modal', $data, TRUE);	
			
			$status = 1;
			$msg	= 'Succesful';
		}
		catch(PDOException $e)
		{		
			$this->rlog_error($e);
			$msg = $this->lang->line('err_internal_server');
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			$msg = $this->lang->line('err_internal_server');
		}
		
		$ajaxData 	= array(
				'status' => $status,
				'msg'	 => $msg,
			 	'modal'  => $modal
		);
		
		echo  json_encode($ajaxData);
	}
	
	public function save_evaluation()
	{
		try
		{	
			$flag 	= 0;
			$msg  	= '';
			$params = get_params();
			
			$this->_validate($params);

			$evaluation_details = $this->crem->get_request_evaluation(array('request_id' => $params['request_id']));
			
			CEIS_Model::beginTransaction();
			
			$audit_table[]	= CEIS_MODEL::tbl_request_evaluation;
			$audit_schema[]	= DB_CEIS;
			$prev_detail[]	= $evaluation_details;
			
			if( ! EMPTY($evaluation_details)):
				$audit_action[]	= AUDIT_UPDATE;
			
				$this->crem->update_request_evaluation($params);
				
				$activity	= "Request evaluation has been updated.";
			else:
				$audit_action[]	= AUDIT_INSERT;
			
				$this->crem->insert_request_evaluation($params);
				
				$activity	= "Request evaluation has been added.";
			endif;
			
			$evaluation_details = $this->crem->get_request_evaluation(array('request_id' => $params['request_id']));
			$curr_detail[]		= array($evaluation_details);
			
			$this->audit_trail->log_audit_trail(
					$activity,
					$this->module,
					$prev_detail,
					$curr_detail,
					$audit_action,
					$audit_table,
					$audit_schema
			);
			
			CEIS_Model::commit();
			
			$flag = 1;
			$msg  = sprintf($this->lang->line('data_spec_saved'), 'Evaluation');
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
			
			$this->rlog_error($e);
			$msg = $this->lang->line('err_internal_server');
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
				"flag" => $flag,
				"msg" => $msg
		);
		
		echo json_encode($info);
	}
	

	
	private function _validate(&$params)
	{
		
		if( ! ISSET($params['legal_requirements']) || EMPTY($params['legal_requirements'])):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Legal Requirements'));
		elseif( ! in_array($params['legal_requirements'], $this->complete_flags)):
			throw new Exception(sprintf($this->lang->line('invalid_data'), 'Legal Requirements'));
		endif;
		
		if( ! ISSET($params['construction_contract']) ):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Construction Contracts'));
		elseif( filter_var($params['construction_contract'], FILTER_VALIDATE_INT) === FALSE):
			throw new Exception(sprintf($this->lang->line('invalid_data'), 'Construction Contracts'));
		endif;
		
		if( ! ISSET($params['completed_contracts']) ):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Completed Contracts'));
		elseif( filter_var($params['completed_contracts'], FILTER_VALIDATE_INT) === FALSE):
			throw new Exception(sprintf($this->lang->line('invalid_data'), 'Completed Contracts'));
		endif;
		
		if( ! ISSET($params['rescinded_contracts']) ):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Rescinded Contracts'));
		elseif( filter_var($params['rescinded_contracts'], FILTER_VALIDATE_INT) === FALSE):
			throw new Exception(sprintf($this->lang->line('invalid_data'), 'Rescinded Contracts'));
		endif;
		
		if( ! ISSET($params['work_experience']) || EMPTY($params['work_experience'])):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Work Experience'));
		elseif( ! in_array($params['work_experience'], $this->complete_flags)):
			throw new Exception(sprintf($this->lang->line('invalid_data'), 'Work Experience'));
		endif;
		
		if( ! ISSET($params['no_staff']) || EMPTY($params['no_staff'])):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Number of Staff'));
		elseif( ! filter_var($params['no_staff'], FILTER_VALIDATE_INT) ):
			throw new Exception(sprintf($this->lang->line('invalid_data'), 'Number of Staff'));
		endif;
		
		if( ! ISSET($params['staff_requirements']) || EMPTY($params['staff_requirements'])):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Staff Requirements'));
		elseif( ! in_array($params['staff_requirements'], $this->complete_flags)):
			throw new Exception(sprintf($this->lang->line('invalid_data'), 'Staff Requirements'));
		endif;
		
		if( ! ISSET($params['application_result']) || EMPTY($params['application_result'])):
			throw new Exception(sprintf($this->lang->line('is_required'), 'Application Requirements'));
		elseif( ! in_array($params['application_result'], $this->approve_flags)):
			throw new Exception(sprintf($this->lang->line('invalid_data'), 'Application Requirements'));
		endif;
		
		$tokens = explode('/', $params['request_id']);
		
		$params['request_id'] = base64_url_decode($tokens[0]);
		$params['salt'] 	  = $tokens[1];
		$params['token'] 	  = $tokens[2];
		
		check_salt($params['request_id'], $params['salt'], $params['token']);
	}
}

/* End of file Ceis_dashboard.php */
/* Location: ./application/modules/ceis/controllers/Ceis_dashboard.php */