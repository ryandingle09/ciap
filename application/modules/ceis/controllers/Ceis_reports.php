<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ceis_reports extends CEIS_Controller
{

	private $module = MODULE_CEIS_REPORTS;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('reports_model','reports');
	}

	public function index(){
		try
		{	
			$data 		= array();
			$resources 	= array();
			$modal 		= array();

			$modal = array(
				'modal_report' => array(
					'controller'	=> __CLASS__,
					'module'		=> PROJECT_CEIS,
					'multiple'		=> true,
					'height'		=> '450px;',
					'ajax'			=> true,
					'size'			=> 'xl'
				)
			);

			$data["contractors"]		= $this->reports->get_contractors();
			$resources['load_css'] 		= array( CSS_SELECTIZE, CSS_MODAL_COMPONENT);
			$resources['load_js'] 		= array( JS_SELECTIZE,JS_MODAL_CLASSIE,JS_MODAL_EFFECTS, 'modules/ceis/'.'ceis_reports');
			$resources['load_modal'] 	= $modal;
			

			$this->template->load('reports', $data, $resources);		
		}
		catch(PDOException $e){
			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}
	}
	
	//SKILLS SUMMARY
	public function generate_report(){
		try{
			// $default 				= ini_get('max_execution_time');
			// set_time_limit(1000);

			$success 				= 0;
			$msg 	 				= "";
			$data 	 				= array();
			$link 	 				= "";
			$val_msg 				= array();
			$modal_data 			= array();
			$report 				= '';
			$modal 					= '';
			$html 					= '';

			$params     			= get_params();
			$data['report']			= $params["report"];
			$report 				= $params["report"];
			$data['generate']		= $params['generate'];
			$data['contractor_list']= $params['contractor_list'];
			$data['quarter']		= $params['quarter'];
			$data['year']			= $params['year'];
			$year 					= $data['year'];

			$info 						= $this->reports->get_specific_contractor($data['contractor_list']);
			$data['contractor_name'] 	= $info['contractor_name']; 
			$contractor_id 				= $info['contractor_id']; 
			
			$skill_summary 			= $this->reports->get_skill_summary_list($year, $contractor_id);

			$data['skill_summary'] 	= $skill_summary;


			$filename 				= 'ceis_reports_'.date("Y_m_d");

			$reports 				= array(
				'1' 				=> 'report_skills_summary',
				'2' 				=> 'report_skills_summary'
			);

			$modals 				= array(
				'1' 				=> 'report_container_modal_menu',
				'2'					=> 'report_container_modal'
			);

			$modal 					= $modals[ $data['report'] ];
			$report 				= $reports[ $data['report'] ];

			$html  .= $this->load->view('reports/'.$report, $data,TRUE);

			$html.= $this->set_report_footer();

			if($data['report'] == 1){	
				$data['generate'] 	= 'PDF';
				$modal_page			= "report_container_modal_menu";
				$filename 			= $filename.'.pdf';
				$pdf 				= $this->pdf( $filename, $html, FALSE, 'S' );

				$modal_data['pdf'] 			= $pdf;
				$modal_data['report_title']	= $data['report'];

				$modal_data['quarter'] 			= $data['quarter'];
				$modal_data['contractor_list'] 	= $data['contractor_list'];
				$modal_data['year'] 			= $data['year'];

				$modal 		= $this->load->view('modals/' . $modal_page, $modal_data, TRUE);	
				$success 	= 1;
			}
			else{
				$modal_page = "report_container_modal";

				if( $data['generate'] == 'PDF' )
				{
					$filename 	= $filename.'.pdf';
					$pdf 		= $this->pdf( $filename, $html, FALSE, 'S' );

					$modal_data['pdf'] 			= $pdf;
					$modal_data['report_title']	= $data['report'];

					$modal 		= $this->load->view('modals/' . $modal_page, $modal_data, TRUE);	
					$success 	= 1;
				}
				else if( $data['generate'] == 'Excel' )
				{	
					$filename 	= $filename.'.xls';
					$this->convert_excel( $html, $filename , '');
					$success 	= 1;
				}	
			}
			
		// set_time_limit($default);

		}catch(PDOException $e){

			$msg 	= $e->getMessage().' -Line '.$e->getLine().' - '.__FUNCTION__;

		}catch(Exception $e){

			$msg 	= $e->getMessage().' -Line '.$e->getLine().' - '.__FUNCTION__;

			if(!EMPTY($val_msg['flag'])) $msg = $e->getMessage();

		}

		echo $msg;

		if( $data['generate'] == 'PDF' )
		{
	
			$ajaxData 	= array(

					'success'	=> $success,
					'msg'		=> $msg,
					'modal' 	=> $modal,
					'data' 		=> $data
			);

			echo  json_encode($ajaxData);

		}
	}

	public function memorandum($year,$quarter=NULL,$contractor_list=NULL){

		try
		{
			$success 				= 0;
			$data 	 				= array();
			$modal_data 			= array();
			$modal 					= '';

			$params     			= get_params();

			$data['report']			= $params["report"];
			$data['generate']		= $params['generate'];
			
			$data['contractor_list']= $contractor_list;
			$data['quarter'] 		= $quarter;
			$data['year'] 			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.pdf';

			//FOR FOREIGN EXCHANGE REMITTANCE
			$submitted_foreign_exchange_info					= $this->reports->get_submitted_foreign_exchange($year,$quarter);
			$data['foreign_remittance_count'] 					= $submitted_foreign_exchange_info[0]['companies_remitted_count'];
			$data['total_remitted_earning'] 					= $submitted_foreign_exchange_info[0]['total_remitted_earning'];

			$prev_submitted_foreign_exchange_info				= $this->reports->get_submitted_foreign_exchange($year-1,$quarter);
			$data['prev_total_remitted_earning'] 				= $prev_submitted_foreign_exchange_info[0]['total_remitted_earning'];

			$total_foreign_exchange_info						= $this->reports->get_total_foreign_exchange($year,$quarter);
			$data['all_companies_remitted_count'] 				= $total_foreign_exchange_info[0]['all_companies_remitted_count'];
			$data['all_total_earning'] 							= $total_foreign_exchange_info[0]['all_total_earning'];
			//------------------------------

			//FOR NEW CONTRACTS WON
			$projects_new_contracts_memorandum_info				= $this->reports->get_project_new_contract_memorandum($year,$quarter);
			$data['tot_project_count'] 							= $projects_new_contracts_memorandum_info[0]['project_contract_count'];

			$service_new_contracts_memorandum_info				= $this->reports->get_service_new_contract_memorandum($year,$quarter);
			$data['tot_service_count'] 							= $service_new_contracts_memorandum_info[0]['service_contract_count'];

			$service_new_contracts_reg_memorandum_info			= $this->reports->get_service_new_contract_reg_memorandum($year,$quarter);
			$data['tot_reg_service_count'] 						= $service_new_contracts_reg_memorandum_info[0]['reg_service_contract_count'];
			//---------------------

			//MANPOWER ON-SITE
			$workers_count_memorandum_info						= $this->reports->get_workers_count_memorandum($year,$quarter);
			$data['tot_worker_count']							= $workers_count_memorandum_info[0]['worker'];

			$prev_workers_count_memorandum_info					= $this->reports->get_workers_count_memorandum($year-1,$quarter);
			$data['tot_prev_workers_count_memorandum_info']		= $prev_workers_count_memorandum_info[0]['worker'];

			$sum_country_onsite_info							= $this->reports->get_sum_country_onsite($year,$quarter);
			$data['tot_country_onsite']							= $sum_country_onsite_info[0]['grand_total'];

			$ind_country_onsite_info							= $this->reports->get_ind_country_onsite($year,$quarter);
			$data['ind_country_onsite_info']					= $ind_country_onsite_info;
			//-----------------		
			
			//MANPOWER DEPLOYED
			$manpower_deployed_memorandum_info					= $this->reports->get_manpower_deployed_memorandum($year,$quarter);
			$data['tot_total_workers'] 							= $manpower_deployed_memorandum_info[0]['total_workers'];

			$prev_manpower_deployed_memorandum_info				= $this->reports->get_manpower_deployed_memorandum($year-1,$quarter);
			$data['prev_tot_total_workers'] 					= $prev_manpower_deployed_memorandum_info[0]['total_workers'];
			//-----------------

			//OUTSTANDING OVERSEAS CONTRACTS
			$companies_outstanding_count_info					= $this->reports->get_companies_outstanding_count($year,$quarter);
			$data['tot_companies_outstanding_count'] 			= $companies_outstanding_count_info[0]['project_count'];

			$project_outstanding_count_info						= $this->reports->get_project_outstanding_count($year,$quarter);
			$data['tot_project_outstanding_count'] 				= $project_outstanding_count_info[0]['project_company_count'];

			$service_outstanding_count_info						= $this->reports->get_service_outstanding_count($year,$quarter);
			$data['tot_service_outstanding_count'] 				= $service_outstanding_count_info[0]['service_company_count'];

			$project_consultant_outstanding_count_info			= $this->reports->get_project_consultant_outstanding_count($year,$quarter);
			$data['tot_project_consultant_outstanding_count'] 	= $project_consultant_outstanding_count_info[0]['project_consultant_count'];

			$service_consultant_outstanding_count_info			= $this->reports->get_service_consultant_outstanding_count($year,$quarter);
			$data['tot_service_consultant_outstanding_count'] 	= $service_consultant_outstanding_count_info[0]['service_consultant_count'];

			$prev_service_consultant_outstanding_count_info		= $this->reports->get_service_consultant_outstanding_count($year-1,$quarter);
			$data['tot_prev_service_consultant_outstanding_count'] = $prev_service_consultant_outstanding_count_info[0]['service_consultant_count'];

			$prev_service_outstanding_count_info				= $this->reports->get_service_outstanding_count($year-1,$quarter);
			$data['tot_prev_service_outstanding_count'] 		= $prev_service_outstanding_count_info[0]['service_company_count'];

			$get_project_ongoing_count_info						= $this->reports->get_project_ongoing_count($year,$quarter);
			$data['total_project_ongoing'] 						= $get_project_ongoing_count_info[0]['total_sum'];

			//COMPLETED CONTRACTS
			$manpower_completed_contracts_memorandum_info		= $this->reports->get_manpower_completed_contract_memorandum($year,$quarter);
			$data['tot_completed_service_count'] 				= $manpower_completed_contracts_memorandum_info[0]['completed_contract'];

			$project_completed_contract_memorandum_info			= $this->reports->get_project_completed_contract_memorandum($year-1,$quarter);
			$data['tot_completed_project_count'] 				= $project_completed_contract_memorandum_info[0]['completed_contract'];
			//-------------------

			/*$services_new_contracts_info							= $this->reports->get_services_new_contracts($year,$quarter);
			$data['services_new_contracts_info'] 					= $services_new_contracts_info;*/

			$html.= $this->load->view('reports/ceis_reports_pocb_memorandum',$data,TRUE);

			// $html.= $this->set_report_footer();
			
			//TRUE = PORTAIT, FALSE = LANDSCAPE
			$pdf = $this->pdf( $filename, $html, TRUE, 'S' );


			$modal_data['pdf'] 				= $pdf;
			$modal_data['report_title']		= "POCB Memorandum";

			$this->load->view('modals/modal_report',$modal_data);	

		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

	}

	public function memorandum_excel($year,$quarter=NULL,$contractor_list=NULL){
		try{
			$success 				= 0;
			$data 	 				= array();
			$modal_data 			= array();

			$params     			= get_params();
			$data['report']			= $params["report"];
			$data['generate']		= $params['generate'];
			/*$data['contractor_list']= $params['contractor_list'];
			$data['quarter']		= $params['quarter'];*/

			$data['contractor_list']= $contractor_list;
			$data['quarter'] 		= $quarter;
			$data['year'] 			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.xls';

			//FOR FOREIGN EXCHANGE REMITTANCE
			$submitted_foreign_exchange_info					= $this->reports->get_submitted_foreign_exchange($year,$quarter);
			$data['foreign_remittance_count'] 					= $submitted_foreign_exchange_info[0]['companies_remitted_count'];
			$data['total_remitted_earning'] 					= $submitted_foreign_exchange_info[0]['total_remitted_earning'];

			$prev_submitted_foreign_exchange_info				= $this->reports->get_submitted_foreign_exchange($year-1,$quarter);
			$data['prev_total_remitted_earning'] 				= $prev_submitted_foreign_exchange_info[0]['total_remitted_earning'];

			$total_foreign_exchange_info						= $this->reports->get_total_foreign_exchange($year,$quarter);
			$data['all_companies_remitted_count'] 				= $total_foreign_exchange_info[0]['all_companies_remitted_count'];
			$data['all_total_earning'] 							= $total_foreign_exchange_info[0]['all_total_earning'];
			//------------------------------

			//FOR NEW CONTRACTS WON
			$projects_new_contracts_memorandum_info				= $this->reports->get_project_new_contract_memorandum($year,$quarter);
			$data['tot_project_count'] 							= $projects_new_contracts_memorandum_info[0]['project_contract_count'];

			$service_new_contracts_memorandum_info				= $this->reports->get_service_new_contract_memorandum($year,$quarter);
			$data['tot_service_count'] 							= $service_new_contracts_memorandum_info[0]['service_contract_count'];

			$service_new_contracts_reg_memorandum_info			= $this->reports->get_service_new_contract_reg_memorandum($year,$quarter);
			$data['tot_reg_service_count'] 						= $service_new_contracts_reg_memorandum_info[0]['reg_service_contract_count'];
			//---------------------

			//MANPOWER ON-SITE
			$workers_count_memorandum_info						= $this->reports->get_workers_count_memorandum($year,$quarter);
			$data['tot_worker_count']							= $workers_count_memorandum_info[0]['worker'];

			$prev_workers_count_memorandum_info					= $this->reports->get_workers_count_memorandum($year-1,$quarter);
			$data['tot_prev_workers_count_memorandum_info']		= $prev_workers_count_memorandum_info[0]['worker'];

			$sum_country_onsite_info							= $this->reports->get_sum_country_onsite($year,$quarter);
			$data['tot_country_onsite']							= $sum_country_onsite_info[0]['grand_total'];

			$ind_country_onsite_info							= $this->reports->get_ind_country_onsite($year,$quarter);
			$data['ind_country_onsite_info']					= $ind_country_onsite_info;
			//-----------------		
			
			//MANPOWER DEPLOYED
			$manpower_deployed_memorandum_info					= $this->reports->get_manpower_deployed_memorandum($year,$quarter);
			$data['tot_total_workers'] 							= $manpower_deployed_memorandum_info[0]['total_workers'];

			$prev_manpower_deployed_memorandum_info				= $this->reports->get_manpower_deployed_memorandum($year-1,$quarter);
			$data['prev_tot_total_workers'] 					= $prev_manpower_deployed_memorandum_info[0]['total_workers'];
			//-----------------

			//OUTSTANDING OVERSEAS CONTRACTS
			$companies_outstanding_count_info					= $this->reports->get_companies_outstanding_count($year,$quarter);
			$data['tot_companies_outstanding_count'] 			= $companies_outstanding_count_info[0]['project_count'];

			$project_outstanding_count_info						= $this->reports->get_project_outstanding_count($year,$quarter);
			$data['tot_project_outstanding_count'] 				= $project_outstanding_count_info[0]['project_company_count'];

			$service_outstanding_count_info						= $this->reports->get_service_outstanding_count($year,$quarter);
			$data['tot_service_outstanding_count'] 				= $service_outstanding_count_info[0]['service_company_count'];

			$project_consultant_outstanding_count_info			= $this->reports->get_project_consultant_outstanding_count($year,$quarter);
			$data['tot_project_consultant_outstanding_count'] 	= $project_consultant_outstanding_count_info[0]['project_consultant_count'];

			$service_consultant_outstanding_count_info			= $this->reports->get_service_consultant_outstanding_count($year,$quarter);
			$data['tot_service_consultant_outstanding_count'] 	= $service_consultant_outstanding_count_info[0]['service_consultant_count'];

			$prev_service_consultant_outstanding_count_info		= $this->reports->get_service_consultant_outstanding_count($year-1,$quarter);
			$data['tot_prev_service_consultant_outstanding_count'] = $prev_service_consultant_outstanding_count_info[0]['service_consultant_count'];

			$prev_service_outstanding_count_info				= $this->reports->get_service_outstanding_count($year-1,$quarter);
			$data['tot_prev_service_outstanding_count'] 		= $prev_service_outstanding_count_info[0]['service_company_count'];

			$get_project_ongoing_count_info						= $this->reports->get_project_ongoing_count($year,$quarter);
			$data['total_project_ongoing'] 						= $get_project_ongoing_count_info[0]['total_sum'];

			//COMPLETED CONTRACTS
			$manpower_completed_contracts_memorandum_info		= $this->reports->get_manpower_completed_contract_memorandum($year,$quarter);
			$data['tot_completed_service_count'] 				= $manpower_completed_contracts_memorandum_info[0]['completed_contract'];

			$project_completed_contract_memorandum_info			= $this->reports->get_project_completed_contract_memorandum($year-1,$quarter);
			$data['tot_completed_project_count'] 				= $project_completed_contract_memorandum_info[0]['completed_contract'];
			//-------------------

			/*$services_new_contracts_info							= $this->reports->get_services_new_contracts($year,$quarter);
			$data['services_new_contracts_info'] 					= $services_new_contracts_info;*/

			$html.= $this->load->view('reports/ceis_reports_pocb_memorandum',$data,TRUE);

			// $html.= $this->set_report_footer();

			$this->convert_excel( $html, $filename , '');

			$success 	= 1;

		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}
		
	}

	public function reg_companies($year,$quarter=NULL,$contractor_list=NULL){
		
		try{
			$success 				= 0;
			$data 	 				= array();
			$modal_data 			= array();
			$modal 					= '';

			$params     			= get_params();
			$data['report']			= $params["report"];
			$data['generate']		= $params['generate'];
			$data['contractor_list']= $params['contractor_list'];
			$data['quarter']		= $params['quarter'];

			$data['contractor_list']= $contractor_list;
			$data['quarter'] 		= $quarter;
			$data['year'] 			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.pdf';

			// START-- A. No. of POCB Reg Companies -----
			$reg_companies_info				= $this->reports->get_reg_companies($year,$quarter);
			$data['reg_companies_info'] 	= $reg_companies_info;

			$prev_reg_companies_info			= $this->reports->get_reg_companies($year-1,$quarter);
			$data['prev_reg_companies_info'] 	= $prev_reg_companies_info;
			// END---------------------------------------

			// START-- B. No. of Active POCB Reg Companies -----
			$active_companies_info				= $this->reports->get_active_companies($year,$quarter);
			$data['active_companies_info'] 		= $active_companies_info;

			$prev_active_companies_info			= $this->reports->get_active_companies($year-1,$quarter);
			$data['prev_active_companies_info'] = $prev_active_companies_info;
			// END---------------------------------------

			// START-- C. Total Foreign Remittances  -----
			$remittances_companies_info				= $this->reports->get_remittances_companies($year,$quarter);
			$data['remittances_companies_info'] 	= $remittances_companies_info;

			$prev_remittances_companies_info		= $this->reports->get_remittances_companies($year-1,$quarter);
			$data['prev_remittances_companies_info']= $prev_remittances_companies_info;
			// END---------------------------------------

			// START-- D. count project  -----
			$not_yet_started_info					= $this->reports->get_not_started_contract($year,$quarter);
			$data['not_yet_started_info'] 			= $not_yet_started_info;

			$prev_not_yet_started_info				= $this->reports->get_not_started_contract($year-1,$quarter);
			$data['prev_not_yet_started_info']		= $prev_not_yet_started_info;
			// END---------------------------------------

			// START-- D. amount project  -----
			$not_yet_started_amount_info					= $this->reports->get_not_started_amount_project_contract($year,$quarter);
			$data['not_yet_started_amount_info'] 			= $not_yet_started_amount_info;

			$prev_not_yet_started_amount_info				= $this->reports->get_not_started_amount_project_contract($year-1,$quarter);
			$data['prev_not_yet_started_amount_info']		= $prev_not_yet_started_amount_info;
			// END---------------------------------------

			// START-- D. count service  -----
			$not_yet_started_service_info					= $this->reports->get_not_started_service_contract($year,$quarter);
			$data['not_yet_started_service_info'] 			= $not_yet_started_service_info;

			$prev_not_yet_started_service_info				= $this->reports->get_not_started_service_contract($year-1,$quarter);
			$data['prev_not_yet_started_service_info']		= $prev_not_yet_started_service_info;
			// END---------------------------------------

			// START-- D. amount service  -----
			$not_yet_started_amount_service_info					= $this->reports->get_not_started_amount_service_contract($year,$quarter);
			$data['not_yet_started_amount_service_info'] 			= $not_yet_started_amount_service_info;

			$prev_not_yet_started_amount_service_info				= $this->reports->get_not_started_amount_service_contract($year-1,$quarter);
			$data['prev_not_yet_started_amount_service_info']		= $prev_not_yet_started_amount_service_info;
			// END---------------------------------------

			//ONGOING

			// START-- E. count project  -----
			$ongoing_info									= $this->reports->get_ongoing_contract($year,$quarter);
			$data['ongoing_info'] 							= $ongoing_info;

			$prev_ongoing_info								= $this->reports->get_ongoing_contract($year-1,$quarter);
			$data['prev_ongoing_info']						= $prev_ongoing_info;
			// END---------------------------------------

			// START-- E. amount project  -----
			$ongoing_amount_info							= $this->reports->get_ongoing_amount_project_contract($year,$quarter);
			$data['ongoing_amount_info'] 					= $ongoing_amount_info;

			$prev_ongoing_amount_info						= $this->reports->get_ongoing_amount_project_contract($year-1,$quarter);
			$data['prev_ongoing_amount_info']				= $prev_ongoing_amount_info;
			// END---------------------------------------

			// START-- E. count service  -----
			$ongoing_service_info							= $this->reports->get_ongoing_service_contract($year,$quarter);
			$data['ongoing_service_info'] 					= $ongoing_service_info;

			$prev_ongoing_service_info						= $this->reports->get_ongoing_service_contract($year-1,$quarter);
			$data['prev_ongoing_service_info']				= $prev_ongoing_service_info;
			// END---------------------------------------

			// START-- E. amount service  -----
			$ongoing_amount_service_info					= $this->reports->get_ongoing_amount_service_contract($year,$quarter);
			$data['ongoing_amount_service_info'] 			= $ongoing_amount_service_info;

			$prev_ongoing_amount_service_info				= $this->reports->get_ongoing_amount_service_contract($year-1,$quarter);
			$data['prev_ongoing_amount_service_info']		= $prev_ongoing_amount_service_info;
			// END---------------------------------------

			//COMPLETED

			// START-- F. count project  -----
			$completed_info									= $this->reports->get_completed_contract($year,$quarter);
			$data['completed_info'] 						= $completed_info;

			$prev_completed_info							= $this->reports->get_completed_contract($year-1,$quarter);
			$data['prev_completed_info']					= $prev_completed_info;
			// END---------------------------------------

			// START-- F. amount project  -----
			$completed_amount_info							= $this->reports->get_completed_amount_project_contract($year,$quarter);
			$data['completed_amount_info'] 					= $completed_amount_info;

			$prev_completed_amount_info						= $this->reports->get_completed_amount_project_contract($year-1,$quarter);
			$data['prev_completed_amount_info']				= $prev_completed_amount_info;
			// END---------------------------------------

			// START-- F. count service  -----
			$completed_service_info							= $this->reports->get_completed_service_contract($year,$quarter);
			$data['completed_service_info'] 				= $completed_service_info;

			$prev_completed_service_info					= $this->reports->get_completed_service_contract($year-1,$quarter);
			$data['prev_completed_service_info']			= $prev_completed_service_info;
			// END---------------------------------------

			// START-- F. amount service  -----
			$completed_amount_service_info					= $this->reports->get_completed_amount_service_contract($year,$quarter);
			$data['completed_amount_service_info'] 			= $completed_amount_service_info;

			$prev_completed_amount_service_info				= $this->reports->get_completed_amount_service_contract($year-1,$quarter);
			$data['prev_completed_amount_service_info']		= $prev_completed_amount_service_info;

			//TERMINATED

			// START-- G. count project  -----
			$terminated_info								= $this->reports->get_terminated_contract($year,$quarter);
			$data['terminated_info'] 						= $terminated_info;

			$prev_terminated_info							= $this->reports->get_terminated_contract($year-1,$quarter);
			$data['prev_terminated_info']					= $prev_terminated_info;
			// END---------------------------------------

			// START-- G. amount project  -----
			$terminated_amount_info							= $this->reports->get_terminated_amount_project_contract($year,$quarter);
			$data['terminated_amount_info'] 				= $terminated_amount_info;

			$prev_terminated_amount_info					= $this->reports->get_terminated_amount_project_contract($year-1,$quarter);
			$data['prev_terminated_amount_info']			= $prev_terminated_amount_info;
			// END---------------------------------------

			// START-- G. count service  -----
			$terminated_service_info						= $this->reports->get_terminated_service_contract($year,$quarter);
			$data['terminated_service_info'] 				= $terminated_service_info;

			$prev_terminated_service_info					= $this->reports->get_terminated_service_contract($year-1,$quarter);
			$data['prev_terminated_service_info']			= $prev_terminated_service_info;
			// END---------------------------------------

			// START-- G. amount service  -----
			$terminated_amount_service_info					= $this->reports->get_terminated_amount_service_contract($year,$quarter);
			$data['terminated_amount_service_info'] 		= $terminated_amount_service_info;

			$prev_terminated_amount_service_info			= $this->reports->get_terminated_amount_service_contract($year-1,$quarter);
			$data['prev_terminated_amount_service_info']	= $prev_terminated_amount_service_info;

			// ONSITE MANPOWER TOTAL
			$total_manpower_onsite_info						= $this->reports->get_total_manpower_onsite($year,$quarter);
			$data['total_manpower_onsite_info'] 			= $total_manpower_onsite_info;

			$prev_total_manpower_onsite_info				= $this->reports->get_total_manpower_onsite($year-1,$quarter);
			$data['prev_total_manpower_onsite_info']		= $prev_total_manpower_onsite_info;

			// DEPLOYED MANPOWER TOTAL
			$total_manpower_deployed_info					= $this->reports->get_total_manpower_deployed($year,$quarter);
			$data['total_manpower_deployed_info'] 			= $total_manpower_deployed_info;

			$prev_total_manpower_deployed_info				= $this->reports->get_total_manpower_deployed($year-1,$quarter);
			$data['prev_total_manpower_deployed_info']		= $prev_total_manpower_deployed_info;


			// print_r($prev_not_yet_started_amount_info);

			$html.= $this->load->view('reports/ceis_reports_pocb_reg_companies',$data,TRUE);
			$html.= $this->set_report_footer();
			$pdf = $this->pdf( $filename, $html, TRUE, 'S' );

			$modal_data['pdf'] = $pdf;
			$modal_data['report_title']	= "Overseas Performance of POCB Registered Companies";

		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e;
		}

		$this->load->view('modals/modal_report',$modal_data);	
	}

	public function reg_companies_excel($year,$quarter=NULL,$contractor_list=NULL){

		try{
			$success 				= 0;
			$data 	 				= array();
			$modal_data 			= array();

			$params     			= get_params();
			$data['report']			= $params["report"];
			$data['generate']		= $params['generate'];
			/*$data['contractor_list']= $params['contractor_list'];
			$data['quarter']		= $params['quarter'];*/

			$data['contractor_list']= $contractor_list;
			$data['quarter'] 		= $quarter;
			$data['year'] 			= $year;
			
			$filename 				= 'ceis_reports_'.date("Y_m_d").'.xls';

			// START-- A. No. of POCB Reg Companies -----
			$reg_companies_info				= $this->reports->get_reg_companies($year,$quarter);
			$data['reg_companies_info'] 	= $reg_companies_info;

			$prev_reg_companies_info			= $this->reports->get_reg_companies($year-1,$quarter);
			$data['prev_reg_companies_info'] 	= $prev_reg_companies_info;
			// END---------------------------------------

			// START-- B. No. of Active POCB Reg Companies -----
			$active_companies_info				= $this->reports->get_active_companies($year,$quarter);
			$data['active_companies_info'] 		= $active_companies_info;

			$prev_active_companies_info			= $this->reports->get_active_companies($year-1,$quarter);
			$data['prev_active_companies_info'] = $prev_active_companies_info;
			// END---------------------------------------

			// START-- C. Total Foreign Remittances  -----
			$remittances_companies_info				= $this->reports->get_remittances_companies($year,$quarter);
			$data['remittances_companies_info'] 	= $remittances_companies_info;

			$prev_remittances_companies_info		= $this->reports->get_remittances_companies($year-1,$quarter);
			$data['prev_remittances_companies_info']= $prev_remittances_companies_info;
			// END---------------------------------------

			// START-- D. count project  -----
			$not_yet_started_info					= $this->reports->get_not_started_contract($year,$quarter);
			$data['not_yet_started_info'] 			= $not_yet_started_info;

			$prev_not_yet_started_info				= $this->reports->get_not_started_contract($year-1,$quarter);
			$data['prev_not_yet_started_info']		= $prev_not_yet_started_info;
			// END---------------------------------------

			// START-- D. amount project  -----
			$not_yet_started_amount_info					= $this->reports->get_not_started_amount_project_contract($year,$quarter);
			$data['not_yet_started_amount_info'] 			= $not_yet_started_amount_info;

			$prev_not_yet_started_amount_info				= $this->reports->get_not_started_amount_project_contract($year-1,$quarter);
			$data['prev_not_yet_started_amount_info']		= $prev_not_yet_started_amount_info;
			// END---------------------------------------

			// START-- D. count service  -----
			$not_yet_started_service_info					= $this->reports->get_not_started_service_contract($year,$quarter);
			$data['not_yet_started_service_info'] 			= $not_yet_started_service_info;

			$prev_not_yet_started_service_info				= $this->reports->get_not_started_service_contract($year-1,$quarter);
			$data['prev_not_yet_started_service_info']		= $prev_not_yet_started_service_info;
			// END---------------------------------------

			// START-- D. amount service  -----
			$not_yet_started_amount_service_info					= $this->reports->get_not_started_amount_service_contract($year,$quarter);
			$data['not_yet_started_amount_service_info'] 			= $not_yet_started_amount_service_info;

			$prev_not_yet_started_amount_service_info				= $this->reports->get_not_started_amount_service_contract($year-1,$quarter);
			$data['prev_not_yet_started_amount_service_info']		= $prev_not_yet_started_amount_service_info;
			// END---------------------------------------

			//ONGOING

			// START-- E. count project  -----
			$ongoing_info									= $this->reports->get_ongoing_contract($year,$quarter);
			$data['ongoing_info'] 							= $ongoing_info;

			$prev_ongoing_info								= $this->reports->get_ongoing_contract($year-1,$quarter);
			$data['prev_ongoing_info']						= $prev_ongoing_info;
			// END---------------------------------------

			// START-- E. amount project  -----
			$ongoing_amount_info							= $this->reports->get_ongoing_amount_project_contract($year,$quarter);
			$data['ongoing_amount_info'] 					= $ongoing_amount_info;

			$prev_ongoing_amount_info						= $this->reports->get_ongoing_amount_project_contract($year-1,$quarter);
			$data['prev_ongoing_amount_info']				= $prev_ongoing_amount_info;
			// END---------------------------------------

			// START-- E. count service  -----
			$ongoing_service_info							= $this->reports->get_ongoing_service_contract($year,$quarter);
			$data['ongoing_service_info'] 					= $ongoing_service_info;

			$prev_ongoing_service_info						= $this->reports->get_ongoing_service_contract($year-1,$quarter);
			$data['prev_ongoing_service_info']				= $prev_ongoing_service_info;
			// END---------------------------------------

			// START-- E. amount service  -----
			$ongoing_amount_service_info					= $this->reports->get_ongoing_amount_service_contract($year,$quarter);
			$data['ongoing_amount_service_info'] 			= $ongoing_amount_service_info;

			$prev_ongoing_amount_service_info				= $this->reports->get_ongoing_amount_service_contract($year-1,$quarter);
			$data['prev_ongoing_amount_service_info']		= $prev_ongoing_amount_service_info;
			// END---------------------------------------

			//COMPLETED

			// START-- F. count project  -----
			$completed_info									= $this->reports->get_completed_contract($year,$quarter);
			$data['completed_info'] 						= $completed_info;

			$prev_completed_info							= $this->reports->get_completed_contract($year-1,$quarter);
			$data['prev_completed_info']					= $prev_completed_info;
			// END---------------------------------------

			// START-- F. amount project  -----
			$completed_amount_info							= $this->reports->get_completed_amount_project_contract($year,$quarter);
			$data['completed_amount_info'] 					= $completed_amount_info;

			$prev_completed_amount_info						= $this->reports->get_completed_amount_project_contract($year-1,$quarter);
			$data['prev_completed_amount_info']				= $prev_completed_amount_info;
			// END---------------------------------------

			// START-- F. count service  -----
			$completed_service_info							= $this->reports->get_completed_service_contract($year,$quarter);
			$data['completed_service_info'] 				= $completed_service_info;

			$prev_completed_service_info					= $this->reports->get_completed_service_contract($year-1,$quarter);
			$data['prev_completed_service_info']			= $prev_completed_service_info;
			// END---------------------------------------

			// START-- F. amount service  -----
			$completed_amount_service_info					= $this->reports->get_completed_amount_service_contract($year,$quarter);
			$data['completed_amount_service_info'] 			= $completed_amount_service_info;

			$prev_completed_amount_service_info				= $this->reports->get_completed_amount_service_contract($year-1,$quarter);
			$data['prev_completed_amount_service_info']		= $prev_completed_amount_service_info;

			//TERMINATED

			// START-- G. count project  -----
			$terminated_info								= $this->reports->get_terminated_contract($year,$quarter);
			$data['terminated_info'] 						= $terminated_info;

			$prev_terminated_info							= $this->reports->get_terminated_contract($year-1,$quarter);
			$data['prev_terminated_info']					= $prev_terminated_info;
			// END---------------------------------------

			// START-- G. amount project  -----
			$terminated_amount_info							= $this->reports->get_terminated_amount_project_contract($year,$quarter);
			$data['terminated_amount_info'] 				= $terminated_amount_info;

			$prev_terminated_amount_info					= $this->reports->get_terminated_amount_project_contract($year-1,$quarter);
			$data['prev_terminated_amount_info']			= $prev_terminated_amount_info;
			// END---------------------------------------

			// START-- G. count service  -----
			$terminated_service_info						= $this->reports->get_terminated_service_contract($year,$quarter);
			$data['terminated_service_info'] 				= $terminated_service_info;

			$prev_terminated_service_info					= $this->reports->get_terminated_service_contract($year-1,$quarter);
			$data['prev_terminated_service_info']			= $prev_terminated_service_info;
			// END---------------------------------------

			// START-- G. amount service  -----
			$terminated_amount_service_info					= $this->reports->get_terminated_amount_service_contract($year,$quarter);
			$data['terminated_amount_service_info'] 		= $terminated_amount_service_info;

			$prev_terminated_amount_service_info			= $this->reports->get_terminated_amount_service_contract($year-1,$quarter);
			$data['prev_terminated_amount_service_info']	= $prev_terminated_amount_service_info;

			// ONSITE MANPOWER TOTAL
			$total_manpower_onsite_info						= $this->reports->get_total_manpower_onsite($year,$quarter);
			$data['total_manpower_onsite_info'] 			= $total_manpower_onsite_info;

			$prev_total_manpower_onsite_info				= $this->reports->get_total_manpower_onsite($year-1,$quarter);
			$data['prev_total_manpower_onsite_info']		= $prev_total_manpower_onsite_info;

			// DEPLOYED MANPOWER TOTAL
			$total_manpower_deployed_info					= $this->reports->get_total_manpower_deployed($year,$quarter);
			$data['total_manpower_deployed_info'] 			= $total_manpower_deployed_info;

			$prev_total_manpower_deployed_info				= $this->reports->get_total_manpower_deployed($year-1,$quarter);
			$data['prev_total_manpower_deployed_info']		= $prev_total_manpower_deployed_info;

			

			$html.= $this->load->view('reports/ceis_reports_pocb_reg_companies',$data,TRUE);
			$html.= $this->set_report_footer();

			$this->convert_excel( $html, $filename , '');

			$success 	= 1;

		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e;
		}
	}

	public function foreign_exchange($year,$quarter=NULL,$contractor_list=NULL){

		try{
			$data 	 				= array();
			$modal_data 			= array();

			$data['contractor_list']= $contractor_list;
			$data['quarter'] 		= $quarter;
			$data['year'] 			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.pdf';
			
			$foreign_exchange_info			= $this->reports->get_foreign_exchange($year,$quarter);
			$data['foreign_exchange_info'] 	= $foreign_exchange_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_foreign_exchange',$data,TRUE);

			$html.= $this->set_report_footer();

			$pdf = $this->pdf( $filename, $html, TRUE, 'S' );

			$modal_data['pdf'] 		= $pdf;

			$this->load->view('modals/modal_report',$modal_data);
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}	
	}

	public function foreign_exchange_excel($year,$quarter=NULL,$contractor_list=NULL){

		try{
			$data 	 				= array();
			$modal_data 			= array();

			$data['quarter'] 		= $quarter;
			$data['year'] 			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.xls';

			$foreign_exchange_info			= $this->reports->get_foreign_exchange($year,$quarter);
			$data['foreign_exchange_info'] 	= $foreign_exchange_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_foreign_exchange',$data,TRUE);

			$html.= $this->set_report_footer();
			$this->convert_excel( $html, $filename , '');

		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}
	}

	public function new_contracts($year,$quarter=NULL,$contractor_list=NULL){
		try{
			$data 	 				= array();
			$modal_data 			= array();

			$data['year']			= $year;
			$data['quarter']		= $quarter;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.pdf';

			$projects_new_contracts_info			= $this->reports->get_projects_new_contracts($year,$quarter);
			$data['projects_new_contracts_info'] 	= $projects_new_contracts_info;

			$services_new_contracts_info			= $this->reports->get_services_new_contracts($year,$quarter);
			$data['services_new_contracts_info'] 	= $services_new_contracts_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_new_contracts',$data,TRUE);
			$html.= $this->set_report_footer();

			$pdf = $this->pdf( $filename, $html, FALSE, 'S' );

			$modal_data['pdf'] = $pdf;
			$this->load->view('modals/modal_report',$modal_data);
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}
	}

	public function new_contracts_excel($year,$quarter=NULL,$contractor_list=NULL){
		try{
			$data 	 				= array();
			$modal_data 			= array();

			$data['quarter'] 		= $quarter;
			$data['year'] 			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.xls';

			$projects_new_contracts_info			= $this->reports->get_projects_new_contracts($year,$quarter);
			$data['projects_new_contracts_info'] 	= $projects_new_contracts_info;

			$services_new_contracts_info			= $this->reports->get_services_new_contracts($year,$quarter);
			$data['services_new_contracts_info'] 	= $services_new_contracts_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_new_contracts',$data,TRUE);

			$html.= $this->set_report_footer();	
			$this->convert_excel( $html, $filename , '');

		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}

		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}
	}

	public function breakdown($year,$quarter=NULL,$contractor_list=NULL){
		try{
			$data 	 				= array();
			$modal_data 			= array();

			$data['year']			= $year;
			$data['quarter']		= $quarter;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.pdf';

			$breakdown_info			= $this->reports->get_breakdown($year,$quarter);
			$data['breakdown_info'] = $breakdown_info;

			$geo_dist_info			= $this->reports->get_dist($year,$quarter);
			$data['geo_dist_info'] 	= $geo_dist_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_breakdown',$data,TRUE);

			$html.= $this->set_report_footer();
		
			$pdf = $this->pdf( $filename, $html, TRUE, 'S' );

			$modal_data['pdf'] = $pdf;
			$this->load->view('modals/modal_report',$modal_data);
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}	
	}

	public function breakdown_excel($year,$quarter=NULL,$contractor_list=NULL){
		try{

			$data 	 				= array();
			$modal_data 			= array();

			$data['quarter']		= $quarter;
			$data['year']			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.xls';

			$breakdown_info			= $this->reports->get_breakdown($year,$quarter);
			$data['breakdown_info'] = $breakdown_info;

			$geo_dist_info			= $this->reports->get_dist($year,$quarter);
			$data['geo_dist_info'] 	= $geo_dist_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_breakdown',$data,TRUE);

			$html.= $this->set_report_footer();

			$this->convert_excel( $html, $filename , '');

		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}
	}

	public function manpower_report($year,$quarter=NULL,$contractor_list=NULL){

		try{
			$data 	 				= array();
			$modal_data 			= array();

			$data['quarter']		= $quarter;
			$data['year']			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.pdf';

			$manpower_info			= $this->reports->get_manpower($year,$quarter);
			$data['manpower_info'] 	= $manpower_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_manpower_report',$data,TRUE);

			$html.= $this->set_report_footer();
			
			$pdf = $this->pdf( $filename, $html, TRUE, 'S' );
			$modal_data['pdf'] = $pdf;
			$this->load->view('modals/modal_report',$modal_data);

		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}
	}

	public function manpower_report_excel($year,$quarter=NULL,$contractor_list=NULL){
		try{
			$data 	 				= array();
			$modal_data 			= array();

			$data['quarter']		= $quarter;
			$data['year']			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.xls';

			$manpower_info			= $this->reports->get_manpower($year,$quarter);
			$data['manpower_info'] 	= $manpower_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_manpower_report',$data,TRUE);

			$html.= $this->set_report_footer();

			$this->convert_excel( $html, $filename , '');

		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}	
	}

	public function geo_worker_first($year,$quarter=NULL,$contractor_list=NULL){
		
		try{
			$data 	 				= array();
			$modal_data 			= array();

			$data['quarter']		= $quarter;
			$data['year']			= $year;


			$geo_region_proj_first_info				= $this->reports->get_geo_region_proj_first($year,$quarter);
			$data['geo_region_proj_first_info'] 	= $geo_region_proj_first_info;

			$geo_region_serv_first_info				= $this->reports->get_geo_region_serv_first($year,$quarter);
			$data['geo_region_serv_first_info'] 	= $geo_region_serv_first_info;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.pdf';

			$html.= $this->load->view('reports/ceis_reports_pocb_geo_worker_first',$data,TRUE);

			$html.= $this->set_report_footer();
			
			$pdf = $this->pdf( $filename, $html, TRUE, 'S' );

			$modal_data['pdf'] = $pdf;
			$this->load->view('modals/modal_report',$modal_data);	
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}
	}

	public function geo_worker_first_excel($year,$quarter=NULL,$contractor_list=NULL){
		try{

			$data 	 				= array();
			$modal_data 			= array();

			$data['quarter']		= $quarter;
			$data['year']			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.xls';

			$geo_region_proj_first_info				= $this->reports->get_geo_region_proj_first($year,$quarter);
			$data['geo_region_proj_first_info'] 	= $geo_region_proj_first_info;

			$geo_region_serv_first_info				= $this->reports->get_geo_region_serv_first($year,$quarter);
			$data['geo_region_serv_first_info'] 	= $geo_region_serv_first_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_geo_worker_first',$data,TRUE);

			$html.= $this->set_report_footer();

			$this->convert_excel( $html, $filename , '');
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}

	}

	public function geo_worker_second($year,$quarter=NULL,$contractor_list=NULL){
		try{
			$success 				= 0;
			$data 	 				= array();
			$modal_data 			= array();

			$data['quarter']		= $quarter;
			$data['year'] 			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.pdf';

			$get_geo_region_second_info				= $this->reports->get_geo_region_second($year,$quarter);
			$data['get_geo_region_second_info'] 	= $get_geo_region_second_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_geo_worker_second',$data,TRUE);

			$html.= $this->set_report_footer();
			
			$pdf = $this->pdf( $filename, $html, TRUE, 'S' );

			$modal_data['pdf'] = $pdf;
			$this->load->view('modals/modal_report',$modal_data);	
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);
			
			echo $e->getMessage();
		}
	}

	public function geo_worker_second_excel($year,$quarter=NULL,$contractor_list=NULL){
		try{
			$data 	 				= array();
			$modal_data 			= array();
			$data['quarter']		= $quarter;
			$data['year'] 			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.xls';

			$get_geo_region_second_info				= $this->reports->get_geo_region_second($year,$quarter);
			$data['get_geo_region_second_info'] 	= $get_geo_region_second_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_geo_worker_second',$data,TRUE);

			$html.= $this->set_report_footer();

			$this->convert_excel( $html, $filename , '');
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);

		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e;
		}
	
	}

	public function report_project($year,$quarter=NULL,$contractor_list=NULL){

		try{
			$data 	 				= array();
			$modal_data 			= array();

			$params     			= get_params();
			$data['quarter']		= $quarter;
			$data['year'] 			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.pdf';

			$get_report_overseas_project_info			= $this->reports->get_report_overseas_project($year,$quarter);
			$data['get_report_overseas_project_info'] 	= $get_report_overseas_project_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_report_project',$data,TRUE);

			$html.= $this->set_report_footer();
			
			$pdf = $this->pdf( $filename, $html, FALSE, 'S' );

			$modal_data['pdf'] 		= $pdf;
			$this->load->view('modals/modal_report',$modal_data);	
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);
			
			echo $e;
		}
	}

	public function report_project_excel($year,$quarter=NULL,$contractor_list=NULL){
		try{
			$data 	 				= array();
			$modal_data 			= array();

			$data['quarter']		= $quarter;
			$data['year'] 			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.xls';

			$get_report_overseas_project_info			= $this->reports->get_report_overseas_project($year,$quarter);
			$data['get_report_overseas_project_info'] 	= $get_report_overseas_project_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_report_project',$data,TRUE);

			$html.= $this->set_report_footer();
			$this->convert_excel( $html, $filename , '');
		}
		catch(PDOException $e){
			echo $this->get_user_message($e);
		}
		catch(Exception $e){
			echo $e;
		}
	}

	public function report_service($year,$quarter=NULL,$contractor_list=NULL){
		try{
			$data 	 				= array();
			$modal_data 			= array();

			$data['quarter']		= $quarter;
			$data['year'] 			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.pdf';

			$get_report_overseas_service_info			= $this->reports->get_report_overseas_service($year,$quarter);
			$data['get_report_overseas_service_info'] 	= $get_report_overseas_service_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_report_service',$data,TRUE);
			$html.= $this->set_report_footer();
			
			$pdf = $this->pdf( $filename, $html, FALSE, 'S' );

			$modal_data['pdf'] = $pdf;

			$this->load->view('modals/modal_report',$modal_data);
		}
		catch(PDOException $e){
			echo $this->get_user_message($e);
		}
		catch(Exception $e){
			echo $e;
		}
	}

	public function report_service_excel($year,$quarter=NULL,$contractor_list=NULL){
		try{
			$data 	 				= array();
			$modal_data 			= array();
			$data['quarter']		= $quarter;
			$data['year'] 			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.xls';
			$get_report_overseas_service_info			= $this->reports->get_report_overseas_service($year,$quarter);
			$data['get_report_overseas_service_info'] 	= $get_report_overseas_service_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_report_service',$data,TRUE);

			$html.= $this->set_report_footer();	
			$this->convert_excel( $html, $filename , '');
		}
		catch(PDOException $e){
			echo $this->get_user_message($e);
		}
		catch(Exception $e){
			echo $e;
		}

	}

	public function completed_service($year,$quarter=NULL,$contractor_list=NULL){
		try{
			$data 	 				= array();
			$modal_data 			= array();
			$modal 					= '';

			$data['quarter']		= $quarter;
			$data['year'] 			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.pdf';

			$get_completed_service_info				= $this->reports->get_completed_service($year,$quarter);
			$data['get_completed_service_info'] 	= $get_completed_service_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_completed_service',$data,TRUE);

			$html.= $this->set_report_footer();	

			$pdf = $this->pdf( $filename, $html, FALSE, 'S' );

			$modal_data['pdf'] = $pdf;
			$this->load->view('modals/modal_report',$modal_data);
		}
		catch(PDOException $e){
			echo $this->get_user_message($e);
		}
		catch(Exception $e){
			echo $e;
		}
	}

	public function completed_service_excel($year,$quarter=NULL,$contractor_list=NULL){
		try{
			$data 	 				= array();
			$modal_data 			= array();
			$data['quarter']		= $quarter;
			$data['year'] 			= $year;

			$filename 				= 'ceis_reports_'.date("Y_m_d").'.xls';

			$get_completed_service_info				= $this->reports->get_completed_service($year,$quarter);
			$data['get_completed_service_info'] 	= $get_completed_service_info;

			$html.= $this->load->view('reports/ceis_reports_pocb_completed_service',$data,TRUE);

			$html.= $this->set_report_footer();
			$this->convert_excel( $html, $filename , '');
		}
		catch(PDOException $e){
			echo $this->get_user_message($e);
		}	
		catch(Exception $e){
			echo $e;
		}

	}
}

/* End of file Ceis_reports.php */
/* Location: ./application/modules/ceis/controllers/Ceis_reports.php */