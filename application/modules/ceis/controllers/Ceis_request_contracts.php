<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_request_contracts extends CEIS_Controller {
	
	private $module = MODULE_CEIS_REQUESTS;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('Ceis_request_contracts_model', 'crcm');
	}

	
	public function modal_project($encoded_id, $salt, $token, $encoded_type)
	{
		try 
		{
			$data = array();
		 	$id   = base64_url_decode($encoded_id);
		 	$type = base64_url_decode($encoded_type);
		 	
		 	// CHECK THE SECURITY VARIABLES
		 	check_salt($id, $salt, $token);
		 	
		 	$data['contract_details'] = $this->crcm->get_contract_details($id, $type);

		 	$this->load->view('modals/project',$data);
		}
		catch(PDOException $e)
		{
			echo $e->getMessage();
		}
		catch(Exception $e)
		{
			echo $e->getMessage();
		}
	}
	
	public function modal_manpower($encoded_id, $salt, $token, $encoded_type)
	{
		try
		{
			$data = array();
			$id   = base64_url_decode($encoded_id);
			$type = base64_url_decode($encoded_type);
			
			// CHECK THE SECURITY VARIABLES
			check_salt($id, $salt, $token);
			
			$data['manpower_details'] = $this->crcm->get_contract_details($id, $type);

			$this->load->view('modals/manpower',$data);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
	
	public function get_contract_project_list($req_id)
	{
		try{
			$rows		    = array();
			$params	 		= get_params();
			$request_id 	= base64_url_decode($req_id);
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns 		= array('a.rqst_contract_id', 'a.contract_type', 'a.project_title', 'a.project_location', 'a.owner', 'FORMAT(a.contract_cost, '.DECIMAL_PLACES.') as contract_cost', 'b.contract_status_name');
			// APPEARS ON TABLE
			$bColumns 		= array('project_title', 'project_location', 'owner', 'contract_cost', 'contract_status_name');
	
			$projects 		= $this->crcm->get_contract_project_list($aColumns, $bColumns, $params, $request_id);
			
			foreach($projects['aaData'] as $aRow):	
				$salt    = gen_salt();
				$token   = in_salt($aRow['rqst_contract_id'], $salt);
				$url     = base64_url_encode($aRow['rqst_contract_id'])."/".$salt."/".$token."/".base64_url_encode($aRow['contract_type']);
				$action  = "<div class='table-actions'>";
				$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_project' onclick=\"modal_project_init('".$url."')\" ><i class='flaticon-search95'></i></a>";
				$action .= "</div>";
				
				$rows[]  = array($aRow['project_title'], $aRow['project_location'], $aRow['owner'], $aRow['contract_cost'], $aRow['contract_status_name'], $action);				
			endforeach;
			
			$projects['aaData'] = $rows;
			
			echo json_encode($projects);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
	}
	
	
	public function get_contract_manpower_list($req_id)
	{
		try
		{
			$rows		    = array();
			$params	 		= get_params();
			$request_id 	= base64_url_decode($req_id);
			//TITLE, LOCATION, FOREIGN PRINCIPAL DURATION STATUS 
			
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns 		= array('a.rqst_contract_id', 'a.contract_type', 'a.project_title', 'a.project_location', 'c.foreign_principal_name', 'a.contract_duration', 'b.contract_status_name');
			// APPEARS ON TABLE
			$bColumns 		= array('project_title', 'project_location', 'foreign_principal_name', 'contract_duration', 'contract_status_name');
			
			$manpower 		= $this->crcm->get_contract_manpower_list($aColumns, $bColumns, $params, $request_id);
				
			foreach($manpower['aaData'] as $aRow):
				$salt    = gen_salt();
				$token   = in_salt($aRow['rqst_contract_id'], $salt);
				$url     = base64_url_encode($aRow['rqst_contract_id'])."/".$salt."/".$token."/".base64_url_encode($aRow['contract_type']);
				$action  = "<div class='table-actions'>";
				$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_manpower' onclick=\"modal_manpower_init('".$url."')\" ><i class='flaticon-search95'></i></a>";
				$action .= "</div>";
				
				$rows[]  = array($aRow['project_title'], $aRow['project_location'], $aRow['foreign_principal_name'], $aRow['contract_duration'], $aRow['contract_status_name'], $action);
			endforeach;
				
			$manpower['aaData'] = $rows;
				
			echo json_encode($manpower);
	
		}
	
		catch(PDOException $e){
			echo $e->getMessage();
		}
	
		catch(Exception $e){
			echo $e->getMessage();
		}
	
	}
	
}


/* End of file Ceis_country_profile.php */
/* Location: ./application/modules/sysad/controllers/Ceis_country_profile.php */