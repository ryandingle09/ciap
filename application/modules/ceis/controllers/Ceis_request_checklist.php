<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ceis_request_checklist extends Ceis_Controller
{
	private $module = MODULE_CEIS_DASHBOARD;

	public function __construct()
	{
		parent::__construct();
		
		$this->load->library('sync_upload');
		$this->load->library('file');
		
		$this->load->model('ceis_request_checklist_model', 'crcm');
	}

	public function download_attachment($encoded_id, $salt, $token, $filename)
	{
		$id = base64_url_decode($encoded_id);
		
		check_salt($id, $salt, $token);
		
		$url 	= UPLOAD_REQUESTS_FOLDER.$id.'/'.$filename;
		$dl_url = PATH_UPLOADS_REQUESTS.$id.'/'.$filename;
		
		$this->sync_upload->produce_file($url, FALSE);
		
		$this->file->download_file($dl_url, $filename);
	}
	
	public function modal_checklist($encoded_id, $salt, $token)
	{
		try
		{	
			$data = array();
			$id   = base64_url_decode($encoded_id);
			$ids  = explode('/', $id);
			$request_id   = $ids[0];
			$checklist_id = $ids[1];
			
			check_salt($id, $salt, $token);
			
			
			$checklist_details = $this->crcm->get_checklist($request_id, $checklist_id);
			$data['checklist_details'] = array_shift($checklist_details);

			
			$html  = $this->load->view('modals/request_checklist',$data, TRUE);
			
			echo $html;
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
		}
	}
}

/* End of file Ceis_dashboard.php */
/* Location: ./application/modules/ceis/controllers/Ceis_dashboard.php */