<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ceis_project_types extends CEIS_Controller {
	
	private $module = MODULE_CEIS_PROJECT_TYPES;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('project_types_model', 'project_types');
	}
	
	public function index()
	{	
		try{
			$data = $resources = $modal = array();

			$modal = array(
				'modal_project_type' => array(
					'controller'	=> __CLASS__,
					'module'		=> PROJECT_CEIS
				)
			);
			
			$resources['load_css'] 		= array(CSS_DATATABLE);
			$resources['load_js'] 		= array(JS_DATATABLE);
			$resources['load_modal'] 	= $modal;
			$resources['datatable'] 	= array();
			$resources['datatable'][] 	= array('table_id' => 'project_type_table', 'path' => PROJECT_CEIS.'/ceis_project_types/get_project_type_list');
		}
		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e;
		}
		
		$this->template->load('project_types', $data, $resources);
	}
	
	public function get_project_type_list()
	{
		try{

			$params = get_params();
			$cnt = 0;
		
			// FIELDS TO BE SELECTED FROM TABLE
			$aColumns = array(
				"a.project_type_code",
				"a.project_type_name",
				"a.classification",
				"a.created_by",
				"a.created_date",
				"a.modified_by",
				"a.modified_date",
				'CONCAT(b.last_name, \',  \', b.first_name) as created_name',
				'CONCAT(c.last_name, \',  \', c.first_name) as modified_name');

			// APPEARS ON TABLE
			$bColumns = array(
				"project_type_name",
				"created_name",
				"created_date",
				"modified_name",
				"modified_date",
				);
		
			$project_types 	= $this->project_types->get_project_type_list($aColumns, $bColumns, $params);
			$iTotal 		= $this->project_types->total_length();
			$iFilteredTotal = $this->project_types->filtered_length($aColumns, $bColumns, $params);
		
			$output = array(
				"sEcho" => intval($_POST['sEcho']),
				"iTotalRecords" => $iTotal["cnt"],
				"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
				"aaData" => array()
			);
		
			foreach ($project_types as $data):
				
				// PRIMARY  KEY
				$project_type_code	= $data["project_type_code"];

				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash($project_type_code);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("project type", "'.$url.'")';
				
				$action = "<div class='table-actions'>";

				//if(($this->permission->check_permission(MODULE_ROLE, ACTION_EDIT))) :
					$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-project_types='bottom' data-delay='50' data-modal='modal_project_type' onclick=\"modal_init('".$url."')\"></a>";
				//endif;

				//if (($this->permission->check_permission(MODULE_ROLE, ACTION_DELETE))) : 	  
					$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-project_types='bottom' data-delay='50'></a>";
				//endif;
			
				$action .= '</div>';

				$row 	= array();
				$row[] 	= $data['project_type_name'];
				$row[] 	= $data['created_name'];
				$row[] 	= $data['created_date'];
				$row[] 	= $data['modified_name'];
				$row[] 	= $data['modified_date'];
				$row[] 	= $action;
				$output['aaData'][] = $row;
			endforeach;
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);

			echo $e->getMessage();
		}
		echo json_encode( $output );
	}
	
	public function modal($encoded_id, $salt, $token)
	{
		try{
			$data = array();
			$resources = array();
			$resources['load_css'] 	= array('selectize.default');
			$load_js 				= array('jquery.jscrollpane','selectize');

			// for update
			if(!EMPTY($encoded_id)){
				// DECODE THE ID TO GET THE ORIGINAL VALUE
				
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);	
				
				$hash_id 		= base64_url_decode($encoded_id);

				$key 			= $this->get_hash_key('project_type_code');
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->project_types->get_specific_project_type($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));	

				$data['project_type_code'] 		= $info['project_type_code'];
				$data['project_type_name'] 		= $info['project_type_name'];
				$data['classification'] 		= $info['classification'];
				$data['created_by'] 			= $info['created_by'];	
				$data['created_date'] 			= $info['created_date'];
				$data["readonly_value"]			= "readonly";	

				// CREATE NEW SECURITY VARIABLES
				$project_type_code	= $info['project_type_code'];
				$hash_id 		= $this->hash($project_type_code);			

			}

			else{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);
				
			}

			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
		}

		catch(PDOException $e){

			$this->rlog_error($e);

			echo $this->get_user_message($e);
		}
		catch(Exception $e){

			$this->rlog_error($e);
			
			echo $e->getMessage();
		}
		
		$resources['load_js'] = $load_js;
		$this->load->view('modals/project_types',$data);
		$this->load_resources->get_resource($resources);
	}
	
	public function process()
	{
		try
		{
			$flag 				= 0;
			$params 			= get_params();
			
			
			// SERVER VALIDATION
			$this->_validate($params);

	
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 				= $this->get_hash_key('project_type_code');
			$where				= array();
			$where[$key]		= $params['hash_id'];

			$info 				= $this->project_types->get_specific_project_type($where);
			$project_type_code	= $info['project_type_code'];

			CEIS_Model::beginTransaction();

			$project_type_name 	= $params['project_type_name'];

			IF(EMPTY($project_type_code)){
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= CEIS_Model::tbl_project_types;
				$audit_schema[]	= DB_CEIS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				// SAVES THE RESOLUTIONS RECORD
				$this->project_types->insert_project_type($params);

				$activity	= "%s has been added in project type.";
				$activity 	= sprintf($activity, $project_type_name);

				$msg  = $this->lang->line('data_saved');
			}

			else{

				$params['project_type_code']	= $project_type_code;

				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= CEIS_Model::tbl_project_types;
				$audit_schema[]	= DB_CEIS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				// SAVES THE PROJECT TYPES RECORD
				$this->project_types->update_project_type($params);

				$activity	= "%s has been updated in project type.";
				$activity 	= sprintf($activity, $project_type_name);

				$msg  = $this->lang->line('data_updated');
			}


			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table, 
				$audit_schema
			);
			
			CEIS_Model::commit();

			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			CEIS_Model::rollback();
			
			$this->rlog_error($e);
			$msg = $this->get_user_message($e);
			
		}
		catch(Exception $e)
		{
			CEIS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	
	}
	
	private function _validate(&$params)
	{
		try
		{
			
			$this->_validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields = array();
			$fields['project_type_code']			= 'Project Code';
			$fields['project_type_name']			= 'Project Name';
			$fields['classification']		= 'Classification';

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _validate_input($params)
	{
		try
		{
			$validation['project_type_name'] = array(
					'data_type' => 'string',
					'name'		=> 'Project Name',
					'max_len'	=> 100
			);

			$validation['project_type_code'] = array(
					'data_type' => 'string',
					'name'		=> 'Project Code',
					'max_len'	=> 100
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function delete_project_type($params)
	{
		try 
		{
			$flag	= 0;
			$msg	= "Error";

			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->_validate_security($params);

			$key 			= $this->get_hash_key('project_type_code');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->project_types->get_specific_project_type($where);
			
			$project_type_code = $info['project_type_code'];

			if(EMPTY($info['project_type_code']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			CEIS_Model::beginTransaction();
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= CEIS_Model::tbl_project_types;
			$audit_schema[]	= DB_CEIS;			
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL

			$this->project_types->delete_project_type($info['project_type_code']);

			// SET AS AN ARRAY
			$curr_detail[]	= array();
			
			// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $project_type_code);
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			CEIS_Model::commit();

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			CEIS_Model::rollback();

			$this->rlog_error($e);
			
			$msg	= $this->get_user_message($e);	
		}
		catch(Exception $e)
		{	
			CEIS_Model::rollback();		

			$this->rlog_error($e);	
			
			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
	}
}

/* End of file Ceis_project_types.php */
/* Location: ./application/modules/ceis/controllers/Ceis_project_types.php */