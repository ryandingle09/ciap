<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Market_value_model extends MPIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
		
	public function get_market_value_list($market_value_type, $select_fields, $where_fields, $params)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
			
			$filter_where 		= $where["search_str"];
			$filter_params 		= $where["search_params"];
			$filter_params[]	= $market_value_type;
							
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
								
				WHERE 1 = 1 
				
				$filter_where
				
				AND A.market_value_type_id = ?
				
				GROUP BY A.market_value_id
				
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query,MPIS_Model::tbl_market_values); 
			

			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($market_value_type, $select_fields, $where_fields, $params)
	{
		try
		{
			
			$this->get_market_value_list($market_value_type, $select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length($market_value_type)
	{
		try
		{
			$fields = array("COUNT(market_value_id) cnt");
			$where	= array("market_value_type_id" => $market_value_type);
				
			return $this->select_one($fields, MPIS_Model::tbl_market_values, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_market_value($market_value_id)
	{
		try
		{			
			$this->delete_data(MPIS_Model::tbl_market_values, array('market_value_id'=>$market_value_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	
	public function delete_market_value_prices($market_value_id)
	{
		try
		{
			$this->delete_data(MPIS_Model::tbl_market_value_prices, array('market_value_id'=>$market_value_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_specific_market_value($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_one($fields, MPIS_Model::tbl_market_values, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_market_value_prices($where){
	
		try
		{
			$fields = array("*");
	
			return $this->select_all($fields, MPIS_Model::tbl_market_value_prices, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	
	public function get_specific_market_value_prices($market_value_type_id, $market_value_id, $price_type_id = NULL){
	
		try
		{
			$where 	= "";
			$val	= array();
			$val[]	= $market_value_id;
			$val[]	= $market_value_type_id;
			
			IF(!EMPTY($price_type_id))
			{
				$where = " AND A.price_type_id = ? ";
				$val[] = $price_type_id;
			}
								
			$query = <<<EOS
				SELECT A.price_type_id, A.price_type_name, B.current_price, B.constant_price
				FROM %s A
				JOIN %s B ON A.price_type_id = B.price_type_id
				AND B.market_value_id = ? 
				
				WHERE A.market_value_type_id = ? 
				
				$where
EOS;

			$query  = sprintf($query, MPIS_Model::tbl_param_price_types, MPIS_Model::tbl_market_value_prices);
		
			return $this->query($query, $val);
	
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function insert_market_value($params){
				
		try
		{

			$val 							= array();
			$val['market_value_type_id']	= $params['market_value_type_id'];			
			$val['market_value_year']		= filter_var($params['year'], FILTER_SANITIZE_NUMBER_INT);			
			$val["created_by"]				= $this->session->userdata("employee_no");
			$val["created_date"] 			= date('Y-m-d H:i:s');
				

			return $this->insert_data(MPIS_Model::tbl_market_values, $val, TRUE);			
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function insert_market_value_prices($params){
	
		try
		{
			
			
			$constant 	= $params['constant'];
			$current 	= $params['current'];
			
			
			foreach($constant as $price_type_id => $constant_price)
			{
				$field 						= array();
				$field['market_value_id']	= $params["market_value_id"];
				$field['price_type_id']		= $price_type_id;
				$field['constant_price']	= filter_var(str_replace(',', '', $constant_price), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
				$field['current_price']		= filter_var(str_replace(',', '', $current[$price_type_id]), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
				
				$this->insert_data(MPIS_Model::tbl_market_value_prices, $field);
			}
						
				
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}	



	public function update_market_value($params)
	{
		try
		{
			
			$val 							= array();
			$val['market_value_year']		= filter_var($params['year'], FILTER_SANITIZE_NUMBER_INT);						
			$val["last_modified_by"] 		= $this->session->userdata("employee_no");
			$val["last_modified_date"] 		= date('Y-m-d H:i:s');
			
			
			$where 							= array();
			$where["market_value_id"]		= $params["market_value_id"];

			$this->update_data(MPIS_Model::tbl_market_values, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	

	public function update_market_value_prices($params){
	
		try
		{
				
			$val					= array();
			$val['constant_price']	= filter_var(str_replace(',', '', $params['constant_price']), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			$val['current_price']	= filter_var(str_replace(',', '', $params['current_price']), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);		
							
			$where						= array();
			$where['market_value_id']	= $params["market_value_id"];
			$where['price_type_id']		= $params["price_type_id"];

			$this->update_data(MPIS_Model::tbl_market_value_prices, $val, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}	
	
	
	/*
	 * USED IN REPORTS MODULE
	 */
	public function get_market_value_reports($where)
	{
		try
		{
			
			$where_arr	= $this->get_where_statement($where);
			$where 		= $where_arr['where'];
			$val		= $where_arr['val'];
			
			$query = <<<EOS
				SELECT 
					A.price_type_id, B.market_value_year, 
				    SUM(A.current_price) current_price, 
				    SUM(A.constant_price) constant_price
				FROM market_value_prices A 
				JOIN market_values B ON A.market_value_id = B.market_value_id
				
				$where	
					
				GROUP BY A.price_type_id, B.market_value_year			
EOS;

			return $this->query($query, $val);
			
		}
		catch(PDOException $e)
		{
			throw $e;
		}
	}
	
	public function get_cost_construction_index_reports($where)
	{
		try 
		{
			
			$where_arr	= $this->get_where_statement($where);
			$where 		= $where_arr['where'];
			$val		= $where_arr['val'];			
			
			$query = <<<EOS
				SELECT 
				    B.market_value_year, ((A.current_price / A.constant_price) * 100) ipin
				
				FROM market_value_prices A
				
				JOIN market_values B 
				ON A.market_value_id = B.market_value_id
				
				
				JOIN param_price_types C
				ON A.price_type_id = C.price_type_id 
				
				$where
					
				ORDER BY B.market_value_year, C.price_type_id			
EOS;
	
			return $this->query($query, $val);
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
	}
	
}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */