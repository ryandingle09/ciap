<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Financial_ratios_model extends MPIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
		
	public function get_financial_ratios_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
						
			$filter_where 		= $where["search_str"];
			$filter_params 		= $where["search_params"];
			
		
						
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				JOIN %s B ON A.company_type_id = B.company_type_id
				JOIN %s C ON A.license_category_id = C.license_category_id
				
				$filter_where
				
				GROUP BY A.financial_ratio_id
				
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query, 
				MPIS_Model::tbl_financial_ratios, 
				MPIS_Model::tbl_param_company_types, 
				MPIS_Model::tbl_param_license_categories
			);
			
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($select_fields, $where_fields, $params)
	{
		try
		{
			
			$this->get_financial_ratios_list($select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(financial_ratio_id) cnt");
						
			return $this->select_one($fields, MPIS_Model::tbl_financial_ratios);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_financial_ratio($financial_ratio_id)
	{
		try
		{			
			$this->delete_data(MPIS_Model::tbl_financial_ratios, array('financial_ratio_id'=>$financial_ratio_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	
	public function delete_financial_ratio_items($financial_ratio_id)
	{
		try
		{
			$this->delete_data(MPIS_Model::tbl_financial_ratio_items, array('financial_ratio_id'=>$financial_ratio_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_specific_financial_ratio($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_one($fields, MPIS_Model::tbl_financial_ratios, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_financial_ratio_items($where){
	
		try
		{
			$fields = array("*");
	
			return $this->select_all($fields, MPIS_Model::tbl_financial_ratio_items, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	
	public function get_specific_financial_ratio_items($financial_ratio_id, $financial_ratio_item_id = NULL){
	
		try
		{
			$where 	= "";
			$val	= array($financial_ratio_id);
			
			IF(!EMPTY($financial_ratio_item_id))
			{
				$where = " WHERE A.financial_ratio_item_id = ? ";
				$val[] = $financial_ratio_item_id;
			}
								
			$query = <<<EOS
				SELECT A.financial_ratio_item_id, A.financial_ratio_item_name, B.amount
				FROM %s A
				JOIN %s B ON A.financial_ratio_item_id = B.financial_ratio_item_id
				AND B.financial_ratio_id = ?
				$where
EOS;

			$query  = sprintf($query, MPIS_Model::tbl_param_financial_ratio_items, MPIS_Model::tbl_financial_ratio_items);
			
			return $this->query($query, $val);
	
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function insert_financial_ratios($params){
				
		try
		{

			$val 							= array();
			$val['company_type_id']			= filter_var($params['company_type'], FILTER_SANITIZE_NUMBER_INT);
			$val['financial_ratio_year']	= filter_var($params['year'], FILTER_SANITIZE_NUMBER_INT);
			$val['license_category_id']		= filter_var($params['category'], FILTER_SANITIZE_NUMBER_INT);
			$val['company_name']			= filter_var($params['company_name'], FILTER_SANITIZE_STRING);
			$val["created_by"] 				= $this->session->userdata("employee_no");
			$val["created_date"] 			= date('Y-m-d H:i:s');
				

			return $this->insert_data(MPIS_Model::tbl_financial_ratios, $val, TRUE);			
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function insert_financial_ratio_items($params){
	
		try
		{
						
			foreach($params['items'] as $key => $amount)
			{
				
				
				$field 								= array();
				$field['financial_ratio_id']		= $params["financial_ratio_id"];
				$field['financial_ratio_item_id']	= $key;
				$field['amount']					= filter_var(str_replace(',', '', $amount), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
				
				
				$this->insert_data(MPIS_Model::tbl_financial_ratio_items, $field);
			}
						
				
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}	



	public function update_financial_ratio($params)
	{
		try
		{

			
			$val 							= array();
			$val['company_type_id']			= filter_var($params['company_type'], FILTER_SANITIZE_NUMBER_INT);
			$val['financial_ratio_year']	= filter_var($params['year'], FILTER_SANITIZE_NUMBER_INT);
			$val['license_category_id']		= filter_var($params['category'], FILTER_SANITIZE_NUMBER_INT);
			$val['company_name']			= filter_var($params['company_name'], FILTER_SANITIZE_STRING);
			$val["last_modified_by"] 		= $this->session->userdata("employee_no");
			$val["last_modified_date"] 		= date('Y-m-d H:i:s');
			
			
			$where 							= array();
			$where["financial_ratio_id"]	= $params["financial_ratio_id"];

			$this->update_data(MPIS_Model::tbl_financial_ratios, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	

	public function update_financial_ratio_item($params){
	
		try
		{
				
			$val				= array();
			$val['amount']		= filter_var(str_replace(',', '', $params['amount']), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
		
							
			$where								= array();
			$where['financial_ratio_id']		= $params["financial_ratio_id"];
			$where['financial_ratio_item_id']	= $params["financial_ratio_item_id"];

			$this->update_data(MPIS_Model::tbl_financial_ratio_items, $val, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	/*
	 * USED IN REPORTS MODULE
	 */
	
	public function get_financial_ratios_reports($where)
	{
		
		try
		{
		
			$where_arr	= $this->get_where_statement($where);
			$where 		= $where_arr['where'];
			$val		= $where_arr['val'];
			
			$query = <<<EOS
				SELECT 
				B.financial_ratio_year,
				
				SUM(IF(A.financial_ratio_item_id = 1, A.amount, 0)) '1',
				SUM(IF(A.financial_ratio_item_id = 2, A.amount, 0)) '2',
				SUM(IF(A.financial_ratio_item_id = 3, A.amount, 0)) '3',
				SUM(IF(A.financial_ratio_item_id = 4, A.amount, 0)) '4',
				SUM(IF(A.financial_ratio_item_id = 5, A.amount, 0)) '5',
				SUM(IF(A.financial_ratio_item_id = 6, A.amount, 0)) '6',
				SUM(IF(A.financial_ratio_item_id = 7, A.amount, 0)) '7',
				SUM(IF(A.financial_ratio_item_id = 8, A.amount, 0)) '8',
				SUM(IF(A.financial_ratio_item_id = 9, A.amount, 0)) '9',
				SUM(IF(A.financial_ratio_item_id = 10, A.amount, 0)) '10',
				SUM(IF(A.financial_ratio_item_id = 11, A.amount, 0)) '11',
				SUM(IF(A.financial_ratio_item_id = 12, A.amount, 0)) '12',
				SUM(IF(A.financial_ratio_item_id = 13, A.amount, 0)) '13',
				SUM(IF(A.financial_ratio_item_id = 14, A.amount, 0)) '14'
				
				FROM  financial_ratio_items A
				
				JOIN financial_ratios B 
				ON A.financial_ratio_id = B.financial_ratio_id
				
				$where	
					
				GROUP BY B.financial_ratio_year;
			
EOS;

			return $this->query($query, $val);
			
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		
		
	}
	
}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */