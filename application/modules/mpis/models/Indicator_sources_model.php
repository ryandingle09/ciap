<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Indicator_sources_model extends MPIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
		
	public function get_indicator_source_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
			
			$filter_where 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
			
		
		
						
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				
				LEFT JOIN (
					SELECT indicator_source_id, COUNT(indicator_source_id) indicator_source_total
					FROM %s
					GROUP BY indicator_source_id
				) B ON A.indicator_source_id = B.indicator_source_id
				
				
				$filter_where
				
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query, MPIS_Model::tbl_param_indicator_sources, MPIS_Model::tbl_indicators); 
				
				
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($select_fields, $where_fields, $params)
	{
		try
		{
			$this->get_indicator_source_list($select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(indicator_source_id) cnt");
				
			return $this->select_one($fields, MPIS_Model::tbl_param_indicator_sources);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_indicator_source($indicator_source_id)
	{
		try
		{			
			$this->delete_data(MPIS_Model::tbl_param_indicator_sources, array('indicator_source_id'=> $indicator_source_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	
	public function get_specific_indicator_source($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_one($fields, MPIS_Model::tbl_param_indicator_sources, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function insert_indicator_source($params){
				
		try
		{
			$val 							= array();
			$val['indicator_source_name']	= filter_var($params['indicator_source_name'], FILTER_SANITIZE_STRING);
			$val["created_by"] 				= $this->session->userdata("employee_no");
			$val["created_date"] 			= date('Y-m-d H:i:s');

			return $this->insert_data(MPIS_Model::tbl_param_indicator_sources, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
	}




	public function update_indicator_source($params)
	{
		try
		{
			
			$val 							= array();
			$val['indicator_source_name']	= filter_var($params['indicator_source_name'], FILTER_SANITIZE_STRING);
			$val["last_modified_by"] 		= $this->session->userdata("employee_no");
			$val["last_modified_date"] 		= date('Y-m-d H:i:s');
			
			
			$where 						= array();
			$where["indicator_source_id"]	= $params["indicator_source_id"];

			$this->update_data(MPIS_Model::tbl_param_indicator_sources, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */