<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Indicators_model extends MPIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
		
	public function get_indicator_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
			
			$filter_where 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
		
						
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				JOIN %s B ON A.indicator_source_type_id = B.indicator_source_type_id
				JOIN %s C ON A.indicator_source_id = C.indicator_source_id
				JOIN %s D ON A.indicator_category_id = D.indicator_category_id
				
				$filter_where
				
				GROUP BY A.indicator_id
				
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query, 
				MPIS_Model::tbl_indicators, 
				MPIS_Model::tbl_param_indicator_source_types, 
				MPIS_Model::tbl_param_indicator_sources,
				MPIS_Model::tbl_param_indicator_categories
			);
			
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($select_fields, $where_fields, $params)
	{
		try
		{
			$this->get_indicator_list($select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(indicator_id) cnt");
				
			return $this->select_one($fields, MPIS_Model::tbl_indicators);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_indicator($indicator_id)
	{
		try
		{			
			$this->delete_data(MPIS_Model::tbl_indicators, array('indicator_id'=>$indicator_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_indicators_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("indicator_id" => $id);
				
			return $this->select_one($fields, MPIS_Model::tbl_indicators, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}
	
	public function get_specific_indicator($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_one($fields, MPIS_Model::tbl_indicators, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function insert_indicator($params){
				
		try
		{
			$val 								 = array();
			$val['indicator_source_type_id']	= filter_var($params['indicator_source_type_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['indicator_source_id']			= filter_var($params['indicator_source_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['indicator_date']				= date('Y-m-d', strtotime($params['indicator_date']));
			$val['indicator_category_id']		= filter_var($params['indicator_category_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['indicator_page']				= filter_var($params['indicator_page'], FILTER_SANITIZE_STRING);			
			$val['indicator_title']				= filter_var($params['indicator_title'], FILTER_SANITIZE_STRING);
			$val["indicator_abstract"] 			= filter_var($params['indicator_abstract'], FILTER_SANITIZE_STRING);
			$val["created_by"] 					= $this->session->userdata("employee_no");
			$val["created_date"] 				= date('Y-m-d H:i:s');

			$this->insert_data(MPIS_Model::tbl_indicators, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}




	public function update_indicator($params)
	{
		try
		{

			
			$val 								 = array();
			$val['indicator_source_type_id']	= filter_var($params['indicator_source_type_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['indicator_source_id']			= filter_var($params['indicator_source_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['indicator_date']				= date('Y-m-d', strtotime($params['indicator_date']));
			$val['indicator_category_id']		= filter_var($params['indicator_category_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['indicator_page']				= filter_var($params['indicator_page'], FILTER_SANITIZE_STRING);
			$val['indicator_title']				= filter_var($params['indicator_title'], FILTER_SANITIZE_STRING);
			$val["indicator_abstract"] 			= filter_var($params['indicator_abstract'], FILTER_SANITIZE_STRING);
			$val["last_modified_by"] 			= $this->session->userdata("employee_no");
			$val["last_modified_date"] 			= date('Y-m-d H:i:s');
			
			
			$where 						= array();
			$where["indicator_id"]		= $params["indicator_id"];

			$this->update_data(MPIS_Model::tbl_indicators, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	
	
	public function get_indicators_reports($where, $val)
	{
		try
		{
			
						
			$query = <<<EOS
				SELECT 
					A.indicator_title, D.indicator_source_name, A.indicator_date, A.indicator_page, A.indicator_abstract,
					B.indicator_category_id, B.parent_id, B.indicator_category_name
				FROM indicators A
				
				JOIN param_indicator_categories B
				ON A.indicator_category_id = B.indicator_category_id
				
				JOIN param_indicator_source_types C
				ON A.indicator_source_type_id = C.indicator_source_type_id
				
				JOIN param_indicator_sources D
				ON A.indicator_source_id = D.indicator_source_id
					
				$where			
						
				ORDER BY A.indicator_date
EOS;

			return $this->query($query, $val);
		}
		catch(PDOException $e)
		{
			
		}
	}
}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */