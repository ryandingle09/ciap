<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Policies_model extends MPIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
		
	public function get_policies_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
			
			$filter_where 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
			
		
		
						
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				JOIN %s B ON A.policy_category_id = B.policy_category_id
				JOIN %s C ON A.board_id = C.board_id
				
				$filter_where
				
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query, MPIS_Model::tbl_policies, MPIS_Model::tbl_param_policy_categories, MPIS_Model::tbl_param_boards); 
				
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($select_fields, $where_fields, $params)
	{
		try
		{
			$this->get_policies_list($select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(policy_id) cnt");
				
			return $this->select_one($fields, MPIS_Model::tbl_policies);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}

	public function get_specific_policy($where){
	
		try
		{
			$fields = array("*");
	
			return $this->select_one($fields, MPIS_Model::tbl_policies, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function insert_policy($params){
	
		try
		{
			$val 						= array();
			$val['policy_category_id']	= filter_var($params['policy_category'], FILTER_SANITIZE_NUMBER_INT);
			$val['board_id']			= filter_var($params['board'], FILTER_SANITIZE_NUMBER_INT);
			$val['policy']				= filter_var($params['policy'], FILTER_SANITIZE_STRING);
			$val["created_by"] 			= $this->session->userdata("employee_no");
			$val["created_date"] 		= date('Y-m-d H:i:s');
	
			return $this->insert_data(MPIS_Model::tbl_policies, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	
	
	
	public function update_policy($params)
	{
		try
		{
				
			$val 						= array();
			$val['policy_category_id']	= filter_var($params['policy_category'], FILTER_SANITIZE_NUMBER_INT);
			$val['board_id']			= filter_var($params['board'], FILTER_SANITIZE_NUMBER_INT);
			$val['policy']				= filter_var($params['policy'], FILTER_SANITIZE_STRING);
			$val["last_modified_by"] 	= $this->session->userdata("employee_no");
			$val["last_modified_date"] 	= date('Y-m-d H:i:s');
				
				
			$where 				= array();
			$where["policy_id"]	= $params["policy_id"];
	
			$this->update_data(MPIS_Model::tbl_policies, $val, $where);
	
		}
	
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{
			$this->rlog_error($e);
		}
	}	
	
	public function delete_policy($policy_id)
	{
		try
		{
			$this->delete_data(MPIS_Model::tbl_policies, array('policy_id'=> $policy_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}	
	
}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */