<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class cons_countries_model extends MPIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
		
	public function get_cons_countries_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
						
			$filter_where 		= $where["search_str"];
			$filter_params 		= $where["search_params"];
			
		
						
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				JOIN %s B ON A.country_code = B.country_code
								
				$filter_where
				
				GROUP BY A.construction_country_id
				
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query, 
				MPIS_Model::tbl_construction_countries, 
				MPIS_Model::tbl_param_countries
			);
			
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($select_fields, $where_fields, $params)
	{
		try
		{
			
			$this->get_cons_countries_list($select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(construction_country_id) cnt");
						
			return $this->select_one($fields, MPIS_Model::tbl_construction_countries);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_cons_country($construction_country_id)
	{
		try
		{			
			$this->delete_data(MPIS_Model::tbl_construction_countries, array('construction_country_id'=>$construction_country_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	
	public function delete_cons_country_indicators($construction_country_id)
	{
		try
		{
			$this->delete_data(MPIS_Model::tbl_construction_country_indicators, array('construction_country_id'=>$construction_country_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_specific_cons_country($where){
	
		try
		{
			
			$fields	= array('A.construction_country_id', 'A.year', 'A.country_code', 'B.country_name');
			
			$tables = array(
				'main'	=> array(
					'table'	=> 	MPIS_Model::tbl_construction_countries,
					'alias'	=> 'A'
				),
				'table2'	=> array(
					'table'		=> 	MPIS_Model::tbl_param_countries,
					'alias'		=> 'B',
					'type'		=> 'JOIN',
					'condition'	=> 'A.country_code = B.country_code'
				),
			);
			
			return $this->select_one($fields, $tables, $where);
			
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_cons_country_indicators($where){
	
		try
		{
			$fields = array("*");
	
			return $this->select_all($fields, MPIS_Model::tbl_construction_country_indicators, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	
	public function get_specific_cons_country_indicators($construction_country_id, $indicator_id = NULL){
	
		try
		{
			$where 	= "";
			$val	= array($construction_country_id);
			
			IF(!EMPTY($indicator_id))
			{
				$where = " WHERE A.construction_indicator_id = ? ";
				$val[] = $indicator_id;
			}
								
			$query = <<<EOS
				SELECT A.construction_indicator_id, A.construction_indicator, B.value
				FROM %s A
				JOIN %s B ON A.construction_indicator_id = B.construction_indicator_id
				AND B.construction_country_id = ?
				$where
EOS;

			$query  = sprintf($query, MPIS_Model::tbl_param_construction_indicators, MPIS_Model::tbl_construction_country_indicators);
			
			return $this->query($query, $val);
	
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function insert_cons_countries($params){
				
		try
		{

			$val 						= array();			
			$val['year']				= filter_var($params['year'], FILTER_SANITIZE_NUMBER_INT);
			$val['country_code']		= filter_var($params['country'], FILTER_SANITIZE_STRING);
			$val["created_by"] 			= $this->session->userdata("employee_no");
			$val["created_date"] 		= date('Y-m-d H:i:s');
				
			return $this->insert_data(MPIS_Model::tbl_construction_countries, $val, TRUE);			
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function insert_cons_country_indicators($params){
	
		try
		{
						
			foreach($params['indicators'] as $indicator_id => $value)
			{
				$field 								= array();
				$field['construction_country_id']	= $params["construction_country_id"];
				$field['construction_indicator_id']	= $indicator_id;
				$field['value']						= filter_var(str_replace(',', '', $value), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
				
				
				$this->insert_data(MPIS_Model::tbl_construction_country_indicators, $field);
			}				
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}	



	public function update_cons_country($params)
	{
		try
		{

			
			$val 						= array();
			$val['year']				= filter_var($params['year'], FILTER_SANITIZE_NUMBER_INT);
			$val['country_code']		= filter_var($params['country'], FILTER_SANITIZE_STRING);
			$val["last_modified_by"] 	= $this->session->userdata("employee_no");
			$val["last_modified_date"] 	= date('Y-m-d H:i:s');
			
			
			$where 								= array();
			$where["construction_country_id"]	= $params["construction_country_id"];

			$this->update_data(MPIS_Model::tbl_construction_countries, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	

	public function update_cons_country_indicator($params){
	
		try
		{
				
			$val				= array();
			$val['value']		= filter_var(str_replace(',', '', $params['value']), FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
		
							
			$where								= array();
			$where['construction_country_id']	= $params["construction_country_id"];
			$where['construction_indicator_id']	= $params["construction_indicator_id"];

			$this->update_data(MPIS_Model::tbl_construction_country_indicators, $val, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}	
	
	/*
	 *  USED IN REPORST MODULE
	 */
	
	public function get_cons_countries_reports($where)
	{
		try 
		{
			$where_arr	= $this->get_where_statement($where);
			$where 		= $where_arr['where'];
			$val		= $where_arr['val'];
			
			
			$query = <<<EOS
				SELECT A.value, B.year, C.country_code, C.country_name 
				FROM construction_country_indicators A
				JOIN construction_countries B  ON A.construction_country_id = B.construction_country_id 
				JOIN param_countries C ON B.country_code = C.country_code
				$where				
				ORDER BY C.country_name
			
EOS;
			
			return $this->query($query, $val);
			
		}
		catch(PDOException $e)
		{
			
		}
	}
	
}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */