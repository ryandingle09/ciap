<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Board_meetings_model extends MPIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}



	public function get_specific_board_meeting($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_one($fields, MPIS_Model::tbl_board_meetings, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function insert_board_meeting($params){
				
		try
		{
			$val 							= array();
			$val['policy_id']				= filter_var($params['policy_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['board_meeting_type']		= filter_var($params['classification'], FILTER_SANITIZE_NUMBER_INT);
			$val['board_meeting_no']		= filter_var($params['bm_no'], FILTER_SANITIZE_NUMBER_INT);
			$val['board_meeting_date']		= date('Y-m-d', strtotime($params['bm_date']));
			$val['gender_sensitive_flag']	= filter_var($params['gender_sensitive_flag'], FILTER_SANITIZE_NUMBER_INT);

			$this->insert_data(MPIS_Model::tbl_board_meetings, $val);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
	}

	public function update_board_meeting($params)
	{
		try
		{
			
			$val 							= array();
			$val['board_meeting_type']		= filter_var($params['classification'], FILTER_SANITIZE_NUMBER_INT);
			$val['board_meeting_no']		= filter_var($params['bm_no'], FILTER_SANITIZE_NUMBER_INT);
			$val['board_meeting_date']		= date('Y-m-d', strtotime($params['bm_date']));
			$val['gender_sensitive_flag']	= filter_var($params['gender_sensitive_flag'], FILTER_SANITIZE_NUMBER_INT);
			
			
			$where 				= array();
			$where["policy_id"]	= $params["policy_id"];

			$this->update_data(MPIS_Model::tbl_board_meetings, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}

}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */