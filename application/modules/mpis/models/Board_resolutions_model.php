<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Board_resolutions_model extends MPIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}



	public function get_specific_board_resolution($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_one($fields, MPIS_Model::tbl_board_resolutions, $where);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function insert_board_resolution($params){
				
		try
		{
			$val 							= array();
			$val['policy_id']				= filter_var($params['policy_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['board_resolution_series']	= filter_var($params['series'], FILTER_SANITIZE_NUMBER_INT);
			$val['board_resolution_no']		= filter_var($params['br_no'], FILTER_SANITIZE_NUMBER_INT);
			$val['board_resolution_date']	= date('Y-m-d', strtotime($params['br_date']));
			$val['board_resolution_status']	= filter_var($params['status'], FILTER_SANITIZE_STRING);
			$val['board_resolution_title']	= filter_var($params['br_title'], FILTER_SANITIZE_STRING);
			$val['board_resolution_body']	= filter_var($params['br_body'], FILTER_SANITIZE_STRING);
			
			$this->insert_data(MPIS_Model::tbl_board_resolutions, $val);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
	}

	public function update_board_resolution($params)
	{
		try
		{
			
			$val 							= array();
			$val['board_resolution_series']	= filter_var($params['series'], FILTER_SANITIZE_NUMBER_INT);
			$val['board_resolution_no']		= filter_var($params['br_no'], FILTER_SANITIZE_NUMBER_INT);
			$val['board_resolution_date']	= date('Y-m-d', strtotime($params['br_date']));
			$val['board_resolution_status']	= filter_var($params['status'], FILTER_SANITIZE_STRING);
			$val['board_resolution_title']	= filter_var($params['br_title'], FILTER_SANITIZE_STRING);
			$val['board_resolution_body']	= filter_var($params['br_body'], FILTER_SANITIZE_STRING);
			
			
			$where 				= array();
			$where["policy_id"]	= $params["policy_id"];

			$this->update_data(MPIS_Model::tbl_board_resolutions, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}

}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */