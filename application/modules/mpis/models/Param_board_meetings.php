<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Board_meetings_model extends MPIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}


		
	public function get_board_meeting_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
			
			$filter_where 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
			
		
		
						
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				
				LEFT JOIN (
					SELECT board_meeting_type, COUNT(board_meeting_type) meeting_total
					FROM %s
					GROUP BY board_meeting_type
				) B ON A.meeting_type_id = B.board_meeting_type
				
				
				$filter_where
				
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query, MPIS_Model::tbl_param_meeting_types, MPIS_Model::tbl_board_meetings); 
				
				
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($select_fields, $where_fields, $params)
	{
		try
		{
			$this->get_board_meeting_list($select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(meeting_type_id) cnt");
				
			return $this->select_one($fields, MPIS_Model::tbl_param_meeting_types);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function get_specific_board_meeting($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_one($fields, MPIS_Model::tbl_board_meetings, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function insert_board_meeting($params){
				
		try
		{
			$val 							= array();
			$val['policy_id']				= filter_var($params['policy_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['board_meeting_type']		= filter_var($params['classification'], FILTER_SANITIZE_NUMBER_INT);
			$val['board_meeting_no']		= filter_var($params['bm_no'], FILTER_SANITIZE_STRING);
			$val['board_meeting_date']		= date('Y-m-d', strtotime($params['bm_date']));
			$val['gender_sensitive_flag']	= filter_var($params['gender_sensitive_flag'], FILTER_SANITIZE_NUMBER_INT);

			$this->insert_data(MPIS_Model::tbl_board_meetings, $val);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
	}

	public function update_board_meeting($params)
	{
		try
		{
			
			$val 							= array();
			$val['meeting_type_name']		= filter_var($params['meeting_type_name'], FILTER_SANITIZE_STRING);
			$val["last_modified_by"] 		= $this->session->userdata("employee_no");
			$val["last_modified_date"] 		= date('Y-m-d H:i:s');
			
			
			$where 						= array();
			$where["meeting_type_id"]	= $params["meeting_type_id"];

			$this->update_data(MPIS_Model::tbl_param_meeting_types, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}


	public function delete_param_board_meeting($meeting_type_id)
	{
		try
		{
			$this->delete_data(MPIS_Model::tbl_param_meeting_types, array('meeting_type_id'=> $meeting_type_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */