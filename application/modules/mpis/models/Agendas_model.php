<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Agendas_model extends MPIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
		
	public function get_agenda_list($select_fields, $where_fields, $params, $where_arr)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			
			// CONSTRUCT WHERE STATEMENT
			$new_arr 	= $this->get_where_statement($where_arr);
			$where_str 	= $new_arr['where'];
			$val		= $new_arr['val'];
						
			$where	= $this->filtering($where_fields, $params, TRUE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
			
			$filter_where 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
						
			$value			= array_merge($val, $filter_params);
						
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s
								
				$where_str  
				$filter_where
				
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query, MPIS_Model::tbl_meeting_agenda); 
								
			$stmt 	= $this->query($query, $value);
		
			return $stmt;
		}
		catch(PDOException $e)
		{			
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($select_fields, $where_fields, $params, $where_arr)
	{
		try
		{
			$this->get_agenda_list($select_fields, $where_fields, $params, $where_arr);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(agenda_id) cnt");
				
			return $this->select_one($fields, MPIS_Model::tbl_meeting_agenda);
		}
		catch(PDOException $e)
		{			
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;			
		}	
	}
	


	
	public function get_specific_agenda($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_one($fields, MPIS_Model::tbl_meeting_agenda, $where);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function insert_agenda($params){
				
		try
		{
			$val 					= array();
			$val['policy_id']		= filter_var($params['policy_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['agenda_code']		= filter_var($params['code'], FILTER_SANITIZE_STRING);
			$val['agenda_title']	= filter_var($params['title'], FILTER_SANITIZE_STRING);
			$val['agenda_body']		= filter_var($params['body'], FILTER_SANITIZE_STRING);
			$val["created_by"] 		= $this->session->userdata("employee_no");
			$val["created_date"] 	= date('Y-m-d H:i:s');

			return $this->insert_data(MPIS_Model::tbl_meeting_agenda, $val, TRUE);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}




	public function update_agenda($params)
	{
		try
		{
			
			$val 						= array();
			$val['policy_id']			= filter_var($params['policy_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['agenda_code']			= filter_var($params['code'], FILTER_SANITIZE_STRING);
			$val['agenda_title']		= filter_var($params['title'], FILTER_SANITIZE_STRING);
			$val['agenda_body']			= filter_var($params['body'], FILTER_SANITIZE_STRING);
			$val["last_modified_by"] 	= $this->session->userdata("employee_no");
			$val["last_modified_date"] 	= date('Y-m-d H:i:s');
			
			
			$where 						= array();
			$where["agenda_id"]			= $params["agenda_id"];

			$this->update_data(MPIS_Model::tbl_meeting_agenda, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	
	public function delete_agenda($agenda_id)
	{
		try
		{
			$this->delete_data(MPIS_Model::tbl_meeting_agenda, array('agenda_id'=> $agenda_id));
		}
		catch(PDOException $e)
		{
			throw $e;
		}
	}
}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */