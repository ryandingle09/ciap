<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cias_params_model extends SYSAD_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
	
	
	public function get_params($table, $where=NULL, $fields ="*", $order_by = NULL )
	{
		
		try {			
			return $this->select_all($fields, $table, $where, $order_by);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		
	}
}		

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */