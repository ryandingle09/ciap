<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cons_professionals_model extends MPIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
		
	public function get_cons_professionals_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
			
			$filter_where 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
		
						
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				JOIN %s B ON A.profession_id = B.profession_id
				
				$filter_where
				
				GROUP BY A.year, A.profession_id
				
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query, 
				MPIS_Model::tbl_construction_professions, 
				MPIS_Model::tbl_param_professions				
			);
			
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($select_fields, $where_fields, $params)
	{
		try
		{
			$this->get_cons_professionals_list($select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(*) cnt");
				
			return $this->select_one($fields, MPIS_Model::tbl_construction_professions);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_cons_professionals($year, $profession_id)
	{
		try
		{	
			$where 					= array();
			$where['year']			= $year;
			$where['profession_id']	= $profession_id;
			
			$this->delete_data(MPIS_Model::tbl_construction_professions, $where);	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
		
	public function get_specific_cons_professionals($where){
	
		try
		{
			$fields = array("A.year", "A.profession_id", "A.number", "B.profession");
			$tables	= array(
				'main'	=> array(
					'table'	=> MPIS_Model::tbl_construction_professions,
					'alias'	=> 'A'							
				),
				'table2'	=> array(
					'table'		=> MPIS_Model::tbl_param_professions,
					'alias'		=> 'B',
					'type'		=> 'JOIN',
					'condition'	=> 'A.profession_id = B.profession_id'
				)					
			);	
			
			return $this->select_one($fields, $tables, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function insert_cons_professionals($params){
				
		try
		{
			$val 					= array();
			$val['year']			= filter_var($params['year'], FILTER_SANITIZE_NUMBER_INT);
			$val['profession_id']	= filter_var($params['profession'], FILTER_SANITIZE_NUMBER_INT);
			$val['number']			= filter_var(str_replace(',', '', $params['number']), FILTER_SANITIZE_NUMBER_INT);		
			$val["created_by"] 		= $this->session->userdata("employee_no");
			$val["created_date"] 	= date('Y-m-d H:i:s');

			$this->insert_data(MPIS_Model::tbl_construction_professions, $val);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function update_cons_professionals($params)
	{
		try
		{

			
			$val 						= array();
			$val['number']				= filter_var(str_replace(',', '', $params['number']), FILTER_SANITIZE_NUMBER_INT);			
			$val["last_modified_by"] 	= $this->session->userdata("employee_no");
			$val["last_modified_date"] 	= date('Y-m-d H:i:s');
			
			$where 					= array();
			$where["year"]			= $params["year"];
			$where["profession_id"]	= $params["profession"];

			$this->update_data(MPIS_Model::tbl_construction_professions, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
			
			throw $e;
		}
	}
	
	public function get_cons_prof_reports($where)
	{
		try
		{
			$where_arr	= $this->get_where_statement($where);
			$where 		= $where_arr['where'];
			$val 		= $where_arr['val'];
			
			$query = <<<EOS
				SELECT A.year, A.number, B.profession_id, B.profession 
				FROM construction_professions A
				JOIN param_professions B ON A.profession_id = B.profession_id
				$where
				ORDER BY B.profession
EOS;

			return $this->query($query, $val);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
	}
}

/* End of file Cons_professionals_model.php */
/* Location: ./application/modules/mpis/models/Cons_professionals_model.php */