<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Professions_model extends MPIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
		
	public function get_profession_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
			
			$filter_where 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
			
		
		
						
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				
				LEFT JOIN (
					SELECT profession_id, COUNT(profession_id) profession_total
					FROM %s
					GROUP BY profession_id
				) B ON A.profession_id = B.profession_id
				
				
				$filter_where
				
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query, MPIS_Model::tbl_param_professions, MPIS_Model::tbl_construction_professions); 
				
				
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($select_fields, $where_fields, $params)
	{
		try
		{
			$this->get_profession_list($select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(profession_id) cnt");
				
			return $this->select_one($fields, MPIS_Model::tbl_param_professions);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_profession($profession_id)
	{
		try
		{			
			$this->delete_data(MPIS_Model::tbl_param_professions, array('profession_id'=> $profession_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	
	public function get_specific_profession($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_one($fields, MPIS_Model::tbl_param_professions, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function insert_profession($params){
				
		try
		{
			$val 					= array();
			$val['profession']		= filter_var($params['profession_name'], FILTER_SANITIZE_STRING);
			$val['short_name']		= filter_var($params['short_name'], FILTER_SANITIZE_STRING);
			$val['active_flag']		= !EMPTY($params['active_flag']) ? $params['active_flag'] : 0;
			$val["created_by"] 		= $this->session->userdata("employee_no");
			$val["created_date"] 	= date('Y-m-d H:i:s');

			return $this->insert_data(MPIS_Model::tbl_param_professions, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
	}




	public function update_profession($params)
	{
		try
		{
			
			$val 						= array();
			$val['profession']			= filter_var($params['profession_name'], FILTER_SANITIZE_STRING);
			$val['short_name']			= filter_var($params['short_name'], FILTER_SANITIZE_STRING);
			$val['active_flag']			= !EMPTY($params['active_flag']) ? $params['active_flag'] : 0;
			$val["last_modified_by"] 	= $this->session->userdata("employee_no");
			$val["last_modified_date"] 	= date('Y-m-d H:i:s');
			
			
			$where 						= array();
			$where["profession_id"]	= $params["profession_id"];

			$this->update_data(MPIS_Model::tbl_param_professions, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */