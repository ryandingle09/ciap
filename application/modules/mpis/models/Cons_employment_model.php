<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cons_employment_model extends MPIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
		
	public function get_cons_employment_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
						
			$filter_where 		= $where["search_str"];
			$filter_params 		= $where["search_params"];
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				
				$filter_where
				
				GROUP BY A.construction_employment_id
				
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query, 
				MPIS_Model::tbl_construction_employments
			);
			
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($select_fields, $where_fields, $params)
	{
		try
		{
			
			$this->get_cons_employment_list($select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(construction_employment_id) cnt");
						
			return $this->select_one($fields, MPIS_Model::tbl_construction_employments);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_cons_employment($cons_employment_id)
	{
		try
		{			
			$this->delete_data(MPIS_Model::tbl_construction_employments, array('construction_employment_id'=>$cons_employment_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	
	public function delete_cons_employment_groups($cons_employment_id)
	{
		try
		{
			$this->delete_data(MPIS_Model::tbl_construction_employment_groups, array('construction_employment_id'=>$cons_employment_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_specific_cons_employment($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_one($fields, MPIS_Model::tbl_construction_employments, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_cons_employment_groups($where){
	
		try
		{
			$fields = array("*");
	
			return $this->select_all($fields, MPIS_Model::tbl_construction_employment_groups, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	
	public function get_specific_cons_employment_groups($cons_employment_id, $group_id = NULL){
	
		try
		{
			$where 	= "";
			$val	= array($cons_employment_id);
			
			IF(!EMPTY($group_id))
			{
				$where = " WHERE A.industry_group_id = ? ";
				$val[] = $group_id;
			}
								
			$query = <<<EOS
				SELECT A.industry_group_id, A.industry_group_name, B.employed_persons
				FROM %s A
				JOIN %s B ON A.industry_group_id = B.industry_group_id
				AND B.construction_employment_id = ?
				$where
EOS;

			$query  = sprintf($query, MPIS_Model::tbl_param_major_industry_groups, MPIS_Model::tbl_construction_employment_groups);
			
			return $this->query($query, $val);
	
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function insert_cons_employment($params){
				
		try
		{

			$val 					= array();			
			$val['year']			= filter_var($params['year'], FILTER_SANITIZE_NUMBER_INT);
			$val['quarter']			= filter_var($params['quarter'], FILTER_SANITIZE_NUMBER_INT);
			$val["created_by"] 		= $this->session->userdata("employee_no");
			$val["created_date"] 	= date('Y-m-d H:i:s');
				

			return $this->insert_data(MPIS_Model::tbl_construction_employments, $val, TRUE);			
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function insert_cons_employment_groups($params){
	
		try
		{
						
			foreach($params['groups'] as $key => $employed_persons)
			{
				$field 									= array();
				$field['construction_employment_id']	= $params["cons_employment_id"];
				$field['industry_group_id']				= $key;
				$field['employed_persons']				= filter_var(str_replace(',', '', $employed_persons), FILTER_SANITIZE_NUMBER_INT);
				
				
				$this->insert_data(MPIS_Model::tbl_construction_employment_groups, $field);
			}				
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}	



	public function update_cons_employment($params)
	{
		try
		{

			
			$val 						= array();			
			$val['year']				= filter_var($params['year'], FILTER_SANITIZE_NUMBER_INT);
			$val['quarter']				= filter_var($params['quarter'], FILTER_SANITIZE_NUMBER_INT);			
			$val["last_modified_by"] 	= $this->session->userdata("employee_no");
			$val["last_modified_date"] 	= date('Y-m-d H:i:s');
			
			
			$where 									= array();
			$where["construction_employment_id"]	= $params["construction_employment_id"];

			$this->update_data(MPIS_Model::tbl_construction_employments, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	

	public function update_cons_employment_group($params){
	
		try
		{
				
			$val						= array();
			$val['employed_persons']	= filter_var(str_replace(',', '', $params['employed_persons']), FILTER_SANITIZE_NUMBER_INT);
									
			$where									= array();
			$where['construction_employment_id']	= $params["cons_employment_id"];
			$where['industry_group_id']				= $params["industry_group_id"];

			$this->update_data(MPIS_Model::tbl_construction_employment_groups, $val, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}	
	
	public function get_cons_employment_reports($where)
	{
		try
		{
			$where_arr	= $this->get_where_statement($where);
			$where 		= $where_arr['where'];
			$val		= $where_arr['val'];
			
			$query = <<<EOS
				SELECT A.industry_group_id, B.year, SUM(A.employed_persons) employed_persons 
				FROM construction_employment_groups A			
				
				JOIN construction_employments B 
				ON A.construction_employment_id = B.construction_employment_id
				
				$where
				
				GROUP BY A.industry_group_id, B.year
EOS;


			return $this->query($query, $val);
		}
		catch(PDOException $e)
		{
			
		}
	}
	
}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */