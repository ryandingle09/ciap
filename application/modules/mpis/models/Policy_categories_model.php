<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Policy_categories_model extends MPIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
		
	public function get_policy_categories_list($select_fields, $where_fields, $params)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
			
			$filter_where 	= $where["search_str"];
			$filter_params 	= $where["search_params"];
			
		
		
						
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				
				LEFT JOIN (
					SELECT policy_category_id, COUNT(policy_category_id) policy_total
					FROM %s
					GROUP BY policy_category_id
				)B ON A.policy_category_id = B.policy_category_id
				
				
				$filter_where
				
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query, MPIS_Model::tbl_param_policy_categories, MPIS_Model::tbl_policies); 
				
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($select_fields, $where_fields, $params)
	{
		try
		{
			$this->get_policy_categories_list($select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(policy_category_id) cnt");
				
			return $this->select_one($fields, MPIS_Model::tbl_param_policy_categories);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_policy_category($policy_category_id)
	{
		try
		{			
			$this->delete_data(MPIS_Model::tbl_param_policy_categories, array('policy_category_id'=> $policy_category_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_indicators_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("indicator_id" => $id);
				
			return $this->select_one($fields, MPIS_Model::tbl_indicators, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}
	
	public function get_specific_policy_category($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_one($fields, MPIS_Model::tbl_param_policy_categories, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function insert_policy_category($params){
				
		try
		{
			$val 							= array();
			$val['policy_category_name']	= filter_var($params['policy_category_name'], FILTER_SANITIZE_STRING);
			$val['policy_category_desc']	= filter_var($params['policy_category_desc'], FILTER_SANITIZE_STRING);
			$val["created_by"] 				= $this->session->userdata("employee_no");
			$val["created_date"] 			= date('Y-m-d H:i:s');

			return $this->insert_data(MPIS_Model::tbl_param_policy_categories, $val, TRUE);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
		
			throw $e;
		}
	}




	public function update_policy_category($params)
	{
		try
		{
			
			$val 							= array();
			$val['policy_category_name']	= filter_var($params['policy_category_name'], FILTER_SANITIZE_STRING);
			$val['policy_category_desc']	= filter_var($params['policy_category_desc'], FILTER_SANITIZE_STRING);
			$val["last_modified_by"] 		= $this->session->userdata("employee_no");
			$val["last_modified_date"] 		= date('Y-m-d H:i:s');
			
			
			$where 							= array();
			$where["policy_category_id"]	= $params["policy_category_id"];

			$this->update_data(MPIS_Model::tbl_param_policy_categories, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */