<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Building_construction_model extends MPIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}	
		
	public function get_bldg_cons_list($bldg_cons_type, $select_fields, $where_fields, $params)
	{
		try
		{	
			$fields	= implode(',', $select_fields);
			$where	= $this->filtering($where_fields, $params, FALSE);
			$order	= $this->ordering($where_fields, $params);
			$limit	= $this->paging($params);
			
			
			
			
			$filter_where 		= $where["search_str"];
			$filter_params 		= $where["search_params"];
			$filter_params[]	= $bldg_cons_type;
		
						
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM %s A
				JOIN %s B ON A.building_construction_type_id = B.building_construction_type_id
				JOIN %s C ON A.building_construction_source = C.source_id
				
				WHERE 1 = 1 
				
				$filter_where
				AND A.building_construction_type_id = ?
				
				GROUP BY A.building_construction_id
				
	        	$order
	        	$limit
EOS;
			
			$query	= sprintf($query, 
				MPIS_Model::tbl_building_constructions, 
				MPIS_Model::tbl_param_building_construction_types, 
				MPIS_Model::tbl_param_building_construction_sources
			);
			
			$stmt 	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($bldg_cons_type, $select_fields, $where_fields, $params)
	{
		try
		{
			
			$this->get_bldg_cons_list($bldg_cons_type, $select_fields, $where_fields, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length($bldg_cons_type)
	{
		try
		{
			$fields = array("COUNT(building_construction_id) cnt");
			$where	= array("building_construction_type_id" => $bldg_cons_type);
				
			return $this->select_one($fields, MPIS_Model::tbl_building_constructions, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function delete_bldg_cons($bldg_cons_id)
	{
		try
		{			
			$this->delete_data(MPIS_Model::tbl_building_constructions, array('building_construction_id'=>$bldg_cons_id));	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}
	
	public function delete_bldg_cons_regions($bldg_cons_id)
	{
		try
		{
			$this->delete_data(MPIS_Model::tbl_building_construction_regions, array('building_construction_id'=>$bldg_cons_id));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_specific_bldg_cons($where){
	
		try
		{
			$fields = array("*");
				
			return $this->select_one($fields, MPIS_Model::tbl_building_constructions, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function get_bldg_cons_regions($where){
	
		try
		{
			$fields = array("*");
	
			return $this->select_all($fields, MPIS_Model::tbl_building_construction_regions, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	
	public function get_specific_bldg_cons_regions($bldg_cons_id, $region_id = NULL){
	
		try
		{
			$where 	= "";
			$val	= array($bldg_cons_id);
			
			IF(!EMPTY($region_id))
			{
				$where = " WHERE A.region_id = ? ";
				$val[] = $region_id;
			}
								
			$query = <<<EOS
				SELECT A.region_id, A.region_name, B.number, B.floor_area, B.value
				FROM %s A
				JOIN %s B ON A.region_id = B.region_id
				AND B.building_construction_id = ?
				$where
EOS;

			$query  = sprintf($query, MPIS_Model::tbl_param_building_construction_regions, MPIS_Model::tbl_building_construction_regions);
			
			return $this->query($query, $val);
	
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}
	
	public function insert_bldg_cons($params){
				
		try
		{

			$val 									= array();
			$val['building_construction_type_id']	= $params['bldg_cons_type'];
			$val['building_construction_source']	= filter_var($params['source_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['building_construction_year']		= filter_var($params['year'], FILTER_SANITIZE_NUMBER_INT);
			$val['building_construction_quarter']	= filter_var($params['quarter'], FILTER_SANITIZE_NUMBER_INT);
			$val["created_by"] 						= $this->session->userdata("employee_no");
			$val["created_date"] 					= date('Y-m-d H:i:s');
				

			return $this->insert_data(MPIS_Model::tbl_building_constructions, $val, TRUE);			
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function insert_bldg_cons_regions($params){
	
		try
		{
			
			
			$numbers 	= $params['number'];
			$floors 	= $params['floor'];
			$values 	= $params['value'];
			
			foreach($numbers as $key => $number)
			{
				$field 								= array();
				$field['building_construction_id']	= $params["building_construction_id"];
				$field['region_id']					= $key;
				$field['number']					= filter_var(str_replace(',', '', $number), FILTER_SANITIZE_NUMBER_INT);
				$field['floor_area']				= filter_var(str_replace(',', '', $floors[$key]), FILTER_SANITIZE_NUMBER_INT);
				$field['value']						= filter_var(str_replace(',', '', $values[$key]), FILTER_SANITIZE_NUMBER_INT);
				
				$this->insert_data(MPIS_Model::tbl_building_construction_regions, $field);
			}
						
				
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}	



	public function update_bldg_cons($params)
	{
		try
		{

			
			$val 									= array();
			$val['building_construction_type_id']	= $params['bldg_cons_type'];
			$val['building_construction_source']	= filter_var($params['source_id'], FILTER_SANITIZE_NUMBER_INT);
			$val['building_construction_year']		= filter_var($params['year'], FILTER_SANITIZE_NUMBER_INT);
			$val['building_construction_quarter']	= filter_var($params['quarter'], FILTER_SANITIZE_NUMBER_INT);			
			$val["last_modified_by"] 				= $this->session->userdata("employee_no");
			$val["last_modified_date"] 				= date('Y-m-d H:i:s');
			
			
			$where 									= array();
			$where["building_construction_id"]		= $params["bldg_cons_id"];

			$this->update_data(MPIS_Model::tbl_building_constructions, $val, $where);

		}
		
		catch (PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch (Exception $e)
		{			
			$this->rlog_error($e);
		}
	}
	

	public function update_bldg_cons_regions($params){
	
		try
		{
				
			$val				= array();
			$val['number']		= filter_var(str_replace(',', '', $params['number']), FILTER_SANITIZE_NUMBER_INT);
			$val['floor_area']	= filter_var(str_replace(',', '', $params['floor']), FILTER_SANITIZE_NUMBER_INT);
			$val['value']		= filter_var(str_replace(',', '', $params['value']), FILTER_SANITIZE_NUMBER_INT);
		
							
			$where								= array();
			$where['building_construction_id']	= $params["bldg_cons_id"];
			$where['region_id']					= $params["region"];

			$this->update_data(MPIS_Model::tbl_building_construction_regions, $val, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
	
			throw $e;
		}
	}	
	
	
	/*
	 * USED IN REPORTS MODULE
	 */
	
	public function get_building_construction_reports($where, $group_by = NULL)
	{
		
		try
		{
		
			$where_arr	= $this->get_where_statement($where);
			$where 		= $where_arr['where'];
			$val		= $where_arr['val'];
			
			$query = <<<EOS
				SELECT 
					A.building_construction_id,
					A.building_construction_type_id,
					A.building_construction_source,
					A.building_construction_year,
					A.building_construction_quarter,
					
					C.region_id,
					C.region_name,
					c.region_code,
					
					B.number, 
					B.floor_area,
					B.value
					
				FROM building_constructions A
				
				JOIN building_construction_regions B 
				ON A.building_construction_id = B.building_construction_id
				
				JOIN param_building_construction_regions C 
				ON B.region_id = C.region_id
					
				$where	
				
				$group_by
				
				ORDER BY A.building_construction_type_id, A.building_construction_source, A.building_construction_year, A.building_construction_quarter, B.region_id 		
			
EOS;
			return $this->query($query, $val);
			
		}
		catch(PDOException $e)
		{
			
		}
		
	}
	
	public function get_building_construction_summary_reports($where)
	{
	
		try
		{
	
			$where_arr	= $this->get_where_statement($where);
			$where 		= $where_arr['where'];
			$val		= $where_arr['val'];
			
			$query = <<<EOS
				SELECT 
					
					A.building_construction_type_id,					
					A.building_construction_year,
					A.building_construction_quarter,
					
					C.region_id,
					
					
					SUM(B.number) number, 
					SUM(B.floor_area) floor,
					SUM(B.value) value
					
				FROM building_constructions A
				
				JOIN building_construction_regions B 
				ON A.building_construction_id = B.building_construction_id
				
				JOIN param_building_construction_regions C 
				ON B.region_id = C.region_id
					
				$where	
				
				GROUP BY A.building_construction_type_id, A.building_construction_year, B.region_id					
						
				ORDER BY A.building_construction_type_id, A.building_construction_year, A.building_construction_quarter, B.region_id 		
			
EOS;
			
			return $this->query($query, $val);
				
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		
	}	
}

/* End of file indicator_model.php */
/* Location: ./application/modules/ceis/models/indicator_model.php */