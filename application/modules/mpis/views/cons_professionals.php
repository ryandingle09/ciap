<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Construction Related Licensed Professionals</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Construction Related Licensed Professionals
		<span>Manage Construction Related Licensed Professionals</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p -l-md p-r-md">
	  	<?php 
	  		if($permission_add)
	  			echo '<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_cons_professionals" id="add_cons_professionals" name="add_cons_professionals" onclick="modal_cons_professionals_init(\''.$url.'\')">Create New cons_professionals</button>';
	  	?>

	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="cons_professionals_table">
  <thead>
	<tr>
	  <th width="20%">Year</th>
	  <th width="30%">Profession</th>
	  <th width="30%">Number</th>	  
	  <th width="20%" class="text-center">Actions</th>
	</tr>
  </thead>
  </table>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'mpis_cons_related_prof', 
		method		: 'delete_cons_professionals', 
		module		: '<?php echo PROJECT_MPIS ?>' });
</script>