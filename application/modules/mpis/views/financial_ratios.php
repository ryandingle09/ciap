<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Financial Ratios</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Financial Ratios
		<span>Manage Financial Ratios</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p -l-md p-r-md">
	  	<?php 
	  		if($permission_add)
	  			echo '<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_financial_ratios" id="add_financial_ratios" name="add_financial_ratios" onclick="modal_financial_ratios_init(\''.$url.'\')">Create New Financial Ratio</button>';
	  	?>

	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="financial_ratios_table">
  <thead>
	<tr>
	  <th width="10%">Year</th>
	  <th width="25%">Business Type</th>
	  <th width="25%">Category</th>
	  <th width="25%">Company Name</th>	  
	  <th width="15%" class="text-center">Actions</th>
	</tr>
  </thead>
  </table>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'mpis_financial_ratios', 
		method		: 'delete_financial_ratio', 
		module		: '<?php echo PROJECT_MPIS ?>' });
</script>