<form id="profession_form">

	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	
	<div class="form-float-label">
	
	
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<?php 
						
						$profession 	= '';
						$class			= '';
						IF(!EMPTY($info['profession']))
						{
							$profession	= $info['profession'];
							$class		= 'active';
						}
					
					?>
					<input type="text" class="validate" required="" aria-required="true" name="profession_name" id="profession_name" value="<?php echo $profession ?>"/>
			    	<label for="profession_name" class="<?php echo $class ?>">Profession</label>
		      	</div>
		    </div>
		</div>
		
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<?php 
						
						$short_name 	= '';
						$class			= '';
						IF(!EMPTY($info['short_name']))
						{
							$short_name	= $info['short_name'];
							$class		= 'active';
						}
					
					?>
					<input type="text" class="validate" required="" aria-required="true" name="short_name" id="short_name" value="<?php echo $short_name ?>"/>
			    	<label for="short_name" class="<?php echo $class ?>">Short name</label>
		      	</div>
		    </div>
		</div>
				
		<div class="row">
			<div class="col s12">
				<div class="input-field col s6 m-t-xs m-b-xs m-l-n">
					
					
					<?php 
						$status	= (ISSET($info['active_flag'])) ? $info['active_flag'] : 1;
					?>
					<input type="checkbox" class="labelauty" name="active_flag" id="active_flag" value="<?php echo !is_null($status) ? (  ($status == '1') ? '1': '0' ) : '1' ?>" data-labelauty="In-Active|Active" <?php echo !is_null($status) ? (  ($status == 1) ? "checked" : ""  ) : "checked" ?> />

				</div>
			</div>
		</div>				
	  
	</div>
	
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	  	<?php if($permission_save):?>
	    	<button class="btn waves-effect waves-light" id="save_btn" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<?php endif; ?>
	</div>
</form>

<script>

$(function(){
  
  
  var $form = $('#profession_form');
  
  $form.parsley();
  $form.submit(function(e) {
	  
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {

	  var data = $(this).serialize();
	  
	  button_loader('save_btn', 1);
	  
	  $.post("<?php echo base_url() . PROJECT_MPIS ?>/mpis_code_libraries_professions/process/", data, function(result) {
		  
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  
		  button_loader('save_btn', 0);
		  
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  
		  button_loader("save_btn",0);
		  
		  modal_profession.closeModal();
		  
		  load_datatable('profession_table', '<?php echo PROJECT_MPIS ?>/mpis_code_libraries_professions/get_profession_list/');
		}
		
	  }, 'json');       
    }
  });

  $(".labelauty").change(function () {
		var val = $(this).val();
		(val == 1) ? $(this).val('0') : $(this).val('1');
	});
});
</script>