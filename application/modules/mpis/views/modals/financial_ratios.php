<form id="financial_ratio_form">

	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	
	<div class="form-float-label">
	
	
		<div class="row m-n">
			<div class="col s4">
				<div class="input-field">
					<label class="active" for="year">Year</label>
					<select name="year" id="year" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select year</option>
						<?php 
							
							for($year = $year_start; $year > $year_end; $year--)
							{
								$selected = ( !EMPTY($info['financial_ratio_year']) AND $info['financial_ratio_year'] == $year) ? "selected" : "";
								echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
							}				
						?>
					</select>           
				</div>
			</div>		
		
			<div class="col s4">
				<div class="input-field">
					<label class="active" for="company_type">Business Type</label>
					<select name="company_type" id="company_type" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select business type</option>
						<?php 
							foreach($company_types as $company_types)
							{
								$selected = ( !EMPTY($info['company_type_id']) AND $info['company_type_id'] == $company_types['company_type_id']) ? "selected" : "";
								echo '<option '.$selected.' value="'.$company_types['company_type_id'].'">'.$company_types['company_type_name'].'</option>';
							}				
						?>
					</select>           
				</div>
			</div>
			
			<div class="col s4">
				<div class="input-field">
					<label class="active" for="category">Category</label>
					<select name="category" id="category" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select category</option>
						<?php 
							foreach($categories as $categories)
							{
								$selected = ( !EMPTY($info['license_category_id']) AND $info['license_category_id'] == $categories['license_category_id']) ? "selected" : "";
								echo '<option '.$selected.' value="'.$categories['license_category_id'].'">'.$categories['license_category'].'</option>';
							}				
						?>
					</select>           
				</div>
			</div>
		</div>
		
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
			  
		  		<?php				
					$company_name	= "";
					$class			= "";
					IF(!EMPTY($info['company_name']))
					{
						$company_name 	= $info['company_name'];
						$class		 	= 'class="active"';
					}											
				?>
			    <input type="text" class="validate" required="" aria-required="true" name="company_name" id="company_name" value="<?php echo $company_name ?>"/>
			    <label for="company_name" <?php echo $class ?>>Company Name</label>
		      </div>			
			</div>
		</div>
		
		<div class="row m-n">
			<div class="col s12">
				<table>
					<thead>
						<th>ITEM</th>
						<th>AMOUNT</th>						
					</thead>
					
					<tbody>
						<?php 
							foreach($items as $items)
							{
								
								$amount = !EMPTY($items['amount']) ? $items['amount'] : 0;
							
								
								echo '
									<tr>
										<td>'.$items['financial_ratio_item_name'].'</td>
										<td><input class="right-align" type="text" name="items['.$items['financial_ratio_item_id'].']" value="'.number_format($amount, 2).'"></td>										
									</tr>
			
								';
							}
						
						?>
					</tbody>
				</table>
			</div>
		</div>
			  
	</div>
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	  	<?php if($permission_save):?>
	    	<button class="btn waves-effect waves-light" id="save_btn" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<?php endif; ?>
	</div>
</form>

<script>

$(function(){
  
  var $form = $('#financial_ratio_form');
  
  $form.parsley();
  
  $form.submit(function(e) {
	  
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {


	  var data = $(this).serialize();
	  
	  button_loader('save_btn', 1);
	  
	  $.post("<?php echo base_url() . PROJECT_MPIS ?>/mpis_financial_ratios/process/", data, function(result) {
		  
		if(result.flag == 0){
			
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  
		  button_loader('save_btn', 0);
		  
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  
		  button_loader("save_btn",0);
		  
		  modal_financial_ratios.closeModal();
		  
		  load_datatable('financial_ratios_table', '<?php echo PROJECT_MPIS ?>/mpis_financial_ratios/get_financial_ratios_list/');
		}
		
	  }, 'json');       
    }
  });
  

});
</script>