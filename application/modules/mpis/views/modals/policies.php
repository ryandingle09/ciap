<form id="policy_form">

	<input type="hidden" id="policy_security"  name="policy_security" value="<?php echo ISSET($security) ? $security : "" ?>">
	
	<div class="form-float-label">
	
	
		<div class="row m-n">
			<div class="col s6">
				<div class="input-field">
					<label class="active" for="policy_category">Policy Category</label>
					<select name="policy_category" id="policy_category" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select policy category</option>
						<?php 
							
							foreach($categories as $categories)
							{
								$selected = ( !EMPTY($info['policy_category_id']) AND $info['policy_category_id'] == $categories['policy_category_id']) ? "selected" : "";
								echo '<option '.$selected.' value="'.$categories['policy_category_id'].'">'.$categories['policy_category_name'].'</option>';
							}				
						?>
					</select>           
				</div>
			</div>	
			
			<div class="col s6">
				<div class="input-field">
					<label class="active" for="board">Board</label>
					<select name="board" id="board" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select board</option>
						<?php 
							
							foreach($boards as $boards)
							{
								$selected = ( !EMPTY($info['board_id']) AND $info['board_id'] == $boards['board_id']) ? "selected" : "";
								echo '<option '.$selected.' value="'.$boards['board_id'].'">'.$boards['board_code'].'</option>';
							}				
						?>
					</select>           
				</div>
			</div>	
						
		</div>
		
		<div class="row m-n"></div>
		
	</div>
		
	<div class="form-float-label" id="policy-container">	
	</div>
	
	
	
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	  	<?php if($permission_save):?>
	    	<button class="btn waves-effect waves-light" id="save_btn" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<?php endif; ?>
	</div>
</form>

<script>

$(function(){

	load_form();
	
	$("#policy_category").on('change', function(){
		load_form();
	});


	function load_form()
	{
		var category	= $("#policy_category").val();
		var security	= $("#policy_security").val();

		if(category == '' || category == '0')
			return;
		
		$.post("<?php echo base_url() . PROJECT_MPIS ?>/mpis_policies/load_form/", {category: category, security: security}, function(result) {
			$("#policy-container").html(result);
		});
	}
	
	var $form = $('#policy_form');
  
	$form.parsley();
	$form.submit(function(e) {
	  
		e.preventDefault();

		for (instance in CKEDITOR.instances) {
			CKEDITOR.instances[instance].updateElement();
		}		
	    
		if ( $(this).parsley().isValid() ) {

			var data = $(this).serialize();
	  
			button_loader('save_btn', 1);
	  
			$.post("<?php echo base_url() . PROJECT_MPIS ?>/mpis_policies/process/", data, function(result) {
		  
				if(result.flag == 0){
					notification_msg("<?php echo ERROR ?>", result.msg);
		  
					button_loader('save_btn', 0);
		  
				} else {
		  			notification_msg("<?php echo SUCCESS ?>", result.msg);
		  
		  			button_loader("save_btn",0);
		  
		  			modal_policy.closeModal();
		  
		  			load_datatable('policies_table', '<?php echo PROJECT_MPIS ?>/mpis_policies/get_policies_list/');
				}		
	  		}, 'json');       
    	}
 	});
});
</script>