<?php 

	if(EMPTY($info))
	{
		echo '
			<div class="table-display b-b" style="border-style:dashed!important; border-color:#ddd!important;">
				<div class="table-cell p-md" style="width:15%; border-style:dashed">
				No record/s found.
				</div>
			</div>
		';
	}
	else
	{
		foreach($info as $info)
		{
			echo '
				<div class="table-display b-b" style="border-style:dashed!important; border-color:#ddd!important;">
					<div class="table-cell p-md" style="width:15%; border-style:dashed">
					'.$info['agenda_code'].'
					</div>
					
					<div class="table-cell p-md" style="width:70%;">
					'.$info['agenda_title'].'
					</div>
					
					<div class="table-cell p-md" style="width:15%;">
							
							
							<a href="javascript:;" class="md-trigger edit tooltipped" data-tooltip="Edit" data-position="bottom" data-delay="50" data-modal="modal_policy" onclick="modal_policy_init(\'\')"></a>
							<a href="javascript:;" onclick="" class="delete tooltipped" data-tooltip="Delete" data-position="bottom" data-delay="50"></a
						
					</div>
				</div>
					
				<div class="p-md"> 	
					'.$info['agenda_body'].'
				</div>					
			';
		}
		
	}
?>


