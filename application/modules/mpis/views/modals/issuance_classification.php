<form id="issuance_type_form">

	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	
	<div class="form-float-label">
	
	
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<?php 
						
						$issuance_type_name 	= '';
						$class					= '';
						IF(!EMPTY($info['issuance_type_name']))
						{
							$issuance_type_name 	= $info['issuance_type_name'];
							$class					= 'active';
						}
					
					?>
					<input type="text" class="validate" required="" aria-required="true" name="issuance_type_name" id="issuance_type_name" value="<?php echo $issuance_type_name ?>"/>
			    	<label for="issuance_type_name" class="<?php echo $class ?>">Issuance Classification</label>
		      	</div>
		    </div>
		</div>
		
		<div class="row m-n">
		    <div class="col s12">
				<div class="input-field">
					<?php 						
						$description	= '';
						$class			= '';
						IF(!EMPTY($info['issuance_type_desc']))
						{
							$description	= $info['issuance_type_desc'];
							$class			= 'active';
						}
					?>
					<textarea  class="materialize-textarea" id="issuance_type_desc" name="issuance_type_desc" value="<?php echo $description ?>"><?php echo $description ?></textarea>
					<label for="issuance_type_desc" class="<?php echo $class ?>">Description</label>
				</div>
			</div>
		</div>		
	  
	</div>
	
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	  	<?php if($permission_save):?>
	    	<button class="btn waves-effect waves-light" id="save_btn" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<?php endif; ?>
	</div>
</form>

<script>

$(function(){
  
  
  var $form = $('#issuance_type_form');
  
  $form.parsley();
  $form.submit(function(e) {
	  
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {

	  var data = $(this).serialize();
	  
	  button_loader('save_btn', 1);
	  
	  $.post("<?php echo base_url() . PROJECT_MPIS ?>/mpis_code_libraries_issuance_classifications/process/", data, function(result) {
		  
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  
		  button_loader('save_btn', 0);
		  
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  
		  button_loader("save_btn",0);
		  
		  modal_issuance_classification.closeModal();
		  
		  load_datatable('issuance_classification_table', '<?php echo PROJECT_MPIS ?>/mpis_code_libraries_issuance_classifications/get_issuance_classification_list/');
		}
		
	  }, 'json');       
    }
  });
});
</script>