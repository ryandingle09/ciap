<form id="cons_professionals_form">

	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	
	<div class="form-float-label">
	
	
	<div class="row m-n">
	
		<div class="col s4">
			<div class="input-field">
				<label class="active" for="year">Year</label>
				<select name="year" id="year" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
					<option value="">select year</option>
					<?php 
						
						for($year = $year_start; $year >= $year_end; $year--)
						{
							$selected = ( !EMPTY($info['year']) AND $info['year'] == $year) ? "selected" : "";
							echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
						}				
					?>
				</select>           
			</div>
		</div>	
	
		<div class="col s8">
			<div class="input-field">
				<label class="active" for="profession">Profession</label>
				<select name="profession" id="profession" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
					<option value="">select profession</option>
					<?php 
						foreach($professions as $professions)
						{
							$selected = ( !EMPTY($info['profession_id']) AND $info['profession_id'] == $professions['profession_id']) ? "selected" : "";
							echo '<option '.$selected.' value="'.$professions['profession_id'].'">'.$professions['profession'].'</option>';
						}				
					?>
				</select>           
			</div>
		</div>
	    
		
	</div>
	
	
	  <div class="row m-n">	 	 
	    <div class="col s10">
		  <div class="input-field">
		  
		  	<?php				
				$number = !EMPTY($info['number']) ? $info['number'] : "0";														
			?>
			
		    <input type="text" class="validate" required="" aria-required="true" name="number" id="number" value="<?php echo number_format($number, 0) ?>"/>
		    <label for="number" class="active">Number</label>
	      </div>
	    </div>
	  </div>	 	 
	</div>
	
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	  	<?php if($permission_save):?>
	    	<button class="btn waves-effect waves-light" id="save_btn" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<?php endif; ?>
	</div>
</form>

<script>

$(function(){
  
  var $form = $('#cons_professionals_form');
  
  $form.parsley();
  $form.submit(function(e) {
	  
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {

		
	  var data = $(this).serialize();
	  
	  button_loader('save_btn', 1);
	  
	  $.post("<?php echo base_url() . PROJECT_MPIS ?>/mpis_cons_related_prof/process/", data, function(result) {
		  
		if(result.flag == 0){
			
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  
		  button_loader('save_btn', 0);
		  
		} else {
			
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  
		  button_loader('save_btn', 0);
		  
		  modal_cons_professionals.closeModal();
		  
		  load_datatable('cons_professionals_table', '<?php echo PROJECT_MPIS ?>/mpis_cons_related_prof/get_cons_professionals_list/');
		}
		
	  }, 'json');       
    }
  });
  
});
</script>