<form id="agenda_form">

	<input type="hidden" name="agenda_security" value="<?php echo ISSET($agenda_security) ? $agenda_security : "" ?>">
		
	<div class="form-float-label">
	
		<div class="row m-n">
	 		<div class="col s2">
				<div class="input-field">
		  
					<?php				
						$agenda_code = "";
						$class			= "";
						IF(!EMPTY($info['agenda_code']))
						{
							$agenda_code	= $info['agenda_code'];
							$class		 	= 'class="active"';
						}											
					?>
				
			    	<input type="text" class="validate" required="" aria-required="true" name="code" id="code" value="<?php echo $agenda_code ?>"/>
			    	<label for="code" <?php echo $class ?>>Code</label>
	      		</div>
	    	</div>
	    	
	    	<div class="col s10">
				<div class="input-field">
		  
					<?php				
						$agenda_title = "";
						$class			= "";
						IF(!EMPTY($info['agenda_title']))
						{
							$agenda_title	= $info['agenda_title'];
							$class		 	= 'class="active"';
						}											
					?>
				
			    	<input type="text" class="validate" required="" aria-required="true" name="title" id="title" value="<?php echo $agenda_title ?>"/>
			    	<label for="title" <?php echo $class ?>>Title</label>
	      		</div>
	    	</div>
		</div>
	  
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<?php 
						$agenda_body = !EMPTY($info['agenda_body']) ? $info['agenda_body'] : ''; 
					?>
					<textarea  required="" aria-required="true" id="body" name="body" value="<?php echo $agenda_body ?>"><?php echo $agenda_body ?></textarea>
				</div>
			</div>
		</div>	  
	</div>
	
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	  	<?php if($permission_save):?>
	    	<button class="btn waves-effect waves-light" id="save_btn" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<?php endif; ?>
	</div>
</form>

<script>

$(function(){
  
  CKEDITOR.replace('body');
  
  var $form = $('#agenda_form');
  
  $form.parsley();
  $form.submit(function(e) {
	  
    e.preventDefault();

	var $policy_form	= $('#policy_form');
	var category 		= $('#policy_category', $policy_form).val();
	var board			= $('#board', $policy_form).val();
	var classification	= $('#classification', $policy_form).val();
	var bm_date			= $('#bm_date', $policy_form).val();


	if(category == '' || category == '0')
	{
		notification_msg("<?php echo ERROR ?>", 'Policy Category is required in policy form.');
		return false;
	}

	if(board == '' || board == '0')
	{
		notification_msg("<?php echo ERROR ?>", 'Board is required in policy form.');
		return false;
	}

	if(classification == '' || classification == '0')
	{
		notification_msg("<?php echo ERROR ?>", 'Classification is required in policy form.');
		return false;
	}

	if(bm_date == '')
	{
		notification_msg("<?php echo ERROR ?>", 'BM Date is required in policy form.');
		return false;
	}

	var policy_data	= $policy_form.serialize();
    
	if ( $(this).parsley().isValid() ) {


		for (instance in CKEDITOR.instances) {
			CKEDITOR.instances[instance].updateElement();
		}		
		
		var data = $(this).serialize() + "&" + policy_data;
	  	
		button_loader('save_btn', 1);
	  
		$.post("<?php echo base_url() . PROJECT_MPIS ?>/mpis_meeting_agendas/process/", data, function(result) {

			$("#policy_security", $policy_form).val(result.security);
			  
			if(result.flag == 0){
		  		notification_msg("<?php echo ERROR ?>", result.msg);
		  
		  		button_loader('save_btn', 0);
		  
			} else {
				notification_msg("<?php echo SUCCESS ?>", result.msg);
		  
		  		button_loader("save_btn",0);
		  
		  		modal_agenda.closeModal();

		  		load_datatable('agendas_table', '<?php echo PROJECT_MPIS ?>/mpis_meeting_agendas/get_agenda_list/' + result.security);
		  
				
			}
		
		}, 'json');       
	}
});

	function validate_policy()
	{
		
	}

});

</script>