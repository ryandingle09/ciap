
<div class="row m-n">
	<div class="col s3">
		<div class="input-field">
			<label class="active" for="classification">Classification</label>
			<select name="classification" id="classification" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
				<option value="">select classification</option>
				<?php 
					
					foreach($meeting_types as $meeting_types)
					{
						$selected = ( !EMPTY($info['board_meeting_type']) AND $info['board_meeting_type'] == $meeting_types['meeting_type_id']) ? "selected" : "";
						echo '<option '.$selected.' value="'.$meeting_types['meeting_type_id'].'">'.$meeting_types['meeting_type_name'].'</option>';
					}				
				?>
			</select>           
		</div>
	</div>
	
	<div class="col s3">
		<div class="input-field">
			<?php				
				$board_meeting_date = "";
				$class			= "";
				IF(!EMPTY($info['board_meeting_date']))
				{
					$board_meeting_date = date('m/d/Y', strtotime($info['board_meeting_date']));
					$class		 		= 'class="active"';
				}											
			?>
			<label <?php echo $class ?> for="bm_date">BM Date <i class="fa fa-calendar"></i></label>				
			<input name="bm_date" required="" aria-required="true" class="validate datepicker" size="16" type="text" value="<?php echo $board_meeting_date; ?>" data-date-format="yyyy-mm-dd" >
		</div>
	</div>
	
	<div class="col s2">
		<div class="input-field">
		  
			<?php				
				$bm_no	= "";
				$class	= "";
				IF(!EMPTY($info['board_meeting_no']))
				{
					$bm_no	= $info['board_meeting_no'];
					$class	= 'class="active"';
				}											
			?>
			<input type="text" class="validate" name="bm_no" id="bm_no" value="<?php echo $bm_no ?>"/>
			<label for="bm_no" <?php echo $class ?>>BM No</label>
		</div>
	</div>
	
	<div class="col s4">
		<div class="input-field col s6 m-t-xs m-b-xs m-l-n" style="width: 100% !important" >
			
			
			<?php 
				$flag	= (ISSET($info['gender_sensitive_flag'])) ? $info['gender_sensitive_flag'] : 1;
			?>
			<input type="checkbox" class="labelauty" name="gender_sensitive_flag" id="gender_sensitive_flag" value="<?php echo !is_null($flag) ? (  ($flag == '1') ? '1': '0' ) : '1' ?>" data-labelauty="This policy is not gender sensitive|This policy is gender sensitive" <?php echo !is_null($flag) ? (  ($flag == 1) ? "checked" : ""  ) : "checked" ?> />

		</div>
	</div>	
</div>

<div class="row m-n m-b-md p-b-md">
	<div class="col s6 b-r-n">
		<h5>Agenda</h5>
	</div>
	
	<div class="col s6 p-r-n right-align">
	  
		<div class="input-field inline p-l-md p-r-md">
			<button onclick="modal_agenda_init('<?php echo $agenda_url ?>');" type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_agenda" id="add_agenda" name="add_agenda">Add</button>
		</div>
	</div>	
</div>
	

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="agendas_table">
  <thead>
	<tr>
	  <th width="5%">Code</th>
	  <th width="15%">Title</th>
	  <th width="70%">Agenda</th>	  
	  <th width="10%" class="text-center">Actions</th>
	</tr>
  </thead>
  </table>
</div>

<script>
	var deleteObj = new handleData({ 
		controller	: 'mpis_meeting_agendas', 
		method		: 'delete_agenda', 
		module		: '<?php echo PROJECT_MPIS ?>' });
</script>

<script>
	$(function(){
		$(".labelauty").change(function () {
			var val = $(this).val();
			(val == 1) ? $(this).val('0') : $(this).val('1');
		});

		var $policy_form	= $("#policy_form");
		var security 		= $("#policy_security", $policy_form).val();
		load_datatable('agendas_table', '<?php echo PROJECT_MPIS ?>/mpis_meeting_agendas/get_agenda_list/' + security);
	});		
	
</script>

	
	