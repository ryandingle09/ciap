<form id="market_value_form">

	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	
	<div class="form-float-label">
			
		<div class="row m-n">
			<div class="col s6">
				<div class="input-field">
					<label class="active" for="year">Year</label>
					<select name="year" id="year" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select year</option>
						<?php 
							
							for($year = $year_start; $year > $year_end; $year--)
							{
								$selected = ( !EMPTY($info['market_value_year']) AND $info['market_value_year'] == $year) ? "selected" : "";
								echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
							}				
						?>
					</select>           
				</div>
			</div>		
		</div>	
		
		<div class="row m-n">
			<div class="col s12">
				<table>
					<thead>
						<th>&nbsp;</th>
						<th>CURRENT</th>
						<th>CONSTANT</th>
						
					</thead>
					
					<tbody>
						<?php 
							foreach($prices as $prices)
							{
								
								$current 	= !EMPTY($prices['current_price']) ? $prices['current_price'] : 0;
								$constant 	= !EMPTY($prices['constant_price']) ? $prices['constant_price'] : 0;
								
								
								echo '
									<tr>
										<td>'.$prices['price_type_name'].'</td>
										<td><input type="text" name="current['.$prices['price_type_id'].']" value="'.number_format($current,2).'"></td>
										<td><input type="text" name="constant['.$prices['price_type_id'].']" value="'.number_format($constant,2).'"></td>
									</tr>
			
								';
							}
						
						?>
					</tbody>
				</table>
			</div>
		</div>
			  
	</div>
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	  	<?php if($permission_save):?>
	    	<button class="btn waves-effect waves-light" id="save_btn" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<?php endif; ?>
	</div>
</form>

<script>

$(function(){
  
  var $form = $('#market_value_form');
  
  $form.parsley();
  
  $form.submit(function(e) {
	  
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {


	  var data = $(this).serialize();
	  
	  button_loader('save_btn', 1);
	  
	  $.post("<?php echo base_url() . PROJECT_MPIS ?>/<?php echo $controller ?>/process/", data, function(result) {
		  
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  
		  button_loader('save_btn', 0);
		  
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  
		  button_loader("save_btn",0);
		  
		  modal_market_value.closeModal();
		  
		  load_datatable('market_value_table', '<?php echo PROJECT_MPIS ?>/<?php echo $controller ?>/get_market_value_list/');
		}
		
	  }, 'json');       
    }
  });
  

});
</script>