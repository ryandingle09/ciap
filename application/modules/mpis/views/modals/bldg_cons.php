<form id="bldg_cons_form">

	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	
	<div class="form-float-label">
	
	
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<label class="active" for="source_id">Source</label>
					<select name="source_id" id="source_id" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select source</option>
						<?php 
							foreach($sources as $sources)
							{
								$selected = ( !EMPTY($info['building_construction_source']) AND $info['building_construction_source'] == $sources['source_id']) ? "selected" : "";
								echo '<option '.$selected.' value="'.$sources['source_id'].'">'.$sources['source_name'].'</option>';
							}				
						?>
					</select>           
				</div>
			</div>
		</div>
		
		<div class="row m-n">
			<div class="col s6">
				<div class="input-field">
					<label class="active" for="year">Year</label>
					<select name="year" id="year" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select year</option>
						<?php 
							
							for($year = $year_start; $year > $year_end; $year--)
							{
								$selected = ( !EMPTY($info['building_construction_year']) AND $info['building_construction_year'] == $year) ? "selected" : "";
								echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
							}				
						?>
					</select>           
				</div>
			</div>
			
			<div class="col s6">
				<div class="input-field">
					<label class="active" for="quarter">Quarter</label>
					<select name="quarter" id="quarter" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select quarter</option>
						<?php 
							foreach($quarters as $key => $val)
							{
								$selected = ( !EMPTY($info['building_construction_quarter']) AND $info['building_construction_quarter'] == $key) ? "selected" : "";
								echo '<option '.$selected.' value="'.$key.'">'.$val.'</option>';
							}				
						?>
					</select>           
				</div>
			</div>
		</div>	
		
		<div class="row m-n">
			<div class="col s12">
				<table>
					<thead>
						<th>REGION</th>
						<th>NUMBER</th>
						<th>FLOOR AREA (SQ. M)</th>
						<th>VALUE (1,000)</th>
					</thead>
					
					<tbody>
						<?php 
							foreach($regions as $regions)
							{
								
								$number = !EMPTY($regions['number']) ? $regions['number'] : 0;
								$floor 	= !EMPTY($regions['floor_area']) ? $regions['floor_area'] : 0;
								$value 	= !EMPTY($regions['value']) ? $regions['value'] : 0;
								
								echo '
									<tr>
										<td>'.$regions['region_name'].'</td>
										<td><input class="right-align" type="text" name="number['.$regions['region_id'].']" value="'.number_format($number,0).'"></td>
										<td><input class="right-align" type="text" name="floor['.$regions['region_id'].']" value="'.number_format($floor,0).'"></td>
										<td><input class="right-align" type="text" name="value['.$regions['region_id'].']" value="'.number_format($value,0).'"></td>
									</tr>
			
								';
							}
						
						?>
					</tbody>
				</table>
			</div>
		</div>
			  
	</div>
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	  	<?php if($permission_save):?>
	    	<button class="btn waves-effect waves-light" id="save_btn" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<?php endif; ?>
	</div>
</form>

<script>

$(function(){
  
  var $form = $('#bldg_cons_form');
  
  $form.parsley();
  
  $form.submit(function(e) {
	  
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {


	  var data = $(this).serialize();
	  
	  button_loader('save_btn', 1);
	  
	  $.post("<?php echo base_url() . PROJECT_MPIS ?>/mpis_<?php echo $modal_page ?>/process/", data, function(result) {
		  
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  
		  button_loader('save_btn', 0);
		  
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  
		  button_loader("save_btn",0);
		  
		  modal_bldg_cons.closeModal();
		  
		  load_datatable('bldg_cons_table', '<?php echo PROJECT_MPIS ?>/mpis_<?php echo $modal_page ?>/get_<?php echo $modal_page ?>_list/');
		}
		
	  }, 'json');       
    }
  });
  

});
</script>