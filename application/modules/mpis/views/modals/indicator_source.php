<form id="indicator_source_form">

	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	
	<div class="form-float-label">
	
	
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<?php 
						
						$indicator_source_name 	= '';
						$class					= '';
						IF(!EMPTY($info['indicator_source_name']))
						{
							$indicator_source_name 	= $info['indicator_source_name'];
							$class					= 'active';
						}
					
					?>
					<input type="text" class="validate" required="" aria-required="true" name="indicator_source_name" id="indicator_source_name" value="<?php echo $indicator_source_name ?>"/>
			    	<label for="indicator_source_name" class="<?php echo $class ?>">Indicator Source</label>
		      	</div>
		    </div>
		</div>
			  
	</div>
	
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	  	<?php if($permission_save):?>
	    	<button class="btn waves-effect waves-light" id="save_btn" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<?php endif; ?>
	</div>
</form>

<script>

$(function(){
  
  
  var $form = $('#indicator_source_form');
  
  $form.parsley();
  $form.submit(function(e) {
	  
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {

	  var data = $(this).serialize();
	  
	  button_loader('save_btn', 1);
	  
	  $.post("<?php echo base_url() . PROJECT_MPIS ?>/mpis_code_libraries_indicator_sources/process/", data, function(result) {
		  
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  
		  button_loader('save_btn', 0);
		  
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  
		  button_loader("save_btn",0);
		  
		  modal_indicator_source.closeModal();
		  
		  load_datatable('indicator_source_table', '<?php echo PROJECT_MPIS ?>/mpis_code_libraries_indicator_sources/get_indicator_source_list/');
		}
		
	  }, 'json');       
    }
  });
});
</script>