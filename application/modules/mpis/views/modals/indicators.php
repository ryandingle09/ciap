<form id="indicator_form">

	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	
	<div class="form-float-label">
	
	
	<div class="row m-n">
		<div class="col s6">
			<div class="input-field">
				<label class="active" for="indicator_source_type_id">Type of Source</label>
				<select name="indicator_source_type_id" id="indicator_source_type_id" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
					<option value="">select type of source</option>
					<?php 
						foreach($source_types as $source_types)
						{
							$selected = ( !EMPTY($info['indicator_source_type_id']) AND $info['indicator_source_type_id'] == $source_types['indicator_source_type_id']) ? "selected" : "";
							echo '<option '.$selected.' value="'.$source_types['indicator_source_type_id'].'">'.$source_types['indicator_source_type'].'</option>';
						}				
					?>
				</select>           
			</div>
		</div>
	    
		<div class="col s6">
			<div class="input-field">
				<label class="active" for="indicator_source_id">Source</label>
				<select name="indicator_source_id" id="indicator_source_id" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
					<option value="">select source</option>
					<?php 
						foreach($sources as $sources)
						{
							$selected = ( !EMPTY($info['indicator_source_id']) AND $info['indicator_source_type_id'] == $sources['indicator_source_id']) ? "selected" : "";
							echo '<option '.$selected.' value="'.$sources['indicator_source_id'].'">'.$sources['indicator_source_name'].'</option>';
						}				
					?>
				</select>           
			</div>
		</div>
	</div>
	
	<div class="row m-n">
		<div class="col s6">
			<div class="input-field">
				<label class="active" for="indicator_category_id">Category</label>
				<select name="indicator_category_id" id="indicator_category_id" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
					<option value="">select category</option>
					<?php 
						foreach($categories as $categories)
						{
							$selected = ( !EMPTY($info['indicator_source_id']) AND $info['indicator_category_id'] == $categories['indicator_category_id']) ? "selected" : "";
							echo '<option '.$selected.' value="'.$categories['indicator_category_id'].'">'.$categories['indicator_category_name'].'</option>';
						}				
					?>
				</select>           
			</div>
		</div>
	    
		<div class="col s4">
			<div class="input-field">
				<?php				
					$indicator_date = "";
					$class			= "";
					IF(!EMPTY($info['indicator_date']))
					{
						$indicator_date = date('m/d/Y', strtotime($info['indicator_date']));
						$class		 	= 'class="active"';
					}											
				?>
				<label <?php echo $class ?> for="indicator_date">Date <i class="fa fa-calendar"></i></label>				
				<input name="indicator_date" required="" aria-required="true" class="validate datepicker" size="16" type="text" value="<?php echo $indicator_date; ?>" data-date-format="yyyy-mm-dd" >
			</div>
		</div>
		
		 <div class="col s2">
		  <div class="input-field">
		  
	  		<?php				
				$indicator_page = "";
				$class			= "";
				IF(!EMPTY($info['indicator_page']))
				{
					$indicator_page = $info['indicator_page'];
					$class		 	= 'class="active"';
				}											
			?>
		    <input type="text" class="validate" required="" aria-required="true" name="indicator_page" id="indicator_page" value="<?php echo $indicator_page ?>"/>
		    <label for="indicator_page" <?php echo $class ?>>Page No</label>
	      </div>
	    </div>
	</div>
	
	  <div class="row m-n">
	 
	  
	    <div class="col s12">
		  <div class="input-field">
		  
		  	<?php				
				$indicator_title = "";
				$class			= "";
				IF(!EMPTY($info['indicator_title']))
				{
					$indicator_title 	= $info['indicator_title'];
					$class		 		= 'class="active"';
				}											
			?>
			
		    <input type="text" class="validate" required="" aria-required="true" name="indicator_title" id="indicator_title" value="<?php echo $indicator_title ?>"/>
		    <label for="indicator_title" <?php echo $class ?>>Article Title</label>
	      </div>
	    </div>
	  </div>
	  
	<div class="row m-n">
	    <div class="col s12">
			<div class="input-field">
				<?php 
					$indicator_abstract = !EMPTY($info['indicator_abstract']) ? $info['indicator_abstract'] : ''; 
				?>
				<textarea  equired="" aria-required="true" id="indicator_abstract" name="indicator_abstract" value="<?php echo $indicator_abstract ?>"><?php echo $indicator_abstract ?></textarea>
			</div>
		</div>
	</div>
	  
	</div>
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	  	<?php if($permission_save):?>
	    	<button class="btn waves-effect waves-light" id="save_indicator" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<?php endif; ?>
	</div>
</form>

<script>

$(function(){
  
  CKEDITOR.replace('indicator_abstract');
  
  var $form = $('#indicator_form');
  
  $form.parsley();
  $form.submit(function(e) {
	  
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {


		for (instance in CKEDITOR.instances) {
			CKEDITOR.instances[instance].updateElement();
		}		
		
	  var data = $(this).serialize();
	  
	  button_loader('save_indicator', 1);
	  
	  $.post("<?php echo base_url() . PROJECT_MPIS ?>/mpis_indicators/process/", data, function(result) {
		  
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  
		  button_loader('save_indicator', 0);
		  
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  
		  button_loader("save_indicator",0);
		  
		  modal_indicator.closeModal();
		  
		  load_datatable('indicator_table', '<?php echo PROJECT_MPIS ?>/mpis_indicators/get_indicator_list/');
		}
		
	  }, 'json');       
    }
  });
  
  <?php if(ISSET($position_name)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>
});
</script>