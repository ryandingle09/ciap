<form id="board_meeting_form">

	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	
	<div class="form-float-label">
	
	
		<div class="row m-n">
			<div class="col s12">
				<div class="input-field">
					<?php 
						
						$meeting_type_name	 	= '';
						$class					= '';
						IF(!EMPTY($info['meeting_type_name']))
						{
							$meeting_type_name	= $info['meeting_type_name'];
							$class				= 'active';
						}
					
					?>
					<input type="text" class="validate" required="" aria-required="true" name="meeting_type_name" id="meeting_type_name" value="<?php echo $meeting_type_name ?>"/>
			    	<label for="meeting_type_name" class="<?php echo $class ?>">Name</label>
		      	</div>
		    </div>
		</div>
			  
	</div>
	
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	  	<?php if($permission_save):?>
	    	<button class="btn waves-effect waves-light" id="save_btn" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<?php endif; ?>
	</div>
</form>

<script>

$(function(){
  
  
  var $form = $('#board_meeting_form');
  
  $form.parsley();
  $form.submit(function(e) {
	  
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {

	  var data = $(this).serialize();
	  
	  button_loader('save_btn', 1);
	  
	  $.post("<?php echo base_url() . PROJECT_MPIS ?>/mpis_code_libraries_board_meeting/process/", data, function(result) {
		  
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  
		  button_loader('save_btn', 0);
		  
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  
		  button_loader("save_btn",0);
		  
		  modal_board_meeting.closeModal();
		  
		  load_datatable('board_meeting_table', '<?php echo PROJECT_MPIS ?>/mpis_code_libraries_board_meeting/get_board_meeting_list/');
		}
		
	  }, 'json');       
    }
  });
});
</script>