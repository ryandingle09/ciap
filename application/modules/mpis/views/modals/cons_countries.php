<form id="cons_countries_form">

	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	
	<div class="form-float-label">
	
	
		<div class="row m-n">
			<div class="col s4">
				<div class="input-field">
					<label class="active" for="year">Year</label>
					<select name="year" id="year" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select year</option>
						<?php 
							
							for($year = $year_start; $year > $year_end; $year--)
							{
								$selected = ( !EMPTY($info['year']) AND $info['year'] == $year) ? "selected" : "";
								echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
							}				
						?>
					</select>           
				</div>
			</div>		
		
			<div class="col s8">
				<div class="input-field">
					<label class="active" for="country">Country</label>
					<select name="country" id="country" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select country</option>
						<?php 
							foreach($countries as $countries)
							{
								$selected = ( !EMPTY($info['country_code']) AND $info['country_code'] == $countries['country_code']) ? "selected" : "";
								echo '<option '.$selected.' value="'.$countries['country_code'].'">'.$countries['country_name'].'</option>';
							}				
						?>
					</select>           
				</div>
			</div>			
		</div>
		

		
		<div class="row m-n">
			<div class="col s12">
				<table>
					<thead>
						<th>INDICATOR</th>
						<th>VALUE</th>						
					</thead>
					
					<tbody>
						<?php 
							foreach($indicators as $indicators)
							{
								
								$value = !EMPTY($indicators['value']) ? $indicators['value'] : 0;
							
								
								echo '
									<tr>
										<td>'.$indicators['construction_indicator'].'</td>
										<td><input class="right-align" type="text" name="indicators['.$indicators['construction_indicator_id'].']" value="'.number_format($value, 2).'"></td>										
									</tr>
			
								';
							}
						
						?>
					</tbody>
				</table>
			</div>
		</div>
			  
	</div>
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	  	<?php if($permission_save):?>
	    	<button class="btn waves-effect waves-light" id="save_btn" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<?php endif; ?>
	</div>
</form>

<script>

$(function(){
  
  var $form = $('#cons_countries_form');
  
  $form.parsley();
  
  $form.submit(function(e) {
	  
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {


	  var data = $(this).serialize();
	  
	  button_loader('save_btn', 1);
	  
	  $.post("<?php echo base_url() . PROJECT_MPIS ?>/mpis_cons_countries/process/", data, function(result) {
		  
		if(result.flag == 0){
			
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  
		  button_loader('save_btn', 0);
		  
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  
		  button_loader("save_btn",0);
		  
		  modal_cons_countries.closeModal();
		  
		  load_datatable('cons_countries_table', '<?php echo PROJECT_MPIS ?>/mpis_cons_countries/get_cons_countries_list/');
		}
		
	  }, 'json');       
    }
  });
  

});
</script>