<form id="asset_form" class="asset_form">
	<input type="hidden" name="security" value="<?php echo ISSET($security) ? $security : "" ?>">
	<input type="hidden" name="salt" value="<?php echo ISSET($salt) ? $salt : "" ?>">
	<input type="hidden" name="token" value="<?php echo ISSET($token) ? $token : "" ?>">
	
	<div class="form-float-label">
		<div class="row m-n">
			<div class="col s6">
				<p class="head-genInfo">GENERAL INFORMATION</p>
			</div>
			<div class="col s3">
				<div class="input-field">
					<label class="active" for="condition">CONDITION</label>
					<select name="condition" id="condition" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select condition</option>
						<option value="2">Serviceable</option>
						<?php if(isset($condition)){ ?>
						<option selected="selected" value="3"><?php echo $condition; ?></option>
						<?php } ?>
						</select>	     
			   	</div>
			</div>
			<div class="col s3">
				<div class="input-field">
					<label class="active" for="status">STATUS</label>
					<select name="status" id="status" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<option value="">select status</option>
						<option value="2">Status 1</option>
						<option value="2">Status 2</option>
						<option value="2">Status 3</option>
						<?php if(isset($status)){ ?>
						<option selected="selected" value="3"><?php echo $status; ?></option>
						<?php } ?>
					</select>
			   	</div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s4">
		        <div class="input-field">
			    	<input id="property_no" type="text" class="validate active" value="<?php echo isset($property_no)?$property_no:''; ?>">
			    	<label for="property_no">PROPERTY NO.</label>
		        </div>
		        <div class="line-btm"></div>
		         <div class="input-field">
			    	<input id="serial_no" type="text" class="validate" value="<?php echo isset($serial_no)?$serial_no:''; ?>">
			    	<label for="serial_no">CERIAL NO.</label>
		        </div>
		       <div class="line-btm"></div>
			</div>
			<div class="col s4">
				<div class="input-field">
					<label class="active" for="category">CATEGORY</label>
					<select name="category" id="category" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<?php if(isset($category)){ ?>
						<option selected="selected" value="3"><?php echo $category; ?></option>
						<?php } ?>
						<option value="">select category</option>
						<option value="1">Category 1</option>
						<option value="2">Category 2</option>
						<option value="3">Category 3</option>
					</select>
				</div>
				<div class="line-btm"></div>
				<div class="input-field rb-field">
					<label class="active" for="classification">CLASSIFICATION</label>
					<select name="classification" id="classification" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<?php if(isset($classification)){ ?>
						<option selected="selected" value="3"><?php echo $classification; ?></option>
						<?php } ?>
						<option value="">select classification</option>
						<option value="1">Classification 1</option>
						<option value="2">Classification 2</option>
					</select>
					<a href="" class="rb-field-btn">
						<i class="small material-icons ">settings</i>
					</a>
				</div>
				<div class="line-btm"></div>
			</div>
			<div class="col s4">
		       <div class="input-field center-content">
		       		<?php $src = isset($picture)?$picture:'upload_img_default_bg.png'; ?>
			    	<img class="uploaded-img" src="<?php echo base_urL().PATH_IMAGES.$src; ?>" />
		        	<a class="waves-effect waves-light btn btn-browse-photo">BROWSE PHOTO</a>
		        </div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s8">
				<div class="input-field">
			    	<input id="property_name" type="text" class="validate" value="<?php echo isset($property_name)?$property_name:''; ?>">
			   		<label for="property_name">PROPERTY NAME</label>		     
			   	</div>
			   	<div class="line-btm"></div>
		        <div class="input-field">
		        	<textarea id="notes" class="materialize-textarea"><?php echo isset($notes)?$notes:''; ?></textarea>
          			<label for="notes">NOTES</label>
		        </div>
			</div>
			<div class="col s4">
				<p class="head-genInfo">LOCATION INFORMATION</p>
				
				<div class="input-field rb-field">
					<label class="active" for="site">SITE</label>
					<select name="site" id="site" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<?php if(isset($site)){ ?>
						<option selected="selected" value="3"><?php echo $site; ?></option>
						<?php } ?>
						<option value="">select site</option>
						<option value="1">Site 1</option>
						<option value="2">Site 2</option>
						<option value="3">Site 3</option>
					</select>
					<a
						href="javascript:;"
						type="button"
						class="rb-field-btn md-trigger"
						data-modal="modal_sites"
						onclick="modal_sites_init('<?php echo $url; ?>')">
						<i class="small material-icons ">settings</i>
					</a>
				</div>
				<div class="line-btm"></div>

				<div class="input-field rb-field">
					<label class="active" for="location">LOCATION</label>
					<select name="location" id="location" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<?php if(isset($location)){ ?>
						<option selected="selected" value="3"><?php echo $location; ?></option>
						<?php } ?>
						<option value="">select site</option>
						<option value="1">Location 1</option>
						<option value="2">Location 2</option>
						<option value="3">Location 3</option>
					</select>
					<a href="" class="rb-field-btn">
						<i class="small material-icons ">settings</i>
					</a>
				</div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s12">
				<p class="head-genInfo">AQUISITION INFORMATION</p>
			</div>
		</div>	
		<div class="row m-n">
			<div class="col s4">
			   	<div class="input-field">
				   	<label for="date_aquired">DATE AQUIRED <i class="fa fa-calendar"></i></label>				
					<input
						name="date_aquired"
						required=""
						aria-required="true"
						class="validate datepicker"
						size="16"
						type="text"
						value="<?php echo isset($property_name)?$property_name:''; ?>"
						data-date-format="yyyy-mm-dd" >
				</div>
			</div>
			<div class="col s2">
				<div class="input-field">
			    	<input id="reference_no" type="text" class="validate" value="<?php echo isset($property_name)?$property_name:''; ?>">
			   		<label for="reference_no">REFERENCE NO.</label>		     
			   	</div>
			</div>
			<div class="col s2">
				<div class="input-field">
			    	<input id="unit_cost" type="text" class="validate" value="<?php echo isset($unit_cost)?$unit_cost:''; ?>">
			   		<label for="unit_cost">UNIT COST</label>		     
			   	</div>
			</div>
			<div class="col s4">
			   	<div class="input-field">
					<label class="active" for="accountable_officer">ACCOUNTABLE OFFICER</label>
					<select name="accountable_officer" id="accountable_officer" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
						<?php if(isset($accountable_officer)){ ?>
						<option selected="selected" value="3"><?php echo $accountable_officer; ?></option>
						<?php } ?>
						<option value="">select accountable officer</option>
						<option value="1">Officer 1</option>
						<option value="2">Officer 2</option>
						<option value="3">Officer 3</option>
					</select>
				</div>
			</div>
		</div>
		<div class="row m-n">
			<div class="col s4">
				<div class="input-field">
			    	<input id="make" type="text" class="validate" value="<?php echo isset($make)?$make:''; ?>">
			   		<label for="make">MAKE</label>		     
			   	</div>
			</div>
			<div class="col s4">
				<div class="input-field">
			    	<input id="model" type="text" class="validate" value="<?php echo isset($model)?$model:''; ?>">
			   		<label for="model">MODEL</label>		     
			   	</div>
			</div>
		</div>
		<div class="row m-n">
			
		</div>

	</div>
	<div class="md-footer default">
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	    <button class="btn waves-effect waves-light" id="save_asset" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	</div>
</form>

<script>
$(document).ready(function() {
	Materialize.updateTextFields();
});
 $(document).ready(function() {

    //$('select').material_select();
  });
$(function(){
  	$('#asset_form').parsley();
  	$('#asset_form').submit(function(e) {
   		e.preventDefault();
    
		if ( $(this).parsley().isValid() ) {
	  		var data = $(this).serialize();
	  	
		  	button_loader('save_asset', 1);
		  	$.post("<?php echo base_url() . PROJECT_PSMS ?>/psms_assets/process/", data, function(result) {
				if(result.flag == 0){
			  		notification_msg("<?php echo ERROR ?>", result.msg);
			  		button_loader('save_asset', 0);
				}
				else
				{
			  		notification_msg("<?php echo SUCCESS ?>", result.msg);
			  		button_loader("save_asset",0);
			  		modalObj.closeModal();
			  		load_datatable('asset_table', '<?php echo PROJECT_CEIS ?>/ceis_asset/get_asset_list/');
				}
			}, 'json');
	    }
	});

	<?php if(ISSET($involvement_code)){ ?>
	$('.input-field label').addClass('active');
	<?php } ?>
})

</script>