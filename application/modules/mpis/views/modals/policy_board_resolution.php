
<div class="row m-n">
	<div class="col s3">
		<div class="input-field">
			<label class="active" for="series">Series</label>
			<select name="series" id="series" required="" aria-required="true" class="selectize" data-parsley-required="true" data-parsley-validation-threshold="0" data-parsley-trigger="keyup">
				<option value="">select series</option>
				<?php 
					
					for($year = $year_end; $year > $year_start; $year--)
					{
						$selected = ( !EMPTY($info['board_resolution_series']) AND $info['board_resolution_series'] == $year) ? "selected" : "";
						echo '<option '.$selected.' value="'.$year.'">'.$year.'</option>';
					}				
				?>
			</select>           
		</div>
	</div>
	
	<div class="col s3">
		<div class="input-field">
			<?php				
				$date	= "";
				$class	= "";
				IF(!EMPTY($info['board_resolution_date']))
				{
					$date	= date('m/d/Y', strtotime($info['board_resolution_date']));
					$class	= 'class="active"';
				}											
			?>
			<label <?php echo $class ?> for="br_date">Date <i class="fa fa-calendar"></i></label>				
			<input name="br_date" required="" aria-required="true" class="validate datepicker" size="16" type="text" value="<?php echo $date; ?>" data-date-format="yyyy-mm-dd" >
		</div>
	</div>
	
	<div class="col s2">
		<div class="input-field">
		  
			<?php				
				$status	= "";
				$class	= "";
				IF(!EMPTY($info['board_resolution_status']))
				{
					$status	= $info['board_resolution_status'];
					$class	= 'class="active"';
				}											
			?>
			<input type="text" class="validate" name="status" id="status" value="<?php echo $status ?>"/>
			<label for="status" <?php echo $class ?>>Status</label>
		</div>
	</div>
	
	<div class="col s4">
		<div class="input-field">
		  
			<?php				
				$br_no	= "";
				$class	= "";
				IF(!EMPTY($info['board_resolution_no']))
				{
					$br_no	= $info['board_resolution_no'];
					$class	= 'class="active"';
				}											
			?>
			<input type="text" class="validate" name="br_no" id="br_no" value="<?php echo $br_no ?>"/>
			<label for="br_no" <?php echo $class ?>>No</label>
		</div>
	</div>
		

</div>


<div class="row m-n">
	<div class="col s12">
		<div class="input-field">
		  
			<?php				
				$title	= "";
				$class	= "";
				IF(!EMPTY($info['board_resolution_title']))
				{
					$title	= $info['board_resolution_title'];
					$class	= 'class="active"';
				}											
			?>
			<input type="text" class="validate" name="br_title" id="br_title" value="<?php echo $title ?>"/>
			<label for="br_title" <?php echo $class ?>>Title</label>
		</div>
	</div>
	

</div>

<div class="row m-n">
	<div class="col s12">
		<div class="input-field">
			<?php 
				$board_resolution_body = !EMPTY($info['board_resolution_body']) ? $info['board_resolution_body'] : ''; 
			?>
			<textarea  required="" aria-required="true" id="br_body" name="br_body" value="<?php echo $board_resolution_body ?>"><?php echo $board_resolution_body ?></textarea>
		</div>
	</div>
</div>	 

	
<script>

$(function(){
  
  CKEDITOR.replace('br_body');

});

</script>