<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Issuance Classifications</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Issuance Classifications
		<span>Manage Issuance Classifications</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p -l-md p-r-md">
	  	<?php 
	  		if($permission_add)
	  			echo '<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_issuance_classification" id="add_issuance_classification" name="add_issuance_classification" onclick="modal_issuance_classification_init(\''.$url.'\')">Create New Issuance Classification</button>';
	  	?>

	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="issuance_classification_table">
  <thead>
	<tr>
	  <th width="20%">Issuance Classification</th>
	  <th width="20%">Description</th>	  
	  <th width="10%" class="text-center">Actions</th>
	</tr>
  </thead>
  </table>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'mpis_code_libraries_issuance_classifications', 
		method		: 'delete_issuance_classification', 
		module		: '<?php echo PROJECT_MPIS ?>' });
</script>