<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active"><?php echo $page ?></a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5><?php echo $page ?>
		<span>Manage <?php echo $page ?></span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p -l-md p-r-md">
	  	<?php 
	  		if($permission_add)
	  			echo '<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_market_value" id="add_market_value" name="add_market_value" onclick="modal_market_value_init(\''.$url.'\')">Create New '.$page.'</button>';
	  	?>

	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="market_value_table">
  <thead>
	<tr>
	  
	  <th width="80%">Year</th> 
	  <th width="20%" class="text-center">Actions</th>
	</tr>
  </thead>
  </table>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: '<?php echo strtolower($controller) ?>', 
		method		: 'delete_market_value', 
		module		: '<?php echo PROJECT_MPIS ?>' });
</script>