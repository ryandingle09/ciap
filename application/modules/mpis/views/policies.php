<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Policies</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Policies
		<span>Manage Policies</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p -l-md p-r-md">
	  	<?php 
	  		if($permission_add)
	  			echo '<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_policy" id="add_policy" name="add_policy" onclick="modal_policy_init(\''.$url.'\')">Create New Policy</button>';
	  	?>

	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="policies_table">
  <thead>
	<tr>
	  <th width="20%">Category</th>
	  <th width="20%">Policy</th>	  
	  <th width="10%" class="text-center">Actions</th>
	</tr>
  </thead>
  </table>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'mpis_policies', 
		method		: 'delete_policy', 
		module		: '<?php echo PROJECT_MPIS ?>' });
</script>