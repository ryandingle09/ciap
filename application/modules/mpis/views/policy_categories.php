<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Policy Categories</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Policy Categories
		<span>Manage Policy Categories</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p -l-md p-r-md">
	  	<?php 
	  		if($permission_add)
	  			echo '<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_policy_categories" id="add_policy_categories" name="add_policy_categories" onclick="modal_policy_categories_init(\''.$url.'\')">Create New Policy Category</button>';
	  	?>

	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="policy_categories_table">
  <thead>
	<tr>
	  <th width="20%">Policy Category</th>
	  <th width="20%">Description</th>	  
	  <th width="10%" class="text-center">Actions</th>
	</tr>
  </thead>
  </table>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'mpis_code_libraries_policy_categories', 
		method		: 'delete_policy_category', 
		module		: '<?php echo PROJECT_MPIS ?>' });
</script>