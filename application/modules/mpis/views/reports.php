<div class="page-title">
	<ul id="breadcrumbs">
		<li><a href="#">Home</a></li>
		<li><a href="#" class="active">Reports</a></li>
	</ul>
	
	<div class="row m-b-n">
		<div class="col s6 p-r-n">
			<h5>Reports
				<span>Manage Reports</span>
			</h5>
		</div>
		
		<div class="col s6 p-r-n right-align">
      		<label></label>
    	</div>
  	</div>
</div>

<div class="m-t-lg"></div>

<ul class="collapsible panel m-t-lg" data-collapsible="expandable">
	<li>
		<div class="collapsible-header active">Generate Report</div>
		<div class="collapsible-body" >
			<form id="report_form">
				<div class="form-basic">
				
					<div class="row m-n">
              			<div class="col s12">
                			<h6>Report</h6>
                			<div class="input-field">
                  				<select name="report" id="report" class="selectize"  placeholder="Select report">
                        			<option value="">select report</option>
                        			<?php 
                        				foreach($reports as $reports)
                        					echo '<option value="'.$reports['resource_code'].'">'.$reports['resource_name'].'</option>';
                        			?>
                  				</select> 
                			</div>
              			</div>
            		</div>
            		
            		<div id="year_range_row" class="row m-n">
              			<div id="year_start_row" class="col s4">
                			<h6>Year</h6>
                			<div class="input-field">
                   				<select required name="year_start" id="year_start" class="selectize">	                  
                   					<?php                    
                   						for($year = $year_max; $year >= $year_min; $year--)
                   						 	echo "<option value='".$year."' >".$year."</option>";
                   					?>
                    			</select>
                			</div>
              			</div>
              			
              			<div id="year_end_row" class="col s4">
                			<h6>&nbsp;</h6>
                			<div class="input-field">
                   				<select required name="year_end" id="year_end" class="selectize">	                  
                   					<?php                    
                   						for($year = $year_max; $year >= $year_min; $year--)
                   							echo "<option value='".$year."' >".$year."</option>";
                   					?>
                    			</select>
                			</div>
              			</div>


              			<div id="quarter_row" class="col s4">
                			<h6>Quarter</h6>
                  			<div class="input-field">
                    			<select required name="quarter" id="quarter" class="selectize">
									<option value="5"> Year-end</option>
									<option value="1"> First Quarter</option>
									<option value="2"> Second Quarter</option>
                      				<option value="3"> Third Quarter</option>
                      				<option value="4"> Fourth Quarter</option>
                      				
                    			</select>                     
                  			</div>
              			</div>
              			
              			<div id="price_row" class="col s4">
                			<h6>Price</h6>
                  			<div class="input-field">
                    			<select required name="price" id="price" class="selectize">
									<option value="CURRENT"> Current</option>									
                      				<option value="CONSTANT"> Constant</option>                      				
                    			</select>                     
                  			</div>
              			</div>      
              			
              			
              			<div id="indicator_row" class="col s4">
                			<h6>Indicator</h6>
                			<div class="input-field">
                   				<select required id="indicator" name="indicator" class="selectize">
                   					<option value="">Select indicator</option>	                                     					
                    			</select>
                			</div>
              			</div>
	              		             			
            		</div>
            		
            		<div id="value_row" class="row m-n">
              			<div class="col s6">
                			<h6>Value</h6>
                			<div class="input-field">
                   				<select required id="value" name="value" class="selectize">
                   					<option value="">Select value</option>
                   					<option value="GVC">Gross Value in Construction</option>
                   					<option value="GVA">Gross value Added in Construction</option>	                                     					
                    			</select>
                			</div>
              			</div>
              		</div>
            		
            		<div id="building_type_row" class="row m-n">
              			<div class="col s6">
                			<h6>Type of Building</h6>
                			<div class="input-field">
                   				<select required id="building_type" name="building_type" class="selectize">
                   					<option value="">Select type of building</option>	                                     					
                    			</select>
                			</div>
              			</div>
              		</div>
              		
              		<div id="business_type_category_row" class="row m-n">
              			<div id="business_type_row" class="col s6">
                			<h6>Type of Business</h6>
                			<div class="input-field">
                   				<select required id="business_type" name="business_type" class="selectize">
                   					<option value="">Select type of business</option>	                                     					
                    			</select>
                			</div>
              			</div>
              			
              			<div id="category_row" class="col s6">
                			<h6>Category</h6>
                			<div class="input-field">
                   				<select required id="category" name="category" class="selectize">
                   					<option value="">Select category</option>	                                     					
                    			</select>
                			</div>
              			</div>
              		</div>
              		
              		<div id="company_ratio_row" class="row m-n">
              			<div id="company_row" class="col s8">
                			<h6>Company</h6>
                			<div class="input-field">
                   				<select required id="company" name="company" class="selectize">
                   					<option value="">Select company</option>	                                     					
                    			</select>
                			</div>
              			</div>
              			
              			<div id="ratio_row" class="col s4">
                			<h6>Ratio</h6>
                			<div class="input-field">
                   				<select required id="ratio" name="ratio" class="selectize">
                   					<option value="">Select ratio</option>	                                     					
                    			</select>
                			</div>
              			</div>
              		</div>
              		
              		<div id="date_range_row" class="row m-n">
              			<div id="date_start_row" class="col s4">
                			<h6>Date</h6>
							<div class="input-field">										
								<input name="date_start" id="date_start"  class="validate datepicker" size="16" type="text"  data-date-format="yyyy-mm-dd" >
							</div>
              			</div>
              			
              			<div id="date_end_row" class="col s4">
                			<h6>Date</h6>
							<div class="input-field">										
								<input name="date_end" id="date_end" class="validate datepicker" size="16" type="text"  data-date-format="yyyy-mm-dd" >
							</div>
              			</div>
              		</div>
              		
              		<div id="indicator_category_row" class="row m-n">
              			<div class="col s6">
                			<h6>Category</h6>
                			<div class="input-field">
                   				<select required id="indicator_category" name="indicator_category" class="selectize">
                   					<option value="">Select category</option>	                                     					
                    			</select>
                			</div>
              			</div>
              		</div>              		
            		
            		<div class="row m-n">
              			<div id="gen_type" class="col s12">
                			<h6>Generate As</h6>

                			<div class="col s12">
                    			<div class="col s2">
                      				<input checked name="generate" type="radio" id="report_type_pdf" value="PDF"/>
                      				<label for="report_type_pdf">PDF</label>
                    			</div>
                    			
                    			<div id="excel" class="col s2">
                      				<input name="generate" type="radio" id="report_type_excel" value="Excel"/>
                      				<label for="report_type_excel">Excel</label>
                    			</div>
                    			
                    			<div id="chart" class="col s2">
                      				<input name="generate" type="radio" id="report_type_chart" value="Chart"/>
                      				<label for="report_type_chart">Chart</label>
                    			</div>                    			
                			</div>                  
              			</div>
            		</div>
            		
            		<div id="percent_share_row" class="row m-n">
              			<div class="col s12">
	              			<div class="col s6">
	                			<h6>Percent Share to</h6>
	                			<div class="input-field">
	                   				<select required id="percent_share" name="percent_share" class="selectize">
	                   					<option value="">Select percent share</option>
	                   					<option value="number">Number</option>
	                   					<option value="floor_area">Floor Area</option>
	                   					<option value="value">Value</option>	                                     					
	                    			</select>
	                			</div>
	              			</div>
              			</div>
              		</div>
          		</div>
          		
          		<div class="panel-footer right-align">
            		<a class="hide" id="gen_link" href="#" target="_tab"></a>
            		<button class="btn waves-effect waves-light"  id="generate_report" name="action" type="button" value="Save">Generate Report</button>     
          		</div>
			</form>
		</div>
	</li>
</ul>