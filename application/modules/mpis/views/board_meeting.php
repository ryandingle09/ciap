<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Board Meeting Classification</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Board Meeting Classification
		<span>Manage Board Meeting Classification</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p -l-md p-r-md">
	  	<?php 
	  		if($permission_add)
	  			echo '<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_board_meeting" id="add_board_meeting" name="add_board_meeting" onclick="modal_board_meeting_init(\''.$url.'\')">Create New Board Meeting Classification</button>';
	  	?>

	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="board_meeting_table">
  <thead>
	<tr>
	  <th width="80%">Name</th>
	  <th width="20%" class="text-center">Actions</th>
	</tr>
  </thead>
  </table>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'mpis_code_libraries_board_meeting', 
		method		: 'delete_board_meeting', 
		module		: '<?php echo PROJECT_MPIS ?>' });
</script>