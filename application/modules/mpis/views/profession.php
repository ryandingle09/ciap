<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Professions</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Professions
		<span>Manage Professions</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p -l-md p-r-md">
	  	<?php 
	  		if($permission_add)
	  			echo '<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_profession" id="add_profession" name="add_profession" onclick="modal_profession_init(\''.$url.'\')">Create New Profession</button>';
	  	?>

	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="profession_table">
  <thead>
	<tr>
	  <th width="40%">Profession</th>
	  <th width="30%">Short Name</th>	  
	  <th width="20%">Active</th>
	  <th width="10%" class="text-center">Actions</th>
	</tr>
  </thead>
  </table>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'mpis_code_libraries_professions', 
		method		: 'delete_profession', 
		module		: '<?php echo PROJECT_MPIS ?>' });
</script>