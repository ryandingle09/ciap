<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Indicator Sources</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Indicator Sources
		<span>Manage Indicator Sources</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p -l-md p-r-md">
	  	<?php 
	  		if($permission_add)
	  			echo '<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_indicator_source" id="add_indicator_source" name="add_indicator_source" onclick="modal_indicator_source_init(\''.$url.'\')">Create New Indicator Source</button>';
	  	?>

	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="indicator_source_table">
  <thead>
	<tr>
	  <th width="20%">Indicator Source</th>
	  <th width="10%" class="text-center">Actions</th>
	</tr>
  </thead>
  </table>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'mpis_code_libraries_indicator_sources', 
		method		: 'delete_indicator_source', 
		module		: '<?php echo PROJECT_MPIS ?>' });
</script>