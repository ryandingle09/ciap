<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="#" class="active">Construction Employment</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Construction Employment
		<span>Manage Construction Employment</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="btn-group">
	    
	  </div>
	  
	  <div class="input-field inline p -l-md p-r-md">
	  	<?php 
	  		if($permission_add)
	  			echo '<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_cons_employment" id="add_cons_employment" name="add_cons_employment" onclick="modal_cons_employment_init(\''.$url.'\')">Create New Construction Employment</button>';
	  	?>

	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable"></div>
<div>
  <table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="cons_employment_table">
  <thead>
	<tr>
	  <th width="40%">Year</th>
	  <th width="40%">Quarter</th>	  
	  <th width="20%" class="text-center">Actions</th>
	</tr>
  </thead>
  </table>
</div>
<script>
	var deleteObj = new handleData({ 
		controller	: 'mpis_cons_employment', 
		method		: 'delete_cons_employment', 
		module		: '<?php echo PROJECT_MPIS ?>' });
</script>