<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_meeting_agendas extends MPIS_Controller {
	
	private $module = MODULE_MPIS_POLICIES;
	
	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('policies_model', 'policies');
		$this->load->model('board_meetings_model', 'board_meetings');
		$this->load->model('agendas_model', 'agendas');
		
		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);

	}
	
	public function get_agenda_list($encoded_id, $salt, $token)
	{
		try 
		{
			$flag	= 0;
			$data	= array();
			$msg	= $this->lang->line('err_internal_server');
			
			$params	= get_params();
			
			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> 0,
				"iTotalDisplayRecords" 	=> 0,
				"flag"					=> $flag,
				"msg"					=> $msg,
				"aaData" 				=> array()
			);
			
			if(!$this->permission)
				throw new Exception($this->lang->line('err_unauthorized_view'));
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);
				
			$hash_id = base64_url_decode($encoded_id);
			
			$key	= $this->get_hash_key('policy_id');
			$where	= array($key => $hash_id);
							
			// FIELDS TO BE SELECTED FROM TABLE
			$select_fields = array(
				"agenda_id",
				"policy_id",
				"agenda_code",
				"agenda_title",
				"agenda_body",
			);
			
			// APPEARS ON TABLE
			$where_fields = array(
				"agenda_code",
				"agenda_title",
				"agenda_body"
			);
			
			$results 		= $this->agendas->get_agenda_list($select_fields, $where_fields, $params, $where);
			$total	 		= $this->agendas->total_length(); // TOTAL COUNT OF RECORDS
			$filtered_total = $this->agendas->filtered_length($select_fields, $where_fields, $params, $where); // TOTAL COUNT OF RECORDS PER PAGE
				
			$ctr	= 0;
			foreach ($results as $data):
				
				$ctr++;
				
				// PRIMARY KEY
				$primary_key	= $data["agenda_id"];
			
				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash($primary_key);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);
				$agenda_url 	= $encoded_id."/".$salt."/".$token;
			
				// PRIMARY KEY
				$primary_key	= $data["policy_id"];
					
				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash($primary_key);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);
				$policy_url 	= $encoded_id."/".$salt."/".$token;
				
				
				$delete_action 	= 'content_delete("agenda", "'.$agenda_url.'", "'.$policy_url.'")';
			
				// START: CONSTRUCT ACTION ICONS
				$action = "<div class='table-actions'>";
			
					if($this->permission_edit) :
						$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_agenda' onclick=\"modal_agenda_init('".$agenda_url."')\"></a>";
					endif;
				
					if($this->permission_delete) :
						$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					endif;
			
				$action .= '</div>';
				// END: CONSTRUCT ACTION ICONS
			
				$row 	= array();
				$row[] 	= $data['agenda_code'];
				$row[] 	= $data['agenda_title'];
				$row[] 	= $data['agenda_body'];
				$row[] 	= $action;
				
				$output['aaData'][] = $row;
			
			endforeach;
				
			$output["iTotalRecords"] 		= $total["cnt"];
			$output["iTotalDisplayRecords"] = $filtered_total["cnt"];
				
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			$msg = $this->get_user_message($e);
		}
		catch(Exception $e)
		{
			$msg = $this->rlog_error($e, TRUE);
		}	
		
		$output['flag']	= $flag;
		$output['msg']	= $msg;
				
		echo json_encode($output);
	}
	
	public function modal_agenda($encoded_id, $salt, $token)
	{
		try
		{
			$data		= array();
			$info		= array();
			$err_msg	= "";
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);
				
			$hash_id = base64_url_decode($encoded_id);
			
			
			$data['permission_save']	= $this->permission_add;
				
			if($this->hash(0) != $hash_id){
				$data['permission_save']	= $this->permission_save;
				
				$key	= $this->get_hash_key('agenda_id');
				$where	= array($key => $hash_id);
				
				$info	= $this->agendas->get_specific_agenda($where);
				
				IF(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			$data['info']	= $info;
			
			// REGENERATE SECURITY VARIABLES
			$salt 						= gen_salt();
			$token						= in_salt($encoded_id, $salt);
			$data['agenda_security']	= $encoded_id . '/' . $salt . '/' . $token;
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);

			$err_msg = $this->get_user_message($e);
		}
		catch(Exception $e)
		{
			$err_msg =	$this->rlog_error($e, TRUE);
		}
		
		$data['err_msg']	= $err_msg;
				
		$this->load->view('modals/agenda', $data);
		
		$resources					= array();
		$resources['load_js'] 		= array();
		$this->load_resources->get_resource($resources);
		
	}

	public function process()
	{
		try
		{
			$flag 			= 0;
			$params 		= get_params();
			$msg			= "";
			$agenda_list	= "";
			
			// CONSTRUCT SECURITY VARIABLES
			$hash_id 			= $this->hash(0);
			$encoded_id 		= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token 				= in_salt($encoded_id, $salt);
			$policy_security	= $encoded_id."/".$salt."/".$token;
						
			// SERVER VALIDATION
			$this->_validate($params);
			

			MPIS_Model::beginTransaction();
			
			/*
			 * START: CHECK THE POLICY ID
			 */
			
			$key 			= $this->get_hash_key('policy_id');
			$where			= array();
			$where[$key]	= $params['policy_hash_id'];
			
			$info 			= $this->policies->get_specific_policy($where);
			$policy_id		= ISSET($info['policy_id']) ? $info['policy_id'] : 0;
			
			if(EMPTY($policy_id))
			{
				
				if($this->permission_add === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_add'));
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				$audit_action[]	= AUDIT_INSERT;
				$audit_table[]	= MPIS_Model::tbl_policies;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array();
					
				$bm_no	= '';
				IF(!EMPTY($params['bm_no']))
					$bm_no	= $params['bm_no'] . $this->number_formatter($params['bm_no']) . ' ';
				
				$classification		= ($params['classification'] == BM_CLASS_REGULAR) ? "Regular" : "Special";
				$params['policy']	= $bm_no . $classification . ' Board Meeting dated ' . $params['bm_date'];
			
				$policy_id 			= $this->policies->insert_policy($params); // SAVES THE POLICY RECORD
				$current_info 		= $this->policies->get_specific_policy(array('policy_id' => $policy_id));
					
				$curr_detail[]	= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
					
			}
			else
			{
				if($this->permission_edit === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_edit'));
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				$audit_action[]	= AUDIT_UPDATE;
				$audit_table[]	= MPIS_Model::tbl_policies;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array($info);
				
				$params['policy_id']	= $policy_id;
				
				$bm_no	= '';
				IF(!EMPTY($params['bm_no']))
					$bm_no	= $params['bm_no'] . $this->number_formatter($params['bm_no']) . ' ';
				
				
				$classification			= ($params['classification'] == BM_CLASS_REGULAR) ? "Regular" : "Special";
				$params['policy']		= $bm_no . $classification . ' Board Meeting dated ' . $params['bm_date'];
				
				$this->policies->update_policy($params); // UPDATE THE RECORD
				
				$current_info 		= $this->policies->get_specific_policy(array('policy_id' => $policy_id));
				$curr_detail[]		= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
					

			}
			/*
			 * END: CHECK THE POLICY ID
			 */
						
			
			/*
			 * STARTT: INSERT/UPDATE BOARD MEETING RECORD
			 */
				
			// CHECK THE BOARD MEETING RECORD			
			$info 			= $this->board_meetings->get_specific_board_meeting(array('policy_id' => $policy_id));
			$policy_bm_id	= $info['policy_id'];
			
			// IF EMPTY MEANS INSERT/SAVE RECORD
			IF(EMPTY($policy_bm_id)){
					
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				$audit_action[]	= AUDIT_INSERT;
				$audit_table[]	= MPIS_Model::tbl_board_meetings;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array();
			
				$params['policy_id']	= $policy_id;
				$this->board_meetings->insert_board_meeting($params); // SAVES THE RECORD
					
				$current_info 		= $this->board_meetings->get_specific_board_meeting(array('policy_id' => $policy_id));
				$curr_detail[]		= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			}
			else{
					
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				$audit_action[]	= AUDIT_UPDATE;
				$audit_table[]	= MPIS_Model::tbl_board_meetings;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array($info);
					
				$params['policy_id']	= $policy_bm_id;
				$this->board_meetings->update_board_meeting($params); // UPDATE THE RECORD
					
				$current_info 		= $this->board_meetings->get_specific_board_meeting(array('policy_id' => $policy_bm_id));
				$curr_detail[]		= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			
			}
			
			/*
			 * END: INSERT/UPDATE BOARD MEETING RECORD
			 */			
	
	
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('agenda_id');
			$where			= array();
			$where[$key]	= $params['agenda_hash_id'];

			$info 		= $this->agendas->get_specific_agenda($where);
			$agenda_id	= $info['agenda_id'];
			

			// IF EMPTY MEANS INSERT/SAVE RECORD
			IF(EMPTY($agenda_id)){

				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				$audit_action[]	= AUDIT_INSERT;
				$audit_table[]	= MPIS_Model::tbl_meeting_agenda;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array();
				
				$params['policy_id']	= $policy_id; 
				$agenda_id 				= $this->agendas->insert_agenda($params); // SAVES THE AGENDA RECORD				
				$current_info 			= $this->agendas->get_specific_agenda(array('agenda_id' => $agenda_id));				
				$activity_title			= $current_info['agenda_code'] . '. ' . $current_info['agenda_title'];
				
				$curr_detail[]			= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				

				$activity	= "%s has been added in agenda.";
				$activity 	= sprintf($activity, $activity_title);

				$msg  = $this->lang->line('data_saved');
			}

			else{

				$params['agenda_id']	= $agenda_id;

				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= MPIS_Model::tbl_meeting_agenda;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array($info);
				
				$this->agendas->update_agenda($params); // UPDATE THE INDICATOR RECORD
				
				$current_info	= $this->agendas->get_specific_agenda(array('agenda_id' => $agenda_id));				
				$activity_title	= $current_info['agenda_code'] . '. ' . $current_info['agenda_title'];
				
				$curr_detail[]	= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				
				$activity	= "%s has been added in agenda.";
				$activity 	= sprintf($activity, $activity_title);

				$msg  = $this->lang->line('data_updated');
			}


			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table, 
				$audit_schema
			);
			
			MPIS_Model::commit();
			
			// CONSTRUCT SECURITY VARIABLES
			$hash_id 			= $this->hash($policy_id);
			$encoded_id 		= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token 				= in_salt($encoded_id, $salt);
			$policy_security	= $encoded_id."/".$salt."/".$token;

			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			MPIS_Model::rollback();
			
			$this->rlog_error($e);
			
			$msg = $this->get_user_message($e);
			
		}
		catch(Exception $e)
		{
			MPIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);			
		}
		
		
		
		$info = array(
			"flag" 			=> $flag,
			"msg" 			=> $msg,
			"security"		=> $policy_security
		);
		
		echo json_encode($info);
		
	}
	
	/*
	 * START: SERVER VALIDATION 
	 */
	
	private function _validate(&$params)
	{
		try
		{

			$this->_validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields 						= array();
			$fields["policy_category"]		= 'Policy Category';
			$fields["board"]				= 'Board';
			$fields["classification"]		= 'Classification';
			$fields["bm_date"]				= 'BM Date';
			
			$fields["code"]					= 'Code';
			$fields["title"]				= 'Title';
			$fields["body"]					= 'Agenda';

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['agenda_security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			if(EMPTY($params['policy_security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['agenda_security']);

			$params['agenda_encoded_id']	= $security[0];
			$params['agenda_salt']			= $security[1];
			$params['agenda_token']			= $security[2];

			check_salt($params['agenda_encoded_id'], $params['agenda_salt'], $params['agenda_token']);

			$params['agenda_hash_id']		= base64_url_decode($params['agenda_encoded_id']);
			
			
			$security = explode('/', $params['policy_security']);
			
			$params['policy_encoded_id']	= $security[0];
			$params['policy_salt']			= $security[1];
			$params['policy_token']			= $security[2];
			
			check_salt($params['policy_encoded_id'], $params['policy_salt'], $params['policy_token']);
			
			$params['policy_hash_id']		= base64_url_decode($params['policy_encoded_id']);
				
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _validate_input($params)
	{
		try
		{
			
			$validation['policy_category'] = array(
				'data_type' => 'digit',
				'name'		=> 'Policy Category'
			);
			
			$validation['board'] = array(
				'data_type' => 'digit',
				'name'		=> 'Board'
			);
			
			$validation['classification'] = array(
				'data_type' => 'digit',
				'name'		=> 'Classification'
			);
			
			$validation['bm_date'] = array(
				'data_type' => 'date',
				'name'		=> 'BM Date'
			);
				
			
			$validation['code'] = array(
				'data_type' => 'string',
				'name'		=> 'Code'
			);
			
			$validation['title'] = array(
				'data_type' => 'string',
				'name'		=> 'Title',
				'min_len'	=> 3,
				'max_len'	=> 255
			);
			
			$validation['body'] = array(
				'data_type' => 'string',
				'name'		=> 'Agenda',
				'min_len'	=> 5,
				'max_len'	=> 60000
			);
			
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	/*
	 * END: SERVER VALIDATION
	 */
	
	
	public function delete_agenda() 
	{
		try 
		{

			$flag				= 0;
			$msg				= $this->lang->line('err_internal_server');
						
			if($this->permission_delete === FALSE)
				throw new Exception($this->lang->line('err_unauthorized_delete'));
			
			$params 					= get_params();
			$params['agenda_security']	= $params['param_1'];
			$params['policy_security']	= $params['param_2'];

			$this->_validate_security($params);

			$key 			= $this->get_hash_key('agenda_id');
			$where			= array();
			$where[$key]	= $params['agenda_hash_id'];
			
			$info 			= $this->agendas->get_specific_agenda($where);

			if(EMPTY($info['policy_id']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			MPIS_Model::beginTransaction();
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= MPIS_Model::tbl_meeting_agenda;
			$audit_schema[]	= DB_MPIS;			
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL

			$this->agendas->delete_agenda($info['agenda_id']);

			// SET AS AN ARRAY
			$curr_detail[]	= array();
			
			// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			$title		= $info['agenda_code'] . '. ' . $info['agenda_title'];				
			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $title);
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			MPIS_Model::commit();

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			MPIS_Model::rollback();
			
			$this->rlog_error($e);

			$msg	= $this->get_user_message($e); 	
		}
		catch(Exception $e)
		{	

			MPIS_Model::rollback();
						
			$msg	= $this->rlog_error($e, TRUE); 	
		}		
		
		$info = array(
			"flag"		=> $flag,
			"msg"		=> $msg,
			"reload"	=> 'datatable',
			"table_id"	=> "agendas_table",
			"path"		=> PROJECT_MPIS . '/mpis_meeting_agendas/get_agenda_list/' . $params['policy_security'] 
		);

		echo json_encode($info);
	}
}

/* End of file Ceis_positions.php */
/* Location: ./application/modules/ceis/controllers/Ceis_positions.php */