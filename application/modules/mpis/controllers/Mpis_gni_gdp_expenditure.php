<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_gni_gdp_expenditure extends MPIS_Controller {
	
	private $module 			= MODULE_MPIS_GNI_GDP_EXPENDITURE;
	private $market_value_type	= MARKET_TYPE_EXPENDITURE;
	private $market_value_name	= '';
	
	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('market_value_model', 'market_value');
		$this->load->model('params_model', 'params');
		
		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);
		
		$info	= $this->params->get_params(MPIS_Model::tbl_param_market_value_types, array('market_value_type_id' => $this->market_value_type));
		$this->market_value_name	= $info[0]['market_value_type_name'];
	}
	
	public function index()
	{	
		try{
			$data = $resources = $modal = array();
			
			
			
			if($this->permission)
			{
				$modal = array(
					'modal_market_value' => array(
						'module'		=> PROJECT_MPIS,
						'controller'	=> __CLASS__,
						'method'		=> 'modal_market_value',
						'title'			=> $this->market_value_name,
						'size'			=> 'lg',
						'multiple'		=> TRUE
					)
				);
				
				$resources['load_css'] 		= array(CSS_DATATABLE);
				$resources['load_js'] 		= array(JS_DATATABLE, JS_EDITOR);
				$resources['load_modal'] 	= $modal;
				$resources['datatable']		= array();
				$resources['datatable'][] 	= array(
					'table_id' 	=> 'market_value_table', 
					'path' 		=> PROJECT_MPIS.'/mpis_gni_gdp_expenditure/get_market_value_list'				
				);
				
				
				// STORE THE PERMISSION
				$data['permission_add']	= $this->permission_add; 	
				
				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash(0);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);
				$data['url']	= $encoded_id."/".$salt."/".$token;
				
				$view_page	= "market_values";
			}
			else
			{
				$view_page	= "errors/html/error_permission";
			}
		
			$data['controller']	= __CLASS__;
			$data['page']		= $this->market_value_name;

			$this->template->load($view_page, $data, $resources);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
	
	public function get_market_value_list()
	{
		try{

			$params = get_params(); // DO NOT USE: $_POST $this->input->post
			
			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> 0,
				"iTotalDisplayRecords" 	=> 0,
				"aaData" 				=> array()
			);
			
			if(!$this->permission)
				throw new Exception($this->lang->line('err_unauthorized_view'));
		
			// FIELDS TO BE SELECTED FROM TABLE
			$select_fields = array(
				"A.market_value_id",
				"A.market_value_year"							
			);

			// APPEARS ON TABLE
			$where_fields = array(
				"A.market_value_year",					
			);
		
			$results 		= $this->market_value->get_market_value_list($this->market_value_type, $select_fields, $where_fields, $params);
			$total	 		= $this->market_value->total_length($this->market_value_type); // TOTAL COUNT OF RECORDS
			$filtered_total = $this->market_value->filtered_length($this->market_value_type, $select_fields, $where_fields, $params); // TOTAL COUNT OF RECORDS PER PAGE
		
			foreach ($results as $data):

				// PRIMARY KEY
				$primary_key	= $data["market_value_id"];

				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash($primary_key);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("market value", "'.$url.'")';

				// START: CONSTRUCT ACTION ICONS
				$action = "<div class='table-actions'>";
				
					if($this->permission_edit) :
						$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_market_value' onclick=\"modal_market_value_init('".$url."')\"></a>";
					endif;
					
					if($this->permission_delete) : 	  
						$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					endif;
				
				$action .= '</div>';
				// END: CONSTRUCT ACTION ICONS				

				$row 	= array();
				$row[] 	= $data['market_value_year'];
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
				
			endforeach;
			
			$output["iTotalRecords"] 		= $total["cnt"];
			$output["iTotalDisplayRecords"] = $filtered_total["cnt"];
		}
		catch(PDOException $e){
			$this->rlog_error($e);
		}
		catch(Exception $e){
			$this->rlog_error($e);
		}
		
		echo json_encode( $output );
	}
	
	public function modal_market_value($encoded_id, $salt, $token)
	{
		try{
			$msg	= "";
			$info	= array();
			$data 	= array();
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);
			
			$hash_id = base64_url_decode($encoded_id);
			
			$data['permission_save']	= $this->permission_add;
			
			if($this->hash(0) != $hash_id){
				
				$key 			= $this->get_hash_key('market_value_id');
				$where			= array();
				$where[$key]	= $hash_id;
				
				$info = $this->market_value->get_specific_market_value($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));
				

				$data['prices']				= $this->market_value->get_specific_market_value_prices($this->market_value_type, $info['market_value_id']);
				
				$data['permission_save']	= $this->permission_edit;
			}
			else
			{
				
				$data['prices']	= $this->params->get_params(MPIS_Model::tbl_param_price_types, array('market_value_type_id' => $this->market_value_type));
			}

			$data['info']				= $info; // MARKET VALUE DETAILS	

			
			$resources					= array();
			$resources['load_css'] 		= array(CSS_DATATABLE,CSS_DATETIMEPICKER, CSS_LABELAUTY, CSS_SELECTIZE,CSS_MODAL_COMPONENT);
			$resources['load_js'] 		= array(JS_DATATABLE, JS_DATETIMEPICKER, JS_LABELAUTY,JS_MODAL_CLASSIE, JS_SELECTIZE, JS_EDITOR);
			
			// SET PARAMS			
			
			$data['year_start']	= date('Y');
			$data['year_end']	= date('Y') - 5;						
				
			$data['controller']	= __CLASS__;
			
			// REGENERATE SECURITY VARIABLES
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			$this->load->view('modals/market_value',$data);
			$this->load_resources->get_resource($resources);			
		}
		catch(PDOException $e){
			$msg = $e->getMessage();
		}
		catch(Exception $e){
			$msg = $e->getMessage();
		}
		

	}
	
	public function process()
	{
		try
		{
			$flag 			= 0;
			$params 		= get_params();
			
			
						
			// SERVER VALIDATION
			$this->_validate($params);

	
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('market_value_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 				= $this->market_value->get_specific_market_value($where);
			$market_value_id	= $info['market_value_id'];

			MPIS_Model::beginTransaction();

			$activity_title	= $params['year'];
			
			$params['market_value_type_id']	= $this->market_value_type;
			
			// IF EMPTY MEANS INSERT/SAVE RECORD
			IF(EMPTY($market_value_id)){
				
				
				// CHECK IF THE RECORD IS ALREADY EXIST
				$exist = $this->_check_market_value($params);
				
				IF($exist)	
					throw new Exception('This record is arlready added.');
				
				
				if($this->permission_add === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_add'));
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= MPIS_Model::tbl_market_values;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array();
				
				$market_value_id = $this->market_value->insert_market_value($params); // SAVES THE BUILDING CONSTRUCTION RECORD
				
				// GET THE CURRENT DATA				
				$current_info 	= $this->market_value->get_specific_market_value(array("market_value_id" => $market_value_id));
				
				$curr_detail[]	= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				
				$audit_action[]	= AUDIT_INSERT;
				$audit_table[]	= MPIS_Model::tbl_market_value_prices;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array();
				
				$params['market_value_id']	= $market_value_id;
				$this->market_value->insert_market_value_prices($params); // SAVES THE BUILDING CONSTRUCTION REGIONS RECORD
				
				$current_info 	= $this->market_value->get_market_value_prices(array("market_value_id" => $market_value_id)); // GET THE CURRENT DATA
				
				$curr_detail[]	= $current_info;
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				

				$activity	= "%s has been added in " . $this->market_value_name . '. ' ;
				$activity 	= sprintf($activity, $activity_title);

				$msg  = $this->lang->line('data_saved');
			}

			else{

				if($this->permission_edit === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_edit'));
				
				$params['market_value_id']	= $market_value_id;
				
				
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				
				$prev_info 	= $this->market_value->get_specific_market_value(array("market_value_id" => $market_value_id)); // GET THE CURRENT DATA
				
				$activity_title	= $prev_info['market_value_year'];

				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= MPIS_Model::tbl_market_values;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array($prev_info);
				
				$this->market_value->update_market_value($params); // UPDATE THE BUILDING CONSTRUCTION RECORD
								
				$current_info 	= $this->market_value->get_specific_market_value(array("market_value_id" => $market_value_id)); // GET THE CURRENT DATA
				
				$curr_detail[]	= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				
				
				
				$constant	= $params['constant'];
				$current	= $params['current'];
				
				
				foreach($constant as $price_type_id => $constant_price)
				{

					// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
					
					$prev_info 	= $this->market_value->get_specific_market_value_prices($this->market_value_type, $market_value_id, $price_type_id); // GET THE CURRENT DATA
					
					$audit_action[]	= AUDIT_UPDATE;
					$audit_table[]	= MPIS_Model::tbl_market_value_prices;
					$audit_schema[]	= DB_MPIS;
					$prev_detail[]	= $prev_info;
					
					$params					= array();
					$params['market_value_id']	= $market_value_id;
					$params['price_type_id']	= $price_type_id;
					$params['constant_price']	= $constant_price;
					$params['current_price']	= $current[$price_type_id];

					
					$this->market_value->update_market_value_prices($params); // SAVES THE BUILDING CONSTRUCTION REGIONS RECORD					
					
					$current_info 	= $this->market_value->get_specific_market_value_prices($this->market_value_type, $market_value_id, $price_type_id);  // GET THE CURRENT DATA
					
					$curr_detail[]	= $current_info;
					// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
					
								
				}
				
				
				$activity	= "%s has been updated in " . $this->market_value_name . ". ";
				$activity 	= sprintf($activity, $activity_title);

				$msg  = $this->lang->line('data_updated');
			}

			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table, 
				$audit_schema
			);
			
			MPIS_Model::commit();

			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			MPIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
			
		}
		catch(Exception $e)
		{
			MPIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	
	}
	
	private function _check_market_value($params)
	{
		
		try {
			
			$where 							= array();
			$where['market_value_year']		= $params['year'];
			$where['market_value_type_id']	= $this->market_value_type;
					
			$info 	= $this->market_value->get_specific_market_value($where);
			
			return $info['market_value_id'];
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
		
	}
	
	private function _validate(&$params)
	{
		try
		{
			
			$this->_validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields 				= array();
			$fields['year']			= 'Year';

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _validate_input($params)
	{
		try
		{
			
			
			$validation['year'] = array(
				'data_type' => 'digit',
				'name'		=> 'Year'				
			);
			
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function delete_market_value($params) 
	{
		try 
		{

			$flag				= 0;
			$msg				= "Error";
			
			if($this->permission_delete === FALSE)
				throw new Exception($this->lang->line('err_unauthorized_delete'));
			
			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->_validate_security($params);

			$key 			= $this->get_hash_key('market_value_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];
			
			$info 			= $this->market_value->get_specific_market_value($where);
			
			if(EMPTY($info['market_value_id']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			$title 		= $info['market_value_year'];			
			
			MPIS_Model::beginTransaction();
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= MPIS_Model::tbl_market_value_prices;
			$audit_schema[]	= DB_MPIS;		
			
			$prev_info	= $this->market_value->get_market_value_prices(array('market_value_id' => $info['market_value_id']));
			
			$prev_detail[]	= $prev_info;
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL

			$this->market_value->delete_market_value_prices($info['market_value_id']); // DELETE BUILDING CONSTRUCTION REGION

			$curr_detail[]	= array();
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= MPIS_Model::tbl_market_values;
			$audit_schema[]	= DB_MPIS;
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			
			$this->market_value->delete_market_value($info['market_value_id']); // DELETE BUILDING CONSTRUCTION
			
			$curr_detail[]	= array();
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			

			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $title);

			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			MPIS_Model::commit();

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			MPIS_Model::rollback();

			$msg	= $e->getMessage(); 	
		}
		catch(Exception $e)
		{	

			MPIS_Model::rollback();
						
			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
	}
}

/* End of file Ceis_positions.php */
/* Location: ./application/modules/ceis/controllers/Ceis_positions.php */