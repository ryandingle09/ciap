<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_board_resolutions extends MPIS_Controller {
	
	private $module = MODULE_MPIS_POLICIES;
	
	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('policies_model', 'policies');
		$this->load->model('board_resolutions_model', 'board_resolutions');
		
		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);

	}
	

	
	public function process()
	{
		try
		{
			$flag 			= 0;
			$params 		= get_params();

			// SERVER VALIDATION
			$this->_validate($params);

	
			MPIS_Model::beginTransaction();
			
			
			/*
			 * START: INSERT/UPDATE POLICY RECORD 
			 */
			
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('policy_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];
			
			$info 		= $this->policies->get_specific_policy($where);
			$policy_id	= $info['policy_id'];
			
			// IF EMPTY MEANS INSERT/SAVE RECORD
			IF(EMPTY($policy_id)){
				
				if($this->permission_add === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_add'));				
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				$audit_action[]	= AUDIT_INSERT;
				$audit_table[]	= MPIS_Model::tbl_policies;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array();
				
				$br_no	= '';
				IF(!EMPTY($params['br_no']))
					$br_no	= $params['br_no'] . $this->number_formatter($params['br_no']) . ' ';
						
				$params['policy']	= 'BR No: ' .$br_no. ' dated ' . $params['br_date'];
				$policy_id 			= $this->policies->insert_policy($params); // SAVES THE POLICY
				
				$current_info 		= $this->policies->get_specific_policy(array('policy_id' => $policy_id));
				$activity_title		= $current_info['policy'];
				$curr_detail[]		= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				

				$activity	= "%s has been added in policies.";
				$activity 	= sprintf($activity, $activity_title);

				$msg  = $this->lang->line('data_saved');
			}
			else{

				if($this->permission_edit === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_edit'));
				
				

				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= MPIS_Model::tbl_policies;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array($info);
				
				$params['policy_id']	= $policy_id;
				
				$br_no	= '';
				IF(!EMPTY($params['br_no']))
					$br_no	= $params['br_no'] . $this->number_formatter($params['br_no']) . ' ';
						
				$params['policy']	= 'BR No: ' .$br_no. ' dated ' . $params['br_date'];								
				$this->policies->update_policy($params); // UPDATE THE RECORD
				
				$current_info 		= $this->policies->get_specific_policy(array('policy_id' => $policy_id));
				$activity_title		= $current_info['policy'];
				$curr_detail[]		= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			
				$activity	= "%s has been updated in policies.";
				$activity 	= sprintf($activity, $activity_title);

				$msg  = $this->lang->line('data_updated');
			}
			
			/*
			 * END: INSERT/UPDATE POLICY RECORD
			 */

			
			/*
			 * STARTT: INSERT/UPDATE BOARD RESOLUTION RECORD
			 */
			
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('policy_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];
				
			$info 			= $this->board_resolutions->get_specific_board_resolution($where);
			$policy_br_id	= $info['policy_id'];
				
			// IF EMPTY MEANS INSERT/SAVE RECORD
			IF(EMPTY($policy_br_id)){

			
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				$audit_action[]	= AUDIT_INSERT;
				$audit_table[]	= MPIS_Model::tbl_board_resolutions;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array();
						
				$params['policy_id']	= $policy_id;
				$this->board_resolutions->insert_board_resolution($params); // SAVES THE RECORD
			
				$current_info 		= $this->board_resolutions->get_specific_board_resolution(array('policy_id' => $policy_id));
				$curr_detail[]		= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			}
			else{
			
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				$audit_action[]	= AUDIT_UPDATE;
				$audit_table[]	= MPIS_Model::tbl_board_resolutions;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array($info);
			
				$params['policy_id']	= $policy_br_id;
				$this->board_resolutions->update_board_resolution($params); // UPDATE THE RECORD
			
				$current_info 		= $this->board_resolutions->get_specific_board_resolution(array('policy_id' => $policy_br_id));				
				$curr_detail[]		= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL

			}
				
			/*
			 * END: INSERT/UPDATE BOARD RESOLUTION RECORD
			 */			
			

			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table, 
				$audit_schema
			);
			
			MPIS_Model::commit();

			$info = array(
				"flag" => 1,
				"msg" => $msg
			);
			
			$this->rlog_debug('PROCESS INFO: ' . var_export($info, true));
	
			return $info;
			
		}
		catch(PDOException $e)
		{
			MPIS_Model::rollback();
			
			throw $e;			
		}
		catch(Exception $e)
		{
			MPIS_Model::rollback();
			
			throw $e;
		}
		
	}
	
	/*
	 * START: SERVER VALIDATION 
	 */
	
	private function _validate(&$params)
	{
		try
		{

			$this->_validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields 					= array();
			$fields["policy_category"]	= 'Policy Category';
			$fields["board"]			= 'Board';
			$fields["series"]			= 'Series';
			$fields["br_date"]			= 'Date';
			$fields["status"]			= 'Status';
			$fields["br_no"]			= 'No';
			$fields["br_title"]			= 'Title';
			$fields["br_body"]			= 'Body';
			
			
						

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['policy_security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['policy_security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _validate_input($params)
	{
		try
		{
			
			
			$validation['policy_category'] = array(
				'data_type' => 'digit',
				'name'		=> 'Policy Category'
			);
			
			$validation['board'] = array(
				'data_type' => 'digit',
				'name'		=> 'Board'
			);
			
			$validation['series'] = array(
				'data_type' => 'digit',
				'name'		=> 'Series'
			);
			
			$validation['br_date'] = array(
				'data_type' => 'date',
				'name'		=> 'Date'
			);

			$validation['status'] = array(
				'data_type' => 'string',
				'name'		=> 'Status'
			);
			
			$validation['br_no'] = array(
				'data_type' => 'digit',
				'name'		=> 'No'
			);
			
			$validation['br_title'] = array(
				'data_type' => 'string',
				'name'		=> 'Title',
				'min_length'=> 3,
				'max_length'=> 255
			);
			
			$validation['br_body'] = array(
				'data_type' => 'string',
				'name'		=> 'Body',
				'min_length'=> 3,
				'max_length'=> 60000
			);
			
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	/*
	 * END: SERVER VALIDATION
	 */
	
	
}

/* End of file Ceis_positions.php */
/* Location: ./application/modules/ceis/controllers/Ceis_positions.php */