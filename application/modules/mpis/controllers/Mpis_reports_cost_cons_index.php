<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_reports_cost_cons_index extends MPIS_Controller {
	
	private $year_start	= 0;
	private $year_end	= 0;
	private $price		= "";
	
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('market_value_model', 'market_value');
		$this->load->model('params_model', 'params');
	}
		
	public function generate()
	{		
		
		try {
			
			$params				= get_params();
				
			$this->year_start	= $params['year_start'];
			$this->year_end		= $params['year_end'];
			
			switch($params['value'])
			{
				case 'GVC':
					$price_type	= PRICE_TYPE_GVC;	
				break;
				
				case 'GVA':
					$price_type	= PRICE_TYPE_GVA;
				break;
				
				default:
					throw new Exception($this->lang->line('err_invalid_request'));
				break;
			}

			
			$price 			= $this->params->get_params(MPIS_Model::tbl_param_price_types, array('price_type_id' => $price_type));
			$this->price	= $price[0]['price_type_name'];
			
			$where = array();
		
			$where['B.market_value_type_id']	= MARKET_TYPE_CONSTRUCTION;
			$where['A.price_type_id']			= $price_type;
			
			if($params['year_start'] == $params['year_end'])
				$where['B.market_value_year']		= $params['year_end'];
			else
				$where['B.market_value_year']		= array(array($params['year_start'],$params['year_end']), array("BETWEEN"));
			
			$result	= $this->market_value->get_cost_construction_index_reports($where);
			
						
			$info	= array();
			foreach($result as $result)
			{
				$year	= $result['market_value_year'];
				$ipin	= $result['ipin']; // IMPLICIT PRICE INDEX
				
				$info[$year]	= $ipin;
			}
		
	
			$filter	= $this->_construct_filter_label(); 
			$head	= $this->_construct_report_header();
			$body	= $this->_construct_report_body($info);
			
			
			$info = array(
				'filter'	=> $filter,
				'content'	=> $head . $body 
					
			);
			
			
			
			return $info;
		}
		catch (PDOException $e)
		{
			throw $e;
		}
		catch (Exception $e)
		{
			throw $e;
		}
					
	}
	
	private function _construct_filter_label()
	{
		
		$year 		= ($this->year_start != $this->year_end) ? $this->year_start . ' to ' . $this->year_end : $this->year_end;				
		
		$filter = '<div>';
			$filter.= '<p>Year: '.$year.'</p>';
			$filter.= '<p>Value: '.$this->price.'</p>';
		$filter.= '</div>';
		
		return $filter;
		
	}
	
	
	private function _construct_report_header()
	{
		$head = '<thead>';
			$head.= '<tr>';
				$head.= '<th width="25%" align="center" >YEAR</th>';
				
				for($year = $this->year_start; $year <= $this->year_end; $year++)
					$head.= '<th align="center" >'.$year.'</th>';
				
			$head.= '<tr>';
		$head.= '</thead>';		
	
		
		return $head;
		
		
	}
	
	private function _construct_report_body($info)
	{
	
	
		$body = '<tbody>';
	
		for($year = $this->year_start; $year <= $this->year_end; $year++)
		{
			$body.= '<tr>';
				$body.= '<td align="center">'.$year.'</td>';
				
				for($year_col = $this->year_start; $year_col <= $this->year_end; $year_col++)
				{
					
					$value = ($year >= $year_col) ? number_format( (($info[$year] / $info[$year_col]) * 100), 2) : "&nbsp;";
					
										
					$body.= '<td align="right">'.$value.'</td>';
				}
			
			$body.= '</tr>';
			
		}
	
		$body.= '</tbody>';

		return $body;
		
	}
	
}

/* End of file Mpis_reports.php */
/* Location: ./application/modules/mpis/controllers/mpis_reports.php */