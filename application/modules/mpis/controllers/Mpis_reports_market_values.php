<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_reports_market_values extends MPIS_Controller {
	
	private $year_start			= 0;
	private $year_end			= 0;
	private $price				= "";
	private $price_label		= "";
	private $price_types		= NULL;
	private $market_value_type	= 0;
	
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('market_value_model', 'market_value');
		$this->load->model('params_model', 'params');
		

	}
		
	public function generate($market_value_type)
	{		
		
		try {
			
			$params				= get_params();
			
			
			$this->year_start			= $params['year_start'];
			$this->year_end				= $params['year_end'];
			$this->market_value_type	= $market_value_type;
			$this->price				= $params['price'];
			
			$this->price_types			= $this->params->get_params(MPIS_Model::tbl_param_price_types, array('market_value_type_id' => $market_value_type));
			
			switch($market_value_type)
			{
				case MARKET_TYPE_EXPENDITURE:
					$this->price_label	= 'TYPE OF EXPENDITURE';
				break;
				
				case MARKET_TYPE_INDUSTRIAL:
				case MARKET_TYPE_CONSTRUCTION:
					$this->price_label	= 'INDUSTRY';
				break;
				
			}
			
			$where = array();
			
			if($params['year_start'] == $params['year_end'])
				$where['B.market_value_year']	= $params['year_end'];
			else
				$where['B.market_value_year']	= array(array($params['year_start'],$params['year_end']), array("BETWEEN"));
			
			
			$where['B.market_value_type_id']	= $market_value_type;
											
			$result	= $this->market_value->get_market_value_reports($where);
			
			$info = array();
			foreach($result as $result)
			{
				
				$price_type	= $result['price_type_id'];
				$year 		= $result['market_value_year'];
				$price		= $result[strtolower($params['price']) . '_price'];
				
				$info[$price_type][$year]	= $price;
			}
			
			
			IF(strtoupper($params['generate']) == 'CHART')
			{
				$data		= array();
				$gvc_flag	= FALSE;
				
				switch($market_value_type)
				{
					case MARKET_TYPE_EXPENDITURE:
						$numerator		= MARKET_PRICE_TYPE_CONS;
						$denominator	= MARKET_PRICE_TYPE_CAPITAL_FORMAT;
						$gvc_flag		= FALSE;
					break;
				
					case MARKET_TYPE_INDUSTRIAL:
						$numerator		= MARKET_PRICE_TYPE_INDUS_CONS;
						$denominator	= MARKET_PRICE_TYPE_INDUS_GDP;
						$gvc_flag		= FALSE;
					break;
					
					case MARKET_TYPE_CONSTRUCTION:
						$gvc_flag	= TRUE;
					break;
				
				}
				
				for($year = $this->year_start; $year <= $this->year_end; $year++)
				{
					
					
					if($gvc_flag)
					{

						
						$private	= !EMPTY($info[MARKET_PRICE_TYPE_GV_PRIV][$year]) ? $info[MARKET_PRICE_TYPE_GV_PRIV][$year] : 0;						
						$public		= !EMPTY($info[MARKET_PRICE_TYPE_GV_PUB][$year]) ? $info[MARKET_PRICE_TYPE_GV_PUB][$year] : 0;
						$total		= $private + $public; 
						
						$data[]	= array(
							'year'		=> $year,
							'color'		=> '#2E4172',
							'private'	=> number_format($private,2),
							'public'	=> number_format($public,2),
							'total'		=> number_format($total,2)
						);
					}
					else
					{						
						$number			= !EMPTY($info[$numerator][$year]) ? $info[$numerator][$year] : 0;
						$total_number	= !EMPTY($info[$denominator][$year]) ? $info[$denominator][$year] : 0;
						$percent		= ($total_number > 0) ? ($number / $total_number) * 100 : 0;
							
						$data[]	= array(
								'year'		=> $year,
								'percent'	=> number_format($percent,2),
								'color'		=> '#2E4172'
						);
					}
				}
				
				$graph	= array();
				
				if($gvc_flag)
				{	
					$graph[] = array(
							"balloonText"	=> "[[category]] PUBLIC: <b>[[value]]</b>",
							"fillAlphas"	=> 0.9,
							"lineAlpha"		=> 0.2,
							"title"			=> 'Public',
							"type"			=> "column",
							"valueField"	=> "public"
					);
					
					$graph[] = array(
							"balloonText"	=> "[[category]] PRIVATE: <b>[[value]]</b>",
							"fillAlphas"	=> 0.9,
							"lineAlpha"		=> 0.2,
							"title"			=> 'Private',
							"type"			=> "column",
							"valueField"	=> "private"
					);
					
					$graph[] = array(
							"balloonText"	=> "[[category]] TOTAL : <b>[[value]]</b>",
							"fillAlphas"	=> 0.9,
							"lineAlpha"		=> 0.2,
							"title"			=> 'TOTAL',
							"type"			=> "column",
							"valueField"	=> "total"
					);
				}
				else 
				{
					
					$graph[]= array(
						"valueField"	=> "percent",
						"colorField"	=> "color",
						"type"			=> "column",
						"lineAlpha"		=> 0,
						"fillAlphas"	=> 1,
					);
				}
					
					
				$filter = $this->_construct_filter_label(1);
					
				$info	= array(
						'data'				=> json_encode($data),
						'graphs'			=> json_encode($graph),
						'category_field'	=> 'year',
						'filter'			=> $filter
				);
				
				
				return $info;				
			}
			ELSE
			{
				$filter	= $this->_construct_filter_label(); 
				$head	= $this->_construct_report_header();
				$body	= $this->_construct_report_body($info);
				
				$info = array(
						'filter'	=> $filter,
						'content'	=> $head . $body
							
				);
			}
			
			
			
			
			return $info;
		}
		catch (PDOException $e)
		{
			throw $e;
		}
		catch (Exception $e)
		{
			throw $e;
		}
					
	}
	
	private function _construct_filter_label($no_tags = 0)
	{
		
		$year 		= ($this->year_start != $this->year_end) ? $this->year_start . ' to ' . $this->year_end : $this->year_end;
						
		if($no_tags)
		{
			$filter = '\nYear: ' . $year;
			$filter.= '\nAt ' . $this->price . ' Price';
		}
		else 
		{
			$filter = '<div>';
				$filter.= '<p>Year: '.$year.'</p>';
				$filter.= '<p>At '.$this->price.' Price </p>';			
			$filter.= '</div>';
		}
		
		return $filter;
		
	}
	
	private function _construct_report_header()
	{
		

		$range		= ($this->year_end - $this->year_start) + 1;
		$rowspan	= ( $range > 1) ? ' rowspan="2" ' : "";
		$colspan	= ( $range > 1) ? ' colspan="'.$range.'" ' : "";
		
		$percent	= 5 * $range;
		
		
		
		$head = '<thead>';
			$head.= '<tr>';
				$head.= '<th width="25%" align="center" '.$rowspan.'>'.$this->price_label.'</th>';				
				$head.= '<th width="'.$percent.'%" align="center" '.$colspan.'>VALUE</th>';
				
				if($range > 1)
					$head.= '<th align="center" colspan="'.($range - 1).'">GROWTH RATE</th>';
			$head.= '</tr>';
			
			if($range > 1)
			{
				$head.= '<tr>';
					for($year = $this->year_start; $year <= $this->year_end; $year++)
						$head.= '<th align="center" >'.$year.'</th>';
					
					for($year = $this->year_start; $year < $this->year_end; $year++)
					{
						$label = $year . '-' . ($year + 1);
						$head.= '<th align="center" >'.$label.'</th>';
					}	
						
				$head.= '</tr>';
			}
			
			
		$head.= '</thead>';
	
		
		return $head;		
		
	}
	
	private function _construct_report_body($info)
	{
		
		$price_types		= $this->price_types;
		$range		= ($this->year_end - $this->year_start) + 1;
		

		$body = '<tbody>';
		
		foreach($price_types as $price_types)
		{
			$price_type_id		= $price_types['price_type_id'];
			$price_type_name	= $price_types['price_type_name'];
			
			$body.= '<tr>';
				$body.= '<td>'.$price_type_name.'</td>';
				
				for($year = $this->year_start; $year <= $this->year_end; $year++)
				{
					$value	= !EMPTY($info[$price_type_id][$year]) ? $info[$price_type_id][$year] : 0;
						
					$body.= '<td align="right">'.number_format($value, 2).'</td>';
				}
				
				if($range > 1)
				{
					for($year = $this->year_start; $year < $this->year_end; $year++)
					{
							
						$prev_value			= !EMPTY($info[$price_type_id][$year]) ? $info[$price_type_id][$year] : 0;
						$curr_value			= !EMPTY($info[$price_type_id][($year+1)]) ? $info[$price_type_id][($year+1)] : 0;
							
						$rate				= ($prev_value > 0) ? (($curr_value / $prev_value) -1 ) * 100 : 0;
										
						$body.= '<td align="right">'.number_format($rate, 2).'</td>';
					}	
				}
				
			$body.= '</tr>';
		}
		
		$body.= '</tbody>';
		
	
		return $body;
	}
}

/* End of file Mpis_reports.php */
/* Location: ./application/modules/mpis/controllers/mpis_reports.php */