<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_reports_construction_employment extends MPIS_Controller {
	
	private $year_start			= 0;
	private $year_end			= 0;
	private $quarter			= "Year-end";
	private $quarter_arr		= array(1 => '1st', 2 => '2nd', 3 => '3rd', 4 => '4th', 5 => 'Year-end');
	private $generate_chart		= FALSE;
	
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('cons_employment_model', 'cons_employment');
		$this->load->model('params_model', 'params');
		

	}
		
	public function generate()
	{		
		
		try {
			
			$params				= get_params();
			
			
			$this->year_start	= $params['year_start'];
			$this->year_end		= $params['year_end'];
			$this->quarter		= $this->quarter_arr[$params['quarter']];
			
			$this->groups		= $this->params->get_params(MPIS_Model::tbl_param_major_industry_groups);
			
			$where = array();
			
			if($params['year_start'] == $params['year_end'])
				$where['B.year']	= $params['year_end'];
			else
				$where['B.year']	= array(array($params['year_start'],$params['year_end']), array("BETWEEN"));
			
			if($params['quarter'] != 5) // 5 = YEAR END
				$where['B.quarter']	= $params['quarter'];
											
						
			$this->generate_chart	= (strtoupper($params['generate']) == 'CHART') ? TRUE : FALSE;		
			
			$result	= $this->cons_employment->get_cons_employment_reports($where);
			
			
			
			$info = array();
			foreach($result as $result)
			{
				
				$group_id	= $result['industry_group_id'];
				$year 		= $result['year'];
				$persons	= $result['employed_persons'];
				
				$info[$group_id][$year]	= $persons;
			}
					
		
			
			IF($this->generate_chart)
			{
				
				$data		= array();
				
				for($year = $this->year_start; $year <= $this->year_end; $year++)
				{
					
					$number			= !EMPTY($info[INDUSTRY_GROUP_CONS][$year]) ? $info[INDUSTRY_GROUP_CONS][$year] : 0;
					$total_number	= !EMPTY($info[INDUSTRY_GROUP_ALL][$year]) ? $info[INDUSTRY_GROUP_ALL][$year] : 0;
					$percent		= ($total_number > 0) ? ($number / $total_number) * 100 : 0;
					
					$data[]	= array(
						'year'		=> $year,
						'persons'	=> number_format($percent,2),
						'color'		=> '#2E4172'
					);	
				}
				
						
				$graph	= array();
				$graph[]= array(
						"valueField"	=> "persons",
						"colorField"	=> "color",
						"type"			=> "column",
						"lineAlpha"		=> 0,
						"fillAlphas"	=> 1,
				);
				
					
					
				$filter = $this->_construct_filter_label(1);
					
				$info	= array(
						'data'				=> json_encode($data),
						'graphs'			=> json_encode($graph),
						'category_field'	=> 'year',
						'filter'			=> $filter
				);
				

				return $info;				
					
			}
			ELSE
			{
				$filter	= $this->_construct_filter_label();
				$head	= $this->_construct_report_header();
				$body	= $this->_construct_report_body($info);
				
				$info = array(
						'filter'	=> $filter,
						'content'	=> $head . $body
							
				);
					
				return $info;
			}
			
			
			
			
		}
		catch (PDOException $e)
		{
			throw $e;
		}
		catch (Exception $e)
		{
			throw $e;
		}
					
	}
	
	private function _construct_filter_label($no_tags = 0)
	{
		
		$year 		= ($this->year_start != $this->year_end) ? $this->year_start . ' to ' . $this->year_end : $this->year_end;
						
		IF($no_tags)
		{
			$filter = '\nYear: ' . $year;
			$filter.= '\nQuarter: ' . $this->quarter;
		}
		ELSE
		{
			$filter = '<div>';
				$filter.= '<p>Year: '.$year.'</p>';		
				$filter.= '<p>Quarter: '.$this->quarter.'</p>';			
			$filter.= '</div>';
		}
		
		return $filter;
		
	}
	
	
	private function _construct_report_header()
	{
		

		$range		= ($this->year_end - $this->year_start) + 1;
		$rowspan	= ( $range > 1) ? ' rowspan="2" ' : "";
		$colspan	= ( $range > 1) ? ' colspan="'.$range.'" ' : "";
		
		$percent	= 5 * $range;
		
		$head = '<thead>';
			$head.= '<tr>';
				$head.= '<th width="25%" align="center" '.$rowspan.'>MAJOR INDUSTRY GROUP</th>';				
				$head.= '<th width="'.$percent.'%" align="center" '.$colspan.'>VALUE</th>';
				
				if($range > 1)
					$head.= '<th align="center" colspan="'.($range - 1).'">GROWTH RATE</th>';
			$head.= '</tr>';
			
			if($range > 1)
			{
				$head.= '<tr>';
					for($year = $this->year_start; $year <= $this->year_end; $year++)
						$head.= '<th align="center" >'.$year.'</th>';
					
					for($year = $this->year_start; $year < $this->year_end; $year++)
					{
						$label = $year . '-' . ($year + 1);
						$head.= '<th align="center" >'.$label.'</th>';
					}	
						
				$head.= '</tr>';
			}
			
			
		$head.= '</thead>';
	
		
		return $head;		
		
	}
	
	private function _construct_report_body($info)
	{
		
		$groups		= $this->groups;
		$range		= ($this->year_end - $this->year_start) + 1;
		

		$body = '<tbody>';
		
		foreach($groups as $groups)
		{
			$group_id	= $groups['industry_group_id'];
			$group		= $groups['industry_group_name'];
			
			$body.= '<tr>';
				$body.= '<td>'.$group.'</td>';
				
				for($year = $this->year_start; $year <= $this->year_end; $year++)
				{
					$persons	= !EMPTY($info[$group_id][$year]) ? $info[$group_id][$year] : 0;
						
					$body.= '<td align="right">'.number_format($persons, 0).'</td>';
				}
				
				if($range > 1)
				{
					for($year = $this->year_start; $year < $this->year_end; $year++)
					{
							
						$prev_value			= !EMPTY($info[$group_id][$year]) ? $info[$group_id][$year] : 0;
						$curr_value			= !EMPTY($info[$group_id][($year+1)]) ? $info[$group_id][($year+1)] : 0;
							
						$rate				= ($prev_value > 0) ? (($curr_value / $prev_value) -1 ) * 100 : 0;
										
						$body.= '<td align="right">'.number_format($rate, 2).'</td>';
					}	
				}
				
			$body.= '</tr>';
		}
		
		$body.= '</tbody>';
		
	
		return $body;
	}
}

/* End of file Mpis_reports.php */
/* Location: ./application/modules/mpis/controllers/mpis_reports.php */