<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_reports_cons_countries extends MPIS_Controller {
	
	private $year_start	= 0;
	private $year_end	= 0;
	private $indicator	= '';
	private $countries	= NULL;
	
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('cons_countries_model', 'cons_countries');
		$this->load->model('params_model', 'params');
		

	}
		
	public function generate()
	{		
		
		try {
			
			$params				= get_params();
				
			$this->year_start	= $params['year_start'];
			$this->year_end		= $params['year_end'];
			$indicator			= $this->params->get_params(MPIS_Model::tbl_param_construction_indicators, array('construction_indicator_id' => $params['indicator']));
			$this->indicator	= $indicator[0]['construction_indicator'];
			
			$where = array();
			
			$where['A.construction_indicator_id'] = $params['indicator'];
			
			if($params['year_start'] == $params['year_end'])
				$where['B.year']		= $params['year_end'];
			else
				$where['B.year']		= array(array($params['year_start'],$params['year_end']), array("BETWEEN"));
			
			$result	= $this->cons_countries->get_cons_countries_reports($where);
			
						
			$info		= array();
			$countries	= array();
			foreach($result as $result)
			{
				$country	= $result['country_code'];
				$name		= $result['country_name'];
				$year		= $result['year'];
				$value		= $result['value'];
				
				$countries[$country]	= $name;
				$info[$country][$year]	= $value;
				
				$info['TOTAL'][$year]	= ISSET($info['TOTAL'][$year]) ? $info['TOTAL'][$year] : 0;
				$info['TOTAL'][$year]  += $value;
			}
			
			$this->countries = $countries;
			
			IF(strtoupper($params['generate']) == 'CHART')
			{
				$data 	= array();
				$ctr	= 0;
				for($year = $this->year_start; $year <= $this->year_end; $year++)
				{
					$data[$ctr]['year']	= $year;
					
					foreach($countries as $country => $name)
					{
						
						$number			= !EMPTY($info[$country][$year]) ? $info[$country][$year] : 0;
						$total_number	= !EMPTY($info['TOTAL'][$year]) ? $info['TOTAL'][$year] : 0;
						$percent		= ($total_number > 0) ? ($number / $total_number) * 100 : 0;
												
						$data[$ctr][$country]	= number_format($percent, 2);
						
					}
					
					$ctr++;
				}
				
				$graph	= array();
				foreach($countries as $country => $name)
				{
					$graph[] = array(
						"balloonText"	=> "[[category]] ".$name.": <b>[[value]]</b>",
						"fillAlphas"	=> 0.9,
						"lineAlpha"		=> 0.2,
						"title"			=> $name,
						"type"			=> "column",
						"valueField"	=> $country
					);
				}
				
				$filter = $this->_construct_filter_label(1);
					
				$info	= array(
						'data'				=> json_encode($data),
						'graphs'			=> json_encode($graph),
						'category_field'	=> 'year',
						'filter'			=> $filter
				);
				
				
				return $info;
					
			}
			ELSE
			{
				
				
				$filter	= $this->_construct_filter_label(); 
				$head	= $this->_construct_report_header();
				$body	= $this->_construct_report_body($info);
				
				
				$info = array(
					'filter'	=> $filter,
					'content'	=> $head . $body 
						
				);
				
				return $info;
			}
		}
		catch (PDOException $e)
		{
			throw $e;
		}
		catch (Exception $e)
		{
			throw $e;
		}
					
	}
	
	private function _construct_filter_label($no_tags = 0)
	{
		
		$year 		= ($this->year_start != $this->year_end) ? $this->year_start . ' to ' . $this->year_end : $this->year_end;				
		
		if($no_tags)
		{
			$filter = '\nYear: ' . $year;
			$filter.= '\nIndicator: ' . $this->indicator;
		}
		else 
		{
			$filter = '<div>';
				$filter.= '<p>Year: '.$year.'</p>';
				$filter.= '<p>Indicator: '.$this->indicator.'</p>';
			$filter.= '</div>';
		}
		
		
		return $filter;
		
	}
	
	
	private function _construct_report_header()
	{
		
		$range		= ($this->year_end - $this->year_start) + 1;
		$rowspan	= ( $range > 1) ? ' rowspan="2" ' : "";
		$colspan	= ( $range > 1) ? ' colspan="'.$range.'" ' : "";
		
		$percent	= 5 * $range;
		
		
		$head = '<thead>';
			$head.= '<tr>';
				$head.= '<th width="25%" align="center" '.$rowspan.'>COUNTRIES</th>';
				$head.= '<th width="'.$percent.'%" align="center" '.$colspan.'>VALUE</th>';
		
				if($range > 1)
				{
					$head.= '<th align="center" colspan="'. $range .'">PERCENT SHARE</th>';
					$head.= '<th align="center" colspan="'.($range - 1).'">GROWTH RATE</th>';
				}
				
			$head.= '</tr>';
				
			if($range > 1)
			{
				$head.= '<tr>';
									
					for($year = $this->year_start; $year <= $this->year_end; $year++)
						$head.= '<th align="center" >'.$year.'</th>';
					
					for($year = $this->year_start; $year <= $this->year_end; $year++)
						$head.= '<th align="center" >'.$year.'</th>';
							
					for($year = $this->year_start; $year < $this->year_end; $year++)
					{
						$label = $year . '-' . ($year + 1);
						$head.= '<th align="center" >'.$label.'</th>';
					}
				
				$head.= '</tr>';
			}
				
				
			$head.= '</thead>';		
	
		
		return $head;
		
		
	}
	
	private function _construct_report_body($info)
	{
	
	
		$countries	= $this->countries;
		$range		= ($this->year_end - $this->year_start) + 1;
	
	
		$body = '<tbody>';
	
		foreach($countries as $code => $name)
		{
	
			$body.= '<tr>';
			$body.= '<td>'.$name.'</td>';
	
			for($year = $this->year_start; $year <= $this->year_end; $year++)
			{
				$value	= !EMPTY($info[$code][$year]) ? $info[$code][$year] : 0;
	
				$body.= '<td align="right">'.number_format($value, 2).'</td>';
			}
	
			if($range > 1)
			{
					
				for($year = $this->year_start; $year <= $this->year_end; $year++)
				{
					$value			= !EMPTY($info[$code][$year])? $info[$code][$year] : 0;
					$total_value	= !EMPTY($info['TOTAL'][$year])? $info['TOTAL'][$year] : 0;
					$percent		= ($total_value > 0) ? ($value / $total_value) * 100 : 0;
	
					$body.= '<td align="right">'.number_format($percent, 2).'</td>';
				}
	
				for($year = $this->year_start; $year < $this->year_end; $year++)
				{
	
					$prev_value			= !EMPTY($info[$code][$year]) ? $info[$code][$year] : 0;
					$curr_value			= !EMPTY($info[$code][($year+1)]) ? $info[$code][($year+1)] : 0;
	
					$rate				= ($prev_value > 0) ? (($curr_value / $prev_value) -1 ) * 100 : 0;
	
					$body.= '<td align="right">'.number_format($rate, 2).'</td>';
				}
			}
	
			$body.= '</tr>';
		}		
		
		$body.= '</tbody>';

		return $body;
		
	}
	
}

/* End of file Mpis_reports.php */
/* Location: ./application/modules/mpis/controllers/mpis_reports.php */