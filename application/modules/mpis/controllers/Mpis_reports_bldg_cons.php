<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_reports_bldg_cons extends MPIS_Controller {
	
	private $year_start			= 0;
	private $year_end			= 0;
	private $quarter			= "Year-end";
	private $quarter_arr		= array(1 => '1st', 2 => '2nd', 3 => '3rd', 4 => '4th', 5 => 'Year-end');
	private $sources			= NULL;
	private $summary_sources	= array(0 => 'Total', 1 => 'Residential Building Construction', 2 => 'Non Residential Building Construction');	
	private $regions			= NULL;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('building_construction_model', 'building_construction');
		$this->load->model('params_model', 'params');
		

	}
		
	public function generate($building_construction_type)
	{		
		
		try {
			
			$params				= get_params();
				
			$this->year_start	= $params['year_start'];
			$this->year_end		= $params['year_end'];
			$this->quarter		= $this->quarter_arr[$params['quarter']];
			
			
			$where	= array();
				
			if($params['building_type'] != "all")
				$where['source_id']						= $params['building_type'];
			else
				$where['building_construction_type']	= $building_construction_type;
						
			$this->sources		= $this->params->get_params(MPIS_Model::tbl_param_building_construction_sources, $where);
			
			$this->regions		= $this->params->get_params(MPIS_Model::tbl_param_building_construction_regions);
			
			
			$where = array();
			
			$where['A.building_construction_type_id']		= $building_construction_type;
			
			if($params['building_type'] != "all")
				$where['A.building_construction_source']	= $params['building_type'];
			
			if($params['year_start'] == $params['year_end'])
				$where['A.building_construction_year']		= $params['year_end'];
			else
				$where['A.building_construction_year']		= array(array($params['year_start'],$params['year_end']), array("BETWEEN"));
			
			if($params['quarter'] != 5) // 5 = YEAR END
				$where['A.building_construction_quarter']	= $params['quarter'];
			
			
			$group_by	= strtoupper($params['generate']) == "CHART" ? " GROUP BY A.building_construction_type_id, A.building_construction_year, B.region_id" : "";
			
			$result		= $this->building_construction->get_building_construction_reports($where);
			
			switch(strtoupper($params['generate']))
			{
				case "CHART":
					$info = array();
					
					foreach($result as $result)
					{
						$region_id		= $result['region_id'];
						$year			= $result['building_construction_year'];
						$value			= $result[$params['percent_share']];
						
						$info[$region_id][$year] = !EMPTY($info[$region_id][$year]) ? $info[$region_id][$year] : 0;
						$info[$region_id][$year]+= $value;
						
						$info['total'][$year] = !EMPTY($info['total'][$year]) ? $info['total'][$year] : 0;
						$info['total'][$year]+= $value;
					}
					
					
					$regions 	= $this->regions;					
					$data		= array();
					$ctr		= 0;
					foreach($regions as $regions)
					{
						$region_id				= $regions['region_id'];
						$data[$ctr]['region']	= $regions['region_code'];
						
						for($year = $this->year_start; $year <= $this->year_end; $year++)
						{
								
							$number			= !EMPTY($info[$region_id][$year]) ? $info[$region_id][$year] : 0;
							$total_number	= !EMPTY($info['total'][$year]) ? $info['total'][$year] : 0;
							$percent		= ($total_number > 0) ? ($number / $total_number) * 100 : 0;
								
								
								
							$data[$ctr]['year' . $year]	= number_format($percent,2);
						}
						
						$ctr++;
					}
					
					$graph	= array();
					for($year = $this->year_start; $year <= $this->year_end; $year++)
					{
						$graph[]	= array(
							"balloonText"	=> "[[category]] (".$year."): <b>[[value]]</b>",
							"fillAlphas"	=> 0.9,
							"lineAlpha"		=> 0.2,
							"title"			=> $year,
							"type"			=> "column",
							"valueField"	=> "year" . $year							
						);
					}
					
					
					$filter = $this->_construct_filter_label(0, 1);
					
					$info	= array(
						'data'				=> json_encode($data),
						'graphs'			=> json_encode($graph),
						'category_field'	=> 'region',
						'filter'			=> $filter							
					);
					
					
					return $info;
					
				break;
				
				case "PDF":
				case "EXCEL":
					$info	= array();
					foreach($result as $result)
					{
						$region_id	= $result['region_id'];
						$source_id	= $result['building_construction_source'];
						$year		= $result['building_construction_year'];
					
						$info[$region_id][$source_id][$year]['number']	= $result['number'];
						$info[$region_id][$source_id][$year]['floor']	= $result['floor_area'];
						$info[$region_id][$source_id][$year]['value']	= $result['value'];
					
						$info['PHIL'][$source_id][$year]['number']	= !EMPTY($info['PHIL'][$source_id][$year]['number']) ? $info['PHIL'][$source_id][$year]['number'] : 0;
						$info['PHIL'][$source_id][$year]['floor']	= !EMPTY($info['PHIL'][$source_id][$year]['floor']) ? $info['PHIL'][$source_id][$year]['floor'] : 0;
						$info['PHIL'][$source_id][$year]['value']	= !EMPTY($info['PHIL'][$source_id][$year]['value']) ? $info['PHIL'][$source_id][$year]['value'] : 0;
					
						$info['PHIL'][$source_id][$year]['number']	+= $result['number'];
						$info['PHIL'][$source_id][$year]['floor']	+= $result['floor_area'];
						$info['PHIL'][$source_id][$year]['value']	+= $result['value'];
					}
						
					$filter	= $this->_construct_filter_label();
					$head	= $this->_construct_report_header();
					$body	= $this->_construct_report_body($info);
						
						
					$info = array(
						'filter'	=> $filter,
						'content'	=> $head . $body								
					);
						
					return $info;						
				break;
			}
		}
		catch (PDOException $e)
		{
			throw $e;
		}
		catch (Exception $e)
		{
			throw $e;
		}
					
	}
	
	private function _construct_filter_label($detail = 1, $no_tags = 0)
	{
		
		$year 		= ($this->year_start != $this->year_end) ? $this->year_start . ' to ' . $this->year_end : $this->year_end;
		$source		= (count($this->sources) > 1) ? "All" : $this->sources[0]['source_name'];		
		
		if($no_tags)
		{
			$filter = '\nYear: ' . $year;
			$filter.= '\nQuarter: ' . $this->quarter;
			
			
				$filter.= '\nType of Building: '.$source;
		}
		else
		{
			$filter = '<div>';
				$filter.= '<p>Year: '.$year.'</p>';		
				$filter.= '<p>Quarter: '.$this->quarter.'</p>';
				
				if($detail)
					$filter.= '<p>Type of Building: '.$source.'</p>';
				
			$filter.= '</div>';
		}
		
		return $filter;
		
	}
	
	
	private function _construct_report_header()
	{
		
		$sources	= $this->sources;
		$range		= ($this->year_end - $this->year_start) + 1;
		
		if($range > 1)
		{
			$colspan = $range * 2 + ($range - 1);
			
			$head = '<thead>';
				$head.= '<tr>';
					$head.= '<th align="center" rowspan="4">Region</th>';
				
					$sub_head 		= '';
					$year_sub_head	= '';
					$percent_head	= '';	
				
					foreach($sources as $sources){
						$head.= '<th align="center" colspan="'.(3 * $colspan).'">'.$sources['source_name'].'</th>';
			
						$sub_head.= '<th align="center" colspan="'.$colspan.'">Number</th>';
						$sub_head.= '<th align="center" colspan="'.$colspan.'">Floor Area</th>';
						$sub_head.= '<th align="center" colspan="'.$colspan.'">Value</th>';
						
						for($i = 0; $i < 3; $i++)
						{	
							for($year = $this->year_start; $year <= $this->year_end; $year++)
								$year_sub_head.= '<th align="center" rowspan="2" >'.$year.'</th>';
									
							$year_sub_head.= '<th align="center" colspan="'.$range.'" >Percent Share</th>';
							$year_sub_head.= '<th align="center" colspan="'.($range-1).'" >Growth Rate</th>';
							
							for($year = $this->year_start; $year <= $this->year_end; $year++)
								$percent_head.= '<th align="center">'.$year.'</th>';
									
							for($year = $this->year_start; $year < $this->year_end; $year++)
							{
								$label = $year . '-' . ($year + 1);
								$percent_head.= '<th align="center" >'.$label.'</th>';
							}
						}
					}
				
				$head.= '</tr>';
			
				$head.= '<tr>';
					$head.= $sub_head;
				$head.= '</tr>';
				
				$head.= '<tr>';
					$head.= $year_sub_head;						
				$head.= '</tr>';
				
				$head.= '<tr>';
					$head.= $percent_head;							
				$head.= '</tr>';
				
			$head.= '</thead>';			
		}
		else 
		{
			
			$head = '<thead>';			
				$head.= '<tr>';
					$head.= '<th align="center" rowspan="2">Region</th>';
					
					$sub_head = '';
					
					foreach($sources as $sources){
						$head.= '<th align="center" colspan="3">'.$sources['source_name'].'</th>';
						
						$sub_head.= '<th align="center">Number</th>';
						$sub_head.= '<th align="center">Floor Area</th>';
						$sub_head.= '<th align="center">Value</th>';
						
					}
					
				$head.= '</tr>';
				
				$head.= '<tr>';
					$head.= $sub_head;					
				$head.= '</tr>';
			$head.= '</thead>';
		}
		
	
		
		return $head;
		
		
	}
	
	private function _construct_report_body($info)
	{
		
		
		$regions	= $this->regions;
		$sources	= $this->sources;
		$range		= ($this->year_end - $this->year_start) + 1;
		
		
		$body = '<tbody>';
		
		foreach($regions as $regions)
		{
			$region_id	= $regions['region_id'];
			
			$body.= '<tr>';
				$body.= '<td>'.$regions['region_name'].'</td>';
				
				
				foreach($sources as $source){
					$source_id	= $source['source_id'];
				
					for($year = $this->year_start; $year <= $this->year_end; $year++)
					{
						$number	= !EMPTY($info[$region_id][$source_id][$year]['number']) ? $info[$region_id][$source_id][$year]['number'] : 0;
													
						$body.= '<td align="right">'.number_format($number, 0).'</td>';
					}
					
					if($range > 1)
					{
						for($year = $this->year_start; $year <= $this->year_end; $year++)
						{
							$number			= !EMPTY($info[$region_id][$source_id][$year]['number'])? $info[$region_id][$source_id][$year]['number'] : 0;
							$total_number	= !EMPTY($info['PHIL'][$source_id][$year]['number'])? $info['PHIL'][$source_id][$year]['number'] : 0;
							$percent		= ($total_number > 0) ? ($number / $total_number) * 100 : 0; 
								
							$body.= '<td align="right">'.number_format($percent, 2).'</td>';
						}	
						
						for($year = $this->year_start; $year < $this->year_end; $year++)
						{
							
							
							$prev_value			= !EMPTY($info[$region_id][$source_id][$year ]['number']) ? $info[$region_id][$source_id][$year ]['number'] : 0;
							$curr_value			= !EMPTY($info[$region_id][$source_id][($year+1)]['number'])? $info[$region_id][$source_id][($year+1)]['number'] : 0;
							
							$rate				= ($prev_value > 0) ? (($curr_value / $prev_value) -1 ) * 100 : 0;
														
							$body.= '<td align="right">'.number_format($rate, 2).'</td>';
						}
					}
					
					for($year = $this->year_start; $year <= $this->year_end; $year++)
					{
						$floor	= !EMPTY($info[$region_id][$source_id][$year]['floor']) ? $info[$region_id][$source_id][$year]['floor'] : 0;
						
						
						$body.= '<td align="right">'.number_format($floor, 0).'</td>';
						
					}
					
					if($range > 1)
					{
						for($year = $this->year_start; $year <= $this->year_end; $year++)
						{
							$floor			= !EMPTY($info[$region_id][$source_id][$year]['floor']) ? $info[$region_id][$source_id][$year]['floor'] : 0;
							$total_floor	= !EMPTY($info['PHIL'][$source_id][$year]['floor']) ? $info['PHIL'][$source_id][$year]['floor'] : 0;
							$percent		= ($total_floor> 0) ? ($floor / $total_floor) * 100 : 0;
					
							$body.= '<td align="right">'.number_format($percent, 2).'</td>';
						}
						
						for($year = $this->year_start; $year < $this->year_end; $year++)
						{
							
							$prev_value			= !EMPTY($info[$region_id][$source_id][$year]['floor']) ? $info[$region_id][$source_id][$year]['floor'] : 0; 
							$curr_value			= !EMPTY($info[$region_id][$source_id][($year+1)]['floor']) ? $info[$region_id][$source_id][($year+1)]['floor'] : 0;
							
							$rate				= ($prev_value > 0) ? (($curr_value / $prev_value) -1 ) * 100 : 0;
														
							$body.= '<td align="right">'.number_format($rate, 2).'</td>';
						}
					}
					
					for($year = $this->year_start; $year <= $this->year_end; $year++)
					{						
						$value	= !EMPTY($info[$region_id][$source_id][$year]['value']) ? $info[$region_id][$source_id][$year]['value'] : 0;
						
						$body.= '<td align="right">'.number_format($value, 0).'</td>';
					}
					
					if($range > 1)
					{
						for($year = $this->year_start; $year <= $this->year_end; $year++)
						{
							$value			= !EMPTY($info[$region_id][$source_id][$year]['value']) ? $info[$region_id][$source_id][$year]['value'] : 0;
							$total_value	= !EMPTY($info['PHIL'][$source_id][$year]['value']) ? $info['PHIL'][$source_id][$year]['value'] : 0;
							$percent		= ($total_value > 0) ? ($value / $total_value) * 100 : 0;
								
							$body.= '<td align="right">'.number_format($percent, 2).'</td>';
						}
						
						for($year = $this->year_start; $year < $this->year_end; $year++)
						{
							
							$prev_value			= !EMPTY($info[$region_id][$source_id][$year]['value']) ? $info[$region_id][$source_id][$year]['value'] : 0;
							$curr_value			= !EMPTY($info[$region_id][$source_id][($year+1)]['value']) ? $info[$region_id][$source_id][($year+1)]['value'] : 0;
							
							$rate				= ($prev_value > 0) ? (($curr_value / $prev_value) -1 ) * 100 : 0;
														
							$body.= '<td align="right">'.number_format($rate, 2).'</td>';
						}
					}
					
				}
				
			$body.= '</tr>';
		}
		
		$body.= '</tbody>';
		
		return $body;
	}

	public function generate_summary()
	{
		try {
			$params				= get_params();
			
			$this->year_start	= $params['year_start'];
			$this->year_end		= $params['year_end'];
			$this->quarter		= $this->quarter_arr[$params['quarter']];
			$this->regions		= $this->params->get_params(MPIS_Model::tbl_param_building_construction_regions);
			
			$sources	= array();
			$sources[]	= array(
				'source_id'		=> 0,
				'source_name'	=> 'Total',
			);
			
			$fields		= array('building_construction_type_id source_id', 'building_construction_name source_name');
			$source_arr	= $this->params->get_params(MPIS_Model::tbl_param_building_construction_types, NULL, $fields);
			
			$this->sources	= array_merge($sources,  $source_arr);
			
			
			$where = array();
			
			if($params['year_start'] == $params['year_end'])
				$where['A.building_construction_year']		= $params['year_end'];
			else
				$where['A.building_construction_year']		= array(array($params['year_start'],$params['year_end']), array("BETWEEN"));
			
			if($params['quarter'] != 5) // 5 = YEAR END
				$where['A.building_construction_quarter']	= $params['quarter'];
							
			$result	= $this->building_construction->get_building_construction_summary_reports($where);
			
			switch(strtoupper($params['generate']))
			{
				case "CHART":
					$info = array();
						
					foreach($result as $result)
					{
						$region_id		= $result['region_id'];
						$type			= $result['building_construction_type_id'];
						$year			= $result['building_construction_year'];
						$value			= $result[$params['percent_share']];
			
						$info[$region_id][$type][$year] = !EMPTY($info[$region_id][$type][$year]) ? $info[$region_id][$type][$year] : 0;
						$info[$region_id][$type][$year]+= $value;
			
						$info['total'][$type][$year] = !EMPTY($info['total'][$type][$year]) ? $info['total'][$type][$year] : 0;
						$info['total'][$type][$year]+= $value;
					}
						
						
					$regions 	= $this->regions;
					$data		= array();
					$ctr		= 0;
					foreach($regions as $regions)
					{
						$region_id	= $regions['region_id'];
						$sources	= $this->sources;
						
						foreach($sources as $sources)
						{
							$source_id = $sources['source_id'];
							
							if($source_id == 0) 
								continue;
							
							$source_label			= ($source_id == 1) ? 'Residential' : 'Non Residential';	
							
							$data[$ctr]['region']	= $regions['region_code'];
							$data[$ctr]['label']	= $regions['region_code'] . ' ' .$source_label;
				
							for($year = $this->year_start; $year <= $this->year_end; $year++)
							{
				
								$number			= !EMPTY($info[$region_id][$source_id][$year]) ? $info[$region_id][$source_id][$year] : 0;
								$total_number	= !EMPTY($info['total'][$source_id][$year]) ? $info['total'][$source_id][$year] : 0;
								$percent		= ($total_number > 0) ? ($number / $total_number) * 100 : 0;
				
								$data[$ctr]['year' . $year]	= number_format($percent,2);
							}
				
							$ctr++;
						}
					}
						
					$graph	= array();
					for($year = $this->year_start; $year <= $this->year_end; $year++)
					{
						$graph[]	= array(
								"balloonText"	=> "[[label]] (".$year."): <b>[[value]]</b>",
								"fillAlphas"	=> 0.9,
								"lineAlpha"		=> 0.2,
								"title"			=> $year,
								"type"			=> "column",
								"valueField"	=> "year" . $year
						);
					}
						
						
					$filter = $this->_construct_filter_label(0, 1);
						
					$info	= array(
							'data'				=> json_encode($data),
							'graphs'			=> json_encode($graph),
							'category_field'	=> 'region',
							'filter'			=> $filter
					);
						
						
					return $info;
						
					break;
			
				case "PDF":
				case "EXCEL":
			
					$info	= array();
						
					foreach($result as $result)
					{
						$region_id	= $result['region_id'];
						$type		= $result['building_construction_type_id'];
						$year		= $result['building_construction_year'];
						
						// RESIDENTIAL AND NON-RESIDENTIAL DATA
						$info[$region_id][$type][$year]['number']	= $result['number'];
						$info[$region_id][$type][$year]['floor']	= $result['floor'];
						$info[$region_id][$type][$year]['value']	= $result['value'];
						
						$info['PHIL'][$type][$year]['number']	= !EMPTY($info['PHIL'][$type][$year]['number']) ? $info['PHIL'][$type][$year]['number'] : 0;
						$info['PHIL'][$type][$year]['floor']	= !EMPTY($info['PHIL'][$type][$year]['floor']) ? $info['PHIL'][$type][$year]['floor'] : 0;
						$info['PHIL'][$type][$year]['value']	= !EMPTY($info['PHIL'][$type][$year]['value']) ? $info['PHIL'][$type][$year]['value'] : 0;
						
						$info['PHIL'][$type][$year]['number']	+= $result['number'];
						$info['PHIL'][$type][$year]['floor']	+= $result['floor'];
						$info['PHIL'][$type][$year]['value']	+= $result['value'];
		
						// TOTAL RESIDENTIAL AND NON-RESIDENTIAL DATA
						
						$info[$region_id][0][$year]['number']	= !EMPTY($info[$region_id][0][$year]['number']) ? $info[$region_id][0][$year]['number'] : 0;
						$info[$region_id][0][$year]['floor']	= !EMPTY($info[$region_id][0][$year]['floor']) ? $info[$region_id][0][$year]['floor'] : 0;
						$info[$region_id][0][$year]['value']	= !EMPTY($info[$region_id][0][$year]['value']) ? $info[$region_id][0][$year]['value'] : 0;
						
						
						$info[$region_id][0][$year]['number']	+= $result['number'];
						$info[$region_id][0][$year]['floor']	+= $result['floor'];
						$info[$region_id][0][$year]['value']	+= $result['value'];
						
						
						$info['PHIL'][0][$year]['number']	= !EMPTY($info['PHIL'][0][$year]['number']) ? $info['PHIL'][0][$year]['number'] : 0;
						$info['PHIL'][0][$year]['floor']	= !EMPTY($info['PHIL'][0][$year]['floor']) ? $info['PHIL'][0][$year]['floor'] : 0;
						$info['PHIL'][0][$year]['value']	= !EMPTY($info['PHIL'][0][$year]['value']) ? $info['PHIL'][0][$year]['value'] : 0;
						
						
						$info['PHIL'][0][$year]['number']	+= $result['number'];
						$info['PHIL'][0][$year]['floor']	+= $result['floor'];
						$info['PHIL'][0][$year]['value']	+= $result['value'];
						
					}
					
					$filter	= $this->_construct_filter_label($detail = 0);
					$head	= $this->_construct_report_header();
					$body	= $this->_construct_report_body($info);
					
					$info = array(
						'filter'	=> $filter,
						'content'	=> $head . $body
								
					);
						
					return $info;								
				break;
			}		
			
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
		
	}
}

/* End of file Mpis_reports.php */
/* Location: ./application/modules/mpis/controllers/mpis_reports.php */