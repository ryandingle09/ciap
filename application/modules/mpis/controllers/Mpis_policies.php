<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_policies extends MPIS_Controller {
	
	private $module = MODULE_MPIS_POLICIES;
	
	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('policies_model', 'policies');
		$this->load->model('params_model', 'params');
		
		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);

	}
	
	public function index()
	{	
		try{
			$data = $resources = $modal = array();
						
			if($this->permission)
			{
				$modal = array(
					'modal_policy' => array(
						'module'		=> PROJECT_MPIS,
						'controller'	=> __CLASS__,
						'method'		=> 'modal_policy',
						'title'			=> 'Policies',
						'size'			=> 'xl',					
						'multiple'		=> TRUE
					),					
				);
				
				$resources['load_css'] 		= array(CSS_DATATABLE);
				$resources['load_js'] 		= array(JS_DATATABLE);
				$resources['load_modal'] 	= $modal;
				$resources['datatable']		= array();
				$resources['datatable'][] 	= array(
					'table_id' 	=> 'policies_table', 
					'path' 		=> PROJECT_MPIS.'/mpis_policies/get_policies_list'				
				);
				
				
				// STORE THE PERMISSION
				$data['permission_add']	= $this->permission_add; 	
				
				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash(0);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);
				$data['url']	= $encoded_id."/".$salt."/".$token;
				
				$view_page	= "policies";
			}
			else
			{
				$view_page	= "errors/html/error_permission";
			}

			$this->template->load($view_page, $data, $resources);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
	
	public function get_policies_list()
	{
		try{

			$params = get_params(); // DO NOT USE: $_POST $this->input->post
			
			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> 0,
				"iTotalDisplayRecords" 	=> 0,
				"aaData" 				=> array()
			);
			
			if(!$this->permission)
				throw new Exception($this->lang->line('err_unauthorized_view'));
	
		
			// FIELDS TO BE SELECTED FROM TABLE
			$select_fields = array(
				"A.policy_id",
				"A.policy",
				"B.policy_category_name",
				"C.board_code"				
			);

			// APPEARS ON TABLE
			$where_fields = array(
				"A.policy",
				"B.policy_category_name",
				"C.board_code"
			);
		
			$results 		= $this->policies->get_policies_list($select_fields, $where_fields, $params);
			$total	 		= $this->policies->total_length(); // TOTAL COUNT OF RECORDS
			$filtered_total = $this->policies->filtered_length($select_fields, $where_fields, $params); // TOTAL COUNT OF RECORDS PER PAGE
			
			$ctr	= 0;
			foreach ($results as $data):
			
				$ctr++;
			
				// PRIMARY KEY
				$primary_key	= $data["policy_id"];

				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash($primary_key);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("policy", "'.$url.'")';

				// START: CONSTRUCT ACTION ICONS
				$action = "<div class='table-actions'>";
				
					if($this->permission_edit) :
						$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_policy' onclick=\"modal_policy_init('".$url."')\"></a>";
					endif;
					
					if($this->permission_delete) : 	  
						$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					endif;
				
				$action .= '</div>';
				// END: CONSTRUCT ACTION ICONS				

				$row 	= array();
				$row[] 	= $data['policy_category_name'];
				$row[] 	= $data['policy'];				
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
				
			endforeach;
			
			$output["iTotalRecords"] 		= $total["cnt"];
			$output["iTotalDisplayRecords"] = $filtered_total["cnt"];
		}
		catch(PDOException $e){
			$this->rlog_error($e);
		}
		catch(Exception $e){
			$this->rlog_error($e);
		}
		
		echo json_encode( $output );
	}
	
	public function modal_policy($encoded_id, $salt, $token)
	{
		try{
			$msg	= "";
			$info	= array();
			$data 	= array();
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);
			
			$hash_id = base64_url_decode($encoded_id);
			
			$data['permission_save']	= $this->permission_add;
			
			if($this->hash(0) != $hash_id){
				
				$key 			= $this->get_hash_key('policy_id');
				$where			= array();
				$where[$key]	= $hash_id;
				
				$info 			= $this->policies->get_specific_policy($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));
				
				$data['permission_save']	= $this->permission_edit;
			}
			
			
			$data['info']		= $info; // policy DETAILS
		
			// GET PARAMS
			$data['categories']	= $this->params->get_params(MPIS_Model::tbl_param_policy_categories);
			$data['boards']		= $this->params->get_params(MPIS_Model::tbl_param_boards);
			
			// REGENERATE SECURITY VARIABLES
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			$this->load->view('modals/policies',$data);
			
			$resources				= array();
			$resources['load_css'] 	= array(CSS_SELECTIZE);
			$resources['load_js'] 	= array(JS_SELECTIZE, JS_EDITOR);
			$this->load_resources->get_resource($resources);			
		}
		catch(PDOException $e){
			$msg = $e->getMessage();
		}
		catch(Exception $e){
			$msg = $e->getMessage();
		}
	}
	
	public function load_form()
	{
		try 
		{
			$params		= get_params();
			$category	= $params['category'];
			$err_msg	= "";
			
			$security	= explode('/', $params['security']);
			$encoded_id	= $security[0];
			$salt		= $security[1];
			$token		= $security[2];
						
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);
				
			$hash_id = base64_url_decode($encoded_id);
			
			$data		= array();
			$resources	= array();

			
			switch($category)
			{
				case POL_CAT_BM:
					
					
					$modal = array(
						'modal_agenda' => array(
							'module'		=> PROJECT_MPIS,
							'controller'	=> 'mpis_meeting_agendas',
							'method'		=> 'modal_agenda',
							'title'			=> 'Agenda',
							'size'			=> 'l',
							'multiple'		=> TRUE
						),
					);
					
					
					$form_name					= 'policy_board_meeting';
					
					// GET THE POLICY INFORMATION
					$info	= array();
					if($hash_id != $this->hash(0))
					{
						$this->load->model('board_meetings_model', 'board_meetings');
						
						$key	= $this->get_hash_key('policy_id');
						$where	= array($key => $hash_id);
						$info	= $this->board_meetings->get_specific_board_meeting($where);
					}
					
					$data['info']				= $info;
					
					$resources['load_modal'] 	= $modal;
					$resources['load_css']		= array(CSS_DATETIMEPICKER, CSS_SELECTIZE, CSS_LABELAUTY, CSS_MODAL_COMPONENT);
					$resources['load_js'] 		= array(JS_DATETIMEPICKER, JS_SELECTIZE, JS_LABELAUTY, JS_MODAL_EFFECTS, JS_MODAL_CLASSIE, JS_EDITOR);
					
					
					// CONSTRUCT SECURITY VARIABLES
					$hash_id 			= $this->hash(0);
					$encoded_id 		= base64_url_encode($hash_id);
					$salt 				= gen_salt();
					$token 				= in_salt($encoded_id, $salt);
					$data['agenda_url']	= $encoded_id."/".$salt."/".$token;
					
					
					$data['meeting_types']	= $this->params->get_params(MPIS_Model::tbl_param_meeting_types);
				break;
				
				case POL_CAT_BR:
					$modal = array(
						'modal_change_status' => array(
							'module'		=> PROJECT_MPIS,
							'controller'	=> 'mpis_board_resolutions',
							'method'		=> 'modal_change_status',
							'title'			=> 'Status',
							'size'			=> 'l',
							'multiple'		=> TRUE
						),
						
						'modal_view_history' => array(
							'module'		=> PROJECT_MPIS,
							'controller'	=> 'mpis_board_resolutions',
							'method'		=> 'modal_view_history',
							'title'			=> 'View History',
							'size'			=> 'l',
							'multiple'		=> TRUE
						)
					);			

					$form_name				= 'policy_board_resolution';
						
					// GET THE POLICY INFORMATION
					$info	= array();
					if($hash_id != $this->hash(0))
					{
						$this->load->model('board_resolutions_model', 'board_resolutions');
					
						$key	= $this->get_hash_key('policy_id');
						$where	= array($key => $hash_id);
						$info	= $this->board_resolutions->get_specific_board_resolution($where);
					}
						
					$data['info']				= $info;
					
					$data['year_start'] = MPIS_START_YEAR;
					$data['year_end']	= date('Y');
					
					$resources['load_modal'] 	= $modal;
					$resources['load_css']		= array(CSS_DATETIMEPICKER, CSS_SELECTIZE, CSS_LABELAUTY, CSS_MODAL_COMPONENT);
					$resources['load_js'] 		= array(JS_DATETIMEPICKER, JS_SELECTIZE, JS_LABELAUTY, JS_MODAL_EFFECTS, JS_MODAL_CLASSIE, JS_EDITOR);
						
				break;
				
				case POL_CAT_ADMIN:
				break;
				
				case POL_CAT_LAE:
				break;
				
				case POL_CAT_BILL:
				break;
				
				case POL_CAT_CASE:
				break;
			}
			
			

			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			$err_msg = $this->get_user_message($e);
		}
		catch(Exception $e)
		{
			$err_msg = $this->rlog_error($e, TRUE);
		}
		
		$data['err_msg']	= $err_msg;
		echo $err_msg;

		$this->load->view('modals/' . $form_name, $data);
		$this->load_resources->get_resource($resources);
	}
	
	public function process()
	{
		try
		{
			$flag 			= 0;
			$params 		= get_params();
			
			
			switch($params['policy_category'])
			{
				case POL_CAT_BM:
					$controller_name = 'mpis_board_meetings';					
				break;
				
				case POL_CAT_BR:
					$controller_name = 'mpis_board_resolutions';
				break;
				
				case POL_CAT_ADMIN:
					$controller_name = 'mpis_board_resolutions';
				break;
				
				case POL_CAT_BILL:
					$controller_name = 'mpis_board_bills';
				break;
				
				case POL_CAT_LAW:
					$controller_name = 'mpis_laws';
				break;
				
				case POL_CAT_CASE:
					$controller_name = 'mpis_cases';
				break;
				
			}
	
			$policy_obj	= modules::load('mpis/' . $controller_name);
			$result		= $policy_obj->process();
			$flag		= $result['flag'];
			$msg		= $result['msg'];
			
			if(EMPTY($flag))
				throw new Exception($msg);
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			$msg = $this->get_user_message($e);			
		}
		catch(Exception $e)
		{
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	
	}
	
	public function delete_policy() 
	{
		try 
		{

			$flag				= 0;
			$msg				= $this->lang->line('err_internal_server');
						
			if($this->permission_delete === FALSE)
				throw new Exception($this->lang->line('err_unauthorized_delete'));
			
			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->_validate_security($params);

			$key 			= $this->get_hash_key('policy_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];
			
			$info 			= $this->policies->get_specific_policy($where);

			if(EMPTY($info['policy_id']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			

			
			MPIS_Model::beginTransaction();
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= MPIS_Model::tbl_policies;
			$audit_schema[]	= DB_MPIS;			
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL

			$this->policies->delete_policy($info['policy_id']);

			// SET AS AN ARRAY
			$curr_detail[]	= array();
			
			// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			$title		= $info['policy'];
			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $title);
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			MPIS_Model::commit();

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			MPIS_Model::rollback();

			$this->rlog_error($e);
			
			$msg	= $this->get_user_message($e); 	
		}
		catch(Exception $e)
		{	

			MPIS_Model::rollback();
			
			$this->rlog_error($e);
						
			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
	
	
			$security = explode('/', $params['security']);
	
			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];
	
			check_salt($params['encoded_id'], $params['salt'], $params['token']);
	
			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
}

/* End of file Mpis_policies.php */
/* Location: ./application/modules/mpis/controllers/Mpis_policies.php */