<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_financial_ratios extends MPIS_Controller {
	
	private $module 		= MODULE_MPIS_FINANCIAL_RATIOS;
	
	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('financial_ratios_model', 'financial_ratios');
		$this->load->model('params_model', 'params');
		
		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);

	}
	
	public function index()
	{	
		try{
			$data = $resources = $modal = array();
			
			if($this->permission)
			{
				$modal = array(
					'modal_financial_ratios' => array(
						'module'		=> PROJECT_MPIS,
						'controller'	=> __CLASS__,
						'method'		=> 'modal_financial_ratios',
						'title'			=> 'Financial Ratios',
						'size'			=> 'lg',
						'multiple'		=> TRUE
					)
				);
				
				$resources['load_css'] 		= array(CSS_DATATABLE);
				$resources['load_js'] 		= array(JS_DATATABLE, JS_EDITOR);
				$resources['load_modal'] 	= $modal;
				$resources['datatable']		= array();
				$resources['datatable'][] 	= array(
					'table_id' 	=> 'financial_ratios_table', 
					'path' 		=> PROJECT_MPIS.'/mpis_financial_ratios/get_financial_ratios_list'				
				);
				
				
				// STORE THE PERMISSION
				$data['permission_add']	= $this->permission_add; 	
				
				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash(0);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);
				$data['url']	= $encoded_id."/".$salt."/".$token;
				
				$view_page	= "financial_ratios";
			}
			else
			{
				$view_page	= "errors/html/error_permission";
			}
		
			$data['controller']	= __CLASS__;
			

			$this->template->load($view_page, $data, $resources);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
	
	public function get_financial_ratios_list()
	{
		try{

			$params = get_params(); // DO NOT USE: $_POST $this->input->post
			
			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> 0,
				"iTotalDisplayRecords" 	=> 0,
				"aaData" 				=> array()
			);
			
			if(!$this->permission)
				throw new Exception($this->lang->line('err_unauthorized_view'));
		
			// FIELDS TO BE SELECTED FROM TABLE
			$select_fields = array(
				"A.financial_ratio_id",
				"A.financial_ratio_year",
				"A.company_name",
				"B.company_type_name",
				"C.license_category"			
			);

			// APPEARS ON TABLE
			$where_fields = array(
				"A.financial_ratio_year",
				"B.company_type_name",	
				"C.license_category",
				"A.company_name"
			);
		
			$results 		= $this->financial_ratios->get_financial_ratios_list($select_fields, $where_fields, $params);
			$total	 		= $this->financial_ratios->total_length(); // TOTAL COUNT OF RECORDS
			$filtered_total = $this->financial_ratios->filtered_length($select_fields, $where_fields, $params); // TOTAL COUNT OF RECORDS PER PAGE
		
			foreach ($results as $data):

				// PRIMARY KEY
				$primary_key	= $data["financial_ratio_id"];

				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash($primary_key);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("financial ratio", "'.$url.'")';

				// START: CONSTRUCT ACTION ICONS
				$action = "<div class='table-actions'>";
				
					if($this->permission_edit) :
						$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_financial_ratios' onclick=\"modal_financial_ratios_init('".$url."')\"></a>";
					endif;
					
					if($this->permission_delete) : 	  
						$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					endif;
				
				$action .= '</div>';
				// END: CONSTRUCT ACTION ICONS				

				$row 	= array();
				$row[] 	= $data['financial_ratio_year'];
				$row[] 	= $data['company_type_name'];
				$row[] 	= $data['license_category'];
				$row[] 	= $data['company_name'];
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
				
			endforeach;
			
			$output["iTotalRecords"] 		= $total["cnt"];
			$output["iTotalDisplayRecords"] = $filtered_total["cnt"];
		}
		catch(PDOException $e){
			$this->rlog_error($e);
		}
		catch(Exception $e){
			$this->rlog_error($e);
		}
		
		echo json_encode( $output );
	}
	
	public function modal_financial_ratios($encoded_id, $salt, $token)
	{
		try{
			$msg	= "";
			$info	= array();
			$data 	= array();
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);
			
			$hash_id = base64_url_decode($encoded_id);
			
			$data['permission_save']	= $this->permission_add;
			
			if($this->hash(0) != $hash_id){
				
				$key 			= $this->get_hash_key('financial_ratio_id');
				$where			= array();
				$where[$key]	= $hash_id;
				
				$info = $this->financial_ratios->get_specific_financial_ratio($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));
				

				$data['items']				= $this->financial_ratios->get_specific_financial_ratio_items($info['financial_ratio_id']);
				
				$data['permission_save']	= $this->permission_edit;
			}
			else
			{
				$data['items']				= $this->params->get_params(MPIS_Model::tbl_param_financial_ratio_items);
			}

			$data['info']				= $info; // FINANCIAL RATIOS					
			
			
			$resources					= array();
			$resources['load_css'] 		= array(CSS_DATATABLE,CSS_DATETIMEPICKER, CSS_LABELAUTY, CSS_SELECTIZE,CSS_MODAL_COMPONENT);
			$resources['load_js'] 		= array(JS_DATATABLE, JS_DATETIMEPICKER, JS_LABELAUTY,JS_MODAL_CLASSIE, JS_SELECTIZE, JS_EDITOR);
			
			// SET PARAMS			
			$data['company_types']		= $this->params->get_params(MPIS_Model::tbl_param_company_types);
			$data['categories']			= $this->params->get_params(MPIS_Model::tbl_param_license_categories);
			$data['year_start']			= date('Y');
			$data['year_end']			= date('Y') - 5;						

			
			// REGENERATE SECURITY VARIABLES
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			$this->load->view('modals/financial_ratios',$data);
			$this->load_resources->get_resource($resources);			
		}
		catch(PDOException $e){
			$msg = $e->getMessage();
		}
		catch(Exception $e){
			$msg = $e->getMessage();
		}
		

	}
	
	public function process()
	{
		try
		{
			$flag 			= 0;
			$params 		= get_params();
			
			
						
			// SERVER VALIDATION
			$this->_validate($params);

	
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('financial_ratio_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 				= $this->financial_ratios->get_specific_financial_ratio($where);
			$financial_ratio_id	= $info['financial_ratio_id'];

			MPIS_Model::beginTransaction();

			$activity_title	= $params['company_name'];
			
			
			// IF EMPTY MEANS INSERT/SAVE RECORD
			IF(EMPTY($financial_ratio_id)){
				
				
				// CHECK IF THE RECORD IS ALREADY EXIST
				$exist = $this->_check_financial_ratios($params);
				
				IF($exist)	
					throw new Exception('This record is already added.');
				
				
				if($this->permission_add === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_add'));
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= MPIS_Model::tbl_financial_ratios;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array();
				
				$financial_ratio_id = $this->financial_ratios->insert_financial_ratios($params); // SAVES THE FINANCIAL RATIO
				
				// GET THE CURRENT DATA				
				$current_info 	= $this->financial_ratios->get_specific_financial_ratio(array("financial_ratio_id" => $financial_ratio_id));
				
				$curr_detail[]	= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				
				$audit_action[]	= AUDIT_INSERT;
				$audit_table[]	= MPIS_Model::tbl_financial_ratio_items;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array();
				
				$params['financial_ratio_id']	= $financial_ratio_id;
				$this->financial_ratios->insert_financial_ratio_items($params); // SAVES THE BUILDING CONSTRUCTION REGIONS RECORD
				
				$current_info 	= $this->financial_ratios->get_financial_ratio_items(array("financial_ratio_id" => $financial_ratio_id)); // GET THE CURRENT DATA
				
				$curr_detail[]	= $current_info;
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				

				$activity	= "%s has been added in financial ratios.";
				$activity 	= sprintf($activity, $activity_title);

				$msg  = $this->lang->line('data_saved');
			}

			else{

				if($this->permission_edit === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_edit'));
				
				$params['financial_ratio_id']	= $financial_ratio_id;
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				
				$prev_info 	= $this->financial_ratios->get_specific_financial_ratio(array("financial_ratio_id" => $financial_ratio_id)); // GET THE CURRENT DATA
				
				$activity_title	= $prev_info['company_name'];

				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= MPIS_Model::tbl_financial_ratios;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array($prev_info);
				
				$this->financial_ratios->update_financial_ratio($params); // UPDATE THE FINANCIAL RATIO
								
				$current_info 	= $this->financial_ratios->get_specific_financial_ratio(array("financial_ratio_id" => $financial_ratio_id)); // GET THE CURRENT DATA
				
				$curr_detail[]	= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				
				
				$items	= $params['items'];
				
				foreach($items as $item_id => $amount)
				{

					// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
					$prev_info 	= $this->financial_ratios->get_specific_financial_ratio_items($financial_ratio_id, $item_id); // GET THE CURRENT DATA
					
					$audit_action[]	= AUDIT_UPDATE;
					$audit_table[]	= MPIS_Model::tbl_financial_ratio_items;
					$audit_schema[]	= DB_MPIS;
					$prev_detail[]	= $prev_info;
					
					$params								= array();
					$params['financial_ratio_id']		= $financial_ratio_id;					
					$params['financial_ratio_item_id']	= $item_id;
					$params['amount']					= $amount;
					
					$this->financial_ratios->update_financial_ratio_item($params); // SAVES THE BUILDING CONSTRUCTION REGIONS RECORD
					
					$current_info 	= $this->financial_ratios->get_specific_financial_ratio_items($financial_ratio_id, $item_id);  // GET THE CURRENT DATA
					
					$curr_detail[]	= $current_info;
					// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
					
								
				}
				
				
				$activity	= "%s has been updated in financial ratio.";
				$activity 	= sprintf($activity, $activity_title);

				$msg  = $this->lang->line('data_updated');
			}

			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table, 
				$audit_schema
			);
			
			MPIS_Model::commit();

			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			MPIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
			
		}
		catch(Exception $e)
		{
			MPIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	
	}
	
	private function _check_financial_ratios($params)
	{
		
		try {
			
			$where = array();
			$where['financial_ratio_year']	= $params['year'];
			$where['company_type_id']		= $params['company_type'];
			$where['license_category_id']	= $params['category'];
			$where['company_name']			= $params['company_name'];
			
			$info 	= $this->financial_ratios->get_specific_financial_ratio($where);
			
			return $info['financial_ratio_id'];
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
		
	}
	
	private function _validate(&$params)
	{
		try
		{
			
			$this->_validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields 					= array();
			$fields['year']				= 'Year';
			$fields['company_type']		= 'Business Type';
			$fields['category']			= 'Category';
			$fields['company_name']		= 'Company Name';			

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _validate_input($params)
	{
		try
		{
			
			$validation['year'] = array(
				'data_type' => 'digit',
				'name'		=> 'Year'
			);
			
			$validation['company_type'] = array(
				'data_type' => 'digit',
				'name'		=> 'Business Type'				
			);
			
			$validation['category'] = array(
				'data_type' => 'digit',
				'name'		=> 'Category'
			);
			
			$validation['company_name'] = array(
				'data_type' => 'string',
				'name'		=> 'Company Name',
				'min_len'	=> '2',
				'max_len'	=> '255'					
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function delete_financial_ratio($params) 
	{
		try 
		{

			$flag				= 0;
			$msg				= "Error";
			
			if($this->permission_delete === FALSE)
				throw new Exception($this->lang->line('err_unauthorized_delete'));
			
			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->_validate_security($params);

			$key 			= $this->get_hash_key('financial_ratio_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];
			
			$info 			= $this->financial_ratios->get_specific_financial_ratio($where);
			
			if(EMPTY($info['financial_ratio_id']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			$year 		= $info['building_construction_year'];
			$quarter	= $info['building_construction_quarter'];
			
			$title		= $info['company_name'];
			
			MPIS_Model::beginTransaction();
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= MPIS_Model::tbl_financial_ratio_items;
			$audit_schema[]	= DB_MPIS;		
			
			$prev_info	= $this->financial_ratios->get_financial_ratio_items(array('financial_ratio_id' => $info['financial_ratio_id']));
			
			$prev_detail[]	= $prev_info;
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL

			$this->financial_ratios->delete_financial_ratio_items($info['financial_ratio_id']); // DELETE BUILDING CONSTRUCTION REGION

			$curr_detail[]	= array();
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= MPIS_Model::tbl_financial_ratios;
			$audit_schema[]	= DB_MPIS;
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			
			$this->financial_ratios->delete_financial_ratio($info['financial_ratio_id']); // DELETE BUILDING CONSTRUCTION
			
			$curr_detail[]	= array();
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			

			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $title);

			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			MPIS_Model::commit();

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			MPIS_Model::rollback();

			$msg	= $e->getMessage(); 	
		}
		catch(Exception $e)
		{	

			MPIS_Model::rollback();
						
			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
	}
}

/* End of file Ceis_positions.php */
/* Location: ./application/modules/ceis/controllers/Ceis_positions.php */