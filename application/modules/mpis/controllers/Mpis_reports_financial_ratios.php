<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_reports_financial_ratios extends MPIS_Controller {
	
	private $year_start			= 0;
	private $year_end			= 0;
	private $business_label		= '';
	private $category_label		= '';
	private $company_label		= '';
	
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('financial_ratios_model', 'financial_ratios');
		$this->load->model('params_model', 'params');
		

	}
		
	public function generate()
	{		
		
		try {
			
			$params				= get_params();
			
			
			$where = array();
			if($params['ratio'] != "all")
			{
				$where['ratio_id']			= array($params['ratio'], array("=", "OR")) ;
				$where['parent_ratio_id']	= array($params['ratio'], array("=")) ;
			}
				
			$this->ratios		= $this->params->get_params(MPIS_Model::tbl_param_financial_ratios, $where); 
				
			$this->year_start	= $params['year_start'];
			$this->year_end		= $params['year_end'];
			
			
			IF($params['business_type'] != "all")
			{
				$info 					= $this->params->get_params(MPIS_Model::tbl_param_company_types, array('company_type_id' => $params['business_type']));
				$this->business_label	= $info[0]['company_type_name'];
			}
			ELSE
			{
				$this->business_label	= "All";
			}
			
			IF($params['category'] != "all")
			{

				$info					= $this->params->get_params(MPIS_Model::tbl_param_license_categories, array('license_category_id' => $params['category']));
				$this->category_label	= $info[0]['license_category'];
			}
			ELSE
			{
				$this->category_label	= "All";
			}
			
			$this->company_label		= ($params['company'] != 'all') ? $params['company'] : 'All'; 	 
			
			$where = array();
			
			if($params['year_start'] == $params['year_end'])
				$where['B.financial_ratio_year']	= $params['year_end'];
			else
				$where['B.financial_ratio_year']	= array(array($params['year_start'],$params['year_end']), array("BETWEEN"));
			
			if($params['business_type'] != "all")
				$where['B.company_type_id']			= $params['building_type'];
			
			if($params['category'] != "all")
				$where['B.license_category_id']		= $params['building_type'];
			
			if($params['company'] != "all")
				$where['B.company_name']			= $params['company'];
			
						
			$result	= $this->financial_ratios->get_financial_ratios_reports($where);
			
			foreach($result as $result)
			{
				
				$year 								= $result['financial_ratio_year'];
				$inventories						= $result[RATIO_ITEM_INVENTORIES];
				$total_current_assets				= $result[RATIO_ITEM_TOTAL_CURRENT_ASSETS];
				$total_fixed_asssets				= $result[RATIO_ITEM_TOTAL_FIXED_ASSETS];
				$total_assets						= $result[RATIO_ITEM_TOTAL_ASSETS];
				$total_current_liabilities			= $result[RATIO_ITEM_TOTAL_CURRENT_LIABILITIES];
				$total_liabilities					= $result[RATIO_ITEM_TOTAL_LIABILITIES];
				$total_networth						= $result[RATIO_ITEM_TOTAL_NETWORTH];
				$construction_income				= $result[RATIO_ITEM_CONSTRUCTION_INCOME];
				$non_construction_income			= $result[RATIO_ITEM_NON_CONSTRUCTION_INCOME];
				$cost_construction_operation		= $result[RATIO_ITEM_COST_CONSTRUCTION_OPERATION];
				$cost_non_construction_operation	= $result[RATIO_ITEM_COST_NON_CONSTRUCTION_OPERATION];
				$total_cost_operations				= $result[RATIO_ITEM_TOTAL_COST_OPERATIONS];
				$total_revenues						= $result[RATIO_ITEM_TOTAL_REVENUES];
				$net_income_after_tax				= $result[RATIO_ITEM_NET_INCOME_AFTER_TAX];
				
				
				$info[RATIO_CURRENT_RATIO][$year]			= $total_current_assets / $total_current_liabilities;
				$info[RATIO_QUICK_RATIO][$year]				= ($total_current_assets - $inventories) / $total_current_liabilities;
				$info[RATIO_TOTAL_ASSETS_TURNOVER][$year]	= $total_revenues / $total_assets;
				$info[RATIO_FIXED_ASSETS_TURNOVER][$year]	= $total_revenues / $total_fixed_asssets;
				$info[RATIO_INVENTORY_TURNOVER][$year]		= $total_cost_operations / $inventories;
				$info[RATIO_DEBT_TOTAL_ASSETS][$year]		= $total_liabilities / $total_assets;
				$info[RATIO_DEBT_NETWORK][$year]			= $total_liabilities / $total_networth;
				$info[RATIO_PROFIT_MARGIN_1][$year]			= $construction_income / $cost_construction_operation;
				$info[RATIO_PROFIT_MARGIN_2][$year]			= $non_construction_income / $cost_non_construction_operation;
				$info[RATIO_PROFIT_MARGIN_3][$year]			= $total_revenues / $total_cost_operations;
				$info[RATIO_RETURN_TOTAL_ASSETS][$year]		= $net_income_after_tax / $total_assets;
				$info[RATIO_RETURN_NETWORTH][$year]			= $net_income_after_tax / $total_networth;
			}
			
			$filter	= $this->_construct_filter_label(); 
			$head	= $this->_construct_report_header();
			$body	= $this->_construct_report_body($info);
			
			
			$info = array(
				'filter'	=> $filter,
				'content'	=> $head . $body 
					
			);
			
			return $info;
		}
		catch (PDOException $e)
		{
			throw $e;
		}
		catch (Exception $e)
		{
			throw $e;
		}
					
	}
	
	private function _construct_filter_label()
	{
		
		$year 		= ($this->year_start != $this->year_end) ? $this->year_start . ' to ' . $this->year_end : $this->year_end;
						
		$filter = '<div>';
			$filter.= '<p>Year: '.$year.'</p>';		
			$filter.= '<p>Business Type: '.$this->business_label.'</p>';
			$filter.= '<p>Category: '.$this->category_label.'</p>';
			$filter.= '<p>Company: '.$this->company_label.'</p>';
			
		$filter.= '</div>';
		
		return $filter;
		
	}
	
	
	private function _construct_report_header()
	{
		

		$range		= ($this->year_end - $this->year_start) + 1;
		$rowspan	= ( $range > 1) ? ' rowspan="2" ' : "";
		$colspan	= ( $range > 1) ? ' colspan="'.$range.'" ' : "";
		
		$percent	= 5 * $range;
		
		$head = '<thead>';
			$head.= '<tr>';
				$head.= '<th width="25%" align="center" '.$rowspan.'>RATIO</th>';
				$head.= '<th width="35%" align="center" '.$rowspan.'>FORMULA</th>';
				$head.= '<th width="'.$percent.'%" align="center" '.$colspan.'>YEAR</th>';
				
				if($range > 1)
					$head.= '<th align="center" colspan="'.($range - 1).'">GROWTH RATE</th>';
			$head.= '</tr>';
			
			if($range > 1)
			{
				$head.= '<tr>';
					for($year = $this->year_start; $year <= $this->year_end; $year++)
						$head.= '<th align="center" >'.$year.'</th>';
					
					for($year = $this->year_start; $year < $this->year_end; $year++)
					{
						$label = $year . '-' . ($year + 1);
						$head.= '<th align="center" >'.$label.'</th>';
					}	
						
				$head.= '</tr>';
			}
			
			
		$head.= '</thead>';
	
		
		return $head;		
		
	}
	
	private function _construct_report_body($info)
	{
		
		$ratios		= $this->ratios;
		$range		= ($this->year_end - $this->year_start) + 1;
		

		$body = '<tbody>';
		
		foreach($ratios as $ratios)
		{
			$ratio_id	= $ratios['ratio_id'];
			$formula	= $ratios['formula_label'];
			$ratio		= EMPTY($ratios['parent_ratio_id']) ? '<B>' . strtoupper($ratios['ratio_name']) . '</B>':  $ratios['ratio_name'];
			
			$body.= '<tr>';
				$body.= '<td>'.$ratio.'</td>';
				$body.= '<td align="center">'.$formula.'</td>';
				
				for($year = $this->year_start; $year <= $this->year_end; $year++)
				{
					$amount	= !EMPTY($info[$ratio_id][$year]) ? $info[$ratio_id][$year] : 0;
						
					IF(EMPTY($ratios['parent_ratio_id']))
						$body.= '<td align="right">&nbsp;</td>';
					ELSE
						$body.= '<td align="right">'.number_format($amount, 2).'</td>';
				}
				
				if($range > 1)
				{
					for($year = $this->year_start; $year < $this->year_end; $year++)
					{
							
						$prev_value			= !EMPTY($info[$ratio_id][$year]) ? $info[$ratio_id][$year] : 0;
						$curr_value			= !EMPTY($info[$ratio_id][($year+1)]) ? $info[$ratio_id][($year+1)] : 0;
							
						$rate				= ($prev_value > 0) ? (($curr_value / $prev_value) -1 ) * 100 : 0;
					
						IF(EMPTY($ratios['parent_ratio_id']))
							$body.= '<td align="right">&nbsp;</td>';
						ELSE
							$body.= '<td align="right">'.number_format($rate, 2).'</td>';
					}	
				}
				
			$body.= '</tr>';
		}
		
		$body.= '</tbody>';
		
	
		return $body;
	}
}

/* End of file Mpis_reports.php */
/* Location: ./application/modules/mpis/controllers/mpis_reports.php */