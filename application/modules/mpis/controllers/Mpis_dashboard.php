<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_dashboard extends MPIS_Controller {

	private $module = MODULE_BAS_DASHBOARD;
	
	public function __construct()
	{
		parent::__construct();
	}
	
	public function index()
	{	
		$data 				= array();
		$resources 			= array();


		$this->template->load('dashboard', $data, $resources);		
	}
}

/* End of file mpis_dashboard.php */
/* Location: ./application/modules/mpis/controllers/mpis_dashboard.php */