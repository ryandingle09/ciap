<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_reports extends MPIS_Controller {
	
	private $module = MODULE_MPIS_REPORTS;
	
	
	public $permission_view		= FALSE;
	
	
	public function __construct()
	{
		parent::__construct();
		
		
		$this->load->model('params_model', 'params');
		$this->load->model('cias_params_model', 'cias_params');
		
		$this->permission			= $this->check_permission($this->module);		
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		

	}
	
	public function index()
	{	
		try{
			$data = $resources = $modal = array();
			
			
			
			if($this->permission)
			{
				$modal = array(
					'modal_report' => array(
						'controller'	=> __CLASS__,
						'module'		=> PROJECT_MPIS,
						'multiple'		=> TRUE,
						'height'		=> '450px;',
						'ajax'			=> TRUE,
						'size'			=> 'XL'
					),
						
					'modal_chart' => array(
						'module'		=> PROJECT_MPIS,
						'controller'	=> __CLASS__,
						'method'		=> 'modal_chart',							
						'multiple'		=> TRUE,							
						'size'			=> 'XL'
					)
				);
				
				
				$resources['load_js'] 		= array(JS_DATETIMEPICKER, 'chart.min', 'modules/mpis/mpis_reports');
				$resources['load_css'] 		= array(CSS_DATETIMEPICKER);
				$resources['load_modal'] 	= $modal;

				
				$data['reports']	= $this->cias_params->get_params(SYSAD_Model::tbl_param_resource, array('parent_resource_code' => MODULE_MPIS_REPORTS), "*", array("sort_no" => "ASC"));
				
				$data['year_max']	= date('Y');
				$data['year_min']	= MPIS_START_YEAR;
				
				$view_page	= "reports";
			}
			else
			{
				$view_page	= "errors/html/error_permission";
			}

			$this->template->load($view_page, $data, $resources);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	

	/*
	 * GET PARAMETERS
	 */
	
	public function get_building_types()
	{
		try {
			$params = get_params();
			
			$info	= $this->params->get_params(MPIS_Model::tbl_param_building_construction_sources, array('building_construction_type' => $params['type']));
			
			IF(!EMPTY($info))
			{
				$option = "<option value='all'>All</option>";
				
				foreach($info as $info)
				{
					$option.= "<option value='".$info['source_id']."'>".$info['source_name']."</option>";
				}
			}
			ELSE
			{
				$option = "<option value=''>No record/s found.</option>";
			}
			
		}
		catch(PDOException $e)
		{		
			$option = "<option value=''>".$e->getMessage()."</option>";
		}
		catch(Exception $e)
		{
			$option = "<option value=''>".$e->getMessage()."</option>";
		}
		
		echo $option;
	}
	
	public function get_business_types()
	{
		try {
			$params = get_params();
				
			$info	= $this->params->get_params(MPIS_Model::tbl_param_company_types);
				
			IF(!EMPTY($info))
			{
				$option = "<option value='all'>All</option>";
	
				foreach($info as $info)
				{
					$option.= "<option value='".$info['company_type_id']."'>".$info['company_type_name']."</option>";
				}
			}
			ELSE
			{
				$option = "<option value=''>No record/s found.</option>";
			}
				
		}
		catch(PDOException $e)
		{
			$option = "<option value=''>".$e->getMessage()."</option>";
		}
		catch(Exception $e)
		{
			$option = "<option value=''>".$e->getMessage()."</option>";
		}
	
		echo $option;
	}
	
	public function get_companies()
	{
		try {
			$params = get_params();
				
			$info	= $this->params->get_params(MPIS_Model::tbl_financial_ratios, NULL, array('DISTINCT company_name'));
				
			IF(!EMPTY($info))
			{
				$option = "<option value='all'>All</option>";
	
				foreach($info as $info)
				{
					$option.= "<option value='".$info['company_name']."'>".$info['company_name']."</option>";
				}
			}
			ELSE
			{
				$option = "<option value=''>No record/s found.</option>";
			}
				
		}
		catch(PDOException $e)
		{
			$option = "<option value=''>".$e->getMessage()."</option>";
		}
		catch(Exception $e)
		{
			$option = "<option value=''>".$e->getMessage()."</option>";
		}
	
		echo $option;
	}
	
	public function get_license_categories()
	{
		try {
			$params = get_params();
				
			$info	= $this->params->get_params(MPIS_Model::tbl_param_license_categories);
				
			IF(!EMPTY($info))
			{
				$option = "<option value='all'>All</option>";
	
				foreach($info as $info)
				{
					$option.= "<option value='".$info['license_category_id']."'>".$info['license_category']."</option>";
				}
			}
			ELSE
			{
				$option = "<option value=''>No record/s found.</option>";
			}
				
		}
		catch(PDOException $e)
		{
			$option = "<option value=''>".$e->getMessage()."</option>";
		}
		catch(Exception $e)
		{
			$option = "<option value=''>".$e->getMessage()."</option>";
		}
	
		echo $option;
	}
	

	public function get_ratios()
	{
		try {
			$params = get_params();
	
			$info	= $this->params->get_params(MPIS_Model::tbl_param_financial_ratios, array('parent_ratio_id' => "IS NULL"));
	
			IF(!EMPTY($info))
			{
				$option = "<option value='all'>All</option>";
	
				foreach($info as $info)
				{
					$option.= "<option value='".$info['ratio_id']."'>".$info['ratio_name']."</option>";
				}
			}
			ELSE
			{
				$option = "<option value=''>No record/s found.</option>";
			}
	
		}
		catch(PDOException $e)
		{
			$option = "<option value=''>".$e->getMessage()."</option>";
		}
		catch(Exception $e)
		{
			$option = "<option value=''>".$e->getMessage()."</option>";
		}
	
		echo $option;
	}
		
	
	public function get_indicator_categories()
	{
		try {
			$params = get_params();
	
			$info	= $this->params->get_params(MPIS_Model::tbl_param_indicator_categories, array('parent_id' => "IS NULL"));
	
			IF(!EMPTY($info))
			{
				$option = "<option value='all'>All</option>";
	
				foreach($info as $info)
				{
					$option.= "<option value='".$info['indicator_category_id']."'>".$info['indicator_category_name']."</option>";
				}
			}
			ELSE
			{
				$option = "<option value=''>No record/s found.</option>";
			}
	
		}
		catch(PDOException $e)
		{
			$option = "<option value=''>".$e->getMessage()."</option>";
		}
		catch(Exception $e)
		{
			$option = "<option value=''>".$e->getMessage()."</option>";
		}
	
		echo $option;
	}
	
	public function get_indicators()
	{
		try {
			$params = get_params();
	
			$info	= $this->params->get_params(MPIS_Model::tbl_param_construction_indicators);
	
			IF(!EMPTY($info))
			{
				$option = "<option value=''>Select indicator</option>";
	
				foreach($info as $info)
				{
					$option.= "<option value='".$info['construction_indicator_id']."'>".$info['construction_indicator']."</option>";
				}
			}
			ELSE
			{
				$option = "<option value=''>No record/s found.</option>";
			}
	
		}
		catch(PDOException $e)
		{
			$option = "<option value=''>".$e->getMessage()."</option>";
		}
		catch(Exception $e)
		{
			$option = "<option value=''>".$e->getMessage()."</option>";
		}
	
		echo $option;
	}
		
	
	
	
	public function generate_report()
	{		
		
		try {
			
			$success		= 0;
			$msg			= '';
			$report			= "";
			$report_title	= "";
			$report_data	= array();
			
			$params			= get_params();
			
			$generate		= strtoupper($params['generate']);

			$report_page	= 'report';
						
			switch($params['report'])
			{
				case "MPIS-REPORTS-R1":	
					
					$report			= 'report_building_construction';
					$report_title	= 'Residential Building Construction';
					
					$this->load->library('../controllers/mpis_reports_bldg_cons');
					$report_data['report'] = $this->mpis_reports_bldg_cons->generate(BLDG_CONS_RES);
														
					$success	= 1;
				break;
				
				case "MPIS-REPORTS-R2":
						
					$report			= 'report_building_construction';
					$report_title	= 'Non Residential Building Construction';
						
					$this->load->library('../controllers/mpis_reports_bldg_cons');
					$report_data['report'] = $this->mpis_reports_bldg_cons->generate(BLDG_CONS_NON_RES);
				
					$success	= 1;
				break;
				
				case "MPIS-REPORTS-R3":	
				
					$report			= 'report_building_construction';
					$report_title	= 'Summary of Number, Floor Area and Value of Buidling Construction';
				
					$this->load->library('../controllers/mpis_reports_bldg_cons');
					$report_data['report'] = $this->mpis_reports_bldg_cons->generate_summary();
				
					$success	= 1;
				break;
				
				case "MPIS-REPORTS-R4":
				
					$report			= 'financial_ratios';
					$report_title	= 'Financial Ratios';
				
					$this->load->library('../controllers/mpis_reports_financial_ratios');
					$report_data['report'] = $this->mpis_reports_financial_ratios->generate();
				
					$success	= 1;
				break;
				
				case "MPIS-REPORTS-R5":	
					$report			= 'construction_employment';
					$report_title	= 'Construction Employment';
				
					$this->load->library('../controllers/mpis_reports_construction_employment');
					$report_data['report'] = $this->mpis_reports_construction_employment->generate();
				
					$success	= 1;
				break;
				
				case "MPIS-REPORTS-R6":	
				
					$report			= 'market_values';
					$report_title	= $generate == 'CHART'  ? '% Share of Capital Investment' : 'Gross National Income and Gross Domestic Product by Expenditure Share';
					
					
				
					$this->load->library('../controllers/mpis_reports_market_values');
					$report_data['report'] = $this->mpis_reports_market_values->generate(MARKET_TYPE_EXPENDITURE);
										
				
					$success	= 1;
				break;
				
				case "MPIS-REPORTS-R7":	
				
					$report			= 'market_values';
					$report_title	= $generate == 'CHART'  ? '% Share to GDP' : 'Gross National Income and Gross Domestic Product by Industrial Origin';
				
					$this->load->library('../controllers/mpis_reports_market_values');
					$report_data['report'] = $this->mpis_reports_market_values->generate(MARKET_TYPE_INDUSTRIAL);
				
					$success	= 1;
				break;
				
				case "MPIS-REPORTS-R8":	
				
					$report			= 'market_values';
					$report_title	= $generate == 'CHART'  ? 'Public & Private Construction Demand' : 'Gross Value and Gross Value Adde in Construction';
				
					$this->load->library('../controllers/mpis_reports_market_values');
					$report_data['report'] = $this->mpis_reports_market_values->generate(MARKET_TYPE_CONSTRUCTION);
				
					$success	= 1;
				break;
				
				case "MPIS-REPORTS-R9":
				
					$report			= 'news_articles';
					$report_title	= 'News Articles';
				
					$this->load->library('../controllers/mpis_reports_indicators');
					$report_data['report'] = $this->mpis_reports_indicators->generate();
				
					$success	= 1;
				break;
				
				case "MPIS-REPORTS-R10":
				
					$report			= 'cons_countries';
					$report_title	= 'Construction Countries';
				
					$this->load->library('../controllers/mpis_reports_cons_countries');
					$report_data['report'] = $this->mpis_reports_cons_countries->generate();
				
					$success	= 1;
				break;
				
				case "MPIS-REPORTS-R11":
				
					$report			= 'cons_related_prof';
					$report_title	= 'Number of Construction - Related Licensed Professionals';
				
					$this->load->library('../controllers/mpis_reports_cons_related_prof');
					$report_data['report'] = $this->mpis_reports_cons_related_prof->generate();
				
					$success	= 1;
				break;
				
				case "MPIS-REPORTS-R12":
				
					$report			= 'cost_cons_index';
					$report_title	= 'Cost of Construction Index';
				
					$this->load->library('../controllers/mpis_reports_cost_cons_index');
					$report_data['report'] = $this->mpis_reports_cost_cons_index->generate();
				
					$success	= 1;
				break;
			}
			
			
			$report_page				= (strtoupper($params['generate']) == "CHART") ? 'report_chart' : 'report';

			$filename 					= $report . '_' . time();
			$report_data['title']		= $report_title;
			$report_data['generate']	= $params['generate'];

			
			$html						= $this->load->view('reports/' . $report_page, $report_data, TRUE);
			
			switch($generate)
			{
				
				case 'PDF':
					$modal_data					= array();
					$modal_data['generate']		= $params['generate'];
					$modal_data['pdf'] 			= $this->pdf( $filename . '.pdf', $html, FALSE, 'S', $header=NULL, $this->set_report_footer() );
					$modal						= $this->load->view('modals/report_container_modal', $modal_data, TRUE);
						
						
					$info 	= array(
							'success'	=> $success,
							'msg'		=> $msg,
							'modal' 	=> $modal
					);
						
					echo  json_encode($info);
				break;
				
				case 'CHART':
					$modal_data					= array();
					$modal_data['generate']		= $params['generate'];
					$modal_data['chart'] 		= $html;
					$modal						= $this->load->view('modals/report_container_modal', $modal_data, TRUE);
			
			
					$info 	= array(
							'success'	=> $success,
							'msg'		=> $msg,
							'modal' 	=> $modal
					);
			
					echo  json_encode($info);
				break;
						
				case 'EXCEL':
					
					$this->convert_excel( $html, $filename , $report_title);
				break;
			}			
			
		}
		catch(PDOException $e)
		{
			$msg = $e->getMessage();
		}
		catch(Exception $e)
		{
			$msg = $e->getMessage();			
		}
		
	
	}
	
	public function modal_chart()
	{
		$resources['load_js'] 		= array('chart.min', 'modules/mpis/mpis_reports');
		$resources['load_init']		= array('ReportChart.init();');
		
		$this->load->view('modals/chart_container_modal');
		$this->load_resources->get_resource($resources);
	}
}

/* End of file Mpis_reports.php */
/* Location: ./application/modules/mpis/controllers/mpis_reports.php */