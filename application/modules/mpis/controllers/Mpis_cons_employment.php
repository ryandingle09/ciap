<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_cons_employment extends MPIS_Controller {
	
	private $module 		= MODULE_MPIS_CONS_EMPLOYMENT;
	
	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('cons_employment_model', 'cons_employment');
		$this->load->model('params_model', 'params');
		
		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);

	}
	
	public function index()
	{	
		try{
			$data = $resources = $modal = array();
			
			if($this->permission)
			{
				$modal = array(
					'modal_cons_employment' => array(
						'module'		=> PROJECT_MPIS,
						'controller'	=> __CLASS__,
						'method'		=> 'modal_cons_employment',
						'title'			=> 'Construction Employment',
						'size'			=> 'lg',
						'multiple'		=> TRUE
					)
				);
				
				$resources['load_css'] 		= array(CSS_DATATABLE);
				$resources['load_js'] 		= array(JS_DATATABLE, JS_EDITOR);
				$resources['load_modal'] 	= $modal;
				$resources['datatable']		= array();
				$resources['datatable'][] 	= array(
					'table_id' 	=> 'cons_employment_table', 
					'path' 		=> PROJECT_MPIS.'/mpis_cons_employment/get_cons_employment_list'				
				);
				
				
				// STORE THE PERMISSION
				$data['permission_add']	= $this->permission_add; 	
				
				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash(0);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);
				$data['url']	= $encoded_id."/".$salt."/".$token;
				
				$view_page	= "cons_employment";
			}
			else
			{
				$view_page	= "errors/html/error_permission";
			}
		
			$data['controller']	= __CLASS__;
			

			$this->template->load($view_page, $data, $resources);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
	
	public function get_cons_employment_list()
	{
		try{

			$params = get_params(); // DO NOT USE: $_POST $this->input->post
			
			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> 0,
				"iTotalDisplayRecords" 	=> 0,
				"aaData" 				=> array()
			);
			
			if(!$this->permission)
				throw new Exception($this->lang->line('err_unauthorized_view'));
		
			// FIELDS TO BE SELECTED FROM TABLE
			$select_fields = array(
				"A.construction_employment_id",
				"A.year",					
				"A.quarter"						
			);

			// APPEARS ON TABLE
			$where_fields = array(
				"A.year",
				"A.quarter"
			);
		
			$results 		= $this->cons_employment->get_cons_employment_list($select_fields, $where_fields, $params);
			$total	 		= $this->cons_employment->total_length(); // TOTAL COUNT OF RECORDS
			$filtered_total = $this->cons_employment->filtered_length($select_fields, $where_fields, $params); // TOTAL COUNT OF RECORDS PER PAGE
		
			foreach ($results as $data):

				// PRIMARY KEY
				$primary_key	= $data["construction_employment_id"];

				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash($primary_key);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("construction employment", "'.$url.'")';

				// START: CONSTRUCT ACTION ICONS
				$action = "<div class='table-actions'>";
				
					if($this->permission_edit) :
						$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_cons_employment' onclick=\"modal_cons_employment_init('".$url."')\"></a>";
					endif;
					
					if($this->permission_delete) : 	  
						$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					endif;
				
				$action .= '</div>';
				// END: CONSTRUCT ACTION ICONS				

				$row 	= array();
				$row[] 	= $data['year'];
				$row[] 	= $data['quarter'];
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
				
			endforeach;
			
			$output["iTotalRecords"] 		= $total["cnt"];
			$output["iTotalDisplayRecords"] = $filtered_total["cnt"];
		}
		catch(PDOException $e){
			$this->rlog_error($e);
		}
		catch(Exception $e){
			$this->rlog_error($e);
		}
		
		echo json_encode( $output );
	}
	
	public function modal_cons_employment($encoded_id, $salt, $token)
	{
		try{
			$msg	= "";
			$info	= array();
			$data 	= array();
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);
			
			$hash_id = base64_url_decode($encoded_id);
			
			$data['permission_save']	= $this->permission_add;
			
			if($this->hash(0) != $hash_id){
				
				$key 			= $this->get_hash_key('construction_employment_id');
				$where			= array();
				$where[$key]	= $hash_id;
				
				$info = $this->cons_employment->get_specific_cons_employment($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));
				

				$data['groups']				= $this->cons_employment->get_specific_cons_employment_groups($info['construction_employment_id']);
				
				$data['permission_save']	= $this->permission_edit;
			}
			else
			{
				$data['groups']				= $this->params->get_params(MPIS_Model::tbl_param_major_industry_groups);
			}

			$data['info']				= $info; // CONSTRUCTION EMPLOYMENT					
						
			$resources					= array();
			$resources['load_css'] 		= array(CSS_DATATABLE,CSS_DATETIMEPICKER, CSS_LABELAUTY, CSS_SELECTIZE,CSS_MODAL_COMPONENT);
			$resources['load_js'] 		= array(JS_DATATABLE, JS_DATETIMEPICKER, JS_LABELAUTY,JS_MODAL_CLASSIE, JS_SELECTIZE, JS_EDITOR);
			
			// SET PARAMS			
			$data['year_start']			= date('Y');
			$data['year_end']			= date('Y') - 5;						
			$data['quarters']	= array( 1 => '1st', 2 => '2nd', 3 => '3rd', 4 => '4th');
			
			// REGENERATE SECURITY VARIABLES
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			$this->load->view('modals/cons_employment',$data);
			$this->load_resources->get_resource($resources);			
		}
		catch(PDOException $e){
			$msg = $e->getMessage();
		}
		catch(Exception $e){
			$msg = $e->getMessage();
		}
		

	}
	
	public function process()
	{
		try
		{
			$flag 			= 0;
			$params 		= get_params();
			
									
			// SERVER VALIDATION
			$this->_validate($params);

	
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('construction_employment_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 				= $this->cons_employment->get_specific_cons_employment($where);
			$cons_employment_id	= $info['construction_employment_id'];

			MPIS_Model::beginTransaction();

			$activity_title	= $params['year'] . ' ' . $params['quarter'] ;
			
			
			// IF EMPTY MEANS INSERT/SAVE RECORD
			IF(EMPTY($cons_employment_id)){
				
				
				// CHECK IF THE RECORD IS ALREADY EXIST
				$exist = $this->_check_cons_employment($params);
				
				IF($exist)	
					throw new Exception('This record is already added.');
				
				
				if($this->permission_add === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_add'));
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= MPIS_Model::tbl_construction_employments;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array();
				
				$cons_employment_id = $this->cons_employment->insert_cons_employment($params); // SAVES THE FINANCIAL RATIO
				
				// GET THE CURRENT DATA				
				$current_info 	= $this->cons_employment->get_specific_cons_employment(array("construction_employment_id" => $cons_employment_id));
				
				$curr_detail[]	= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				
				$audit_action[]	= AUDIT_INSERT;
				$audit_table[]	= MPIS_Model::tbl_construction_employment_groups;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array();
				
				$params['cons_employment_id']	= $cons_employment_id;
				$this->cons_employment->insert_cons_employment_groups($params); // SAVES THE BUILDING CONSTRUCTION REGIONS RECORD
				
				$current_info 	= $this->cons_employment->get_cons_employment_groups(array("construction_employment_id" => $cons_employment_id)); // GET THE CURRENT DATA
				
				$curr_detail[]	= $current_info;
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				

				$activity	= "%s has been added in financial ratios.";
				$activity 	= sprintf($activity, $activity_title);

				$msg  = $this->lang->line('data_saved');
			}

			else{

				if($this->permission_edit === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_edit'));
				
				$params['construction_employment_id']	= $cons_employment_id;
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
				
				$prev_info 	= $this->cons_employment->get_specific_cons_employment(array("construction_employment_id" => $cons_employment_id)); // GET THE CURRENT DATA
				
				$activity_title	= $prev_info['year'] . ' ' . $prev_info['quarter'];

				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= MPIS_Model::tbl_construction_employments;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array($prev_info);
				
				$this->cons_employment->update_cons_employment($params); // UPDATE THE FINANCIAL RATIO
								
				$current_info 	= $this->cons_employment->get_specific_cons_employment(array("construction_employment_id" => $cons_employment_id)); // GET THE CURRENT DATA
				
				$curr_detail[]	= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				
				
				$groups	= $params['groups'];
				
				foreach($groups as $group_id => $employed_persons)
				{

					// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
					$prev_info 	= $this->cons_employment->get_specific_cons_employment_groups($cons_employment_id, $group_id); // GET THE CURRENT DATA
					
					$audit_action[]	= AUDIT_UPDATE;
					$audit_table[]	= MPIS_Model::tbl_construction_employment_groups;
					$audit_schema[]	= DB_MPIS;
					$prev_detail[]	= $prev_info;
					
					$params							= array();
					$params['cons_employment_id']	= $cons_employment_id;					
					$params['industry_group_id']	= $group_id;
					$params['employed_persons']		= $employed_persons;
					
					$this->cons_employment->update_cons_employment_group($params); // SAVES THE BUILDING CONSTRUCTION REGIONS RECORD
					
					$current_info 	= $this->cons_employment->get_specific_cons_employment_groups($cons_employment_id, $group_id);  // GET THE CURRENT DATA
					
					$curr_detail[]	= $current_info;
					// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
					
								
				}
				
				
				$activity	= "%s has been updated in construction employment.";
				$activity 	= sprintf($activity, $activity_title);

				$msg  = $this->lang->line('data_updated');
			}


			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table, 
				$audit_schema
			);
	
			
			MPIS_Model::commit();

			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			MPIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
			
		}
		catch(Exception $e)
		{
			MPIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	
	}
	
	private function _check_cons_employment($params)
	{
		
		try {
			
			$where 				= array();
			$where['year']		= $params['year'];
			$where['quarter']	= $params['quarter'];
						
			
			$info 	= $this->cons_employment->get_specific_cons_employment($where);
			
			return $info['construction_employment_id'];
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
		
	}
	
	private function _validate(&$params)
	{
		try
		{
			
			$this->_validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields 			= array();
			$fields['year']		= 'Year';
			$fields['quarter']	= 'Quarter';

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _validate_input($params)
	{
		try
		{

			$validation['year'] = array(
				'data_type' => 'digit',
				'name'		=> 'Year'				
			);
			
			$validation['quarter'] = array(
				'data_type' => 'digit',
				'name'		=> 'Quarter'
			);
			
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function delete_cons_employment($params) 
	{
		try 
		{

			$flag				= 0;
			$msg				= "Error";
			
			if($this->permission_delete === FALSE)
				throw new Exception($this->lang->line('err_unauthorized_delete'));
			
			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->_validate_security($params);

			$key 			= $this->get_hash_key('construction_employment_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];
			
			$info 			= $this->cons_employment->get_specific_cons_employment($where);
			
			if(EMPTY($info['construction_employment_id']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			$year 		= $info['year'];
			$quarter	= $info['quarter'];
			
			$title		= $year . ' ' . $quarter;
			
			MPIS_Model::beginTransaction();
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= MPIS_Model::tbl_construction_employment_groups;
			$audit_schema[]	= DB_MPIS;		
			
			$prev_info	= $this->cons_employment->get_cons_employment_groups(array('construction_employment_id' => $info['construction_employment_id']));
			
			$prev_detail[]	= $prev_info;
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL

			$this->cons_employment->delete_cons_employment_groups($info['construction_employment_id']); // DELETE BUILDING CONSTRUCTION REGION

			$curr_detail[]	= array();
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= MPIS_Model::tbl_construction_employments;
			$audit_schema[]	= DB_MPIS;
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			
			$this->cons_employment->delete_cons_employment($info['construction_employment_id']); // DELETE BUILDING CONSTRUCTION
			
			$curr_detail[]	= array();
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			

			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $title);

			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			MPIS_Model::commit();

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			MPIS_Model::rollback();

			$msg	= $e->getMessage(); 	
		}
		catch(Exception $e)
		{	

			MPIS_Model::rollback();
						
			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
	}
}

/* End of file Ceis_positions.php */
/* Location: ./application/modules/ceis/controllers/Ceis_positions.php */