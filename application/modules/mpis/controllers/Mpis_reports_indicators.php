<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_reports_indicators extends MPIS_Controller {
	
	private $date_start			= 0;
	private $date_end			= 0;
	private $parent_categories	= NULL;
	private $child_categories	= NULL;
	private $category_label		= "";
	private $total_count		= "";
	
	
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('indicators_model', 'indicators');
		$this->load->model('params_model', 'params');
		

	}
		
	public function generate()
	{		
		
		try {
			
			$params				= get_params();
						
			$this->date_start	= $params['date_start'];
			$this->date_end		= $params['date_end'];
			
			$this->_construct_categories();			
			
			$where	= " WHERE 1 = 1 ";
			$val	= array();
			
			if($params['date_start'] == $params['date_end'])
			{
				$where.= " AND A.indicator_date = ? ";
				$val[] = date('Y-m-d', strtotime($params['date_end']));
			}
			else
			{
				$where.= " AND A.indicator_date BETWEEN ? AND ? ";
				
				$val[] = date('Y-m-d', strtotime($params['date_start']));
				$val[] = date('Y-m-d', strtotime($params['date_end']));
			}
				
			
			if($params['indicator_category'] != "all"){
				$where.= " AND ( A.indicator_category_id = ? OR A.indicator_category_id IN ( SELECT indicator_category_id FROM param_indicator_categories WHERE parent_id = ? )  )";
				$val[] = $params['indicator_category'];
				$val[] = $params['indicator_category'];
				
				$category				= $this->params->get_params(MPIS_Model::tbl_param_indicator_categories, array('indicator_category_id' => $params['indicator_category']));
				$this->category_label	= $category[0]['indicator_category_name'];
			}
			else
			{
				$this->category_label	= "All";
			}
											
			$info	= $this->indicators->get_indicators_reports($where, $val);
			
			$filter	= $this->_construct_filter_label(); 
			$head	= $this->_construct_report_header();
			$body	= $this->_construct_report_body($info);
			$footer	= $this->_construct_report_footer();
			
			
			$info = array(
				'filter'	=> $filter,
				'content'	=> $head . $body . $footer 
					
			);
			
			return $info;
		}
		catch (PDOException $e)
		{
			throw $e;
		}
		catch (Exception $e)
		{
			throw $e;
		}
					
	}
	
	private function _construct_categories()
	{
		
		$this->parent_categories = $this->params->get_params(MPIS_Model::tbl_param_indicator_categories, array('parent_id' => 'IS NULL'));
		
		$result = $this->params->get_params(MPIS_Model::tbl_param_indicator_categories, array('parent_id' => 'IS NOT NULL'));		
		
		foreach($result as $result)
		{
			$categories[$result['parent_id']][$result['indicator_category_id']] = $result;	
		}
		
		$this->child_categories = $categories;
		
		
	}
	
	
	private function _construct_filter_label()
	{
		
		$date 		= ($this->date_start != $this->date_end) ? $this->date_start . ' to ' . $this->date_end : $this->date_end;
						
		$filter = '<div>';
			$filter.= '<p>Period: '.$date.'</p>';		
			$filter.= '<p>Category: '.$this->category_label.'</p>';
			
		$filter.= '</div>';
		
		return $filter;
		
	}
	
	
	private function _construct_report_header()
	{

		
		$head = '
			<thead>
				<tr>
					<th align="center" width="25%">Name of Article</th>
					<th align="center" width="15%">Name of Newspaper</th>
					<th align="center" width="10%">Date</th>
					<th align="center" width="10%">Page</th>
					<th align="center" width="15%">Category</th>
					<th align="center" width="25%">Abstract</th>
				</tr>
			</thead>
		';
		
		
		return $head;		
		
	}
	
	private function _construct_report_body($info)
	{
		$body = '<tbody>';
		
			$total	= array();
			
			foreach($info as $info)
			{
				$category	= $info['indicator_category_id'];
				$parent		= $info['parent_id'];
				
				$total[$category]	= ISSET($total[$category]) ? $total[$category] : 0;
				
				$total[$category]	+= 1;
				
				IF($parent)
				{
					$total[$parent]	= ISSET($total[$parent]) ? $total[$parent] : 0;
					$total[$parent]	+= 1;
				}
				
				$body.= '
					<tr>
						<td valign="top">'.$info['indicator_title'].'</td>
						<td valign="top">'.$info['indicator_source_name'].'</td>
						<td valign="top" align="center">'.date('d-F-Y', strtotime($info['indicator_date'])).'</td>
						<td valign="top" align="center">'.$info['indicator_page'].'</td>
						<td valign="top">'.$info['indicator_category_name'].'</td>
						<td valign="top">'.$info['indicator_abstract'].'</td>
					</tr>	
				';
			}
			
		$body.= '</tbody>';
		
		$this->total_count	= $total;
		
		return $body;
	}

	private function _construct_report_footer()
	{
		
		$parent_categories 	= $this->parent_categories;
		$child_categories	= $this->child_categories;
		$total_count		= $this->total_count;
		
		$footer = '</table> <BR> <table width="50%" border="1" cellpadding="3">';
			$footer.= '<tr>';
				$footer.= '<td width="40%" align="center"><B>SUMMARY</B></td>';
				$footer.= '<td width="10%"  align="center"><B>TOTAL</B></td>';
			$footer.= '</tr>';
			
			$grand_total	= 0;
			foreach($parent_categories as $parent_categories){
				
				$category_id 	= $parent_categories['indicator_category_id'];
				$count			= !EMPTY($total_count[$category_id]) ? $total_count[$category_id] : 0;
				$grand_total    += $count;
						
				$footer.= '<tr>';
					$footer.= '<td>'.$parent_categories['indicator_category_name'].'</td>';
					$footer.= '<td align="right">'.$count.'</td>';
				$footer.= '</tr>';
				
				IF(!EMPTY($child_categories[$category_id]))
				{
					$child_category = $child_categories[$category_id];
					
					foreach($child_category as $child)
					{
						$category_id 	= $child['indicator_category_id'];
						$count			= !EMPTY($total_count[$category_id]) ? $total_count[$category_id] : 0;
						
						$footer.= '<tr>';
							$footer.= '<td style="padding-left: 20px;">'.$child['indicator_category_name'].'</td>';
							$footer.= '<td align="right">'.$count.'</td>';
						$footer.= '</tr>';
					}
				}
			}
			

			
			$footer.= '<tr>';
				$footer.= '<td><b>GRAND TOTAL</b></td>';
				$footer.= '<td align="right"><b>'.$grand_total.'</b></td>';
			$footer.= '</tr>';
			
			
		
		
		return $footer;
		
	}
}

/* End of file Mpis_reports.php */
/* Location: ./application/modules/mpis/controllers/mpis_reports.php */