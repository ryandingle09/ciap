<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed'); 

class Mpis_cons_related_prof extends MPIS_Controller {
	
	private $module = MODULE_MPIS_CONS_RELATED_PROF;
	
	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('cons_professionals_model', 'cons_professionals');
		$this->load->model('params_model', 'params');
		
		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);

	}
	
	public function index()
	{	
		try{
			$data = $resources = $modal = array();
			
			
			
			if($this->permission)
			{
				$modal = array(
					'modal_cons_professionals' => array(
						'module'		=> PROJECT_MPIS,
						'controller'	=> __CLASS__,
						'method'		=> 'modal_cons_professionals',
						'title'			=> 'Construction Related License Professionals',					
						'multiple'		=> TRUE
					)
				);
				
				$resources['load_css'] 		= array(CSS_DATATABLE);
				$resources['load_js'] 		= array(JS_DATATABLE, JS_EDITOR);
				$resources['load_modal'] 	= $modal;
				$resources['datatable']		= array();
				$resources['datatable'][] 	= array(
					'table_id' 	=> 'cons_professionals_table', 
					'path' 		=> PROJECT_MPIS.'/mpis_cons_related_prof/get_cons_professionals_list'				
				);
				
				
				// STORE THE PERMISSION
				$data['permission_add']	= $this->permission_add; 	
				
				// CONSTRUCT SECURITY VARIABLES
				$year_hash_id 		= $this->hash(0);
				$year_encoded_id 	= base64_url_encode($year_hash_id);
				
				
				$prof_hash_id 		= $this->hash(0);
				$prof_encoded_id	= base64_url_encode($prof_hash_id);
				
				$encoded_id			= $year_encoded_id . '/' . $prof_encoded_id; 				
				$salt 				= gen_salt();			
				$token 				= in_salt($encoded_id, $salt);			
								
				$data['url']		= $encoded_id."/".$salt."/".$token;
				
				$view_page	= "cons_professionals";
			}
			else
			{
				$view_page	= "errors/html/error_permission";
			}

			$this->template->load($view_page, $data, $resources);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
	
	public function get_cons_professionals_list()
	{
		try{

			$params = get_params(); // DO NOT USE: $_POST $this->input->post
			
			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> 0,
				"iTotalDisplayRecords" 	=> 0,
				"aaData" 				=> array()
			);
			
			if(!$this->permission)
				throw new Exception($this->lang->line('err_unauthorized_view'));
		
			// FIELDS TO BE SELECTED FROM TABLE
			$select_fields = array(
				"A.year",				
				"A.profession_id",
				"B.profession",
				"A.number"					
			);

			// APPEARS ON TABLE
			$where_fields = array(
				"A.year",				
				"B.profession",
				"A.number"
			);
		
			$results 		= $this->cons_professionals->get_cons_professionals_list($select_fields, $where_fields, $params);
			$total	 		= $this->cons_professionals->total_length(); // TOTAL COUNT OF RECORDS
			$filtered_total = $this->cons_professionals->filtered_length($select_fields, $where_fields, $params); // TOTAL COUNT OF RECORDS PER PAGE
		
			foreach ($results as $data):

				// PRIMARY KEY
				$year			= $data["year"];
				$profession_id	= $data["profession_id"];

				// CONSTRUCT SECURITY VARIABLES
				$year_hash 		= $this->hash($year);
				$year_encoded 	= base64_url_encode($year_hash);
				
				$prof_hash 		= $this->hash($profession_id);
				$prof_encoded 	= base64_url_encode($prof_hash);
				
				$encoded_id		= $year_encoded . '/' . $prof_encoded; 				
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("construction related licensed professionals", "'.$url.'")';

				// START: CONSTRUCT ACTION ICONS
				$action = "<div class='table-actions'>";
				
					if($this->permission_edit) :
						$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_cons_professionals' onclick=\"modal_cons_professionals_init('".$url."')\"></a>";
					endif;
					
					if($this->permission_delete) : 	  
						$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					endif;
				
				$action .= '</div>';
				// END: CONSTRUCT ACTION ICONS				

				$row 	= array();
				$row[] 	= $data['year'];
				$row[] 	= $data['profession'];
				$row[] 	= number_format($data['number'],0);
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
				
			endforeach;
			
			$output["iTotalRecords"] 		= $total["cnt"];
			$output["iTotalDisplayRecords"] = $filtered_total["cnt"];
		}
		catch(PDOException $e){
			$this->rlog_error($e);
		}
		catch(Exception $e){
			$this->rlog_error($e);
		}
		
		echo json_encode( $output );
	}
	
	public function modal_cons_professionals($year_encoded, $prof_encoded, $salt, $token)
	{
		try{
			$msg	= "";
			$info	= array();
			$data 	= array();
			
			$encoded_id	= $year_encoded . '/' . $prof_encoded;
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);
			
						
			$year_hash		= base64_url_decode($year_encoded);
			$prof_hash		= base64_url_decode($prof_encoded);

			
			
			$data['permission_save']	= $this->permission_add;
			
			if($this->hash(0) != $year_hash){
				
				$where			= array();				
				
				$key 			= $this->get_hash_key('year');				
				$where[$key]	= $year_hash;
				
				$key 			= $this->get_hash_key('A.profession_id');
				$where[$key]	= $prof_hash;
										
				
				$info = $this->cons_professionals->get_specific_cons_professionals($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));
				
				$data['permission_save']	= $this->permission_edit;
			}
			
			$resources					= array();
			$resources['load_css'] 		= array(CSS_DATATABLE, CSS_LABELAUTY, CSS_SELECTIZE,CSS_MODAL_COMPONENT);
			$resources['load_js'] 		= array(JS_DATATABLE, JS_LABELAUTY,JS_MODAL_CLASSIE, JS_SELECTIZE);
			
			
			$data['info']	= $info; // CONSTRUCTION PROFESSIONAL DETAILS
			
			// GET PARAMS
			$data['professions']	= $this->params->get_params(MPIS_Model::tbl_param_professions);
			$data['year_start']		= date('Y');
			$data['year_end']		= MPIS_START_YEAR; 
			
			
			
			// REGENERATE SECURITY VARIABLES
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
						
			$this->load->view('modals/cons_professionals',$data);
			$this->load_resources->get_resource($resources);			
		}
		catch(PDOException $e){
			$msg = $e->getMessage();
		}
		catch(Exception $e){
			$msg = $e->getMessage();
		}
		
		echo $msg;
		

	}
	
	public function process()
	{
		try
		{
			$flag 			= 0;
			$params 		= get_params();
			
						
			// SERVER VALIDATION
			$this->_validate($params);

	
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$where			= array();
			
			$key 			= $this->get_hash_key('year');			
			$where[$key]	= $params['year_hash'];
			
			$key 			= $this->get_hash_key('A.profession_id');
			$where[$key]	= $params['prof_hash'];

			$info			= $this->cons_professionals->get_specific_cons_professionals($where);
			$profession_id	= $info['profession_id'];
			

			MPIS_Model::beginTransaction();
			
			// IF EMPTY MEANS INSERT/SAVE RECORD
			IF(EMPTY($profession_id)){
				
				if($this->permission_add === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_add'));
				
				// CHECK IF THE RECORD IS ALREADY EXIST
				$exist = $this->_check_cons_professional($params);
					
				IF($exist)
					throw new Exception('This record is already added.');	
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= MPIS_Model::tbl_construction_professions;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array();
				
				$this->cons_professionals->insert_cons_professionals($params); // SAVES THE cons_professionals RECORD
				
				
				$where						= array();									
				$where['year']				= $params['year'];
				$where['A.profession_id']	= $params['profession'];				
				$current_info				= $this->cons_professionals->get_specific_cons_professionals($where);
								
				$curr_detail[]	= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				

				$activity	= "%s has been added in construction related licensed professionals.";
				$activity 	= sprintf($activity, $current_info['profession']);

				$msg  = $this->lang->line('data_saved');
			}

			else{

				if($this->permission_edit === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_edit'));
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= MPIS_Model::tbl_construction_professions;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array($info);
				
				
				$this->cons_professionals->update_cons_professionals($params); // UPDATE THE cons_professionals RECORD
				
				$where						= array();									
				$where['year']				= $params['year'];
				$where['A.profession_id']	= $params['profession'];				
				$current_info				= $this->cons_professionals->get_specific_cons_professionals($where);
				
				$curr_detail[]	= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				$activity	= "%s has been updated in construction related licensed professionals.";
				$activity 	= sprintf($activity, $current_info['profession']);

				$msg  = $this->lang->line('data_updated');
			}


			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table, 
				$audit_schema
			);
			
			MPIS_Model::commit();

			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			MPIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
			
		}
		catch(Exception $e)
		{
			MPIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	
	}
	

	private function _check_cons_professional($params)
	{
	
		try {
				
			$where 						= array();
			$where['year']				= $params['year'];
			$where['A.profession_id']	= $params['profession'];
				
			$info 	= $this->cons_professionals->get_specific_cons_professionals($where);
				
			return $info['profession_id'];
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}	
	
	private function _validate(&$params)
	{
		try
		{
			
			$this->_validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields 					= array();
			$fields['year']				= 'Year';
			$fields['profession']		= 'Profession';
			$fields['number']			= 'Number';
			

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}

			$security = explode('/', $params['security']);

			$params['year_encoded']	= $security[0];
			$params['prof_encoded']	= $security[1];
			$params['salt']			= $security[2];
			$params['token']		= $security[3];
			
			$params['encoded_id']	= $params['year_encoded'] . '/' . $params['prof_encoded'];  

			check_salt($params['encoded_id'], $params['salt'], $params['token']);
			
			$params['year_hash']	= base64_url_decode($params['year_encoded']);
			$params['prof_hash']	= base64_url_decode($params['prof_encoded']);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _validate_input($params)
	{
		try
		{
			
			$validation['year'] = array(
				'data_type' => 'digit',
				'name'		=> 'Year'
			);
			
			$validation['profession'] = array(
				'data_type' => 'digit',
				'name'		=> 'Profession'
			);
			
			$validation['number'] = array(
				'data_type' => 'digit',
				'name'		=> 'Number'
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function delete_cons_professionals($params) 
	{
		try 
		{

			$flag				= 0;
			$msg				= "Error";
			
			if($this->permission_delete === FALSE)
				throw new Exception($this->lang->line('err_unauthorized_delete'));
			
			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->_validate_security($params);

			
			$where			= array();
			
			$key 			= $this->get_hash_key('year');
			$where[$key]	= $params['year_hash'];
			
			$key 			= $this->get_hash_key('A.profession_id');
			$where[$key]	= $params['prof_hash'];
			
			$info 			= $this->cons_professionals->get_specific_cons_professionals($where);
			
			
			$title			= $info['year'] . ' ' . $info['profession'];

			if(EMPTY($info['profession_id']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			MPIS_Model::beginTransaction();
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= MPIS_Model::tbl_construction_professions;
			$audit_schema[]	= DB_MPIS;			
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL

			$this->cons_professionals->delete_cons_professionals($info['year'], $info['profession_id']);

			// SET AS AN ARRAY
			$curr_detail[]	= array();
			
			// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $title);
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			MPIS_Model::commit();

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			MPIS_Model::rollback();

			$msg	= $e->getMessage(); 	
		}
		catch(Exception $e)
		{	

			MPIS_Model::rollback();
						
			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
	}
}

/* End of file mpis_cons_related_prof.php */
/* Location: ./application/modules/mpis/controllers/mpis_cons_related_prof.php */