<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_code_libraries_issuance_classifications extends MPIS_Controller {
	
	private $module = MODULE_MPIS_CODE_LIBRARIES_ISSUANCE_CLASSIFICATIONS;
	
	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('issuance_classifications_model', 'issuance_classifications');
		
		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);

	}
	
	public function index()
	{	
		try{
			$data = $resources = $modal = array();
					
			if($this->permission)
			{
				$modal = array(
					'modal_issuance_classification' => array(
						'module'		=> PROJECT_MPIS,
						'controller'	=> __CLASS__,
						'method'		=> 'modal_issuance_classification',
						'title'			=> 'Issuance Classification',					
						'multiple'		=> TRUE
					),
					
				);
				
				$resources['load_css'] 		= array(CSS_DATATABLE);
				$resources['load_js'] 		= array(JS_DATATABLE);
				$resources['load_modal'] 	= $modal;
				$resources['datatable']		= array();
				$resources['datatable'][] 	= array(
					'table_id' 	=> 'issuance_classification_table', 
					'path' 		=> PROJECT_MPIS.'/mpis_code_libraries_issuance_classifications/get_issuance_classification_list'				
				);
				
				
				// STORE THE PERMISSION
				$data['permission_add']	= $this->permission_add; 	
				
				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash(0);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);
				$data['url']	= $encoded_id."/".$salt."/".$token;
				
				$view_page	= "issuance_classification";
			}
			else
			{
				$view_page	= "errors/html/error_permission";
			}

			$this->template->load($view_page, $data, $resources);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
	
	public function get_issuance_classification_list()
	{
		try{

			$params = get_params(); // DO NOT USE: $_POST $this->input->post
			
			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> 0,
				"iTotalDisplayRecords" 	=> 0,
				"aaData" 				=> array()
			);
			
			if(!$this->permission)
				throw new Exception($this->lang->line('err_unauthorized_view'));
		
			// FIELDS TO BE SELECTED FROM TABLE
			$select_fields = array(
				"A.issuance_type_id",
				"A.issuance_type_name",
				"A.issuance_type_desc",
				"B.issuance_type_total"
			);

			// APPEARS ON TABLE
			$where_fields = array(
				"A.issuance_type_name",
				"A.issuance_type_desc",
			);
		
			$results 		= $this->issuance_classifications->get_issuance_classification_list($select_fields, $where_fields, $params);
			$total	 		= $this->issuance_classifications->total_length(); // TOTAL COUNT OF RECORDS
			$filtered_total = $this->issuance_classifications->filtered_length($select_fields, $where_fields, $params); // TOTAL COUNT OF RECORDS PER PAGE
		
			foreach ($results as $data):

				// PRIMARY KEY
				$primary_key	= $data["issuance_type_id"];

				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash($primary_key);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("issuance classification", "'.$url.'")';

				// START: CONSTRUCT ACTION ICONS
				$action = "<div class='table-actions'>";
				
					if($this->permission_edit) :
						$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_issuance_classification' onclick=\"modal_issuance_classification_init('".$url."')\"></a>";
					endif;
					
					if($this->permission_delete AND EMPTY($data['issuance_type_total'])) : 	  
						$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					endif;
				
				$action .= '</div>';
				// END: CONSTRUCT ACTION ICONS				

				$row 	= array();
				$row[] 	= $data['issuance_type_name'];
				$row[] 	= $data['issuance_type_desc'];
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
				
			endforeach;
			
			$output["iTotalRecords"] 		= $total["cnt"];
			$output["iTotalDisplayRecords"] = $filtered_total["cnt"];
		}
		catch(PDOException $e){
			$this->rlog_error($e);
		}
		catch(Exception $e){
			$this->rlog_error($e);
		}
		
		echo json_encode( $output );
	}
	
	public function modal_issuance_classification($encoded_id, $salt, $token)
	{
		try{
			$msg	= "";
			$info	= array();
			$data 	= array();
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);
			
			$hash_id = base64_url_decode($encoded_id);
			
			$data['permission_save']	= $this->permission_add;
			
			if($this->hash(0) != $hash_id){
				
				$key 			= $this->get_hash_key('issuance_type_id');
				$where			= array();
				$where[$key]	= $hash_id;
				
				$info = $this->issuance_classifications->get_specific_issuance_classification($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));
				
				$data['permission_save']	= $this->permission_edit;
			}
			
			$resources					= array();
			$resources['load_css'] 		= array(CSS_LABELAUTY);
			$resources['load_js'] 		= array(JS_LABELAUTY);
			
			
			$data['info']	= $info; // POLICY CATEGORY DETAILS
		
			// REGENERATE SECURITY VARIABLES
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			$this->load->view('modals/issuance_classification',$data);
			$this->load_resources->get_resource($resources);			
		}
		catch(PDOException $e){
			$msg = $e->getMessage();
		}
		catch(Exception $e){
			$msg = $e->getMessage();
		}
		

	}
	
	public function process()
	{
		try
		{
			$flag 			= 0;
			$params 		= get_params();
			
						
			// SERVER VALIDATION
			$this->_validate($params);

	
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('issuance_type_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 				= $this->issuance_classifications->get_specific_issuance_classification($where);
			$issuance_type_id	= $info['issuance_type_id'];
			

			MPIS_Model::beginTransaction();

			$activity_title	= $params['issuance_type_name'];
			
			// IF EMPTY MEANS INSERT/SAVE RECORD
			IF(EMPTY($issuance_type_id)){
				
				if($this->permission_add === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_add'));
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= MPIS_Model::tbl_param_issuance_types;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array();
				
				$issuance_type_id = $this->issuance_classifications->insert_issuance_classification($params); // SAVES THE INDICATOR RECORD
				
				$current_info 		= $this->issuance_classifications->get_specific_issuance_classification(array('issuance_type_id' => $issuance_type_id));
				
				$curr_detail[]		= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				

				$activity	= "%s has been added in issuance classification.";
				$activity 	= sprintf($activity, $activity_title);

				$msg  = $this->lang->line('data_saved');
			}

			else{

				if($this->permission_edit === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_edit'));
				
				$params['issuance_type_id']	= $issuance_type_id;

				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= MPIS_Model::tbl_param_issuance_types;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array($info);
				
				$this->issuance_classifications->update_issuance_classification($params); // UPDATE THE INDICATOR RECORD
				
				$current_info 	= $this->issuance_classifications->get_specific_issuance_classification(array('issuance_type_id' => $issuance_type_id));
				
				$curr_detail[]	= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				$activity	= "%s has been updated in issuance classification.";
				$activity 	= sprintf($activity, $activity_title);

				$msg  = $this->lang->line('data_updated');
			}


			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table, 
				$audit_schema
			);
			
			MPIS_Model::commit();

			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			MPIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
			
		}
		catch(Exception $e)
		{
			MPIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	
	}
	
	/*
	 * START: SERVER VALIDATION 
	 */
	
	private function _validate(&$params)
	{
		try
		{

			$this->_validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields = array();
			$fields["issuance_type_name"]		= 'Issuance Classification';

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _validate_input($params)
	{
		try
		{
			
			$validation['issuance_type_name'] = array(
				'data_type' => 'string',
				'name'		=> 'Issuance Classification',
				'min_len'	=> 5,
				'max_len'	=> 255
			);
			
			IF(!EMPTY($params['issuance_type_desc']))
			{
				$validation['issuance_type_desc'] = array(
					'data_type' => 'string',
					'name'		=> 'Description',
					'min_len'	=> 5,
					'max_len'	=> 60000
				);
			}
			
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	/*
	 * END: SERVER VALIDATION
	 */
	
	
	public function delete_issuance_classification() 
	{
		try 
		{

			$flag				= 0;
			$msg				= "Error";
						
			if($this->permission_delete === FALSE)
				throw new Exception($this->lang->line('err_unauthorized_delete'));
			
			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->_validate_security($params);

			$key 			= $this->get_hash_key('issuance_type_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];
			
			$info 			= $this->issuance_classifications->get_specific_issuance_classification($where);
			
			
			$title			= $info['issuance_type_name'];

			if(EMPTY($info['issuance_type_id']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			MPIS_Model::beginTransaction();
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= MPIS_Model::tbl_param_issuance_types;
			$audit_schema[]	= DB_MPIS;			
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL

			$this->issuance_classifications->delete_issuance_classification($info['issuance_type_id']);

			// SET AS AN ARRAY
			$curr_detail[]	= array();
			
			// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $title);
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			MPIS_Model::commit();

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			MPIS_Model::rollback();

			$msg	= $e->getMessage(); 	
		}
		catch(Exception $e)
		{	

			MPIS_Model::rollback();
						
			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
	}
}

/* End of file Ceis_positions.php */
/* Location: ./application/modules/ceis/controllers/Ceis_positions.php */