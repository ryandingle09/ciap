<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_disbursement_vouchers extends BAS_Controller {

	private $module = MODULE_BAS_DISBURSEMENT_VOUCHERS;

	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;

	public $permission_print	= FALSE;	
	public $permission_void		= FALSE;
	public $permission_certify	= FALSE;
	public $permission_override	= FALSE;
	public $permission_excess	= FALSE;
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Bas_disbursement_voucher_model', 'disbursement');
		$this->load->model('Bas_billings_model', 'billings');
		$this->load->model('Bas_obligation_slips_model', 'obligations');
		$this->load->model('Bas_line_items_model', 'line_items');

		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);
		$this->permission_print		= $this->check_permission($this->module, ACTION_PRINT);
		$this->permission_void		= $this->check_permission($this->module, ACTION_VOID);
		$this->permission_override	= $this->check_permission($this->module, ACTION_OVERRIDE);
		$this->permission_certify	= $this->check_permission($this->module, ACTION_CERTIFY);
		$this->permission_excess	= $this->check_permission($this->module, ACTION_EXCESS);
	}
	
	public function index()
	{	
		try
		{	
			$data 			= array();
			$resources 		= array(
				'load_modal' => array(
					'modal_default' => array(
						'controller'	=> __CLASS__,
						'method'		=> 'all_modal',
						'module'		=> PROJECT_BAS,
						'title'			=> 'Disbursement Voucher',
						'multiple'   	=> true,
						'size'			=> 'xl',
						'height'	 	=> '500px;'
					),
				),
				'load_css' 	=> array(
					CSS_DATATABLE,
					CSS_SELECTIZE,
					CSS_LABELAUTY,
					CSS_MODAL_COMPONENT, 
					CSS_DATETIMEPICKER
				),
				'load_js' 	=> array(
					JS_DATATABLE,
					JS_SELECTIZE,
					JS_MODAL_CLASSIE,
					JS_MODAL_EFFECTS,
					JS_LABELAUTY,
					'modules/'.PROJECT_BAS.'/'.strtolower( __CLASS__ ),
						//'modules/'.PROJECT_BAS.'/'.strtolower( __CLASS__ )
				),
				'datatable' => array(
					array(
						'table_id' => 'tbl_disbursement', 
						'path' => PROJECT_BAS.'/bas_disbursement_vouchers/get_disbursement_list'
					),
				),
			);
			
			$this->template->load('disbursement_voucher', $data, $resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}
	
	public function get_disbursement_list()
	{
		try
		{
			$params = get_params();
			$cnt 	= 0;
		
			$fields = array
			(
				'd.disbursement_id',
				'd.disbursement_fund_id',
				'pf.disbursement_fund_id',
				'pf.fund_name',
				'd.dv_no', 
				'd.dv_date', 
				'IF(d.disbursed_amount = 0, "", FORMAT(d.disbursed_amount, 0)) disbursed_amount',
				'p.disbursement_status',
				'p.disbursement_status_id', 
				'd.disbursement_status_id', 
				'd.obligation_id'
			);

			$selects = array
			(
				'fund_name', 
				'dv_no', 
				'dv_date', 
				'disbursed_amount', 
				'disbursement_status',
			);
		
			$list = $this->disbursement->get_disbursement_list($fields, $selects, $params);
			$iTotal = $this->disbursement->total_length();
			$iFilteredTotal = $this->disbursement->filtered_length($fields, $selects, $params);

			$output = array(
				"sEcho" => intval($_POST['sEcho']),
				"iTotalRecords" => $iTotal["cnt"],
				"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
				"aaData" => array()
			);
		
			foreach ($list as $aRow):
				$cnt++;
				$row = array();
				$action = "";

				$id	= $aRow["disbursement_id"];
				
				$hash_id 		= $this->hash($id); 
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;

				$count = $this->disbursement->get_disbursement_count($id);
				$ctr = $count['cnt'];

				$delete_action 	= 'content_delete("Disbursement Voucher", "'.$url.'")';

				for ( $i=0 ; $i<count($selects) ; $i++ )
				{
					$row[] = $aRow[ $selects[$i] ];
				}
				
				$action = "<div class='table-actions'>";

					if($this->permission_print) :
						$action .= "
							<a href='bas_disbursement_vouchers/print_voucher_slip/".$url."' target='_blank' class='tooltip tooltipped' data-tooltip='Print' data-position='bottom' data-delay='50'><i class='flaticon-printing23'></i>
							</a>";
					endif;

					if($this->permission_view) :
						$action .= "
							<a href='javascript:void(0)' class='tooltip view md-trigger tooltipped' data-tooltip='View or Approve' data-position='bottom' data-delay='50' data-modal='modal_default' onclick=\"modal_default_init('".$url."/approval')\">
							</a>";
					endif;

					if($aRow['disbursement_status'] != STATUS_VOIDED && $aRow['disbursement_status'] != STATUS_DISAPPROVED):
						
						if($this->permission_override) :
							$action .= "
								<a href='javascript:void(0)' class='tooltip md-trigger tooltipped' data-tooltip='Override' data-position='bottom' data-delay='50' data-modal='modal_default' onclick=\"modal_default_init('".$url."/override')\"><i class='flaticon-access1'></i>
								</a>";
						endif;
						
						if($this->permission_void) :
							$action .= "
								<a href='javascript:void(0)' class='tooltip md-trigger tooltipped' data-tooltip='Void' data-position='bottom' data-delay='50' data-modal='modal_default' onclick=\"modal_default_init('".$url."/void')\"><i class='flaticon-cross95'></i>
								</a>";
						endif;

					endif;

				$action .= '</div>';
				
				$row[]	= $action;
					
				$output['aaData'][] = $row;

			endforeach;
		
			echo json_encode( $output );
		
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
			$this->get_user_message($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function print_voucher_slip($encoded_id, $salt, $token)
	{

		try
		{
			$data = array();

			if(EMPTY($encoded_id))
				throw new Exception('Invalid action.');


			check_salt($encoded_id, $salt, $token);
				
			$hash_id 			= base64_url_decode($encoded_id);
			$key 				= $this->get_hash_key('disbursement_id'); 
			$where				= array();
			$where[$key]		= $hash_id;

			$info 				= $this->disbursement->get_specific_disbursement($where);

			$disbursement_id 	= $info['disbursement_id'];

			$key2 				= $this->get_hash_key('d.disbursement_id'); 

			$getter 			= $this->disbursement->get_print_info($hash_id, $key2);

			foreach($getter as $key => $index){ $id = $index['disbursement_id'];}

			$data = array(
				'account_codes' => $this->disbursement->get_all_account_codes(),
				'charges' 		=> $this->disbursement->get_charges($id),
				'form_data' 	=> $getter,
			);

			$html 				= $this->load->view( 'reports/voucher_slip_form', $data, TRUE );

			$filename			= 'Obligation_slip_form_'.date('F').'_'.date('d').'_'.date('Y');
			
			ob_end_clean();
			
			$this->pdf( $filename, $html, TRUE, 'I', NULL, NULL, TRUE );
			
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}
	
	public function all_modal($encoded_id, $salt, $token)
	{
		try
		{
			$data 		= array();
			$resources 	= array();

			$params   	= get_params();
			
			if(!EMPTY($encoded_id))
			{
				check_salt($encoded_id, $salt, $token);
				
				$hash_id 		= base64_url_decode($encoded_id);
				$key 			= $this->get_hash_key('disbursement_id'); 
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->disbursement->get_specific_disbursement($where);
			}
			else
			{
				$hash_id 	= $this->hash(0);				
			}
			
			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);

			$status = $this->disbursement->get_specific_status($info['disbursement_status_id']);

			$data = array(
				'security' 					=> $encoded_id . '/' . $salt . '/' . $token,
				'slips'						=> $this->billings->get_obligation_slip_nos_for_disbursement(),
				'bills'						=> $this->disbursement->get_billings(),
				'funds'						=> $this->disbursement->get_funds(),
				'details' 					=> $this->disbursement->get_disbursement_details(['disbursement_id'=> (!empty($params['details'])) ? $params['details'] : $info['disbursement_id']]),
				'fund' 						=> $info['disbursement_fund_id'],
				'obligation_slip_no'		=> $info['obligation_id'],
				'billing_id' 				=> $info['billing_id'],
				'disbursement_id' 			=> $info['disbursement_id'],
				'dv_date' 					=> $info['dv_date'],
				'dv_no' 					=> $info['dv_no'],
				'fund' 						=> $info['disbursement_fund_id'],
				'payee' 					=> $info['payee_id'],
				'particulars' 				=> $info['particulars'],
				'disbursement_status_id' 	=> $info['disbursement_status_id'],
				'remarks'					=> $info['remarks'],
				'particulars'				=> $info['particulars'],
				'current_status'			=> $status['disbursement_status'],
				'full_details'				=> $this->disbursement->get_specific_disbursement_full($info['disbursement_id']),
				'obligation_charges' 		=> $this->disbursement->get_obligation_charges($info['obligation_id']),
				'details2'					=> $this->disbursement->get_disbursement_details_by_item($info['disbursement_id']),
				'details3'					=> $this->disbursement->get_disbursement_deductions($info['disbursement_id']),
				'account_codes' 			=> $this->disbursement->get_account_codes(),
				'deductions' 				=> $this->disbursement->get_deductions(),
				'isca_details'	 			=> $this->disbursement->get_arbitrator_from_isca_deduction($info['arbitrator_id']),
			);

			$resources = [
				'load_css' 	=> [
					'selectize.default'
				],
				'load_js' 	=> [
					'popModal.min', 
					'selectize'
				],
				'load_modal'=> [
					'modal_tax' => [
						'controller'		=> __CLASS__,
						'method'			=> 'all_modal',
						'module'			=> PROJECT_BAS,
						'multiple'	 		=> true,
					],
				],
				'loaded_init' => [
					'ModalEffects.re_init();',
					'selectize_init();',
				]
			];

			$content 	= $this->uri->segment(7);
			$info 		= [];

			switch ($content){

			    case "tax":
			    	if($this->uri->segment(8) == 'cmdf')$info = $this->disbursement->get_payee_info($this->uri->segment(10));
			    	elseif($this->uri->segment(8) == 'ciac') $info = $this->disbursement->get_obligation_info($this->uri->segment(8));
			    	else $info = $this->disbursement->get_obligation_info($this->uri->segment(8));
			    	$data['payee_info'] = $info;
			        $this->load->view("modals/disbursement_tax", $data);
			        break;
			    default:
			        $this->load->view("modals/disbursement_voucher", $data);
			}

			$this->load_resources->get_resource($resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function get_row()
	{
		try
		{
			$params = get_params();

			$data = [
				'count' 		=> $params['new_count'],
				'identity'		=> $params['identity'],
				'account_codes' => $this->disbursement->get_account_codes(),
				'deductions' 	=> $this->disbursement->get_deductions(),
			];

			$resources = [
				'load_css' 	=> [
					'selectize.default'
				],
				'load_js' 	=> [
					'popModal.min', 
					'selectize',
				],
				'loaded_init' => [
					'ModalEffects.re_init();',
					'selectize_init();',
				],
			];

			if($params['identity'] == 'ciac')
			{
				$this->load->view('tables/disbursement_ciac_row', $data);
			}
			else
			{
				$this->load->view('tables/disbursement_cmdf_row', $data);
			}

			$this->load_resources->get_resource($resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}
	

	public function load_tables()
	{
		try
		{
			$params = get_params();

			$resources 	= [];
			$data 		= [];

			$value 		= $params['value'];
			$identity 	= $params['identity'];
			$security 	= $params['security'];
			$view 		= 'Invalid Request';

			if($identity == 'fund')
			{

				if($value == '1') $view = 'disbursement_gaa_fund_table';
				if($value == '2') $view = 'disbursement_cmdf_fund_table';
				if($value == '3') $view = 'disbursement_ciac_fund_table';
			}

			if($identity == 'obligation_slip')
			{
				$view = 'disbursement_obligation_table';
			}

			$resources = [
				'load_css' 	=> ['selectize.default'],
				'load_js' 	=> [
					'popModal.min', 
					'selectize',
				],
				'loaded_init' => [
					'ModalEffects.re_init();'
				]
			];

			$full_details = $this->disbursement->get_specific_disbursement_full($params['details']);

			foreach ($full_details as $key => $vals):
			//even this is empty, array value of this query can access outside after this end loop
			endforeach;

			$data = [
				'obligation_info'		=> $this->disbursement->get_obligation_info($value),
				'obligation_charges'	=> $this->disbursement->get_obligation_charges($value),
				'security' 				=> $security,
				'slips'					=> $this->billings->get_obligation_slip_nos_for_disbursement(),
				'funds'					=> $this->disbursement->get_funds(),
				'payees' 				=> $this->disbursement->get_payees(),
				'cases' 				=> $this->disbursement->get_cases(),
				'arbitrators'			=> $this->disbursement->get_arbitrators(),
				'proceeding'			=> $this->disbursement->get_proceeding_stages(),
				'account_codes' 		=> $this->disbursement->get_account_codes(),
				'deductions' 			=> $this->disbursement->get_deductions(),
				'details2'				=> $this->disbursement->get_disbursement_details_by_item($params['details']),
				'details3'				=> $this->disbursement->get_disbursement_details_by_item_3($params['details']),
				'proceeding_id' 		=> $vals['proceeding_stage_id'],
				'arbitrator' 			=> $vals['arbitrator_id'],
				'case' 					=> $vals['case_id'],
				'arbitrator_fee'		=> $vals['disbursement_amount'],
				'particulars'			=> $vals['particulars'],
			];
			
			$this->load->view('tables/'.$view.'', $data);
			$this->load_resources->get_resource($resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function get_arbitrator_from_isca_deduction()
	{
		try
		{
			$params 	= get_params();
			
			$deductions = $this->disbursement->get_arbitrator_from_isca_deduction($params['value']);
			
			foreach ($deductions as $key => $value) 
			{
				echo '<tr>';

				echo '<td>'.$value['deduction_name'].'</td>';
				echo '<td class="right-align">'.number_format($value['amount']).'</td>';

				echo '<tr>';
			}
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function get_amount()
	{
		try
		{
			$params 	= get_params();

			$identity 	= $params['identity'];
			$value 		= $params['value'];

			$options 	= $this->disbursement->get_amount($identity, $value);
			
			echo json_encode($options);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function fill_dropdowns()
	{
		try
		{
			$params 	= get_params();

			$identity 	= $params['identity'];
			$value 		= $params['value'];
			$extra 		= $params['target2'];

			$options 	= $this->disbursement->fill_dropdowns($identity, $value, $extra);
			
			echo json_encode($options);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}
	
	public function process()
	{
		try
		{
			$flag 	= 0;
            $class  = ERROR;
			$params = get_params();

			$this->_validate($params);
	
			$key 			= $this->get_hash_key('disbursement_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->disbursement->get_specific_disbursement($where);
			$id				= $info['disbursement_id'];
			
			BAS_Model::beginTransaction();

			if(EMPTY($id))
			{
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= BAS_Model::tbl_disbursements;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);

				$this->disbursement->insert_disbursement($params);

				$activity	= "New Disbusement has been added.";

				$msg  = $this->lang->line('data_saved');
			}
			else
			{
				$audit_action[]	= AUDIT_UPDATE;
				$audit_table[]	= BAS_Model::tbl_disbursements;
				$audit_schema[]	= DB_BAS;

				$current_info	= $this->disbursement->get_specific_disbursement(array("disbursement_id" => $id) , TRUE);
				$prev_detail[]	= array($current_info);
				
				$params['disbursement_id']	= $id;

				$this->disbursement->update_disbursement($params);

				$info			= $this->disbursement->get_specific_disbursement(array("disbursement_id" => $id) , TRUE);
				$curr_detail[]	= $info;
				
				$activity	= "Disbursement has been updated.";

				$msg  = $this->lang->line('data_updated');
			}
			
			$activity = $activity;//sprintf($activity, $params['dv_no']);

			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
			
			BAS_Model::commit();
			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
			$this->get_user_message($e);
			
		}
		catch(Exception $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
		echo json_encode($info);
	}

	public function validate_tax_form()
	{
		try
		{
			$flag 		= 0;
			$params 	= get_params();

			$this->_validate_tax($params);

			$flag 		= 1;
			$tax_amount = (str_replace(',','', $params['ewt_amount']) + str_replace(',','', $params['vat_amount']));
		}
		catch(PDOException $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
			
		}
		catch(Exception $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}

		$info = array(
			"flag" 			=> $flag,
			"msg" 			=> $msg,
			"tax_amount" 	=> number_format($tax_amount),
			"count"			=> $params['count'],
			"ewt_rate"		=> $params['ewt_rate'],
			"ewt_amount"	=> $params['ewt_amount'],
			"vat_rate"		=> $params['vat_rate'],
			"vat_amount"	=> $params['vat_amount']
		);

		echo json_encode($info);
	}

	public function _validate_tax(&$params)
	{
		try
		{
			$this->_validate_security($params);

			$fields = [
				'ewt_rate' 		=> 'Ewt Rate',
				'vat_rate' 		=> 'Vat Rate',
				'ewt_amount'	=> 'Ewt Amount',
				'vat_amount'	=> 'Vat Amount',
			];

			if($params['ewt_rate'] >= 100 || $params['vat_rate'] >= 100)
			{
				throw new Exception('Vat rate or Ewt Rate Cannot greater than 100%');
			}

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _validate(&$params)
	{
		try
		{
			$this->_validate_security($params);

			$fields = [];

			if(isset($params['remarks'])) $fields['remarks'] = 'Remarks';

			if($params['funds'] == '1')//gaa
			{
				if($params['status'] == STATUS_POSTED || $params['status'] == STATUS_OVERRIDEN)
				{
					$fields = [
						'funds' 				=> 'Fund Source',
						'tax_amount' 			=> 'Tax Amount',
						'disbursed_amount' 		=> 'Disbursement Amount',
					];

					if($params['status'] == STATUS_POSTED)
					{
						$fields = [
							'obligation_slip_no' 	=> 'Obligation Slip No',
							'obligation_slip_no'	=> 'Bill No',
						];
					}
				}
			}

			if($params['funds'] == '2')//cmdf
			{
				$fields = [
					'funds' 				=> 'Fund Source',
					'tax_amount' 			=> 'Tax Amount',
					'amount' 				=> 'Base Amount',
				];

				if($params['status'] == STATUS_POSTED)
				{
					$fields = [
						'payee'				=> 'Payee',
						'address'			=> 'Payee Address',
						'particulars'		=> 'Particulars',
						'account_code'		=> 'Account Code',
					];
				}
			}

			if($params['funds'] == '3')//ciac
			{
				$fields = [
					'funds' 		=> 'Fund Source'
				];

				if(ISSET($params['deduction_type']))
					$fields['deduction_type'] = 'Deduction Type';

				if(ISSET($params['amount']))
					$fields['amount'] = 'Amount';

				if($params['funds'] == 'POST')
				{
					$fields = [
						'case'			=> 'Case No',
						'proceeding'	=> 'Stage of proceeding',
						'arbitrator'	=> 'Case No',
						'particulars'	=> 'Particulars',
					];
				}

				foreach(array_count_values($params['deduction_type']) as $key => $val)
				{
					if($val == 2) throw new Exception("<b>Invalid Action Detected!</b> Please select different deduction type.");
				}

			}
			
			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

    private function _validate_security(&$params)
    {
        try
        {
            if(EMPTY($params['security']))
            {
                throw new Exception($this->lang->line('err_invalid_request'));
            }

            $security = explode('/', $params['security']);

            $params['encoded_id']   = $security[0];
            $params['salt']         = $security[1];
            $params['token']        = $security[2];

            check_salt($params['encoded_id'], $params['salt'], $params['token']);

            $params['hash_id']      = base64_url_decode($params['encoded_id']);
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
	
	private function _validate_input($params)
	{
		try
		{
			$validation = array(
				'funds' => array(
					'data_type' => 'digit',
					'name'		=> 'Fund Source',
					'max_len'	=> 1
				),
				'obligation_slip_no' => array(
					'data_type' => 'string',
					'name'		=> 'Obligation Slip No',
					'max_len'	=> 255
				),
				'bill_no' => array(
					'data_type' => 'digit',
					'name'		=> 'Bill No',
					'max_len'	=> 11
				),
				'disbursed_amount' => array(
					'data_type' => 'amount',
					'name'		=> 'Disburse Amount',
					'min_len'	=> 1
				),
				'ewt_rate' => array(
					'data_type' => 'digit',
					'name'		=> 'EWT Rate',
					'max_len'	=> 3,
					'min_len'	=>	1
				),
				'ewt_amount' => array(
					'data_type' => 'amount',
					'name'		=> 'EWT Rate',
					'min_len'	=> 1
				),
				'vat_rate' => array(
					'data_type' => 'digit',
					'name'		=> 'Vat Rate',
					'max_len'	=> 3,
					'min_len'	=>	1
				),
				'vat_amount' => array(
					'data_type' => 'amount',
					'name'		=> 'Vat Amount',
					'min_len'	=> 1
				),
				'tax_amount' => array(
					'data_type' => 'amount',
					'name'		=> 'Tax Amount',
					//'min_len'	=> 1
				),
				'payee' => array(
					'data_type' => 'digit',
					'name'		=> 'Tax Amount',
					//'min_len'	=> 1
				),
				'amount' => array(
					'data_type' => 'amount',
					'name'		=> 'Base Amount',
					'min_len'	=> 1
				),
				'address' => array(
					'data_type' => 'string',
					'name'		=> 'Payee Address',
					//'min_len'	=> 1
				),
				'particulars' => array(
					'data_type' => 'string',
					'name'		=> 'Particulars',
					//'min_len'	=> 1
				),
				'remarks' => array(
					'data_type' => 'string',
					'name'		=> 'Remarks',
					//'min_len'	=> 1
				),
				'case' => array(
					'data_type' => 'string',
					'name'		=> 'Case No',
					'min_len'	=> 1,
					//'max_len'	=> 7
				),
				'proceeding' => array(
					'data_type' => 'digit',
					'name'		=> 'Stage of proceeding',
					'min_len'	=> 1
				),
				'arbitrator' => array(
					'data_type' => 'digit',
					'name'		=> 'Arbitrators',
					//'min_len'	=> 1
				),
				'deduction_type' => array(
					'data_type' => 'string',
					'name'		=> 'Deduction Type',
					//'min_len'	=> 1
				)
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function delete_disbursement($params)
	{
		try
		{
			$flag	= 0;
			$msg	= "Error";

			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->validate_security($params);

			$key 			= $this->get_hash_key('disbursement_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->disbursement->get_specific_disbursement($where);
			
			if(EMPTY($info['disbursement_id']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			$this->disbursement->delete_disbursement($info['disbursement_id']);

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			$msg	= $e->getMessage(); 
			$this->get_user_message($e);	
		}
		catch(Exception $e)
		{	
			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag" 		=> $flag,
			"msg" 		=> $msg,
			"reload" 	=> 'datatable',
			"table_id" 	=> 'tbl_disbursement',
			"path"		=> PROJECT_BAS . '/bas_disbursement_vouchers/get_disbursement_list/'
		);

		echo json_encode($info);

	}

	public function get_obligation_details()
	{
		try
		{
			$params = get_params();
			$obligation =  $this->obligations->get_obligation_full_details($params['obligation_slip_no']);
			echo json_encode($obligation);
		}		
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}	

	}

	public function delete_row()
	{
		try
		{
			$params = get_params();

			$data 	=  $this->disbursement->delete_disbursement($params);
			echo json_encode(['status'=>$data]);
		}		
		catch( PDOException $e )
		{
			$this->rlog_error($e);
			$this->get_user_message($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}	
	}
	
}


/* End of file users.php */
/* Location: ./application/modules/sysad/controllers/users.php */