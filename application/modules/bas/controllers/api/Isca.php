<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

class Isca extends REST_Controller {

    function __construct()
    {
        parent::__construct();

        $this->methods['get_get']['limit'] = 500; // 500 requests per hour per user/key
        $this->methods['put_post']['limit'] = 100; // 100 requests per hour per user/key
        $this->methods['delete_delete']['limit'] = 50; // 50 requests per hour per user/key

        $this->load->model('api_model', 'api');
    }

    public function opt_get()//for delete porpose - get request for simple transact
    {
        /*
        URL 
        
        NOTE : include security as url get params 

        use case id for show/get method identity

        - DISREGARD
        - domain/bas/api/isca/opt?porpose=showall&security=encodedsucurityofyourcontroller -- all data and porpose = param_porpose = show all
        - domain/bas/api/isca/opt/id/1/?porpose=showone&security=encodedsucurityofyourcontroller -- specific data with param porpose = show one and include segment id
        - domain/bas/api/isca/opt/id/1/?porpose=delete&security=encodedsucurityofyourcontroller -- spec data with param porpose = delete and include segment id

        */

        $id         = $this->get('id');

        $security   = $this->input->get('security');

        $data       = $this->api->read($id);

        $params     = ['security'=> $security];

        $this->_validate_security($params);

        if($id === NULL)
        {
            if ($data)
            {
                $this->response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
            }
            else
            {
                // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'No data were found'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
        }
        else
        {
            foreach ($data as $key => $value)
            {
                if (isset($value['id']) && $value['id'] === $id)
                {
                    $data = $value;
                }
            }

            $this->response($data, REST_Controller::HTTP_OK); // OK (200) being the HTTP response code
        }
    }

    public function put_post()
    {
        //update/post/edit api function

        /**************************************
        URL --

        - bas/api/isca/put?action=delete -- post/insert data


        JSON SRTUCTURE PARAMS --
        IN ANY ORDER 

        security' => 'encodesecurity',
        data 
            parent
                -child => val
                -etc   => val,

            parent
                -child => val
                -etc   => val,

            parent
                -child => val
                -etc   => val

        close

        ex.

        array(
            'security' => 'encodesecutiyy',
            'data' => array(

                //--CASES
                'cases' =>  array(
                    'case_id'                   => 'case_id',
                    'case_no'                   => 'case_no',
                    'case_title'                => 'case_title',
                    'created_by'                => 'created_by',
                    'created_date'              => 'created_date', 
                ),

                //--ARBITRATORS
                'arbitators' =>  array(
                    'arbitrator_id'             => 'arbitrator_id',
                    'name'                      => 'name',
                    'created_by'                => 'created_by',
                    'created_date'              => 'created_date',
                ),   

                //--CASE ARBITRATOR DEDUCTIONS
                'case_arbitrator_fee_shares'    => array(
                    'arbitrator_payment_id'     => $value['arbitrator_payment_id'],
                    'arbitrator_id'             => $value['arbitrator_id'],
                    'deduction_type_id'         => $value['deduction_type_id'],
                    'amount'                    => $value['amount']
                ),   

                //--CASE ARBITRATOR FEE SHARES
                'case_arbitrator_fee_shares'    => array(
                    'arbitrator_payment_id'     => 'arbitrator_payment_id',
                    'arbitrator_id'             => 'arbitrator_id',
                    'stage_activity_id'         => 'stage_activity_id',
                    'percent'                   => 'percent',
                    'total_amount'              => 'total_amount',
                ),   

                 //--CASE ARBITRATOR PAYMENTS
                'case_arbitrator_payments'      => array(
                    'arbitrator_payment_id'     => 'arbitrator_payment_id',
                    'case_id'                   => 'case_id',
                    'proceeding_stage_id'       => 'proceeding_stage_id',
                    'final_flag'                => 'final_flag',
                    'created_by'                => 'created_by',
                    'created_date'              => 'created_date'
                ),  

                 //--PARAM PROCEEDING STAGES
                'param_proceeding_stages'       => array(
                    'proceeding_stage_id'       => 'proceeding_stage_id',
                    'proceeding_stage'          => 'proceeding_stage'
                ),   
            )
        );

        **************************************/

        $action  = $this->get('action');//for action process

        $data    = $this->post();

        $this->_validate_security($data);//activate this in real scenario

        if(ISSET($action))
        {   
            if($action == 'delete')
            {

                //delete data 
                $this->api->delete($data);
            }
            else
            {
                 // Set the response and exit
                $this->response([
                    'status' => FALSE,
                    'message' => 'METHOD NOT FOUND'
                ], REST_Controller::HTTP_NOT_FOUND); // NOT_FOUND (404) being the HTTP response code
            }
            }
        }
        else
        {
            //insert process
            $this->api->insert($data);
        }

        $this->set_response($data, REST_Controller::HTTP_CREATED); // CREATED (201) being the HTTP response code
    }

    //validate action
    private function _validate_security(&$params)
    {
        try
        {
            if(EMPTY($params['security']))
            {
                throw new Exception($this->lang->line('err_invalid_request'));
            }

            $security = explode('/', $params['security']);

            $params['encoded_id']   = $security[0];
            $params['salt']         = $security[1];
            $params['token']        = $security[2];

            check_salt($params['encoded_id'], $params['salt'], $params['token']);

            $params['hash_id']      = base64_url_decode($params['encoded_id']);
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }

}
