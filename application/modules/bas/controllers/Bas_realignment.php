<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bas_realignment extends BAS_Controller 
{
	private $module = MODULE_BAS_REALIGNMENT;

	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;

	public function __construct()
	{
		parent::__construct();

		$this->load->model('bas_realignment_model', 'realignment');

		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);
	}

	public function index()
	{
		try
		{
			$data = array();

			$resources	 = array(
				'load_css'	=> array(
					CSS_DATATABLE, CSS_MODAL_COMPONENT, 
					CSS_SELECTIZE,
					CSS_DATETIMEPICKER
				),
				'load_js'	=> array(
					JS_DATATABLE, 
					JS_SELECTIZE, 
					JS_DATETIMEPICKER,
					'modules/'.PROJECT_BAS.'/'.strtolower( __CLASS__ )
				),
				'load_modal'=> array(
						'modal_realignment'	=> array(
						'controller'	=> __CLASS__,
						'module'		=> PROJECT_BAS,
						'size'			=> 'lg'
					),
				),
				'datatable' 	=> array(
					array('table_id' => 'realignment_table', 'path' => PROJECT_BAS.'/bas_realignment/get_realignment_list'),
				)
			);

			$this->template->load('bas_realignment', $data, $resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function get_realignment_list()
	{
		try 
		{
			$params = get_params();
			$cnt = 0;
		
			$fields = array(
				"realign_id", 
				"source_line_item_name", 
				"release_date", 
				"reference_no", 
				"recipient", 
				"amount", 
				"posted_flag"
			);
			$selects = array("release_date", "reference_no", "realigned_amount", "posted_flag");
		
			$realignment = $this->realignment->get_realignment_list($fields, $selects, $params);
			$iTotal = $this->realignment->total_length();
			$iFilteredTotal = $this->realignment->filtered_length($fields, $selects, $params);

			$output = array(
				"sEcho" => intval($_POST['sEcho']),
				"iTotalRecords" => $iTotal["cnt"],
				"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
				"aaData" => array()
			);
		
			foreach ($realignment as $aRow):
				$cnt++;
				$row = array();
				$action = "";

				$id	= $aRow["realign_id"];
				
				$hash_id 		= $this->hash($id); 
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;

				$counts = $this->realignment->get_realignment_count($id);
				$ctr = $counts['cnt'];

				$delete_action 	= 'content_delete("Realignment Detail", "'.$url.'")';

				for ( $i=0 ; $i<count($selects) ; $i++ )
				{
					$row[] = $aRow[ $selects[$i] ];
				}

				$recipient 	= $aRow["recipient_line_item_id"];
				$source 	= $aRow["source_line_item_id"];

				$action = "<div class='table-actions'>";
				if($this->permission_view) :
					$action .= "<a href='javascript:void(0)' class='tooltip view md-trigger tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_realignment' onclick=\"modal_init('".$url."/view')\"></a>";
				endif;

				if($aRow['posted_flag'] == STATUS_DRAFTED):
					if($this->permission_edit) :
						$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_realignment' onclick=\"modal_init('".$url."')\"></a>";
					endif;
					
					if ($this->permission_delete) : 	  
						$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					endif;
				endif;
				$action .= '</div>';
				
				$row[]	= $this->realignment->get_line_item_names($recipient);
				$row[]	= $this->realignment->get_line_item_names($source);

				$row[]	= $action;

				$output['aaData'][] = $row;

			endforeach;
		
			echo json_encode( $output );
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
			$this->get_user_message($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function modal($encoded_id, $salt, $token)
	{
		try 
		{
			$data 		= array();
			$data2 		= array();
			$resources 	= array();
			$info 		= array();
			$active 	= '';
			$to_update 	= '';
			$single 	= [];

			if(!EMPTY($encoded_id))
			{
				check_salt($encoded_id, $salt, $token);
				
				$hash_id 		= base64_url_decode($encoded_id);
				$key 			= $this->get_hash_key('realign_id'); 
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->realignment->get_specific_realignment($where);

				if(EMPTY($info)) throw new Exception($this->lang->line('err_invalid_request'));	

				$realign_id 	= $info['realign_id'];

				$data2 = $this->realignment->get_realignment_details($realign_id);

				$active 		= 'active';
				$to_update 		= sha1('true');

				
			}
			else
			{
				$hash_id 	= $this->hash(0);	
				$active = '';	
				$to_update 		= sha1('false');	
			}
			
			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			
			$data = array(
				'security' 				=> $encoded_id . '/' . $salt . '/' . $token,
				'account_codes' 		=> $this->realignment->get_dropdowns('account_codes'),
				'fund_sources'			=> $this->realignment->get_dropdowns('fund_sources'),
				'fund_year'				=> $this->realignment->get_dropdowns('fund_year'),
				'line_items'			=> $this->realignment->get_dropdowns('line_items'),
				'account_code_list'		=> $this->realignment->get_dropdowns('account_codes_list'),

				'release_date' 			=> $info['release_date'],
				'reference_no'			=> $info['reference_no'],
				'amount'				=> $data2['realigned_amount'],

				//for view selected_options
				'year' 					=> $info['year'],
				'source_type_num'		=> $info['source_type_num'],
				'source_line_item_id'	=> $data2['source_line_item_id'],
				'recipient_line_item_id'=> $data2['recipient_line_item_id'],
				'source_account_code'	=> $data2['source_account_code'],
				'recipient_account_code'=> $data2['recipient_account_code'],

				'active'				=> $active,
				'to_update'				=> $to_update,
			);

			if($this->uri->segment(7) != 'view'):
				$single = [
					'source_year' 		=> $info['year'],
					'source_type_num'	=> $info['source_type_num'],
				];
			endif;

			$resources = array(
				'load_css' 		=> array(
					CSS_SELECTIZE,
				),
				'load_js' 		=> array(
					'popModal.min', 
					JS_SELECTIZE
				),
				'loaded_init'	=> array(
					'ModalEffects.re_init();',
					'datepicker_init();',
					'selectize_init();'
				),
				//for select set option
				'single' => $single,
			);
			$this->load->view("modals/realignment_modal", $data);
			$this->load_resources->get_resource($resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}

	}

	public function get_line_items_by_fund_sources()
	{
		try
		{
			$params = get_params();
			echo json_encode($this->realignment->get_line_items_by_fund_sources($params));
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function get_fund_sources_by_year()
	{
		try
		{
			$params = get_params();
			echo json_encode($this->realignment->get_fund_sources_by_year($params));
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function get_account_codes_by_line_item()
	{
		try
		{
			$params = get_params();
			echo json_encode($this->realignment->get_account_codes_by_line_item($params));
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function get_account_codes_by_amount()
	{
		try
		{
			$params = get_params();
			echo $this->realignment->get_account_codes_by_amount($params);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function check_excess_amount()
	{
		try
		{
			$params = get_params();
			$status = $this->realignment->check_excess_amount($params);
			
			echo json_encode(['status'=>$status]);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function process()
	{
		try
		{
			$flag 	= 0;
            $error  = ERROR;
			$params = get_params();

			$this->_validate($params);
	
			$key 			= $this->get_hash_key('realign_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->realignment->get_specific_realignment($where);
			$id				= $info['realign_id'];
			
			BAS_Model::beginTransaction();

			if(EMPTY($id))
			{
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= BAS_Model::tbl_line_item_realigns;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);

				$this->realignment->insert_realignment($params);

				$activity		= "New Realignment has been added.";
				$msg  			= $this->lang->line('data_saved');
				$error 			= SUCCESS;
			}
			else
			{
				$audit_action[]	= AUDIT_UPDATE;
				$audit_table[]	= BAS_Model::tbl_line_item_realigns;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= $info;

				$params['realign_id'] =	$id;

				$this->realignment->update_realignment($params);
				
				$info2			= $this->realignment->get_specific_realignment(array("realign_id" => $id) , TRUE);
				
				$curr_detail[]	= $info2;
				
				$activity		= "Realignment has been updated.";
				$msg  			= $this->lang->line('data_updated');
				$error 			= SUCCESS;
			}
			
			$activity = sprintf($activity, $params['reference_no']);

			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
			
			BAS_Model::commit();
			$flag = 1;
			$class = $error;
		}
		catch(PDOException $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
			$this->get_user_message($e);
			
		}
		catch(Exception $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag" 			=> $flag,
			"msg" 			=> $msg,
			"error_class"	=> $error
		);
		
		echo json_encode($info);
	}
	
	private function _validate(&$params)
	{
		try
		{
			$this->_validate_security($params);

			$fields = array(
				'release_date' 			=> 'Date of Release',
				'reference_no' 			=> 'Release Number',
				'realigned_amount' 		=> 'Amount to be Realigned',
			);

			if($params['to_update'] == sha1('false'))
			{
				$fields['source_type_num']  	 = 'Fund Source';
				$fields['source_year']   		 = 'Year';
				$fields['source_line_item_id']   = 'Line Item Source';
				$fields['source_account_code'] 	 = 'Account Code Source';
				$fields['recipient_line_item_id']= 'Line Item Reciepient';
				$fields['recipient_account_code']= 'Account Code Reciepient';

				if($params['source_line_item_id'] == $params['recipient_line_item_id'])
				{
					if($params['source_account_code'] == $params['recipient_account_code'])
					{
						throw new Exception("<b>Invalid Action Detected!</b> You can't select same acount code within same Line Item");
					}
				}
			}
			else
			{
				if(!empty($params['source_line_item_id']) || !empty($params['recipient_line_item_id']) || !empty($params['source_account_code']) || !empty($params['recipient_account_code']))
				{
					$fields['source_type_num']  	 = 'Fund Source';
					$fields['source_year']   		 = 'Year';
					$fields['source_line_item_id']   = 'Line Item Source';
					$fields['source_account_code'] 	 = 'Account Code Source';
					$fields['recipient_line_item_id']= 'Line Item Reciepient';
					$fields['recipient_account_code']= 'Account Code Reciepient';

					if($params['source_line_item_id'] == $params['recipient_line_item_id'])
					{
						if($params['source_account_code'] == $params['recipient_account_code'])
						{
							throw new Exception("<b>Invalid Action Detected!</b> You can't select same acount code within same Line Item");
						}
					}
				}
			}
			
			$this->check_required_fields($params, $fields);
			return $this->_validate_input($params);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

    private function _validate_security(&$params)
    {
        try
        {
            if(EMPTY($params['security']))
            {
                throw new Exception($this->lang->line('err_invalid_request'));
            }

            $security = explode('/', $params['security']);
            $params['encoded_id']   = $security[0];
            $params['salt']         = $security[1];
            $params['token']        = $security[2];

            check_salt($params['encoded_id'], $params['salt'], $params['token']);
            $params['hash_id']      = base64_url_decode($params['encoded_id']);
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
	
	private function _validate_input($params)
	{
		try
		{
			$validation = array(
				'release_date' => array(
					'data_type' => 'date',
					'name'		=> 'release_date',
					//'max_len'	=> 50
				),
				'reference_no' => array(
					'data_type' => 'string',
					'name'		=> 'reference_no',
					'max_len'	=> 100
				),
				'source_type_num' => array(
					'data_type' => 'digit',
					'name'		=> 'source_type_num',
					'max_len'	=> 11
				),
				'source_year' => array(
					'data_type' => 'digit',
					'name'		=> 'source_year',
					'max_len'	=> 4
				),
				'source_line_item_id' => array(
					'data_type' => 'digit',
					'name'		=> 'source_line_item_id',
					'max_len'	=> 11
				),
				'source_account_code' => array(
					'data_type' => 'digit',
					'name'		=> 'source_account_code',
					'max_len'	=> 11
				),
				'recipient_line_item_id' => array(
					'data_type' => 'digit',
					'name'		=> 'recipient_line_item_id',
					'max_len'	=> 11
				),
				'recipient_account_code' => array(
					'data_type' => 'digit',
					'name'		=> 'recipient_account_code',
					'max_len'	=> 11
				),
				'realigned_amount' => array(
					'data_type' => 'float',
					'name'		=> 'realigned_amount',
					'max_len'	=> 11
				)
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function delete_realignment($params)
	{
		try
		{
			$flag	= 0;
			$msg	= "Error";

			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->validate_security($params);

			$key 			= $this->get_hash_key('realign_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->realignment->get_specific_realignment($where);
			
			if(EMPTY($info['realign_id']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			$this->realignment->delete_realignment($info['realign_id']);

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			$msg	= $e->getMessage(); 
			$this->get_user_message($e);	
		}
		catch(Exception $e)
		{	
			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag" 		=> $flag,
			"msg" 		=> $msg,
			"reload" 	=> 'datatable',
			"table_id" 	=> 'realignment_table',
			"path"		=> PROJECT_BAS . '/bas_realignment/get_realignment_list/'
		);

		echo json_encode($info);
	}

}