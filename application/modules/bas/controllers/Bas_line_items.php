<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bas_line_items extends BAS_Controller 
{
	private $module = MODULE_BAS_LINE_ITEMS;

	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;

	public function __construct()
	{
		parent::__construct();

		$this->load->model('bas_line_items_model','line_items');

		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);
	}

	public function index()
	{

		try
		{
			// CONSTRUCT SECURITY VARIABLES
			$hash_id 		= $this->hash(0); 
			$encoded_id 	= base64_url_encode($hash_id);
			$salt 			= gen_salt();			
			$token 			= in_salt($encoded_id, $salt);			
			$security 		= $encoded_id."/".$salt."/".$token;

			$resources = array(
				'load_css' => array(
					CSS_MODAL_COMPONENT, 
					CSS_SELECTIZE,
					CSS_DATETIMEPICKER, 
					CSS_LABELAUTY,
					CSS_DATATABLE,
					'modules/bas'
				),
				'load_js'=> array(
					JS_SELECTIZE, 
					JS_DATETIMEPICKER, 
					JS_LABELAUTY,
					JS_DATATABLE,
					'add_row',
					'modules/'.PROJECT_BAS.'/'.strtolower( __CLASS__ )
				),
				'loaded_init' 	=> 	array(
					'ModalEffects.re_init();',
					'selectize_init();',
					"$('ul.tabs').tabs();",
				),
				'load_modal'=> array(
					'modal_account_code' => array(
						'controller' => __CLASS__,
						'method'	 => 'account_code_modal',
						'module' 	 => PROJECT_BAS,
						'multiple'   => true,
						'size'		 => 'lg',
						'height' 	 => '400px;'
					),
					'modal_office' 	 => array(
						'controller' => __CLASS__,
						'method'	 => 'office_modal',
						'module'	 => PROJECT_BAS,
						'multiple'   => true,
						'size'		 => 'lg',
						'height'	 => '500px;'
					),
					'modal_allotment_releases'	=> array(
						'controller' => __CLASS__,
						'method'	 => 'allotment_releases_modal',
						'module'	 => PROJECT_BAS,
						'multiple'	 => true,
						'size'		 => 'xl',
						'height'	 => '400px;'
					)
				)
			);

			$data = array(
				'security'				=> $security,
				'param_source_types' 	=> $this->line_items->get_param_source_types_list(),
			);

			$this->template->load( 'bas_line_items', $data, $resources );
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}

	}

	public function delete_data()
	{
		try
		{

			$params = get_params();

			$identifier = $this->uri->segment(4);
			$id = $this->uri->segment(5);
			$item = $this->uri->segment(6);

			if($identifier == 'account_code')
			{
				$a = $this->line_items->delete_account_code($id, $item);
				echo $a;
			}
			elseif($identifier == 'office')
			{
				$a = $this->line_items->delete_office($id, $item);
				echo $a;
			}
			elseif($identifier == 'release_details')
			{
				$line_item_id = $this->line_items->delete_release_details($id, $this->uri->segment(6));
				echo $line_item_id;
			}
			elseif($identifier == 'release')
			{
				$a = $this->line_items->delete_releases($params['param_1']);
				$info = array(
					"flag" 		=> 1,
					"msg" 		=> 'Data deleted',
					"reload" 	=> 'datatable',
					"table_id" 	=> 'allotment_release_list_table',
					"path"		=> PROJECT_BAS . '/bas_line_items/get_allotment_release_list/'.$params['param_2'].''
				);
	
				echo json_encode($info);
			}
			elseif($identifier == 'delete_sub_line_items')
			{
				$action = $this->uri->segment(6);
				$row = $this->uri->segment(7);
				if($action == sha1('delete_data_'.$row.'_'.$id.''))
				{

					$a = $this->line_items->delete_sub_line_items($id);
					echo $a;
				} else echo 'Operation Not Permitted.';
			}
			else show_404();
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
			echo 2;
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
			echo 2;
		}

	}

	public function load_override_tables()
	{
		try
		{
			$code = PARAM_ACCOUNT_CODE_PS;
			$uri = $this->uri->segment(6);

			if(isset($uri)):

				if($uri == 'ps')
				{
					$code = PARAM_ACCOUNT_CODE_PS;
				}
				if($uri == 'mooe')
				{
					$code = PARAM_ACCOUNT_CODE_MOOE;
				}
				if($uri == 'co')
				{
					$code = PARAM_ACCOUNT_CODE_CO;
				}

			endif;

			$data = array(
				'view' 				=> $this->uri->segment(4),
				'count' 			=> $this->uri->segment(5),
				'account_codes' 	=> $this->line_items->get_account_codes($code),
				'all_account_codes' => $this->line_items->get_all_account_codes(),
				'line_item_office'	=> $this->line_items->get_line_item_office(),
			);
			$resources['loaded_init'] = array(
				"selectize_init();",
			);

			$this->load->view('tables/override_actions_table', $data);
			$this->load_resources->get_resource($resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function find_line_items()
	{
		try
		{
			$info = $this->line_items->check_if_exists_fund_sources($this->uri->segment(4), $this->uri->segment(5));
			
			$data = array(
				'line_items' 	=> $info,
				'rows' 			=> sizeof($info),
				'count'			=> $_GET['count'],
				'parent'		=> $_GET['parent']
			);
			
			$resources = array(
				'loaded_init' 	=> array(
					'ModalEffects.re_init();'
				),
			);

			$this->load->view( 'tables/line_items_table', $data);
			$this->load_resources->get_resource( $resources );
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function check_if_final()
	{
		try
		{
			$params = get_params();

			$this->line_items->check_if_final($params['year'], $params['source']);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function check_line_items()
	{
		try
		{
			$params = get_params();
			$this->line_items->check_line_items($params['year'], $params['source']);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function get_line_items()
	{
		try
		{
			$params = get_params();

			$resources 	= array(
				'load_css'	=> array(
					CSS_DATATABLE,
					CSS_SELECTIZE,
					CSS_MODAL_COMPONENT,
					'selectize.default'
				),
				'load_js' 	=> array(
					JS_DATATABLE,
					JS_SELECTIZE,
					'popModal.min', 'selectize'
				),
				'loaded_init' 	=> 	array(
					'ModalEffects.re_init();',
					'selectize_init();',
					"$('ul.tabs').tabs();",
				)
			);
			
			$this->line_items->set_as_final_line_items($params['year'], $params['source']);

			$info = $this->line_items->get_line_items($params['year'], $params['source']);
			$data = array(
				'data' 		=> $info,
				'rows' 		=> sizeof($info),
			);

			$this->load->view('tables/overried_line_items_table', $data);
			$this->load_resources->get_resource( $resources );
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function process($encoded_id, $salt, $token)
	{
		try
		{	
			$flag 	= 0;
            $class  = ERROR;
            $msg 	= '';
            $status = '';

			$params = get_params();

			$this->_validate($params);

			$row 		= $this->uri->segment(4);
			$action 	= $this->uri->segment(5);
			$id 		= $this->uri->segment(6);
			$update_key = '';
			$delete_key = '';


			BAS_Model::beginTransaction();

			$check = $this->line_items->check_if_exists_fund_sources_data($params['line_item_year'], $params['source_type']);
			if(empty($check))
			{
				$this->line_items->insert_fund_source($params);
			}

			if($action == sha1('insert_data_'.$row.''))
			{
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= BAS_Model::tbl_line_items;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);

				$activity	= "%s has been added.";
				$activity 	= sprintf($activity, $params['line_item_'.$row.'']);

				$id = $this->line_items->insert_line_items($params, $row);

				$status 		= $id;
				$update_key 	= sha1('update_data_'.$id.'_'.$row.'');
				$delete_key		= sha1('delete_data_'.$row.'_'.$id.'');

				$msg  	= $this->lang->line('data_saved');
				$error 	= SUCCESS;
				$flag 	= 1;
			}
			elseif($action == sha1('update_data_'.$id.'_'.$row.''))
			{
				$info	= $this->line_items->get_specific_line_item_for_audit_trail($id);

				$audit_action[]	= AUDIT_UPDATE;
				$audit_table[]	= BAS_Model::tbl_line_items;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= array($info);

				$this->line_items->update_line_items_data($params, $row , $id);

				$info	= $this->line_items->get_specific_line_item_for_audit_trail($id);

				$curr_detail[]	= array($info);
				
				$activity	= "%s has been updated.";
				$activity 	= sprintf($activity, $params['line_item_'.$row.'']);

				$status = 'updated';
				$msg  	= $this->lang->line('data_updated');
				$error 	= SUCCESS;
				$flag 	= 1;
			}
			else
			{
				$status = '';
				$msg  	= 'Action Not Permitted!';
				$error 	= ERROR;
				$flag 	= 0;
			}

			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
			
			BAS_Model::commit();

		}
		catch( PDOException $e )
		{
			$msg = $this->rlog_error($e, TRUE);
			$this->get_user_message($e);
		}
		catch( Exception $e )
		{
			$msg = $this->rlog_error($e, TRUE);
		}

		$info = array(
			"flag" 			=> $flag,
			"msg" 			=> $msg,
			"error_class"	=> $error,
			"status"		=> $status,
			"update_key"	=> $update_key,
			"delete_key"	=> $delete_key
		);
		
		echo json_encode($info);
	}

	public function process_account_code($encoded_id, $salt, $token)
	{
		try
		{
			$flag 	= 0;
            $class  = ERROR;
            $msg 	= '';
            $status = '';

			$params = get_params();

			$this->_validate($params);

			BAS_Model::beginTransaction();

			$code 		 	= $params['account_code'];
			$update_code 	= $params['update_account_code'];

			$info 			= $this->line_items->get_specific_account_codes($params);

			if(ISSET($code)):

				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= BAS_Model::tbl_line_item_accounts;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);

				$activity		= "New Account code has been Added.";

				$this->line_items->process_account_codes($params);

				$msg			= 'Record was successfully saved.';
				$flag 			= 1;
				$class 			= SUCCESS;

			endif;

			if(ISSET($update_code)):

				$audit_action[]	= AUDIT_UPDATE;
				$audit_table[]	= BAS_Model::tbl_line_item_accounts;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= $info;

				$activity		= "Account codes has been updated.";

				$info 			= $this->line_items->get_specific_account_codes($params);

				$this->line_items->process_update_account_code($params);
				
				$curr_detail[]	= $info;

				$msg  			= 'Record was successfully was saved.';
				$flag 			= 1;
				$class 			= SUCCESS;

			endif;

			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
			
			BAS_Model::commit();
		}
		catch( PDOException $e )
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
			$this->get_user_message($e);
		}
		catch( Exception $e )
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}

		$info = array(
			"flag" 			=> $flag,
			"msg" 			=> $msg,
			"error_class"	=> $class,
			"status"		=> $status
		);
		
		echo json_encode($info);
	}

	public function process_allotment_release($encoded_id, $salt, $token)
	{
		try
		{
			$flag 	= 0;
            $class  = ERROR;
            $msg 	= '';
            $status = '';

			$params = get_params();

			$this->_validate($params);

			BAS_Model::beginTransaction();

			$key 			= 'release_id';
			$not_hash_id 	= (!empty($params['id'])) ? $params['id'] : '0';

			$info 			= $this->line_items->get_specific_release($key, $not_hash_id);
			
			foreach($info as $key => $val)
			{
				$id 			= $val['release_id'];
				$line_item_id 	= $val['line_item_id'];
				$line_item_name = $val['line_item_name'];
				$reference_no 	= $val['reference_no'];
				$reference_date = $val['release_id'];
			}

			if(isset($id)) $id;else $id = '';

			if(EMPTY($id))
			{
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= BAS_Model::tbl_line_item_releases;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);

				$line_item_id 	= $this->line_items->process_release($params);

				$activity		= "%s has been added.";
				$activity 		= sprintf($activity, $params['reference_no']);

				$msg  = 'Record was successfully saved.';
				$flag = 1;
				$class = SUCCESS;
			}
			else
			{

				$audit_action[]	= AUDIT_UPDATE;
				$audit_table[]	= BAS_Model::tbl_line_item_releases;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= array($info);

				$this->line_items->update_release($params);
				
				$info			= $this->line_items->get_specific_release($key, $id);

				foreach($info as $key => $val)
				{
					$id 			= $val['release_id'];
					$line_item_id 	= $val['line_item_id'];
					$line_item_name = $val['line_item_name'];
					$reference_no 	= $val['reference_no'];
					$reference_date = $val['release_id'];
				}
				$curr_detail[]	= array($info);
				
				$activity	= "%s has been updated.";

				$activity 	= sprintf($activity, $reference_no);

				$msg  = 'Record was successfully updated.';
				$flag = 1;
				$class = SUCCESS;
			}

			$activity = sprintf($activity, $params['release_number']);

			$status = $line_item_id;

			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
			
			BAS_Model::commit();
		}
		catch( PDOException $e )
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
			$this->get_user_message($e);
		}
		catch( Exception $e )
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}

		$info = array(
			"flag" 			=> $flag,
			"msg" 			=> $msg,
			"error_class"	=> $class,
			"status"		=> $status
		);
		
		echo json_encode($info);
	}

	public function process_office()
	{
		try
		{
			$flag 				= 0;
            $error  			= ERROR;
            $msg 				= '';
            $status 			= '';

			$params = get_params();

			$this->_validate($params);

			//custom  validate total amount to avoid exceed on the line items
			$this->_validate_amount($params);

			BAS_Model::beginTransaction();

			$id 				= $params['line_item_id'];
			$office				= $params['office'];
			$update_office		= $params['update_office'];

			if(isset($update_office))
			{
				$info 			= $this->line_items->get_specific_office($id);

				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= BAS_Model::tbl_line_item_offices;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= $info;

				$this->line_items->process_update_office($params);

				$info2 = $this->line_items->get_specific_office($id);

				$curr_detail[]	= $info2;

				$activity		= "Offices has been Updated.";

				$msg  			= 'Record successfully updated.';
				$error 			= SUCCESS;
				$flag  			= 1;
			}

			if(isset($office))
			{
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= BAS_Model::tbl_line_item_offices;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= array();

				$this->line_items->process_office($params);

				$curr_detail[]	= $params;

				$activity		= "New office has been added.";

				$msg  			= 'Record successfully saved.';
				$error 			= SUCCESS;
				$flag  			= 1;
			}

			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
			
			BAS_Model::commit();
		}
		catch(PDOException $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
			$this->get_user_message($e);
		}
		catch(Exception $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}

		$info = array(
			"flag" 			=> $flag,
			"msg" 			=> $msg,
			"error_class"	=> $error,
			"status"		=> $status
		);
		
		echo json_encode($info);
	}

	public function account_code_modal()
	{
		try
		{
			$params = get_params();

			$resources 	= array(
				'load_css'	=> array(
					CSS_DATATABLE,
					CSS_SELECTIZE,
					CSS_MODAL_COMPONENT,
					'selectize.default'
				),
				'load_js' 	=> array(
					JS_DATATABLE,
					JS_SELECTIZE,
					'popModal.min', 'selectize'
				),
				'loaded_init' 	=> 	array(
					'ModalEffects.re_init();',
					'selectize_init();',
					"$('ul.tabs').tabs();",
				)
			);

			$line_item = $this->line_items->get_line_item_name($params);

			foreach($line_item as $key => $va)
			{
				$line_item_name	= $va['line_item_name'];
				$line_item_id 	= $va['line_item_id'];
				$year 			= $va['year'];
				$source 		= $va['source_type_name'];
				$code 			= $va['line_item_code'];
			}

			// CONSTRUCT SECURITY VARIABLES
			$hash_id 		= $this->hash($line_item_id); 
			$encoded_id 	= base64_url_encode($hash_id);
			$salt 			= gen_salt();			
			$token 			= in_salt($encoded_id, $salt);			
			$security 		= $encoded_id."/".$salt."/".$token;

			$data = array(
				'account_codes' 	=> $this->line_items->get_account_codes(PARAM_ACCOUNT_CODE_PS),
				'line_item_codes'	=> $this->line_items->get_line_items_account_codes(PARAM_ACCOUNT_CODE_PS, $line_item_id),
				'line_item_name' 	=> $line_item_name,
				'line_item_id' 		=> $line_item_id,
				'year' 				=> $year,
				'source' 			=> $source,
				'code' 				=> $code,
				'security'			=> $security
			);

			$this->load->view( 'modals/account_code_modal', $data );
			$this->load_resources->get_resource($resources);

		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}


	public function office_modal()
	{
		try
		{
			$params = get_params();

			$resources 	= array(
				'load_css'	=> array(
					CSS_DATATABLE,
					CSS_SELECTIZE,
					CSS_MODAL_COMPONENT,
					'selectize.default'
				),
				'load_js' 	=> array(
					JS_DATATABLE,
					JS_SELECTIZE,
					'popModal.min', 'selectize'
				),
				'loaded_init' 	=> 	array(
					'ModalEffects.re_init();',
					'selectize_init();',
				)
			);

			$line_item = $this->line_items->get_line_item_name($params);

			foreach($line_item as $key => $va)
			{
				$line_item_name	= $va['line_item_name'];
				$line_item_id 	= $va['line_item_id'];
				$year 			= $va['year'];
				$source 		= $va['source_type_name'];
				$code 			= $va['line_item_code'];
			}

			// CONSTRUCT SECURITY VARIABLES
			$hash_id 		= $this->hash($line_item_id); 
			$encoded_id 	= base64_url_encode($hash_id);
			$salt 			= gen_salt();			
			$token 			= in_salt($encoded_id, $salt);			
			$security 		= $encoded_id."/".$salt."/".$token;

			$data = array(
				'line_item_name' 	=> $line_item_name,
				'line_item_id' 		=> $line_item_id,
				'year' 				=> $year,
				'code' 				=> $code,
				'source' 			=> $source,
				'offices'			=> $this->line_items->get_offices($params['id']),
				'offices_on_ob'		=> $this->line_items->get_offices_on_ob(),
				'line_item_office'	=> $this->line_items->get_line_item_office(),
				'security'			=> $security
			);

			$this->load->view( 'modals/office_modal', $data );
			$this->load_resources->get_resource($resources);
		}

		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function allotment_releases_modal()
	{
		try
		{
			$params = get_params();

			$resources 	= array(
				'load_css'	=> array(
					CSS_DATATABLE,
					CSS_SELECTIZE,
					CSS_MODAL_COMPONENT
				),
				'load_js' 	=> array(
					JS_DATATABLE,
					JS_SELECTIZE
				),
				'load_modal' => array(
					'modal_allotment_release' => array(
						'controller'		=> __CLASS__,
						'method'			=> 'allotment_release_list_modal',
						'module'			=> PROJECT_BAS,
						'multiple'	 		=> true,
						'size'		 		=> 'lg',
						'height'			=> '500px;'
					),
				),
				'datatable' => array(
					array('table_id' 	=> 'allotment_release_list_table', 'path' => PROJECT_BAS.'/bas_line_items/get_allotment_release_list/'.$params['id'].''),
				),
			);

			$line_item = $this->line_items->get_line_item_name($params);

			foreach($line_item as $key => $va)
			{
				$line_item_name	= $va['line_item_name'];
				$line_item_id 	= $va['line_item_id'];
				$year 			= $va['year'];
				$source 		= $va['source_type_name'];
				$code 			= $va['line_item_code'];
			}

			// CONSTRUCT SECURITY VARIABLES
			$hash_id 		= $this->hash($line_item_id); 
			$encoded_id 	= base64_url_encode($hash_id);
			$salt 			= gen_salt();			
			$token 			= in_salt($encoded_id, $salt);			
			$security 		= $encoded_id."/".$salt."/".$token;

			$data = array(
				'line_item_name' 	=> $line_item_name,
				'line_item_id' 		=> $line_item_id,
				'year' 				=> $year,
				'code' 				=> $code,
				'source' 			=> $source,
				'all_account_codes' => $this->line_items->get_all_account_codes(),
				'security'			=> $security
			);

			$this->load->view( 'modals/allotment_release_list_modal', $data);
			$this->load_resources->get_resource( $resources );
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function allotment_release_list_modal($encoded_id, $salt, $token)
	{
		try
		{
			$params = get_params();
			$data 		= array();
			$resources 	= array();
			$info = '';

			// CHECK IF THE ACTION IS UPDATE/INSERT
			if(!EMPTY($encoded_id))
			{
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);

				$hash_id 		= base64_url_decode($encoded_id);
				$key 			= $this->get_hash_key('release_id'); 

				$info = $this->line_items->get_specific_release($key, $hash_id);

				if(EMPTY($info)) throw new Exception($this->lang->line('err_invalid_request'));	

			    foreach ($info as $key => $value) {
			        $line_item_id 	= $value['line_item_id']; 
			        $release_id 	= $value['release_id']; 
			        $line_item_name = $value['line_item_name']; 
			        $reference_no	= $value['reference_no']; 
			        $reference_date = $value['reference_date']; 
			    }

			    $active= 'active';
			}
			else
			{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);	
			}

			// CONSTRUCT SECURITY VARIABLES
			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$security			= $encoded_id . '/' . $salt . '/' . $token;

			$resources 	= array(
				'loaded_init'	=> array(
					'datepicker_init();',
					'selectize_init();',
				),
				'load_css' 		=> array(
					'selectize.default'
				),
				'load_js' 		=> array(
					'popModal.min', 
					'selectize'
				),
			);

			$params['id'] = ($this->uri->segment(7)) ? $this->uri->segment(7) : $params['id'];

			$line_item 	= $this->line_items->get_line_item_name($params);

			foreach($line_item as $key => $va)
			{
				$line_item_name	= $va['line_item_name'];
				$line_item_id 	= $va['line_item_id'];
				$year 			= $va['year'];
				$source 		= $va['source_type_name'];
				$code 			= $va['line_item_code'];
			}

			$details 	= $this->line_items->get_release_details($release_id);

			$data = array(
				'line_item_name' 	=> $line_item_name,
				'line_item_id' 		=> $line_item_id,
				'source' 			=> $source,
				'year' 				=> $year,
				'code' 				=> $code,
				'reference_date'	=> $reference_date,
				'reference_no'		=> $reference_no,
				//'account_codes' 	=> $this->line_items->get_all_account_codes_on_line_item_accounts(),
				'all_account_codes' => $this->line_items->get_all_account_codes_2(),
				'security'			=> $security,
				'data'				=> $info,
				'active'			=> $active,
				'details'			=> $details,
				'hash_id'			=> $hash_id,
				'release_id'		=> $release_id
			);

			$this->load->view('modals/allotment_release_modal',$data);
			$this->load_resources->get_resource($resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}

	}

	public function get_allotment_release_list()
	{
		try
		{
			$params = get_params();
			$cnt = 0;
		
			$fields = array(
				"release_id", 
				"line_item_id", 
				"reference_date", 
				"reference_no", 
				"FORMAT(total_release_amount, 0) total_release_amount", 
			);
			$selects = array(
				"reference_date", 
				"reference_no", 
				"total_release_amount", 
			);

			$line_item_id = $this->uri->segment(4);
		
			$releases 		= $this->line_items->get_allotment_release_amount_list($fields, $selects, $params, $line_item_id);
			$iTotal 		= $this->line_items->total_length($line_item_id);
			$iFilteredTotal = $this->line_items->filtered_length($fields, $selects, $params, $line_item_id);

			$output = array(
				"sEcho" => intval($_POST['sEcho']),
				"iTotalRecords" => $iTotal["cnt"],
				"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
				"aaData" => array()
			);
		
			foreach ($releases as $aRow):
				$cnt++;
				$row = array();
				$action = "";
			
				// PRIMARY KEY
				$id	= $aRow["release_id"];
				$line_item_id	= $aRow["line_item_id"];
				
				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash($id); 
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("Allotment Release ", "'.$id.'", "'.$line_item_id.'")';
		
				for ( $i=0 ; $i<count($selects) ; $i++ )
				{
					$row[] = $aRow[ $selects[$i] ];
				}

				$action  = "<div class='table-actions'>";
				if($this->permission_view) :
					$action .= "<a href='javascript:void(0)' class='tooltip view md-trigger tooltipped view_data' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_allotment_release' onclick=\"modal_allotment_release_init('".$url."/view')\"></a>";
				endif;

				//ACTIVATE FOR PORPOSE-------------------------------------------------------------------------
				/*if($this->permission_edit) :
					$action .= "<a href='javascript:void(0)' class='tooltip edit md-trigger tooltipped edit_data' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_allotment_release' onclick=\"modal_allotment_release_init('".$url."/".$line_item_id."')\"></a>";
				endif;
				if($this->permission_delete) :
					$action .= "<a href='javascript:void(0)' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50' onclick='".$delete_action."'></a>";
				endif;*/
				
				$action .= "</div>";
				$row[] = $action;	
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
			$this->get_user_message($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function get_tabs()
	{
		try{

			$params 	= get_params();

			$tab 		= $this->uri->segment(4);

			$content 	= '';
			$code 		= '';

			$resources 	= array(
				'loaded_init' => 	array(
						'selectize_init();',
						"$('ul.tabs').tabs();",
				)
			);
			
			if($tab == 'ps')
			{
				$code 		= PARAM_ACCOUNT_CODE_PS;
				$content 	= 'tabs/line_items_ps_tabs';
			}
			if($tab == 'mooe') 
			{
				$code 		= PARAM_ACCOUNT_CODE_MOOE;
				$content 	= 'tabs/line_items_mooe_tabs';
			}
			if($tab == 'co')
			{
				$code 		= PARAM_ACCOUNT_CODE_CO;
				$content 	= 'tabs/line_items_coo_tabs';
			}

			$data = array(
				'account_codes' 	=> $this->line_items->get_account_codes($code),
				'line_item_codes'	=> $this->line_items->get_line_items_account_codes($code, $params['id']),
				'line_item_id' 		=> $params['id'] 
			);

			$this->load->view($content, $data);
			$this->load_resources->get_resource($resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}

	}

	public function check_total_amount()
	{
		try
		{
			$params 	= get_params();
			$data 		= [];
			$message 	= '';
			$status 	= 0;
			$identity  	= $this->uri->segment('4');

			$checker 	= $this->line_items->check_total_amount($params, $identity);

			$appro 			= array_sum(str_replace(',', '', $params['appropriation_amount']));
			$update_appro 	= array_sum(str_replace(',', '', $params['update_appropriation_amount']));
			$allo  			= array_sum(str_replace(',', '', $params['allotment_amount']));
			$update_allo  	= array_sum(str_replace(',', '', $params['update_allotment_amount']));
			
			if(count($checker) != 0)
			{
				foreach ($checker as $key => $value){}

				if($identity == 'account_code')
				{
					if($value[''.$params['uri'].'_appropriation'] <= ($appro + $update_appro))
					{
						$message =  'Total Appropriation cannot exceed on the line items '.$params['uri'].' appropriation amount of '.number_format($value[''.$params['uri'].'_appropriation']).'';
						$status  = 0;
					}
					elseif($value[''.$params['uri'].'_allotment'] <= ($allo + $update_allo))
					{
						$message =  'Total Allotment cannot exceed on the line items '.$params['uri'].' allotment amount of '.number_format($value[''.$params['uri'].'_allotment']).'';
						$status  = 0;
					}
					else
					{
						$message =  '';
						$status  = 1;
					}
				}
				else
				{
					if($value['total_appropriation'] <= ($appro + $update_appro))
					{
						$message =  'Total Appropriation cannot exceed on the line items Total appropriation amount of '.number_format($value['total_appropriation']).'';
						$status  = 0;
					}
					elseif($value['total_allotment'] <= ($allo + $update_allo))
					{
						$message =  'Total Allotment cannot exceed on the line items Total allotment amount of '.number_format($value['total_allotment']).'';
						$status  = 0;
					}
					else
					{
						$message =  '';
						$status  = 1;
					}
				}
			}
			else
			{
				$message =  '';
				$status  = 1;
			}

			$data = [
				'message' 	=> $message,
				'status' 	=> $status
			];
			
			echo json_encode($data);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	private function _validate($params)
	{
		try		
		{
			$this->_validate_security($params);

			$fields = [];

			switch($params['validation']){

				case 'account_code':

						if(isset($params['appropriation_amount'])):
							$fields = [
								'appropriation_amount' 	=> 'Appropriation Amount',
								'allotment_amount' 		=> 'Allotment Amount',
								'account_code' 			=> 'Account Code'
							];
						endif;

						if(isset($params['update_appropriation_amount'])):
							$fields = [
								'update_appropriation_amount' 	=> 'Appropriation Amount',
								'update_allotment_amount' 		=> 'Allotment Amount',
								'update_account_code' 			=> 'Account Code'
							];
						endif;

						$codes = array_merge($params['account_code'], $params['update_account_code']);
						
						foreach(array_count_values($codes) as $key => $val)
						{
							if($val == 2) throw new Exception("<b>Invalid Action Detected!</b> Please select different account code.");
						}

					break;

				case 'office':
					
						if(isset($params['office'])):
							$fields = [
								'office' 				=> 'Office',
								'allotment_amount' 		=> 'Allotment Amount',
								'appropriation_amount' 	=> 'Appropriation Amount'
							];
						endif;

						if(isset($params['update_office'])):
							$fields = [
								'update_office' 				=> 'Office',
								'update_allotment_amount' 		=> 'Allotment Amount',
								'update_appropriation_amount' 	=> 'Appropriation Amount'
							];
						endif;

						$offices = array_merge($params['office'], $params['update_office']);

						foreach(array_count_values($offices) as $key => $val)
						{
							if($val == 2) throw new Exception("<b>Invalid Action Detected!</b> Please select different office.");
						}

					break;

				case 'allotment_release':
					
						if(isset($params['account_code'])):
							$fields = [
								'release_date' 		=> 'Release Date',
								'release_number' 	=> 'Release Number',
								'allotment_amount' 	=> 'Allotment Amount',
								'account_code' 		=> 'Account Code'
							];
						endif;

						if(isset($params['update_account_code'])):
							$fields = [
								'release_date' 				=> 'Release Date',
								'release_number' 			=> 'Release Number',
								'update_allotment_amount' 	=> 'Allotment Amount',
								'update_account_code' 		=> 'Account Code'
							];
						endif;

						$codes = array_merge($params['account_code'], $params['update_account_code']);
						
						foreach(array_count_values($codes) as $key => $val)
						{
							if($val == 2) throw new Exception("<b>Invalid Action Detected!</b> Please select different account code.");
						}

					break;
				
				default:
					
					$row = $this->uri->segment(4);

					$fields = [
						'line_item_year' 	=> 'Year',
						'source_type'		=> 'Type of Source',
					];

					$fields = [
						'code_'.$row.'' 		=> 'Code',
						'line_item_'.$row.'' 	=> 'Line Item Name'
					];

					//validate only when
					if(isset($params['is_parent_level_'.$row.'']) &&  $params['is_parent_level_'.$row.''] != '1')
					{
						if(isset($params['ps_appropriation_amount_'.$row.''])) $fields['ps_appropriation_amount_'.$row.''] 	= 'Ps Appropriation';
						if(isset($params['mooe_appropriation_amount_'.$row.''])) $fields['mooe_appropriation_amount_'.$row.''] 	= 'Mooe Appropriation';
						if(isset($params['coo_appropriation_amount_'.$row.''])) $fields['coo_appropriation_amount_'.$row.''] 	= 'Co Appropriation';
						if(isset($params['ps_allotment_amount_'.$row.''])) $fields['ps_allotment_amount_'.$row.''] 		= 'Ps Allotment';
						if(isset($params['mooe_allotment_amount_'.$row.''])) $fields['mooe_allotment_amount_'.$row.''] 	= 'Mooe Allotment';
						if(isset($params['coo_allotment_amount_'.$row.''])) $fields['coo_allotment_amount_'.$row.''] 	= 'Co Allotment';
					}

				break;
			}

			$this->check_required_fields($params, $fields);

			return $this->_validate_input($params);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_security(&$params)
    {
        try
        {
            if(EMPTY($params['security']))
            {
                throw new Exception($this->lang->line('err_invalid_request'));
            }


            $security = explode('/', $params['security']);

            $params['encoded_id']   = $security[0];
            $params['salt']         = $security[1];
            $params['token']        = $security[2];

            check_salt($params['encoded_id'], $params['salt'], $params['token']);

            $params['hash_id']      = base64_url_decode($params['encoded_id']);
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
	
	private function _validate_input($params)
	{
		try
		{
			//line item per row validation input
			$row = $this->uri->segment(5);

			$validation = [
				'line_item_year' => [
					'data_type' => 'integer',
					'name'		=> 'line_item_year',
					'max_len'	=> 11
				],
				'source_type' => [
					'data_type' => 'integer',
					'name'		=> 'source_type',
					'max_len'	=> 11
				],
				'code_'.$row.'' => [
					'data_type' => 'string',
					'name'		=> 'code_'.$row.'',
					'max_len'	=> 100
				],
				'line_item_'.$row.'' => [
					'data_type' => 'string',
					'name'		=> 'line_item_'.$row.'',
					'max_len'	=> 100
				],
				'ps_appropriation_amount_'.$row.'' 	=> [
					'data_type' => 'amount',
					'name'		=> 'ps_appropriation_amount_'.$row.'',
					//'max_len'	=> 11
				],
				'mooe_appropriation_amount_'.$row.'' => [
					'data_type' => 'amount',
					'name'		=> 'mooe_appropriation_amount_'.$row.'',
					//'max_len'	=> 11
				],
				'coo_appropriation_amount_'.$row.'' => [
					'data_type' => 'amount',
					'name'		=> 'coo_appropriation_amount_'.$row.'',
					//'max_len'	=> 11
				],
				'ps_allotment_amount_'.$row.'' 	=> [
					'data_type' => 'amount',
					'name'		=> 'ps_allotment_amount_'.$row.'',
					//'max_len'	=> 11
				],
				'mooe_allotment_amount_'.$row.'' => [
					'data_type' => 'amount',
					'name'		=> 'mooe_allotment_amount_'.$row.'',
					//'max_len'	=> 11
				],
				'coo_allotment_amount_'.$row.'' => [
					'data_type' => 'amount',
					'name'		=> 'coo_allotment_amount_'.$row.'',
					//'max_len'	=> 11
				],

				//allotment release validation
				'release_date' => [
					'data_type' => 'string',
					'name'		=> 'release_date',
					'max_len'	=> 50
				],
				'release_number' => [
					'data_type' => 'string',
					'name'		=> 'release_number',
					'max_len'	=> 50
				],
				'allotment_amount' => [
					'data_type' => 'amount',
					'name'		=> 'allotment_amount',
					//'max_len'	=> 11
				],
				'account_code' => [
					'data_type' => 'string',
					'name'		=> 'account_code',
					'max_len'	=> 50
				],

				//account code validation
				'appropriation_amount' => [
					'data_type' => 'amount',
					'name'		=> 'account_code',
					'max_len'	=> 11
				],
				'allotment_amount' => [
					'data_type' => 'amount',
					'name'		=> 'account_code',
					'max_len'	=> 11
				],
				'update_allotment_amount' => [
					'data_type' => 'amount',
					'name'		=> 'account_code',
					'max_len'	=> 11
				],
				'update_allotment_amount' => [
					'data_type' => 'amount',
					'name'		=> 'account_code',
					'max_len'	=> 11
				],

				//offfice
				'office' => [
					'data_type' => 'string',
					'name'		=> 'office',
					'max_len'	=> 50
				],
				'mooe_amount' => [
					'data_type' => 'amount',
					'name'		=> 'mooe_amount',
					//'max_len'	=> 11
				],
				'update_office' => [
					'data_type' => 'string',
					'name'		=> 'update_office',
					'max_len'	=> 50
				],
				'update_mooe_amount' => [
					'data_type' => 'amount',
					'name'		=> 'update_mooe_amount',
					//'max_len'	=> 11
				],
			];
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function _validate_amount($params)
	{
		$id = $params['line_item_id'];

		$office							= $params['office'];
		$allotment_amount 				= (is_numeric($params['allotment_amount'])) ? $params['allotment_amount'] : str_replace(',','', $params['allotment_amount']);
		$appropriation_amount 			= (is_numeric($params['appropriation_amount'])) ? $params['appropriation_amount'] : str_replace(',','', $params['appropriation_amount']);

		$old_office 					= $params['old_office'];
		$update_office 					= $params['update_office'];
		$update_allotment_amount 		= (is_numeric($params['update_allotment_amount'])) ? $params['update_allotment_amount'] : str_replace(',','', $params['update_allotment_amount']);
		$update_appropriation_amount 	= (is_numeric($params['update_appropriation_amount'])) ? $params['update_appropriation_amount'] : str_replace(',','', $params['update_appropriation_amount']);

		//check mooe total on the line items before doing the process
		$gathered_total_allotment 		= 0;
		$gathered_total_appropriation 	= 0;

		if(ISSET($office)):
			foreach($office as $key => $val)
			{
				$gathered_total_allotment 		+= $allotment_amount[$key];
				$gathered_total_appropriation 	+= $appropriation_amount[$key];
			}
		endif;

		if(ISSET($update_office)):
			foreach($update_office as $key => $val)
			{
				$gathered_total_allotment 		+= $update_allotment_amount[$key];
				$gathered_total_appropriation 	+= $update_appropriation_amount[$key];
			}
		endif;

		//get selected line item mooe amounts
		$current_mooe_amount = $this->line_items->validate_total_office_amount($id);

		if($gathered_total_allotment <= $current_mooe_amount['mooe_allotment'] || $gathered_total_appropriation <= $current_mooe_amount['mooe_appropriation']):
			return true;
		else:
			throw new Exception("Total amount of appropriation and allotment cannot exceed on the base amount on the selected line item.");
		endif;
	}

}