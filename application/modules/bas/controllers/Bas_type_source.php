<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_type_source extends BAS_Controller {

	private $module = MODULE_BAS_TYPE_SOURCE;

	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Bas_type_source_model', 'source');

		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);
	}
	
	public function index()
	{	
		try
		{
			$data = array();

			$resources = array(
				'load_modal' => array(
					'modal' => array(
						'controller'	=> __CLASS__,
						'module'		=> PROJECT_BAS,
						'method'		=> 'modal_add', // DEFAULT VALUE IS MODAL
						'title'			=> 'Type of Source',
					)	
				),
				'load_css'	=> array(
					CSS_DATATABLE,
					CSS_SELECTIZE
				),
				'load_js' 	=> array(
					JS_DATATABLE,
					JS_SELECTIZE
				),
				'datatable' => array(
					array('table_id' => 'tbl_source', 'path' => PROJECT_BAS.'/bas_type_source/get_source_list')
				)
			);
			
			$this->template->load('type_source', $data, $resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function check_exists_source()
	{
		try
		{
			$params = get_params();
			$name 	= $params['name'];
			if(!empty($params['id'])) $status = '0'; 
			else $status = $this->source->check_exists_source($name);

			echo json_encode(['status'=> $status]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e, TRUE);
			
		}
		catch(Exception $e)
		{
			$this->rlog_error($e, TRUE);
		}
	}
	
	public function get_source_list()
	{
		try
		{
			$params = get_params();
			$cnt = 0;
		
			$fields = array("source_type_num", "source_type_name", "IF(active_flag = 1, 'ACTIVE', 'NOT ACTIVE') active_flag");
			$selects = array("source_type_name", "active_flag");
		
			$source = $this->source->get_source_list($fields, $selects, $params);
			$iTotal = $this->source->total_length();
			$iFilteredTotal = $this->source->filtered_length($fields, $selects, $params);

			$output = array(
				"sEcho" => intval($_POST['sEcho']),
				"iTotalRecords" => $iTotal["cnt"],
				"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
				"aaData" => array()
			);
		
			foreach ($source as $aRow):
				$cnt++;
				$row = array();
				$action = "";
			
				$data_id = $aRow["source_type_num"];
				$id = base64_url_encode($data_id);
				$salt = gen_salt();
				$token = in_salt($data_id, $salt);			
				$url = $id."/".$salt."/".$token;

				$users_cnt = $this->source->get_source_count($id);
				$ctr = $users_cnt['cnt'];

				$delete_action = ($ctr > 0) ? 'alert_msg("'.ERROR.'", "'.$this->lang->line("parent_delete_error").'")' : 'content_delete("Source Type", "'.$id.'")';
				
				for ( $i=0 ; $i<count($selects) ; $i++ )
				{
					$row[] = $aRow[ $selects[$i] ];
				}

				$action  = "<div class='table-actions'>";
					if($this->permission_view):
						$action .= "<a href='javascript:void(0)' class='tooltip view md-trigger tooltipped view_data' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal' onclick=\"modal_init('".$url."/view')\"></a>";
					endif;

					if($this->permission_edit):
						$action .= "<a href='javascript:void(0)' class='tooltip edit md-trigger tooltipped edit_data' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal' onclick=\"modal_init('".$url."')\"></a>";
					endif;

					if($this->permission_delete):
						$action .= "<a href='javascript:void(0)' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50' onclick='".$delete_action."'></a>";
					endif;
				$action .= "</div>";

				/*if($cnt == count($users_cnt)){
					$action.= "<script src='". base_url() . PATH_JS."modalEffects.js' type='text/javascript'></script>";
					$action.= "<script src='". base_url() . PATH_JS."classie.js' type='text/javascript'></script>";
					$action.= "<script>$(function(){ $('.tooltipped').tooltip({delay: 50});	});</script>";
				}*/

				$row[] = $action;	
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
			$this->get_user_message($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}
	
	
	public function modal_add($id = NULL, $salt = NULL, $token = NULL)
	{
		try
		{
			$data = array();
			$resources = array();
			if(!IS_NULL($id))
			{
				$id = base64_url_decode($id);
				check_salt($id, $salt, $token);
				$data["data"] = $this->source->get_source_details($id);
				$resources['single'] = array(
					'status'=> $data["data"]['active_flag']
				);
			}
			$resources['load_css'] = array('selectize.default');
			$resources['load_js'] = array('popModal.min', 'selectize');
			
			$this->load->view("modals/type_source", $data);
			$this->load_resources->get_resource($resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}	
	}
	
	
	public function process()
	{
		try
		{
			$flag 	= 0;
			$params	= get_params();
			$this->_validate($params);
			
			$id		= $params['id'];
			$salt 	= $params['salt'];
			$token 	= $params['token'];
	
			check_salt($id, $salt, $token);
			
			BAS_Model::beginTransaction();
			
			$audit_table[]	= BAS_Model::tbl_param_source_types;
			$audit_schema[]	= DB_BAS;
				
			if(EMPTY($id))
			{
				$audit_action[]	= AUDIT_INSERT;
				$prev_detail[]	= array();

				$id = $this->source->insert_source($params);

				$msg = $this->lang->line('data_saved');

				$curr_detail[] = array($params);	
				$activity = "%s has been added";
			}
			else
			{
				$audit_action[]	= AUDIT_UPDATE;

				$prev_detail[] = array($this->source->get_source_details($id));

				$this->source->update_source($params);

				$msg = $this->lang->line('data_updated');
				
				$curr_detail[] = array($this->source->get_source_details($id));
				$activity = "%s has been updated";
			}
			
			$activity = sprintf($activity, $params['source']);

			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
			
			BAS_Model::commit();
			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
			$this->get_user_message($e);
			
		}
		catch(Exception $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
		echo json_encode($info);
	}
	
	private function _validate($params)
	{
		try
		{
			$this->_validate_security($params);

			$fields = array(
					'source' 	=> 'Source Type',
			);
			if(!isset($params['status'])) throw new Exception('Source Type Status is required.');	
			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_security(&$params)
    {
        try
        {
            if(EMPTY($params['security']))
            {
                throw new Exception($this->lang->line('err_invalid_request'));
            }


            $security = explode('/', $params['security']);

            $params['encoded_id']   = $security[0];
            $params['salt']         = $security[1];
            $params['token']        = $security[2];

            check_salt($params['encoded_id'], $params['salt'], $params['token']);

            $params['hash_id']      = base64_url_decode($params['encoded_id']);
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
	
	private function _validate_input($params)
	{
		try
		{
			$validation = array(
				'source' => array(
					'data_type' => 'string',
					'name'		=> 'source',
					'max_len'	=> 255
				),
				'status' => array(
					'data_type' => 'digit',
					'name'		=> 'status',
					'max_len'	=> 1
				)
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function delete_source($id = NULL, $salt = NULL, $token = NULL)
	{
		try
		{
			$flag = 0;
			$params	= get_params();
				
			$action = AUDIT_DELETE;
	
			// CHECK IF THE SECURITY VARIABLES WERE CORRUPTED OR INTENTIONALLY EDITED BY THE USER
			$id = base64_url_decode($params['param_1']);
				
			// BEGIN TRANSACTION
			BAS_Model::beginTransaction();
			
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= BAS_Model::tbl_param_source_types;
			$audit_schema[]	= DB_BAS;
	
			// GET THE DETAIL FIRST BEFORE UPDATING THE RECORD
			$prev_detail[] = array($this->source->get_source_details($id));
			
			$this->source->delete_source($id);
			$msg = $this->lang->line('data_deleted');
				
			// GET THE DETAIL AFTER UPDATING THE RECORD
			$curr_detail[] = array($this->source->get_source_details($id));
				
			// ACTIVITY TO BE LOGGED ON THE AUDIT TRAIL
			$activity = "%s has been deleted";
			$activity = sprintf($activity, $prev_detail[0][0]['source_type_name']);
	
			// LOG AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
				
			BAS_Model::commit();
			$flag = 1;
				
		}
		catch(PDOException $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
			$this->get_user_message($e);
		}
		catch(Exception $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}
		$info = array(
			"flag" 		=> $flag,
			"msg" 		=> $msg,
			"reload" 	=> 'datatable',
			"table_id" 	=> 'tbl_source',
			"path"		=> PROJECT_BAS . '/bas_type_source/get_source_list/'
		);
		
		echo json_encode($info);
	}
	
}


/* End of file users.php */
/* Location: ./application/modules/sysad/controllers/users.php */