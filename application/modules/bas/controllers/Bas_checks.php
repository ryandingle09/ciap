<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_checks extends BAS_Controller {

	private $module = MODULE_BAS_CHECKS;

	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	public $permission_void 	= FALSE;
	public $permission_override = FALSE;
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Bas_checks_model', 'checks');

		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);
		$this->permission_void		= $this->check_permission($this->module, ACTION_VOID);
		$this->permission_override	= $this->check_permission($this->module, ACTION_OVERRIDE);
	}
	
	public function index()
	{	
		try
		{
			$data 				= [];

			$resources 	= [
				'load_modal' 	=> [
					'modal_add' => [
						'controller'	=> __CLASS__,
						'method'		=> 'modal_add', // DEFAULT VALUE IS MODAL
						'module'		=> PROJECT_BAS,
						'title'			=> 'Checks',
						'multiple'   	=> true,
					],	
				],
				'load_css' => [
					CSS_DATATABLE,
					CSS_SELECTIZE,
					CSS_MODAL_COMPONENT,
					CSS_DATETIMEPICKER
				],
				'load_js' => [
					JS_DATATABLE,
					JS_SELECTIZE,
					JS_MODAL_CLASSIE,
					JS_MODAL_EFFECTS,
					JS_DATETIMEPICKER,
					'modules/'.PROJECT_BAS.'/'.strtolower( __CLASS__ )
				],
				'datatable' => [
					[
						'table_id' => 'tbl_check', 
						'path' => PROJECT_BAS.'/bas_checks/get_check_list'
					]
				]
			];
			
			$this->template->load('checks', $data, $resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}
	
	public function get_check_list()
	{
		try
		{
			
			$params = get_params();
			$cnt = 0;
		
			$fields = array(
				"c.check_id", 
				"c.disbursement_id", 
				"c.check_date", 
				"c.check_no", 
				"FORMAT(c.check_amount, 0), check_amount", 
				"c.check_status_id", 
				"d.dv_no", 
				"d.dv_date",
				"FORMAT(d.disbursed_amount, 0) disbursed_amount",
				"p.check_status_id", 
				"p.status_name" 
			);

			$selects = array(
				"dv_no",
				"dv_date",
				"disbursed_amount",
				"check_no",
				"check_date",
				"check_amount",
				"status_name"
			);
		
			$list = $this->checks->get_checks_list($fields, $selects, $params);
			$iTotal = $this->checks->total_length();
			$iFilteredTotal = $this->checks->filtered_length($fields, $selects, $params);

			$output = array(
				"sEcho" => intval($_POST['sEcho']),
				"iTotalRecords" => $iTotal["cnt"],
				"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
				"aaData" => array()
			);
		
			foreach ($list as $aRow):
				$cnt++;
				$row = array();
				$action = "";

				$id	= $aRow["check_id"];
				
				$hash_id 		= $this->hash($id); 
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;

				$count = $this->checks->get_checks_count($id);
				$ctr = $count['cnt'];

				$delete_action 	= 'content_delete("Check", "'.$url.'")';

				for ( $i=0 ; $i<count($selects) ; $i++ )
				{
					$row[] = $aRow[ $selects[$i] ];
				}
				
				$action = "<div class='table-actions'>";

					if($this->permission_view):
					$action .= "
						<a href='javascript:void(0)' class='tooltip view md-trigger tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_add' onclick=\"modal_add_init('".$url."/view')\">
						</a>";
					endif;

					if($this->permission_override):
					$action .= "
						<a href='javascript:void(0)' class='tooltip md-trigger tooltipped' data-tooltip='Overried' data-position='bottom' data-delay='50' data-modal='modal_add' onclick=\"modal_add_init('".$url."/override')\"><i class='flaticon-access1'></i>
						</a>";
					endif;

					if($this->permission_void):
					$action .= "
						<a href='javascript:void(0)' class='tooltip md-trigger tooltipped' data-tooltip='Void' data-position='bottom' data-delay='50' data-modal='modal_add' onclick=\"modal_add_init('".$url."/void')\"><i class='flaticon-cross95'></i>
						</a>";
					endif;

				$action .= '</div>';
				
				$row[]	= $action;
					
				$output['aaData'][] = $row;

			endforeach;
		
			echo json_encode( $output );

		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
			$this->get_user_message($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}
	
	
	public function modal_add($encoded_id, $salt, $token)
	{
		try
		{
			$data 				= array();
			$resources 			= array();
			
			if(!EMPTY($encoded_id))
			{
				check_salt($encoded_id, $salt, $token);
				
				$hash_id 		= base64_url_decode($encoded_id);
				$key 			= $this->get_hash_key('check_id'); 
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->checks->get_specific_checks_2($hash_id, $key);

				if(EMPTY($info)) throw new Exception($this->lang->line('err_invalid_request'));	

				foreach($info as $key => $info):

				$data['dv_no'] 				= $info['disbursement_id'];
				$data['dv_no_real'] 		= $info['dv_no'];
				$data['dv_date'] 			= $info['dv_date'];
				$data['check_date'] 		= $info['check_date'];
				$data['check_no'] 			= $info['check_no'];	
				$data['check_amount'] 		= $info['check_amount'];
				$data['mode_of_payment'] 	= $info['mode_type'];
				$data['remarks'] 			= $info['remarks'];

				$check_id 					= $info['check_id'];

				endforeach;
				
				$data["data"] = $this->checks->get_checks_details($data['check_id']);

				if($this->uri->segment(7) == 'override'):
					$resources['single'] = array(
						'dv_no'	=> $info['disbursement_id'],
						'mode'	=> $info['mode_of_payment']
					);
				endif;
			}
			else
			{
				$hash_id 	= $this->hash(0);				
			}
			
			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			$data['disbursement']	= $this->checks->get_disbursement_list();
			$data['modes']			= $this->checks->get_mode_list();
			
			$resources['load_css'] 	= array(
				'selectize.default'
			);
			$resources['load_js'] 	= array(
				'popModal.min', 
				'selectize'
			);
			$resources['loaded_init']	= array(
				'datepicker_init();',
				'selectize_init();'
			);
			
			$this->load->view("modals/checks", $data);
			$this->load_resources->get_resource($resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	
	public function process()
	{
		try
		{
			$flag 	= 0;
            $class  = ERROR;
			$params = get_params();

			$this->_validate($params);
	
			$key 				= $this->get_hash_key('check_id');
			$where				= array();
			$where[$key]		= $params['hash_id'];

			$info 				= $this->checks->get_specific_checks($where);
			$id					= $info['check_id'];
			
			BAS_Model::beginTransaction();

			if(EMPTY($id))
			{
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= BAS_Model::tbl_checks;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);

				$this->checks->insert_check($params);

				$activity		= "New Check has been added.";
				$msg  = $this->lang->line('data_saved');
			}
			else
			{
				$audit_action[]	= AUDIT_UPDATE;
				$audit_table[]	= BAS_Model::tbl_checks;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= array($info);
				
				$params['check_id']	= $id;

				$this->checks->update_check($params, $info['check_status_id']);
				
				$infonew		= $this->checks->get_specific_checks(array("check_id" => $id) , TRUE);
				$curr_detail[]	= array($infonew);
				
				$activity		= "Check has been updated.";
				$msg  			= $this->lang->line('data_updated');
			}
			
			$activity = sprintf($activity, $params['ada_no']);

			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
			
			BAS_Model::commit();
			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
			$this->get_user_message($e);
			
		}
		catch(Exception $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
		echo json_encode($info);
	}

	
	private function _validate(&$params)
	{
		try
		{
			$this->_validate_security($params);
			
			if($params['status'] != STATUS_VOIDED):

				$fields = [
					'check_date' 	=> 'CHECK ADA DATE',
					'check_no' 		=> 'CHECK ADA NO.',
					'check_amount' 	=> 'CHECK ADA AMOUNT',
					'dv_no' 		=> 'DV NO',
					'mode' 			=> 'MODE OF PAYMENT',
				];

			endif;

			if(isset($params['remarks'])) $fields['remarks'] = 'Remarks';
			
			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

    private function _validate_security(&$params)
    {
        try
        {
            if(EMPTY($params['security']))
            {
                throw new Exception($this->lang->line('err_invalid_request'));
            }


            $security = explode('/', $params['security']);

            $params['encoded_id']   = $security[0];
            $params['salt']         = $security[1];
            $params['token']        = $security[2];

            check_salt($params['encoded_id'], $params['salt'], $params['token']);

            $params['hash_id']      = base64_url_decode($params['encoded_id']);
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
	
	private function _validate_input($params)
	{
		try
		{
			$validation = array(
				'check_no' => array(
					'data_type' => 'digit',
					'name'		=> 'check_no',
					'max_len'	=> 10
				),
				'check_date' => array(
					'data_type' => 'date',
					'name'		=> 'check_date',
				),
				'dv_no' => array(
					'data_type' => 'digit',
					'name'		=> 'dv_no',
					'max_len'	=> 11
				),
				'check_amount' => array(
					'data_type' => 'amount',
					'name'		=> 'check_amount',
					'min_len'	=> 1
				),
				'remarks' => array(
					'data_type' => 'string',
					'name'		=> 'remarks',
					//'max_len'	=> 1
				),
				'mode' => array(
					'data_type' => 'digit',
					'name'		=> 'mode',
					//'max_len'	=> 1
				)
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function delete_check($params)
	{
		try
		{
			$flag	= 0;
			$msg	= "Error";

			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->validate_security($params);

			$key 			= $this->get_hash_key('account_code');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->chart->get_specific_chart($where);
			
			if(EMPTY($info['account_code']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			$this->chart->delete_chart($info['account_code']);

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			$msg	= $e->getMessage(); 
			$this->get_user_message($e);	
		}
		catch(Exception $e)
		{	
			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag" 		=> $flag,
			"msg" 		=> $msg,
			"reload" 	=> 'datatable',
			"table_id" 	=> 'tbl_chart',
			"path"		=> PROJECT_BAS . '/bas_chart_accounts/get_chart_list/'
		);

		echo json_encode($info);

	}
	
}


/* End of file users.php */
/* Location: ./application/modules/sysad/controllers/users.php */