<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_dashboard extends BAS_Controller {

	private $module = MODULE_BAS_DASHBOARD;
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Bas_dashboard_model', 'dashboard');
	}
	
	public function index()
	{
		try
		{	
			$data = [];

			$resources = [
				'load_css' => [
					CSS_DATATABLE,
				],
				'load_js' => [
					JS_DATATABLE,
				],
				'datatable' => [
					[
						'table_id' => 'tbl_dashboard', 
						'path' => PROJECT_BAS.'/bas_dashboard/get_dashboard_list'
					]
				],
			];
			
			$this->template->load('dashboard', $data, $resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}
	
	public function get_dashboard_list()
	{
		try
		{
			$params = get_params();
			$cnt = 0;
		
			$fields = array(
				'line_item_name', 
				'FORMAT(all_total_appropriation, 0) all_total_appropriation', 
				'FORMAT(all_total_allotment, 0) all_total_allotment', 
				'FORMAT(all_total_obligation, 0 all_total_obligation', 
				'FORMAT(all_total_disbursement, 0) all_total_disbursement',
				'FORMAT(utilization_rate, 0) utilization_rate',
				'FORMAT(unobligated_amount, 0) unobligated_amount',
				'FORMAT(unpaid_amount, 0) unpaid_amount'
			);
			$selects = array(
				'line_item_name', 
				'all_total_appropriation', 
				'all_total_allotment', 
				'all_total_obligation', 
				'all_total_disbursement',
				'utilization_rate',
				'unobligated_amount',
				'unpaid_amount'
			);
		
			$list = $this->dashboard->get_dashboard_list($fields, $selects, $params);
			$iTotal = $this->dashboard->total_length();
			$iFilteredTotal = $this->dashboard->filtered_length($fields, $selects, $params);

			$output = array(
				"sEcho" => intval($_POST['sEcho']),
				"iTotalRecords" => $iTotal["cnt"],
				"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
				"aaData" => array()
			);
		
			foreach ($list as $aRow):
				$cnt++;
				$row = array();

				for ( $i=0 ; $i<count($selects) ; $i++ )
				{
					$row[] = $aRow[ $selects[$i] ];
				}
					
				$output['aaData'][] = $row;

			endforeach;
		
			echo json_encode( $output );
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}
	
}


/* End of file users.php */
/* Location: ./application/modules/sysad/controllers/users.php */