<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Bas_obligation_slips extends BAS_Controller 
{
	private $module = MODULE_BAS_OBLIGATION_SLIPS;

	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;

	public $permission_print	= FALSE;	
	public $permission_void		= FALSE;
	public $permission_certify	= FALSE;
	public $permission_override	= FALSE;
	public $permission_excess	= FALSE;

	public function __construct()
	{
		parent::__construct();

		$this->load->model('bas_obligation_slips_model', 'obligation');
		$this->load->model('bas_realignment_model', 'realignment');

		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);
		$this->permission_print		= $this->check_permission($this->module, ACTION_PRINT);
		$this->permission_void		= $this->check_permission($this->module, ACTION_VOID);
		$this->permission_override	= $this->check_permission($this->module, ACTION_OVERRIDE);
		$this->permission_certify	= $this->check_permission($this->module, ACTION_CERTIFY);
		$this->permission_excess	= $this->check_permission($this->module, ACTION_EXCESS);
	}

	public function index()
	{
		try
		{
			$data 		= array();
			// CSS_MODAL_COMPONENT
			
			$resources = array(
				'load_css'	=> array(
					CSS_DATATABLE, CSS_SELECTIZE,
					CSS_DATETIMEPICKER
				),
				'load_js'=> array(
					JS_DATATABLE, JS_SELECTIZE, JS_DATETIMEPICKER,
					'add_row',
					'modules/'.PROJECT_BAS.'/'.strtolower( __CLASS__ )
				),
				'load_modal' => array(
					'modal_obligation_slip' 	=> array(
						'controller'			=> __CLASS__,
						'module'				=> PROJECT_BAS,
						'size'					=> 'xl',
						'height'				=> '500px;',
					),
				),
				'datatable' => array(
					array('table_id' => 'obligation_slips_table', 'path' => PROJECT_BAS.'/bas_obligation_slips/get_obligation_list'),
				),
			);

			$this->template->load('bas_obligation_slips', $data, $resources);	
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
		
	}

	public function get_obligation_list()
	{
		try
		{
			$params = get_params();
			$cnt = 0;
		
			$fields = array("obligation_id","release_date", "payee_name", "particulars","requested_amount", "obligation_status","os_date","obligated_amount");
			$selects = array("requested_date", "payee_name", "particulars","requested_amount", "obligation_status","os_no","obligated_amount");
		
			$obligation = $this->obligation->get_obligation_list($fields, $selects, $params);
			$iTotal = $this->obligation->total_length();
			$iFilteredTotal = $this->obligation->filtered_length($fields, $selects, $params);

			$output = array(
				"sEcho" 				=> intval($_POST['sEcho']),
				"iTotalRecords" 		=> $iTotal["cnt"],
				"iTotalDisplayRecords" 	=> $iFilteredTotal["cnt"],
				"aaData" 				=> array()
			);
		
			foreach ($obligation as $aRow):
				$cnt++;
				$row = array();
				$action = "";

				$id	= $aRow["obligation_id"];
				
				$hash_id 		= $this->hash($id); 
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;

				$counts 		= $this->obligation->get_obligation_count($id);
				$ctr 			= $counts['cnt'];

				$delete_action 	= 'content_delete("Obligation", "'.$url.'")';

				for ( $i=0 ; $i<count($selects) ; $i++ )
				{
					$row[] = $aRow[ $selects[$i] ];
				}

				$action = "<div class='table-actions'>";

				if($aRow['obligation_status'] != STATUS_VOIDED):
					if($this->permission_print) :
						$action .= "
							<a href='bas_obligation_slips/print_obligation_slip/".$url."' target='_blank' class='tooltip tooltipped' data-tooltip='Print' data-position='bottom' data-delay='50'><i class='flaticon-printing23'></i>
							</a>";
					endif;
				endif;

				if($this->permission_view) :
					$action .= "
						<a href='javascript:void(0)' class='tooltip view md-trigger tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_obligation_slip' onclick=\"modal_init('".$url."/view')\">
						</a>
					";
				endif;

				if($aRow['obligation_status'] == STATUS_DRAFTED):
					if($this->permission_edit) :
						$action .= "
							<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_obligation_slip' onclick=\"modal_init('".$url."')\">
							</a>";
					endif;
					
					if($this->permission_delete) : 	  
						$action .= "
							<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'>
							</a>";
					endif;
				endif;

				if($aRow['obligation_status'] == STATUS_SUBMITTED || $aRow['obligation_status'] == STATUS_CERTIFIED):

					if($this->permission_override) :
						$action .= "
							<a href='javascript:void(0)' class='tooltip md-trigger tooltipped' data-tooltip='Override' data-position='bottom' data-delay='50' data-modal='modal_obligation_slip' onclick=\"modal_init('".$url."/override')\"><i class='flaticon-access1'></i>
							</a>";
					endif;

					if($aRow['obligation_status'] == STATUS_SUBMITTED):
						if($this->permission_certify) :
							$action .= "
								<a href='javascript:void(0)' class='tooltip md-trigger tooltipped' data-tooltip='Certify' data-position='bottom' data-delay='50' data-modal='modal_obligation_slip' onclick=\"modal_init('".$url."/view/certify')\"><i class='flaticon-checkmark21'></i>
								</a>";
						endif;
					endif;
					
				endif;

				if($aRow['obligation_status'] == STATUS_CERTIFIED):
					if($this->permission_void) :
						$action .= "
							<a href='javascript:void(0)' class='tooltip md-trigger tooltipped' data-tooltip='Void' data-position='bottom' data-delay='50' data-modal='modal_obligation_slip' onclick=\"modal_init('".$url."/void')\"><i class='flaticon-cross95'></i>
							</a>";
					endif;

					if($this->permission_excess):
						$action .= "
							<a href='javascript:void(0)' class='tooltip md-trigger tooltipped' data-tooltip='Return Excess' data-position='bottom' data-delay='50' data-modal='modal_obligation_slip' onclick=\"modal_init('".$url."/excess')\"><i class='flaticon-arrows97'></i>
							</a>";
					endif;
				endif;

				$action .= '</div>';
				
				$row[]	= $action;
				$output['aaData'][] = $row;

			endforeach;
		
			echo json_encode( $output );
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
			$this->get_user_message($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function process()
	{
		try
		{
			$flag 	= 0;
            $error  = ERROR;
			$params = get_params();

			$this->_validate($params);
	
			$key 			= $this->get_hash_key('obligation_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->obligation->get_specific_obligation($where);
			$id				= $info['obligation_id'];

			BAS_Model::beginTransaction();

			if(EMPTY($id))
			{
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= BAS_Model::tbl_obligations;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= array();

				$id = $this->obligation->insert_obligation($params);

				$info 			= $this->obligation->get_specific_obligation(['obligation_id'=>$id]);

				$curr_detail[]	= array($info);

				$activity		= "New Obligation slip has been added.";

				$msg  			= $this->lang->line('data_saved');

				$error 			= SUCCESS;
			}
			else
			{
				$audit_action[]	= AUDIT_UPDATE;
				$audit_table[]	= BAS_Model::tbl_obligations;
				$audit_schema[]	= DB_BAS;

				$info			= $this->obligation->get_specific_obligation(array("obligation_id" => $id) , TRUE);

				$prev_detail[]	= $info;
				
				$params['obligation_id']		= $id;

				$this->obligation->update_obligation($params);
				
				$info			= $this->obligation->get_specific_obligation(array("obligation_id" => $id) , TRUE);

				$curr_detail[]	= $info;
				
				$activity		= "Obligation has been updated.";

				$msg  			= $this->lang->line('data_updated');

				$error 			= SUCCESS;
			}
			
			$activity = sprintf($activity, $params['obligation_id']);

			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
			
			BAS_Model::commit();
			$flag = 1;
			$class = $error;
		}
		catch(PDOException $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
			$this->get_user_message($e);
			
		}
		catch(Exception $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag" 			=> $flag,
			"msg" 			=> $msg,
			"error_class"	=> $error,
			"obligation_id" => $id
		);

		echo json_encode($info);
	}

	public function process_actions()
	{
		try
		{
			$error 		= ERROR;
			$msg 		= 'Something Went Wrong.';
			$flag 		= 0;
			$params 	= get_params();

			$this->_validate_action_form($params);

			$action 	= $this->obligation->process_actions($params);

			if($action)
			{
				$flag 	= 1;
				$msg 	= $this->lang->line('data_saved');
				$error 	= SUCCESS;
			}
		}
		catch( PDOException $e )
		{
			$msg  		= $this->rlog_error($e, TRUE);
		}
		catch( Exception $e )
		{
			$msg  		= $this->rlog_error($e, TRUE);
		}

		$info = array(
			"flag" 			=> $flag,
			"msg" 			=> $msg,
			"error_class"	=> $error,
		);

		echo json_encode($info);
	}

	public function get_action()
	{
		try
		{
			$params 			= get_params();
			$obligation_id 		= $params['obligation_id'];

			$data = array(
				'obligations' 			=> $this->obligation->get_obligation_full_details($obligation_id),
				'obligation_charges'	=> $this->obligation->get_obligation_charges($obligation_id),
			);

			$resources['loaded_init'] = array(
				'ModalEffects.re_init();',
				'datepicker_init();',
				'selectize_init();'
			);

			$this->load->view('tables/obligation_slips_certify_table', $data);
			$this->load_resources->get_resource($resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function modal($encoded_id, $salt, $token)
	{
		try 
		{
			$data 		= array();
			$data2 		= array();
			$resources 	= array();
			$info 		= array();
			$active 	= '';

			if(!EMPTY($encoded_id))
			{
				check_salt($encoded_id, $salt, $token);
				
				$hash_id 		= base64_url_decode($encoded_id);
				$key 			= $this->get_hash_key('obligation_id'); 
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->obligation->get_specific_obligation($where);

				if(EMPTY($info)) throw new Exception($this->lang->line('err_invalid_request'));	

				$obligation_id 	= $info['obligation_id'];

				$data2 = $this->obligation->get_obligation_details($obligation_id);

				$active = 'active';
			}
			else
			{
				$hash_id 	= $this->hash(0);	
				$active = '';	
				$obligation_id = '0';	
			}
			
			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			
			$data = array(
				'security' 				=> $encoded_id . '/' . $salt . '/' . $token,
				'payees'				=> $this->realignment->get_dropdowns('payees'),
				'fund_year'				=> $this->realignment->get_dropdowns('fund_year'),
				'active'				=> $active,
				'obligations' 			=> $this->obligation->get_obligation_full_details($obligation_id),
				'obligation_charges'	=> $this->obligation->get_obligation_charges($obligation_id),
				'signatories'			=> $this->obligation->get_obligation_signatories($obligation_id),
				'requested_date' 		=> $info['requested_date'],
				'address'				=> $info['address'],
				'particulars'			=> $info['particulars'],
				'source_type_num'		=> $info['source_type_num'],
				'obligation_status_id'	=> $info['obligation_status_id'],
				'payee' 				=> $info['payee_id'],
				'year' 					=> $info['year'],
			);

			$resources = array(
				'load_css' 		=> array(
					CSS_SELECTIZE,
				),
				'load_js' 		=> array(
					JS_SELECTIZE,
				),
				'loaded_init'	=> array(
					'ModalEffects.re_init();',
					'datepicker_init();',
					'selectize_init();'
				),
			);

			$this->load->view( 'modals/obligation_slips_modal',$data);
			$this->load_resources->get_resource($resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function get_payees_info()
	{
		try
		{
			$id = $this->uri->segment(4);
			echo $this->obligation->get_payees_info($id);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}


	public function get_fund_sources_by_year()
	{
		$params = get_params();
		echo modules::run('bas/bas_realignment/get_fund_sources_by_year',$params);
	}

	public function fill_dropdowns()
	{
		try
		{
			$params = get_params();
			$list = $this->obligation->fill_dropdowns($params, $this->uri->segment(4));
			echo json_encode($list);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function create_row()
	{
		try
		{
			$data = array(
				'view' 				=> $this->uri->segment(4),
				'count' 			=> $this->uri->segment(5),
				'line_items'		=> $this->obligation->get_line_items($this->uri->segment(6),$this->uri->segment(7)),
			);
			$resources['loaded_init'] = array(
				"selectize_init();",
			);
			$this->load->view('tables/obligation_table', $data);
			$this->load_resources->get_resource($resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function check_exceed_amount()
	{
		try
		{
			$params = get_params();

			$line_item 		= $params['line_item'];
			$account_code 	= $params['account_code'];
			$amount 		= str_replace(',','', $params['requested_amount']);

			foreach($line_item as $key => $val)
			{
				$query = $this->obligation->check_exceed_amount($val, $account_code[$key], $amount[$key], $key);
				if($query['total'] <= $amount[$key]) echo "Requested amount on row <b>".($key + 1)."</b> cannot <b>exceed</b> on the available balance amount of <b>".number_format($query['total'])."</b> <br>";
			}
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	private function _validate_action_form(&$params)
	{
		try
		{
			$this->_validate_security($params);

			$fields = array();

			if($params['status'] != STATUS_DECLINED):
				if(ISSET($params['obligated_amount'])){ $fields['obligated_amount'] 	= 'Obligated Amount';}
			endif;

			if(ISSET($params['remarks'])){$fields['remarks'] = 'Remarks';}

			$this->check_required_fields($params, $fields);

			return $this->_validate_action_input($params);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function _validate_action_input($params)
	{
		try
		{
			$validation = array(
				'obligated_amount' => array(
					'data_type' => 'integer',
					'name'		=> 'obligated_amount',
					'max_len'	=> 11
				),
				'obligated_date'=> array(
					'data_type' => 'date',
					'name'		=> 'obligated_date'
					//'max_len'	=> 1
				),
				'obligation_no' 	=> array(
					'data_type' => 'string',
					'name'		=> 'obligation_no',
					'max_len'	=> 20
				),
				'remarks' 	=> array(
					'data_type' => 'string',
					'name'		=> 'remarks',
					'max_len'	=> 255
				),
			);

			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	private function _validate(&$params)
	{
		try
		{
			$this->_validate_security($params);

			$fields = array(
				'requested_date' 		=> 'Requested Date',
				'payee' 				=> 'Payee',
				'fund_source' 			=> 'Fund Source',
				'address' 				=> 'Payee Address',
				'particulars' 			=> 'Particulars',
				'line_item' 			=> 'Line Item',
				'account_code' 			=> 'Account Code',
				'office' 				=> 'Office',
				'requested_amount' 		=> 'Requested Amount',
			);

			if(ISSET($params['role'])):
				$fields['role'] 		= 'Role';
				$fields['name'] 		= 'Name';
				$fields['position'] 	= 'Position';
			endif;

			if(ISSET($params['update_role'])):
				$fields['update_role'] 		= 'Role';
				$fields['update_name'] 		= 'Name';
				$fields['update_position'] 	= 'Position';
			endif;

			if(!empty($params['line_item'])):

				$item_ids 	= $params['line_item'];
				$item_ids2 	= $params['account_code'];

				foreach(array_count_values($item_ids) as $key => $val)
				{
					if($val == 2)
					{
						foreach(array_count_values($item_ids2) as $keys => $vals)
						{
							if($vals == 2)
							{
								throw new Exception("<b>Invalid Action Detected!</b> Please select different line item or account code.");
							}
						}
					}
				}

			endif;
			
			$this->check_required_fields($params, $fields);
			return $this->_validate_input($params);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

    private function _validate_security(&$params)
    {
        try
        {
            if(EMPTY($params['security']))
            {
                throw new Exception($this->lang->line('err_invalid_request'));
            }

            $security = explode('/', $params['security']);
            $params['encoded_id']   = $security[0];
            $params['salt']         = $security[1];
            $params['token']        = $security[2];

            check_salt($params['encoded_id'], $params['salt'], $params['token']);
            $params['hash_id']      = base64_url_decode($params['encoded_id']);
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
	
	private function _validate_input($params)
	{
		try
		{
			$validation = array(
				'requested_date' => array(
					'data_type' => 'date',
					'name'		=> 'requested_date',
					//'max_len'	=> 50
				),
				'payee' => array(
					'data_type' => 'digit',
					'name'		=> 'payee',
					'max_len'	=> 11
				),
				'fund_source' => array(
					'data_type' => 'digit',
					'name'		=> 'source_type_num',
					'max_len'	=> 11
				),
				'address' => array(
					'data_type' => 'string',
					'name'		=> 'address',
					'max_len'	=> 255
				),
				'particulars' => array(
					'data_type' => 'string',
					'name'		=> 'particulars',
					'max_len'	=> 255
				),
				'line_item' => array(
					'data_type' => 'digit',
					'name'		=> 'line_item',
					'max_len'	=> 11
				),
				'account_code' => array(
					'data_type' => 'string',
					'name'		=> 'account_code',
					'max_len'	=> 50
				),
				'office' => array(
					'data_type' => 'string',
					'name'		=> 'office',
					'max_len'	=> 10
				),
				'requested_amount' => array(
					'data_type' => 'digit',
					'name'		=> 'requested_amount',
					'max_len'	=> 25
				),
				'role' => array(
					'data_type' => 'string',
					'name'		=> 'role',
					'max_len'	=> 1
				),
				'name' => array(
					'data_type' => 'string',
					'name'		=> 'name',
					'max_len'	=> 50
				),
				'position' => array(
					'data_type' => 'string',
					'name'		=> 'position',
					'max_len'	=> 50
				),
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function delete_obligation($params)
	{
		try
		{
			$flag	= 0;
			$msg	= "Error";

			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->validate_security($params);

			$key 			= $this->get_hash_key('obligation_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->obligation->get_specific_obligation($where);
			
			if(EMPTY($info['obligation_id']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			$this->obligation->delete_obligation	($info['obligation_id']);

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			$msg	= $e->getMessage(); 	
		}
		catch(Exception $e)
		{	
			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag" 		=> $flag,
			"msg" 		=> $msg,
			"reload" 	=> 'datatable',
			"table_id" 	=> 'obligation_slips_table',
			"path"		=> PROJECT_BAS . '/bas_obligation_slips/get_obligation_list/'
		);

		echo json_encode($info);
	}

	public function delete_row($encoded_id, $salt, $token)
	{
		try
		{
			check_salt($encoded_id, $salt, $token);
			$hash_id 		= base64_url_decode($encoded_id);
			$key 			= $this->get_hash_key('obligation_id'); 

			$table = $this->uri->segment(7);

			$id = 'line_item_id';
			($table == 'signatory') ? $id = 'signatory_id' : $id;

			$where = [
				$key 	=> $hash_id,
				$id		=> $this->uri->segment(8)
			];

			$this->obligation->delete_row($where, $table);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function print_obligation_slip($encoded_id, $salt, $token)
	{

		try
		{
			$data 					= array();
			$style_content 			= "";

			if(!EMPTY($encoded_id))
			{
				check_salt($encoded_id, $salt, $token);
					
				$hash_id 		= base64_url_decode($encoded_id);
				$key 			= $this->get_hash_key('obligation_id'); 
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->obligation->get_specific_obligation($where);

				$obligation_id = $info['obligation_id'];

				$style_content .= $this->load->view( 'reports/styles/report_common', array(), TRUE );
				$style_content .= $this->load->view( 'reports/styles/obligation_slip_form_styles', array(), TRUE );

				$data = array(
					'payees'				=> $this->realignment->get_dropdowns('payees'),
					'fund_year'				=> $this->realignment->get_dropdowns('fund_year'),
					'obligations' 			=> $this->obligation->get_obligation_full_details($obligation_id),
					'obligation_charges'	=> $this->obligation->get_obligation_charges($obligation_id),
					'signatories'			=> $this->obligation->get_obligation_signatories($obligation_id),
					'requested_date' 		=> $info['requested_date'],
					'address'				=> $info['address'],
					'particulars'			=> $info['particulars'],
					'source_type_num'		=> $info['source_type_num'],
					'obligation_status_id'	=> $info['obligation_status_id'],
					'payee' 				=> $info['payee_id'],
					'os_date' 				=> $info['os_date'],
					'os_no' 				=> $info['os_no'],
					'year' 					=> $info['year'],
					'obligated_amount' 		=> $info['obligated_amount'],
					'style_content' 		=> $style_content,
				);
				
				$html = $this->load->view( 'reports/obligation_slip_form', $data, TRUE );

				$filename	= 'Obligation_slip_form_'.date('F').'_'.date('d').'_'.date('Y');
				
				ob_end_clean();
				
				$this->pdf( $filename, $html, TRUE, 'I', NULL, NULL, TRUE );
			}
			else
			{
				echo 'Invalid Action!';
			}
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function array_sum_test()
	{
		try
		{
			$params = get_params();

			echo number_format(array_sum(str_replace(',', '', $params['obligated_amount'])));
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}
}