<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_chart_accounts extends BAS_Controller {

	private $module = MODULE_BAS_CHART_ACCOUNTS;

	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Bas_chart_accounts_model', 'chart');

		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);
	}
	
	public function index()
	{	
		try
		{
			$data = $resources = $modal = [];

			$resources = [
				'load_modal'	=> [
					'modal' 	=> [
						'controller'	=> __CLASS__,
						'module'		=> PROJECT_BAS,
						'method'		=> 'modal_add', // DEFAULT VALUE IS MODAL
						'title'			=> 'Chart of Account',
					],
				],
				'load_css' 	=> [
					CSS_DATATABLE,
					CSS_SELECTIZE
				],
				'load_js' 	=> [
					JS_DATATABLE,
					JS_SELECTIZE
				],
				'datatable' => [
					['table_id' => 'tbl_chart', 'path' => PROJECT_BAS.'/bas_chart_accounts/get_chart_list'],
				],
			];
			
			$this->template->load('chart_accounts', $data, $resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}
	
	public function get_chart_list()
	{
		try
		{
			$params = get_params();
			$cnt = 0;
		
			$fields = array("account_code", "account_name", "parent_account_code", "IF(active_flag = 1, 'ACTIVE', 'NOT ACTIVE') active_flag");
			$selects = array("account_code", "account_name", "active_flag");
		
			$chart = $this->chart->get_chart_list($fields, $selects, $params);
			$iTotal = $this->chart->total_length();
			$iFilteredTotal = $this->chart->filtered_length($fields, $selects, $params);

			$output = array(
				"sEcho" => intval($_POST['sEcho']),
				"iTotalRecords" => $iTotal["cnt"],
				"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
				"aaData" => array()
			);
		
			foreach ($chart as $aRow):
				$cnt++;
				$row = array();
				$action = "";

				// PRIMARY KEY
				$id	= $aRow["account_code"];
				
				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash($id); 
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;

				$users_cnt = $this->chart->get_chart_count($id);
				$ctr = $users_cnt['cnt'];

				$delete_action 	= 'content_delete("Chart of Account", "'.$url.'")';

				for ( $i=0 ; $i<count($selects) ; $i++ )
				{
					$row[] = $aRow[ $selects[$i] ];
				}

				$action = "<div class='table-actions'>";
					if($this->permission_view):
						$action .= "<a href='javascript:void(0)' class='tooltip view md-trigger tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal' onclick=\"modal_init('".$url."/view')\"></a>";
					endif;

					if($this->permission_edit) :
						$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal' onclick=\"modal_init('".$url."')\"></a>";
					endif;
					
					if ($this->permission_delete) : 	  
						$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					endif;
				$action .= '</div>';
				
				$row[]	= $action;
					
				$output['aaData'][] = $row;

			endforeach;
		
			echo json_encode( $output );

		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
			$this->get_user_message($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}
	
	
	public function modal_add($encoded_id, $salt, $token)
	{
		try
		{
			$data 		= array();
			$resources 	= array();
			$trigger 	= '';

			// CHECK IF THE ACTION IS UPDATE/INSERT
			if(!EMPTY($encoded_id))
			{
				// CHECK THE SECURITY VARIABLES
				check_salt($encoded_id, $salt, $token);
				
				$hash_id 		= base64_url_decode($encoded_id);
				$key 			= $this->get_hash_key('account_code'); 
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->chart->get_specific_chart($where);

				if(EMPTY($info)) throw new Exception($this->lang->line('err_invalid_request'));	

				$data['account_code'] 			= $info['account_code'];
				$data['account_name'] 			= $info['account_name'];
				$data['parent_account_code'] 	= $info['parent_account_code'];	
				$data['active_flag'] 			= $info['active_flag'];
				$data['trigger']			 	= 'update';
				$data["readonly_value"]			= "readonly";	

				//$account_code 	= $info['account_code'];

				$data["data"] = $this->chart->get_chart_details($data['account_code']);
				//echo $info['active_flag'];

				$resources['single'] = array(
					'status'=> $info['active_flag'],
					'parent_code'=> $info['parent_account_code']
				);
			}
			else
			{
				// USE THIS VARIABLE FOR INSERT 
				$hash_id 	= $this->hash(0);		
				$data['trigger']			 	= 'insert';		
			}
			
			// CONSTRUCT SECURITY VARIABLES
			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			$data['charts']	= $this->chart->get_parent_acount_list();
			
			$resources['load_css'] 	= array('selectize.default');
			$resources['load_js'] 	= array('popModal.min', 'selectize');
			
			$this->load->view("modals/chart_accounts", $data);
			$this->load_resources->get_resource($resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function check_exist_account_codes()
	{
		try
		{
			$trigger 	= $this->uri->segment(5);
			$old_code 	= $this->uri->segment(6);
			$code 		= $this->uri->segment(4);

			if($trigger == 'insert'):
				$count = $this->chart->check_exist_account_codes($code);
				echo json_encode(['count'=>$count]);
			endif;

			if($trigger == 'update'):
				$count = 0;
				if($old_code == $code) $cound = 0;
				else $count = $this->chart->check_exist_account_codes($code);
				echo json_encode(['count'=>$count]);
			endif;

		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
		
	}
	
	
	public function process()
	{
		try
		{
			$flag 	= 0;
            $class  = ERROR;
			$params = get_params();

			$this->_validate($params);
	
			$key 			= $this->get_hash_key('account_code');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->chart->get_specific_chart($where);
			$id				= $info['account_code'];
			
			BAS_Model::beginTransaction();

			if(EMPTY($id))
			{
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= BAS_Model::tbl_param_account_codes;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);

				$this->chart->insert_chart($params);

				$activity	= "%s has been added.";
				$activity 	= sprintf($activity, $params['title']);

				$msg  = 'Record was successfully saved.';
			}
			else
			{
				$audit_action[]	= AUDIT_UPDATE;
				$audit_table[]	= BAS_Model::tbl_param_account_codes;
				$audit_schema[]	= DB_BAS;

				$prev_detail[]	= array($info);

				$this->chart->update_chart($params);

				//$params['account_code']	= $params['code'];
				
				//$infonew		= $this->chart->get_specific_chart(array("account_code" => $id) , TRUE);

				$curr_detail[]	= array($params);
				
				$activity	= "%s has been updated.";
				$activity 	= sprintf($activity, $params['title']);

				$msg  = 'Record was successfully updated.';
			}

			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
			
			BAS_Model::commit();
			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
			$this->get_user_message($e);
			
		}
		catch(Exception $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
		echo json_encode($info);
	}
	
	private function _validate(&$params)
	{
		try
		{
			$this->_validate_security($params);

			$fields = array(
					'code' 			=> 'Account Code',
					'title' 		=> 'Account Title',
			);
			
			if(!isset($params['status'])) throw new Exception('Account Status is required.');	
			
			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

    private function _validate_security(&$params)
    {
        try
        {
            if(EMPTY($params['security']))
            {
                throw new Exception($this->lang->line('err_invalid_request'));
            }


            $security = explode('/', $params['security']);

            $params['encoded_id']   = $security[0];
            $params['salt']         = $security[1];
            $params['token']        = $security[2];

            check_salt($params['encoded_id'], $params['salt'], $params['token']);

            $params['hash_id']      = base64_url_decode($params['encoded_id']);
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
	
	private function _validate_input($params)
	{
		try
		{
			$validation = array(
				'code' => array(
					'data_type' => 'string',
					'name'		=> 'code',
					'max_len'	=> 100
				),
				'title' => array(
					'data_type' => 'string',
					'name'		=> 'title',
					'max_len'	=> 255
				),
				'parent_code' => array(
					'data_type' => 'string',
					'name'		=> 'status',
					'max_len'	=> 100
				),
				'status' => array(
					'data_type' => 'digit',
					'name'		=> 'status',
					'max_len'	=> 1
				)
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function delete_chart($params)
	{
		try
		{
			$flag	= 0;
			$msg	= "Error";

			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->validate_security($params);

			$key 			= $this->get_hash_key('account_code');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->chart->get_specific_chart($where);
			
			if(EMPTY($info['account_code']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			$this->chart->delete_chart($info['account_code']);

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			$msg	= $e->getMessage(); 
			$this->get_user_message($e);	
		}
		catch(Exception $e)
		{	
			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag" 		=> $flag,
			"msg" 		=> $msg,
			"reload" 	=> 'datatable',
			"table_id" 	=> 'tbl_chart',
			"path"		=> PROJECT_BAS . '/bas_chart_accounts/get_chart_list/'
		);

		echo json_encode($info);
	}
	
}