<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_payees extends BAS_Controller {

	private $module = MODULE_BAS_PAYEES;

	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Bas_payees_model', 'payees');

		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);
	}
	
	public function index()
	{
		try
		{
			$data  = array();
			
			$resources = array(
				'load_modal' => array(
					'modal' => array(
						'controller'	=> __CLASS__,
						'module'		=> PROJECT_BAS,
						'method'		=> 'modal_add', // DEFAULT VALUE IS MODAL
						'title'			=> 'Payee',
					)
				),
				'load_css'	=> array(CSS_DATATABLE,
					CSS_SELECTIZE
				),
				'load_js' 	=> array(JS_DATATABLE,
					JS_SELECTIZE
				),
				'datatable' => array(
					array('table_id' => 'tbl_payees', 'path' => PROJECT_BAS.'/bas_payees/get_payees_list'),
				)
			);
			$this->template->load('payees', $data, $resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}
	
	public function get_payees_list()
	{
		try
		{
			$params = get_params();
			$cnt = 0;
		
			$fields = array(
				"payee_id", 
				"payee_name", 
				"payee_address", 
				"payee_tin", 
				"vatable_flag", 
				"IF(active_flag = 1, 'ACTIVE', 'NOT ACTIVE') active_flag"
			);
			$selects = array(
				"payee_name", 
				"payee_address", 
				"payee_tin", 
				"active_flag"
			);
		
			$payees = $this->payees->get_payees_list($fields, $selects, $params);
			$iTotal = $this->payees->total_length();
			$iFilteredTotal = $this->payees->filtered_length($fields, $selects, $params);

			$output = array(
				"sEcho" => intval($_POST['sEcho']),
				"iTotalRecords" => $iTotal["cnt"],
				"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
				"aaData" => array()
			);
		
			foreach ($payees as $aRow):
				$cnt++;
				$row = array();
				$action = "";
			
				$data_id = $aRow["payee_id"];
				$id = base64_url_encode($data_id);
				$salt = gen_salt();
				$token = in_salt($data_id, $salt);			
				$url = $id."/".$salt."/".$token;

				$users_cnt = $this->payees->get_payees_count($id);
				$ctr = $users_cnt['cnt'];

				$delete_action = ($ctr > 0) ? 'alert_msg("'.ERROR.'", "'.$this->lang->line("parent_delete_error").'")' : 'content_delete("payee", "'.$id.'")';
				
				for ( $i=0 ; $i<count($selects) ; $i++ )
				{
					$row[] = $aRow[ $selects[$i] ];
				}

				$action  = "<div class='table-actions'>";
				$action .= "<a href='javascript:void(0)' class='tooltip view md-trigger tooltipped view_data' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal' onclick=\"modal_init('".$url."/view')\"></a>";
				$action .= "<a href='javascript:void(0)' class='tooltip edit md-trigger tooltipped edit_data' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal' onclick=\"modal_init('".$url."')\"></a>";
				$action .= "<a href='javascript:void(0)' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50' onclick='".$delete_action."'></a>";
				$action .= "</div>";

				$row[] = $action;	
				$output['aaData'][] = $row;
			endforeach;
		
			echo json_encode( $output );

		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
			$this->get_user_message($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}
	
	
	public function modal_add($id = NULL, $salt = NULL, $token = NULL)
	{
		try
		{
			$data = array();
			
			if(!IS_NULL($id))
			{
				$id = base64_url_decode($id);
				check_salt($id, $salt, $token);
				$data["data"] = $this->payees->get_payees_details($id);

				$resources['single'] = array(
					'vat'=> $data["data"]['vatable_flag'],
					'status'=> $data["data"]['active_flag']
				);
			}

			$resources['load_css'] = array('selectize.default');
			$resources['load_js'] = array('popModal.min', 'selectize');
			
			$this->load->view("modals/payees", $data);
			$this->load_resources->get_resource($resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}
	
	
	public function process()
	{
		try
		{
			$flag = 0;
			$params	= get_params();
			$this->_validate($params);
			
			$id	= filter_var($params['id'], FILTER_SANITIZE_NUMBER_INT);
			$salt = $params['salt'];
			$token = $params['token'];
	
			check_salt($id, $salt, $token);
			
			BAS_Model::beginTransaction();
			
			$audit_table[]	= BAS_Model::tbl_param_payees;
			$audit_schema[]	= DB_BAS;
				
			if(EMPTY($id))
			{
				$audit_action[]	= AUDIT_INSERT;
				$prev_detail[]	= array();

				$id = $this->payees->insert_payees($params);
				$msg = 'Record was successfully saved.';

				$curr_detail[] = array($this->payees->get_payees_details($id));	
				$activity = "%s has been added";
			}
			else
			{
				$audit_action[]	= AUDIT_UPDATE;
				$prev_detail[] = array($this->payees->get_payees_details($id));
				$this->payees->update_payees($params);
				$msg = 'Record was successfully updated.';
				
				$curr_detail[] = array($this->payees->get_payees_details($id));
				$activity = "%s has been updated";
			}
			
			$activity = sprintf($activity, $params['payee']);

			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
			
			BAS_Model::commit();
			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
			$this->get_user_message($e);
			
		}
		catch(Exception $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	
	
	}
	
	private function _validate($params)
	{
		try
		{
			$this->_validate_security($params);

			$fields = array(
					'payee' 	=> 'Payee Name',
					'address' 	=> 'Payee Address',
					'tin' 		=> 'Payee TIN Number'
			);
			if(!isset($params['vat'])) throw new Exception('Payee Vat is required.');	
			if(!isset($params['status'])) throw new Exception('Payee Status is required.');	
			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_security(&$params)
    {
        try
        {
            if(EMPTY($params['security']))
            {
                throw new Exception($this->lang->line('err_invalid_request'));
            }


            $security = explode('/', $params['security']);

            $params['encoded_id']   = $security[0];
            $params['salt']         = $security[1];
            $params['token']        = $security[2];

            check_salt($params['encoded_id'], $params['salt'], $params['token']);

            $params['hash_id']      = base64_url_decode($params['encoded_id']);
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
	
	private function _validate_input($params)
	{
		try
		{
			$validation = array(
				'payee' => array(
					'data_type' => 'string',
					'name'		=> 'payee',
					'max_len'	=> 255
				),
				'address' => array(
					'data_type' => 'string',
					'name'		=> 'addresss',
				),
				'tin' => array(
					'data_type' => 'string',
					'name'		=> 'tin',
					'max_len'	=> 100
				),
				'vat' => array(
					'data_type' => 'digit',
					'name'		=> 'vat',
					'max_len'	=> 1
				),
				'status' => array(
					'data_type' => 'digit',
					'name'		=> 'status',
					'max_len'	=> 1
				)
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function delete_payees($id = NULL, $salt = NULL, $token = NULL)
	{
		try
		{
			$flag = 0;
			$params	= get_params();
				
			$action = AUDIT_DELETE;
	
			// CHECK IF THE SECURITY VARIABLES WERE CORRUPTED OR INTENTIONALLY EDITED BY THE USER
			$id = base64_url_decode($params['param_1']);
				
			// BEGIN TRANSACTION
			BAS_Model::beginTransaction();
			
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= BAS_Model::tbl_param_payees;
			$audit_schema[]	= DB_BAS;
	
			// GET THE DETAIL FIRST BEFORE UPDATING THE RECORD
			$prev_detail[] = array($this->payees->get_payees_details($id));
			
			$this->payees->delete_payees($id);
			$msg = $this->lang->line('data_deleted');
				
			// GET THE DETAIL AFTER UPDATING THE RECORD
			$curr_detail[] = array($this->payees->get_payees_details($id));
				
			// ACTIVITY TO BE LOGGED ON THE AUDIT TRAIL
			$activity = "%s has been deleted";
			$activity = sprintf($activity, $prev_detail[0][0]['role_name']);
	
			// LOG AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
				
			BAS_Model::commit();
			$flag = 1;
				
		}
		catch(PDOException $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
			$this->get_user_message($e);
		}
		catch(Exception $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}
	
		$info = array(
			"flag" 		=> $flag,
			"msg" 		=> $msg,
			"reload" 	=> 'datatable',
			"table_id" 	=> 'tbl_payees',
			"path"		=> PROJECT_BAS . '/bas_payees/get_payees_list/'
		);
	
		echo json_encode($info);
	}
	
}


/* End of file users.php */
/* Location: ./application/modules/sysad/controllers/users.php */