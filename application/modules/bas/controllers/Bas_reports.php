<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_reports extends BAS_Controller {

	private $module = MODULE_BAS_REPORTS;

	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Bas_reports_model', 'reports');
		$this->load->model('Bas_line_items_model', 'line_items');

		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);
	}
	
	public function index()
	{	
		$data 				= array();
		$resources 			= [
			'load_css' => [
				CSS_SELECTIZE,
				CSS_DATETIMEPICKER
			],
			'load_js' => [
				JS_SELECTIZE,
				JS_DATETIMEPICKER,
				'modules/'.PROJECT_BAS.'/'.strtolower( __CLASS__ )
			],
		];
		
		$this->template->load('reports', $data, $resources);
	}
	
	
	public function fill_dropdowns()
	{
		try
		{
			$params =  get_params();

			if($params['identity'] == 'year')
			{
				echo json_encode($this->reports->fill_dropdowns($params['identity'], $params['value']));
			}
			elseif($params['identity'] == 'fund_source')
			{
				echo json_encode($this->reports->fill_dropdowns($params['identity'], $params['value'], $params['value2']));
			}
			elseif($params['identity'] == 'report_type')
			{
				echo json_encode($this->reports->fill_dropdowns($params['identity']));
			}
			else echo 'Action no permitted!';//blocked
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
		}
	}

	public function generate_data($params)
	{
		try
		{
			$params 		= get_params();

			$report_type 	= $params['report_type'];

			$type 			= '';

			switch ($report_type) 
			{
				case 'SAAODBE':
					$type 	= 'SAAODBE';
					break;
				case 'SAAODB':
					$type 	= 'SAAODB';
					break;
				case 'SAAOB':
					$type 	= 'SAAOB';
					break;
				case 'SMFO':
					$type 	= 'SMFO';
					break;
				case 'RAO':
					$type 	= 'RAO';
					$this->generate_rao($params);
					die();
					break;
			}

			if(empty($type)) redirect('bas/bas_reports');

			$report_type_model 	= 'get_'.$type.'_report';

			$report = $this->reports->{$report_type_model}($params);

			$data = [
				'report' 				=> $report,
				'fund_sourse'			=> $params['fund_source'],
				'from'					=> ''.date('F j,  Y', strtotime(''.$params['year'].'-'.$params['from'].'-01')).'',
				'to'					=> ''.date('F j,  Y', strtotime(''.$params['year'].'-'.$params['to'].'-31')).'',
				'department'			=> 'Trade and Industry',
				'agency'				=> 'Construction Industry Authority of Philippines',
				'operating_unit'		=> '',
				'organization_code'		=> '22-004-00-000',
				'funding_source_code'	=> '',
			];

			$html = $this->load->view( 'reports/'.$type.'', $data, TRUE );

			$filename	= ''.$type.'_form_'.date('F').'_'.date('d').'_'.date('Y');
			
			if($params['file_type'] == '2')
			{
				$this->convert_excel( $html, $filename , '');
			}
			else
			{
				ob_end_clean();

				if($type == 'SMFO') $this->pdf( $filename, $html, TRUE, 'I', NULL, NULL, TRUE );
				else $this->pdf( $filename, $html, FALSE, 'I', NULL, NULL, TRUE );
			}
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
		}
	}

	public  function generate_rao($params)
	{
		try
		{
			$params['from']				= ''.date('Y-m-d', strtotime(''.$params['year'].'-'.$params['from'].'-01')).'';//'2016-01-01';//
			$params['to']				= ''.date('Y-m-d', strtotime(''.$params['year'].'-'.$params['to'].'-31')).'';//'2016-12-31';
			$params['year'] 			= ''.$params['year'].'';//'2016';//
			$params['fund_source'] 		= ''.$params['fund_source'].'';//'1';//
			$params['line_item'] 		= ''.$params['line_item'].'';//'23';//
			$params['allotment_class'] 	= $params['allotment_class'];//'501';

			$code = $params['allotment_class'];

			switch ($code) {
				case '501':
					$code = 'PS';
					break;
				case '502':
					$code = 'MOOE';
					break;
				case '503':
					$code = 'CO';
					break;
			}

			$parents = $this->reports->get_account_codes($params['allotment_class'], 'parent');
			$parent_code = $this->reports->get_account_codes($params['allotment_class'], 'parent');
			$obligations = $this->reports->get_obligation_nos($params);
			

			///BEGIN VARIABLES
			$allotment_amount 			= 0;
			$allotment_amount_data 		= 0;
			$total_allotment 			= 0;
			$total_appropriation_row	= 0;
			$total_allotment_row 		= 0;
			$total_amount 				= 0;
			$total_afm_per_account_code	= [];
			$total_afm_per_total		= [];
			$last_month_per_account_code= [];
			$ob_html 					= [];
			$ob_html2 					= [];
			$afm_html 					= [];
			$ob_html 					= [];
			$ob_row_total_amount 		= [];
			$ob_row_total_amount2 		= [];
			$ob_row_total_amount3		= [];
			$total_amount_arr 			= [];
			$month_no 					= 0;
			$month_nos					= 0;
			$month_name 				= '';
			$total_to_date 				= [];
			$balance 	 				= [];

			$header  =  '';

			$title_head .= '<table width="100%"><tr><td align="center" colspan="10"><div align="center">';
			$title_head .= '<b>PHILIPPINE CONTRACTORS ACCREDITATION BOARD<br>';
			$title_head .= 'REGISTRY ALLOTMENT AND OBLIGATIONS<br>';
			$title_head .= 'STATUS OF FUNDS<br>';
			$title_head .= date('F j,  Y', strtotime($params['from'])).' - '.date('F j,  Y', strtotime($params['to']));
			$title_head .= '</div></td></tr></table>';

			$start    = (new DateTime($params['from']))->modify('first day of this month');
			$end      = (new DateTime($params['to']))->modify('first day of next month');
			$interval = DateInterval::createFromDateString('1 month');
			$period   = new DatePeriod($start, $interval, $end);

			foreach ($period as $lol => $dt):

				$line_item = $this->reports->get_line_item_data($params['line_item']);
				$line_item_name = $line_item['line_item_code'].'.&nbsp;'.$line_item['line_item_name'];

				$month_name = date('M', strtotime($dt->format('Y-m')));
				$month_no 	= date('m', strtotime($dt->format('Y-m')));
				$month_nos 	= date('n', strtotime($dt->format('Y-m')));

				$month_names = '<br>'.$month_name;

				$params['from']		= ''.date('Y-m-d', strtotime(''.$params['year'].'-'.$month_no.'-01')).'';//'2016-'.$month_no.'-01';//
				$params['to']		= ''.date('Y-m-d', strtotime(''.$params['year'].'-'.$month_no.'-31')).'';//'2016-'.$month_no.'-31';//

				//RESET VARIABLES
			    $header  					= '<tr><td rowspan="2" align="center">Alobs</td>';
				$sheader  					= '<tr>';
				$stheader 					= '<tr><td align="center">Appropriation</td>';
				$allotment_amount 			= 0;
				$allotment_amount_data 		= 0;
				$total_allotment 			= 0;
				$total_appropriation_row	= 0;
				$total_allotment_row 		= 0;
				$total_amount 				= 0;
				$ob_html    				= [];
				$total_afm_per_account_code	= [];
				$total_afm_per_total		= [];
				$ob_html 					= [];
				$ob_html2 					= [];
				$afm_html 					= [];
				$ob_html 					= [];
				$ob_row_total_amount 		= [];
				$ob_row_total_amount2 		= [];
				$ob_row_total_amount3		= [];
				$total_amount_arr 			= [];
				$appropriation 				= [];

				$amount_totals 				= [];

				foreach($parents as $key => $p):

					$ob_total  = [];
					$stotal    = 0;
					$childs    = $this->reports->get_account_codes($p['account_code'], 'child');
					$colspan   = count($childs) + 1;
					$header   .= '<td align="center" colspan="'.$colspan.'">'.$p['account_name'].'</td>';
				
					foreach($childs as $keyc => $c):

						$allotment_amount_data 	= $this->reports->get_total_allotments_by_child_account_code($params, $c['account_code']);
						$total_allotment 		= str_replace(',', '', $allotment_amount_data);
						$total_appropriation_row += $total_allotment;

						$sheader 	.= '<td>'.$c['account_name'].'</td>';
						$stheader 	.= '<td align="right">'.$allotment_amount_data.'</td>';	

						$appropriation[$c['account_code']] = $allotment_amount_data;

						$stotal 	+= $total_allotment;	

						foreach($obligations as $keys => $o):

					 		if(! ISSET($ob_html[$o['os_no']])):
								$ob_html[$o['os_no']] 	= '<td>'.$o['os_no'].'</td>';
								$ob_html2[$o['os_no']] 	= '<td>Total Alobs for the month</td>';
							endif;		

							$amount_per_code = $this->reports->get_amount_per_account_code_2($params, $c['account_code'], $o['os_no']);
							$total_amount    = str_replace(',', '', $amount_per_code);

							$ob_html[$o['os_no']] 	.= '<td align="right">'.$amount_per_code.'</td>';	
							$ob_total[$o['os_no']] 	+= $total_amount;
							$ob_row_total_amount[$o['os_no']] 	+= $total_amount;

							$ob_row_total_amount2[$keys] 		+= $total_amount;

							$total_afm_per_account_code[$c['account_code']] += $total_amount; 

						endforeach;	

					endforeach;	

					array_walk($ob_html, function(&$val, $key, $prefix)
					{
						$amount = (!number_format($prefix[$key]) == 0) ? number_format($prefix[$key]) : ' - ';
						$val = $val.'<td align="right">'.$amount.'</td>';
					}, $ob_total);

					$stotal = (number_format($stotal) != 0) ? number_format($stotal) : ' - ';

					$stheader 	.= '<td align="right">'.$stotal.'</td>';	
					$sheader 	.= '<td align="center">Total</td>';

					$total_amount_arr[] += $total_amount;

					$total_afm_per_account_code 	= array_merge($total_afm_per_account_code, [array_sum($ob_total)]);

					$appropriation = array_merge($appropriation, [$stotal]);

				endforeach;

				array_walk($ob_html, function(&$val, $key, $prefix) 
				{
					$amount = (!number_format($prefix[$key]) == 0) ? number_format($prefix[$key]) : ' - ';
					$val = $val.'<td align="right">'.$amount.'</td>';
				}, $ob_row_total_amount);

				$appropriation = array_merge($appropriation, [$total_appropriation_row]);

				$stheader 		.= '<td align="right">'.number_format($total_appropriation_row).'</td></tr>';
				$sheader 		.= '</tr>';
				$header 		.= '<td rowspan="2" align="center">TOTAL <br> '.$code.'</td></tr>';
				$final_ob_html 	= '<tr>'.rtrim(implode('</tr><tr>', $ob_html), '<tr>');
				$final_afm_html = '<tr>'.rtrim(implode('</tr><tr>', $ob_html2), '<tr>');


				//contruct table -----------------------------------------------------------------------------

				$title .= $line_item_name;
				$title .= $month_names;
				$title .= '<br><table width="100%" border="1" class="border text-center small"><tbody>';
				$title .= $header;
				$title .= $sheader;
				$title .= $stheader;
				$title .= '<tr><td colspan="'.(count($childs) + 4).'" style="">&nbsp;<td></tr>';
				$title .= $final_ob_html;


				$title .= '</tr><tr><td style="font-size: 11px;">Total ALOBS for the month</td>';
				$total_afm_per_account_code = array_merge($total_afm_per_account_code, [array_sum($ob_row_total_amount2)]);
				foreach ($total_afm_per_account_code as $key => $value)
				{
					if(number_format($value) != 0)
					{
						$title .= '<td align="right">'.number_format($value).'</td>';
					}
					else
					{
						$title .= '<td align="right"> - </td>';
					}
				}
				$title .= '</tr>';

				$title .= '<tr><td style="font-size: 11px;">Total ALOBS last month</td>';
				$last_month_per_account_code[$month_nos] = $total_afm_per_account_code;
				foreach ($last_month_per_account_code[$month_nos - 1] as $key => $value)
				{
					if(number_format($value) != 0)
					{
						$title .= '<td align="right">'.number_format($value).'</td>';
					}
					else
					{
						$title .= '<td align="right"> - </td>';
					}
				}
				$title .= '</tr>';

				$title .= '<tr><td style="font-size: 12px;">Total to Date</td>';
				$total_to_date 	= $this->sum_totals($total_afm_per_account_code, $last_month_per_account_code[$month_nos - 1]);
				foreach ($total_to_date as $key => $value)
				{
					if(number_format($value) != 0)
					{
						$title .= '<td align="right">'.number_format($value).'</td>';
					}
					else
					{
						$title .= '<td align="right"> - </td>';
					}
				}
				$title .= '</tr>';

				$title .= '<tr><td colspan="'.(count($childs) + 4).'" style="">&nbsp;<td></tr>';

				$title .= '<tr><td style="font-size: 12px;">Balance of allotment</td>';
				$balance = $this->dif_totals($total_to_date, $appropriation);
				foreach ($balance as $key => $value)
				{
					if(number_format($value) != 0)
					{
						$title .= '<td align="right">'.number_format($value).'</td>';
					}
					else
					{
						$title .= '<td align="right"> - </td>';
					}
				}
				$title .= '</tr>';
				$title .= '</tbody></table><br>';

				//END -----------------------------------------------------------------------------

			endforeach;

			$data = [
				'title_head' => $title_head,
				'title' 	 => $title
			];

			$html 			= $this->load->view( 'reports/RAO', $data, TRUE );
			$filename		= 'ROA_REPORT_'.date('F').'_'.date('d').'_'.date('Y');
			//ob_end_clean();
			//$this->pdf( $filename, $html, FALSE, 'I', NULL, NULL, TRUE );
			$this->convert_excel( $html, $filename , '');
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
		}
	}

	public function sum_totals()// function to sum multiple arrays in same keys useful for sum of total in total arrays
	{
		// Combine multiple associative arrays and sum the values for any common keys
		// The function can accept any number of arrays as arguments
		// The values must be numeric or the summed value will be 0
		
		// Get the number of arguments being passed
		$numargs = func_num_args();
		
		// Save the arguments to an array
		$arg_list = func_get_args();
		
		// Create an array to hold the combined data
		$out = array();

		// Loop through each of the arguments
		for ($i = 0; $i < $numargs; $i++) {
			$in = $arg_list[$i]; // This will be equal to each array passed as an argument

			// Loop through each of the arrays passed as arguments
			foreach($in as $key => $value) {
				// If the same key exists in the $out array
				if(array_key_exists($key, $out)) {
					// Sum the values of the common key
					$sum = str_replace(',','', $in[$key]) + str_replace(',','', $out[$key]);
					// Add the key => value pair to array $out
					$out[$key] = $sum;
				}else{
					// Add to $out any key => value pairs in the $in array that did not have a match in $out
					$out[$key] = $in[$key];
				}
			}
		}
		
		return $out;
	}

	public function dif_totals()// function to subtract multiple arrays in same keys useful for subtract of total in total arrays
	{
		// Combine multiple associative arrays and subtract the values for any common keys
		// The function can accept any number of arrays as arguments
		// The values must be numeric or the subtract value will be 0
		
		// Get the number of arguments being passed
		$numargs = func_num_args();
		
		// Save the arguments to an array
		$arg_list = func_get_args();
		
		// Create an array to hold the combined data
		$out = array();

		// Loop through each of the arguments
		for ($i = 0; $i < $numargs; $i++) {
			$in = $arg_list[$i]; // This will be equal to each array passed as an argument

			// Loop through each of the arrays passed as arguments
			foreach($in as $key => $value) {
				// If the same key exists in the $out array
				if(array_key_exists($key, $out)) {
					// subtract the values of the common key
					$dif = str_replace(',','', $in[$key]) - str_replace(',','', $out[$key]);
					// Add the key => value pair to array $out
					$out[$key] = $dif;
				}else{
					// Subtract to $out any key => value pairs in the $in array that did not have a match in $out
					$out[$key] = $in[$key];
				}
			}
		}
		
		return $out;
	}

	
	public function validate()
	{
		//TEST specific validation ONLY
		try
		{
			$params = get_params();

			$this->load->library('form_validation');

			if($params['report_type'] == 'SMFO')
			{
				$this->form_validation->set_rules('office','<b>Office</b>', 'required|trim|max_length[11]|numeric');
			}

			if($params['report_type'] == 'RAO')
			{
				$this->form_validation->set_rules('line_item','<b>Line Item</b>', 'required|trim|max_length[11]|numeric');
				$this->form_validation->set_rules('allotment_class','<b>Allotment Class</b>', 'required|trim|max_length[11]|numeric');
			}

			$this->form_validation->set_rules('from','<b>Start/From Date</b>', 'required|trim');
			$this->form_validation->set_rules('fund_source','<b>Fund Source</b>', 'required|trim|max_length[11]|numeric');

			$this->form_validation->set_rules('report_type','<b>Report Type</b>', 'required|trim|max_length[10]|alpha');
			$this->form_validation->set_rules('to','<b>To</b>', 'required|trim');
			$this->form_validation->set_rules('year','<b>Year</b>', 'required|trim|max_length[4]|numeric');
			$this->form_validation->set_rules('file_type','<b>File Type PDF/EXCEL</b>', 'required|trim|max_length[1]|min_length[1]|numeric');

			if($this->form_validation->run() == FALSE) echo json_encode(validation_errors());
			else echo 1;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
		}
	}
		
	
}


/* End of file users.php */
/* Location: ./application/modules/sysad/controllers/users.php */