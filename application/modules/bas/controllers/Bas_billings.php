<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_billings extends BAS_Controller {

	private $module = MODULE_BAS_BILLINGS;

	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	
	public function __construct()
	{
		parent::__construct();

		$this->load->model('Bas_billings_model', 'billings');

		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);
	}
	
	public function index()
	{
		try
		{	
			$data 		= [];

			$resources 	= [
				'load_css' 	=> [
					CSS_DATATABLE,
					CSS_SELECTIZE,
					CSS_MODAL_COMPONENT,
					CSS_DATETIMEPICKER
				],
				'load_js' 	=> [
					JS_DATATABLE,
					JS_SELECTIZE,
					JS_MODAL_CLASSIE,
					JS_MODAL_EFFECTS,
					JS_DATETIMEPICKER,
				],
				'load_modal' 	=> [
					'modal_add' 		=> [
						'controller'	=> __CLASS__,
						'method'		=> 'modal_add', // DEFAULT VALUE IS MODAL
						'module'		=> PROJECT_BAS,
						'title'			=> 'Billings',
						'multiple'   	=> true,
					],	
				],	
				'datatable'		=> [
					[
						'table_id' => 'tbl_billings', 
						'path' => PROJECT_BAS.'/bas_billings/get_billings_list'
					]
				]
			];
			
			$this->template->load('billings', $data, $resources);

		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	public function check_exceed_amount_due()
	{
		$params = get_params();

		$this->_validate($params);

		$data = $this->billings->check_exceed_amount_due($params['amount_due'], $params['os_no']);

		$due 	= str_replace(',','',$params['amount_due']);

		if($data['obligated_amount'] <= $due) 
			echo '<b>Amount due</b> cannot <b>exceed</b> on the obligated amount of <b>'.number_format($data['obligated_amount']).'</b>.';
	}
	
	public function get_billings_list()
	{
		try
		{
			$params = get_params();
			$cnt = 0;
			$flag = 0;
		
			$fields = array('amount_due', 'bill_no', 'bill_date', 'due_date', 'os_no');
			$selects = array('bill_no', 'bill_date', 'due_date', 'os_no');
		
			$list 			= $this->billings->get_billings_list($fields, $selects, $params);
			$iTotal 		= $this->billings->total_length();
			$iFilteredTotal = $this->billings->filtered_length($fields, $selects, $params);

			$output = array(
				"sEcho" => intval($_POST['sEcho']),
				"iTotalRecords" => $iTotal["cnt"],
				"iTotalDisplayRecords" => $iFilteredTotal["cnt"],
				"aaData" => array()
			);
		
			foreach ($list as $aRow):
				$cnt++;
				$row = array();
				$action = "";

				$id	= $aRow["billing_id"];
				
				$hash_id 		= $this->hash($id); 
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;

				$count = $this->billings->get_billings_count($id);
				$ctr = $count['cnt'];

				$delete_action 	= 'content_delete("Billing ", "'.$url.'")';

				for ( $i=0 ; $i<count($selects) ; $i++ )
				{
					$row[] = $aRow[ $selects[$i] ];
				}
				
				$action = "<div class='table-actions'>";
					if($this->permission_view):
						$action .= "<a href='javascript:void(0)' class='tooltip view md-trigger tooltipped' data-tooltip='View' data-position='bottom' data-delay='50' data-modal='modal_add' onclick=\"modal_add_init('".$url."/view')\"></a>";
					endif;
					
					if($this->permission_edit) :
						$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_add' onclick=\"modal_add_init('".$url."')\"></a>";
					endif;
					
					if ($this->permission_delete) : 	  
						$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					endif;
				$action .= '</div>';
				
				$row[]	= $action;
					
				$output['aaData'][] = $row;

			endforeach;
		
			echo json_encode( $output );

		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
			$this->get_user_message($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}
	
	
	public function modal_add($encoded_id, $salt, $token)
	{		
		try
		{
			$data 		= array();
			$resources 	= array();
			
			if(!EMPTY($encoded_id))
			{
				check_salt($encoded_id, $salt, $token);
				
				$hash_id 		= base64_url_decode($encoded_id);
				$key 			= $this->get_hash_key('b.billing_id'); 
				$where			= array();
				$where[$key]	= $hash_id;

				$info = $this->billings->get_specific_billings_2($key, $hash_id);

				if(EMPTY($info)) throw new Exception($this->lang->line('err_invalid_request'));	

				foreach ($info as $key => $value)
				{
					$bill_date1 = new DateTime($value['due_date']);
					$bill_date2 = new DateTime($value['bill_date']);

					$bill_date 	= $bill_date2->format('Y/m/d'); // 2012-07-31
					$due_date 	= $bill_date1->format('Y/m/d'); // 2012-07-31

					$data['billing_id'] 	= $value['billing_id'];
					$data['obligation_id'] 	= $value['obligation_id'];
					$data['bill_no'] 		= $value['bill_no'];	
					$data['bill_date'] 		= $bill_date;
					$data['due_date'] 		= $due_date;
					$data['amount_due'] 	= $value['amount_due'];
					$data['os_no'] 			= $value['os_no'];
					$data["readonly_value"]	= "readonly";	

					$billing_id 			= $value['billing_id'];

					$data["data"] = $this->billings->get_billings_details($billing_id);
				}
			}
			else
			{
				$hash_id 	= $this->hash(0);				
			}
			
			$encoded_id			= base64_url_encode($hash_id);
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			$data['slips']	= $this->billings->get_obligation_slip_nos();

			$resources = array(
				'load_css' => array(
					'selectize.default'
				),
				'load_js' 	=> array(
					'popModal.min',
					'selectize',
					//'modules/'.PROJECT_BAS.'/'.strtolower( __CLASS__ )
				),
				'loaded_init'=> array(
					'datepicker_init();',
					'selectize_init();'
				)
			);
			
			$this->load->view("modals/billings", $data);
			$this->load_resources->get_resource($resources);
		}
		catch( PDOException $e )
		{
			$this->rlog_error($e);
		}
		catch( Exception $e )
		{
			$this->rlog_error($e);
		}
	}

	
	public function process()
	{
		try
		{
			$flag 	= 0;
            $class  = ERROR;
			$params = get_params();

			$this->_validate($params);
	
			$key 			= $this->get_hash_key('billing_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->billings->get_specific_billings($where);
			$id				= $info['billing_id'];
			
			BAS_Model::beginTransaction();

			if(EMPTY($id))
			{
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= BAS_Model::tbl_billings;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= array();
				$curr_detail[]	= array($params);

				$this->billings->insert_billings($params);

				$activity	= "New Billing has been added.";

				$msg  = $this->lang->line('data_saved');
			}
			else
			{
				$audit_action[]	= AUDIT_UPDATE;
				$audit_table[]	= BAS_Model::tbl_billings;
				$audit_schema[]	= DB_BAS;
				$prev_detail[]	= array($info);
				
				$params['billing_id']	= $id;

				$this->billings->update_billings($params);
				
				$info2			= $this->billings->get_specific_billings(array("billing_id" => $id) , TRUE);

				$curr_detail[]	= array($info2);
				
				$activity	= "Billing has been updated.";

				$msg  = $this->lang->line('data_updated');
			}
			
			$activity = sprintf($activity, $params['bill_no']);

			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table,
				$audit_schema
			);
			
			BAS_Model::commit();
			
			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
			$this->get_user_message($e);
			
		}
		catch(Exception $e)
		{
			BAS_Model::rollback();
			$msg = $this->rlog_error($e, TRUE);
		}
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
		echo json_encode($info);
	}
	
	private function _validate(&$params)
	{
		try
		{
			$this->_validate_security($params);
			
			$fields = array(
					'bill_no' 			=> 'Bill No',
					'bill_date' 		=> 'Bill Date',
					'due_date' 			=> 'Due Date',
					'os_no' 			=> 'Obligation Slip No',
					'amount_due' 		=> 'Amount Due'
			);	
			
			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

    private function _validate_security(&$params)
    {
        try
        {
            if(EMPTY($params['security']))
            {
                throw new Exception($this->lang->line('err_invalid_request'));
            }


            $security = explode('/', $params['security']);

            $params['encoded_id']   = $security[0];
            $params['salt']         = $security[1];
            $params['token']        = $security[2];

            check_salt($params['encoded_id'], $params['salt'], $params['token']);

            $params['hash_id']      = base64_url_decode($params['encoded_id']);
        }
        catch(Exception $e)
        {
            throw $e;
        }
    }
	
	private function _validate_input($params)
	{
		try
		{
			$validation = array(
				'bill_no' => array(
					'data_type' => 'string',
					'name'		=> 'Bill no',
					'max_len'	=> 100
				),
				'bill_date' => array(
					'data_type' => 'string',
					'name'		=> 'Bill date',
					'max_len'	=> 20
				),
				'os_no' => array(
					'data_type' => 'digit',
					'name'		=> 'OS no',
					'max_len'	=> 11
				),
				'due_date' => array(
					'data_type' => 'string',
					'name'		=> 'Due date',
					//'max_len'	=> 1
				),
				'amount_due' => array(
					'data_type' => 'amount',
					'name'		=> 'Amount due',
					'min_len'	=> 1
				)
			);
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function delete_billing($params)
	{
		try
		{
			$flag	= 0;
			$msg	= "Error";

			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->validate_security($params);

			$key 			= $this->get_hash_key('billing_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 			= $this->billings->get_specific_billings($where);
			
			if(EMPTY($info['billing_id']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			$this->billings->delete_billings($info['billing_id']);

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			$msg	= $e->getMessage(); 
			$this->get_user_message($e);	
		}
		catch(Exception $e)
		{	
			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag" 		=> $flag,
			"msg" 		=> $msg,
			"reload" 	=> 'datatable',
			"table_id" 	=> 'tbl_billings',
			"path"		=> PROJECT_BAS . '/bas_billings/get_billings_list/'
		);

		echo json_encode($info);

	}
	
}