<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_realignment_model extends BAS_Model {

	//fixed code define
	var $find_code_ps   	= PARAM_ACCOUNT_CODE_PS;//501
	var $find_code_mooe   	= PARAM_ACCOUNT_CODE_MOOE;//502
	var $find_code_co   	= PARAM_ACCOUNT_CODE_CO;//503

	//fix status
	var $voided   			= STATUS_VOIDED;
	var $excess   			= STATUS_EXCESS;
	var $overriden   		= STATUS_OVERRIDEN;
	var $posted   			= STATUS_POSTED;
	var $submitted   		= STATUS_SUBMITTED;
	var $approved   		= STATUS_APPROVED;
	var $disapproved   		= STATUS_DISAPPROVED;
	var $drafted   			= STATUS_DRAFTED;

	public function __construct()
	{
		parent::__construct();
		
	}

	public function get_dropdowns($table)
	{
		try
		{
			if($table == 'account_codes')
			{
				$tbl1 = BAS_Model::tbl_param_account_codes;
				$tbl2 = BAS_Model::tbl_line_item_accounts;

				$query = <<<EOS
				SELECT 

				pac.account_code,
				pac.account_name,

				lia.account_code

				FROM $tbl1 pac
				JOIN $tbl2 lia ON lia.account_code = pac.account_code
EOS;

				return $this->query($query);
			}
			elseif($table == 'fund_sources')
			{
				$tbl1 = BAS_Model::tbl_fund_sources;
				$tbl2 = BAS_Model::tbl_param_source_types;

				$query = <<<EOS
				SELECT 

				fs.year, 
				fs.source_type_num, 

				pst.source_type_num, 
				pst.source_type_name

				FROM $tbl2 pst
				JOIN $tbl1 fs 

				ON fs.source_type_num = pst.source_type_num
				GROUP BY pst.source_type_num
				ORDER BY pst.source_type_name ASC 
EOS;

				return $this->query($query);
			}
			elseif($table == 'fund_year')
			{
				$tbl1 = BAS_Model::tbl_fund_sources;

				$query = <<<EOS
				SELECT 

				year

				FROM $tbl1

				GROUP BY year
				ORDER BY year ASC 
EOS;

				return $this->query($query);
			}
			elseif($table == 'line_items')
			{
				$tbl1 = BAS_Model::tbl_line_items;

				$query = <<<EOS
				SELECT 
				line_item_id,
				line_item_name
				FROM $tbl1
EOS;

				return $this->query($query);
			}
			elseif($table == 'account_codes_list')
			{
				return $this->select_all(array('account_name','account_code'),BAS_Model::tbl_param_account_codes);
			}
			elseif($table == 'payees')
			{
				return $this->select_all(array('*'),BAS_Model::tbl_param_payees);
			}
			elseif($table == 'offices')
			{
				return $this->select_all(array('*'),BAS_Model::tbl_param_offices);
			}
			else return false;
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_specific_realignment($where, $multi=FALSE){

		try
		{
			$fields 	= array("*");
			$function	= ($multi) ? "select_all" : "select_one";
			
			return $this->{$function}($fields, BAS_Model::tbl_line_item_realigns, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);	
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_realignment_details($id)
	{
		try
		{
			$fields = array("*");
			$where = array('realign_id' => $id);
			return $this->select_one($fields, BAS_Model::tbl_line_item_realign_details, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	
	
	
	public function get_realignment_list($fields, $selects, $params)
	{
		try
		{
			$c_columns = array(
				"release_date", "reference_no", "realigned_amount", "IF(posted_flag = 1, '$this->drafted', '$this->posted')"
			);
			$tbl_fields = str_replace(" , ", " ", implode(", ", $fields));
			
			$where = $this->filtering($c_columns, $params, FALSE);
			$order = $this->ordering($selects, $params);
			$limit = $this->paging($params);
			
			$filter_str = $where["search_str"];
			$filter_params = $where["search_params"];

			$tbl1 = BAS_Model::tbl_line_items;
			$tbl2 = BAS_Model::tbl_line_item_realigns;
			$tbl3 = BAS_Model::tbl_line_item_realign_details;
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS 
				li.line_item_id,
				li.line_item_name,
				
				lir.realign_id,
				lir.source_type_num,
				lir.release_date,
				lir.reference_no,
				IF(lir.posted_flag = 1, '$this->drafted', '$this->posted') posted_flag,

				lird.realign_id,
				FORMAT(lird.realigned_amount, 0) as realigned_amount,
				lird.source_line_item_id,
				lird.source_account_code,
				lird.recipient_line_item_id

				FROM $tbl2 lir
				JOIN $tbl3 lird ON lird.realign_id = lir.realign_id
				JOIN $tbl1 li ON  li.line_item_id = lird.source_line_item_id
				$filter_str
	        	$order
	        	$limit
EOS;
	
			$stmt = $this->query($query, $filter_params);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	

	public function get_line_item_names($id)
	{
		$name = $this->select_one('line_item_name', BAS_Model::tbl_line_items, array('line_item_id'=>$id));
		return $name['line_item_name'];
	}
	
	
	public function filtered_length($fields, $selects, $params)
	{
		try
		{
			$this->get_realignment_list($fields, $selects, $params);
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(realign_id) cnt");
			return $this->select_one($fields, BAS_Model::tbl_line_item_realigns);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function check_excess_amount($params)
	{
		try
		{
			$where = array('line_item_id'=>$params['source_line_item_id'],'account_code'=>$params['source_account_code']);
			$amount = $this->select_one(array('SUM(allotment_amount + realigned_amount + obligated_amount) as current_amount'), BAS_Model::tbl_line_item_accounts, $where);
			
			if($amount['current_amount'] <= str_replace(',','',$params['realigned_amount']))
			return 1;
			else return 0;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function insert_realignment($params)
	{
		try
		{
			$date = new DateTime($params['release_date']);
			$newdate = $date->format('Y-m-d'); // 2012-07-31

			$data2 = array(
				'year'						=> filter_var($params['source_year'], FILTER_SANITIZE_NUMBER_INT),
				'release_date' 				=> filter_var($newdate, FILTER_SANITIZE_NUMBER_INT),
				'reference_no' 				=> filter_var($params['reference_no'], FILTER_SANITIZE_STRING),
				'posted_flag' 				=> filter_var($params['status'], FILTER_SANITIZE_NUMBER_INT),
				'source_type_num' 			=> filter_var($params['source_type_num'], FILTER_SANITIZE_STRING),
				'created_by' 				=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
				'created_date' 				=> date('Y-m-d H:i:s'),
			);

			$id = $this->insert_data(BAS_Model::tbl_line_item_realigns, $data2, TRUE);

			$data3 = array(
				'realign_id'				=> $id,
				'source_line_item_id' 		=> filter_var($params['source_line_item_id'], FILTER_SANITIZE_NUMBER_INT),
				'source_account_code' 		=> filter_var($params['source_account_code'], FILTER_SANITIZE_NUMBER_INT),
				'recipient_line_item_id' 	=> filter_var($params['recipient_line_item_id'], FILTER_SANITIZE_NUMBER_INT),
				'recipient_account_code' 	=> filter_var($params['recipient_account_code'], FILTER_SANITIZE_NUMBER_INT),
				'realigned_amount' 			=> filter_var(str_replace(',','', $params['realigned_amount']), FILTER_SANITIZE_NUMBER_FLOAT),
			);
			
			$this->insert_data(BAS_Model::tbl_line_item_realign_details, $data3);

			if($params['status'] == 0)://if post is not draft
			
				$source 	= $params['source_account_code'];
				$recipient 	= $params['recipient_account_code'];

				//check code matches sosurce
				$pos1  	= strpos($source, $this->find_code_ps);
				$pos2 	= strpos($source, $this->find_code_mooe);
				$pos3 	= strpos($source, $this->find_code_co);

				//check code matches recipient
				$pos11  = strpos($recipient, $this->find_code_ps);
				$pos22 	= strpos($recipient, $this->find_code_mooe);
				$pos33 	= strpos($recipient, $this->find_code_co);

				if ($pos1 !== false || $pos11 !== false) $identity = 'ps';
				if ($pos2 !== false || $pos22 !== false) $identity = 'mooe';
				if ($pos3 !== false || $pos33 !== false) $identity = 'co';
				
				//------- source
				$source_get = $this->select_one(
					array('line_item_id','account_code','appropriation_amount','allotment_amount','realigned_amount'),
					BAS_Model::tbl_line_item_accounts,
					array('line_item_id'=> $params['source_line_item_id'],'account_code'=>$params['source_account_code'])
				);

				$old_source_amount 	= $this->get_line_item_realignment_amount($source_get['line_item_id'], $identity);

				$source_data = array(
					''.$identity.'_appropriation_realigned' 	=> ($old_source_amount[''.$identity.'_appropriation_realigned'] + $source_get['appropriation_amount']),
					''.$identity.'_allotment_realigned' 		=> ($old_source_amount[''.$identity.'_allotment_realigned'] + $source_get['allotment_amount']),
				);

				$this->update_data(
					BAS_Model::tbl_line_items, 
					$source_data, 
					array('line_item_id'=> $source_get['line_item_id'])
				);

				//add the realignment amount to line item accounts by source
				$current_source = $this->select_one(['realigned_amount'], BAS_Model::tbl_line_item_accounts,['account_code'=>$params['source_account_code'], 'line_item_id'=>$params['source_line_item_id']]);
				
				$this->update_data(
					BAS_Model::tbl_line_item_accounts,
					['realigned_amount'=> ($current_source['realigned_amount'] - str_replace(',','',$params['realigned_amount']))],
					['account_code'=>$params['source_account_code'], 'line_item_id'=>$params['source_line_item_id']]
				);
				//-----------------------------------------------------------------------------------------------------

				//------ recipient
				$recipient_get = $this->select_one(
					array('line_item_id','account_code','appropriation_amount','allotment_amount','realigned_amount'),
					BAS_Model::tbl_line_item_accounts,
					array('line_item_id'=> $params['recipient_line_item_id'],'account_code'=>$params['recipient_account_code'])
				);
				
				$old_recipient_amount	= $this->get_line_item_realignment_amount($recipient_get['line_item_id'], $identity);

				$recipient_data = array(
					''.$identity.'_appropriation_realigned' 	=> ($old_recipient_amount[''.$identity.'_appropriation_realigned'] + $recipient_get['appropriation_amount']),
					''.$identity.'_allotment_realigned' 		=> ($old_recipient_amount[''.$identity.'_allotment_realigned'] + $recipient_get['allotment_amount']),
				);

				$this->update_data(
					BAS_Model::tbl_line_items, 
					$recipient_data, 
					array('line_item_id'=> $recipient_get['line_item_id'])
				);

				//add the realignment amount to line item accounts by recipient select
				$current_recipient = $this->select_one(['realigned_amount'], BAS_Model::tbl_line_item_accounts,['account_code'=>$params['recipient_account_code'], 'line_item_id'=>$params['recipient_line_item_id']]);

				$this->update_data(
					BAS_Model::tbl_line_item_accounts,
					['realigned_amount'=> ($current_recipient['realigned_amount'] + str_replace(',','',$params['realigned_amount']))],
					['account_code'=>$params['recipient_account_code'], 'line_item_id'=>$params['recipient_line_item_id']]
				);
				//-----------------------------------------------------------------------------------------------------

			endif;

			return $id;
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e,TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e,TRUE);
			throw $e;		
		}
	}
	
	public function update_realignment($params)
	{
		try
		{
			$date = new DateTime($params['release_date']);
			$newdate = $date->format('Y-m-d'); // 2012-07-31

			$where = array('realign_id' => $params['realign_id']);

			$data = array(
				'release_date' 				=> filter_var($newdate, FILTER_SANITIZE_NUMBER_INT),
				'reference_no' 				=> filter_var($params['reference_no'], FILTER_SANITIZE_STRING),
				'posted_flag' 				=> filter_var($params['status'], FILTER_SANITIZE_NUMBER_INT),
				'modified_date'				=> date('Y-m-d H:i:s'),
				'modified_by'				=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1'
			);

			if(!empty($params['year']))
				$data['year']					= filter_var($params['year'], FILTER_SANITIZE_NUMBER_INT);
			if(!empty($params['source_type_num']))
				$data['source_type_num']		= filter_var($params['source_type_num'], FILTER_SANITIZE_NUMBER_INT);
			
			$this->update_data(BAS_Model::tbl_line_item_realigns, $data, $where);

			if($params['status'] == 0): //equal to post not draft

				//get the old realigned amount
				$selects = array('source_line_item_id', 'recipient_line_item_id', 'source_account_code', 'recipient_account_code', 'realign_id');

				$old_details = $this->select_one($selects, BAS_Model::tbl_line_item_realign_details, $where);

				$source_line_item_id 		= $old_details['source_line_item_id'];
				$recipient_line_item_id 	= $old_details['recipient_line_item_id'];
				$source_account_code 		= $old_details['source_account_code'];
				$recipient_account_code 	= $old_details['recipient_account_code'];

				//get source cur amount then deduct to line items realigned_amount
				$selected_source 		= $this->select_one(
					array('line_item_id','appropriation_amount','allotment_amount','account_code', 'realigned_amount'),
					BAS_Model::tbl_line_item_accounts,
					array('line_item_id'=>$source_line_item_id,'account_code'=>$source_account_code)
				);

				$selected_recipient 	= $this->select_one(
					array('line_item_id','appropriation_amount','allotment_amount','account_code', 'realigned_amount'),
					BAS_Model::tbl_line_item_accounts,
					array('line_item_id'=>$recipient_line_item_id,'account_code'=> $recipient_account_code)
				);

				//check code matches source
				$pos1  	= strpos($selected_source['account_code'], $this->find_code_ps);
				$pos2 	= strpos($selected_source['account_code'], $this->find_code_mooe);
				$pos3 	= strpos($selected_source['account_code'], $this->find_code_co);

				$pos11  = strpos($selected_recipient['account_code'], $this->find_code_ps);
				$pos22 	= strpos($selected_recipient['account_code'], $this->find_code_mooe);
				$pos33 	= strpos($selected_recipient['account_code'], $this->find_code_co);

				if ($pos1 !== false || $pos11 !== false) $identity = 'ps';
				if ($pos2 !== false || $pos22 !== false) $identity = 'mooe';
				if ($pos3 !== false || $pos33 !== false) $identity = 'co';

				//--------------------------------------------------------------------------------------------------------------------
				//subtract the old
				$source_current_alignment 		= $this->get_line_item_realignment_amount($selected_source['line_item_id'],$identity);
				$recipient_current_alignment 	= $this->get_line_item_realignment_amount($selected_recipient['line_item_id'],$identity);
				//source
				$new_source_realigned = array(
					''.$identity.'_appropriation_realigned'	=> ($source_current_alignment[''.$identity.'_appropriation_realigned'] - $selected_source['appropriation_amount']),
					''.$identity.'_allotment_realigned'		=> ($source_current_alignment[''.$identity.'_allotment_realigned'] - $selected_source['allotment_amount'])
				);

				$this->update_data(BAS_Model::tbl_line_items,$new_source_realigned,array('line_item_id'=>$selected_source['line_item_id']));

				//update realign amount/subtract old on the line item accounts
				$this->update_data(
					BAS_Model::tbl_line_item_accounts,
					['realigned_amount'=> ($selected_source['realigned_amount'] - $selected_source['realigned_amount'])],
					['account_code'=>$selected_source['source_account_code'], 'line_item_id'=>$selected_source['source_line_item_id']]
				);

				//recipient
				$new_recipient_realigned = array(
					''.$identity.'_appropriation_realigned'	=> ($recipient_current_alignment[''.$identity.'_appropriation_realigned'] - $selected_recipient['appropriation_amount']),
					''.$identity.'_allotment_realigned'		=> ($recipient_current_alignment[''.$identity.'_allotment_realigned'] -  $selected_recipient['allotment_amount'])
				);

				$this->update_data(BAS_Model::tbl_line_items,$new_recipient_realigned,array('line_item_id'=>$selected_recipient['line_item_id']));

				//update realign amount/subtract old on the line item accounts
				$this->update_data(
					BAS_Model::tbl_line_item_accounts,
					['realigned_amount'=> ($selected_recipient['realigned_amount'] - $selected_recipient['realigned_amount'])],
					['account_code'=>$selected_recipient['source_account_code'], 'line_item_id'=>$selected_recipient['source_line_item_id']]
				);
				//--------------------------------------------------------------------------------------------------------------------
				
				//add the new-------------

				//------- source
				$source_get = $this->select_one(
					array('line_item_id','account_code','appropriation_amount','allotment_amount','realigned_amount'),
					BAS_Model::tbl_line_item_accounts,
					array('line_item_id'=> $params['source_line_item_id'],'account_code'=>$params['source_account_code'])
				);

				$old_source_amount 	= $this->get_line_item_realignment_amount($source_get['line_item_id'], $identity);

				$source_data = array(
					''.$identity.'_appropriation_realigned' 	=> (floatval($old_source_amount[''.$identity.'_appropriation_realigned']) + floatval($source_get['appropriation_amount'])),
					''.$identity.'_allotment_realigned' 		=> (floatval($old_source_amount[''.$identity.'_allotment_realigned']) + floatval($source_get['allotment_amount'])),
				);

				$this->update_data(
					BAS_Model::tbl_line_items, 
					$source_data, 
					array('line_item_id'=> $source_get['line_item_id'])
				);

				//update realign amount/subtract old on the line item accounts
				$this->update_data(
					BAS_Model::tbl_line_item_accounts,
					['realigned_amount'=> ($source_get['realigned_amount']-str_replace(',','',$params['realigned_amount']))],
					['account_code'=>$params['source_account_code'], 'line_item_id'=>$params['source_line_item_id']]
				);
				//-----------------

				//------ recipient
				$recipient_get = $this->select_one(
					array('line_item_id','account_code','appropriation_amount','allotment_amount','realigned_amount'),
					BAS_Model::tbl_line_item_accounts,
					array('line_item_id'=> $params['recipient_line_item_id'],'account_code'=>$params['recipient_account_code'])
				);
				
				$old_recipient_amount	= $this->get_line_item_realignment_amount($recipient_get['line_item_id'], $identity);

				$recipient_data = array(
					''.$identity.'_appropriation_realigned' 	=> (floatval($old_recipient_amount[''.$identity.'_appropriation_realigned']) + floatval($recipient_get['appropriation_amount'])),
					''.$identity.'_allotment_realigned' 		=> (floatval($old_recipient_amount[''.$identity.'_allotment_realigned']) + floatval($recipient_get['allotment_amount'])),
				);

				$this->update_data(
					BAS_Model::tbl_line_items, 
					$recipient_data, 
					array('line_item_id'=> $recipient_get['line_item_id'])
				);

				//update realign amount/subtract old on the line item accounts
				$this->update_data(
					BAS_Model::tbl_line_item_accounts,
					['realigned_amount'=> ($recipient_get['realigned_amount']+str_replace(',','',$params['realigned_amount']))],
					['account_code'=>$params['recipient_account_code'], 'line_item_id'=>$params['recipient_line_item_id']]
				);
			endif;

			$data2 = array(
				'source_line_item_id' 		=> filter_var($params['source_line_item_id'], FILTER_SANITIZE_NUMBER_INT),
				'source_account_code'		=> filter_var($params['source_account_code'], FILTER_SANITIZE_NUMBER_INT),
				'recipient_line_item_id'	=> filter_var($params['recipient_line_item_id'], FILTER_SANITIZE_NUMBER_INT),
				'recipient_account_code'	=> filter_var($params['recipient_account_code'], FILTER_SANITIZE_NUMBER_INT),
				'realigned_amount'			=> filter_var(floatval(str_replace(',','', $params['realigned_amount'])), FILTER_SANITIZE_NUMBER_FLOAT)
			);
			
			$this->update_data(BAS_Model::tbl_line_item_realign_details, $data2, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}
	
	public function delete_realignment($id)
	{
		try
		{
			$selects = array('source_line_item_id', 'recipient_line_item_id', 'source_account_code', 'recipient_account_code', 'realigned_amount');
			$r_details = $this->select_one($selects ,BAS_Model::tbl_line_item_realign_details, array('realign_id'=>$id));

			$source_line_item_id 		= $r_details['source_line_item_id'];
			$recipient_line_item_id 	= $r_details['recipient_line_item_id'];
			$source_account_code 		= $r_details['source_account_code'];
			$recipient_account_code 	= $r_details['recipient_account_code'];
			$realigned_amount 			= $r_details['realigned_amount'];

			//get source cur amount then deduct to line items realigned_amount
			$selected_source 		= $this->select_one(
				array('line_item_id','appropriation_amount','allotment_amount','account_code', 'realigned_amount'),
				BAS_Model::tbl_line_item_accounts,
				array('line_item_id'=>$source_line_item_id,'account_code'=>$source_account_code)
			);

			$selected_recipient 	= $this->select_one(
				array('line_item_id','appropriation_amount','allotment_amount','account_code', 'realigned_amount'),
				BAS_Model::tbl_line_item_accounts,
				array('line_item_id'=>$recipient_line_item_id,'account_code'=>$recipient_account_code)
			);

			//check code matches source
			$pos1  	= strpos($selected_source['account_code'], $this->find_code_ps);
			$pos2 	= strpos($selected_source['account_code'], $this->find_code_mooe);
			$pos3 	= strpos($selected_source['account_code'], $this->find_code_co);

			$pos11  = strpos($selected_recipient['account_code'], $this->find_code_ps);
			$pos22 	= strpos($selected_recipient['account_code'], $this->find_code_mooe);
			$pos33 	= strpos($selected_recipient['account_code'], $this->find_code_co);

			if ($pos1 !== false || $pos11 !== false) $identity = 'ps';
			if ($pos2 !== false || $pos22 !== false) $identity = 'mooe';
			if ($pos3 !== false || $pos33 !== false) $identity = 'co';

			//get current realign amount both source and recipient
			$source_current_alignment 		= $this->get_line_item_realignment_amount($selected_source['line_item_id'],$identity);
			$recipient_current_alignment 	= $this->get_line_item_realignment_amount($selected_recipient['line_item_id'],$identity);

			//revert realigned amount
			$this->update_data(//for source revert
				BAS_Model::tbl_line_item_accounts,
				['realigned_amount'=> ($selected_source['realigned_amount'] + $realigned_amount)],
				['account_code'=>$source_account_code, 'line_item_id'=>$source_line_item_id]
			);

			$this->update_data(//for recipient revert
				BAS_Model::tbl_line_item_accounts,
				['realigned_amount'=> ($selected_recipient['realigned_amount'] - $realigned_amount)],
				['account_code'=>$recipient_account_code, 'line_item_id'=>$recipient_line_item_id]
			);
			//------------------------------------------------------------

			$new_source_realigned = array(
				''.$identity.'_appropriation_realigned'	=> ($source_current_alignment[''.$identity.'_appropriation_realigned'] - $selected_source['appropriation_amount']),
				''.$identity.'_allotment_realigned'		=> ($source_current_alignment[''.$identity.'_allotment_realigned'] - $selected_source['allotment_amount'])
			);

			$this->update_data(BAS_Model::tbl_line_items,$new_source_realigned,array('line_item_id'=>$selected_source['line_item_id']));

			$new_recipient_realigned = array(
				''.$identity.'_appropriation_realigned'	=> ($recipient_current_alignment[''.$identity.'_appropriation_realigned'] - $selected_recipient['appropriation_amount']),
				''.$identity.'_allotment_realigned'		=> ($recipient_current_alignment[''.$identity.'_allotment_realigned'] -  $selected_recipient['allotment_amount'])
			);

			$this->update_data(BAS_Model::tbl_line_items,$new_recipient_realigned,array('line_item_id'=>$selected_recipient['line_item_id']));
			
			//delete ralignment and realignment details after the above process
			$this->delete_data(BAS_Model::tbl_line_item_realign_details, array('realign_id' => $id));	
			$this->delete_data(BAS_Model::tbl_line_item_realigns, array('realign_id' => $id));			
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}

	public function delete_drow($where, $table)
	{
		try
		{
			if($table == 'line_items')
			{
				$this->delete_data(BAS_Model::tbl_obligation_charges, $where);
			}
			else
			{
				$this->delete_data(BAS_Model::tbl_obligation_signatories, $where);
			}
			
			return true;
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}

	public function get_line_item_realignment_amount($line_item_id, $pos)
	{
		$selects = array('line_item_id',''.$pos.'_appropriation_realigned',''.$pos.'_allotment_realigned');
		return $this->select_one($selects, BAS_Model::tbl_line_items, array('line_item_id'=>$line_item_id));
	}

	public function get_realignment_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("realign_id" => $id);
			return $this->select_one($fields, BAS_Model::tbl_line_item_realigns, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_line_items_by_fund_sources($params)
	{
		try
		{	
			$year = $params['year'];
			$id = $params['id'];

			$line_items = BAS_Model::tbl_line_items;

			$query = <<<EOS
				SELECT 
				ac.line_item_name,
				ac.line_item_id,
				ac.parent_line_item_id,
				ac.year,
				ac.source_type_num 
				FROM $line_items c
				JOIN $line_items a ON a.parent_line_item_id = c.line_item_id
				JOIN $line_items ac ON ac.parent_line_item_id = c.line_item_id
				WHERE ac.year = '$year' and ac.source_type_num = '$id' AND ac.line_item_id != c.parent_line_item_id
EOS;
			return $this->query($query);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_fund_sources_by_year($params)
	{
		try
		{	
			$year = $params['year'];

			$fund_sources 			= BAS_Model::tbl_fund_sources;
			$param_source_types 	= BAS_Model::tbl_param_source_types;

			$query = <<<EOS
			SELECT

			fs.year,
			fs.source_type_num,
			pst.source_type_num,
			pst.source_type_name

			FROM $fund_sources fs
			JOIN $param_source_types pst ON fs.source_type_num = pst.source_type_num

			WHERE fs.year = '$year'
			GROUP BY fs.year
EOS;
			return $this->query($query);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_account_codes_by_line_item($params)
	{
		try
		{	
			$line_item_id = ($params['id'] != '') ? $params['id'] : 0;

			$param_account_codes 	= BAS_Model::tbl_param_account_codes;
			$line_item_acounts 		= BAS_Model::tbl_line_item_accounts;

			$query = <<<EOS
			SELECT
			lia.line_item_id,
			lia.account_code,

			pcc.account_code,
			pcc.account_name

			FROM $line_item_acounts lia
			JOIN $param_account_codes pcc ON pcc.account_code = lia.account_code

			WHERE lia.line_item_id = $line_item_id
EOS;
			return $this->query($query);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_account_codes_by_amount($params)
	{
		try
		{	
			$where = array('line_item_id'=>$params['line_item_id'],'account_code'=>$params['account_code']);
			$amount = $this->select_one(array('SUM(allotment_amount + realigned_amount + obligated_amount) as current_amount'), BAS_Model::tbl_line_item_accounts, $where);
			return number_format($amount['current_amount']);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	
}