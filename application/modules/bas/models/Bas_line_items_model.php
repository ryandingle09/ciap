<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_line_items_model extends BAS_Model {

	//fixed code define
	var $find_code_ps   	= PARAM_ACCOUNT_CODE_PS;//501
	var $find_code_mooe   	= PARAM_ACCOUNT_CODE_MOOE;//502
	var $find_code_co   	= PARAM_ACCOUNT_CODE_CO;//503

	//fix status
	var $voided   			= STATUS_VOIDED;
	var $excess   			= STATUS_EXCESS;
	var $overriden  		= STATUS_OVERRIDEN;
	var $posted   			= STATUS_POSTED;
	var $submitted   		= STATUS_SUBMITTED;
	var $approved   		= STATUS_APPROVED;
	var $disapproved   		= STATUS_DISAPPROVED;

	public function __construct()
	{
		parent::__construct();
	}

	public function validate_total_office_amount($id)
	{
		try
		{
			return $this->select_one(['mooe_appropriation', 'mooe_allotment'], BAS_Model::tbl_line_items, ['line_item_id'=>$id]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;			
		}
	}

	public function get_specific_line_item_for_audit_trail($id){

		try
		{
			return $this->select_one(['*'], BAS_Model::tbl_line_items, ['line_item_id'=> $id]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;			
		}
	}

	public function get_specific_line_items($key, $id){

		try
		{
			$table1 = BAS_Model::tbl_line_items;
			$query = <<<EOS
				SELECT *
				FROM $table1 lr 
				WHERE $key = ?
EOS;

			$stmt = $this->query($query,array($id));
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;			
		}
	}

	public function get_specific_release($key, $id){

		try
		{
			$table1 = BAS_Model::tbl_line_items;
			$table2 = BAS_Model::tbl_line_item_releases;

			$query = <<<EOS
				SELECT
				l.line_item_id, 
				l.line_item_name, 
				lr.line_item_id,
				lr.release_id,
				lr.reference_no,
				lr.reference_date,
				lr.total_release_amount
				FROM $table2 lr 
				JOIN $table1 l
				ON l.line_item_id = lr.line_item_id 
				WHERE $key = ?
EOS;

			$stmt = $this->query($query,array($id));
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;			
		}
	}

	public function get_specific_office($line_item_id){

		try
		{
			$table2 = BAS_Model::tbl_line_item_offices;
			$table1 = BAS_Model::tbl_param_offices;

			$query = <<<EOS
				SELECT
				t2.*,
				t1.*
				FROM $table2 t2
				JOIN $table1 t1 ON t1.office_num = t2.office_num
				WHERE t2.line_item_id = ?
EOS;
			$stmt = $this->query($query,array($line_item_id));
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;			
		}
	}

	public function get_specific_account_codes($params)
	{
		try
		{
			$table2 = BAS_Model::tbl_line_item_accounts;

			$query = <<<EOS
				SELECT * 
				FROM $table2
				WHERE line_item_id = ?
EOS;
			$stmt = $this->query($query,array($params['line_item_id']));
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;			
		}
	}

	public function get_release_details($id)
	{
		try
		{
			$fields = array('*');
			$where = array('release_id' => $id);
			return $this->select_all($fields, BAS_Model::tbl_line_item_release_details, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;			
		}
	}

	public function get_allotment_release_amount_list($fields, $selects, $params, $id)
	{
		try
		{
			$c_columns = array(
				"release_id", 
				"line_item_id", 
				"reference_date", 
				"reference_no", 
				"total_release_amount"
			);
			$tbl_fields = str_replace(" , ", " ", implode(", ", $fields));
			
			$where = $this->filtering($c_columns, $params, TRUE);
			$order = $this->ordering($selects, $params);
			$limit = $this->paging($params);
			
			$filter_str = $where["search_str"];
			$filter_params = $where["search_params"];
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $tbl_fields 
				FROM %s
				WHERE line_item_id = $id
				$filter_str
	        	$order
	        	$limit
EOS;
			$table = sprintf($query, BAS_Model::tbl_line_item_releases);
			$stmt = $this->query($table, $filter_params);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;			
		}
	}

	public function filtered_length($fields, $selects, $params, $id)
	{
		try
		{
			$this->get_allotment_release_amount_list($fields, $selects, $params, $id);
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
			
			$where = array('line_item_id'=> $id);
			$stmt = $this->query($query, $where, FALSE);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;			
		}
	}
	
	
	public function total_length($id)
	{
		try
		{
			$fields = array("COUNT(release_id) cnt");
			$where = array('line_item_id'=> $id);
			return $this->select_one($fields, BAS_Model::tbl_line_item_releases, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;			
		}
	}

	public function get_offices($id)
	{
		try
		{
			$fields = array('*');
			$where = array('line_item_id' => $id);
			return $this->select_all($fields, BAS_Model::tbl_line_item_offices, $where);	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function get_line_item_office()
	{
		try
		{
			$fields = array('*');
			return $this->select_all($fields, BAS_Model::tbl_param_offices);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function delete_account_code($id, $item)
	{
		try
		{
			$count = $this->select_all(['account_code'], BAS_Model::tbl_obligation_charges, ['account_code'=> $id]);
			if(sizeof($count) == 0)
			{
				$this->delete_data(BAS_Model::tbl_line_item_accounts, array('account_code' => $id, 'line_item_id'=> $item));	
				return 1;
			}else return 0;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function delete_office($id, $item)
	{
		try
		{
			$count = $this->select_all(['office_num'], BAS_Model::tbl_obligation_charges, ['office_num'=> $id]);
			/*if(sizeof($count) == 0)
			{*/
				//update line items mooe amount. subtract the amount -----------------------------------------------
				$new_data = $this->select_one(['*'], BAS_Model::tbl_line_item_offices, ['office_num'=>$id,'line_item_id'=>$item]);
				//subtract
				$line_item_mooe_data = $this->select_one(['total_appropriation','total_allotment','mooe_appropriation','mooe_allotment', 'parent_line_item_id'],BAS_Model::tbl_line_items,['line_item_id'=>$item]);
				$this->update_data(BAS_Model::tbl_line_items, 
					[
						'mooe_appropriation'	=> ($line_item_mooe_data['mooe_appropriation'] - $new_data['mooe_appropriation']),
						'mooe_allotment'		=> ($line_item_mooe_data['mooe_allotment'] - $new_data['mooe_allotment']),
						//total 
						'total_appropriation'	=> ($line_item_mooe_data['total_appropriation'] - $line_item_mooe_data['mooe_appropriation']),
						'total_allotment'		=> ($line_item_mooe_data['total_allotment'] - $line_item_mooe_data['mooe_appropriation']),
					], 
					['line_item_id'=>$item]
				);

				//update line item parent total
				$parent_data = $this->select_all(
					array('SUM(total_appropriation) total_appropriation, SUM(total_allotment) total_allotment'),
					BAS_Model::tbl_line_items,
					array('parent_line_item_id'=> $line_item_mooe_data['parent_line_item_id'])
				);

				$to_update_total = [
					'total_appropriation' 	=> $parent_data[0]['total_appropriation'],
					'total_allotment' 		=> $parent_data[0]['total_allotment']
				];

				$this->update_data(BAS_Model::tbl_line_items, $to_update_total, ['line_item_id'=> $line_item_mooe_data['parent_line_item_id']]);


				$this->delete_data(BAS_Model::tbl_line_item_offices, array('office_num' => $id, 'line_item_id' => $item));	
				return 1;
			/*}
			else return 0;*/
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function process_office($params)
	{
		try
		{
			$id = $params['line_item_id'];

			$office							= $params['office'];
			$allotment_amount 				= str_replace(',','', $params['allotment_amount']);
			$appropriation_amount 			= str_replace(',','', $params['appropriation_amount']);

			$total_allotment 				= 0;
			$total_appropriation 			= 0;

			foreach($office as $key => $val)
			{
				$data = array(
					'line_item_id' 			=> filter_var($id, FILTER_SANITIZE_NUMBER_INT),
					'office_num' 			=> filter_var($val, FILTER_SANITIZE_STRING),
					'mooe_allotment' 		=> filter_var($allotment_amount[$key], FILTER_SANITIZE_NUMBER_FLOAT),
					'mooe_appropriation' 	=> filter_var($appropriation_amount[$key], FILTER_SANITIZE_NUMBER_FLOAT),
					'created_by' 			=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
					'created_date' 			=> date("Y-m-d H:i:s"),
				);

				$o_num = $this->insert_data(BAS_Model::tbl_line_item_offices, $data, TRUE);

				$total_allotment 		+= $allotment_amount[$key];
				$total_appropriation 	+= $appropriation_amount[$key];

				$inserted = $this->select_one(
					['total_allotment', 'total_appropriation'], 
					BAS_Model::tbl_line_item_offices, 
					['office_num'=>$o_num, 'line_item_id'=>$id]
				);

				$this->update_data(
					BAS_Model::tbl_line_item_offices, 
					['total_allotment'=> ($inserted['total_allotment'] + $allotment_amount[$key]), 'total_appropriation'=> ($inserted['total_appropriation'] + $appropriation_amount[$key])],
					['office_num'=>$o_num,'line_item_id'=>$id]
				);

				//update line items mooe amount. add the new amount -----------------------------------------------

				//add
				$line_item_mooe_data = $this->select_one(['total_appropriation','total_allotment','mooe_appropriation','mooe_allotment', 'parent_line_item_id'],BAS_Model::tbl_line_items,['line_item_id'=>$id]);
				$this->update_data(BAS_Model::tbl_line_items, 
					[
						'mooe_appropriation'	=> ($line_item_mooe_data['mooe_appropriation'] + $appropriation_amount[$key]),
						'mooe_allotment'		=> ($line_item_mooe_data['mooe_allotment'] + $allotment_amount[$key]),
						//total 
						'total_appropriation'	=> ($line_item_mooe_data['total_appropriation'] + $line_item_mooe_data['mooe_appropriation']),
						'total_allotment'		=> ($line_item_mooe_data['total_allotment'] + $line_item_mooe_data['mooe_appropriation']),
					], 
					['line_item_id'=>$id]
				);
			}

			//update line item parent total
			$parent_data = $this->select_all(
				array('SUM(total_appropriation) total_appropriation, SUM(total_allotment) total_allotment'),
				BAS_Model::tbl_line_items,
				array('parent_line_item_id'=> $line_item_mooe_data['parent_line_item_id'])
			);

			$to_update_total = [
				'total_appropriation' 	=> $parent_data[0]['total_appropriation'],
				'total_allotment' 		=> $parent_data[0]['total_allotment']
			];

			$this->update_data(BAS_Model::tbl_line_items, $to_update_total, ['line_item_id'=> $line_item_mooe_data['parent_line_item_id']]);

			return $o_num;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e, TRUE);
			throw $e;			
		}
	}

	public function process_update_office($params)
	{
		try
		{
			$id = $params['line_item_id'];

			$old_office 					= $params['old_office'];
			$update_office 					= $params['update_office'];
			$update_allotment_amount 		= str_replace(',','', $params['update_allotment_amount']);
			$update_appropriation_amount 	= str_replace(',','', $params['update_appropriation_amount']);

			$total_allotment 				= 0;
			$total_appropriation 			= 0;

			foreach($update_office as $key 	=> $val)
			{
				//get the old then subtract individual amount ----------------------
				$old_amount = $this->select_one(['total_allotment', 'total_appropriation', 'mooe_allotment', 'mooe_appropriation'], BAS_Model::tbl_line_item_offices,['office_num'=> $old_office[$key], 'line_item_id'=> $id]);

				$this->update_data(
					BAS_Model::tbl_line_item_offices, 
					[
						'total_allotment'		=> ($old_amount['total_allotment'] - $old_amount['mooe_allotment']), 
						'total_appropriation'	=> ($old_amount['total_appropriation'] - $old_amount['mooe_appropriation'])
					],
					[
						'office_num'			=> $old_office[$key],'line_item_id'=>$id
					]
				);

				//update -----------------------------------------------------------
				$this->update_data(
					BAS_Model::tbl_line_item_offices, 
					[
						'line_item_id' 			=> filter_var($id, FILTER_SANITIZE_NUMBER_INT),
						'office_num' 			=> filter_var($val, FILTER_SANITIZE_STRING),
						'mooe_allotment' 		=> filter_var($update_allotment_amount[$key], FILTER_SANITIZE_NUMBER_INT),
						'mooe_appropriation' 	=> filter_var($update_appropriation_amount[$key], FILTER_SANITIZE_NUMBER_INT),
						'modified_date'			=> date("Y-m-d H:i:s"),
						'modified_by'			=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
					],
					[
						'line_item_id' => $id,'office_num' => $old_office[$key]
					]
				);

				$total_allotment 		+= $update_allotment_amount[$key];
				$total_appropriation 	+= $update_appropriation_amount[$key];

				//get the new updated amount then add per office -------------------
				$updated_amount = $this->select_one(
					['total_allotment','total_appropriation'], 
					BAS_Model::tbl_line_item_offices,
					[
						'office_num'	=>$val,
						'line_item_id'	=>$id
					]
				);

				$this->update_data(
					BAS_Model::tbl_line_item_offices, 
					[
						'total_allotment'		=> ($updated_amount['total_allotment'] + $update_allotment_amount[$key]), 
						'total_appropriation'	=> ($updated_amount['total_appropriation'] + $update_appropriation_amount[$key])],
					[
						'office_num'			=> $val,
						'line_item_id'			=> $id
					]
				);

				//update line items mooe amount. subtract the old and add the new amount -----------------------------------------------

				//subtract
				$line_item_mooe_data = $this->select_one(['total_appropriation','total_allotment','mooe_appropriation','mooe_allotment', 'parent_line_item_id'],BAS_Model::tbl_line_items,['line_item_id'=>$id]);
				$this->update_data(BAS_Model::tbl_line_items, 
					[
						'mooe_appropriation'	=> ($line_item_mooe_data['mooe_appropriation'] - $update_appropriation_amount[$key]),
						'mooe_allotment'		=> ($line_item_mooe_data['mooe_allotment'] - $update_allotment_amount[$key]),
						//total 
						'total_appropriation'	=> ($line_item_mooe_data['total_appropriation'] - $line_item_mooe_data['mooe_appropriation']),
						'total_allotment'		=> ($line_item_mooe_data['total_allotment'] - $line_item_mooe_data['mooe_appropriation']),
					], 
					[
						'line_item_id'			=> $id
					]
				);

				//add
				$line_item_mooe_data = $this->select_one(['total_appropriation','total_allotment','mooe_appropriation','mooe_allotment', 'parent_line_item_id'],BAS_Model::tbl_line_items,['line_item_id'=>$id]);
				$this->update_data(BAS_Model::tbl_line_items, 
					[
						'mooe_appropriation'	=> ($line_item_mooe_data['mooe_appropriation'] + $update_appropriation_amount[$key]),
						'mooe_allotment'		=> ($line_item_mooe_data['mooe_allotment'] + $update_allotment_amount[$key]),
						//total 
						'total_appropriation'	=> ($line_item_mooe_data['total_appropriation'] + $line_item_mooe_data['mooe_appropriation']),
						'total_allotment'		=> ($line_item_mooe_data['total_allotment'] + $line_item_mooe_data['mooe_appropriation']),
					], 
					[
						'line_item_id'			=>$id
					]
				);
			}

			//update line item parent total
			$parent_data = $this->select_all(
				array('SUM(total_appropriation) total_appropriation, SUM(total_allotment) total_allotment'),
				BAS_Model::tbl_line_items,
				array('parent_line_item_id'=> $line_item_mooe_data['parent_line_item_id'])
			);

			$to_update_total = [
				'total_appropriation' 	=> $parent_data[0]['total_appropriation'],
				'total_allotment' 		=> $parent_data[0]['total_allotment']
			];

			$this->update_data(BAS_Model::tbl_line_items, $to_update_total, ['line_item_id'=> $line_item_mooe_data['parent_line_item_id']]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function get_offices_on_ob()
	{
		try
		{
			$db = DB_BAS.'.'.BAS_Model::tbl_obligation_charges;

			$get = <<<EOS
				SELECT * FROM $db GROUP BY office_num 
EOS;
			$stmt = $this->db->prepare($get);
			$stmt->execute();
			return $stmt->fetchAll(PDO::FETCH_ASSOC);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function get_line_items_account_codes($code, $line_item_id)
	{
		try
		{

			$line_item_accounts = BAS_Model::tbl_line_item_accounts;

			$query = <<<EOS
				SELECT * FROM $line_item_accounts  
				WHERE LEFT(account_code, 3) = '$code' AND line_item_id = '$line_item_id'

EOS;

			return $this->query($query);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function get_account_codes($code)
	{
		try
		{
			$table = BAS_Model::tbl_param_account_codes;

			$get = <<<EOS
				SELECT
				ac.account_name,
				ac.account_code,
				ac.parent_account_code 
				FROM $table c
				JOIN $table a ON a.parent_account_code = c.account_code
				JOIN $table ac ON ac.parent_account_code = c.account_code 
				WHERE LEFT(ac.account_code, 3) = '$code' AND ac.account_code != c.parent_account_code 
				GROUP BY ac.account_code
EOS;
			return $this->query($get);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function get_all_account_codes_2()
	{
		try
		{
			$table = BAS_Model::tbl_param_account_codes;

			$get = <<<EOS
				SELECT
				ac.account_name,
				ac.account_code,
				ac.parent_account_code 
				FROM $table c
				JOIN $table a ON a.parent_account_code = c.account_code
				JOIN $table ac ON ac.parent_account_code = c.account_code 
				WHERE ac.account_code != c.parent_account_code 
				GROUP BY ac.account_code
EOS;
			return $this->query($get);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function get_all_account_codes()
	{
		try
		{
			$fields = array('*');
			return $this->select_all($fields, BAS_Model::tbl_param_account_codes);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function get_all_account_codes_on_line_item_accounts()
	{
		$table1 = BAS_Model::tbl_line_item_accounts;
		$table2 = BAS_Model::tbl_param_account_codes;

		$query = <<<EOS
			SELECT
			t1.account_code,
			ac.parent_account_code,
			ac.account_code,
			ac.account_name
			FROM $table1 t1
			JOIN $table2 t2 ON t2.account_code = t1.account_code
			JOIN $table2 ac ON ac.parent_account_code = t1.account_code
EOS;

		return $this->query($query);
	}

	public function get_param_source_types_list()
	{
		try
		{
			$fields = array(
				"source_type_num", 
				"source_type_name", 
				"active_flag"
			);
			return $this->select_all($fields, BAS_Model::tbl_param_source_types);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function check_if_exists_fund_sources($year, $fund_source){

		try
		{
			$fields 	= ['*'];

			return $this->select_all(
				$fields, BAS_Model::tbl_line_items, 
				[
					'year' 				=> $year,
					'source_type_num' 	=> $fund_source
				]
			);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}	

	public function check_if_exists_fund_sources_data($year, $fund_source){

		try
		{
			$where					= array(
				'year' 				=> $year,
				'source_type_num' 	=> $fund_source
			);

			$fields 	= array("*");
			return $this->select_all($fields, BAS_Model::tbl_fund_sources, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}	

	public function select_parent_line_items($fields, $where)
	{
		try
		{
			return $this->select_one($fields, BAS_Model::tbl_line_items, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}

	public function process_release($params)
	{
		try
		{
			$line_item_id 		= $params['line_item_id'];
			$release_date 		= $params['release_date'];
			$release_number 	= $params['release_number'];

			$date = new DateTime($release_date);
			$newdate = $date->format('Y-m-d'); // 2012-07-31

			$data = array(
				'line_item_id'	 	=> filter_var($line_item_id, FILTER_SANITIZE_NUMBER_INT),
				'reference_no'	 	=> filter_var($release_number, FILTER_SANITIZE_NUMBER_INT),
				'reference_date' 	=> $newdate,
				'created_by'		=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
				'created_date'		=> date("Y-m-d H:i:s"),
			);
			//insert
			$id = $this->insert_data(BAS_Model::tbl_line_item_releases, $data, TRUE);

			$code 		= $params['account_code'];
			$allotment 	= str_replace(',','', $params['allotment_amount']);

			//insert
			foreach($code as $key => $val)
			{
				$data = array(
					'release_id' 			=> $id,
					'account_code' 			=> filter_var($val, FILTER_SANITIZE_STRING),
					'allotment_amount' 		=> filter_var($allotment[$key], FILTER_SANITIZE_NUMBER_FLOAT),
				);

				$this->insert_data(BAS_Model::tbl_line_item_release_details, $data);

				//OLD CODE NO CLARIFICATION BACKUP
				//reverse add the new ones----------------------------------------------------------------------------------
				/*$cur_line_tem_data = $this->select_one(
					['allotment_amount','line_item_id', 'account_code'], 
					BAS_Model::tbl_line_item_accounts, 
					['line_item_id'=> $line_item_id, 'account_code'=> $val]
				);

				//add to current line item amount
				$this->update_data(
					BAS_Model::tbl_line_item_accounts, 
					['allotment_amount'	=>	floatval($cur_line_tem_data['allotment_amount']) + $allotment[$key]], 
					['line_item_id'		=> 	$cur_line_tem_data['line_item_id'], 'account_code' => $cur_line_tem_data['account_code'] ]
				);*/
				//----------------------------------------------------------------------------------------------------------

				//check code matches source
				$pos1  	= strpos($val, $this->find_code_ps);
				$pos2 	= strpos($val, $this->find_code_mooe);
				$pos3 	= strpos($val, $this->find_code_co);

				if ($pos1 !== false) $identity = 'ps';
				if ($pos2 !== false) $identity = 'mooe';
				if ($pos3 !== false) $identity = 'co';

				//update line item allotment based on account code
				$cur_line_tem_data = $this->select_one(
					[
						''.$identity.'_allotment',
						'line_item_id', 
						'parent_line_item_id'
					], 
					BAS_Model::tbl_line_items, 
					['line_item_id' => $line_item_id]
				);

				$this->update_data(
					BAS_Model::tbl_line_items,
					[
						''.$identity.'_allotment' => ($cur_line_tem_data[''.$identity.'_allotment'] + $allotment[$key]),

					],
					['line_item_id' => $line_item_id]
				);
				//-------------------------------------------------------------

				//update selected line items total for new additional allotment
				$cur_line_tem_data2 = $this->select_one(
					[
						'SUM(ps_allotment + mooe_allotment + co_allotment) total_allotment_new',
						'SUM(ps_allotment + mooe_appropriation + co_appropriation) total_appropriation_new',
						'line_item_id', 
						'parent_line_item_id'
					], 
					BAS_Model::tbl_line_items, 
					['line_item_id' => $line_item_id]
				);

				$this->update_data(
					BAS_Model::tbl_line_items,
					[
						'total_allotment' 		=> $cur_line_tem_data2['total_allotment_new'],
						'total_appropriation' 	=> $cur_line_tem_data2['total_appropriation_new'],

					],
					['line_item_id' => $line_item_id]
				);
				//-------------------------------------------------------------

				//update parent to get all total-------------------------------
				$parent = $cur_line_tem_data['parent_line_item_id'];

				if(!is_null($parent))
				{
					$parent_data = $this->select_all(
						array('SUM(total_appropriation) total_appropriation, SUM(total_allotment) total_allotment'),
						BAS_Model::tbl_line_items,
						array('parent_line_item_id'=> $parent)
					);

					$to_update_total = [
						'total_appropriation' 	=> $parent_data[0]['total_appropriation'],
						'total_allotment' 		=> $parent_data[0]['total_allotment']
					];

					$this->update_data(BAS_Model::tbl_line_items, $to_update_total, ['line_item_id'=> $parent]);
				}
				//-------------------------------------------------------------
			}

			//get
			$fields = ['SUM(allotment_amount) total'];
			$where 	= ['release_id' => $id];
			$total 	= $this->select_all($fields, BAS_Model::tbl_line_item_release_details, $where);

			//get update and
			foreach($total as $key => $val):

				$where = ['release_id' => $id];
				$update_data = ['total_release_amount' => $val['total']];

				$this->update_data(BAS_Model::tbl_line_item_releases, $update_data, $where);

			endforeach;

			return $line_item_id;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}

	public function update_release($params)
	{
		$id = $params['id'];

		$line_item_id 		= $params['line_item_id'];
		$release_date 		= $params['release_date'];
		$release_number 	= $params['release_number'];

		$update_code 				= $params['update_code'];
		$update_account_code 		= str_replace(',','', $params['update_account_code']);
		$update_allotment_amount 	= str_replace(',','', $params['update_allotment_amount']);

		$date = new DateTime($release_date);
		$newdate = $date->format('Y-m-d'); // 2012-07-31

		$data = array(
			'reference_no'	 	=> filter_var($release_number, FILTER_SANITIZE_NUMBER_INT),
			'reference_date' 	=> $newdate,
		);
		//update releases
		$this->update_data(BAS_Model::tbl_line_item_releases, $data, array('release_id'=> $id));

		//update release details
		foreach($update_account_code as $key => $val)
		{
			$data = array(
				'account_code' 			=> filter_var($val, FILTER_SANITIZE_STRING),
				'allotment_amount' 		=> filter_var($update_allotment_amount[$key], FILTER_SANITIZE_NUMBER_FLOAT),
			);

			//subtract the old ones-------------------------------------------------------------------------------------
			$cur_line_tem_data = $this->select_one(
				['allotment_amount','line_item_id', 'account_code'], 
				BAS_Model::tbl_line_item_accounts, 
				['line_item_id'=> $line_item_id, 'account_code'=> $val]
			);

			//get the old details amount
			$cur_details_data = $this->select_one(
				['allotment_amount', 'account_code'], 
				BAS_Model::tbl_line_item_release_details, 
				['release_id'=> $id, 'account_code'=> $val]
			);

			//subtract to current line item amount
			$this->update_data(
				BAS_Model::tbl_line_item_accounts, 
				['allotment_amount'	=>	floatval($cur_line_tem_data['allotment_amount']) - floatval($cur_details_data['allotment_amount'])], 
				['line_item_id'		=> 	$cur_line_tem_data['line_item_id'], 'account_code' => $cur_line_tem_data['account_code'] ]
			);
			//----------------------------------------------------------------------------------------------------------

			//reverse add the new ones----------------------------------------------------------------------------------
			$cur_line_tem_data = $this->select_one(
				['allotment_amount','line_item_id', 'account_code'], 
				BAS_Model::tbl_line_item_accounts, 
				['line_item_id'=> $line_item_id, 'account_code'=> $val]
			);

			//subtract to current line item amount
			$this->update_data(
				BAS_Model::tbl_line_item_accounts, 
				['allotment_amount'	=>	floatval($cur_line_tem_data['allotment_amount']) + $update_allotment_amount[$key]], 
				['line_item_id'		=> 	$cur_line_tem_data['line_item_id'], 'account_code' => $cur_line_tem_data['account_code'] ]
			);
			//----------------------------------------------------------------------------------------------------------

			$where = array('account_code' => $update_code[$key]);

			$this->update_data(BAS_Model::tbl_line_item_release_details, $data, $where);
		}

		if(ISSET($params['account_code'])): //for added release details

			$code 		 		= $params['account_code'];
			$allotment 			= $params['allotment_amount'];
			foreach($code as $key => $val)
			{
				$data = array(
					'release_id' 			=> filter_var($id, FILTER_SANITIZE_NUMBER_INT),
					'account_code' 			=> filter_var($val, FILTER_SANITIZE_STRING),
					'allotment_amount' 		=> filter_var($allotment[$key], FILTER_SANITIZE_NUMBER_FLOAT),
				);

				//reverse add the new ones----------------------------------------------------------------------------------
				$cur_line_tem_data = $this->select_one(
					['allotment_amount','line_item_id', 'account_code'], 
					BAS_Model::tbl_line_item_accounts, 
					['line_item_id'=> $line_item_id, 'account_code'=> $val]
				);

				//subtract to current line item amount
				$this->update_data(
					BAS_Model::tbl_line_item_accounts, 
					['allotment_amount'	=>	floatval($cur_line_tem_data['allotment_amount']) + $allotment[$key]], 
					['line_item_id'		=> 	$cur_line_tem_data['line_item_id'], 'account_code' => $cur_line_tem_data['account_code'] ]
				);
				//----------------------------------------------------------------------------------------------------------

				$this->insert_data(BAS_Model::tbl_line_item_release_details, $data);
			}

		endif;
		//get

		$fields = array('SUM(allotment_amount) total');
		$where = array('release_id' => $id,);
		$total = $this->select_all($fields, BAS_Model::tbl_line_item_release_details, $where);

		//get update and
		foreach($total as $key => $val):

			$where = array('release_id' => $id);
			$update_data = array('total_release_amount' => $val['total']);
			$this->update_data(BAS_Model::tbl_line_item_releases, $update_data, $where);

		endforeach;
		
		return $line_item_id;		
	}

	public function process_account_codes($params)
	{
		try
		{
			$id = $params['line_item_id'];

			$code 					= $params['account_code'];
			$appropriation 			= str_replace(',','', $params['appropriation_amount']);
			$allotment 				= str_replace(',','', $params['allotment_amount']);

			foreach($code as $key => $val)
			{
				$data = array(
					'line_item_id' 			=> filter_var($id, FILTER_SANITIZE_NUMBER_INT),
					'account_code' 			=> filter_var($val, FILTER_SANITIZE_STRING),
					'appropriation_amount' 	=> filter_var($appropriation[$key], FILTER_SANITIZE_NUMBER_FLOAT),
					'allotment_amount' 		=> filter_var($allotment[$key], FILTER_SANITIZE_NUMBER_FLOAT),
					'created_by' 			=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
					'created_date' 			=> date("Y-m-d H:i:s"),
				);

				$this->insert_data(BAS_Model::tbl_line_item_accounts, $data);
			}
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}

	public function process_update_account_code($params)
	{
		try
		{	
			$id = $params['line_item_id'];

			$old_code 				= $params['old_account_code'];
			$update_code 			= $params['update_account_code'];
			$update_appropriation 	= str_replace(',','', $params['update_appropriation_amount']);
			$update_allotment 		= str_replace(',','', $params['update_allotment_amount']);

			foreach($update_code as $key => $val)
			{
				$data = array(
					'line_item_id' 			=> filter_var($id, FILTER_SANITIZE_NUMBER_INT),
					'account_code' 			=> filter_var($val, FILTER_SANITIZE_STRING),
					'appropriation_amount' 	=> filter_var($update_appropriation[$key], FILTER_SANITIZE_NUMBER_FLOAT),
					'allotment_amount' 		=> filter_var($update_allotment[$key], FILTER_SANITIZE_NUMBER_FLOAT),
					'modified_by' 			=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
					'modified_date' 		=> date("Y-m-d H:i:s"),
				);

				//GET THE OLD VALUE AND SUBTRACT
				//check code matches source
				$pos1  	= strpos($val, $this->find_code_ps);
				$pos2 	= strpos($val, $this->find_code_mooe);
				$pos3 	= strpos($val, $this->find_code_co);

				if ($pos1 !== false) $identity = 'ps';
				if ($pos2 !== false) $identity = 'mooe';
				if ($pos3 !== false) $identity = 'co';

				$old_get = $this->select_one(
					array('line_item_id','account_code','appropriation_amount','allotment_amount'),
					BAS_Model::tbl_line_item_accounts,
					array('line_item_id'=>$id,'account_code'=>$val)
				);

				$total_current_alignment 	= $this->get_line_item_realignment_amount($id,$identity);

				$new_amount_data = array(
					''.$identity.'_appropriation_realigned' 	=> ($total_current_alignment[''.$identity.'_appropriation_realigned'] - $old_get['appropriation_amount']),
					''.$identity.'_allotment_realigned' 		=> ($total_current_alignment[''.$identity.'_allotment_realigned'] - $old_get['allotment_amount']),
				);

				$this->update_data(
					BAS_Model::tbl_line_items, 
					$new_amount_data, 
					array('line_item_id'=> $id)
				);

				//new amount
				$total_new_alignment 	= $this->get_line_item_realignment_amount($id,$identity);

				//then add the new data amount
				$new_amount_data2 = array(
					''.$identity.'_appropriation_realigned' 	=> ($total_new_alignment[''.$identity.'_appropriation_realigned'] + $update_appropriation[$key]),
					''.$identity.'_allotment_realigned' 		=> ($total_new_alignment[''.$identity.'_allotment_realigned'] + $update_allotment[$key]),
				);

				$this->update_data(
					BAS_Model::tbl_line_items, 
					$new_amount_data2, 
					array('line_item_id'=> $id)
				);

				//update the data
				$where = array('line_item_id'=> $id,'account_code'=> $old_code[$key]);
				$this->update_data(BAS_Model::tbl_line_item_accounts, $data, $where);
			}
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}

	public function get_line_item_realignment_amount($line_item_id, $pos)
	{
		$selects = array('line_item_id',''.$pos.'_appropriation_realigned',''.$pos.'_allotment_realigned');
		return $this->select_one($selects, BAS_Model::tbl_line_items, array('line_item_id'=>$line_item_id));
	}

	public function insert_fund_source($params)
	{
		try
		{
			$fund_data = array(
				'year' 					=> filter_var($params['line_item_year'], FILTER_SANITIZE_NUMBER_INT),
				'source_type_num'		=> filter_var($params['source_type'], FILTER_SANITIZE_NUMBER_INT),
				'created_by' 			=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
				'created_date' 			=> date("Y-m-d H:i:s"),
			);
			return $this->insert_data(BAS_Model::tbl_fund_sources, $fund_data);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}

	public function set_as_final_line_items($year, $source)
	{
		try
		{
			return $this->update_data(BAS_Model::tbl_fund_sources, ['final_flag'=>1], ['year'=>$year,'source_type_num'=>$source]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}

	public function insert_line_items($params, $row)
	{
		try
		{
			$year 						= $params['line_item_year'];
			$source_type 				= $params['source_type'];

			$code 						= $params['code_'.$row.''];
			$line_item 					= $params['line_item_'.$row.''];

			$parent 					= $params['parent_line_item_'.$row.''];

			$ps_allotment_amount 		= (empty($params['ps_allotment_amount_'.$row.''])) ? '0' : str_replace(',','', $params['ps_allotment_amount_'.$row.'']);
			$mooe_allotment_amount 		= (empty($params['mooe_allotment_amount_'.$row.''])) ? '0' : str_replace(',','', $params['mooe_allotment_amount_'.$row.'']);
			$coo_allotment_amount 		= (empty($params['coo_allotment_amount_'.$row.''])) ? '0' : str_replace(',','', $params['coo_allotment_amount_'.$row.'']);
			$ps_appropriation_amount 	= (empty($params['ps_appropriation_amount_'.$row.''])) ? '0' : str_replace(',','', $params['ps_appropriation_amount_'.$row.'']);
			$mooe_appropriation_amount 	= (empty($params['mooe_appropriation_amount_'.$row.''])) ? '0' : str_replace(',','', $params['mooe_appropriation_amount_'.$row.'']);
			$coo_appropriation_amount 	= (empty($params['coo_appropriation_amount_'.$row.''])) ? '0' : str_replace(',','', $params['coo_appropriation_amount_'.$row.'']);

			$total_allotment 		= ($ps_allotment_amount + $mooe_allotment_amount + $coo_allotment_amount);
			$total_appropriation 	= ($ps_appropriation_amount + $mooe_appropriation_amount + $coo_appropriation_amount);

			$data = array(
				'year' => $year,
				'source_type_num' 		=> filter_var($source_type,FILTER_SANITIZE_NUMBER_INT),
				'line_item_code' 		=> filter_var($code,FILTER_SANITIZE_STRING),
				'line_item_name' 		=> filter_var($line_item,FILTER_SANITIZE_STRING),
				'ps_allotment' 			=> filter_var($ps_allotment_amount,FILTER_SANITIZE_NUMBER_INT),
				'mooe_allotment' 		=> filter_var($mooe_allotment_amount,FILTER_SANITIZE_NUMBER_INT),
				'co_allotment' 			=> filter_var($coo_allotment_amount,FILTER_SANITIZE_NUMBER_INT),
				'ps_appropriation' 		=> filter_var($ps_appropriation_amount,FILTER_SANITIZE_NUMBER_INT),
				'mooe_appropriation' 	=> filter_var($mooe_appropriation_amount,FILTER_SANITIZE_NUMBER_INT),
				'co_appropriation' 		=> filter_var($coo_appropriation_amount,FILTER_SANITIZE_NUMBER_INT),

				'total_allotment'		=> $total_allotment,
				'total_appropriation'	=> $total_appropriation,

				'created_date'			=> date("Y-m-d H:i:s"),

				'created_by' 			=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1'
			);

			if(!empty($parent))
			{
				$data['parent_line_item_id'] = $parent;
			}
			
			$id = $this->insert_data(BAS_Model::tbl_line_items, $data, TRUE);

			if(!empty($parent))
			{
				$parent_data = $this->select_all(
					array('SUM(total_appropriation) total_appropriation, SUM(total_allotment) total_allotment'),
					BAS_Model::tbl_line_items,
					array('parent_line_item_id'=> $parent)
				);

				$to_update_total = [
					'total_appropriation' 	=> $parent_data[0]['total_appropriation'],
					'total_allotment' 		=> $parent_data[0]['total_allotment']
				];

				$this->update_data(BAS_Model::tbl_line_items, $to_update_total, ['line_item_id'=> $parent]);
			}

			return $id;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}

	public function update_line_items_data($params, $row, $id)
	{
		try
		{
			$line_item_id 				= $id;
			$code 						= $params['code_'.$row.''];
			$line_item 					= $params['line_item_'.$row.''];

			$parent 					= $params['parent_line_item_'.$row.''];

			$ps_allotment_amount 		= (empty($params['ps_allotment_amount_'.$row.''])) ? '0' : str_replace(',','', $params['ps_allotment_amount_'.$row.'']);
			$mooe_allotment_amount 		= (empty($params['mooe_allotment_amount_'.$row.''])) ? '0' : str_replace(',','', $params['mooe_allotment_amount_'.$row.'']);
			$coo_allotment_amount 		= (empty($params['coo_allotment_amount_'.$row.''])) ? '0' : str_replace(',','', $params['coo_allotment_amount_'.$row.'']);
			$ps_appropriation_amount 	= (empty($params['ps_appropriation_amount_'.$row.''])) ? '0' : str_replace(',','', $params['ps_appropriation_amount_'.$row.'']);
			$mooe_appropriation_amount 	= (empty($params['mooe_appropriation_amount_'.$row.''])) ? '0' : str_replace(',','', $params['mooe_appropriation_amount_'.$row.'']);
			$coo_appropriation_amount 	= (empty($params['coo_appropriation_amount_'.$row.''])) ? '0' : str_replace(',','', $params['coo_appropriation_amount_'.$row.'']);

			$total_allotment 		= ($ps_allotment_amount + $mooe_allotment_amount + $coo_allotment_amount);
			$total_appropriation 	= ($ps_appropriation_amount + $mooe_appropriation_amount + $coo_appropriation_amount);

			$data = array(
				'line_item_code' 		=> filter_var($code,FILTER_SANITIZE_STRING),
				'line_item_name' 		=> filter_var($line_item,FILTER_SANITIZE_STRING),
				'ps_allotment' 			=> filter_var($ps_allotment_amount,FILTER_SANITIZE_NUMBER_INT),
				'mooe_allotment' 		=> filter_var($mooe_allotment_amount,FILTER_SANITIZE_NUMBER_INT),
				'co_allotment' 			=> filter_var($coo_allotment_amount,FILTER_SANITIZE_NUMBER_INT),
				'ps_appropriation' 		=> filter_var($ps_appropriation_amount,FILTER_SANITIZE_NUMBER_INT),
				'mooe_appropriation' 	=> filter_var($mooe_appropriation_amount,FILTER_SANITIZE_NUMBER_INT),
				'co_appropriation' 		=> filter_var($coo_appropriation_amount,FILTER_SANITIZE_NUMBER_INT),

				'total_allotment'		=> $total_allotment,
				'total_appropriation'	=> $total_appropriation,

				'modified_date'			=> date("Y-m-d H:i:s"),
				'modified_by'			=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1'// '1' - test
			);

			$where = array(
				 'line_item_id' => $line_item_id
			);
			$this->update_data(BAS_Model::tbl_line_items, $data, $where);

			if(!empty($parent))
			{
				$parent_data = $this->select_all(
					array('SUM(total_appropriation) total_appropriation, SUM(total_allotment) total_allotment'),
					BAS_Model::tbl_line_items,
					array('parent_line_item_id'=> $parent)
				);

				$to_update_total = [
					'total_appropriation' 	=> $parent_data[0]['total_appropriation'],
					'total_allotment' 		=> $parent_data[0]['total_allotment']
				];

				$this->update_data(BAS_Model::tbl_line_items, $to_update_total, ['line_item_id'=> $parent]);
			}

			return true;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}

	public function return_line_item_id($id)
	{
		return $id;
	}

	public function check_if_final($year, $source)
	{
		try
		{
			$where = array(
				'year' 				=> $year,
				'source_type_num' 	=> $source
			);

			$count = $this->select_one(['*'], BAS_Model::tbl_fund_sources, $where);
			echo $count['final_flag'];
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}

	public function check_line_items($year, $source)
	{
		try
		{
			$where = array(
				'year' 				=> $year,
				'source_type_num' 	=> $source
			);

			$count = $this->select_all(['COUNT(line_item_id) as count'], BAS_Model::tbl_line_items, $where);
			echo $count[0]['count'];
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}

	public function get_line_items($year, $source)
	{
		try
		{
			$fields = array('*');
			$where = array(
				'year' 				=> $year,
				'source_type_num' 	=> $source
			);
			return $this->select_all($fields, BAS_Model::tbl_line_items, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}

	public function get_line_item_name($params)
	{
		try
		{
			$table1 = BAS_Model::tbl_line_items;
			$table2 = BAS_Model::tbl_param_source_types;

			$query = <<<EOS
				SELECT 
				l.line_item_id,
				l.line_item_code,
				l.line_item_name,
				l.year,
				l.source_type_num,
				
				p.source_type_num,
				p.source_type_name

				FROM $table1 l
				JOIN $table2 p
				ON p.source_type_num = l.source_type_num
				WHERE l.line_item_id = ?
EOS;
			return $this->query($query, array($params['id']));
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}
	
	public function update_line_items($params)
	{
		try
		{
			$val = array(
				'account_code' 			=> filter_var($params['code'], FILTER_SANITIZE_STRING),
				'account_name' 			=> filter_var($params['title'], FILTER_SANITIZE_STRING),
				'parent_account_code' 	=> filter_var($params['parent_code'], FILTER_SANITIZE_STRING),
				'active_flag' 			=> filter_var($params['status'], FILTER_SANITIZE_NUMBER_INT),
			);
			$where = array(
				'account_code' => $params['account_code']
			);
			$this->update_data(BAS_Model::tbl_param_account_codes, $val, $where);
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}
	
	public function delete_line_items($id)
	{
		try
		{
			$this->delete_data(BAS_Model::tbl_param_account_codes, array('account_code' => $id));				
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function delete_release_details($id, $release_id)
	{
		try
		{
			$fields = array('*');
			$where = array('account_code'=> $id);
			$old = $this->select_one($fields, BAS_Model::tbl_line_item_release_details, $where);
			//-----------------------
			
			$amount = floatval($old['allotment_amount']);		

			$fields = array('*');
			$where = array('release_id'=> $release_id);
			$old_total = $this->select_one($fields, BAS_Model::tbl_line_item_releases, $where);
			//----------------------

			$old_total_val = floatval($old_total);

			$new_total = ($old_total_val -  $amount);

			$where = array('release_id' => $release_id);
			$update_data = array('total_release_amount' => $new_total);
			$this->update_data(BAS_Model::tbl_line_item_releases, $update_data, $where);
			//----------------------	

			//subtract the old ones-------------------------------------------------------------------------------------
			$cur_line_tem_data = $this->select_one(
				['allotment_amount','line_item_id', 'account_code'], 
				BAS_Model::tbl_line_item_accounts, 
				['line_item_id'=> $old_total['line_item_id'], 'account_code'=> $id]
			);

			//get the old details amount
			$cur_details_data = $this->select_one(
				['allotment_amount', 'account_code'], 
				BAS_Model::tbl_line_item_release_details, 
				['release_id'=> $release_id, 'account_code'=> $id]
			);

			//subtract to current line item amount
			$this->update_data(
				BAS_Model::tbl_line_item_accounts, 
				['allotment_amount'	=>	floatval($cur_line_tem_data['allotment_amount']) - floatval($cur_details_data['allotment_amount'])], 
				['line_item_id'		=> 	$cur_line_tem_data['line_item_id'], 'account_code' => $cur_line_tem_data['account_code'] ]
			);
			//----------------------------------------------------------------------------------------------------------

			$this->delete_data(BAS_Model::tbl_line_item_release_details, array('account_code' => $id));	

			return $old_total['line_item_id'];
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function delete_releases($id)
	{
		try
		{
			$this->delete_data(BAS_Model::tbl_line_item_release_details, array('release_id' => $id));	
			$this->delete_data(BAS_Model::tbl_line_item_releases, array('release_id' => $id));				
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function delete_sub_line_items($id)
	{
		try
		{
			//before delete check if this item is use as a parent or has a child
			$fields = array("COUNT(*) cnt");
			$where	= array("parent_line_item_id" => $id);
			$count = $this->select_one($fields, BAS_Model::tbl_line_items, $where);
			//get the count
			$count = $count['cnt'];
			//condition the count
			if($count != 0)
			{
				return 1;
			}
			else 
			{
				$this->delete_data(BAS_Model::tbl_line_items, array('line_item_id' => $id));	
				return 0;
			}
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_allotment_release_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("release_id" => $id);
			return $this->select_one($fields, BAS_Model::tbl_line_item_releases, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function check_total_amount($params, $identity)
	{
		try
		{
			$line_items 	= BAS_Model::tbl_line_items;

			$appropriation  = ''.$params['uri'].'_appropriation';
			$allotment 		= ''.$params['uri'].'_allotment';

			$line_item_id 	= $params['line_item_id'];

			if($identity == 'account_code')
			{
				$query = <<<EOS
					SELECT
					$appropriation,
					$allotment
					FROM $line_items 
					WHERE line_item_id = '$line_item_id'
EOS;
			}
			else
			{
				$query = <<<EOS
					SELECT
					total_appropriation,
					total_allotment
					FROM $line_items 
					WHERE line_item_id = '$line_item_id'
EOS;
			}
			
			return $this->query($query);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
}