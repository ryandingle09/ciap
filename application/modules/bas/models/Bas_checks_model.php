<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_checks_model extends BAS_Model {

	//fixed code define
	var $find_code_ps   	= PARAM_ACCOUNT_CODE_PS;//501
	var $find_code_mooe   	= PARAM_ACCOUNT_CODE_MOOE;//502
	var $find_code_co   	= PARAM_ACCOUNT_CODE_CO;//503

	//fix status
	var $voided   			= STATUS_VOIDED;
	var $excess   			= STATUS_EXCESS;
	var $overriden   		= STATUS_OVERRIDE;
	var $posted   			= STATUS_POSTED;
	var $submitted   		= STATUS_SUBMITTED;
	var $approved   		= STATUS_APPROVED;
	var $disapproved   		= STATUS_DISAPPROVED;
	var $declined   		= STATUS_DECLINED;
	var $acive   			= STATUS_ACTIVE;
	var $not_active   		= STATUS_NOT_ACTIVE;
	var $certified   		= STATUS_CERTIFIED;

	public function __construct()
	{
		parent::__construct();
	}

	public function get_mode_list()
	{
		try
		{
			return $this->select_all(['*'], BAS_Model::tbl_param_check_mode_payments);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_disbursement_list()
	{
		try
		{
			$disbursement 		= BAS_Model::tbl_disbursements;
			$param_disbursement = BAS_Model::tbl_param_disbursement_status; 


			$query = <<<EOS
				SELECT
				d.dv_no,
				d.disbursement_id,
				d.disbursement_status_id,

				p.disbursement_status_id,
				p.disbursement_status 

				FROM $disbursement d 
				JOIN $param_disbursement p ON p.disbursement_status_id = d.disbursement_status_id  
				WHERE p.disbursement_status = '$this->approved' 
EOS;
			return $this->query($query);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}	
	}

	public function get_specific_checks_2($hash_id, $key){

		try
		{
			$checks 					= BAS_Model::tbl_checks;
			$disbursement 				= BAS_Model::tbl_disbursements;
			$param_check_mode_payments 	= BAS_Model::tbl_param_check_mode_payments;

			$query = <<<EOS
				SELECT 
				c.*, 
				d.dv_no,
				d.dv_date,
				d.disbursement_id,
				m.mode_payment_id, 
				m.mode_type  

				FROM $checks c 
				JOIN $disbursement d ON d.disbursement_id = c.disbursement_id 
				JOIN $param_check_mode_payments m ON m.mode_payment_id = c.mode_of_payment  
				WHERE $key = ? 
EOS;

			return $this->query($query, [$hash_id]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_specific_checks($where, $multi=FALSE){

		try
		{
			$fields 	= array("*");
			$function	= ($multi) ? "select_all" : "select_one";
			
			return $this->{$function}($fields, BAS_Model::tbl_checks, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_checks_details($id)
	{
		try
		{
			$fields = array("*");
			$where = array('check_id' => $id);
			return $this->select_one($fields, BAS_Model::tbl_checks, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	
	
	
	public function get_checks_list($fields, $selects, $params)
	{
		try
		{
			$c_columns = [
				"dv_no",
				"dv_date",
				"disbursed_amount",
				"check_no",
				"check_date",
				"check_amount",
				"status_name"
			];

			$tbl_fields = str_replace(" , ", " ", implode(", ", $fields));
			
			$where 		= $this->filtering($c_columns, $params, FALSE);
			$order 		= $this->ordering($selects, $params);
			$limit 		= $this->paging($params);
			
			$filter_str 	= $where["search_str"];
			$filter_params 	= $where["search_params"];

			$checks 				= BAS_Model::tbl_checks;
			$disbursemet 			= BAS_Model::tbl_disbursements;
			$param_check_status 	= BAS_Model::tbl_param_check_status;
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS 
				$tbl_fields 
				FROM 
				$checks c 
				JOIN $disbursemet d ON d.disbursement_id = c.disbursement_id 
				LEFT JOIN $param_check_status p ON p.check_status_id = c.check_status_id 
				$filter_str
	        	$order  
	        	$limit 
EOS;
			$stmt = $this->query($query, $filter_params);
			return $stmt;
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, true);
			throw $e;
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, true);
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($fields, $selects, $params)
	{
		try
		{
			$this->get_checks_list($fields, $selects, $params);
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(check_id) cnt");
			return $this->select_one($fields, BAS_Model::tbl_checks);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function insert_check($params)
	{
		try
		{
			$status_id = $this->insert_data(BAS_Model::tbl_param_check_status, ['status_name'=>$params['status']], TRUE);

			$data = [
				'check_status_id' 		=> $status_id,
				'disbursement_id' 		=> filter_var($params['dv_no'], FILTER_SANITIZE_NUMBER_INT),
				'check_date' 			=> filter_var(date('Y-m-d', strtotime($params['check_date'])), FILTER_SANITIZE_NUMBER_INT),
				'check_no' 				=> filter_var($params['check_no'], FILTER_SANITIZE_STRING),
				'mode_of_payment'		=> filter_var($params['mode'], FILTER_SANITIZE_STRING),
				'check_amount' 			=> filter_var(str_replace(',','', $params['check_amount']), FILTER_SANITIZE_NUMBER_INT),
				'created_by'			=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
				'created_date'			=> date('Y-m-d H:i:s'),
			];
			
			return $this->insert_data(BAS_Model::tbl_checks, $data);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, true);
			throw $e;
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, true);
			throw $e;			
		}
	}
	
	public function update_check($params, $check_status_id)
	{
		try
		{
			$this->update_data(BAS_Model::tbl_param_check_status, ['status_name'=>$params['status']], ['check_status_id'=>$check_status_id]);

			if($params['status'] == $this->voided)
			{
				$data = [
					'remarks'				=> filter_var($params['remarks'], FILTER_SANITIZE_STRING),
				];
			}
			else
			{
				$data = [
					'disbursement_id' 		=> filter_var($params['dv_no'], FILTER_SANITIZE_NUMBER_INT),
					'check_date' 			=> filter_var(date('Y-m-d', strtotime($params['check_date'])), FILTER_SANITIZE_NUMBER_INT),
					'check_no' 				=> filter_var($params['check_no'], FILTER_SANITIZE_STRING),
					'mode_of_payment'		=> filter_var($params['mode'], FILTER_SANITIZE_STRING),
					'check_amount' 			=> filter_var(str_replace(',','', $params['check_amount']), FILTER_SANITIZE_NUMBER_INT),
					'modified_by'			=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
					'modified_date'			=> date('Y-m-d H:i:s'),
				];
			}
			
			return $this->update_data(BAS_Model::tbl_checks, $data, ['check_id'=> $params['check_id']]);
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}

	public function get_checks_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("check_id" => $id);
			return $this->select_one($fields, BAS_Model::tbl_checks, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	
}