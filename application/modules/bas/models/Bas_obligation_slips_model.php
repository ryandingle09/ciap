<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_obligation_slips_model extends BAS_Model {

	//fixed code define
	var $find_code_ps   	= PARAM_ACCOUNT_CODE_PS;//501
	var $find_code_mooe   	= PARAM_ACCOUNT_CODE_MOOE;//502
	var $find_code_co   	= PARAM_ACCOUNT_CODE_CO;//503

	//fix status
	var $voided   			= STATUS_VOIDED;
	var $excess   			= STATUS_EXCESS;
	var $overriden   		= STATUS_OVERRIDEN;
	var $posted   			= STATUS_POSTED;
	var $submitted   		= STATUS_SUBMITTED;
	var $approved   		= STATUS_APPROVED;
	var $disapproved   		= STATUS_DISAPPROVED;
	var $drafted   			= STATUS_DRAFTED;
	var $declined 			= STATUS_DECLINED;
	var $certified 			= STATUS_CERTIFIED;

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Bas_realignment_model', 'realignment');
	}

	public function get_payees_info($id)
	{
		try
		{
			$query = $this->select_one(array('payee_id','payee_address'), BAS_Model::tbl_param_payees, array('payee_id'=>$id));
			return $query['payee_address'];
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}

	public function check_exceed_amount($line_item, $account_code, $amount, $key)
	{
		try
		{
			return $this->select_one(['SUM(allotment_amount + realigned_amount - obligated_amount) as total'], BAS_Model::tbl_line_item_accounts, ['line_item_id'=> $line_item,'account_code'=> $account_code]);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}

	public function get_line_items($fund, $year)
	{
		try
		{
			$line_items = BAS_Model::tbl_line_items;

			$query = <<<EOS
				SELECT 
				ac.line_item_name,
				ac.line_item_id,
				ac.parent_line_item_id,
				ac.year,
				ac.source_type_num 
				FROM $line_items c
				JOIN $line_items a ON a.parent_line_item_id = c.line_item_id
				JOIN $line_items ac ON ac.parent_line_item_id = c.line_item_id
				WHERE ac.source_type_num = ? and ac.year = ? AND ac.line_item_id != c.parent_line_item_id
EOS;
		 	return $this->query($query, [$fund, $year]);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}

	public function get_code_and_office($selects, $table, $where)
	{
		try
		{
			//return $this->select_all($selects, $table, $where);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}

	public function fill_dropdowns($params, $identity)
	{
		try
		{
			if($identity == 'line_items')
			{
				$line_items = BAS_Model::tbl_line_items;

				$query = <<<EOS
					SELECT 
					ac.line_item_name,
					ac.line_item_id,
					ac.parent_line_item_id,
					ac.year,
					ac.source_type_num 
					FROM $line_items c
					JOIN $line_items a ON a.parent_line_item_id = c.line_item_id
					JOIN $line_items ac ON ac.parent_line_item_id = c.line_item_id
					WHERE ac.source_type_num = ? and ac.year = ? AND ac.line_item_id != c.parent_line_item_id
EOS;
			 return $this->query($query, array($params['fund'], $params['year']));

			}
			elseif($identity == 'account_code')
			{
				$line_item_id 			= $params['line_item_id'];
				$line_item_accounts 	= BAS_Model::tbl_line_item_accounts;
				$param_account_codes 	= BAS_Model::tbl_param_account_codes;

				$query = <<<EOS
					SELECT
					lia.account_code,
					pac.account_name,
					pac.account_code
					FROM $line_item_accounts lia
					JOIN $param_account_codes pac ON pac.account_code = lia.account_code
					WHERE lia.line_item_id = $line_item_id
EOS;

				return $this->query($query);
			}
			elseif($identity == 'office')
			{
				$line_item_id 			= $params['line_item_id'];
				$line_item_offices 		= BAS_Model::tbl_line_item_offices;
				$param_offices 			= BAS_Model::tbl_param_offices;

				$query = <<<EOS
					SELECT
					lio.office_num,
					po.office_name,
					po.office_num
					FROM $line_item_offices lio
					JOIN $param_offices po ON po.office_num = lio.office_num
					WHERE lio.line_item_id = $line_item_id
EOS;

				return $this->query($query);
			}
			else return false;
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}

	public function get_specific_obligation($where, $multi=FALSE)
	{

		try
		{
			$fields 	= array("*");
			$function	= ($multi) ? "select_all" : "select_one";
			
			return $this->{$function}($fields, BAS_Model::tbl_obligations, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_obligation_details($id)
	{
		try
		{
			$fields = array("*");
			$where = array('obligation_id' => $id);
			return $this->select_one($fields, BAS_Model::tbl_obligations, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	
	
	
	public function get_obligation_list($fields, $selects, $params)
	{
		try
		{
			$c_columns 	= array("obligation_status", "payee_name", "particulars","requested_amount", "obligation_status","os_date","obligated_amount");
			$tbl_fields = str_replace(" , ", " ", implode(", ", $fields));
			
			$where = $this->filtering($c_columns, $params, FALSE);
			$order = $this->ordering($selects, $params);
			$limit = $this->paging($params);
			
			$filter_str 	= $where["search_str"];
			$filter_params 	= $where["search_params"];

			$obligations 				= BAS_Model::tbl_obligations;
			$param_obligation_status	= BAS_Model::tbl_param_obligation_status;
			$param_payees 				= BAS_Model::tbl_param_payees;
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS 
				
				o.obligation_id,
				o.os_no,
				o.requested_date,
				o.payee_id,
				o.particulars,
				o.obligation_status_id,
				FORMAT(o.obligated_amount, 0) obligated_amount,
				FORMAT(o.requested_amount, 0) requested_amount,

				pos.obligation_status_id,
				pos.obligation_status,

				pp.payee_id,
				pp.payee_name
				
				FROM $obligations o
				JOIN $param_obligation_status pos ON pos.obligation_status_id = o.obligation_status_id
				JOIN $param_payees pp ON pp.payee_id = o.payee_id 
				$filter_str
	        	$order
	        	$limit
EOS;

			$stmt = $this->query($query, $filter_params);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($fields, $selects, $params)
	{
		try
		{
			$this->get_obligation_list($fields, $selects, $params);
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(obligation_id) cnt");
			return $this->select_one($fields, BAS_Model::tbl_obligations);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function insert_obligation($params)
	{
		try
		{
			$date = new DateTime($params['requested_date']);
			$newdate = $date->format('Y-m-d'); // 2012-07-31

			$obligation_status_table = array(
				'obligation_status' => $params['status']
			);

			$status_id = $this->insert_data(BAS_Model::tbl_param_obligation_status, $obligation_status_table, TRUE);

			$obligation_table = [
				'requested_date'		=> filter_var($newdate, FILTER_SANITIZE_NUMBER_INT),
				'payee_id' 				=> filter_var($params['payee'], FILTER_SANITIZE_NUMBER_INT),
				'address' 				=> filter_var($params['address'], FILTER_SANITIZE_STRING),
				'particulars' 			=> filter_var($params['particulars'], FILTER_SANITIZE_STRING),
				'year' 					=> filter_var($params['year'], FILTER_SANITIZE_NUMBER_INT),
				'source_type_num' 		=> filter_var($params['fund_source'], FILTER_SANITIZE_NUMBER_INT),
				//'requested_amount' 		=> filter_var(str_replace(',','', $params['requested_amount']), FILTER_SANITIZE_NUMBER_INT),
				'obligation_status_id'	=> $status_id
			];

			$id = $this->insert_data(BAS_Model::tbl_obligations, $obligation_table, TRUE);


			$account_code 		= $params['account_code'];
			$office 	 		= $params['office'];
			$requested_amount 	= str_replace(',','', $params['requested_amount']);

			$total_requested_amount = 0;

			foreach($params['line_item'] as $key => $val)
			{

				$obligation_charges_table = array(
					'obligation_id'			=> $id,
					'line_item_id' 			=> filter_var($val, FILTER_SANITIZE_NUMBER_INT),
					'account_code' 			=> filter_var($account_code[$key], FILTER_SANITIZE_NUMBER_INT),
					'office_num' 			=> filter_var($office[$key], FILTER_SANITIZE_NUMBER_INT),
					'requested_amount' 		=> filter_var($requested_amount[$key], FILTER_SANITIZE_NUMBER_INT)
				);

				$this->insert_data(BAS_Model::tbl_obligation_charges, $obligation_charges_table);

				$total_requested_amount += $requested_amount[$key];
			}

			$this->update_data(BAS_Model::tbl_obligations, array('requested_amount'=>$total_requested_amount), array('obligation_id'=>$id));

			$name 	 		= $params['name'];
			$designation 	= $params['position'];

			foreach($params['role'] as $key => $val)
			{

				$obligation_signatory_table = array(
					'obligation_id'	=> $id,
					'role' 			=> filter_var($val, FILTER_SANITIZE_STRING),
					'name' 			=> filter_var($name[$key], FILTER_SANITIZE_STRING),
					'designation' 	=> filter_var($designation[$key], FILTER_SANITIZE_STRING),
				);

				$this->insert_data(BAS_Model::tbl_obligation_signatories, $obligation_signatory_table);

			}

			return $id;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}

	public function process_actions($params)
	{
		try
		{
			$date = new DateTime($params['obligated_date']);
			$newdate = $date->format('Y/m/d'); // 2012-07-31

			//if($params['status'] != $this->drafted || $params['status'] != $this->submitted || $params['status'] != $this->declined):

				if(ISSET($params['obligated_amount'])):

					//subtract the old obligation amount to line items table
					/*$get_sources = $this->select_all(
						array('account_code', 'line_item_id'),
						BAS_Model::tbl_obligation_charges,
						array('obligation_id' => $params['obligation_id'])
					);*/

					/*foreach($get_sources as $key => $val)
					{
						$account_codes 	= $val['account_code'];
						$line_item_ids 	= $val['line_item_id'];

						//check code matches source
						$pos1  	= strpos($account_codes, $this->find_code_ps);
						$pos2 	= strpos($account_codes, $this->find_code_mooe);
						$pos3 	= strpos($account_codes, $this->find_code_co);

						if ($pos1 !== false) $identity = 'ps';
						if ($pos2 !== false) $identity = 'mooe';
						if ($pos3 !== false) $identity = 'co';

						$amount_line_data = $this->select_one(array(''.$identity.'_obligation'),BAS_Model::tbl_line_items, array('line_item_id'=>$line_item_ids));

						//subtract
						$source_data = array(
							''.$identity.'_obligation' 	=> $amount_line_data[''.$identity.'_obligation'] - $this->get_total_obligation($params['obligation_id'])
						);

						$this->update_data(
							BAS_Model::tbl_line_items, 
							$source_data, 
							array('line_item_id'=> $line_item_ids)
						);
					}*/

					$total_obligated_amount = 0;

					foreach($params['obligated_amount'] as $key => $val)
					{
						$this->update_data(
							BAS_Model::tbl_obligation_charges,
							array('obligated_amount'=>filter_var(str_replace(',','', $val), FILTER_SANITIZE_NUMBER_INT)),
							array(
								'obligation_id'	=> $params['obligation_id'],
								'line_item_id' 	=> $params['line_item_id'][$key],
								'account_code' 	=> $params['account_code'][$key],
							)
						);

						$total_obligated_amount += $val;
					}

					//update obligation amount
					$this->update_data(BAS_Model::tbl_obligations, array('obligated_amount'=> $this->get_total_obligation($params['obligation_id'])), array('obligation_id'=>$params['obligation_id']));

					/*//add the new amount
					foreach($get_sources as $key => $val)
					{
						$account_codes 	= $val['account_code'];
						$line_item_ids 	= $val['line_item_id'];

						//check code matches sosurce
						$pos1  	= strpos($account_codes, $this->find_code_ps);
						$pos2 	= strpos($account_codes, $this->find_code_mooe);
						$pos3 	= strpos($account_codes, $this->find_code_co);

						if ($pos1 !== false) $identity = 'ps';
						if ($pos2 !== false) $identity = 'mooe';
						if ($pos3 !== false) $identity = 'co';

						$amount_line_data = $this->select_one(array(''.$identity.'_obligation'),BAS_Model::tbl_line_items, array('line_item_id'=>$line_item_ids));

						//subtract
						$source_data = array(
							''.$identity.'_obligation' 	=> $amount_line_data[''.$identity.'_obligation'] + $this->get_total_obligation($params['obligation_id'])
						);

						$this->update_data(
							BAS_Model::tbl_line_items, 
							$source_data, 
							array('line_item_id'=> $line_item_ids)
						);
					}*/

					//return true;
				endif;

			//endif;

			//---- UPDATE LINE ITEMS -----------------------------------------------------
			$line_items = $this->select_all(['*'],BAS_Model::tbl_obligation_charges,['obligation_id' => $params['obligation_id']]);

			foreach($line_items as $key => $row)
			{
				//get currrent mooe amount value
				$mooe = $this->select_one(['mooe_obligation'], BAS_Model::tbl_line_items, ['line_item_id'=>$row['line_item_id']]);

				switch ($params['status'])
				{
					case $this->certified://mooe_obligation = mooe_obligation + obligated_amount

						$update_me = [
							'mooe_obligation' => ($mooe['mooe_obligation'] + $row['obligated_amount'])
						];

						$this->update_data(BAS_Model::tbl_line_items,$update_me,['line_item_id'=>$row['line_item_id']]);

						break;

					case $this->overriden://mooe_obligation = mooe_obligation - obligated_amount

						$update_me = [
							'mooe_obligation' => ($mooe['mooe_obligation'] - $row['obligated_amount'])
						];

						$this->update_data(BAS_Model::tbl_line_items,$update_me,['line_item_id'=>$row['line_item_id']]);
					
						break;

					case $this->excess://mooe_obligation = mooe_obligation - (obligated_amount - disbursed_amount)

						$update_me = [
							'mooe_obligation' => ($mooe['mooe_obligation'] - ($row['obligated_amount'] - $row['disbursed_amount']))
						];

						$this->update_data(BAS_Model::tbl_line_items,$update_me,['line_item_id'=>$row['line_item_id']]);
					
						break;

					case $this->voided://mooe_obligation = mooe_obligation - obligated_amount

						$update_me = [
							'mooe_obligation' => ($mooe['mooe_obligation'] - $row['obligated_amount'])
						];

						$this->update_data(BAS_Model::tbl_line_items,$update_me,['line_item_id'=>$row['line_item_id']]);
					
						break;
				}
			}

			if(ISSET($params['obligated_date']) || ISSET($params['obligation_no']))
			{

				$date = new DateTime($params['obligated_date']);
				$newdate = $date->format('Y/m/d'); // 2012-07-31

				if($params['status'] == $this->certified)
				{
					$obligated_date 	= new DateTime($params['obligated_date']);

					$os_no 		= $params['obligation_no'];
					$os_date 	= $obligated_date->format('Y-m-d');

					if(empty($params['obligated_date']) || empty($params['obligation_no']))
					{
						$status = BAS_Model::tbl_param_obligation_status;

						$query = <<<EOS
								SELECT
								COUNT(obligation_status_id) as count 
								FROM $status
								WHERE obligation_status != 'SUBMITTED' and obligation_status != 'DRAFT' and obligation_status != 'DECLINE'
EOS;
					$count = $this->query($query);

						$os_no 		= ''.date("y").'-'.date("d").'-'.($count[0]['count'] + 1).'';
						$os_date 	= date('Y-m-d');
					}

					$this->update_data(
						BAS_Model::tbl_obligations, 
						array(
							'os_no'			=> filter_var($os_no, FILTER_SANITIZE_STRING), 
							'os_date'		=> filter_var($os_date, FILTER_SANITIZE_NUMBER_INT),
							'manual_flag'	=> (ISSET($params['manual_flag'])) ? '1' : '0',
						), 
						array('obligation_id'=> $params['obligation_id'])
					);
				}

			}

			if(ISSET($params['remarks']))
			{
				$this->update_data(BAS_Model::tbl_obligations, array('remarks'=>$params['remarks']), array('obligation_id'=>$params['obligation_id']));
			}

			$this->update_data(
				BAS_Model::tbl_param_obligation_status, 
				array('obligation_status' => $params['status']), 
				array('obligation_status_id'=> $params['obligation_status_id'])
			);

			return true;
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}

	public function get_total_obligation($obligation_id)
	{
		try
		{
			$fields = array("SUM(obligated_amount) total");
			$where	= array("obligation_id" => $obligation_id);
			$data = $this->select_one($fields, BAS_Model::tbl_obligation_charges, $where);
			return $data['total'];
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}
	
	public function update_obligation($params)
	{
		try
		{
			//----------------UPDATE OBLIGATION TABLE --------------------------------------

			$date = new DateTime($params['requested_date']);
			$newdate = $date->format('Y-m-d'); // 2012-07-31

			$obligation_status_table = array(
				'obligation_status' => $params['status']
			);

			$this->update_data(BAS_Model::tbl_param_obligation_status, $obligation_status_table, array('obligation_status_id'=> $params['obligation_status_id']));

			$where = array('obligation_id' => $params['obligation_id']);

			$data = array(
				'year'						=> filter_var($params['year'], FILTER_SANITIZE_NUMBER_INT),
				'requested_date' 			=> filter_var($newdate, FILTER_SANITIZE_NUMBER_INT),
				'source_type_num' 			=> filter_var($params['fund_source'], FILTER_SANITIZE_NUMBER_INT),
				'address' 					=> filter_var($params['address'], FILTER_SANITIZE_STRING),
				'particulars' 				=> filter_var($params['particulars'], FILTER_SANITIZE_STRING),
				'payee_id' 					=> filter_var($params['payee'], FILTER_SANITIZE_NUMBER_INT)
			);
			
			$this->update_data(BAS_Model::tbl_obligations, $data, $where);


			//--------------------- UPDATE OBLIGATION CHARGES TABLE PROCESS
/*
			$old_data = $this->select_all(array('*'), BAS_Model::tbl_obligation_charges ,array('obligation_id' => $params['obligation_id']));

			//subtract and delete the old amount
			foreach($old_data as $key => $val)
			{
				$account_codes 	= $val['account_code'];
				$line_item_ids 	= $val['line_item_id'];

				//check code matches sosurce
				$pos1  	= strpos($account_codes, $this->find_code_ps);
				$pos2 	= strpos($account_codes, $this->find_code_mooe);
				$pos3 	= strpos($account_codes, $this->find_code_co);

				if ($pos1 !== false) $identity = 'ps';
				if ($pos2 !== false) $identity = 'mooe';
				if ($pos3 !== false) $identity = 'co';

				$amount_line_data = $this->select_one(array(''.$identity.'_obligation'),BAS_Model::tbl_line_items, array('line_item_id'=>$line_item_ids));

				//subtract
				$source_data = array(
					''.$identity.'_obligation' 	=> $amount_line_data[''.$identity.'_obligation'] - $this->get_total_obligation($params['obligation_id'])
				);

				$this->update_data(
					BAS_Model::tbl_line_items, 
					$source_data, 
					array('line_item_id'=> $line_item_ids)
				);

				$this->delete_data(
					BAS_Model::tbl_obligation_charges, 
					array(
						'obligation_id'=>$params['obligation_id'],
						'line_item_id' => $line_item_ids,
						'account_code' => $account_codes
					)
				);
			}*/

			//add the new amount and insert new charges if any
			$account_code 		= $params['account_code'];
			$office 	 		= $params['office'];
			$requested_amount 	= str_replace(',','', $params['requested_amount']);

			$total_requested_amount = 0;
			
			//delete first the old to perform get delete then insert
			$this->delete_data(BAS_Model::tbl_obligation_charges, ['obligation_id'=>$params['obligation_id']]);

			foreach($params['line_item'] as $key => $val)
			{

				$obligation_charges_table = array(
					'obligation_id'			=> $params['obligation_id'],
					'line_item_id' 			=> filter_var($val, FILTER_SANITIZE_NUMBER_INT),
					'account_code' 			=> filter_var($account_code[$key], FILTER_SANITIZE_NUMBER_INT),
					'office_num' 			=> filter_var($office[$key], FILTER_SANITIZE_NUMBER_INT),
					'requested_amount' 		=> filter_var($requested_amount[$key], FILTER_SANITIZE_NUMBER_INT)
				);

				$this->insert_data(BAS_Model::tbl_obligation_charges, $obligation_charges_table);

				$total_requested_amount += $requested_amount[$key];
			}

			$this->update_data(BAS_Model::tbl_obligations, array('requested_amount'=>$total_requested_amount), array('obligation_id'=>$params['obligation_id']));
			$this->update_data(BAS_Model::tbl_obligations, array('obligated_amount'=>0), array('obligation_id'=>$params['obligation_id']));

			//--------SIGNATORY UPDATE OR INSERT NEW ---------------------------------------------------

			if(ISSET($params['update_role'])):

				$id 	 		= $params['update_signatory_id'];
				$name 	 		= $params['update_name'];
				$designation 	= $params['update_position'];

				foreach($params['update_role'] as $key => $val)
				{

					$obligation_signatory_table = array(
						'role' 			=> filter_var($val, FILTER_SANITIZE_STRING),
						'name' 			=> filter_var($name[$key], FILTER_SANITIZE_STRING),
						'designation' 	=> filter_var($designation[$key], FILTER_SANITIZE_STRING),
					);

					$this->update_data(BAS_Model::tbl_obligation_signatories, $obligation_signatory_table, array('signatory_id'=> $id[$key]));

				}

			endif;

			if(ISSET($params['role'])):

				$name 	 		= $params['name'];
				$designation 	= $params['position'];

				foreach($params['role'] as $key => $val)
				{

					$obligation_signatory_table = array(
						'obligation_id'	=> $params['obligation_id'],
						'role' 			=> filter_var($val, FILTER_SANITIZE_STRING),
						'name' 			=> filter_var($name[$key], FILTER_SANITIZE_STRING),
						'designation' 	=> filter_var($designation[$key], FILTER_SANITIZE_STRING),
					);

					$this->insert_data(BAS_Model::tbl_obligation_signatories, $obligation_signatory_table);

				}

			endif;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}
	
	public function delete_obligation($id)
	{
		try
		{
			$data = $this->select_one(
				array('obligation_status_id'),
				BAS_Model::tbl_obligations, 
				array('obligation_id'=>$id)
			);

			$old_data = $this->select_all(array('*'), BAS_Model::tbl_obligation_charges ,array('obligation_id' => $id));

			//subtract and delete the old amount
			foreach($old_data as $key => $val)
			{
				$account_codes 	= $val['account_code'];
				$line_item_ids 	= $val['line_item_id'];

				//check code matches sosurce
				$pos1  	= strpos($account_codes, $this->find_code_ps);
				$pos2 	= strpos($account_codes, $this->find_code_mooe);
				$pos3 	= strpos($account_codes, $this->find_code_co);

				if ($pos1 !== false) $identity = 'ps';
				if ($pos2 !== false) $identity = 'mooe';
				if ($pos3 !== false) $identity = 'co';

				$amount_line_data = $this->select_one(array(''.$identity.'_obligation'),BAS_Model::tbl_line_items, array('line_item_id'=>$line_item_ids));

				//subtract
				$source_data = array(
					''.$identity.'_obligation' 	=> $amount_line_data[''.$identity.'_obligation'] - $this->get_total_obligation($id)
				);

				$this->update_data(
					BAS_Model::tbl_line_items, 
					$source_data, 
					array('line_item_id'=> $line_item_ids)
				);
			}

			$this->delete_data(BAS_Model::tbl_obligation_signatories, array('obligation_id' => $id));	
			$this->delete_data(BAS_Model::tbl_obligation_charges, array('obligation_id' => $id));	
			$this->delete_data(BAS_Model::tbl_obligations, array('obligation_id' => $id));	

			$this->delete_data(
				BAS_Model::tbl_param_obligation_status, 
				array('obligation_status_id' => $data['obligation_status_id'])
			);			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function delete_row($where, $table)
	{
		try
		{
			if($table == 'line_item')
			{
				$this->delete_data(BAS_Model::tbl_obligation_charges, $where);
			}
			else
			{
				$this->delete_data(BAS_Model::tbl_obligation_signatories, $where);
			}

			return true;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_obligation_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("obligation_id" => $id);
			return $this->select_one($fields, BAS_Model::tbl_obligations, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_obligation_full_details($obligation_id)
	{
		try
		{	
			$obligation_table 	= BAS_Model::tbl_obligations;
			$payee_table	 	= BAS_Model::tbl_param_payees;
			$fund_source_table	= BAS_Model::tbl_param_source_types;
			$param_obligation_status	= BAS_Model::tbl_param_obligation_status;

			$query = <<<EOS
			SELECT
			o.requested_date,
			o.obligation_id,
			o.payee_id,
			o.address as payee_address,
			o.particulars,
			o.source_type_num,
			o.remarks,
			o.obligation_status_id,
			o.os_no,
			o.os_date,

			po.obligation_status_id,
			po.obligation_status,

			p.payee_id,
			p.payee_name,

			f.source_type_num,
			f.source_type_name

			FROM $obligation_table o
			JOIN $payee_table p ON p.payee_id = o.payee_id
			JOIN $fund_source_table f ON f.source_type_num = o.source_type_num
			JOIN $param_obligation_status po ON po.obligation_status_id = o.obligation_status_id
			WHERE o.obligation_id = $obligation_id
EOS;
			return $this->query($query);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_obligation_charges($obligation_id)
	{
		try
		{	
			$charges 			= BAS_Model::tbl_obligation_charges;
			$codes 			 	= BAS_Model::tbl_param_account_codes;
			$line_item 			= BAS_Model::tbl_line_items;

			$query = <<<EOS
			SELECT
			c.obligation_id,
			c.line_item_id,
			c.account_code,
			c.office_num,
			c.requested_amount,
			c.obligated_amount,
			c.disbursed_amount,

			co.account_code,
			co.account_name,

			l.line_item_id,
			l.line_item_code,
			l.line_item_name

			FROM $charges c
			JOIN $codes co ON co.account_code = c.account_code
			JOIN $line_item l ON l.line_item_id = c.line_item_id
			WHERE c.obligation_id = $obligation_id
EOS;
			return $this->query($query);
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}


	public function get_obligation_signatories($obligation_id)
	{
		try
		{	
			$signatories 			= BAS_Model::tbl_obligation_signatories;
			$query = <<<EOS
			SELECT * FROM $signatories WHERE obligation_id = $obligation_id
EOS;
			return $this->query($query);
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	
}