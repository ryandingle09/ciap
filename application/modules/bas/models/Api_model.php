<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_model extends BAS_Model {

	public function __construct()
	{
		parent::__construct();
	}

	public function insert($params)
	{
		try
		{
			//parse the json object
			foreach($params['data'] as $a => $params_in)
			{
				//if arrays has values and is present then do the process else no insertion action to be perform --------------------
				if(ISSET($params['cases']))
				{
					if(count($params_in['cases'] != 0))
					{
						foreach($params_in['cases'] as $key => $value)
						{
							return $this->insert_data(
								BAS_model::tbl_cases,
								[
									'case_id' 		=> $value['case_id'],
									'case_no' 		=> $value['case_no'],
									'case_title' 	=> $value['case_title'],
						            'created_by'    => $value['created_by'],
						            'created_date'  => $value['created_date'],
						        ]
							);
						}
					}
				}

				if(ISSET($params['arbitrators']))
				{
					if(count($params_in['arbitrators'] != 0))
					{
						foreach($params_in['arbitrators'] as $key => $value)
						{
							return $this->insert_data(
								BAS_model::tbl_arbitrators,
								[
									'arbitrator_id' => $value['arbitrator_id'],
									'name' 			=> $value['name'],
						            'created_by'    => $value['created_by'],
						            'created_date'  => $value['created_date'],
						        ]
							);
						}
					}
				}

				if(ISSET($params['case_arbitrator_deductions']))
				{
					if(count($params_in['case_arbitrator_deductions'] != 0))
					{
						foreach($params_in['case_arbitrator_deductions'] as $key => $value)
						{
							return $this->insert_data(
								BAS_model::tbl_case_arbitrator_deductions,
								[
									'arbitrator_payment_id'	=> $value['arbitrator_payment_id'],
									'arbitrator_id' 		=> $value['arbitrator_id'],
									'deduction_type_id' 	=> $value['deduction_type_id'],
						            'amount'				=> $value['amount']
						        ]
							);
						}
					}
				}

				if(ISSET($params['case_arbitrator_fee_shares']))
				{
					if(count($params_in['case_arbitrator_fee_shares'] != 0))
					{
						foreach($params_in['case_arbitrator_fee_shares'] as $key => $value)
						{
							return $this->insert_data(
								BAS_model::tbl_case_arbitrator_fee_shares,
								[
									'arbitrator_payment_id' => $value['arbitrator_payment_id'],
									'arbitrator_id' 		=> $value['arbitrator_id'],
									'stage_activity_id' 	=> $value['stage_activity_id'],
						            'percent'    			=> $value['percent'],
						            'total_amount'  		=> $value['total_amount'],
						        ]
							);
						}
					}
				}

				if(ISSET($params['case_arbitrator_payments']))
				{
					if(count($params_in['case_arbitrator_payments'] != 0))
					{
						foreach($params_in['case_arbitrator_payments'] as $key => $value)
						{
							return $this->insert_data(
								BAS_model::tbl_case_arbitrator_payments,
								[
									'arbitrator_payment_id' => $value['arbitrator_payment_id'],
									'case_id' 				=> $value['case_id'],
									'proceeding_stage_id' 	=> $value['proceeding_stage_id'],
						            'final_flag'    		=> $value['final_flag'],
						            'created_by'  			=> $value['created_by'],
						            'created_date'  		=> $value['created_date']
						        ]
							);
						}
					}
				}

				if(ISSET($params['param_proceeding_stages']))
				{
					if(count($params_in['param_proceeding_stages'] != 0))
					{
						foreach($params_in['param_proceeding_stages'] as $key => $value)
						{
							return $this->insert_data(
								BAS_model::tbl_param_proceeding_stages,
								[
									'proceeding_stage_id' 	=> $value['proceeding_stage_id'],
									'proceeding_stage' 		=> $value['proceeding_stage']
						        ]
							);
						}
					}
				}
			}
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
			
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function read($id)
	{
		try
		{
			if(!empty($id))
			{
				return $this->select_all(['*'],BAS_model::tbl_arbitrators, ['arbitrator_id'=>$id]);
			}
			else
			{
				return $this->select_all(['*'],BAS_model::tbl_arbitrators);
			}
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
			
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function delete($params)
	{
		try
		{
			//parse the json object to delete
			foreach($params['data'] as $a => $params_in)
			{
				//if arrays has values and is present then do the process else no delete action to be perform --------------------
				if(ISSET($params['cases']))
				{
					if(count($params['cases']) != 0)
					{	
						foreach($params_in['cases'] as $key => $value)
						{
							return $this->delete_data(
								BAS_model::tbl_cases,
								['case_id' 		=> $value['case_id']]
							);
						}
					}
				}
				
				if(ISSET($params['arbitrators']))
				{
					if(count($params['arbitrators']) != 0)
					{
						foreach($params_in['arbitrators'] as $key => $value)
						{
							return $this->delete_data(
								BAS_model::tbl_arbitrators,
								[
									'arbitrator_id' => $value['arbitrator_id']]
						        ]
							);
						}
					}
				}

				if(ISSET($params['case_arbitrator_deductions']))
				{
					if(count($params['case_arbitrator_deductions']) != 0)
					{
						foreach($params_in['case_arbitrator_deductions'] as $key => $value)
						{
							return $this->delete_data(
								BAS_model::tbl_case_arbitrator_deductions,
								[
									'arbitrator_payment_id'	=> $value['arbitrator_payment_id'],
									'arbitrator_id' 		=> $value['arbitrator_id'],
						        ]
							);
						}
					}
				}

				if(ISSET($params['case_arbitrator_fee_shares']))
				{
					if(count($params['case_arbitrator_fee_shares']) != 0)
					{
						foreach($params_in['case_arbitrator_fee_shares'] as $key => $value)
						{
							return $this->delete_data(
								BAS_model::tbl_case_arbitrator_fee_shares,
								[
									'arbitrator_payment_id' => $value['arbitrator_payment_id'],
									'arbitrator_id' 		=> $value['arbitrator_id'],
						        ]
							);
						}
					}
				}

				if(ISSET($params['case_arbitrator_payments']))
				{
					if(count($params['case_arbitrator_payments']) != 0)
					{
						foreach($params_in['case_arbitrator_payments'] as $key => $value)
						{
							return $this->delete_data(
								BAS_model::tbl_case_arbitrator_payments,
								[
									'arbitrator_payment_id' => $value['arbitrator_payment_id'],
									'case_id' 				=> $value['case_id'],
									'proceeding_stage_id' 	=> $value['proceeding_stage_id'],
						        ]
							);
						}
					}
				}

				if(ISSET($params['param_proceeding_stages']))
				{
					if(count($params['param_proceeding_stages']) != 0)
					{
						foreach($params_in['param_proceeding_stages'] as $key => $value)
						{
							return $this->delete_data(
								BAS_model::tbl_param_proceeding_stages,
								[
									'proceeding_stage_id' 	=> $value['proceeding_stage_id'],
						        ]
							);
						}
					}
				}
			}
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
			
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
}

?>