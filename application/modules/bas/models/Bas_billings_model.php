<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_billings_model extends BAS_Model {

	//fixed code define
	var $find_code_ps   	= PARAM_ACCOUNT_CODE_PS;//501
	var $find_code_mooe   	= PARAM_ACCOUNT_CODE_MOOE;//502
	var $find_code_co   	= PARAM_ACCOUNT_CODE_CO;//503

	//fix status
	var $voided   			= STATUS_VOIDED;
	var $excess   			= STATUS_EXCESS;
	var $overriden   		= STATUS_OVERRIDEN;
	var $posted   			= STATUS_POSTED;
	var $submitted   		= STATUS_SUBMITTED;
	var $approved   		= STATUS_APPROVED;
	var $disapproved   		= STATUS_DISAPPROVED;
	var $certified   		= STATUS_CERTIFIED;

	public function __construct()
	{
		parent::__construct();
	}

	public function check_exceed_amount_due($amount,$id)
	{
		try
		{
			return $this->select_one(['obligated_amount'], BAS_Model::tbl_obligations, ['obligation_id'=>$id]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_obligation_slip_nos()
	{
		try
		{
			$table 	= BAS_Model::tbl_obligations;
			$table2 = BAS_Model::tbl_param_obligation_status;

			$query = <<<EOS
				SELECT
				o.os_no, 
				o.obligation_id,
				o.obligation_status_id,
				os.obligation_status_id,
				os.obligation_status

				FROM $table o
				JOIN $table2 os ON os.obligation_status_id = o.obligation_status_id
				WHERE os.obligation_status = '$this->certified' OR os.obligation_status = '$this->overriden' OR os.obligation_status = '$this->excess'

EOS;
			return $this->query($query);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_obligation_slip_nos_for_disbursement()
	{
		try
		{
			$table 	= BAS_Model::tbl_obligations;
			$table2 = BAS_Model::tbl_param_obligation_status;

			$query = <<<EOS
				SELECT
				o.os_no, 
				o.obligation_id,
				o.obligation_status_id,
				os.obligation_status_id,
				os.obligation_status

				FROM $table o
				JOIN $table2 os ON os.obligation_status_id = o.obligation_status_id
				WHERE os.obligation_status = '$this->certified' OR os.obligation_status = '$this->overriden'

EOS;
			$stmt = $this->query($query);
			return $stmt; 
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_specific_billings_2($key, $hash_id){

		try
		{
			$billings 		= BAS_Model::tbl_billings;
			$obligations 	= BAS_Model::tbl_obligations;

			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS

				b.billing_id,
				b.obligation_id,
				b.bill_no,
				b.bill_date,
				b.due_date,
				b.amount_due,

				o.obligation_id as ob_id,
				o.os_no

				FROM $billings b
				JOIN $obligations o 
				ON o.obligation_id = b.obligation_id
				WHERE $key = ?
EOS;
			$stmt = $this->query($query, array($hash_id), TRUE);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_specific_billings($where)
	{
		try
		{
			$fields = array("billing_id","obligation_id","bill_no","bill_date","due_date","amount_due");
			return $this->select_one($fields, BAS_Model::tbl_billings, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	

	public function get_billings_details($id)
	{
		try
		{
			$fields = array("billing_id","obligation_id","bill_no","bill_date","due_date","amount_due");
			$where = array('billing_id' => $id);
			return $this->select_one($fields, BAS_Model::tbl_billings, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	
	
	
	public function get_billings_list($fields, $selects, $params)
	{
		try
		{
			$c_columns = array('amount_due', 'bill_no', 'bill_date', 'due_date', 'os_no');
			$tbl_fields = str_replace(" , ", " ", implode(", ", $fields));
			
			$where = $this->filtering($c_columns, $params, FALSE);
			$order = $this->ordering($selects, $params);
			$limit = $this->paging($params);
			
			$filter_str = $where["search_str"];
			$filter_params = $where["search_params"];

			$billings 		= BAS_Model::tbl_billings;
			$obligations 	= BAS_Model::tbl_obligations;
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS

				b.billing_id,
				b.obligation_id,
				b.bill_no,
				b.bill_date,
				b.due_date,
				b.amount_due,

				o.obligation_id,
				o.os_no

				FROM $billings b
				JOIN $obligations o ON o.obligation_id = b.obligation_id
				$filter_str
	        	$order
	        	$limit
EOS;
			$stmt = $this->query($query, $filter_params);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($fields, $selects, $params)
	{
		try
		{
			$this->get_billings_list($fields, $selects, $params);
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(billing_id) cnt");
			return $this->select_one($fields, BAS_Model::tbl_billings);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function insert_billings($params)
	{
		try
		{
			$bill_date 	= new DateTime($params['bill_date']);
			$due_date 	= new DateTime($params['due_date']);

			$data = array(
				'bill_date' 	=> filter_var($bill_date->format('Y-m-d'), FILTER_SANITIZE_STRING),
				'bill_no' 		=> filter_var($params['bill_no'], FILTER_SANITIZE_STRING),
				'due_date' 		=> filter_var($due_date->format('Y-m-d'), FILTER_SANITIZE_STRING),
				'amount_due' 	=> filter_var(str_replace(',','', $params['amount_due']), FILTER_SANITIZE_NUMBER_INT),
				'obligation_id' => filter_var($params['os_no'], FILTER_SANITIZE_NUMBER_INT)
			);
			
			return $this->insert_data(BAS_Model::tbl_billings, $data);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}
	
	public function update_billings($params)
	{
		try
		{
			$bill_date 	= new DateTime($params['bill_date']);
			$due_date 	= new DateTime($params['due_date']);

			$val = array(
				'bill_date' 	=> filter_var($bill_date->format('Y-m-d'), FILTER_SANITIZE_STRING),
				'bill_no' 		=> filter_var($params['bill_no'], FILTER_SANITIZE_STRING),
				'due_date' 		=> filter_var($due_date->format('Y-m-d'), FILTER_SANITIZE_STRING),
				'amount_due' 	=> filter_var(str_replace(',','', $params['amount_due']), FILTER_SANITIZE_NUMBER_INT),
				'obligation_id' => filter_var($params['os_no'], FILTER_SANITIZE_NUMBER_INT)
			);
			$where = array(
				'billing_id' => $params['billing_id']
			);
			$this->update_data(BAS_Model::tbl_billings, $val, $where);
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}
	
	public function delete_billings($id)
	{
		try
		{
			$this->delete_data(BAS_Model::tbl_billings, array('billing_id' => $id));				
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_billings_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("billing_id" => $id);
			return $this->select_one($fields, BAS_Model::tbl_billings, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	
}