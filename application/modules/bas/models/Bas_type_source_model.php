<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_type_source_model extends BAS_Model {

	//fixed code define
	var $find_code_ps   	= PARAM_ACCOUNT_CODE_PS;//501
	var $find_code_mooe   	= PARAM_ACCOUNT_CODE_MOOE;//502
	var $find_code_co   	= PARAM_ACCOUNT_CODE_CO;//503

	//fix status
	var $voided   			= STATUS_VOIDED;
	var $excess   			= STATUS_EXCESS;
	var $overriden   		= STATUS_OVERRIDEN;
	var $posted   			= STATUS_POSTED;
	var $submitted   		= STATUS_SUBMITTED;
	var $approved   		= STATUS_APPROVED;
	var $disapproved   		= STATUS_DISAPPROVED;

	public function __construct()
	{
		parent::__construct();
	}

	public function get_source_details($id)
	{
		try
		{
			$fields = array("source_type_num", "source_type_name","active_flag");
			$where = array('source_type_num' => $id);
			return $this->select_one($fields, BAS_Model::tbl_param_source_types, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	
	
	public function check_exists_source($name)
	{
		try
		{
			$data = $this->select_all(['source_type_name'], BAS_Model::tbl_param_source_types, ['source_type_name'=>$name]);
			return sizeof($data);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}
	
	public function get_source_list($fields, $selects, $params)
	{
		try
		{
			$c_columns = array("source_type_num", "source_type_name", "active_flag");
			$tbl_fields = str_replace(" , ", " ", implode(", ", $fields));
			
			$where = $this->filtering($c_columns, $params, FALSE);
			$order = $this->ordering($selects, $params);
			$limit = $this->paging($params);
			
			$filter_str = $where["search_str"];
			$filter_params = $where["search_params"];
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $tbl_fields 
				FROM %s
				$filter_str
	        	$order
	        	$limit
EOS;
			$table = sprintf($query, BAS_Model::tbl_param_source_types);
			$stmt = $this->query($table, $filter_params);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($fields, $selects, $params)
	{
		try
		{
			$this->get_source_list($fields, $selects, $params);
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(source_type_num) cnt");
			return $this->select_one($fields, BAS_Model::tbl_param_source_types);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function insert_source($params)
	{
		try
		{
			$data = array(
				'source_type_name' 	=> filter_var($params['source'], FILTER_SANITIZE_STRING),
				'active_flag' 		=> filter_var($params['status'], FILTER_SANITIZE_NUMBER_INT),
			);
			
			return $this->insert_data(BAS_Model::tbl_param_source_types, $data);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}
	
	public function update_source($params)
	{
		try
		{
			$val = array(
				'source_type_name'	=> filter_var($params['source'], FILTER_SANITIZE_STRING),
				'active_flag'		=> filter_var($params['status'], FILTER_SANITIZE_NUMBER_INT),
			);
			$where = array(
				'source_type_num' => filter_var($params['id'], FILTER_SANITIZE_NUMBER_INT),
			);

			$this->update_data(BAS_Model::tbl_param_source_types, $val, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}
	
	public function delete_source($id)
	{
		try
		{
			$this->delete_data(BAS_Model::tbl_param_source_types, array('source_type_num' => $id));				
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_source_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("source_type_num" => $id);
			return $this->select_one($fields, BAS_Model::tbl_param_source_types, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	
}