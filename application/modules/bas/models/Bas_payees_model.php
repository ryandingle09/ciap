<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_payees_model extends BAS_Model {

	//fixed code define
	var $find_code_ps   	= PARAM_ACCOUNT_CODE_PS;//501
	var $find_code_mooe   	= PARAM_ACCOUNT_CODE_MOOE;//502
	var $find_code_co   	= PARAM_ACCOUNT_CODE_CO;//503

	//fix status
	var $voided   			= STATUS_VOIDED;
	var $excess   			= STATUS_EXCESS;
	var $overriden   		= STATUS_OVERRIDEN;
	var $posted   			= STATUS_POSTED;
	var $submitted   		= STATUS_SUBMITTED;
	var $approved   		= STATUS_APPROVED;
	var $disapproved   		= STATUS_DISAPPROVED;
	
	public function __construct()
	{
		parent::__construct();
	}

	public function get_payees_details($id)
	{
		try
		{
			$fields = array("payee_id","payee_name","payee_address","payee_tin","vatable_flag","active_flag");
			$where = array('payee_id' => $id);
			return $this->select_one($fields, BAS_Model::tbl_param_payees, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	
	
	
	public function get_payees_list($fields, $selects, $params)
	{
		try
		{
			$c_columns = array("payee_name","payee_address","payee_tin","active_flag", "payee_id");
			$tbl_fields = str_replace(" , ", " ", implode(", ", $fields));
			
			$where = $this->filtering($c_columns, $params, FALSE);
			$order = $this->ordering($selects, $params);
			$limit = $this->paging($params);
			
			$filter_str = $where["search_str"];
			$filter_params = $where["search_params"];
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $tbl_fields 
				FROM %s
				$filter_str
	        	$order
	        	$limit
EOS;
			$table = sprintf($query, BAS_Model::tbl_param_payees);
			$stmt = $this->query($table, $filter_params);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($fields, $selects, $params)
	{
		try
		{
			$this->get_payees_list($fields, $selects, $params);
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(payee_id) cnt");
			return $this->select_one($fields, BAS_Model::tbl_param_payees);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function insert_payees($params)
	{
		try
		{
			$data = array(
				'payee_name' 	=> filter_var($params['payee'], FILTER_SANITIZE_STRING),
				'payee_address' => filter_var($params['address'], FILTER_SANITIZE_STRING),
				'payee_tin'		=> filter_var($params['payee'], FILTER_SANITIZE_STRING),
				'vatable_flag' 	=> filter_var($params['payee'], FILTER_SANITIZE_NUMBER_INT),
				'active_flag' 	=> filter_var($params['address'], FILTER_SANITIZE_NUMBER_INT),
			);
			
			return $this->insert_data(BAS_Model::tbl_param_payees, $data);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}
	
	public function update_payees($params)
	{
		try
		{

			$val = array(
				'payee_name' 	=> filter_var($params['payee'], FILTER_SANITIZE_STRING),
				'payee_address' => filter_var($params['address'], FILTER_SANITIZE_STRING),
				'payee_tin' 	=> filter_var($params['tin'], FILTER_SANITIZE_STRING),
				'vatable_flag' 	=> filter_var($params['vat'], FILTER_SANITIZE_STRING),
				'active_flag' 	=> filter_var($params['status'], FILTER_SANITIZE_STRING),
			);

			$where = array(
				'payee_id' => filter_var($params['id'], FILTER_SANITIZE_NUMBER_INT)
			);
			$this->update_data(BAS_Model::tbl_param_payees, $val, $where);
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}
	
	public function delete_payees($id)
	{
		try
		{
			$this->delete_data(BAS_Model::tbl_param_payees, array('payee_id' => $id));				
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_payees_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("payee_id" => $id);
			return $this->select_one($fields, BAS_Model::tbl_param_payees, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	
}