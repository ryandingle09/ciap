<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_reports_model extends BAS_Model {

	//fixed code define
	var $find_code_ps   	= PARAM_ACCOUNT_CODE_PS;//501
	var $find_code_mooe   	= PARAM_ACCOUNT_CODE_MOOE;//502
	var $find_code_co   	= PARAM_ACCOUNT_CODE_CO;//503

	//fix status
	var $voided   			= STATUS_VOIDED;
	var $excess   			= STATUS_EXCESS;
	var $overriden   		= STATUS_OVERRIDED;
	var $posted   			= STATUS_POSTED;
	var $submitted   		= STATUS_SUBMITTED;
	var $approved   		= STATUS_APPROVED;
	var $disapproved   		= STATUS_DISAPPROVED;
	var $denied   			= STATUS_DENIED;
	var $active   			= STATUS_ACTIVE;
	var $not_active   		= STATUS_NOT_ATIVE;

	public function __construct()
	{
		parent::__construct();
	}

	public function get_SAAODB_report($params)
	{
		try
		{
			$line_items 				= BAS_Model::tbl_line_items;	

			$disbursements 				= BAS_Model::tbl_disbursements;
			$disbursement_charges 		= BAS_Model::tbl_disbursement_charges;

			$obligations 				= BAS_Model::tbl_obligations;
			$obligation_charges 		= BAS_Model::tbl_obligation_charges;

			$line_item_realigns 		= BAS_Model::tbl_line_item_realigns;
			$line_item_realign_details 	= BAS_Model::tbl_line_item_realign_details;

			$line_item_accounts 		= BAS_Model::tbl_line_item_accounts;

			$ps 						= PARAM_ACCOUNT_CODE_PS;
			$mooe 						= PARAM_ACCOUNT_CODE_MOOE;
			$co 						= PARAM_ACCOUNT_CODE_CO;
			
			$from 	= ''.$params['from'].'';
			$to 	= ''.$params['to'].'';
			$year 	= ''.$params['year'].'';
			$source = ''.$params['fund_source'].'';

			$query = <<<EOS
				SELECT
				A.line_item_id,
				A.year,
				A.source_type_num,
				A.parent_line_item_id,
				A.line_item_code,
				A.line_item_name,
				A.total_appropriation,
				A.ps_appropriation,
				A.mooe_appropriation,
				A.co_appropriation,
				A.total_allotment,
				A.ps_allotment,
				A.mooe_allotment,
				A.co_allotment,
				A.ps_appropriation_realigned,
				A.mooe_appropriation_realigned,
				A.co_appropriation_realigned,
				A.ps_allotment_realigned,
				A.mooe_allotment_realigned,
				A.co_allotment_realigned,
				A.ps_obligation,
				A.mooe_obligation,
				A.ps_obligation,
				A.co_obligation,
				A.ps_disbursement,
				A.mooe_disbursement,
				A.co_disbursement,
				A.created_by,
				A.created_date,
				(A.ps_appropriation + A.mooe_appropriation + A.co_appropriation) as total_appropriation,
				(A.ps_allotment + A.mooe_allotment + A.co_allotment) as total_allotment,
				(A.ps_appropriation_realigned + A.mooe_appropriation_realigned + A.co_appropriation_realigned) as total_appropriation_realigned, 
				(A.ps_allotment_realigned + A.mooe_allotment_realigned + A.co_allotment_realigned) as total_allotment_realigned,

				B.*,
				C.*,
				D.*,
				E.*

				FROM 

				$line_items A

				LEFT JOIN (

					SELECT 
					A.line_item_id as o_line_item_id, A.obligated_amount, 					
					
					SUM(IF(B.os_date IS NOT NULL, A.obligated_amount, 0)) as total_obligated_amount, 
					SUM(IF(QUARTER(B.os_date) = 1, A.obligated_amount, 0)) obligated_amount_1,
					SUM(IF(QUARTER(B.os_date) = 2, A.obligated_amount, 0)) obligated_amount_2,
					SUM(IF(QUARTER(B.os_date) = 3, A.obligated_amount, 0)) obligated_amount_3,
					SUM(IF(QUARTER(B.os_date) = 4, A.obligated_amount, 0)) obligated_amount_4,

					SUM(IF(QUARTER(B.os_date) = 1, IF(LEFT(A.account_code, 3) = $ps, A.obligated_amount,0) , 0)) ps_ob_amount_1,
					SUM(IF(QUARTER(B.os_date) = 2, IF(LEFT(A.account_code, 3) = $ps, A.obligated_amount,0) , 0)) ps_ob_amount_2,
					SUM(IF(QUARTER(B.os_date) = 3, IF(LEFT(A.account_code, 3) = $ps, A.obligated_amount,0) , 0)) ps_ob_amount_3,
					SUM(IF(QUARTER(B.os_date) = 4, IF(LEFT(A.account_code, 3) = $ps, A.obligated_amount,0) , 0)) ps_ob_amount_4,

					SUM(IF(QUARTER(B.os_date) = 1, IF(LEFT(A.account_code, 3) = $mooe, A.obligated_amount,0) , 0)) mooe_ob_amount_1,
					SUM(IF(QUARTER(B.os_date) = 2, IF(LEFT(A.account_code, 3) = $mooe, A.obligated_amount,0) , 0)) mooe_ob_amount_2,
					SUM(IF(QUARTER(B.os_date) = 3, IF(LEFT(A.account_code, 3) = $mooe, A.obligated_amount,0) , 0)) mooe_ob_amount_3,
					SUM(IF(QUARTER(B.os_date) = 4, IF(LEFT(A.account_code, 3) = $mooe, A.obligated_amount,0) , 0)) mooe_ob_amount_4,

					SUM(IF(QUARTER(B.os_date) = 1, IF(LEFT(A.account_code, 3) = $co, A.obligated_amount,0) , 0)) co_ob_amount_1,
					SUM(IF(QUARTER(B.os_date) = 2, IF(LEFT(A.account_code, 3) = $co, A.obligated_amount,0) , 0)) co_ob_amount_2,
					SUM(IF(QUARTER(B.os_date) = 3, IF(LEFT(A.account_code, 3) = $co, A.obligated_amount,0) , 0)) co_ob_amount_3,
					SUM(IF(QUARTER(B.os_date) = 4, IF(LEFT(A.account_code, 3) = $co, A.obligated_amount,0) , 0)) co_ob_amount_4

					FROM $obligation_charges A

					JOIN $obligations B ON A.obligation_id = B.obligation_id

					WHERE B.os_date BETWEEN '$from' AND '$to' 

					GROUP BY A.line_item_id
					
				) B ON B.o_line_item_id = A.line_item_id


				LEFT JOIN (

					SELECT 

					A.line_item_id as d_line_item_id, A.disbursed_amount, A.account_code,
					
					SUM(IF(B.dv_date IS NOT NULL, A.disbursed_amount, 0)) as total_disbursed_amount, 
					SUM(IF(QUARTER(B.dv_date) = 1, A.disbursed_amount, 0)) disbursed_amount_1,
					SUM(IF(QUARTER(B.dv_date) = 2, A.disbursed_amount, 0)) disbursed_amount_2,
					SUM(IF(QUARTER(B.dv_date) = 3, A.disbursed_amount, 0)) disbursed_amount_3,
					SUM(IF(QUARTER(B.dv_date) = 4, A.disbursed_amount, 0)) disbursed_amount_4,

					SUM(IF(QUARTER(B.dv_date) = 1, IF(LEFT(A.account_code, 3) = $ps, A.disbursed_amount,0) , 0)) ps_dv_amount_1,
					SUM(IF(QUARTER(B.dv_date) = 2, IF(LEFT(A.account_code, 3) = $ps, A.disbursed_amount,0) , 0)) ps_dv_amount_2,
					SUM(IF(QUARTER(B.dv_date) = 3, IF(LEFT(A.account_code, 3) = $ps, A.disbursed_amount,0) , 0)) ps_dv_amount_3,
					SUM(IF(QUARTER(B.dv_date) = 4, IF(LEFT(A.account_code, 3) = $ps, A.disbursed_amount,0) , 0)) ps_dv_amount_4,

					SUM(IF(QUARTER(B.dv_date) = 1, IF(LEFT(A.account_code, 3) = $mooe, A.disbursed_amount,0) , 0)) mooe_dv_amount_1,
					SUM(IF(QUARTER(B.dv_date) = 2, IF(LEFT(A.account_code, 3) = $mooe, A.disbursed_amount,0) , 0)) mooe_dv_amount_2,
					SUM(IF(QUARTER(B.dv_date) = 3, IF(LEFT(A.account_code, 3) = $mooe, A.disbursed_amount,0) , 0)) mooe_dv_amount_3,
					SUM(IF(QUARTER(B.dv_date) = 4, IF(LEFT(A.account_code, 3) = $mooe, A.disbursed_amount,0) , 0)) mooe_dv_amount_4,

					SUM(IF(QUARTER(B.dv_date) = 1, IF(LEFT(A.account_code, 3) = $co, A.disbursed_amount,0) , 0)) co_dv_amount_1,
					SUM(IF(QUARTER(B.dv_date) = 2, IF(LEFT(A.account_code, 3) = $co, A.disbursed_amount,0) , 0)) co_dv_amount_2,
					SUM(IF(QUARTER(B.dv_date) = 3, IF(LEFT(A.account_code, 3) = $co, A.disbursed_amount,0) , 0)) co_dv_amount_3,
					SUM(IF(QUARTER(B.dv_date) = 4, IF(LEFT(A.account_code, 3) = $co, A.disbursed_amount,0) , 0)) co_dv_amount_4

					FROM $disbursement_charges A
					
					JOIN $disbursements B ON A.disbursement_id = B.disbursement_id

					WHERE B.dv_date BETWEEN '$from' AND '$to'

					GROUP BY A.line_item_id, A.account_code
					
				) C ON C.d_line_item_id = A.line_item_id

				LEFT JOIN (

					SELECT source_line_item_id, SUM(realigned_amount) transfer_to

					FROM line_item_realign_details

					GROUP BY source_line_item_id

				) D ON A.line_item_id = D.source_line_item_id

				LEFT JOIN (

					SELECT recipient_line_item_id, SUM(realigned_amount) transfer_from

					FROM line_item_realign_details

					GROUP BY recipient_line_item_id

				) E ON A.line_item_id = E.recipient_line_item_id

				WHERE A.source_type_num = '$source' AND A.year = '$year'

				ORDER BY A.line_item_id ASC
EOS;
			return $this->query($query);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
			
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}

	public function get_SAAODBE_report($params)
	{
		try
		{
			$line_items 			= BAS_Model::tbl_line_items;	

			$account_codes 			= BAS_Model::tbl_param_account_codes;	
			$line_item_accounts 	= BAS_Model::tbl_line_item_accounts; 

			$obligations 			= BAS_Model::tbl_obligations;
			$obligation_charges 	= BAS_Model::tbl_obligation_charges;

			$disbursements 			= BAS_Model::tbl_disbursements;
			$disbursement_charges 	= BAS_Model::tbl_disbursement_charges;
			
			$ps 					= PARAM_ACCOUNT_CODE_PS;
			$mooe 					= PARAM_ACCOUNT_CODE_MOOE;
			$co 					= PARAM_ACCOUNT_CODE_CO;
			
			$from 	= ''.$params['from'].'';
			$to 	= ''.$params['to'].'';
			$year 	= ''.$params['year'].'';
			$source = ''.$params['fund_source'].'';

			$query = <<<EOS

				SELECT 

				A.account_code,
				A.account_name,
				A.parent_account_code,
				A.active_flag,

				B.*,
				C.*,
				D.*,
				E.*,
				F.*
	
				FROM $account_codes A 

				LEFT JOIN (

					SELECT 

					A.line_item_id,
					A.account_code as b_account_code,
					A.appropriation_amount,
					A.allotment_amount,
					A.realigned_amount,
					A.obligated_amount,
					A.disbursed_amount

					FROM $line_item_accounts A 
					GROUP BY A.account_code 

				) B ON B.b_account_code = A.account_code 

				LEFT JOIN (

					SELECT 

					A.account_code as o_account_code, A.line_item_id as o_line_item_id, A.obligated_amount, 					
					
					SUM(IF(B.os_date IS NOT NULL, A.obligated_amount, 0)) as total_obligated_amount, 
					SUM(IF(QUARTER(B.os_date) = 1, A.obligated_amount, 0)) obligated_amount_1,
					SUM(IF(QUARTER(B.os_date) = 2, A.obligated_amount, 0)) obligated_amount_2,
					SUM(IF(QUARTER(B.os_date) = 3, A.obligated_amount, 0)) obligated_amount_3,
					SUM(IF(QUARTER(B.os_date) = 4, A.obligated_amount, 0)) obligated_amount_4

					FROM $obligation_charges A

					JOIN $obligations B ON A.obligation_id = B.obligation_id

					WHERE B.os_date BETWEEN '$from' AND '$to' 

					GROUP BY A.line_item_id
					
				) E ON E.o_line_item_id = B.line_item_id

				LEFT JOIN (

					SELECT  

					A.line_item_id as f_line_item_id, A.disbursed_amount, A.account_code as f_account_code,
					
					SUM(IF(B.dv_date IS NOT NULL, A.disbursed_amount, 0)) as total_disbursed_amount, 
					SUM(IF(QUARTER(B.dv_date) = 1, A.disbursed_amount, 0)) disbursed_amount_1,
					SUM(IF(QUARTER(B.dv_date) = 2, A.disbursed_amount, 0)) disbursed_amount_2,
					SUM(IF(QUARTER(B.dv_date) = 3, A.disbursed_amount, 0)) disbursed_amount_3,
					SUM(IF(QUARTER(B.dv_date) = 4, A.disbursed_amount, 0)) disbursed_amount_4

					FROM $disbursement_charges A
					
					JOIN $disbursements B ON A.disbursement_id = B.disbursement_id

					WHERE B.dv_date BETWEEN '$from' AND '$to'

					GROUP BY A.line_item_id, A.account_code
					
				) F ON F.f_line_item_id = B.line_item_id

				LEFT JOIN (

					SELECT source_line_item_id, SUM(realigned_amount) transfer_to

					FROM line_item_realign_details 

					GROUP BY source_line_item_id

				) D ON B.line_item_id = D.source_line_item_id

				LEFT JOIN (

					SELECT recipient_line_item_id, SUM(realigned_amount) transfer_from

					FROM line_item_realign_details

					GROUP BY recipient_line_item_id

				) C ON B.line_item_id = C.recipient_line_item_id

				ORDER BY A.account_code ASC

EOS;
			return $this->query($query);
			//return $this->query($query, [$source, $year]);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
			
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}

	public function get_SAAOB_report($params)
	{
		try
		{
			$line_items 				= BAS_Model::tbl_line_items;	

			$obligations 				= BAS_Model::tbl_obligations;
			$obligation_charges 		= BAS_Model::tbl_obligation_charges;

			$line_item_realigns 		= BAS_Model::tbl_line_item_realigns;
			$line_item_realign_details 	= BAS_Model::tbl_line_item_realign_details;

			$line_item_accounts 		= BAS_Model::tbl_line_item_accounts;

			$ps 						= PARAM_ACCOUNT_CODE_PS;
			$mooe 						= PARAM_ACCOUNT_CODE_MOOE;
			$co 						= PARAM_ACCOUNT_CODE_CO;
			
			$from 	= ''.$params['from'].'';
			$to 	= ''.$params['to'].'';
			$year 	= ''.$params['year'].'';
			$source = ''.$params['fund_source'].'';

			$query = <<<EOS
				SELECT
				A.line_item_id,
				A.year,
				A.source_type_num,
				A.parent_line_item_id,
				A.line_item_code,
				A.line_item_name,
				A.total_appropriation,
				A.ps_appropriation,
				A.mooe_appropriation,
				A.co_appropriation,
				A.total_allotment,
				A.ps_allotment,
				A.mooe_allotment,
				A.co_allotment,
				A.ps_appropriation_realigned,
				A.mooe_appropriation_realigned,
				A.co_appropriation_realigned,
				A.ps_allotment_realigned,
				A.mooe_allotment_realigned,
				A.co_allotment_realigned,
				A.ps_obligation,
				A.mooe_obligation,
				A.ps_obligation,
				A.co_obligation,
				A.ps_disbursement,
				A.mooe_disbursement,
				A.co_disbursement,
				A.created_by,
				A.created_date,
				(A.ps_appropriation + A.mooe_appropriation + A.co_appropriation) as total_appropriation,
				(A.ps_allotment + A.mooe_allotment + A.co_allotment) as total_allotment,
				(A.ps_appropriation_realigned + A.mooe_appropriation_realigned + A.co_appropriation_realigned) as total_appropriation_realigned, 
				(A.ps_allotment_realigned + A.mooe_allotment_realigned + A.co_allotment_realigned) as total_allotment_realigned,

				B.*,
				D.*,
				E.*

				FROM 

				$line_items A

				LEFT JOIN (

					SELECT 
					A.line_item_id as o_line_item_id, A.obligated_amount, 					
					
					SUM(IF(B.os_date IS NOT NULL, A.obligated_amount, 0)) as total_obligated_amount, 
					SUM(IF(QUARTER(B.os_date) = 1, A.obligated_amount, 0)) obligated_amount_1,
					SUM(IF(QUARTER(B.os_date) = 2, A.obligated_amount, 0)) obligated_amount_2,
					SUM(IF(QUARTER(B.os_date) = 3, A.obligated_amount, 0)) obligated_amount_3,
					SUM(IF(QUARTER(B.os_date) = 4, A.obligated_amount, 0)) obligated_amount_4,

					SUM(IF(QUARTER(B.os_date) = 1, IF(LEFT(A.account_code, 3) = $ps, A.obligated_amount,0) , 0)) ps_ob_amount_1,
					SUM(IF(QUARTER(B.os_date) = 2, IF(LEFT(A.account_code, 3) = $ps, A.obligated_amount,0) , 0)) ps_ob_amount_2,
					SUM(IF(QUARTER(B.os_date) = 3, IF(LEFT(A.account_code, 3) = $ps, A.obligated_amount,0) , 0)) ps_ob_amount_3,
					SUM(IF(QUARTER(B.os_date) = 4, IF(LEFT(A.account_code, 3) = $ps, A.obligated_amount,0) , 0)) ps_ob_amount_4,

					SUM(IF(QUARTER(B.os_date) = 1, IF(LEFT(A.account_code, 3) = $mooe, A.obligated_amount,0) , 0)) mooe_ob_amount_1,
					SUM(IF(QUARTER(B.os_date) = 2, IF(LEFT(A.account_code, 3) = $mooe, A.obligated_amount,0) , 0)) mooe_ob_amount_2,
					SUM(IF(QUARTER(B.os_date) = 3, IF(LEFT(A.account_code, 3) = $mooe, A.obligated_amount,0) , 0)) mooe_ob_amount_3,
					SUM(IF(QUARTER(B.os_date) = 4, IF(LEFT(A.account_code, 3) = $mooe, A.obligated_amount,0) , 0)) mooe_ob_amount_4,

					SUM(IF(QUARTER(B.os_date) = 1, IF(LEFT(A.account_code, 3) = $co, A.obligated_amount,0) , 0)) co_ob_amount_1,
					SUM(IF(QUARTER(B.os_date) = 2, IF(LEFT(A.account_code, 3) = $co, A.obligated_amount,0) , 0)) co_ob_amount_2,
					SUM(IF(QUARTER(B.os_date) = 3, IF(LEFT(A.account_code, 3) = $co, A.obligated_amount,0) , 0)) co_ob_amount_3,
					SUM(IF(QUARTER(B.os_date) = 4, IF(LEFT(A.account_code, 3) = $co, A.obligated_amount,0) , 0)) co_ob_amount_4

					FROM $obligation_charges A

					JOIN $obligations B ON A.obligation_id = B.obligation_id

					WHERE B.os_date BETWEEN '$from' AND '$to' 

					GROUP BY A.line_item_id
					
				) B ON B.o_line_item_id = A.line_item_id

				LEFT JOIN (

					SELECT source_line_item_id, SUM(realigned_amount) transfer_to

					FROM line_item_realign_details

					GROUP BY source_line_item_id

				) D ON A.line_item_id = D.source_line_item_id

				LEFT JOIN (

					SELECT recipient_line_item_id, SUM(realigned_amount) transfer_from

					FROM line_item_realign_details

					GROUP BY recipient_line_item_id

				) E ON A.line_item_id = E.recipient_line_item_id

				WHERE A.source_type_num = '$source' AND A.year = '$year'

				ORDER BY A.line_item_id ASC
EOS;
			return $this->query($query);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
			
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}

	public function get_SMFO_report($params)
	{
		try
		{
			$line_items 			= BAS_Model::tbl_line_items;	

			$account_codes 			= BAS_Model::tbl_param_account_codes;	
			$param_offices 			= BAS_Model::tbl_param_offices; 
			$line_item_offices 		= BAS_Model::tbl_line_item_offices; 
			$line_item_accounts 	= BAS_Model::tbl_line_item_accounts; 

			$obligations 			= BAS_Model::tbl_obligations;
			$obligation_charges 	= BAS_Model::tbl_obligation_charges;
			
			$ps 					= PARAM_ACCOUNT_CODE_PS;
			$mooe 					= PARAM_ACCOUNT_CODE_MOOE;
			$co 					= PARAM_ACCOUNT_CODE_CO;
			
			$from 	= ''.$params['from'].'';
			$to 	= ''.$params['to'].'';
			$year 	= ''.$params['year'].'';
			$source = ''.$params['fund_source'].'';
			$office = ''.$params['office'].'';

			$query = <<<EOS

				SELECT
				
				A.line_item_id,
				A.account_code,
				A.allotment_amount,

				B.*,
				C.*,
				D.*,
				E.*

				FROM $line_item_accounts A

				JOIN (

					SELECT 
					
					A.account_name,
					A.account_code as b_account_code

					FROM $account_codes A

				) B ON B.b_account_code = A.account_code 

				JOIN (

					SELECT 

					line_item_id,
					source_type_num,
					year

					FROM $line_items

				) E ON E.line_item_id = A.line_item_id

				LEFT JOIN (

					SELECT 
					
					A.line_item_id,
					A.office_num

					FROM $line_item_offices A

				) C ON C.line_item_id = A.line_item_id 

				LEFT JOIN (

					SELECT 

					A.line_item_id ,					
					A.account_code, 
					A.office_num,

					SUM(IF(B.os_date IS NOT NULL, A.obligated_amount, 0)) as total_obligated_amount

					FROM $obligation_charges A

					JOIN $obligations B ON A.obligation_id = B.obligation_id

					WHERE B.os_date BETWEEN '$from' AND '$to'

				) D ON D.line_item_id = A.line_item_id
		
				WHERE LEFT(A.account_code, 3) = '$mooe' AND C.office_num = '$office' AND E.source_type_num = '$source' AND E.year = '$year'

				GROUP BY A.account_code

EOS;
			//echo $query;die();
			return $this->query($query);
		}

		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
			
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}

	public function get_obligation_nos($params)
	{
		try
		{
			$line_items 			= BAS_Model::tbl_line_items;	 
			$disbursements 			= BAS_Model::tbl_disbursements;
			$obligations 			= BAS_Model::tbl_obligations;
			$disbursement_charges 	= BAS_Model::tbl_disbursement_charges;
			$obligation_charges 	= BAS_Model::tbl_obligation_charges;
			$param_account_codes 	= BAS_Model::tbl_param_account_codes;
			
			$from 		= ''.$params['from'].'';
			$to 		= ''.$params['to'].'';
			$year 		= ''.$params['year'].'';
			$source 	= ''.$params['fund_source'].'';
			$line_item 	= ''.$params['line_item'].'';
			$code 		=	 $params['allotment_class'];

			$query = <<<EOS
				SELECT 
				A.os_date,
				A.os_no,
				A.obligation_id
				FROM $obligations A 
				WHERE A.os_date IS NOT NULL AND A.os_no IS NOT NULL 
				AND MONTH(A.os_date) BETWEEN MONTH('$from') AND MONTH('$to')
				GROUP BY A.os_no 
EOS;
			//echo $query;die();
			return $this->query($query);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
			
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}


	public function get_amount_per_account_code($params, $account_code)
	{
		try
		{
			$line_items 			= BAS_Model::tbl_line_items;	 
			$disbursements 			= BAS_Model::tbl_disbursements;
			$obligations 			= BAS_Model::tbl_obligations;
			$disbursement_charges 	= BAS_Model::tbl_disbursement_charges;
			$obligation_charges 	= BAS_Model::tbl_obligation_charges;
			$param_account_codes 	= BAS_Model::tbl_param_account_codes;
			
			$from 		= ''.$params['from'].'';
			$to 		= ''.$params['to'].'';
			$year 		= ''.$params['year'].'';
			$source 	= ''.$params['fund_source'].'';
			$line_item 	= ''.$params['line_item'].'';
			$code 		=	 $params['allotment_class'];

			$query = <<<EOS
				SELECT 
				A.os_date,
				A.os_no,
				A.obligation_id,
				B.line_item_id,
				B.account_code,
				B.obligated_amount,
				C.account_name 
				FROM $obligations A 
				JOIN $obligation_charges B ON B.obligation_id = A.obligation_id 
				JOIN $param_account_codes C ON C.account_code = B.account_code 
				WHERE A.os_date IS NOT NULL AND A.os_no IS NOT NULL AND B.line_item_id = '$line_item' 
				AND MONTH(A.os_date) BETWEEN MONTH('$from') AND MONTH('$to')
				AND LEFT(B.account_code, 3) = '$account_code' 
				GROUP BY A.os_no
EOS;
			//echo $query;die();
			return $this->query($query);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
			
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}

		public function get_amount_per_account_code_2($params, $account_code, $os_no)
	{
		try
		{
			$line_items 			= BAS_Model::tbl_line_items;	 
			$disbursements 			= BAS_Model::tbl_disbursements;
			$obligations 			= BAS_Model::tbl_obligations;
			$disbursement_charges 	= BAS_Model::tbl_disbursement_charges;
			$obligation_charges 	= BAS_Model::tbl_obligation_charges;
			$param_account_codes 	= BAS_Model::tbl_param_account_codes;
			
			$from 		= ''.$params['from'].'';
			$to 		= ''.$params['to'].'';
			$year 		= ''.$params['year'].'';
			$source 	= ''.$params['fund_source'].'';
			$line_item 	= ''.$params['line_item'].'';
			$code 		=	 $params['allotment_class'];

			$query = <<<EOS
				SELECT 
				A.os_date,
				A.os_no, 
				A.obligation_id, 
				B.line_item_id, 
				B.account_code, 
				SUM(B.obligated_amount) total_obligated_amount, 
				C.account_name 
				FROM $obligations A 
				JOIN $obligation_charges B ON B.obligation_id = A.obligation_id 
				JOIN $param_account_codes C ON C.account_code = B.account_code 
				WHERE A.os_no = '$os_no' AND B.line_item_id = '$line_item' 
				AND B.account_code = '$account_code' 
				AND MONTH(A.os_date) BETWEEN MONTH('$from') AND MONTH('$to') 
EOS;
			//echo $query;die();
			$data = $this->query($query);

			if(empty($data))
			{
				return '-';
			}
			else
			{
				foreach ($data as $key => $value)
				{
					return (!is_null($value['total_obligated_amount'])) ? number_format($value['total_obligated_amount']) : ' - ';
				}
			}
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
			
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}

	public function get_account_codes($code, $identity)
	{
		try
		{
			$param_account_codes 			= BAS_Model::tbl_param_account_codes;	 

			if($identity == 'parent')
			{
				$query = <<<EOS
					SELECT
					ac.account_name,
					ac.account_code,
					ac.parent_account_code 
					FROM $param_account_codes c
					JOIN $param_account_codes a ON a.parent_account_code != c.account_code
					JOIN $param_account_codes ac ON ac.parent_account_code != c.account_code 
					WHERE LEFT(ac.account_code, 3) = ? and ac.account_code = c.parent_account_code 
					GROUP BY ac.account_code
EOS;
			}
			else if($identity == 'child')
			{
				$query = <<<EOS
					SELECT
					A.account_name,
					A.account_code
					FROM $param_account_codes A
					WHERE A.parent_account_code = '$code'
EOS;
			}
			else
			{
				return 0;
			}

			return $this->query($query,[$code]);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
			
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}


	public function get_total_allotments_by_child_account_code($params, $code)
	{
		try
		{	
			$line_item_releases 			= BAS_Model::tbl_line_item_releases;
			$line_item_release_details 		= BAS_Model::tbl_line_item_release_details;

			$line_item = ''.$params['line_item'].'';

			$query = <<<EOS
				SELECT 
				A.release_id,
				A.line_item_id,
				B.account_code,
				SUM(B.allotment_amount) total_allotment_amount
				FROM $line_item_releases A 
				JOIN $line_item_release_details B ON B.release_id = A.release_id AND A.line_item_id = '$line_item'
				WHERE B.account_code = '$code' 
EOS;
			//echo $query;die();
			$data = $this->query($query);

			if(empty($data))
			{
				return '-';
			}
			else
			{
				foreach ($data as $key => $value)
				{
					return (!is_null($value['total_allotment_amount'])) ? number_format($value['total_allotment_amount']) : ' - ';
				}
			}
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
			
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}

	public function get_line_item_data($id)
	{
		try
		{
			return $this->select_one(['*'], BAS_Model::tbl_line_items, ['line_item_id'=>$id]);
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
			
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}

	public function fill_dropdowns($identity, $value, $value2)
	{
		try
		{
			$fund_sources 		= BAS_Model::tbl_fund_sources;
			$param_source_types = BAS_Model::tbl_param_source_types;
			$line_items 		= BAS_Model::tbl_line_items;
			$param_offices 		= BAS_Model::tbl_param_offices;


			if($identity == 'year')
			{
				$query = <<<EOS
					SELECT 
					s.source_type_num,
					s.year,
					st.source_type_name,
					st.source_type_num 

					FROM $fund_sources s 
					JOIN $param_source_types st ON st.source_type_num = s.source_type_num 
					WHERE s.year = ?
EOS;
				return $this->query($query, [$value]);
			}
			elseif($identity == 'fund_source')
			{
				$query = <<<EOS
					SELECT 
					ac.line_item_name,
					ac.line_item_id,
					ac.parent_line_item_id,
					ac.year,
					ac.source_type_num 
					FROM $line_items c
					JOIN $line_items a ON a.parent_line_item_id = c.line_item_id
					JOIN $line_items ac ON ac.parent_line_item_id = c.line_item_id
					WHERE ac.year = ? and ac.source_type_num = ? AND ac.line_item_id != c.parent_line_item_id
EOS;
				return $this->query($query, [$value2, $value]);
			}
			elseif($identity == 'report_type')
			{
				$query = <<<EOS
					SELECT 
					*
					FROM $param_offices  
EOS;
				return $this->query($query);
			}
			else return false;
		}
		catch(PDOException $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
			
		}
		catch(Exception $e)
		{
			echo $this->rlog_error($e, TRUE);
			throw $e;
		}
	}
	
	
}