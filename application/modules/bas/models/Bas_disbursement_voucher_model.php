<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_disbursement_voucher_model extends BAS_Model {

	//fixed code define
	var $find_code_ps   	= PARAM_ACCOUNT_CODE_PS;//501
	var $find_code_mooe   	= PARAM_ACCOUNT_CODE_MOOE;//502
	var $find_code_co   	= PARAM_ACCOUNT_CODE_CO;//503

	//fix status
	var $voided   			= STATUS_VOIDED;
	var $excess   			= STATUS_EXCESS;
	var $overriden  		= STATUS_OVERRIDEN;
	var $posted   			= STATUS_POSTED;
	var $submitted   		= STATUS_SUBMITTED;
	var $approved   		= STATUS_APPROVED;
	var $disapproved   		= STATUS_DISAPPROVED;

	public function __construct()
	{
		parent::__construct();
	}

	public function get_print_info($hash_id, $key)
	{
		try
		{
			$disbursement 			= BAS_Model::tbl_disbursements;
			$obligation				= BAS_Model::tbl_obligations;
			$check					= BAS_Model::tbl_checks;
			$payee					= BAS_Model::tbl_param_payees;
			$billing				= BAS_Model::tbl_billings;

			$query = <<<EOS
				SELECT 

				d.payee_id,
				d.dv_no,
				d.dv_date,
				d.particulars,
				d.disbursed_amount,
				d.disbursement_id,
				d.billing_id,
				d.obligation_id,

				p.payee_id,
				p.payee_address,
				p.payee_name,
				p.payee_tin,

				o.os_no,
				o.os_date,
				o.obligation_id,

				b.obligation_id,
				b.amount_due,

				c.check_id,
				c.disbursement_id,
				c.mode_of_payment

				FROM $disbursement d  
				JOIN $payee p ON p.payee_id = d.payee_id  
				LEFT JOIN $billing b ON b.billing_id = d.billing_id  
				LEFT JOIN $obligation o ON o.obligation_id = d.obligation_id  
				LEFT JOIN $check c ON c.disbursement_id = d.disbursement_id   
				WHERE $key = ? 
				GROUP BY d.disbursement_id
EOS;

			return $this->query($query, [$hash_id]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_arbitrator_from_isca_deduction($id)
	{
		try
		{
			$case_arbitrator_deductions = BAS_Model::tbl_case_arbitrator_deductions;
			$param_deduction_types 		= BAS_Model::tbl_param_deduction_types;

			$query = <<<EOS
				SELECT 
				cad.*,
				pdt.*
				FROM $case_arbitrator_deductions cad 
				LEFT JOIN $param_deduction_types pdt ON pdt.deduction_id = cad.deduction_type_id 
				WHERE cad.arbitrator_id = '$id' 
EOS;
			return $this->query($query);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_deductions()
	{
		try
		{
			return $this->select_all(['*'], BAS_Model::tbl_param_deduction_types);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_arbitrators()
	{
		try
		{
			return $this->select_all(['arbitrator_id', 'name'], BAS_Model::tbl_arbitrators);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_cases()
	{
		try
		{
			return $this->select_all(['case_id', 'case_no'], BAS_Model::tbl_cases);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_proceeding_stages()
	{
		try
		{
			return $this->select_all(['*'], BAS_Model::tbl_param_proceeding_stages);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_charges($id)
	{
		try
		{
			return $this->select_all(['*'], BAS_Model::tbl_disbursement_charges, ['disbursement_id'=>$id]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_all_account_codes()
	{
		try
		{
			return $this->select_all(['*'], BAS_Model::tbl_param_account_codes);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_payees()
	{
		try
		{
			return $this->select_all(['*'], BAS_Model::tbl_param_payees);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_payee_info($id)
	{
		try
		{
			$payees = BAS_Model::tbl_param_payees;

			$query =  <<<EOS
				SELECT * FROM $payees WHERE payee_id = ?
EOS;
			return $this->query($query, [$id]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_obligation_info($value)
	{
		try
		{
			$fund 		= BAS_Model::tbl_param_source_types;
			$obligation = BAS_Model::tbl_obligations;
			$payee 		= BAS_Model::tbl_param_payees;

			$query = <<<EOS
				SELECT 
				o.*,
				p.*,
				f.*
				FROM $obligation o 
				JOIN $fund f ON f.source_type_num = o.source_type_num 
				JOIN $payee p ON p.payee_id = o.payee_id 
				WHERE o.obligation_id = ?
EOS;
			return $this->query($query,[$value]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_obligation_charges($value)
	{
		try
		{
			$charges 		= BAS_Model::tbl_obligation_charges;
			$line_items 	= BAS_Model::tbl_line_items;
			$account_code	= BAS_Model::tbl_param_account_codes;

			$query = <<<EOS
				SELECT 
				c.*,
				a.account_code,
				a.account_name,
				l.line_item_id,
				l.line_item_name,
				l.line_item_code 
				FROM $charges c 
				JOIN $account_code a ON a.account_code = c.account_code 
				JOIN $line_items l ON l.line_item_id = c.line_item_id 
				WHERE c.obligation_id = ?
EOS;
			return $this->query($query,[$value]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function fill_dropdowns($identity, $value, $extra)
	{
		try
		{
			$options = '';
			$where   = [];

			$billing 					= BAS_Model::tbl_billings;
			$payee 						= BAS_Model::tbl_param_payees;
			$case_arbitrator_payments 	= BAS_Model::tbl_case_arbitrator_payments;
			$cases 						= BAS_Model::tbl_cases;
			$arbitrators 				= BAS_Model::tbl_arbitrators;
			$case_arbitrator_payments 	= BAS_Model::tbl_case_arbitrator_payments;
			$case_arbitrator_fee_shares = BAS_Model::tbl_case_arbitrator_fee_shares;
			$param_proceeding_stages 	= BAS_Model::tbl_param_proceeding_stages;

			if($identity == 'obligation_slip')
			{
				$options = <<<EOS
					SELECT bill_no, billing_id FROM $billing WHERE obligation_id = ?
EOS;
				$where   = [$value];
			}

			$payee = BAS_Model::tbl_param_payees;

			if($identity == 'payee')
			{
				$options = <<<EOS
					SELECT * FROM $payee WHERE payee_id = ?
EOS;
				$where   = [$value];
			}

			if($identity == 'case_no')
			{
				$options = <<<EOS
					SELECT 
					cap.*,
					pps.proceeding_stage
					FROM $case_arbitrator_payments cap
					LEFT JOIN $param_proceeding_stages pps ON pps.proceeding_stage_id = cap.proceeding_stage_id
					WHERE 
					cap.case_id = ?
EOS;
				$where   = [$value];
			}

			if($identity == 'arbitrator')
			{
				$options = <<<EOS
					SELECT 
					cap.*,
					a.name,
					pps.arbitrator_id
					FROM $case_arbitrator_payments cap
					LEFT JOIN $case_arbitrator_fee_shares pps ON pps.arbitrator_payment_id = cap.arbitrator_payment_id
					JOIN $arbitrators a ON a.arbitrator_id = pps.arbitrator_id
					WHERE 
					cap.case_id = ?
EOS;
				$where   = [$value];
			}

			return $this->query($options, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
			
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_amount($identity, $value)
	{
		try
		{
			if($identity == 'arbitrator')
			{
				$fields = array("FORMAT(total_amount, 0) total_amount");
				return $this->select_one($fields, BAS_Model::tbl_case_arbitrator_fee_shares, ['arbitrator_id'=>$value]);
			}
			else
			{
				return ['total_amount'=>'0.00'];
			}
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function get_funds()
	{
		try
		{
			$fields = array("disbursement_fund_id", "fund_name");
			return $this->select_all($fields, BAS_Model::tbl_param_disbursement_funds);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function get_billings()
	{
		try
		{
			$fields = array("bill_no", "billing_id");
			return $this->select_all($fields, BAS_Model::tbl_billings);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function get_specific_disbursement($where, $multi=FALSE){

		try
		{
			$fields 	= array("*");
			$function	= ($multi) ? "select_all" : "select_one";
			
			return $this->{$function}($fields, BAS_Model::tbl_disbursements, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_specific_status($disbursement_status_id){

		try
		{
			return $this->select_one(['*'], BAS_Model::tbl_param_disbursement_status, ['disbursement_status_id'=>$disbursement_status_id]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}



	public function get_specific_disbursement_full($id){

		try
		{
			$disbursement 		= BAS_Model::tbl_disbursements;
			$obligations 		= BAS_Model::tbl_obligations;
			$fund_source		= BAS_Model::tbl_param_source_types;
			$funds 				= BAS_Model::tbl_param_disbursement_funds;
			$payee 				= BAS_Model::tbl_param_payees;
			$billing 			= BAS_Model::tbl_billings;
			$cases 				= BAS_Model::tbl_cases;
			$arbitrators		= BAS_Model::tbl_arbitrators;
			$proceeding			= BAS_Model::tbl_param_proceeding_stages;

			$query = <<<EOS
					SELECT 
					p.*, 
					f.*, 
					o.obligation_id, 
					o.os_no, o.os_date, 
					o.requested_date, 
					o.source_type_num, 
					fs.*, 
					b.*, 
					c.*,  
					a.*,
					pc.*,
					d.particulars
					FROM $disbursement d 
					LEFT JOIN $payee p on p.payee_id = d.payee_id 
					JOIN $funds f ON f.disbursement_fund_id = d.disbursement_fund_id 
					LEFT JOIN $obligations o ON o.obligation_id = d.obligation_id 
					LEFT JOIN $fund_source fs ON fs.source_type_num = o.source_type_num 
					LEFT JOIN $billing b ON b.billing_id = d.billing_id  
					LEFT JOIN $cases c ON c.case_id = d.case_no 
					LEFT JOIN $arbitrators a ON a.arbitrator_id = d.arbitrator_id 
					LEFT JOIN $proceeding pc ON pc.proceeding_stage_id = d.proceeding_stage_id
					WHERE d.disbursement_id = ?
EOS;
			return $this->query($query, [$id]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_disbursement_details($where)
	{
		try
		{
			$fields 	= array("*");
			$function	= ($multi) ? "select_all" : "select_one";
			
			return $this->{$function}($fields, BAS_Model::tbl_disbursement_charges, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	

	public function get_disbursement_details_by_item($id)
	{
		try
		{
			$charges 		= BAS_Model::tbl_disbursement_charges;

			$query = <<<EOS
				SELECT * FROM $charges WHERE disbursement_id = ?
EOS;
			return $this->query($query,[$id]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function get_disbursement_deductions($id)
	{
		try
		{
			$charges 		= BAS_Model::tbl_disbursement_deductions;

			$query = <<<EOS
				SELECT * FROM $charges WHERE disbursement_id = ?
EOS;
			return $this->query($query,[$id]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function get_disbursement_details_by_item_3($id)
	{
		try
		{
			$charges 		= BAS_Model::tbl_disbursement_deductions;

			$query = <<<EOS
				SELECT * FROM $charges WHERE disbursement_id = ?
EOS;
			return $this->query($query,[$id]);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}
	
	
	public function get_disbursement_list($fields, $selects, $params)
	{
		try
		{
			$c_columns = array
			(	
				'pf.fund_name', 
				'd.dv_no', 
				'd.dv_date', 
				'd.disbursed_amount', 
				'p.disbursement_status'
			);

			$tbl_fields = str_replace(" , ", " ", implode(", ", $fields));
			
			$where = $this->filtering($c_columns, $params, FALSE);
			$order = $this->ordering($selects, $params);
			$limit = $this->paging($params);

			$disbursement 				= BAS_Model::tbl_disbursements;
			$obligations 				= BAS_Model::tbl_obligations;
			$param_disbursement_status 	= BAS_Model::tbl_param_disbursement_status;
			$param_disbursement_funds 	= BAS_Model::tbl_param_disbursement_funds;
			
			$filter_str = $where["search_str"];
			$filter_params = $where["search_params"];
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS 
				$tbl_fields
				FROM $disbursement d
				LEFT JOIN $obligations o
				ON o.obligation_id = d.obligation_id 
				JOIN $param_disbursement_status p 
				ON p.disbursement_status_id = d.disbursement_status_id 
				JOIN $param_disbursement_funds pf 
				ON pf.disbursement_fund_id = d.disbursement_fund_id 
				$filter_str
	        	$order
	        	$limit 
EOS;
			$stmt = $this->query($query, $filter_params);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($fields, $selects, $params)
	{
		try
		{
			$this->get_disbursement_list($fields, $selects, $params);
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(disbursement_id) cnt");
			return $this->select_one($fields, BAS_Model::tbl_disbursements);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function get_account_codes()
	{
		try
		{
			$table = BAS_Model::tbl_param_account_codes;

			$get = <<<EOS
				SELECT
				ac.account_name,
				ac.account_code,
				ac.parent_account_code 
				FROM $table c
				JOIN $table a ON a.parent_account_code = c.account_code
				JOIN $table ac ON ac.parent_account_code = c.account_code 
				WHERE ac.account_code != c.parent_account_code  
				GROUP BY ac.account_code
EOS;
			return $this->query($get);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function insert_disbursement($params)
	{
		try
		{
			//process flow / insert status, disbursement, charges, deduction, tax -------------------------------

			//insert status ------------------------------------
			$status = [
				'disbursement_status' => $params['status']
			];

			$status_id = $this->insert_data(BAS_Model::tbl_param_disbursement_status, $status, TRUE);

			if($params['funds'] == 1)//GAA FUND
			{
				//insert disbursement -------------------------------
				$data = [
					'disbursement_fund_id' 	=> filter_var($params['funds'], FILTER_SANITIZE_NUMBER_INT),
					'payee_id' 				=> filter_var($params['payee'], FILTER_SANITIZE_NUMBER_INT),
					'obligation_id' 		=> filter_var($params['obligation_slip_no'], FILTER_SANITIZE_NUMBER_INT),
					'billing_id' 			=> filter_var($params['bill_no'], FILTER_SANITIZE_NUMBER_INT),
					'disbursement_status_id'=> $status_id,//
					'payee_address' 		=> filter_var($params['address'], FILTER_SANITIZE_STRING),
					'particulars' 			=> filter_var($params['particulars'], FILTER_SANITIZE_STRING),
					'created_by' 			=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
					'created_date' 			=> date('Y-m-d H:i:s')
				];
				
				$id = $this->insert_data(BAS_Model::tbl_disbursements, $data, TRUE);

				//insert charges ------------------------------------
				$ewt_rate 					= str_replace(',','', $params['ewt_rate']);
				$ewt_amount 				= str_replace(',','', $params['ewt_amount']);
				$vat_rate 					= str_replace(',','', $params['vat_rate']);
				$vat_amount 				= str_replace(',','', $params['vat_amount']);
				$base_tax_amount 			= str_replace(',','', $params['base_tax_amount']);

				$total_vat_amount 			= 0;
				$tatal_vate_rate 			= 0;
				$total_ewt_amount 			= 0;
				$total_ewt_rate 			= 0;
				$total_disburse_amount 		= 0;

				foreach($ewt_rate as $key => $val)
				{
					$charges = [
						'disbursement_id' 	=> $id,
						'line_item_id' 		=> filter_var($params['line_item'][$key], FILTER_SANITIZE_NUMBER_INT),
						'account_code' 		=> filter_var($params['account_code'][$key], FILTER_SANITIZE_NUMBER_INT),
						'obligated_amount' 	=> filter_var($params['obligated_amount'][$key], FILTER_SANITIZE_NUMBER_INT),
						'disbursed_amount' 	=> filter_var($params['disbursed_amount'][$key], FILTER_SANITIZE_NUMBER_INT),
						'base_tax_amount' 	=> filter_var($base_tax_amount[$key], FILTER_SANITIZE_NUMBER_INT),
						'ewt_rate' 			=> filter_var($val, FILTER_SANITIZE_NUMBER_INT),
						'ewt_amount' 		=> filter_var($ewt_amount[$key], FILTER_SANITIZE_NUMBER_INT),
						'vat_rate' 			=> filter_var($vat_rate[$key], FILTER_SANITIZE_NUMBER_INT),
						'vat_amount' 		=> filter_var($vat_amount[$key], FILTER_SANITIZE_NUMBER_INT)
					];

					$this->insert_data(BAS_Model::tbl_disbursement_charges, $charges);

					$total_vat_amount 		+= $vat_amount[$key];
					$total_vat_rate 		+= $vat_rate[$key];
					$total_ewt_rate 		+= $ewt_rate[$key];
					$total_ewt_amount 		+= $ewt_amount[$key];

					$total_disbursed_amount += str_replace(',','', $params['disbursed_amount'][$key]);
				}

				//update disbursement vat total, ewt total and disbursed total
				$count = $this->select_all(['COUNT(disbursement_id) as count'], BAS_Model::tbl_disbursements);

				$to_update = [
					'dv_date'				=> date('Y-m-d'),
					'dv_no'					=> date('y').'-'.date('m').'-'.($count[0]['count']),
					'disbursed_amount' 		=> $total_disbursed_amount,
					'vat_amount' 			=> $total_vat_amount,
					'vat_rate' 				=> $total_vat_rate,
					'ewt_rate' 				=> $total_ewt_rate,
					'wht_amount' 			=> $total_ewt_amount
				];

				$this->update_data(BAS_Model::tbl_disbursements, $to_update ,['disbursement_id'=> $id]);

				return $id;
			}

			if($params['funds'] == 2)//CMDF FUND
			{
				//insert disbursement -------------------------------
				$data = [
					'disbursement_fund_id' 	=> filter_var($params['funds'], FILTER_SANITIZE_NUMBER_INT),
					'payee_id' 				=> filter_var($params['payee'], FILTER_SANITIZE_NUMBER_INT),
					'disbursement_status_id'=> $status_id,//
					'payee_address' 		=> filter_var($params['address'], FILTER_SANITIZE_STRING),
					'particulars' 			=> filter_var($params['particulars'], FILTER_SANITIZE_STRING),
					'created_by' 			=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
					'created_date' 			=> date('Y-m-d H:i:s')
				];
				
				$id = $this->insert_data(BAS_Model::tbl_disbursements, $data, TRUE);

				//insert charges ------------------------------------
				$ewt_rate 					= str_replace(',','', $params['ewt_rate']);
				$ewt_amount 				= str_replace(',','', $params['ewt_amount']);
				$vat_rate 					= str_replace(',','', $params['vat_rate']);
				$vat_amount 				= str_replace(',','', $params['vat_amount']);
				$vat_amount					= str_replace(',','', $params['vat_amount']);
				$base_tax_amount 			= str_replace(',','', $params['base_tax_amount']);

				$total_vat_amount 			= 0;
				$tatal_vate_rate 			= 0;
				$total_ewt_amount 			= 0;
				$total_ewt_rate 			= 0;

				$total_disbursed_amount 	= 0;

				foreach($ewt_rate as $key => $val)
				{
					$charges = [
						'disbursement_id' 	=> $id,
						'account_code' 		=> filter_var($params['account_code'][$key], FILTER_SANITIZE_NUMBER_INT),
						'base_tax_amount' 	=> filter_var($base_tax_amount[$key], FILTER_SANITIZE_NUMBER_INT),
						'ewt_rate' 			=> filter_var($val, FILTER_SANITIZE_NUMBER_INT),
						'ewt_amount' 		=> filter_var($ewt_amount[$key], FILTER_SANITIZE_NUMBER_INT),
						'vat_rate' 			=> filter_var($vat_rate[$key], FILTER_SANITIZE_NUMBER_INT),
						'vat_amount' 		=> filter_var($vat_amount[$key], FILTER_SANITIZE_NUMBER_INT)
					];

					$this->insert_data(BAS_Model::tbl_disbursement_charges, $charges);

					$total_vat_amount 		+= $vat_amount[$key];
					$total_vat_rate 		+= $vat_rate[$key];
					$total_ewt_rate 		+= $ewt_rate[$key];
					$total_ewt_amount 		+= $ewt_amount[$key];

					$total_disbursed_amount += $base_tax_amount[$key];
				}

				//update disbursement vat total, ewt total and disbursed total
				$count = $this->select_all(['COUNT(disbursement_id) as count'], BAS_Model::tbl_disbursements);

				$to_update = [
					'dv_date'				=> date('Y-m-d'),
					'dv_no'					=> date('y').'-'.date('m').'-'.($count[0]['count']),
					'vat_amount' 			=> $total_vat_amount,
					'vat_rate' 				=> $total_vat_rate,
					'ewt_rate' 				=> $total_ewt_rate,
					'wht_amount' 			=> $total_ewt_amount,
					'disbursed_amount'		=> $total_disbursed_amount,
				];

				$this->update_data(BAS_Model::tbl_disbursements, $to_update ,['disbursement_id'=> $id]);

				return $id;
			}

			if($params['funds'] == 3)// CIAC FUND
			{
				//insert disbursement -------------------------------
				$data = [
					'disbursement_fund_id' 	=> filter_var($params['funds'], FILTER_SANITIZE_NUMBER_INT),
					'case_no' 				=> filter_var($params['case'], FILTER_SANITIZE_STRING),
					'disbursement_status_id'=> $status_id,//
					'proceeding_stage_id' 	=> filter_var($params['proceeding'], FILTER_SANITIZE_NUMBER_INT),
					'arbitrator_id' 		=> filter_var($params['arbitrator'], FILTER_SANITIZE_NUMBER_INT),
					'disbursed_amount' 		=> filter_var(str_replace(',','', $params['arbitrator_fee']), FILTER_SANITIZE_NUMBER_INT),
					'particulars' 			=> filter_var($params['particulars'], FILTER_SANITIZE_STRING),
					'created_by' 			=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
					'created_date' 			=> date('Y-m-d H:i:s')
				];
				
				$id = $this->insert_data(BAS_Model::tbl_disbursements, $data, TRUE);

				//insert charges ------------------------------------
				$deduction 					= $params['deduction_type'];
				$amount 					= str_replace(',','', $params['amount']);

				foreach($deduction as $key => $val)
				{
					$charges = [
						'disbursement_id' 	=> $id,
						'deduction' 		=> filter_var($val, FILTER_SANITIZE_STRING),
						'deduction_amount' 	=> filter_var($amount[$key], FILTER_SANITIZE_NUMBER_INT),
					];

					$this->insert_data(BAS_Model::tbl_disbursement_deductions, $charges);

					//update disbursement vat total, ewt total and disbursed total
					$count = $this->select_all(['COUNT(disbursement_id) as count'], BAS_Model::tbl_disbursements);

					$to_update = [
						'dv_date'				=> date('Y-m-d'),
						'dv_no'					=> date('y').'-'.date('m').'-'.($count[0]['count']),
					];

					$this->update_data(BAS_Model::tbl_disbursements, $to_update ,['disbursement_id'=> $id]);

					return $id;
				}
			}
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}
	
	public function update_disbursement($params)
	{
		try
		{
			//process flow / insert status, disbursement, charges, deduction, tax --------------------------------
			$status = [
				'disbursement_status' => $params['status']
			];

			$status_id = $this->update_data(BAS_Model::tbl_param_disbursement_status, $status, ['disbursement_status_id'=>$params['disbursement_status_id']]);

			if($params['funds'] == 1)//GAA FUND
			{
				if($params['status'] == $this->voided)
				{

					if($params['from_status'] == $this->approved)
					{
						$charges = $this->select_all(['*'], BAS_Model::tbl_disbursement_charges, ['disbursement_id'=> $params['disbursement_id']]);

						foreach ($charges as $key => $value) 
						{
							$account_codes 	= $value['account_code'];
							$line_item 		= $value['line_item_id'];

							//check code matches sosurce
							$pos1  			= strpos($account_codes, $this->find_code_ps);
							$pos2 			= strpos($account_codes, $this->find_code_mooe);
							$pos3 			= strpos($account_codes, $this->find_code_co);

							if ($pos1 !== false) $identity = 'ps';
							if ($pos2 !== false) $identity = 'mooe';
							if ($pos3 !== false) $identity = 'co';

							//line item --------------------------------------------
							$line_item_data = $this->select_one([''.$identity.'_disbursement'],BAS_Model::tbl_line_items, ['line_item_id'=>$line_item]);

							$data_line = [
								''.$identity.'_disbursement' 	=> ($line_item_data[''.$identity.'_disbursement'] - $value['disbursed_amount']),
							];

							$this->update_data(BAS_Model::tbl_line_items, $data_line, ['line_item_id'=> $line_item]);

							//account code ----------------------------------------
							$account_code_data = $this->select_one(['disbursed_amount'],BAS_Model::tbl_line_item_accounts, ['line_item_id'=> $line_item, 'account_code'=> $account_codes]);

							$data_code = [
								'disbursed_amount' 	=> ($account_code_data['disbursed_amount'] - $value['disbursed_amount'])
							];

							$this->update_data(BAS_Model::tbl_line_item_accounts, $data_code, ['line_item_id'=> $line_item, 'account_code'=> $account_codes]);
						}

					}
					
					//update sttus and remarks on dv table
					$data = [
						'remarks' 			=> filter_var($params['remarks'], FILTER_SANITIZE_STRING),
						'modified_by' 		=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
						'modified_date' 	=> date('Y-m-d H:i:s')
					];
					
					$this->update_data(BAS_Model::tbl_disbursements, $data, ['disbursement_id'=> $params['disbursement_id']]);

				}
				elseif($params['status'] == $this->approved)//trigger the action if approved
				{
					foreach($params['account_code'] as $key => $val)
					{
						$account_codes 	= $val;
						$line_item 		= $params['line_item'][$key];

						//check code matches sosurce
						$pos1  			= strpos($account_codes, $this->find_code_ps);
						$pos2 			= strpos($account_codes, $this->find_code_mooe);
						$pos3 			= strpos($account_codes, $this->find_code_co);

						if ($pos1 !== false) $identity = 'ps';
						if ($pos2 !== false) $identity = 'mooe';
						if ($pos3 !== false) $identity = 'co';

						//line item --------------------------------------------
						$line_item_data = $this->select_one([''.$identity.'_disbursement'],BAS_Model::tbl_line_items, ['line_item_id'=>$line_item]);

						$data_line = [
							''.$identity.'_disbursement' 	=> ($line_item_data[''.$identity.'_disbursement'] + $params['disbursed_amount'][$key]),
						];

						$this->update_data(BAS_Model::tbl_line_items, $data_line, ['line_item_id'=> $line_item]);

						//account code ----------------------------------------
						$account_code_data = $this->select_one(['disbursed_amount'],BAS_Model::tbl_line_item_accounts, ['line_item_id'=> $line_item, 'account_code'=> $val]);

						$data_code = [
							'disbursed_amount' 	=> ($account_code_data['disbursed_amount'] + $params['disbursed_amount'][$key])
						];

						$this->update_data(BAS_Model::tbl_line_item_accounts, $data_code, ['line_item_id'=> $line_item, 'account_code'=> $val]);
					}
				}
				elseif($params['status'] == $this->overriden && $params['from_status'] == $this->approved)//trigger if action has been aprroved/disaprroved already
				{
					foreach($params['account_code'] as $key => $val)
					{
						$account_codes 	= $val;
						$line_item 		= $params['line_item'][$key];

						//check code matches sosurce
						$pos1  			= strpos($account_codes, $this->find_code_ps);
						$pos2 			= strpos($account_codes, $this->find_code_mooe);
						$pos3 			= strpos($account_codes, $this->find_code_co);

						if ($pos1 !== false) $identity = 'ps';
						if ($pos2 !== false) $identity = 'mooe';
						if ($pos3 !== false) $identity = 'co';

						$current_data = $this->select_one(['disbursed_amount'],BAS_Model::tbl_disbursement_charges, ['line_item_id'=> $line_item, 'account_code'=> $val]);

						//line item --------------------------------------------
						$line_item_data = $this->select_one([''.$identity.'_disbursement'],BAS_Model::tbl_line_items, ['line_item_id'=>$line_item]);


						$data_line = [
							''.$identity.'_disbursement' 	=> ($line_item_data[''.$identity.'_disbursement'] - $current_data['disbursed_amount']),
						];

						$this->update_data(BAS_Model::tbl_line_items, $data_line, ['line_item_id'=> $line_item]);

						//account code ----------------------------------------
						$account_code_data = $this->select_one(['disbursed_amount'],BAS_Model::tbl_line_item_accounts, ['line_item_id'=> $line_item, 'account_code'=> $val]);


						$data_code = [
							'disbursed_amount' 	=> ($account_code_data['disbursed_amount'] - $current_data['disbursed_amount'])
						];

						$this->update_data(BAS_Model::tbl_line_item_accounts, $data_code, ['line_item_id'=> $line_item, 'account_code'=> $val]);
					}

					//process normal override
					//insert disbursement -------------------------------
					$data = [
						'disbursement_fund_id' 	=> filter_var($params['funds'], FILTER_SANITIZE_NUMBER_INT),
						'modified_by' 			=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
						'modified_date' 		=> date('Y-m-d H:i:s')
					];
					
					$this->update_data(BAS_Model::tbl_disbursements, $data, ['disbursement_id'=> $params['disbursement_id']]);
					
					//delete current 
					$this->delete_data(BAS_Model::tbl_disbursement_charges, ['disbursement_id'=> $params['disbursement_id']]);

					//insert charges ------------------------------------
					$ewt_rate 					= str_replace(',','', $params['ewt_rate']);
					$ewt_amount 				= str_replace(',','', $params['ewt_amount']);
					$vat_rate 					= str_replace(',','', $params['vat_rate']);
					$vat_amount 				= str_replace(',','', $params['vat_amount']);
					$base_tax_amount 			= str_replace(',','', $params['base_tax_amount']);

					$total_vat_amount 			= 0;
					$tatal_vate_rate 			= 0;
					$total_ewt_amount 			= 0;
					$total_ewt_rate 			= 0;
					$total_disburse_amount 		= 0;

					foreach($ewt_rate as $key => $val)
					{
						$charges = [
							'disbursement_id' 	=> $params['disbursement_id'],
							'line_item_id' 		=> filter_var($params['line_item'][$key], FILTER_SANITIZE_NUMBER_INT),
							'account_code' 		=> filter_var($params['account_code'][$key], FILTER_SANITIZE_NUMBER_INT),
							'obligated_amount' 	=> filter_var($params['obligated_amount'][$key], FILTER_SANITIZE_NUMBER_INT),
							'disbursed_amount' 	=> filter_var(str_replace(',','',$params['disbursed_amount'][$key]), FILTER_SANITIZE_NUMBER_INT),
							'base_tax_amount' 	=> filter_var($base_tax_amount[$key], FILTER_SANITIZE_NUMBER_INT),
							'ewt_rate' 			=> filter_var($val, FILTER_SANITIZE_NUMBER_INT),
							'ewt_amount' 		=> filter_var($ewt_amount[$key], FILTER_SANITIZE_NUMBER_INT),
							'vat_rate' 			=> filter_var($vat_rate[$key], FILTER_SANITIZE_NUMBER_INT),
							'vat_amount' 		=> filter_var($vat_amount[$key], FILTER_SANITIZE_NUMBER_INT)
						];

						$this->insert_data(BAS_Model::tbl_disbursement_charges, $charges);

						$total_vat_amount 		+= $vat_amount[$key];
						$total_vat_rate 		+= $vat_rate[$key];
						$total_ewt_rate 		+= $ewt_rate[$key];
						$total_ewt_amount 		+= $ewt_amount[$key];

						$total_disbursed_amount += str_replace(',','', $params['disbursed_amount'][$key]);
					}

					$to_update = [
						'disbursed_amount' 		=> $total_disbursed_amount,
						'vat_amount' 			=> $total_vat_amount,
						'vat_rate' 				=> $total_vat_rate,
						'ewt_rate' 				=> $total_ewt_rate,
						'wht_amount' 			=> $total_ewt_amount
					];

					$this->update_data(BAS_Model::tbl_disbursements, $to_update ,['disbursement_id'=> $params['disbursement_id']]);
				}
				else
				{
					//insert disbursement -------------------------------
					$data = [
						'disbursement_fund_id' 	=> filter_var($params['funds'], FILTER_SANITIZE_NUMBER_INT),
						'modified_by' 			=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
						'modified_date' 		=> date('Y-m-d H:i:s')
					];
					
					$this->update_data(BAS_Model::tbl_disbursements, $data, ['disbursement_id'=> $params['disbursement_id']]);
					
					//delete current 
					$this->delete_data(BAS_Model::tbl_disbursement_charges, ['disbursement_id'=> $params['disbursement_id']]);

					//insert charges ------------------------------------
					$ewt_rate 					= str_replace(',','', $params['ewt_rate']);
					$ewt_amount 				= str_replace(',','', $params['ewt_amount']);
					$vat_rate 					= str_replace(',','', $params['vat_rate']);
					$vat_amount 				= str_replace(',','', $params['vat_amount']);
					$base_tax_amount 			= str_replace(',','', $params['base_tax_amount']);

					$total_vat_amount 			= 0;
					$tatal_vate_rate 			= 0;
					$total_ewt_amount 			= 0;
					$total_ewt_rate 			= 0;
					$total_disburse_amount 		= 0;

					foreach($ewt_rate as $key => $val)
					{
						$charges = [
							'disbursement_id' 	=> $params['disbursement_id'],
							'line_item_id' 		=> filter_var($params['line_item'][$key], FILTER_SANITIZE_NUMBER_INT),
							'account_code' 		=> filter_var($params['account_code'][$key], FILTER_SANITIZE_NUMBER_INT),
							'obligated_amount' 	=> filter_var($params['obligated_amount'][$key], FILTER_SANITIZE_NUMBER_INT),
							'disbursed_amount' 	=> filter_var(str_replace(',','',$params['disbursed_amount'][$key]), FILTER_SANITIZE_NUMBER_INT),
							'base_tax_amount' 	=> filter_var($base_tax_amount[$key], FILTER_SANITIZE_NUMBER_INT),
							'ewt_rate' 			=> filter_var($val, FILTER_SANITIZE_NUMBER_INT),
							'ewt_amount' 		=> filter_var($ewt_amount[$key], FILTER_SANITIZE_NUMBER_INT),
							'vat_rate' 			=> filter_var($vat_rate[$key], FILTER_SANITIZE_NUMBER_INT),
							'vat_amount' 		=> filter_var($vat_amount[$key], FILTER_SANITIZE_NUMBER_INT)
						];

						$this->insert_data(BAS_Model::tbl_disbursement_charges, $charges);

						$total_vat_amount 		+= $vat_amount[$key];
						$total_vat_rate 		+= $vat_rate[$key];
						$total_ewt_rate 		+= $ewt_rate[$key];
						$total_ewt_amount 		+= $ewt_amount[$key];

						$total_disbursed_amount += str_replace(',','', $params['disbursed_amount'][$key]);
					}

					$to_update = [
						'disbursed_amount' 		=> $total_disbursed_amount,
						'vat_amount' 			=> $total_vat_amount,
						'vat_rate' 				=> $total_vat_rate,
						'ewt_rate' 				=> $total_ewt_rate,
						'wht_amount' 			=> $total_ewt_amount
					];

					$this->update_data(BAS_Model::tbl_disbursements, $to_update ,['disbursement_id'=> $params['disbursement_id']]);
				}

				return $params['disbursement_id'];
			}
			//---------------------------------------------------------------------------------------------------------
			if($params['funds'] == 2)//CMDF FUND
			{
				if($params['status'] == $this->voided)
				{
					
					//update sttus and remarks on dv table
					$data = [
						'remarks' 			=> filter_var($params['remarks'], FILTER_SANITIZE_STRING),
						'modified_by' 		=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
						'modified_date' 	=> date('Y-m-d H:i:s')
					];
					
					$this->update_data(BAS_Model::tbl_disbursements, $data, ['disbursement_id'=> $params['disbursement_id']]);
				}
				/*elseif($params['status'] == $this->overriden && $params['from_status'] == $this->approved)//trigger if action has been aprroved/disaprroved already
				{
					
					//delete current 
					$this->delete_data(BAS_Model::tbl_disbursement_charges, ['disbursement_id'=> $params['disbursement_id']]);

					//insert charges ------------------------------------
					$ewt_rate 					= str_replace(',','', $params['ewt_rate']);
					$ewt_amount 				= str_replace(',','', $params['ewt_amount']);
					$vat_rate 					= str_replace(',','', $params['vat_rate']);
					$vat_amount 				= str_replace(',','', $params['vat_amount']);
					$vat_amount					= str_replace(',','', $params['vat_amount']);
					$base_tax_amount 			= str_replace(',','', $params['base_tax_amount']);

					$total_vat_amount 			= 0;
					$tatal_vate_rate 			= 0;
					$total_ewt_amount 			= 0;
					$total_ewt_rate 			= 0;

					foreach($ewt_rate as $key => $val)
					{
						$charges = [
							'disbursement_id' 	=> $params['disbursement_id'],
							'account_code' 		=> filter_var($params['account_code'][$key], FILTER_SANITIZE_NUMBER_INT),
							'base_tax_amount' 	=> filter_var($base_tax_amount[$key], FILTER_SANITIZE_NUMBER_INT),
							'ewt_rate' 			=> filter_var($val, FILTER_SANITIZE_NUMBER_INT),
							'ewt_amount' 		=> filter_var($ewt_amount[$key], FILTER_SANITIZE_NUMBER_INT),
							'vat_rate' 			=> filter_var($vat_rate[$key], FILTER_SANITIZE_NUMBER_INT),
							'vat_amount' 		=> filter_var($vat_amount[$key], FILTER_SANITIZE_NUMBER_INT)
						];

						$this->insert_data(BAS_Model::tbl_disbursement_charges, $charges);

						$total_vat_amount 		+= $vat_amount[$key];
						$total_vat_rate 		+= $vat_rate[$key];
						$total_ewt_rate 		+= $ewt_rate[$key];
						$total_ewt_amount 		+= $ewt_amount[$key];
					}

					$to_update = [
						'vat_amount' 			=> $total_vat_amount,
						'vat_rate' 				=> $total_vat_rate,
						'ewt_rate' 				=> $total_ewt_rate,
						'wht_amount' 			=> $total_ewt_amount
					];

					$this->update_data(BAS_Model::tbl_disbursements, $to_update ,['disbursement_id'=> $params['disbursement_id']]);

					return $params['disbursement_id'];
				}*/
				else
				{
					//delete current 
					$this->delete_data(BAS_Model::tbl_disbursement_charges, ['disbursement_id'=> $params['disbursement_id']]);

					//insert charges ------------------------------------
					$ewt_rate 					= str_replace(',','', $params['ewt_rate']);
					$ewt_amount 				= str_replace(',','', $params['ewt_amount']);
					$vat_rate 					= str_replace(',','', $params['vat_rate']);
					$vat_amount 				= str_replace(',','', $params['vat_amount']);
					$vat_amount					= str_replace(',','', $params['vat_amount']);
					$base_tax_amount 			= str_replace(',','', $params['base_tax_amount']);

					$total_vat_amount 			= 0;
					$tatal_vate_rate 			= 0;
					$total_ewt_amount 			= 0;
					$total_ewt_rate 			= 0;

					$total_disbursed_amount 	= 0;

					foreach($ewt_rate as $key => $val)
					{
						$charges = [
							'disbursement_id' 	=> $params['disbursement_id'],
							'account_code' 		=> filter_var($params['account_code'][$key], FILTER_SANITIZE_NUMBER_INT),
							'base_tax_amount' 	=> filter_var($base_tax_amount[$key], FILTER_SANITIZE_NUMBER_INT),
							'ewt_rate' 			=> filter_var($val, FILTER_SANITIZE_NUMBER_INT),
							'ewt_amount' 		=> filter_var($ewt_amount[$key], FILTER_SANITIZE_NUMBER_INT),
							'vat_rate' 			=> filter_var($vat_rate[$key], FILTER_SANITIZE_NUMBER_INT),
							'vat_amount' 		=> filter_var($vat_amount[$key], FILTER_SANITIZE_NUMBER_INT),
							'disbursed_amount' 	=> filter_var($base_tax_amount[$key], FILTER_SANITIZE_NUMBER_INT)
						];

						$this->insert_data(BAS_Model::tbl_disbursement_charges, $charges);

						$total_vat_amount 		+= $vat_amount[$key];
						$total_vat_rate 		+= $vat_rate[$key];
						$total_ewt_rate 		+= $ewt_rate[$key];
						$total_ewt_amount 		+= $ewt_amount[$key];


						$total_disbursed_amount += $base_tax_amount[$key];
					}

					$to_update = [
						'vat_amount' 			=> $total_vat_amount,
						'vat_rate' 				=> $total_vat_rate,
						'ewt_rate' 				=> $total_ewt_rate,
						'wht_amount' 			=> $total_ewt_amount,
						'disbursed_amount'		=> $total_disbursed_amount,
					];

					$this->update_data(BAS_Model::tbl_disbursements, $to_update ,['disbursement_id'=> $params['disbursement_id']]);

					return $params['disbursement_id'];
				}
			}
			//---------------------------------------------------------------------------------------------------------
			if($params['funds'] == 3)// CIAC FUND OVERRIDE/UPDATE
			{
				if($params['status'] == $this->voided)
				{
					//delete deduction amounts upon void action
					//$this->delete_data(BAS_Model::tbl_disbursement_deductions, ['disbursement_id'=> $params['disbursement_id']]);

					$data = [
						//'disbursed_amount'	=> '0.00',
						'remarks'			=> filter_var($params['remarks'], FILTER_SANITIZE_STRING),
						'modified_by'		=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : '1',
						'modified_date'		=> date('Y-m-d H:i:s')
					];
					
					$this->update_data(BAS_Model::tbl_disbursements, $data, ['disbursement_id'=> $params['disbursement_id']]);
				}
				else
				{		
					// do process for ciac
					//delete current 
					$this->delete_data(BAS_Model::tbl_disbursement_deductions, ['disbursement_id'=> $params['disbursement_id']]);

					//insert charges ------------------------------------
					$deduction_type				= $params['deduction_type'];
					$amount 					= str_replace(',','', $params['amount']);

					foreach($deduction_type as $key => $val)
					{
						$charges = [
							'disbursement_id' 	=> $params['disbursement_id'],
							'deduction'		 	=> filter_var($val, FILTER_SANITIZE_STRING),
							'deduction_amount' 	=> filter_var($amount[$key], FILTER_SANITIZE_NUMBER_INT),
						];

						$this->insert_data(BAS_Model::tbl_disbursement_deductions, $charges);
					}

					return $params['disbursement_id'];
				}
			}
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}
	
	public function delete_disbursement($params)
	{
		try
		{
			if($params['identity'] == 'cmdf_account_code')
			{
				return true;
			}
			else
			{
				$this->delete_data(BAS_Model::tbl_disbursement_deductions, ['deduction_id'=> $params['id']]);
				return true;
			}
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_disbursement_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("disbursement_id" => $id);
			return $this->select_one($fields, BAS_Model::tbl_disbursements, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	
}