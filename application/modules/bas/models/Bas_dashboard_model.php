<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_dashboard_model extends BAS_Model {

	public function __construct()
	{
		parent::__construct();
	}
	
	public function get_dashboard_list($fields, $selects, $params)
	{
		try
		{
			$c_columns = array(
				'line_item_name', 
				'all_total_appropriation', 
				'all_total_allotment', 
				'all_total_obligation', 
				'all_total_disbursement',
				'utilization_rate',
				'unobligated_amount',
				'unpaid_amount'
			);

			$tbl_fields = str_replace(" , ", " ", implode(", ", $fields));
			
			$where = $this->filtering($c_columns, $params, FALSE);
			$order = $this->ordering($selects, $params);
			$limit = $this->paging($params);
			
			$filter_str = $where["search_str"];
			$filter_params = $where["search_params"];

			$line_items 			= BAS_Model::tbl_line_items;
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS

				line_item_name,

				FORMAT(ps_appropriation + mooe_appropriation + co_appropriation, 3) as all_total_appropriation,
				FORMAT(ps_allotment + mooe_allotment + co_allotment, 3) all_total_allotment,
				FORMAT(ps_disbursement + mooe_disbursement + co_disbursement, 3) all_total_disbursement,
				FORMAT(ps_obligation + mooe_obligation + co_obligation, 3) all_total_obligation,

				FORMAT(((ps_appropriation + mooe_appropriation + co_appropriation) / (ps_allotment + mooe_allotment + co_allotment)) * 100, 3) utilization_rate,
				FORMAT((ps_appropriation + mooe_appropriation + co_appropriation) - (ps_obligation + mooe_obligation + co_obligation), 3) unobligated_amount,
				FORMAT((ps_obligation + mooe_obligation + co_obligation) - (ps_disbursement + mooe_disbursement + co_disbursement), 3) unpaid_amount

				FROM $line_items

				$filter_str
	        	$order
	        	$limit
EOS;
			return $this->query($query, $filter_params);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	
	
	public function filtered_length($fields, $selects, $params)
	{
		try
		{
			$this->get_dashboard_list($fields, $selects, $params);
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(line_item_id) cnt");
			return $this->select_one($fields, BAS_Model::tbl_line_items);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function get_dashboard_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("line_item_id" => $id);
			return $this->select_one($fields, BAS_Model::tbl_line_items, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
}