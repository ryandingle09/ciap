<form id="form" class="form-basic">
	<input type="hidden" name="security" value="<?php echo $security; ?>">
	<input 
	type="hidden" 
	name="status" 
	value="<?php if($this->uri->segment(7) == 'override') echo STATUS_OVERRIDEN;elseif($this->uri->segment(7) == 'void') echo STATUS_VOIDED;else echo STATUS_POSTED;?>">
	<input type="hidden" name="" id="obligation_no_2" value="<?php echo $obligation_slip_no;?>">
	<input type="hidden" name="" id="bill_no_2" value="<?php echo $billing_id;?>">
	<input type="hidden" name="details" id="details" value='<?php echo $disbursement_id;?>'>
	<input type="hidden" name="payee_id" id="payee_id" value='<?php echo $payee;?>'>
	<input type="hidden" name="particulars_a" id="particulars_a" value='<?php echo $particulars;?>'>
	<input type="hidden" name="disbursement_status_id" id="disbursement_status_id" value='<?php echo $disbursement_status_id;?>'>
	<input type="hidden" name="from_status" value="<?php echo $current_status;?>">
	<?php if($this->uri->segment(7) == 'override' || $this->uri->segment(7) == 'void' || $this->uri->segment(7) == 'approval'):?>
	<input type="hidden" name="funds" value="<?php echo $fund;?>">
	<?php endif;?>

	<?php if($this->uri->segment(7) == 'void' || $this->uri->segment(7) == 'approval'):?>
	<div class="row" style="padding: 20px;">

		<div class="col s12">
			
			<table border="0" width="100%">
				<tr>
					<td width="40%">
						<div><b>DV NO.</b></div>
						<div><?php echo (!empty($dv_no)) ? $dv_no : '(Upon Proccessing)';?></div>
					</td>
					<td>
						<div><b>DV DATE.</b></div>
						<div><?php echo (!empty($dv_date)) ? $dv_date : '(Upon Proccessing)';?></div>
					</td>
				</tr>
				<?php foreach($full_details as $all => $val):?>
				<tr>
					<td>
						<div><b>FUND</b></div>
						<div><?php echo $val['fund_name'];?></div>
					</td>
				</tr>
				<?php if($val['disbursement_fund_id'] == '1'):?>
				<tr>
					<td>
						<div><b>OBLIGATION SLIP NO.</b></div>
						<div><?php echo $val['os_no'];?></div>
					</td>
					<td>
						<div><b>BILL NO.</b></div>
						<div><?php echo $val['bill_no'];?></div>
					</td>
				</tr>
				<tr>
					<td>
						<div><b>DATE OF REQUEST</b></div>
						<div><?php echo date('F j, Y', strtotime($val['requested_date']));?></div>
					</td>
				</tr>
				<tr>
					<td>
						<div><b>PAYEE</b></div>
						<div><?php echo $val['payee_name'];?></div>
					</td>
				</tr>
				<tr>
					<td>
						<div><b>ADDRESS</b></div>
						<div><?php echo $val['payee_address'];?></div>
					</td>
				</tr>
				<tr>
					<td>
						<div><b>PARTICULARS</b></div>
						<div><?php echo $particulars;?></div>
					</td>
				</tr>
				<tr>
					<td>
						<div><b>FUND SOURCE</b></div>
						<div><?php echo $val['source_type_name'];?></div>
					</td>
				</tr>
				<?php elseif($val['disbursement_fund_id'] == '2'):?>
				<tr>
					<td>
						<div><b>PAYEE</b></div>
						<div><?php echo $val['payee_name'];?></div>
					</td>
				</tr>
				<tr>
					<td>
						<div><b>ADDRESS</b></div>
						<div><?php echo $val['payee_address'];?></div>
					</td>
				</tr>
				<tr>
					<td>
						<div><b>PARTICULARS</b></div>
						<div><?php echo $particulars;?></div>
					</td>
				</tr>
				<?php 
				elseif($val['disbursement_fund_id'] == 3):
					foreach($full_details as $key => $row):
				?>
				<tr>
					<td>
						<div><b>CASE NO</b></div>
						<div><?php echo $row['case_no'];?></div>
					</td>
				</tr>
				<tr>
					<td>
						<div><b>STAGE OF PROCEEDING</b></div>
						<div><?php echo $row['proceeding_stage'];?></div>
					</td>
				</tr>
				<tr>
					<td>
						<div><b>ARBITRATOR</b></div>
						<div><?php echo $row['name'];?></div>
					</td>
				</tr>
				<tr>
					<td>
						<div><b>PARTICULARS</b></div>
						<div><?php echo $particulars;?></div>
					</td>
				</tr>
				<?php 
					endforeach;
				endif;
				?>
				<?php 
					$obligation_id 	= $val['obligation_id'];
				?>
				<?php endforeach;?>
			</table>

			<?php if($fund == 1):?>

			<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="tbl_gaa_fund">
				<thead class="teal lighten-5">
					<tr>
						<th>LINE ITEM</th>
						<th>ACCOUNT CODE</th>
						<th>REQUESTED AMOUNT</ht>
						<th>OBLIGATED AMOUNT</th>
						<th>DISBURSED AMOUNT</th>
						<th>TAX AMOUNT</th>
					</tr>
				</thead>
				<tbody>
					<?php 
						$total_ob_amount 	= 0;
						$total_rq_amount 	= 0;
						$total_db_amount 	= 0;
						$total_tax_amount 	= 0;
					?>
					<?php 
					$count = 0;
					foreach($obligation_charges as $kissme => $here):
					$count ++;
					?>
					<?php
					foreach($details2 as $keys => $row):
					?>
					<tr>
						<td>
							<?php echo $here['line_item_code']. ' - '.$here['line_item_name'];?>
							<input type="hidden" name="line_item[]" id="line_item_<?php echo $count;?>" class="line_item_<?php echo $key;?>" value="<?php echo $here['line_item_id'];?>">
						</td>
						<td>
							<?php echo $here['account_code']. ' - '.$here['account_name'];?>
							<input type="hidden" name="account_code[]" id="account_code_<?php echo $count;?>" class="account_code_<?php echo $key;?>" value="<?php echo $here['account_code'];?>">
						</td>
						<td>
							<?php echo number_format($here['requested_amount']);?>
							<input type="hidden" name="requested_amount[]" value="<?php echo $here['requested_amount'];?>">
						</td>
						<td>
							<?php echo number_format($here['obligated_amount']);?>
							<input type="hidden" name="obligated_amount[]" value="<?php echo $here['obligated_amount'];?>">
						</td>
						<td>
							<?php echo number_format($row['disbursed_amount']);?>
							<input type="hidden" name="disbursed_amount[]" id="disbursed_amount_<?php echo $count;?>" class="disbursed_amount_<?php echo $key;?>" value="<?php echo $row['disbursed_amount'];?>">
						</td>
						<td>
							<a href='javascript:void(0)' 
							class='tooltip md-trigger tooltipped tax_link_<?php echo $count;?>' 
							data-tooltip='Tax amount' data-position='bottom' data-delay='50' data-modal='modal_tax' 
							onclick="modal_tax_init('<?php echo $security;?>/tax/<?php echo $obligation_id;?>/<?php echo $count;?>/view')">[ <?php echo number_format($row['ewt_amount']+$row['vat_amount']);?> ]
							</a>
							<input type="hidden" name="tax_amount[]" value="<?php echo number_format($row['ewt_amount']+$row['vat_amount']);?>" id="tax_amount_<?php echo $count;?>">
							<input type="hidden" name="base_tax_amount[]" value="<?php echo number_format($row['base_tax_amount']);?>" id="base_tax_amount_<?php echo $count;?>">
							<input type="hidden" name="ewt_rate[]" value="<?php echo floatval($row['ewt_rate']);?>" id="ewt_rate_<?php echo $count;?>">
							<input type="hidden" name="ewt_amount[]" value="<?php echo number_format($row['ewt_amount']);?>" id="ewt_amount_<?php echo $count;?>">
							<input type="hidden" name="vat_rate[]" value="<?php echo floatval($row['vat_rate']);?>" id="vat_rate_<?php echo $count;?>">
							<input type="hidden" name="vat_amount[]" value="<?php echo number_format($row['vat_amount']);?>" id="vat_amount_<?php echo $count;?>">
						</td>
					</tr>
					<?php 
						$total_ob_amount 	+= $here['obligated_amount'];
						$total_rq_amount 	+= $here['requested_amount'];
						$total_db_amount 	+= $row['disbursed_amount'];
						$total_tax_amount 	+= $row['ewt_amount']+$row['vat_amount'];
					?>
					<?php endforeach;?>
					
					<?php endforeach;?>
				</tbody>
				<thead>
					<tr>
						<td>&nbsp</td>
						<td>&nbsp&nbspTOTAL</td>
						<td>&nbsp&nbsp<?php echo number_format($total_rq_amount);?></td>
						<td>&nbsp&nbsp<?php echo number_format($total_ob_amount);?></td>
						<td>&nbsp&nbsp<?php echo number_format($total_db_amount);?></td>
						<td>&nbsp&nbsp<?php echo number_format($total_tax_amount);?></td>
					</tr>
				</thead>
			</table>

			<?php elseif($fund == 2):?>
			<table cellpadding="0" cellspacing="0" type="button" class="table table-default table-layout-auto" id="tbl_cmd_fund">
				<thead class="teal lighten-5">
					<tr>
						<th width="40%">ACCOUNT CODE</th>
						<th width="40%">AMOUNT</th>
						<th width="15%">TAX AMOUNT</ht>
					</tr>
				</thead>
				<tbody class="rows">
					<?php ;foreach($details2 as $key => $row):?>
					<tr id="row_<?php echo $key;?>" class="row_<?php echo $key;?>">
						<td>
							<?php foreach($account_codes as $keys => $row2): ?>
							<?php echo ($row['account_code'] == $row2['account_code']) ? ''.$row2['account_code'].' - '.$row2['account_name'].'' : '';?>
							<?php endforeach;?>
							<input type="hidden" name="account_code[]" id="account_code_<?php echo $count;?>" class="account_code_<?php echo $key;?>" value="<?php echo $row['account_code'];?>">
						</td>
						<td>
							<?php echo number_format($row['base_tax_amount']);?>
							<input type="hidden" name="amount[]" id="amount_<?php echo $key;?>" class="amount_<?php echo $key;?>" value="<?php echo $row['base_tax_amount'];?>">
						</td>
						<td>
							<a href='javascript:void(0)' class='tooltip md-trigger tooltipped ppp tax_link_<?php echo $key;?>' id="tax_link_<?php echo $key;?>" data-count="<?php echo $key;?>" data-tooltip='Tax amount' data-position='bottom' data-delay='50' data-modal='modal_tax' onclick="modal_tax_init('<?php echo $security;?>/tax/cmdf/<?php echo $key;?>/<?php echo $payee;?>/view')">[ <?php echo number_format($row['ewt_amount']+$row['vat_amount']);?> ]</a>

							<input type="hidden" name="tax_amount[]" value="<?php echo number_format($row['ewt_amount']+$row['vat_amount']);?>" id="tax_amount_<?php echo $key;?>">
							<input type="hidden" name="base_tax_amount[]" value="<?php echo number_format($row['base_tax_amount']);?>" id="base_tax_amount_<?php echo $key;?>">
							<input type="hidden" name="ewt_rate[]" value="<?php echo floatval($row['ewt_rate']);?>" id="ewt_rate_<?php echo $key;?>">
							<input type="hidden" name="ewt_amount[]" value="<?php echo number_format($row['ewt_amount']);?>" id="ewt_amount_<?php echo $key;?>">
							<input type="hidden" name="vat_rate[]" value="<?php echo floatval($row['vat_rate']);?>" id="vat_rate_<?php echo $key;?>">
							<input type="hidden" name="vat_amount[]" value="<?php echo number_format($row['vat_amount']);?>" id="vat_amount_<?php echo $key;?>">
						</td>
					</tr>
					<?php endforeach;?>
				</tbody>
			</table>

			<?php elseif($fund == 3):?>

			<table style="width: 100%" cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="tbl_ciac_fund">
				<?php if($this->uri->segment(7) == 'void' || $this->uri->segment(7) == 'approval'):?>
				<thead class="teal lighten-5">
					<tr>
						<th>DEDUCTION</th>
						<th width="20%" class="right-align">AMOUNT</th>
					</tr>
				</thead>
				<tbody class="rows">
					<?php foreach($isca_details as $key => $row):?>
					<tr>
						<td><?php echo $row['deduction_name'];?></td>
						<td class="right-align"><?php echo number_format($row['amount']);?></td>
					</tr>
					<?php endforeach;?>
					<?php foreach($details3 as $key => $row):?>
					<tr>
						<td><?php echo $row['deduction'];?></td>
						<td class="right-align"><?php echo number_format($row['deduction_amount']);?></td>
					</tr>
					<?php endforeach;?>
				</tbody>

				<?php else:?>

				<thead class="teal lighten-5">
					<tr>
						<th>DEDUCTION</th>
						<th class="right-align">AMOUNT</th>
						<th>ACTION</th>
					</tr>
				</thead>
				<tbody class="rows">
					<tr>
						<td style="width: 50%">Due to absences</td>
						<td>1,000.00</td>
					</tr>
					<?php 
					if(sizeof($details3) != 0):
					foreach($details3 as $key => $row):
					?>
					<tr id="row_0" class="row_<?php echo $key;?>">
						<td>
							<select aria-required="true" name="deduction_type[]" id="deduction_type" required="" class="selectize if">
								<option value="">Deduction Type</option>
								<?php foreach ($deductions as $keys => $value) { ?>
									<option <?php echo ($row['deduction'] == $value['deduction_code']) ? 'selected="selected"' : '';?> value="<?php echo $value['deduction_code'];?>"><?php echo $value['deduction_name'];?></option>
								<?php } ?>
							</select>
						</td>
						<td class="right-align">
							<?php print_r($details3);?>
							<input type="text" 
							name="amount[]" 
							placeholder="Enter amount" 
							onkeyup="javascript:this.value=CommaCustom(this.value, <?php echo $key;?>);"
							autocomplete="off"
							aria-required="true" 
							required="" 
							value="<?php echo $row['deduction_amount'];?>"
							class="validate right-align amount_<?php echo $key;?>" 
							id="amount_<?php echo $key;?>">
						</td>
						<td>
							<div class="table-actions">
								<a style="cursor:pointer" data-count="<?php echo $key;?>" id="delete_row" data-identity="ciac_deduction" data-id="<?php echo $row['deduction_id'];?>" data-dv="<?php echo $row['disbursement_id'];?>" class="delete"></a>
							</div>
						</td>
					</tr>
					<?php 
					endforeach;
					else:
					?>
					<tr id="row_0" class="row_0">
						<td>
							<select aria-required="true" name="deduction_type[]"  id="deduction_type" required="" class="selectize if">
								<option value="">Deduction Type</option>
								<?php foreach ($deductions as $key => $value) { ?>
									<option value="<?php echo $value['deduction_code'];?>"><?php echo $value['deduction_name'];?></option>
								<?php } ?>
							</select>
						</td>
						<td class="right-align">
							<input type="text" 
							name="amount[]" 
							placeholder="Enter amount" 
							onkeyup="javascript:this.value=CommaCustom(this.value, 0);"
							autocomplete="off"
							aria-required="true" 
							required="" 
							class="validate right-align amount_0" 
							id="amount_<?php echo $count;?>">
						</td>
						<td>
							<div class="table-actions">
								<a style="cursor:pointer" data-count="0" id="delete_row" data-identifier="deduction" data-id="" class="delete"></a>
							</div>
						</td>
					</tr>
					<?php endif;?>
				</tbody>
				<?php endif;?>
			</table>

			<input type="hidden" name="count" id="count" class="count" value="<?php echo ($count == -0) ? '0' : $count -1;?>">

			<?php endif;?>			
			<?php if($this->uri->segment(7) == 'void'):?>
			<div class="row">
				<br><br>
				<b>REMARKS</b>
				<div class="input-field">
					<textarea placeholder="Enter Remarks" class="validate materialize-textarea if" required="" aria-required="true" name="remarks" id="remarks"><?php echo $remarks;?></textarea>
				</div>
			</div>
			<?php endif;?>
		</div>
	</div>
	
	<?php else:?>
	<div class="row" style="padding: 20px;">
		<div class="col s12">
			
			<table border="0" width="100%">
				<tr>
					<td width="35%">
						<b>DV NO.</b><br>
						<?php echo (!empty($dv_no)) ? $dv_no : '(Upon Proccessing)';?>
					</td>
					<td style="padding-left: 10%">
						<b>DV DATE.</b><br>
						<?php echo (!empty($dv_date)) ? $dv_date : '(Upon Proccessing)';?>
					</td>
				</tr>
				<tr>
					<td>
						<b>FUND</b>
						<div class="input-field">
							<select width="100%" aria-required="true" <?php echo ($this->uri->segment(7) == 'void' || $this->uri->segment(7) == 'override') ? 'disabled="disabled"' : '' ;?> name="funds" id="funds" class="selectize validate dropdown" data-identity="fund" required="">
								<option value="">Select fund section</option>
								<?php foreach($funds as $key => $val):?>
								<option value="<?php echo $val['disbursement_fund_id'];?>"><?php echo $val['fund_name'];?> Fund</option>
								<?php endforeach;?>
							</select>
						</div
					</td>
				</tr>
			</table>

			<div class="table-contents">
				
			</div>
			
		</div>
	</div>
	<?php endif;?>

	<div class="md-footer default buts">
	    <!--<button class="btn btn-secondary waves-effect waves-light" id="print" value="<?php echo BTN_PRINT ?>"><?php echo BTN_PRINT ?></button>-->
	    <?php if($current_status != STATUS_VOIDED):?>
		    <?php if($this->uri->segment(7) == 'override'):?>
		    <button class="btn btn-primary waves-effect waves-light" id="save" value="<?php echo BTN_OVERRIDE ?>"><?php echo BTN_OVERRIDE ?></button>

		    <?php elseif($this->uri->segment(7) == 'void'):?>
		    <button class="btn btn-primary waves-effect waves-light" id="save" value="<?php echo BTN_VOID ?>"><?php echo BTN_VOID ?></button>

			<?php elseif($this->uri->segment(7) == 'approval'):?>

				<?php if($current_status != STATUS_DISAPPROVED):?>

				    <button type="button" class="btn btn-primary waves-effect waves-light" id="button" data-status="<?php echo STATUS_APPROVED;?>" value="<?php echo BTN_APPROVED ?>"><?php echo BTN_APPROVED ?></button>
				    <button type="button" class="btn btn-secondary waves-effect waves-light" id="button" data-status="<?php echo STATUS_DISAPPROVED;?>" value="<?php echo BTN_DISAPRROVED ?>"><?php echo BTN_DISAPRROVED ?></button>

				<?php endif;?>

		    <?php else:?>
		    <button class="btn btn-primary waves-effect waves-light" id="save" value="<?php echo BTN_POST ?>"><?php echo BTN_POST ?></button>

			<?php endif;?>
		<?php endif;?>
		<a class="waves-effect waves-teal btn-flat cancel_modal" id="cancel">Cancel</a>
	</div>
</form>

<script>
$(function(){
	<?php if($this->uri->segment(7) == 'override'):?>
		selectize_init();
		$("#funds")[0].selectize.setValue(<?php echo $fund;?>);
	<?php endif;?>
		
	$('#form').parsley();

	<?php if(ISSET($data)){ ?>
	$('.input-field label').addClass('active');
	<?php } ?>
});
</script>
