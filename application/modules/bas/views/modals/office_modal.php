<form id="office_form">
	<input type="hidden" name="security" value="<?php echo $security;?>">
	<input type="hidden" name="line_item_id" value="<?php echo $line_item_id;?>">
	<input type="hidden" name="validation" value="office">
	<div class="card-panel form-basic">
		<div class="row p-n m-n p-n-n">
			<div class="col s6">
				<b><?php echo $year;?></b>
				<br>
				Year
				<br><br>

				<b><?php echo $source;?></b>
				<br>
				Source
				<br><br>

				<b><?php echo $code;?> <?php echo $line_item_name;?></b>
				<br>
				Line Item<br><br>
			</div>
			<div class="col s6 p-r-n right-align">
				<div class="input-field inline p-l-md">
					<button type="button" class="btn waves-effect waves-light btn-success" data-table="office" id="add_row" style="margin-top: 100px">Add Office</button>
		  		</div>
			</div>
		</div>
		<div class="row m-t-lg">
			<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="office_table">
				<thead class="cyan lighten-5">
					<th>Office</th>
					<th>Appropriation</th>
					<th>Allotment</th>
					<th class="center-align">Action</th>
				</thead>
				<tbody>
					<?php 
					if(!empty($offices)):
					$key = 0;
					foreach($offices as $key => $row): 
					$key ++;
					?>
					<tr id="row_<?php echo $row['office_num'];?>">
						<td width="30%">
							<input type="hidden" name="old_office[]" id="old_office" value="<?php echo $row['office_num'];?>">
							<select class="selectize validate" name="update_office[]" id="office_code" required="" aria-required="true">
								<?php foreach($line_item_office as $key => $row2):?>
								<option <?php if($row2['office_num'] == $row['office_num']) echo 'selected';?> value="<?php echo $row2['office_num'];?>"><?php echo $row2['office_name'];?></option>
								<?php endforeach;?>
							</select>
						</td>
						<td width="30%">
							<input class="right-align validate" name="update_appropriation_amount[]" required="" aria-required="true"
							value="<?php echo number_format(floatval($row['mooe_appropriation']));?>"
							type="text" autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);"
							placeholder="Enter amount" />
						</td>
						<td width="30%">
							<input class="right-align validate" name="update_allotment_amount[]" required="" aria-required="true"
							value="<?php echo number_format(floatval($row['mooe_allotment']));?>"
							type="text" autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);"
							placeholder="Enter amount" />
						</td>
						<td>
							<div class="table-actions">
								<a style="cursor:pointer" data-count="<?php echo floatval($row['office_num']);?>" data-identifier="office" data-id="<?php echo $row['office_num'];?>" data-item="<?php echo $line_item_id;?>" class="delete" id="delete_row">
								</a>
							</div>
						</td>
					</tr>
					<?php
					endforeach;
					else:
					?>
					<tr id="row_1">
						<td width="30%">
							<select class="selectize validate" name="office[]" id="office_code" required="" aria-required="true">
								<option value="">Select Office</option>
								<?php foreach($line_item_office as $key => $row2):?>
								<option value="<?php echo $row2['office_num'];?>"><?php echo $row2['office_name'];?></option>
								<?php endforeach;?>
							</select>
						</td>
						<td width="30%">
							<input class="right-align validate" name="appropriation_amount[]" required="" aria-required="true"
							type="text" autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);"
							placeholder="Enter amount" />
						</td>
						<td width="30%">
							<input class="right-align validate" name="allotment_amount[]" required="" aria-required="true"
							type="text" autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
							placeholder="Enter amount" />
						</td>
						<td>
							<div class="table-actions">
								<a style="cursor:pointer" data-count="0" id="delete_row" data-identifier="" data-id="deny" class="delete"></a>
							</div>
						</td>
					</tr>
					<?php
					endif;
					?>
				</tbody>
			</table>
			<input type="hidden" name="count_all" id="count_all" value="1">
		</div>
	</div>

	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
		<button class="btn waves-effect waves-light btn-primary" id="save_office" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	</div>
</form>
<script type="text/javascript">
	$(document).ready(function(){
		$('#office_form').parsley();
	})
</script>