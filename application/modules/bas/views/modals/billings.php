<?php if($this->uri->segment(7) == 'view'):?>
<div class="row">
	<div class="col s12">
		<table>
			<tr>
				<td>
					<div><span>BILL NO</span></div>
					<div><span><?php echo $bill_no;?></span></div>
				</td>
			</tr>
			<tr>
				<td>
					<div><span>BILL DATE</span></div>
					<div><span><?php echo $bill_date;?></span></div>
				</td>
				<td>
					<div><span>DUE DATE</span></div>
					<div><span><?php echo $due_date;?></span></div>
				</td>
			</tr>
			<tr>
				<td>
					<div><span>OBLIGATION SLIP NO.</span></div>
					<div><span><?php echo $os_no;?></span></div>
				</td>
				<td>
					<div><span>AMOUNT DUE</span></div>
					<div><span><?php echo $amount_due;?></span></div>
				</td>
			</tr>
		</table>
	</div>
</div>
<?php else: ?>
<form id="form">
	<input type="hidden" name="security" value="<?php echo $security; ?>">
	<div class="form-float-label">

		<div class="row">
			<div class="col s12">
				<div class="input-field">
				    <input type="text" class="datepicker" required="" aria-required="true" name="bill_date" id="bill_date" value="<?php echo $bill_date;?>"/>
				    <label for="bill_date">BILLING DATE</label>
			     </div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
				    <input type="text" class="datepicker validate" required="" aria-required="true" name="due_date" id="due_date" value="<?php echo $due_date;?>"/>
				    <label for="due_date">DUE DATE</label>
			     </div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<input type="text" class="validate" required="" aria-required="true" name="bill_no" id="bill_no" value="<?php echo $bill_no;?>">
					<label for="bill_no">BILL NO.</label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<input type="text" class="validate" required="" aria-required="true" name="amount_due" id="amount_due" value="<?php echo ($amount_due != 0.00) ? number_format($amount_due) : '' ;?>"
					autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);"
					>
					<label for="amount_due">AMOUNT DUE.</label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<select name="os_no" id="os_no" class="selectize validate" required="" aria-required="true">
						<option value="">Select Oblgation Slip</option>
						<?php foreach($slips as $key => $val): ?>
						<option <?php echo ($val['obligation_id'] == $obligation_id) ? 'selected="selected"' : '';?> value="<?php echo $val['obligation_id'];?>"><?php echo $val['os_no'];?></option>
						<?php endforeach; ?>
					</select> 
					<label for="os_no" class="active block">OBLIGATION SLIP NO.</label>
				</div>
			</div>
		</div>

	</div>

	<div class="md-footer default">
	    <button class="btn waves-effect waves-light" id="save" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<a class="waves-effect waves-teal btn-flat cancel_modal">Cancel</a>
	</div>
</form>
<?php endif;?>

<script>
$(function(){

	$('#cancel').click(function(){
		$('.cancel_modal').trigger('click');
	});
  
  $('#form').parsley();
  $('#form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {
		var data = $(this).serialize();
		$.post("<?php echo base_url() . PROJECT_BAS ?>/bas_billings/check_exceed_amount_due",data,function(response){

			if(response.trim() == '')
			{
				button_loader('save', 1);
				  $.post("<?php echo base_url() . PROJECT_BAS ?>/bas_billings/process/", data, function(result) {
					if(result.flag == 0){
					  notification_msg("<?php echo ERROR ?>", result.msg);
					  button_loader('save', 0);
					} else {
					  notification_msg("<?php echo SUCCESS ?>", result.msg);
					  button_loader("save",0);
					  $('.cancel_modal').trigger('click');
					  
					  load_datatable('tbl_billings', '<?php echo PROJECT_BAS ?>/bas_billings/get_billings_list/');
					}
				  }, 'json');  
			}
			else notification_msg('ERROR',response);

		});
    }
  });
  
  <?php if(ISSET($data)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>
})
</script>