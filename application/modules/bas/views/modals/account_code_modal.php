<div class="tabs-wrapper m-n p-n">
	<div style="width:400px; margin: 0px;padding: 0px">
		<ul class="tabs" style="border-bottom: 1px solid #eee;padding: 0px;margin: 0px">
			<li data-tab="ps" data-id="<?php echo $line_item_id;?>" class="tab col s3 gettab" style="border-bottom: 1px solid #eee;border-right: 1px solid #eee;"><a href="#">PS</a></li>
			<li data-tab="mooe" data-id="<?php echo $line_item_id;?>" class="tab col s3 gettab" style="border-bottom: 1px solid #eee;border-right: 1px solid #eee;"><a href="#">MOOE</a></li>
			<li data-tab="co" data-id="<?php echo $line_item_id;?>" class="tab col s3 gettab" style="border-bottom: 1px solid #eee;border-right: 1px solid #eee;"><a href="#">CO</a></li>
		</ul>
	</div>
</div>
<form id="account_code_form">
	<input type="hidden" name="security" value="<?php echo $security;?>">
	<input type="hidden" name="line_item_id" value="<?php echo $line_item_id;?>">
	<input type="hidden" name="validation" value="account_code">
	<div class="row">
		<div class="col s6 p-t-lg">
			<b><?php echo $year;?></b>
			<br>
			Year
			<br><br>

			<b><?php echo $source;?></b>
			<br>
			Source
			<br><br>

			<b><?php echo $code;?> <?php echo $line_item_name;?></b>
			<br>
			Line Item<br><br>
		</div>
		<div class="col s6 right-align">
			<div class="input-field inline p-l-md">
				<button type="button" class="btn waves-effect waves-light btn-success" id="add_row" data-table="account_code" style="margin-top: 110px">Add Account Code</button>
	  		</div>
		</div>
	</div>
	<div id="tab_content" class="p-n m-n p-n-n">
		<input type="hidden" name="uri" value="ps">
		<div class="panel p-n m-n form-basic">
			<div class="row">
				<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="account_code_table">
					<thead class="cyan lighten-5">
						<tr>
							<td width="40%">ACCOUNT CODE</td>
							<td width="27%">APPROPRIATION</td>
							<td>ALLOTMENT</td>
							<td class="center-align" width="5%">Action</td>
						</tr>
					</thead>
					<tbody>
						<?php 
						if(!empty($line_item_codes)):
						foreach($line_item_codes as $key => $row): 
						?>
						<tr id="row_<?php echo $row['account_code'];?>">
							<td>
								<input type="hidden" name="old_account_code[]" id="old_account_code" value="<?php echo $row['account_code'];?>">
								<select class="selectize validate" required="" aria-required="true" 
								name="update_account_code[]" id="account_code">
									<?php foreach($account_codes as $keys => $row2): ?>
									<option <?php if($row2['account_code'] == $row['account_code']) echo 'selected="selected"';?> value="<?php echo $row2['account_code'];?>" ><?php echo $row2['account_code'].'&nbsp; - &nbsp;'.$row2['account_name'];?>
									</option>
									<?php endforeach;?>
								</select>
							</td>
							<td width="25%">
								<input name="update_appropriation_amount[]" class="right-align validate" 
								required="" aria-required="true" type="text" autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
								value="<?php echo number_format(floatval($row['appropriation_amount']));?>"
								placeholder="Enter appropriation amount" />
							</td>
							<td width="25%">
								<input name="update_allotment_amount[]" class="right-align validate" 
								required="" aria-required="true" type="text" onkeyup="javascript:this.value=Comma(this.value);" 
								value="<?php echo number_format(floatval($row['allotment_amount']));?>" 
								placeholder="Enter allotment amount" />
							</td>
							<td>
								<div class="table-actions">
									<a style="cursor:pointer" data-count="<?php echo $row['account_code'];?>" id="delete_row" data-identifier="account_code" data-id="<?php echo $row['account_code'];?>" class="delete"></a>
								</div>
							</td>
						</tr>
						<?php
						endforeach;
						else:
						?>
						<tr id="row_1">
							<td width="40%">
								<select class="selectize validate" required="" aria-required="true"
								name="account_code[]" id="account_code">
									<option value="">Select account code</option>
									<?php foreach($account_codes as $key => $row):?>
									<option value="<?php echo $row['account_code'];?>"><?php echo $row['account_code'];?> - <?php echo $row['account_name'];?></option>
									<?php endforeach;?>
								</select>
							</td>
							<td width="25%">
								<input name="appropriation_amount[]" class="right-align validate" 
								required="" aria-required="true" type="text" onkeyup="javascript:this.value=Comma(this.value);"  
								placeholder="Enter appropriation amount" />
							</td>
							<td width="25%">
								<input name="allotment_amount[]" class="right-align validate" 
								required="" aria-required="true" type="text" onkeyup="javascript:this.value=Comma(this.value);"  
								placeholder="Enter allotment amount" />
							</td>
							<td>
								<div class="table-actions">
									<a style="cursor:pointer" data-count="denied" id="delete_row" data-identifier="" class="delete"></a>
								</div>
							</td>
						</tr>
						<?php endif;?>
					</tbody>
				</table>
				<input type="hidden" name="count_all" id="count_all" value="1">
			</div>
		</div>
		
	</div>
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal">Cancel</a>
		<button type="submit" class="btn waves-effect waves-light btn-primary" id="save_account_code" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	</div>
</form>

<script type="text/javascript">
	$(document).ready(function(){
		$('#account_code_form').parsley();
	})
</script>