<?php if($this->uri->segment(7) == 'view'): ?>

<div class="row" style="padding: 20px;">
	<div class="col s12">
		<table width="100%">
			<tr>
				<td>
					<div><b><span><?php echo $release_date;?></span></b></div>
					<div><span>Released Date</span></div>
					<hr>
				</td>
				<td>
					<div><b><span><?php echo $reference_no;?></span></b></div>
					<div><span>Release No.</span></div>
					<hr>
				</td>
			</tr>
			<tr>
				<td>
					<div><b><span><?php echo $year;?></span></b></div>
					<div><span>Year</span></div>
					<hr>
				</td>
				<td>
					<div>
						<b>
							<span>
								<?php foreach($fund_sources as $key => $val): ?>
								<?php if($val['source_type_num'] == $source_type_num) echo strtoupper($val['source_type_name']);?>
								<?php endforeach;?>
							</span>
						</b>
					</div>
					<div><span>Fund Source</span></div>
					<hr>
				</td>
			</tr>
			<tr>
				<td>
					<div>
						<b>
							<span>
								<?php foreach($line_items as $key => $val): ?>
								<?php if($val['line_item_id'] == $source_line_item_id) echo $val['line_item_name'];?>
								<?php endforeach;?>
							</span>
						</b>
					</div>
					<div><span>Line Item Source</span></div>
					<hr>
				</td>
				<td>
					<div>
						<b>
							<span>
								<?php foreach($line_items as $key => $val): ?>
								<?php if($val['line_item_id'] == $recipient_line_item_id) echo $val['line_item_name'];?>
								<?php endforeach;?>
							</span>
						</b>
					</div>
					<div><span>Line Item Recipient</span></div>
					<hr>
				</td>
			</tr>
			<tr>
				<td>
					<div>
						<b>
							<span>
								<?php foreach($account_code_list as $key => $val): ?>
								<?php if($val['account_code'] == $source_account_code) echo strtoupper($val['account_name']);?>
								<?php endforeach;?>
							</span>
						</b>
					</div>
					<div><span>Account Code Source</span></div>
					<hr>
				</td>
				<td>
					<div>
						<b>
							<span>
								<?php foreach($account_code_list as $key => $val): ?>
								<?php if($val['account_code'] == $recipient_account_code) echo strtoupper($val['account_name']);?>
								<?php endforeach;?>
							</span>
						</b>
					</div>
					<div><span>Account Code Recipient</span></div>
					<hr>
				</td>
			</tr>

			<tr>
				<td colspan="2">
					<div><b><span><?php echo number_format($amount);?></span></b></div>
					<div><span>Realigned Amount</span></div>
					<hr>
				</td>
			</tr>
		</table>
	</div>
</div>

<?php else: ?>
<form id="form">
	<input type="hidden" name="security" value="<?php echo $security;?>">
	<input type="hidden" name="to_update" value="<?php echo $to_update;?>">
	<input type="hidden" name="status" id="status" value="">
	<div class="form-float-label">
		<div class="row">
			<div class="col s6">
			  <div class="input-field">
			  	<?php
			  	$date = new DateTime($release_date);
				$newdate = $date->format('Y/m/d'); // 2012-07-31
			  	?>
			    <input type="text" class="datepicker validate" required="" name="release_date" id="release_date" value="<?php if(!empty($release_date)) echo $newdate;?>" autocomplete="off"/>
			    <label for="date_release" class="<?php echo $active;?>">Released Date</label>
		      </div>
		    </div>
		    <div class="col s6">
			  <div class="input-field">
			    <input type="text" class="validate" required="" name="reference_no" id="reference_no" value="<?php echo $reference_no;?>" autocomplete="off"/>
			    <label for="release_number" class="<?php echo $active;?>">Release Number</label>
		      </div>
		    </div>
		</div>
		<div class="row m-n">
			<div class="col s6 p-t-md">
			  <div class="input-field">
			  	<label for="source_year" class="active block">YEAR</label>
			    <select name="source_year" class="selectize getFundSources validate" aria-required="true" required="" data-target="source_type_num" id="source_year" placeholder="Please Select">
					<option value="" disabled selected>Choose Year</option>
					<?php foreach($fund_year as $key => $val):?>
					<option value="<?php echo $val['year'];?>"><?php echo $val['year'];?></option>
					<?php endforeach;?>
				</select>
		      </div>
		    </div>
			<div class="col s6 p-t-md">
			  <div class="input-field">
			  	<label for="source_type_num" class="active block">Fund Source</label>
			    <select name="source_type_num" data-selected="<?php echo $source_type_num;?>" class="selectize getLineItemsByFundSources validate" aria-required="true" required="" data-target1="source_line_item_id" data-target2="recipient_line_item_id" id="source_type_num" placeholder="Please Select">
					<option value="" disabled selected>Choose source</option>
				</select>
		      </div>
		    </div>
		</div>
	</div>
	<div class="panel form-basic">
		<div class="row">
			<table cellpadding="0" cellspacing="0" class="table table-layout-auto" id="realignment_form_table">
				<thead class="teal lighten-5">
					<tr>
						<td width="20%"></td>
						<td width="33%">Source</td>
						<td width="33%">Recipient</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<h6>Line Item</h6>
							<input type="hidden" id="line_item_id_trigger" value="<?php echo $source_line_item_id;?>">
							<input type="hidden" id="line_item_id_trigger2" value="<?php echo $recipient_line_item_id;?>">
							<input type="hidden" name="realigned_amount_1" class="target_amount_1">
							<input type="hidden" name="realigned_amount_2" class="target_amount_2">
						</td>
						<td>
							<select name="source_line_item_id" id="source_line_item_id" data-selected="<?php echo $source_line_item_id;?>" data-target="source_account_code" class="selectize getAccountCodes validate" aria-required="true" required="">
								<option value="">Choose Source</option>
							</select>
						</td>
						<td>
							<select name="recipient_line_item_id" id="recipient_line_item_id" class="selectize getAccountCodes validate" aria-required="true" required="" data-selected="<?php echo $recipient_line_item_id;?>" data-target="recipient_account_code" style="width: 100%">
								<option value="">Choose Recipient</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<h6>Account Code</h6>
						</td>
						<td>
							<select name="source_account_code" id="source_account_code" data-selected="<?php echo $source_account_code;?>" data-target="target_amount_1" class="selectize getAccountCodesCurrentAmount validate" aria-required="true" required="">
								<option value="">Choose Source</option>
							</select>
						</td>
						<td>
							<select name="recipient_account_code" id="recipient_account_code" data-selected="<?php echo $recipient_account_code;?>" data-target="target_amount_2" class="selectize getAccountCodesCurrentAmount validate" aria-required="true" required="">
								<option value="">Choose Recipient</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>
							<h6>Current Amount</h6>
						</td>
						<td class="right-align">
							<code id="target_amount_1">0.00</code>
						</td>
						<td class="right-align">
							<code id="target_amount_2">0.00</code>
						</td>
					</tr>
					<tr>
						<td>
							<h6>Amount to be re-aligned</h6>
						</td>
						<td class="form-basic">
							<input type="text" name="realigned_amount" 
							class="right-align validate" onkeyup="javascript:this.value=Comma(this.value);"
							required="" autocomplete="off"
							placeholder="Enter Amount" value="<?php echo (!empty($amount)) ? number_format($amount) : '';?>">
						</td>
					</tr>
				</tbody>

			</table>
		</div>
	</div>
	<div class="md-footer default">
		<button type="button" class="btn btn-scondary waves-effect waves-light" data-status="1" id="save" value="<?php echo BTN_DRAFT ?>"><?php echo BTN_DRAFT ?></button>
		<button type="button" class="btn btn-primary waves-effect waves-light" data-status="0" id="save" value="<?php echo BTN_POST ?>"><?php echo BTN_POST ?></button>
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	</div>
</form>
<?php endif;?>

<script type="text/javascript">
	$('#form').parsley();
</script>