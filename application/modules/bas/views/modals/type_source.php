<?php
$id = $data['source_type_num'];
$salt = gen_salt();
$token = in_salt($id, $salt);
?>
<?php if($this->uri->segment(7) == 'view'):?>
	<div class="row" style="padding: 20px;">
		<div class="col s10">
			<div><b><span><?=ucfirst($data['source_type_name']);?></span></b></div>
			<div><span>Payee Name</span></div>
			<hr>
			<br>

			<div><b><span><?php if($data['active_flag'] == 1) echo 'Active'; else echo 'Not Active';?></span></b></div>
			<div><span>Status</span></div>
			<br>
		</div>
	</div>
	<div class="md-footer default">
	  	<a class="btn waves-effect waves-light" id="cancel">Close</a>
	</div>
<?php else: ?>
<form id="form">
	<input type="hidden" name="id" value="<?php echo $data['source_type_num'] ?>">
	<input type="hidden" name="salt" value="<?php echo $salt ?>">
	<input type="hidden" name="token" value="<?php echo $token ?>">
	<div class="form-float-label">

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<input type="text" class="validate" required="" aria-required="true" name="source" id="source" value="<?=$data['source_type_name']?>">
					<label for="payee">Type of Source</label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<label for="status" class="active block">STATUS</label>
					<select required="" aria-required="true" name="status" id="status" class="selectize">
						<option value=""> -- </option>
						<option selected="selected" value="1">ACTIVE</option>
						<option value="0">NOT ACTIVE</option>
					</select>
				</div>
			</div>
		</div>

	</div>

	<div class="md-footer default">
	    <button class="btn waves-effect waves-light" id="save" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<a class="waves-effect waves-teal btn-flat" id="cancel">Cancel</a>
	</div>
</form>
<?php endif;?>

<script>
$(function(){
  	$("#cancel").on("click", function(){
		modalObj.closeModal();
	});
  
  $('#form').parsley();

  $('#form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {
	  	var data = $(this).serialize();
	  
	  	$.post("<?php echo base_url() . PROJECT_BAS ?>/bas_type_source/check_exists_source/",{'name':$('#source').val(), 'id': $('input[name="id"]').val() },function(response){
	  		if(response.status == '0')
	  		{
	  			button_loader('save', 1);
				$.post("<?php echo base_url() . PROJECT_BAS ?>/bas_type_source/process/", data, function(result) {
						if(result.flag == 0){
						  notification_msg("<?php echo ERROR ?>", result.msg);
						  button_loader('save', 0);
						} else {
						  notification_msg("<?php echo SUCCESS ?>", result.msg);
						  button_loader("save",0);
						  modalObj.closeModal();
						  
						  load_datatable('tbl_source', '<?php echo PROJECT_BAS ?>/bas_type_source/get_source_list/');
						}
				}, 'json');  
	  		}else notification_msg('ERROR', 'Type of source name must be unique.');
		},'json');     
    }

  });
  
  <?php if(ISSET($data)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>
})
</script>
</script>