<?php if($this->uri->segment(7) == 'view'):?>
	<div class="row" style="padding: 20px;">
		<div class="col s10">
			<div><b><span><?php echo $account_code;?></span></b></div>
			<div><span>Account Code</span></div>
			<hr>
			<br>

			<div><b><span><?php echo ucwords($account_name);?></span></b></div>
			<div><span>Account Title</span></div>
			<hr>
			<br>

			<div><b><span><?php if($parent_account_code == NULL || $parent_account_code == '') echo 'No Parent'; else echo $parent_account_code;?></span></b></div>
			<div><span>Parent Code</span></div>
			<hr>
			<br>
	
			<div><b><span><?php if($data['active_flag'] == 1) echo 'Active'; else echo 'Not Active';?></span></b></div>
			<div><span>Status</span></div>
			<br>
		</div>
	</div>
	<div class="md-footer default">
	  	<a class="btn waves-effect waves-light cancel_modal" id="cancel">Close</a>
	</div>
<?php else: ?>
<form id="form">
	<input type="hidden" name="security" value="<?php echo $security; ?>">
	<input type="hidden" name="trigger" id="trigger" value="<?php echo $trigger; ?>">
	<input type="hidden" name="old_code" id="old_code" value="<?php echo $account_code; ?>">
	<div class="form-float-label">

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<input type="text" class="validate" required="" aria-required="true" name="code" id="code" value="<?php echo $account_code;?>" autocomplete="off">
					<label for="payee">CODE</label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<input type="text" class="validate" required="" aria-required="true" name="title" id="title" value="<?php echo $account_name;?>" autocomplete="off">
					<label for="payee">ACCOUNT TITLE</label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<label for="status" class="active">PARENT CODE</label>
					<select aria-required="false" name="parent_code" id="parent_code" class="selectize" autocomplete="off">
						<option value=""> - </option>
						<?php
						foreach($charts as $row) 
						{
							if(!empty($account_code)){
								if($row['parent_account_code'] == $account_code) continue;
								echo '<option value="'.$row['account_code'].'">'.$row['account_code'].' - '.$row['account_name'].'</option>';
							}else echo '<option value="'.$row['account_code'].'">'.$row['account_code'].' - '.$row['account_name'].'</option>';
							
						
						} 
						?>
						</select>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<label for="status" class="active">STATUS</label>
					<select required="" aria-required="true" name="status" id="status" class="selectize" autocomplete="off">
						<option value=""> -- </option>
						<option selected="selected" value="1">ACTIVE</option>
						<option value="0">NOT ACTIVE</option>
					</select>
				</div>
			</div>
		</div>

	</div>

	<div class="md-footer default">
	    <button class="btn waves-effect waves-light" id="save" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<a class="waves-effect waves-teal btn-flat cancel_modal">Cancel</a>
	</div>
</form>
<?php endif;?>

<script>
$(function(){
  	$("#cancel").on("click", function(){
		modalObj.closeModal();
	});
  
  $('#form').parsley();
  $('#form').submit(function(e) {
    e.preventDefault();

	if ( $(this).parsley().isValid() ) {

	  var data = $(this).serialize();
	  var url= $base_url+'bas/bas_chart_accounts/check_exist_account_codes/'+$('#code').val()+'/'+$('#trigger').val();
	  if($('#trigger').val() == 'update'){
	  	url = $base_url+'bas/bas_chart_accounts/check_exist_account_codes/'+$('#code').val()+'/'+$('#trigger').val()+'/'+$('#old_code').val();
	  }

	  $.get(url,function(response){
	  	if(response.count == '1')
	  	{
	  		notification_msg('ERROR','<b>Account code exists.</b> Please use different account code.');
	  	}
	  	else
	  	{
	  		button_loader('save', 1);
			$.post("<?php echo base_url() . PROJECT_BAS ?>/bas_chart_accounts/process/", data, function(result) {
				if(result.flag == 0){
				  notification_msg("<?php echo ERROR ?>", result.msg);
				  button_loader('save', 0);
				} else {
				  notification_msg("<?php echo SUCCESS ?>", result.msg);
				  button_loader("save",0);
				  modalObj.closeModal();
				  
				  load_datatable('tbl_chart', '<?php echo PROJECT_BAS ?>/bas_chart_accounts/get_chart_list/');
				}
			}, 'json');      
	  	}
	  },'json');
    }
  });
  
  <?php if(ISSET($data)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>
})
</script>