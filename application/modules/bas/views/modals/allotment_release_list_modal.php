<form id="director_form">
	<div class="panel p-md">
		<div class="row">
			<div class="col s12 p-r-n right-align">
				<div class="input-field inline p-l-md">
					<button type="button" class="btn waves-effect waves-light btn-success md-trigger" data-modal="modal_allotment_release" onclick="modal_allotment_release_init('?id=<?php echo $line_item_id;?>')" >Add Allotment Release</button>
		  		</div>
			</div>
		</div>

		<div class="row">

			<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="allotment_release_list_table" >
				<thead>
					<tr>
						<th>Release Date</th>
						<th>Release Number</th>
						<th>Release Amount</th>
						<th class="center-align">Action</th>
					</tr>
				</thead>
				<tbody></tbody>
			</table>
		
		</div>

	</div>
	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	</div>
	
</form>

<script type="text/javascript">
 var deleteObj = new handleData({
	 	controller : 'bas_line_items', 
	 	method : 'delete_data/release/', 
	 	module: '<?php echo PROJECT_BAS ?>'
	 });
</script>
	



