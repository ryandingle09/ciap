<?php if($this->uri->segment(7) == 'view'):?>
	<div class="row" style="padding: 20px;">
		<div class="col s10">
			<div><b><span><?php echo date("M j, Y", strtotime($reference_date));?></span></b></div>
			<div><span>Reference Date</span></div>
			<hr>
			<br>

			<div><b><span><?php echo ucwords($reference_no);?></span></b></div>
			<div><span>Reference No.</span></div>
			<hr>
			<br>

			<div><b><span>Alloment Release Details</span></b></div>
			<br>
			<div>
			<?php foreach($details as $key => $row): ?>
				<?php foreach($all_account_codes as $key2 => $row2):?>
		  			 
		  			<?php if($row2['account_code'] == $row['account_code']):?>
		  			<div>
		  			<span>
		  			Account Code :
		  			<?php echo '<b>'.$row2['account_name'].'</b>';?>
		  			</span>
			  		</div>
			  		<div>
			  			<span>
			  			Alloment Amount : 
			  			<b><?php echo number_format($row['allotment_amount']);?></b>
			  			<br>
			  			</span>
			  		</div>
			  		<hr>
		  		<?php endif;?>
		  		<?php endforeach;?>
		  	<?php endforeach;?>
			</div>
			<hr>
			<br>
		</div>
	</div>
<?php else :?>
<form id="release_form">
	<input type="hidden" name="security" value="<?php echo $security;?>">
	<input type="hidden" name="validation" value="allotment_release">
	<div class="card-panel p-l-n">
		<div class="row">
			<div class="col s12">	
				<b><?php echo $year;?></b>
				<br>
				Year
				<br><br>

				<b><?php echo $source;?></b>
				<br>
				Source
				<br><br>

				<b><?php echo $code;?> <?php echo $line_item_name;?></b>
				<br>
				Line Item<br><br>
				<input type="hidden" name="line_item_id" value="<?php echo $line_item_id;?>">
			</div>	
		</div>
	</div>

	<div>
		<div class="row">
			<div class="col s6">
			  	<div class="input-field">
				  	<input type="hidden" name="security" value="<?php echo $security;?>">
				  	<input type="hidden" name="hash_id" value="<?php echo $hash_id;?>">
				  	<input type="hidden" name="id" value="<?php echo $release_id;?>">
				  	<?php 
				  	if(isset($reference_date)):
				  	$date = new DateTime($reference_date);
					$newdate = $date->format('Y/m/d');
					endif;
				  	?>
				    <input type="text" class="datepicker validate" required="" name="release_date" id="release_date" value="<?php echo $newdate;?>"/>
				    <label for="date_release" class="<?php if(isset($active)) echo 'active';?>">Date of Release</label>
		      	</div>
		    </div>

			<div class="col s6">
			  	<div class="input-field">
				    <input type="text" class="validate" required="" name="release_number" id="release_number" value="<?php echo $reference_no;?>"/>
				    <label for="release_number" class="<?php if(isset($active)) echo 'active';?>">Release Number</label>
			    </div>
		    </div>
		</div>
	</div>

	<div class="panel p-md p-t-n m-t-n">
		<div class="row p-t-n m-t-n">
			<div class="col s12 p-r-n m-t-n p-t-n right-align">
				<div class="input-field inline">
					<button type="button" class="btn waves-effect waves-light btn-success" id="add_row" data-table="release">Add Account Code</button>
		  		</div>
			</div>
		</div>

		<hr> 

		<div class="row form-basic">
			<table cellpadding="0" width="100%" cellspacing="0" class="table table-default table-layout-auto" id="allotment_release_table">
				<thead>
					<tr>
			  			<td width="70%">Account Code</td>
			  			<td width="40%">Allotment</th>
			  			<td class="center-align">Action</td>
			  		</tr>
		  		</thead>
		  		<tbody>
		  			<?php 
		  			if(!empty($details)): 
		  			foreach($details as $key => $row):
		  			?>
		  			<tr id="row_<?php echo $row['account_code'];?>">
		  				<td>
		  					<input type="hidden" name="update_code[]" value="<?php echo $row['account_code'];?>">
		  					<select class="selectize validate" required="" aria-required="true" 
							name="update_account_code[]" id="account_code">
								<option value="">Select account code</option>
								<?php foreach($all_account_codes as $key => $row2):?>
								<option <?php if($row2['account_code'] == $row['account_code']) echo 'selected';?> value="<?php echo $row2['account_code'];?>"><?php echo $row2['account_name'];?></option>
								<?php endforeach;?>
							</select>
						</td>
						<td width="30%">
							<input class="right-align validate" placeholder="Enter allotment amount" name="update_allotment_amount[]"
							required="" aria-required="true" type="text" autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);"
							placeholder="Enter allotment amount" value="<?php echo number_format(floatval($row['allotment_amount']));?>"/>
						</td>
						<td class="center-align">
							<div class="table-actions">
								<a style="cursor:pointer" data-id="<?php echo $row['account_code'];?>" data-count="<?php echo $row['release_id'];?>" id="delete_row" data-identifier="release_details" class="delete"></a>
							</div>
						</td>
		  			</tr>
		  			<?php
		  			endforeach;
		  			else:
		  			?>
		  			<tr id="row_1">
		  				<td>
		  					<select class="selectize validate" required="" aria-required="true" 
							name="account_code[]" id="account_code">
								<option value="">Select Account code</option>
								<?php foreach($all_account_codes as $key => $row2):?>
								<option value="<?php echo $row2['account_code'];?>"><?php echo $row2['account_code'];?> - <?php echo $row2['account_name'];?></option>
								<?php endforeach;?>
							</select>
						</td>
						<td width="30%">
							<input class="right-align validate" placeholder="Enter allotment amount" name="allotment_amount[]"
							required="" aria-required="true" type="text" autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
							placeholder="Enter allotment amount" />
						</td>
						<td class="center-align">
							<div class="table-actions">
								<a style="cursor:pointer" data-id="deny" data-count="denied" id="delete_row" data-identifier="" class="delete"></a>
							</div>
						</td>
		  			</tr>
		  			<?php endif;?>
		  		</tbody>
			</table>
			<input type="hidden" name="count_all" id="count_all" value="1">
		</div>
	</div>

	<div class="md-footer default">
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
		<button type="submit" class="btn waves-effect waves-light btn-primary" id="save" value="<?php echo BTN_POST ?>"><?php echo BTN_POST ?></button>
	</div>
	
</form>
<?php endif;?>

<script type="text/javascript">
	$(document).ready(function(){
		$('#release_form').parsley();
	})
</script>