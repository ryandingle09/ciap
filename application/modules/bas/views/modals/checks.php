<?php if($this->uri->segment(7) == 'void'):?>
<form id="form">
	<input type="hidden" name="security" value="<?php echo $security; ?>">
	<input type="hidden" name="status" value="VOID">
	<div class="form-float-label">

		<div class="row">
			<div class="col s12">
				<table>
				<tr>
					<td>
						<div><span>DV NO.</span></div>
						<div><p><?php echo ($dv_no_real) ? $dv_no_real : 'Upon Processing';?></p></div>
					</td>
					<td>
						<div><span>DV DATE.</span></div>
						<div><p><?php echo $dv_date;?></p></div>
					</td>
				</tr>
				<tr>
					<td>
						<div><span>CHECK/ADA NO.</span></div>
						<div><p><?php echo $check_no;?></p></div>
					</td>
					<td>
						<div><span>CHECK/ADA DATE</span></div>
						<div><p><?php echo $check_date;?></p></div>
					</td>
				</tr>
				<tr>
					<td>
						<div><span>CHECK AMOUNT</span></div>
						<div><p><?php echo number_format($check_amount);?></p></div>
					</td>
					<td>
						<div><span>MODE OF PAYMENT</span></div>
						<div><p><?php echo $mode_of_payment;?></p></div>
					</td>
				</tr>
				</tr>
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<textarea class="validate materialize-textarea" required="" aria-required="true" name="remarks" id="remarks"><?php echo ($remarks);?></textarea>
					<label for="remarks">REMARKS</label>
				</div>
			</div>
		</div>

	</div>

	<div class="md-footer default">
	    <button class="btn waves-effect waves-light" id="save" value="<?php echo BTN_VOID ?>"><?php echo BTN_VOID ?></button>
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" id="cancel">Cancel</a>
	</div>
</form>
<?php elseif($this->uri->segment(7) == 'view'):?>
<form id="form">

	<div class="form-float-label">

		<div class="row">
			<div class="col s12">

				<table>
					<tr>
						<td>
							<div><span>DV NO.</span></div>
							<div><p><b><?php echo ($dv_no_real) ? $dv_no_real : 'Upon Processing';?></b></p></div>
						</td>
						<td>
							<div><span>DV DATE.</span></div>
							<div><p><b><?php echo $dv_date;?></b></p></div>
						</td>
					</tr>
					<tr>
						<td>
							<div><span>CHECK/ADA NO.</span></div>
							<div><p><b><?php echo $check_no;?></b></p></div>
						</td>
						<td>
							<div><span>CHECK/ADA DATE</span></div>
							<div><p><b><?php echo $check_date;?></b></p></div>
						</td>
					</tr>
					<tr>
						<td>
							<div><span>CHECK AMOUNT</span></div>
							<div><p><?php echo number_format($check_amount);?></p></div>
						</td>
						<td>
							<div><span>MODE OF PAYMENT</span></div>
							<div><p><?php echo $mode_of_payment;?></p></div>
						</td>
					</tr>
				</table>
			</div>

		</div>

	</div>

	<div class="md-footer default">
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" id="cancel">Cancel</a>
	</div>

</form>

<?php elseif($this->uri->segment(7) == 'override'):?>
<form id="form">
	<input type="hidden" name="security" value="<?php echo $security; ?>">
	<input type="hidden" name="status" value="OVERRIDE">
	<div class="form-float-label">

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<label for="dv_no" class="active">DV NO.</label>
					<select aria-required="true" required="" name="dv_no" id="dv_no" class="selectize validate">
						<option value="">SELECT DV NO.</option>
						<?php foreach($disbursement as $key => $row):?>
						<option value="<?php echo $row['disbursement_id'];?>"><?php echo $row['dv_no'];?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<label for="mode" class="active">MODE OF PAYMENT</label>
					<select aria-required="true" required="" name="mode" id="mode" class="selectize validate">
						<option value="">SELECT MODE OF PAYMENT</option>
						<?php foreach($modes as $key => $row):?>
						<option value="<?php echo $row['mode_payment_id'];?>"><?php echo $row['mode_type'];?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
				    <input type="text" aria-required="true" required="" class="datepicker  validate" name="check_date" id="check_date" value="<?php echo date('Y/m/d', strtotime($check_date));?>"/>
				    <label for="check_date">CHECK/ADA DATE</label>
			     </div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<input type="text" class="validate" required="" aria-required="true" name="check_no" id="check_no" value="<?php echo $check_no;?>" autocomplete="off">
					<label for="check_no">CHECK/ADA NO</label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<input type="text" class="validate" required="" aria-required="true" name="check_amount" id="check_amount" value="<?php echo number_format($check_amount);?>"  onkeyup="javascript:this.value=Comma(this.value);" autocomplete="off">
					<label for="check_amount">CHECK/ADA AMOUNT</label>
				</div>
			</div>
		</div>

	</div>

	<div class="md-footer default">
	    <button class="btn waves-effect waves-light" id="save" value="<?php echo BTN_OVERRIDE ?>"><?php echo BTN_OVERRIDE ?></button>
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" id="cancel">Cancel</a>
	</div>
</form>
<?php else: ?>
<form id="form">
	<input type="hidden" name="security" value="<?php echo $security; ?>">
	<input type="hidden" name="status" value="POST">
	<div class="form-float-label">

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<label for="dv_no" class="active">DV NO.</label>
					<select aria-required="true" required="" name="dv_no" id="dv_no" class="selectize validate">
						<option value="">SELECT DV NO.</option>
						<?php foreach($disbursement as $key => $row):?>
						<option value="<?php echo $row['disbursement_id'];?>"><?php echo $row['dv_no'];?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<label for="mode" class="active">MODE OF PAYMENT</label>
					<select aria-required="true" required="" name="mode" id="mode" class="selectize validate">
						<option value="">SELECT MODE OF PAYMENT</option>
						<?php foreach($modes as $key => $row):?>
						<option value="<?php echo $row['mode_payment_id'];?>"><?php echo $row['mode_type'];?></option>
						<?php endforeach;?>
					</select>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
				    <input type="text" aria-required="true" required="" class="datepicker  validate" name="check_date" id="check_date" value="<?php echo (!empty($check_date)) ? date('Y/m/d', strtotime($check_date)) : '' ;?>"/>
				    <label for="check_date">CHECK/ADA DATE</label>
			     </div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<input type="text" class="validate" required="" aria-required="true" name="check_no" id="check_no" value="<?php echo $check_no;?>" autocomplete="off">
					<label for="check_no">CHECK/ADA NO</label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<input type="text" class="validate" required="" aria-required="true" name="check_amount" id="check_amount" value="<?php echo (!empty($check_amount)) ? number_format($check_amount) : '';?>"  onkeyup="javascript:this.value=Comma(this.value);" autocomplete="off">
					<label for="check_amount">CHECK/ADA AMOUNT</label>
				</div>
			</div>
		</div>

	</div>

	<div class="md-footer default">
	    <button class="btn waves-effect waves-light" id="save" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<a class="waves-effect waves-teal btn-flat cancel_modal" id="cancel">Cancel</a>
	</div>
</form>
<?php endif;?>

<script>
$(function(){
  
  $('#form').parsley();
  $('#form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {
	  var data = $(this).serialize();
	  
	  button_loader('save', 1);
	  $.post("<?php echo base_url() . PROJECT_BAS ?>/bas_checks/process/", data, function(result) {
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  button_loader('save', 0);
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  button_loader("save",0);

		  $('.cancel_modal').trigger('click');
		  
		  load_datatable('tbl_check', '<?php echo PROJECT_BAS ?>/bas_checks/get_check_list/');
		}
	  }, 'json');       
    }
  });
  
  <?php if(ISSET($data)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>
})
</script>