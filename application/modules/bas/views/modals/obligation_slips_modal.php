<?php if($this->uri->segment(7) == 'view'): ?>
<form id="form" data-identity="null" class="p-lg form-basic">
	<input type="hidden" name="security" value="<?php echo $security;?>">
	<input type="hidden" name="hash_id" value="<?php echo $hash_id;?>">
	<input type="hidden" name="status" id="status" value="<?php echo STATUS_SUBMITTED;?>">
	<input type="hidden" name="obligation_status_id" id="obligation_status_id" value="<?php echo $obligation_status_id;?>">

	<div class="form-fields form1">
		<div class="row">
			<?php 
			$status = '';
			foreach($obligations as $key => $val):
			$status = $val['obligation_status'];
			?>
			<input type="hidden" name="obligation_id" value="<?php echo $val['obligation_id'];?>">
			<input type="hidden" name="obligation_status_id" id="obligation_status_id" value="<?php echo $obligation_status_id;?>">
			<table width="100%">
				<tr>
					<td width="30%">
						OBLIGATION SLIP NO.<br>
						<p><?php if(!is_null($val['os_no'])) echo $val['os_no']; else echo '(Upon Processing)';?></p>
					</td>
					<td colspan="2">
						OBLIGATION SLIP DATE<br>
						<p>
						<?php 
							$dateTime = new DateTime($val['os_date']);
							if(!is_null($val['os_date'])) echo $dateTime->format("d F Y"); else echo '(Upon Processing)';
						?>
						</p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						DATE OF REQUEST<br>
						<p class="requested_date">
						<?php
						$dateTime = new DateTime($val['requested_date']);
						echo $dateTime->format("d F Y");
						?>
						</p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						PAYEE<br>
						<p class="payee"><?php echo $val['payee_name'];?></p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						ADDRESS<br>
						<p class="payee_address"><?php echo $val['payee_address'];?></p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						PARTICULARS<br>
						<p class="particulars"><?php echo $val['particulars'];?></p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						FUND SOURCE<br>
						<p class="fund_source"><?php echo $val['source_type_name'];?></p>
					</td>
				</tr>
			</table>
			<?php endforeach; ?>

			<table class="bordered responsive-table" style="border-top: 1px solid #ddd">
				<thead class="cyan lighten-5">
					<tr>
						<th data-field="id">LINE ITEM</th>
						<th data-field="name">ACCOUNT CODE</th>
						<th data-field="price">REQUESTED AMOUNT</th>
						<th data-field="price">OBLIGATED AMOUNT</th>
					</tr>
				</thead>
				<tbody class="light-green lighten-5">
					<?php $total = 0;foreach($obligation_charges as $key => $val):?>
					<tr>
						<td>
							<input type="hidden" name="line_item_id[]" value="<?php echo $val['line_item_id'];?>">
							<?php echo $val['line_item_code'].'&nbsp;&nbsp'.$val['line_item_name'];?>
						</td>
						<td>
							<input type="hidden" name="account_code[]" value="<?php echo $val['account_code'];?>">
							<?php echo $val['account_code'].'&nbsp;&nbsp'.$val['account_name'];?>
							
						</td>
						<td>
							<?php echo number_format($val['requested_amount'], 2);?>
						</td>
						<td class="right-align">
							<?php if($status == STATUS_SUBMITTED && $this->uri->segment(8) == 'certify'): ?>
							<div class="col s12">
								<div class="input-field">
									<input 
									type="text" 
									name="obligated_amount[]" 
									style="text-align: right"
									autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);"
							    	placeholder="Enter Amount" 
									value="<?php echo ($val['obligated_amount'] != 0.00) ? number_format($val['obligated_amount']) : '';?>"/>
								</div>
							</div>
							<?php else: ?>
							<?php echo number_format($val['obligated_amount']);?>
							<?php endif;?>
						</td>
					</tr>
					<?php
					 $total += $val['requested_amount'];
					 $total2 += $val['obligated_amount'];
					 endforeach; 
					 ?>
					<tr>
						<td></td>
						<td>TOTAL</td>
						<td><?php echo number_format($total);?></td>
						<td><p class="show_type_total right-align"><?php echo number_format($total2);?></p></td>
					</tr>
				</tbody>
			</table>
			<?php if($status == STATUS_SUBMITTED && $this->uri->segment(8) == 'certify'): ?>
			<div class="m-t-lg">
      			<div class="row m-b-lg m-t-lg">
					<div class="col s12">
						<input type="checkbox" id="manual_flag" name="manual_flag" />
      					<label for="manual_flag">MANUAL ENCODING ( ? )</label>
					</div>
				</div>
				<div class="row">
					<div class="col s2">
						<label for="obligated_date">OBLIGATED DATE</label>
						<div class="input-field">
							<input type="text" name="obligated_date" id="obligated_date" class="datepicker">
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s4">
						<label for="obligation_no">OBLIGATION NO.</label>
						<div class="input-field">
							<input type="text" name="obligation_no" id="obligation_no" class="obligation_no">
						</div>
					</div>
				</div>
			</div>
			<?php endif;?>
		</div>
	</div>

	<?php if($status == STATUS_SUBMITTED && $this->uri->segment(8) == 'certify'): ?>
	<div class="md-footer default form2-footer">
		<button type="button" data-btn="draft" data-status="<?php echo STATUS_CERTIFIED; ?>" data-identity="<?php echo STATUS_CERTIFIED; ?>" class="btn waves-effect waves-light btn-secondary" id="save" value="<?php echo BTN_CERTIFY ?>"><?php echo BTN_CERTIFY ?></button>
		<button type="button" data-btn="submit" data-status="<?php echo STATUS_DECLINED; ?>" data-identity="<?php echo STATUS_CERTIFIED; ?>" class="btn waves-effect waves-light btn-primary" id="save" value="<?php echo BTN_DECLINE ?>"><?php echo BTN_DECLINE ?></button>
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	</div>
	<?php endif;?>
</form>


<?php elseif($this->uri->segment(7) == 'override'): ?>

<form id="form" data-identity="null" class="p-lg form-basic">
	<input type="hidden" name="security" value="<?php echo $security;?>">
	<input type="hidden" name="hash_id" value="<?php echo $hash_id;?>">
	<input type="hidden" name="status" id="status" value="<?php echo STATUS_OVERRIDEN; ?>">
	<input type="hidden" name="obligation_status_id" id="obligation_status_id" value="<?php echo $obligation_status_id;?>">

	<div class="form-fields form1">
		<div class="row">
			<?php 
			foreach($obligations as $key => $val):?>
			<input type="hidden" name="obligation_id" value="<?php echo $val['obligation_id'];?>">
			<table width="100%">
				<tr>
					<td width="30%">
						OBLIGATION SLIP NO.<br>
						<p><?php if(!is_null($val['os_no'])) echo $val['os_no']; else echo '(Upon Processing)';?></p>
					</td>
					<td colspan="2">
						OBLIGATION SLIP DATE<br>
						<p>
						<?php 
							$dateTime = new DateTime($val['os_date']);
							if(!is_null($val['os_date'])) echo $dateTime->format("d F Y"); else echo '(Upon Processing)';
						?>
						</p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						DATE OF REQUEST<br>
						<p class="requested_date">
						<?php
						$dateTime = new DateTime($val['requested_date']);
						echo $dateTime->format("d F Y");
						?>
						</p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						PAYEE<br>
						<p class="payee"><?php echo $val['payee_name'];?></p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						ADDRESS<br>
						<p class="payee_address"><?php echo $val['payee_address'];?></p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						PARTICULARS<br>
						<p class="particulars"><?php echo $val['particulars'];?></p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						FUND SOURCE<br>
						<p class="fund_source"><?php echo $val['source_type_name'];?></p>
					</td>
				</tr>
			</table>
			<?php endforeach; ?>

			<table class="bordered responsive-table" style="border-top: 1px solid #ddd">
				<thead class="cyan lighten-5">
					<tr>
						<th data-field="id">LINE ITEM</th>
						<th data-field="name">ACCOUNT CODE</th>
						<th data-field="price">REQUESTED AMOUNT</th>
						<th data-field="price">OBLIGATED AMOUNT</th>
					</tr>
				</thead>
				<tbody class="light-green lighten-5">
					<?php $total = 0;foreach($obligation_charges as $key => $val):?>
					<tr>
						<td>
							<input type="hidden" name="line_item_id[]" value="<?php echo $val['line_item_id'];?>">
							<?php echo $val['line_item_code'].'&nbsp;&nbsp'.$val['line_item_name'];?>
						</td>
						<td>
							<input type="hidden" name="account_code[]" value="<?php echo $val['account_code'];?>">
							<?php echo $val['account_code'].'&nbsp;&nbsp'.$val['account_name'];?>
							
						</td>
						<td>
							<?php echo number_format($val['requested_amount']);?>
						</td>
						<td>
							<div class="col s12">
								<div class="input-field">
									<input 
									type="text" 
									data-controller="bas_obligation_slips" 
									class="validate controller" 
									required="" aria=required="true" 
									name="obligated_amount[]" 
									id="obligated_amount" 
									style="text-align: right"
									autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);"
							    	placeholder="Enter Amount" 
									value="<?php echo ($val['obligated_amount'] != '0.00') ? number_format($val['obligated_amount']) : '';?>"/>
								</div>
							</div>
						</td>
					</tr>
					<?php
					 $total += $val['requested_amount'];
					 $total2 += $val['obligated_amount'];
					 endforeach; 
					 ?>
					<tr>
						<td></td>
						<td>TOTAL</td>
						<td><?php echo number_format($total);?></td>
						<td><p class="show_type_total right-align"><?php echo number_format($total2);?></p></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>

	<div class="md-footer default form2-footer">
		<button type="button" data-btn="submit" data-identity="<?php echo STATUS_OVERRIDEN;?>" data-status="<?php echo STATUS_OVERRIDEN;?>" class="btn waves-effect waves-light btn-primary" id="save" value="<?php echo BTN_OVERRIDE ?>">
			<?php echo BTN_OVERRIDE ?>
		</button>
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	</div>
</form>

<?php elseif($this->uri->segment(7) == 'void'): ?>

<form id="form" data-identity="VOID" class="p-lg form-basic">
	<input type="hidden" name="security" value="<?php echo $security;?>">
	<input type="hidden" name="hash_id" value="<?php echo $hash_id;?>">
	<input type="hidden" name="status" id="status" value="<?php echo STATUS_VOIDED;?>">
	<input type="hidden" name="obligation_status_id" id="obligation_status_id" value="<?php echo $obligation_status_id;?>">

	<div class="form-fields form1">
		<div class="row">
			<?php 
			$remarks = '';
			foreach($obligations as $key => $val):
			$remarks = $val['remarks'];
			?>
			<input type="hidden" name="obligation_id" value="<?php echo $val['obligation_id'];?>" >
			<table width="100%">
				<tr>
					<td width="30%">
						OBLIGATION SLIP NO.<br>
						<p><?php if(!is_null($val['os_no'])) echo $val['os_no']; else echo '(Upon Processing)';?></p>
					</td>
					<td colspan="2">
						OBLIGATION SLIP DATE<br>
						<p>
						<?php 
							$dateTime = new DateTime($val['os_date']);
							if(!is_null($val['os_date'])) echo $dateTime->format("d F Y"); else echo '(Upon Processing)';
						?>
						</p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						DATE OF REQUEST<br>
						<p class="requested_date">
						<?php
						$dateTime = new DateTime($val['requested_date']);
						echo $dateTime->format("d F Y");
						?>
						</p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						PAYEE<br>
						<p class="payee"><?php echo $val['payee_name'];?></p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						ADDRESS<br>
						<p class="payee_address"><?php echo $val['payee_address'];?></p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						PARTICULARS<br>
						<p class="particulars"><?php echo $val['particulars'];?></p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						FUND SOURCE<br>
						<p class="fund_source"><?php echo $val['source_type_name'];?></p>
					</td>
				</tr>
			</table>
			<?php endforeach; ?>

			<table class="bordered responsive-table" style="border-top: 1px solid #ddd">
				<thead class="cyan lighten-5">
					<tr>
						<th data-field="id">LINE ITEM</th>
						<th data-field="name">ACCOUNT CODE</th>
						<th data-field="price">REQUESTED AMOUNT</th>
						<th data-field="price">OBLIGATED AMOUNT</th>
					</tr>
				</thead>
				<tbody class="light-green lighten-5">
					<?php $total = 0;foreach($obligation_charges as $key => $val):?>
					<tr>
						<td>
							<input type="hidden" name="line_item_id[]" value="<?php echo $val['line_item_id'];?>">
							<?php echo $val['line_item_code'].'&nbsp;&nbsp'.$val['line_item_name'];?>
						</td>
						<td>
							<input type="hidden" name="account_code[]" value="<?php echo $val['account_code'];?>">
							<?php echo $val['account_code'].'&nbsp;&nbsp'.$val['account_name'];?>
							
						</td>
						<td>
							<?php echo number_format($val['requested_amount']);?>
						</td>
						<td>
							<?php echo number_format($val['obligated_amount']);?>
						</td>
					</tr>
					<?php 
					$total += $val['requested_amount'] ;
					$total2 += $val['obligated_amount'] ;
					endforeach; 
					?>
					<tr>
						<td></td>
						<td>TOTAL</td>
						<td><?php echo number_format($total);?></td>
						<td><?php echo number_format($total2);?></p></td>
					</tr>
				</tbody>
			</table>

			<div class="form-basic m-t-lg">
				<p>REMARKS</p>
				<textarea id="remarks" name="remarks" class="materialize-textarea"><?php echo $remarks;?></textarea>
			</div>
			
		</div>
	</div>

	<div class="md-footer default form2-footer">
		<button type="button" data-btn="submit" data-status="<?php echo STATUS_VOIDED;?>" data-identity="<?php echo STATUS_VOIDED;?>" class="btn waves-effect waves-light btn-primary" id="save" value="<?php echo BTN_VOID ?>">
			<?php echo BTN_VOID ?> REQUEST
		</button>
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	</div>
</form>

<?php elseif($this->uri->segment(7) == 'excess'): ?>

<form id="form" data-identity="RETURN EXCESS" class="p-lg form-basic">
	<input type="hidden" name="security" value="<?php echo $security;?>">
	<input type="hidden" name="hash_id" value="<?php echo $hash_id;?>">
	<input type="hidden" name="status" id="status" value="<?php echo STATUS_EXCESS;?>">
	<input type="hidden" name="obligation_status_id" id="obligation_status_id" value="<?php echo $obligation_status_id;?>">

	<div class="form-fields form1">
		<div class="row">
			<?php 
			$remarks = '';
			foreach($obligations as $key => $val):
			$remarks = $val['remarks'];
			?>
			<input type="hidden" name="obligation_id" value="<?php echo $val['obligation_id'];?>" >
			<table width="100%">
				<tr>
					<td width="30%">
						OBLIGATION SLIP NO.<br>
						<p><?php if(!is_null($val['os_no'])) echo $val['os_no']; else echo '(Upon Processing)';?></p>
					</td>
					<td colspan="2">
						OBLIGATION SLIP DATE<br>
						<p>
						<?php 
							$dateTime = new DateTime($val['os_date']);
							if(!is_null($val['os_date'])) echo $dateTime->format("d F Y"); else echo '(Upon Processing)';
						?>
						</p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						DATE OF REQUEST<br>
						<p class="requested_date">
						<?php
						$dateTime = new DateTime($val['requested_date']);
						echo $dateTime->format("d F Y");
						?>
						</p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						PAYEE<br>
						<p class="payee"><?php echo $val['payee_name'];?></p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						ADDRESS<br>
						<p class="payee_address"><?php echo $val['payee_address'];?></p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						PARTICULARS<br>
						<p class="particulars"><?php echo $val['particulars'];?></p>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						FUND SOURCE<br>
						<p class="fund_source"><?php echo $val['source_type_name'];?></p>
					</td>
				</tr>
			</table>
			<?php endforeach; ?>

			<table class="bordered responsive-table" style="border-top: 1px solid #ddd">
				<thead class="cyan lighten-5">
					<tr>
						<th data-field="price">LINE ITEM</th>
						<th data-field="price">ACCOUNT CODE</th>
						<th data-field="price">OBLIGATED AMOUNT</th>
						<th data-field="price">DISBURSED AMOUNT</th>
						<th data-field="price">EXCESS AMOUNT</th>
					</tr>
				</thead>
				<tbody class="light-green lighten-5">
					<?php $total = 0;foreach($obligation_charges as $key => $val):?>
					<tr>
						<td>
							<input type="hidden" name="line_item_id[]" value="<?php echo $val['line_item_id'];?>">
							<?php echo $val['line_item_code'].'&nbsp;&nbsp'.$val['line_item_name'];?>
						</td>
						<td>
							<input type="hidden" name="account_code[]" value="<?php echo $val['account_code'];?>">
							<?php echo $val['account_code'].'&nbsp;&nbsp'.$val['account_name'];?>
							
						</td>
						<td>
							<?php echo number_format($val['obligated_amount']);?>
						</td>
						<td>
							<?php echo number_format($val['disbursed_amount']);?>
						</td>
						<td>
							<?php 
							$excess = $val['obligated_amount'] - $val['disbursed_amount'];
							echo number_format($excess);
							?>
						</td>
					</tr>
					<?php 
					$obligated_total += $val['obligated_amount'];
					$disbursed_total += $val['disbursed_amount'];
					$excess_total 	+= $excess;
					endforeach; 
					?>
					<tr>
						<td></td>
						<td>TOTAL</td>
						<td><?php echo number_format($obligated_total);?></td>
						<td><?php echo number_format($disbursed_total);?></p></td>
						<td><?php echo number_format($excess_total);?></p></td>
					</tr>
				</tbody>
			</table>

			<div class="form-basic m-t-lg">
				<p>REMARKS</p>
				<textarea id="remarks" name="remarks" class="materialize-textarea"><?php echo $remarks;?></textarea>
			</div>
			
		</div>
	</div>

	<div class="md-footer default form2-footer">
		<button type="button" data-btn="submit" data-status="<?php echo STATUS_EXCESS;?>" data-identity="<?php echo STATUS_EXCESS;?>" class="btn waves-effect waves-light btn-primary" id="save" value="<?php echo BTN_EXCESS ?>">
			<?php echo BTN_EXCESS ?> 
		</button>
		
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	</div>
</form>

<?php else: ?>
<form id="form" data-identity="null">
	<input type="hidden" name="security" id="security" value="<?php echo $security;?>">
	<input type="hidden" name="hash_id" value="<?php echo $hash_id;?>">
	<input type="hidden" name="status" id="status" value="<?php echo STATUS_SUBMITTED;?>">
	<input type="hidden" name="obligation_status_id" id="obligation_status_id" value="<?php echo $obligation_status_id;?>">
	<!--form 1-->
	<div class="form-fields form1">
		<div class="row">
			<div class="form-float-label">

				<div class="row">
					<div class="col s2">
						<div class="input-field">
							<?php
						  	$date = new DateTime($requested_date);
							$newdate = $date->format('Y/m/d'); // 2012-07-31
						  	?>
						    <input type="text" class="datepicker validate" required="" name="requested_date" id="requested_date" value="<?php if(!empty($requested_date)) echo $newdate;?>"/>
						    <label for="requested_date" class="active">REQUEST DATE</label>
						</div>
					</div>

					<div class="col s10">
						<div class="input-field">
						    <select name="payee" id="payee" class="selectize" aria-required="true" required="">
								<option>select payee</option>
								<?php foreach($payees as $key => $val):?>
								<option <?php echo ($payee == $val['payee_id']) ? 'selected="selected"' : '';?> value="<?php echo $val['payee_id'];?>"><?php echo $val['payee_name'];?></option>
								<?php endforeach;?>
						    </select>
						  	<label for="payee" class="active block">PAYEE</label>
						  </div>
					</div>

				</div>

				<div class="row">

					<div class="col s6">
						<div class="input-field">
				        	<textarea name="address" id="address" class="materialize-textarea validate" required=""><?php echo $address;?></textarea>
				         	<label id="label_class" for="address" class="<?php echo (ISSET($address) && !empty($address)) ? 'active' : '' ;?>">ENTER PAYEES ADDRESS</label>
				        </div>
				    </div>
					
					<div class="col s6">
				        <div class="input-field">
				        	<textarea name="particulars" id="textarea1" class="materialize-textarea validate" required=""><?php echo $particulars;?></textarea>
				         	<label for="textarea1" class="<?php echo (ISSET($particulars) && !empty($particulars)) ? 'active' : '' ;?>">PARTICULARS</label>
				        </div>
				    </div>

				</div>

				<div class="row">

					<div class="col s2">
						<div class="input-field">
						    <label for="year" class="active block">YEAR</label>
						    <select name="year" class="selectize getFundSources validate" required="" aria-required="true" data-target="fund_source" id="year" placeholder="Please Select">
								<option value="" disabled selected>Choose Year</option>
								<?php foreach($fund_year as $key => $val):?>
								<option <?php echo ($year == $val['year']) ? 'selected="selected"' : '';?> value="<?php echo $val['year'];?>"><?php echo $val['year'];?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>

					<div class="col s10">
						<div class="input-field">
							<label for="fund_source" class="active block">FUND SOURCE</label>
							<select data-selected="<?php echo $source_type_num;?>" name="fund_source" id="fund_source" class="selectize getLineItems" required="" aria-required="true" data-target="line_item_0">
								<option value="" disabled selected></option>
							</select>
						</div>
					</div>

				</div>

			</div>
		</div>

		<div class="card-panel p-n">

			<div class="input-field col s12 p-r-lg">
				<div align="right">
					<a class="waves-effect waves-light btn addRow" data-identity="line_item">ADD LINE ITEM</a>
				</div>
			</div>

			<table id="line_item_table" width="100" cellpadding="0" cellspacing="0" class="m-t-sm m-r-n m-l-n table table-default table-layout-auto" style="border-bottom: 1px solid #ddd">
				<thead class="cyan lighten-5">
					<tr>
						<td class="p-l-sm" width="25%">LINE ITEM</td>
						<td class="p-l-sm" width="30%">ACCOUNT CODE</td>
						<td class="p-l-sm" width="25%">OFFICE</td>
						<td class="p-l-sm" width="20%">REQUESTED AMOUNT</td>
						<td class="p-l-sm" width="5%" style="text-align:center">ACTION</td>
					</tr>
				</thead>
				<tbody>
					<?php if(!empty($obligation_charges)):?>
					<?php $count = 0;foreach($obligation_charges as $key => $val):
					$count++;
					?>
					<tr id="row_<?php echo $key;?>">
						<td class="p-l-sm">
							<div class="form-basic">

								<div class="input-field col s6">
									<select data-selected="<?php echo $val['line_item_id'];?>" name="line_item[]" id="line_item_<?php echo $key;?>" class="selectize getCodesAndOffices line_item_<?php echo $key;?> validate" required="" data-target1="account_code_<?php echo $key;?>" data-target2="office_<?php echo $key;?>">
									</select>
								</div>

							</div>
						</td>
						<td class="p-l-sm">
							<div class="form-basic">

								<div class="input-field col s6">
									<select data-selected="<?php echo $val['account_code'];?>" name="account_code[]" id="account_code_<?php echo $key;?>" class="selectize validate account_code_<?php echo $key;?>" required="">
									</select>
								</div>

							</div>
						</td>
						<td class="p-l-sm">
							<div class="form-basic">

								<div class="input-field col s6">
									<select data-selected="<?php echo $val['office_num'];?>" name="office[]" id="office_<?php echo $key;?>" class="selectize validate office_<?php echo $key;?>" required="">
										<?php foreach($line_item as $keys => $vals):?>
										<option <?php if($val['line_item_id'] == $vals['line_item_id']) echo 'selected="selected"';?> value="<?php echo $vals['line_item_id'];?>"><?php echo $vals['line_item_name'];?></option>
										<?php endforeach;?>
									</select>
								</div>

							</div>
						</td>
						<td width="20%" class="p-l-sm">
							<div class="form-basic">
								<div class="input-field col s12">
									<input 
									type="text" 
									class="validate" 
									required="" aria=required="true" 
									name="requested_amount[]" 
									id="release_date" 
									style="text-align: right"
									autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);"
							    	placeholder="Enter Amount" 
									value="<?php echo number_format($val['requested_amount']);?>"/>
								</div>
							</div>
						</td>
						<td>
							<div class="table-actions">
								<a style="cursor:pointer" data-tooltip="Delete" data-identity="line_item" data-count="<?php echo $key;?>" id="delete_row" data-identifier="line_item" data-id="<?php echo $val['line_item_id'];?>" class="delete delete_row"></a>
							</div>
						</td>
					</tr>
					<?php endforeach; ?>
					<?php else:?>
					<tr id="row_0">
						<td class="p-l-sm">
							<div class="form-basic">

								<div class="input-field col s6">
									<select name="line_item[]" id="line_item_0" class="selectize getCodesAndOffices validate line_item_0" required="" data-target1="account_code_0" data-target2="office_0">
										<option value="" disabled selected>SELECT</option>
									</select>
								</div>

							</div>
						</td>
						<td class="p-l-sm">
							<div class="form-basic">

								<div class="input-field col s6">
									<select name="account_code[]" id="account_code_0" class="selectize validate account_code_0" required="">
										<option value="" disabled selected>SELECT</option>
									</select>
								</div>

							</div>
						</td>
						<td class="p-l-sm">
							<div class="form-basic">

								<div class="input-field col s6">
									<select name="office[]" id="office_0" class="selectize validate office_0" required="">
										<option value="" disabled selected>SELECT</option>
									</select>
								</div>

							</div>
						</td>
						<td width="20%" class="p-l-sm">
							<div class="form-basic">
								<div class="input-field col s12">
									<input 
									type="text" 
									class="validate" 
									required="" aria-required="true" 
									name="requested_amount[]" 
									id="release_date" 
									style="text-align: right"
									autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);"
							    	placeholder="Enter Amount" 
									value=""/>
								</div>
							</div>
						</td>
						<td>
							<div class="table-actions">
								<a style="cursor:pointer" data-tooltip="Delete" data-identity="line_item" data-count="0" id="delete_row" data-identifier="line_item" data-id="" class="delete delete_row"></a>
							</div>
						</td>
					</tr>
					<?php endif;?>
				</tbody>
			</table>
			<input type="hidden" name="line_item_count" id="line_item_count" value="<?php echo (!empty($obligation_charges)) ? $count - 1 : '0';?>">
		</div>

		<div class="card-panel p-n" style="margin-bottom: 10%">

			<div class="input-field col s12 m-t-n p-r-lg">
				<div align="right">
					<a class="waves-effect waves-light btn addRow" data-identity="signatory">ADD SIGNATORY</a>
				</div>
			</div>

			<table id="signatory_table" width="100" cellpadding="0" cellspacing="0" class="m-t-sm m-r-n m-l-n table table-default table-layout-auto" style="border-bottom: 1px solid #ddd">
				<thead class="cyan lighten-5">
					<tr>
						<td class="p-l-sm" width="35%">ROLE</td>
						<td class="p-l-sm" width="35%">NAME</td>
						<td class="p-l-sm" width="20%">POSITION</td>
						<td class="p-l-sm" width="5%" style="text-align: center">ACTION</td>
					</tr>
				</thead>
				<tbody>
					<?php if(!empty($signatories)):?>
					<?php $count = 0; foreach($signatories as $key => $val):
						$count++;
					?>
					<tr id="row_<?php echo $key;?>">
						<td class="p-l-sm">
							<input type="hidden" name="update_signatory_id[]" value="<?php echo $val['signatory_id'];?>">
							<div class="form-basic">
								<div class="input-field col s12">
									<select name="update_role[]" id="role_<?php echo $count;?>" data-count="<?php echo $key;?>" class="selectize validate" required="">
										<option value="" disabled selected>Please Select</option>
										<option <?php echo ($val['role'] == 'R') ? 'selected="selected"' : '';?> value="R">Requested By</option>
										<option <?php echo ($val['role'] == 'C') ? 'selected="selected"' : '';?> value="C">Certified By</option>
									</select>
								</div>
							</div>
						</td>	
						<td class="p-l-sm">
							<div class="form-basic">
								<div class="input-field col s12">
									<input 
									type="text" 
									class="validate name_<?php echo $key;?>" 
									required="" aria=required="true" 
									name="update_name[]" placeholder="Enter Name" 
									value="<?php echo $val['name'];?>"/>
								</div>
							</div>
						</td>
						<td width="30%" class="p-l-sm">
							<div class="form-basic">
								<div class="input-field col s12">
									<input 
									type="text" 
									class="validate position_<?php echo $key;?>" 
									required="" aria=required="true" 
									name="update_position[]"  placeholder="Enter Position" 
									value="<?php echo $val['designation'];?>"/>
								</div>
							</div>
						</td>
						<td>
							<div class="table-actions">
								<a style="cursor:pointer" data-tooltip="Delete" data-identity="signatory" data-count="<?php echo $key;?>" id="delete_row" data-identifier="signatory" data-id="<?php echo $val['signatory_id'];?>" class="delete delete_row"></a>
							</div>
						</td>
					</tr>
					<?php endforeach;?>
					<?php else: ?>
					<tr id="row_0">
						<td class="p-l-sm">
							<div class="form-basic">
								<div class="input-field col s12">
									<select name="role[]" id="role_0" data-count="0" class="selectize validate role" required="">
										<option value="">Select Role</option>
										<option value="R">Requested By</option>
										<option value="C">Certified By</option>
									</select>
								</div>
							</div>
						</td>	
						<td class="p-l-sm">
							<div class="form-basic">
								<div class="input-field col s12 form-basic">
									<input 
									type="text" 
									class="validate name_0" 
									required="" aria=required="true" 
									name="name[]" placeholder="Enter Name" 
									value=""/>
								</div>
							</div>
						</td>
						<td width="30%" class="p-l-sm">
							<div class="form-basic">
								<div class="input-field col s12 form-basic">
									<input 
									type="text" 
									class="validate position_0" 
									required="" aria=required="true" 
									name="position[]"  placeholder="Enter Position" 
									value=""/>
								</div>
							</div>
						</td>
						<td>
							<div class="table-actions">
								<a style="cursor:pointer" data-tooltip="Delete" data-identity="signatory" data-count="0" id="delete_row" data-identifier="signatory" data-id="" class="delete delete_row"></a>
							</div>
						</td>
					</tr>
					<?php endif;?>
				</tbody>
			</table>
			<input type="hidden" name="signatory_count" id="signatory_count" value="<?php echo (!empty($signatories)) ? $count - 1 : '0';?>">
		</div>

	</div>
	<!--end-->

	<!--form2 -->
	<div class="verify-field p-lg form2" style="display: none">
		
		
	</div>

	<div class="md-footer default form1-footer">
		<!--
		<button type="button" data-btn="print" data-status="PRINT" class="btn waves-effect waves-light" id="save" value="<?php echo BTN_PRINT ?>"><?php echo BTN_PRINT ?></button>
		-->
		<button type="button" data-btn="draft" data-status="<?php echo STATUS_DRAFTED;?>" data-identity="<?php echo STATUS_DRAFTED;?>" class="btn waves-effect waves-light btn-secondary" id="save" value="<?php echo BTN_DRAFT ?>"><?php echo BTN_DRAFT ?></button>
		<button type="button" data-btn="submit" data-status="<?php echo STATUS_SUBMITTED;?>" data-identity="<?php echo STATUS_SUBMITTED;?>" class="btn waves-effect waves-light btn-primary" id="save" value="<?php echo BTN_SUBMIT ?>"><?php echo BTN_SUBMIT ?></button>
		<a class="waves-effect waves-teal btn-flat cancel_modal" >Cancel</a>
	</div>
</form>
<script type="text/javascript">
	
	<?php if(!empty($payee) && !empty($year)):?>
	$(document).ready(function(){
		selectize_init();
		$("#payee")[0].selectize.setValue(<?php echo $payee;?>);
		$("#year")[0].selectize.setValue(<?php echo $year;?>);
	});
	<?php endif;?>
</script>

<?php endif;?>
<script type="text/javascript">
	$('form').parsley();
</script>