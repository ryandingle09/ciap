<?php
$id = $data['payee_id'];
$salt = gen_salt();
$token = in_salt($id, $salt);
?>
<?php if($this->uri->segment(7) == 'view'):?>
	<div class="row" style="padding: 20px;">
		<div class="col s10">
			<div><b><span><?php echo ucfirst($data['payee_name']);?></span></b></div>
			<div><span>Payee Name</span></div>
			<hr>
			<br>

			<div><b><span><?php echo ucwords($data['payee_address']);?></span></b></div>
			<div><span>Payee Address</span></div>
			<hr>
			<br>

			<div><b><span><?php echo $data['payee_tin'];?></span></b></div>
			<div><span>Payee Tin Number</span></div>
			<hr>
			<br>

			<div><b><span><?php if($data['vatable_flag'] == 1) echo 'Vatable'; else echo 'Not Vatable';?></span></b></div>
			<div><span>Vat Status</span></div>
			<hr>
			<br>

			<div><b><span><?php if($data['active_flag'] == 1) echo 'Active'; else echo 'Not Active';?></span></b></div>
			<div><span>Status</span></div>
			<br>
		</div>
	</div>
	<div class="md-footer default">
	  	<a class="btn waves-effect waves-light" id="cancel">Close</a>
	</div>
<?php else: ?>
<form id="form">
	<input type="hidden" name="id" value="<?php echo $data['payee_id'] ?>">
	<input type="hidden" name="salt" value="<?php echo $salt ?>">
	<input type="hidden" name="token" value="<?php echo $token ?>">
	<div class="form-float-label">
		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<input type="text" class="validate" required="" aria-required="true" name="payee" id="payee" value="<?php echo $data['payee_name']?>">
					<label for="payee">Payee Name</label>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<textarea class="validate materialize-textarea" required="" aria-required="true" name="address" id="address"><?php echo $data['payee_address']?></textarea>
					<label for="address">Payee Address</label>
				</div>
			</div>
		</div>

		<div class="row">

			<div class="col s6">
				<div class="input-field">
					<input type="text" class="validate" required="" aria-required="true" name="tin" id="tin" value="<?php echo $data['payee_tin']?>">
					<label for="tin">TIN</label>
				</div>
			</div>

			<div class="col s6">
				<div class="input-field">
					<label for="vat" class="active block">SELECT VAT</label>
					<select required="" aria-required="true" name="vat" id="vat" class="selectize">
						<option value=""> -- </option>
						<option value="1">VAT</option>
						<option value="0">NON VAT</option>
					</select>
				</div>
			</div>

		</div>

		<div class="row">
			<div class="col s12">
				<div class="input-field">
					<label for="status" class="active block">STATUS</label>
					<select required="" aria-required="true" name="status" id="status" class="selectize">
						<option value=""> -- </option>
						<option selected="selected" value="1">ACTIVE</option>
						<option value="0">NOT ACTIVE</option>
					</select>
				</div>
			</div>
		</div>

	</div>

	<div class="md-footer default">
	    <button class="btn waves-effect waves-light" id="save" value="<?php echo BTN_SAVE ?>"><?php echo BTN_SAVE ?></button>
	  	<a class="waves-effect waves-teal btn-flat" id="cancel">Cancel</a>
	</div>
</form>
<?php endif;?>

<script>
$(function(){
  	$("#cancel").on("click", function(){
		modalObj.closeModal();
	});
  
  $('#form').parsley();
  $('#form').submit(function(e) {
    e.preventDefault();
    
	if ( $(this).parsley().isValid() ) {
	  var data = $(this).serialize();
	  
	  button_loader('save', 1);
	  $.post("<?php echo base_url() . PROJECT_BAS ?>/bas_payees/process/", data, function(result) {
		if(result.flag == 0){
		  notification_msg("<?php echo ERROR ?>", result.msg);
		  button_loader('save', 0);
		} else {
		  notification_msg("<?php echo SUCCESS ?>", result.msg);
		  button_loader("save",0);
		  modalObj.closeModal();
		  
		  load_datatable('tbl_payees', '<?php echo PROJECT_BAS ?>/bas_payees/get_payees_list/');
		}
	  }, 'json');       
    }
  });
  
  <?php if(ISSET($data)){ ?>
	$('.input-field label').addClass('active');
  <?php } ?>
})
</script>