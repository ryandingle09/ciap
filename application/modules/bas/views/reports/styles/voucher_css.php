.text-ceter{
	text-align: center;
	padding: 0px;
	margin: 0px;
}
.outline{
	border: 1px solid #ccc;
	margin: 30px;
	padding: 0px;
}
.small{
	font-size: 12px;
}
.no-padding{
	padding: 0px;
} 
.no-margin{
	margin: 0px;
}
.border-top{
	border-top: 1px solid #ccc;
}
.border-bottom{
	border-bottom: 1px solid #ccc;
}
.border-left{
	border-left: 1px solid #ccc;
}
.border-right{
	border-right: 1px solid #ccc;
}
.inline{
	display: inline-block;
}
.box{
	width: 25px;
	height: 25px;
	border: 1px solid #ccc;
}
table td {
	padding: 5px;
}
.border{
	border: 1px solid #ccc;
}