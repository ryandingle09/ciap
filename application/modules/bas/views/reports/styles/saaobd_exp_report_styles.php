.saaobd_exp_header {
	border-collapse: collapse !important;
    width: 100%;
  	max-width: 100%;
  	margin-bottom: 20px;
}

.saaodb_exp_header th,
.saaodb_exp_header td {
	padding: 7px !important;
}

.saaobd_exp_header_border {
	border-collapse: collapse !important;
    width: 100%;
  	max-width: 100%;
  	margin-bottom: 20px;
}

.saaodb_exp_header_border th,
.saaodb_exp_header_border td {
	padding: 7px !important;
	border: 1px solid #ddd !important;
}

.center_header {
	text-align : center !important;
	font-size  : 14px !important;
	font-weight: bold;
}

.left_header {
	text-align : left !important;
	font-size  : 14px !important;
	font-weight: bold;
}

.right_header {
	text-align : right !important;
	font-size  : 11px !important;
	font-weight: bold;
}

.text-c {
	font-size : 11px !important;
	font-weight: bold;
}

.text-cen {
	font-size : 11px !important;
	font-weight: bold;
	text-align : center !important;
}

.saaobd_exp {
	border-collapse: collapse !important;
    width: 100%;
  	max-width: 100%;
  	margin-bottom: 20px;	
}

.saaobd_exp td,
.saaobd_exp th {
	padding: 7px !important;
	border: 1px solid #ddd !important;	
}

.text-right {
	text-align : right;
	font-size : 11px !important;
}

.text-left {
	text-align : left;
	font-size : 11px !important;
}

.text-c-not-bold {
	font-size : 11px !important;
}

f-left {
  float :left !important;
}

f-right {
  float : right !important;
}