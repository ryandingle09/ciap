.raomooe_header {
	border-collapse: collapse !important;
    width: 100%;
  	max-width: 100%;
  	margin-bottom: 20px;
}

.raomooe_header th,
.raomooe_header td {
	padding: 7px !important;
}

.center_header {
	font-size 	: 14px !important;
	text-align 	: center !important;
}

.left_header {
	font-size 	: 14px !important;
	text-align 	: left !important;
}

.raomooe_exp {
	border-collapse: collapse !important;
    width: 100%;
  	max-width: 100%;
  	margin-bottom: 20px;	
}

.raomooe_exp td,
.raomooe_exp th {
	padding: 7px !important;
	border: 1px solid #ddd !important;	
}

.text-right {
	text-align : right;
	font-size : 11px !important;
}

.right_header {
	font-size 	: 14px !important;
	text-align 	: right !important;
}

.text-cen {
	text-align : center !important;
}
