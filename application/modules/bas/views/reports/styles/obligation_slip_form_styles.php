li{
  display: inline;
  list-style: none;
}


.obligation_form_table {
    border-collapse: collapse !important;
    width: 100%;
  	max-width: 100%;
}

.obligation_form_table th,
.obligation_form_table td {
	border: 1px solid #ddd !important;
	padding: 7px !important;
  
}

.obligation_form_header_table {
    border-collapse: collapse !important;
    width: 100%;
  	max-width: 100%;
  	margin-bottom: 0px;
}

.obligation_form_header_table th,
.obligation_form_header_table td {
	border: 1px solid #ddd !important;
	padding: 7px !important;
  
}