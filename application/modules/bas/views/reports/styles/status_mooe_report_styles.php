.status_moee {
	border-collapse: collapse !important;
    width: 100%;
  	max-width: 100%;
  	margin-bottom: 20px;
}

.status_moee th,
.status_moee td {
	border: 1px solid #ddd !important;
	padding: 7px !important;
}

.status_mooe_header {
	border-collapse: collapse !important;
    width: 100%;
  	max-width: 100%;
  	margin-bottom: 20px;
}

.status_moee_header th,
.status_moee_header td {
	padding: 7px !important;
}

.center_header {
	text-align : center !important;
	font-size  : 14px !important;
	font-weight: bold;
}

.text-center {
	text-align : center !important;
	font-size  : 11px !important;
}