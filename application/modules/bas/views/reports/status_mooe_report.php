
<?php 
	$office 		= ( ISSET( $office ) ) ? $office : '';
	$report_title 	= ( ISSET( $report_title ) ) ? $report_title : '';
	$date_to 		= ( ISSET( $date_to ) ) ? $date_to : '';
?>

<html>
<head>

	<style type="text/css">
		<?php 
			echo $styles;

		?>
	</style>

	<title><?php echo $report_title ?></title>
</head>

<body>
	<table class="status_mooe_header" >	
		<tr rowspan="4">
			<td class="center_header">
				<?php echo $office ?><br/>
				MAINTENANCE & OTHER OPERATING EXPENSES<br/>
				STATUS OF FUNDS AS OF <?php echo $date_to ?>
			</td>
		</tr>
	</table>
	<table class="status_moee">	
		<thead>
			<tr >
				<th class="text-center" width="35%">EXPENSE ITEM</th>
				<th class="text-center" width="20%">ALLOTMENT</th>
				<th class="text-center" width="20%">OBLIGATIONS INCURRED</th>
				<th class="text-center" width="20%">BALANCE</th>
			</tr>
		</thead>
		<tbody>
			<?php 

				$allot_total 			= 0;
				$obl_total 				= 0;
				$bal_total 				= 0;

				if( !EMPTY( $status_mooe ) ) :

					foreach( $status_mooe as $r ) :
						$allotment 		= str_replace(',', '', $r['allotment'] );
						$obligations 	= str_replace(',', '', $r['obligations'] );
						
						$balance 		= (float)$allotment - (float)$obligations; 

						$allot_total   += $allotment;
						$obl_total     += $obligations;
						$bal_total     += $balance;
			?>
			<tr>
				<td><?php echo $r['expense_item'] ?></td>
				<td class="text-right"><?php echo $r['allotment'] ?></td>
				<td class="text-right"><?php echo $r['obligations'] ?></td>
				<td class="text-right"><?php echo number_format( $balance, 2, '.', ',' ) ?></td>
			</tr>
			<?php endforeach; ?>

			<?php else: ?>

			<?php endif; ?>
			<tr>
				<td><b>TOTAL</b></td>
				<td class="text-right"><?php echo number_format( $allot_total, 2, '.', ',' ) ?></td>
				<td class="text-right"><?php echo number_format( $obl_total, 2, '.', ',' ) ?></td>
				<td class="text-right"><?php echo number_format( $bal_total, 2, '.', ',' ) ?></td>
			</tr>
		</tbody>
	</table>
</body>

</html>