
<html>
<head>

	<style type="text/css">
		<?php 
			echo $style_content;
		?>
	</style>

	<title>Obligation Slip</title>
</head>
<body>
<div class="content">
	<table class="obligation_form_header_table">
		<thead>
			<tr>
				<td class="text-left" style="border-right-style: none !important;">
					<img style="width: 70px;height: 70px;" src="<?php echo base_url().PATH_IMAGES ?>logo.png">
				</td>
				<td colspan="2" class="text-center font-11" style="border-right-style: none !important; border-left-style: none !important;">
					<p class="bold" >Republic of the Philippines<br/>
						Department of Trade and Industry<br/>
						CONSTRUCTION INDUSTRY AUTHORITY OF THE PHILIPPINES<br/>
					</p>
					5th Flr., Executive Bldg., Center 369 Sen. Gil Puyat Ave. Makati City<br/>
					<u>ciapdti@yahoo.com</u> 8961816 / 8961829
				</td>
				<td class="text-right" style="border-left-style: none !important;">
					<img style="width: 70px;height: 70px;" src="<?php echo base_url().PATH_IMAGES ?>dti-logo.png">		
				</td>
			</tr>
		</thead>
	</table>

	<table class="obligation_form_table" >
		<thead>
			
			<tr>
				<td class="text-center bold" colspan="4">
					<h4>OBLIGATION SLIP</h4>
				</td>
			</tr>
		</thead>
		<tbody>
			<tr>					
				<td colspan="2" class="font-11">
					<label class="control-label-m">
						Payee/Office : 
					</label>

					<?php 
						foreach($payees as $key => $val):
						if($val['payee_id'] == $payee) echo strtoupper($val['payee_name']);
						endforeach;
					?>
				</td>
				<td colspan="2" class="font-11">
					<label class="control-label-m">
						No.
					</label>
					<?php echo $os_no;?>
				</td>
			</tr>
			<tr>
				<td rowspan="2" colspan="2" class="font-11">
					<label class="control-label-m">
						Address : <?php echo $address;?>
					</label>
				</td>
				<td colspan="2" class="font-11">
					<label class="control-label-m">
						Date: 
					</label>
					<?php 
						$dateTime = new DateTime($os_date);
						echo $dateTime->format("d-F.Y");
					?>
				</td>
			</tr>
			<tr>
				<td colspan="2" class="font-11">
					<label class="control-label-m">
						Responsibility Center :
					</label>
				</td>
			</tr>
			<tr>
				<td class="text-center">
					<label class="control-label-m">
						Particulars
					</label>
				</td>
				<td class="text-center">
					<label class="control-label-m">
						F/P.P.A
					</label>	
				</td>
				<td class="text-center">
					<label class="control-label-m">
						Account Code
					</label>	
				</td>
				<td class="text-center">
					<label class="control-label-m">
						Amount
					</label>	
				</td>
			</tr>
			<tr>
				<td class="text-center" class="font-11">
					<?php echo $particulars;?>
				</td>
				<td class="text-center" class="font-11">
					<?php foreach($obligation_charges as $key => $val):?>
						<p><?php echo $val['line_item_code'];?></p>
					<?php endforeach;?>
				</td>
				<td class="text-center font-11">
					<?php foreach($obligation_charges as $key => $val):?>
						<p><?php echo $val['account_code'];?></p>
					<?php endforeach;?>
				</td>
				<td class="text-right font-11" >
					<?php foreach($obligation_charges as $key => $val):?>
						<p><?php echo number_format($val['obligated_amount'],2);?></p>
					<?php endforeach;?>
				</td>
			</tr>
			<tr>
				<td colspan="3" class="text-right">
					<label class="control-label-m">
						Total
					</label>
				</td>
				<td class="text-right font-11">
					<p>P <?php echo number_format($obligated_amount, 2);?></p>
				</td>
			</tr>
		</tbody>
	</table>
	<table width="100%" class="obligation_form_table">
		<tr>
			<td colspan="2" class="text-center"n width="350px">
				<label class="control-label-m">
					A. Requested By:
				</label>
			</td>
			<td colspan="2" class="text-center">
				<label class="control-label-m">
					B. Funds Available:
				</label>
			</td>
		</tr>
		<tr>
			<td colspan="2">
				<p class="bold font-11">
					Certified: Charges to appropriate/Allotment necessary,<br/>
					lawful and under my direct supervision.		
				</p>
				
			</td>
			<td colspan="2">
				<p class="bold font-11">
					Certified: Appropriation/Allotment available and <br/>
					Obligated for the purpose as indicated above.
				</p>
			</td>
			</tr>
	</table>
	<table style=";margin-left: -0.3%">
		<tr>
			<td style="border: 0px;padding: 0px;margin: 0px">
				<?php foreach($signatories as $key => $val):?>
					<?php //if($val['role'] == 'R'):?>
					<table cellpadding="0" cellspacing="0" class="obligation_form_table" style="width:300px;padding: 0px;margin: 0px">
						<tr>
							<td colspan="2" class="font-11" style="border-top: none;border-left: 1px solid #ccc;width: 348px">
								<label class="control-label-s">
									Signature:
								</label>
							</td>
						</tr>

						<tr>
							<td colspan="2" class="font-11">
								<label class="control-label-s">
									Printed Name:
								</label>
								<?php if($val['role'] == 'R'):?>
								<?php echo $val['name'];?>
								<?php endif;?>
							</td>
						</tr>

						<tr>
							<td colspan="2" class="font-11">
								<label class="control-label-s">
									Position:
								</label>
								<?php if($val['role'] == 'R'):?>
								<?php echo $val['designation'];?>
								<?php endif;?>
							</td>
						</tr>

						<tr>	
							<td colspan="2" class="font-11">
								<label class="control-label-s">
									Date:
								</label>
							</td>
						</tr>
					</table>
					<?php //endif;?>
				<?php endforeach; ?>
			</td>
			<td colspan="2" style="border: 0px;margin: 0px;padding: 0px;">
				<?php foreach($signatories as $key => $val):?>
					<table class="obligation_form_table" style="width: 500px;">
						<tr>
							<td colspan="2" class="font-11" style="border-left: none;border-top: none;">
								<label class="control-label-s">
									Signature:
								</label>
							</td>
						</tr>

						<tr>
							<td colspan="2" class="font-11" style="border-left: none;">
								<label class="control-label-s">
									Printed Name:
								</label>
								<?php if($val['role'] == 'C'):?>
								<?php echo $val['name'];?>
								<?php endif;?>
							</td>
						</tr>

						<tr>
							<td colspan="2" class="font-11" style="border-left: none;">
								<label class="control-label-s">
									Position:
								</label>
								<?php if($val['role'] == 'C'):?>
								<?php echo $val['designation'];?>
								<?php endif;?>
							</td>
						</tr>
						<tr>	
							<td colspan="2" class="font-11" style="border-left: none;">
								<label class="control-label-s">
									Date:
								</label>
							</td>
						</tr>
					</table>
				<?php endforeach; ?>
			</td>
		</tr>
	</table>
</div>
</body>
</html>

