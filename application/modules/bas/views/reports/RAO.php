<!DOCTYPE html>
<html>
	<head>
		<title>RAO - REPORT</title>
		<style type="text/css">
			.auto{
				margin: 0px auto;
			}
			.text-center{
				text-align: center;
				padding: 0px;
				margin: 0px;
			}
			.text-left{
				text-align: left;
				padding: 0px;
				margin: 0px;
			}
			.text-right{
				text-align: right;
				padding: 0px;
				margin: 0px;
			}
			.outline{
				border: 1px solid #ccc;
				margin: 30px;
				padding: 0px;
			}
			.small{
				font-size: 11px;
			}
			.small2{
				font-size: 10px;
			}
			.small3{
				font-size: 9px;
			}
			.small4{
				font-size: 8px;
			}
			.small5{
				font-size: 5px;
			}
			.no-padding{
				padding: 0px;
			} 
			.no-margin{
				margin: 0px;
			}
			.border-top{
				border-top: 1px solid #000;
			}
			.border-bottom{
				border-bottom: 1px solid #000;
			}
			.border-left{
				border-left: 1px solid #cc000c;
			}
			.border-right{
				border-right: 1px solid #000;
			}
			.inline{
				display: inline-block;
			}
			table td {
				padding: 5px;
			}
			.border{
				border: 1px solid #000;
			}
					
		</style>
	</head>
	<body>
		<?php echo $title_head;?>
		<br>
		<div style="margin-left: 30px">
			<!-- START MAIN TABLE -->
			<?php echo $title;?>
		</div>
	</body>
</html>