<?php 

	$report_title 	= ( ISSET( $report_title ) ) ? $report_title : '';
	$date_to 		= ( ISSET( $date_to ) ) ? $date_to : '';

	$report_type 	= ( ISSET( $report_type ) ) ? $report_type : '';
	$line_item	    = ( ISSET( $line_item ) ) ? $line_item : '';

?>

<html>
<head>
	<style type="text/css">
		<?php 
			echo $styles;

		?>
	</style>
	<title><?php echo $report_title ?></title>
</head>
<body>
	<table class="raomooe_header" >
		<tr>
			<th class="center_header" colspan="2" >
				<?php echo strtoupper( $report_title ) ?> - <?php echo $date_to ?>
			</th>
		</tr>
		<tr>
			<td class="left_header" style="font-weight: bold">
				P/P/A.
			</th>
			<td class="left_header" style="font-weight: bold">
				<?php echo $line_item ?>
			</th>
		</tr>
	</table>
	<table class="raomooe_exp">
		<tr>
			<th rowspan="3" class="text-cen">
				ALOBS No.
			</th>
		</tr>
		<tr>
			<th class="text-cen" colspan="3">Travelling Expenses</th>
			<th class="text-cen" colspan="5">Communication Services</th>
			<th class="text-cen" rowspan="2">Represent Expenses</th>
			<th rowspan="2" class="text-cen">Total MOOE</th>
		</tr>
		<tr>
			<th class="text-cen">Local</th>
			<th class="text-cen">Foreign</th>
			<th class="text-cen">Total</th>
			<th class="text-cen">Landline</th>
			<th class="text-cen">Postage</th>
			<th class="text-cen">Mobile</th>
			<th class="text-cen">Internet</th>
			<th class="text-cen">Total</th>
		</tr>
		<tr>
			<td class="text-cen">Appropriation</td>
			<td class="text-right">100,000.00</td>
			<td class="text-right"></td>
			<td class="text-right">100,000.00</td>
			<td class="text-right">56,000.00</td>
			<td class="text-right">2,000.00</td>
			<td class="text-right">22,000.00</td>
			<td class="text-right">27,000.00</td>
			<td class="text-right">107,000.00</td>
			<td class="text-right">58,000.00</td>
			<td class="text-right">265,000.00</td>
		</tr>
		<tr>
			<td class="text-cen">16-01-08</td>
			<td></td>
			<td></td>			
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td class="text-cen">16-01-09</td>
			<td></td>
			<td></td>			
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td class="text-cen">16-01-14</td>
			<td class="text-right">5,000.00</td>
			<td></td>			
			<td class="text-right">5,000.00</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td class="text-right">5,000.00</td>
		</tr>
		<tr>
			<td>Total ALOBS for the month</td>
			<td class="text-right">5,000.00</td>
			<td></td>			
			<td class="text-right">5,000.00</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td class="text-right">5,000.00</td>
		</tr>
		<tr>
			<td>Total ALOBS last month</td>
			<td></td>
			<td></td>			
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td>Total Date</td>
			<td class="text-right">5,000.00</td>
			<td></td>			
			<td class="text-right">5,000.00</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td class="text-right">5,000.00</td>
		</tr>
		<tr>
			<td>Balance Allotment</td>
			<td class="text-right">95,000.00</td>
			<td></td>			
			<td class="text-right">95,000.00</td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td class="text-right">260,000.00</td>
		</tr>
	</table>
</body>
</html>