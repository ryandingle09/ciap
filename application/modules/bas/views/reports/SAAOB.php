<!DOCTYPE html>
<html>
	<head>
		<title>SAAOB - REPORT</title>
		<style type="text/css">
			.auto{
				margin: 0px auto;
			}
			.text-center{
				text-align: center;
				padding: 0px;
				margin: 0px;
			}
			.text-left{
				text-align: left;
				padding: 0px;
				margin: 0px;
			}
			.text-right{
				text-align: right;
				padding: 0px;
				margin: 0px;
			}
			.outline{
				border: 1px solid #ccc;
				margin: 30px;
				padding: 0px;
			}
			.small{
				font-size: 11px;
			}
			.small2{
				font-size: 10px;
			}
			.small3{
				font-size: 9px;
			}
			.small4{
				font-size: 8px;
			}
			.small5{
				font-size: 5px;
			}
			.no-padding{
				padding: 0px;
			} 
			.no-margin{
				margin: 0px;
			}
			.border-top{
				border-top: 1px solid #000;
			}
			.border-bottom{
				border-bottom: 1px solid #000;
			}
			.border-left{
				border-left: 1px solid #cc000c;
			}
			.border-right{
				border-right: 1px solid #000;
			}
			.inline{
				display: inline-block;
			}
			table td {
				padding: 5px;
			}
			.border{
				border: 1px solid #000;
			}
					
		</style>
	</head>
	<body>
		<div style="margin-left: 25px">
			<div class="text-right">
				<b class="small3">
					FAR No. 1
				</b>
			</div>
			<div class="text-center">
				<b class="small3">
					STATEMENT OF APPROPRIATIONS, ALLOTMENTS, OBLIGATIONS AND BALANCES
				</b><br>
				<p class="small3">As of the Quarter ending <?php echo $to;?></p>
			</div>
		</div>
		
		<div style="margin-left: 25px">
			
			<table class="small3" width="100%" cellpadding="0" cellspacing="0">

				<tr>
					<td width="6%">
						<b>Department</b>
					</td>
					<td class="border-bottom text-center" width="20.8%"> <b><?php echo $department;?></b> </td>
					<td width="54.9%">&nbsp;</td>	
					<td class="border text-center" width="6%"><?php echo ($fund_sourse == 1) ? 'X' : '';?></td>
					<td>
						<div>Current Year Appropriations</div>
					</td>
				</tr>
			</table>


			<table class="small3" width="100%" cellpadding="0" cellspacing="0">

				<tr>
					<td width="5%">
						<b>Agency</b>
					</td>
					<td class="border-bottom text-center" width="30%"><b><?php echo $agency;?></b></td>
					<td width="46.7%">&nbsp;</td>	
					<td class="border-bottom border-left border-right text-center" width="6%"><?php echo ($fund_sourse == 2) ? 'X' : '';?></td>
					<td>
						<div>Supplemental Appropriations</div>
					</td>
				</tr>

			</table>

			<table class="small3" width="100%" cellpadding="0" cellspacing="0">

				<tr>
					<td width="18%">
						<b>Operating Unit</b>
					</td>
					<td class="border-bottom" width="20%"></td>
					<td width="43.7%">&nbsp;</td>	
					<td class="border-bottom border-left border-right text-center" width="6%"><?php echo ($fund_sourse == 3) ? 'X' : '';?></td>
					<td>
						<div>Continuing Appropriations</div>
					</td>
				</tr>

			</table>

			<table class="small3" width="100%" cellpadding="0" cellspacing="0">

				<tr>
					<td width="18%">
						<b>Organization Code (UACS)</b>
					</td>
					<td class="border-bottom text-left" width="20%"><b><?php echo $organization_code;?></b></td>
					<td width="45%">&nbsp;</td>	
					<td></td>
					<td></td>
				</tr>

			</table>

			<table class="small3" width="100%" cellpadding="0" cellspacing="0">

				<tr>
					<td width="18%">
						<b>Funding Source Code( as clustered):</b>
					</td>
					<td class="border-bottom" width="20%"></td>
					<td width="45%">&nbsp;</td>	
					<td></td>
					<td></td>
				</tr>

			</table>
			<br>

			<!-- START MAIN TABLE -->

			<table width="100%" class="border small4 text-center" cellpadding="0" cellspacing="0">

				<tr>
					<td rowspan="3" class="border-right border-bottom">
						PARTICULARS					
					</td>
					<td rowspan="3" class="border-right border-bottom">
						UACS<br>CODE					
					</td>
					<td colspan="3" class="border-right border-bottom">
						Appropriations					
					</td>
					<td colspan="5" class="border-right border-bottom">
						Allotments					
					</td>
					<td colspan="5" class="border-right border-bottom">
						Current Year Obligations					
					</td>
					<td colspan="4" class=" border-bottom">
						Balances					
					</td>
				</tr>

				<tr>
					<td rowspan="2" class="border-right border-bottom">Authorized<br>Appropriations</td>
					<td rowspan="2" class="border-right border-bottom">Adjustments<br>Transfer (To)From<br>Realignment</td>
					<td rowspan="2" class="border-right border-bottom">Adusted<br>Appropriation</td>
					<td rowspan="2" class="border-right border-bottom">Allotments<br>Recieved</td>
					<td rowspan="2" class="border-right border-bottom">Adjustments<br>(withdrawal<br>Realignment)</td>
					<td rowspan="2" class="border-right border-bottom">Transfer<br>To</td>
					<td rowspan="2" class="border-right border-bottom">Transfer<br>From</td>
					<td rowspan="2" class="border-right border-bottom">Adjusted<br>Total<br>Allotments</td>
					<td rowspan="2" class="border-right border-bottom">1st Quarter<br>Ending<br>March 31</td>
					<td rowspan="2" class="border-right border-bottom">2nd Quarter<br>Ending<br>June 30</td>
					<td rowspan="2" class="border-right border-bottom">3rd Quarter<br>Ending<br>Sept 30</td>
					<td rowspan="2" class="border-right border-bottom">4th Quarter<br>Ending<br>March 31</td>
					<td rowspan="2" class="border-right border-bottom">Total</td>
					<td rowspan="2" class="border-right border-bottom">Unreleased<br>Appropriation</td>
					<td rowspan="2" class="border-right border-bottom">Unobligated<br>Allotment</td>
					<td class="border-right border-bottom" colspan="2">Unpaid Obligations</td>
				</tr>

				<tr>
					<td class="border-right border-bottom">Due and<br>demandable</td>
					<td class="border-right border-bottom">Not Yet Due<br>Demandable</td>
				</tr>
				
				<tr>
					<td class="border-right border-bottom small5">1</td>
					<td class="border-right border-bottom small5">2</td>
					<td class="border-right border-bottom small5">3</td>
					<td class="border-right border-bottom small5">4</td>
					<td class="border-right border-bottom small5">5=(3+4)</td>
					<td class="border-right border-bottom small5">6</td>
					<td class="border-right border-bottom small5">7</td>
					<td class="border-right border-bottom small5">8</td>
					<td class="border-right border-bottom small5">9</td>
					<td class="border-right border-bottom small5">10=(6+(-)7-8+9)</td>
					<td class="border-right border-bottom small5">11</td>
					<td class="border-right border-bottom small5">12</td>
					<td class="border-right border-bottom small5">13</td>
					<td class="border-right border-bottom small5">14</td>
					<td class="border-right border-bottom small5">15=(11+12+13+14)</td>
					<td class="border-right border-bottom small5">21=(5+10)</td>
					<td class="border-right border-bottom small5">22=(10-15)</td>
					<td class="border-right border-bottom small5">(15-20)<br>23</td>
					<td class="border-bottom">24</td>
				</tr>

				<?php foreach($report as $key => $row):?>
				<tr>
					<!--1-->
					<td style="padding-bottom: 0px;padding-top: 5px;margin: 0px"  class="border-right text-left" width="15%" valign="top">
						<?php 
							foreach($report as $keys => $rows):
								echo ($row['parent_line_item_id'] == $rows['line_item_id']) ? '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' : '';
							endforeach;
						?>

						<?php echo $row['line_item_code'];?>&nbsp;<?php echo $row['line_item_name'];?><br>

						<?php if($row['parent_line_item_id'] != $line_item_id): ?>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;PS<br>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;MOOE<br>
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CO
						<?php endif; ?> 
					</td>
					<!--2-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" width="1%"></td>
					<!--3-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" valign="top">
						<?php 
							if($row['parent_line_item_id'] != $line_item_id):

								echo (number_format($row['total_appropriation']) != 0) ? number_format($row['total_appropriation']).'<br>' : '';
								echo (number_format($row['ps_appropriation']) != 0) ? number_format($row['ps_appropriation']).'<br>' : '';
								echo (number_format($row['mooe_appropriation']) != 0) ? number_format($row['mooe_appropriation']).'<br>' : '';
								echo (number_format($row['co_appropriation']) != 0) ? number_format($row['co_appropriation']) : '';

							endif; 
						?> 
					</td>
					<!--4-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" valign="top">
						<?php 
							if($row['parent_line_item_id'] != $line_item_id):

								echo (number_format($row['total_appropriation_realigned']) != 0) ? number_format($row['total_appropriation_realigned']).'<br>' : '';
								echo (number_format($row['ps_appropriation_realigned']) != 0) ? number_format($row['ps_appropriation_realigned']).'<br>' : '';
								echo (number_format($row['mooe_appropriation_realigned']) != 0) ? number_format($row['mooe_appropriation_realigned']).'<br>' : '';
								echo (number_format($row['co_appropriation_realigned']) != 0) ? number_format($row['co_appropriation_realigned']) : '';

							endif; 
						?> 
					</td>
					<!--5-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" valign="top">
						<?php 
							if($row['parent_line_item_id'] != $line_item_id):

								$total_apro_and_realigned 	= ($row['total_appropriation'] + $row['total_appropriation_realigned']);
								$total_ps_appro 			= ($row['ps_appropriation'] + $row['ps_appropriation_realigned']);
								$total_mooe_appro 			= ($row['mooe_appropriation'] + $row['mooe_appropriation_realigned']);
								$total_co_appro 			= ($row['co_appropriation'] + $row['co_appropriation_realigned']);

								echo (number_format($total_apro_and_realigned) != 0) ? number_format($total_apro_and_realigned).'<br>' : '';
								echo (number_format($total_ps_appro) != 0) ? number_format($total_ps_appro).'<br>' : '';
								echo (number_format($total_mooe_appro) != 0) ? number_format($total_mooe_appro).'<br>' : '';
								echo (number_format($total_co_appro) != 0) ? number_format($total_co_appro).'<br>' : '';
							
							endif; 
						?> 
					</td>
					<!--6-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" valign="top">
						<?php 
							
							if($row['parent_line_item_id'] != $line_item_id):

								echo (number_format($row['total_allotment']) != 0) ? number_format($row['total_allotment']).'<br>' : '';
								echo (number_format($row['ps_allotment']) != 0) ? number_format($row['ps_allotment']).'<br>' : '';
								echo (number_format($row['mooe_allotment']) != 0) ? number_format($row['mooe_allotment']).'<br>' : '';
								echo (number_format($row['co_allotment']) != 0) ? number_format($row['co_allotment']) : ';';
							
							endif; 
						?> 
					</td>
					<!--7-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" valign="top">
						<?php 

							if($row['parent_line_item_id'] != $line_item_id):

								echo (number_format($row['total_allotment_realigned']) != 0) ? number_format($row['total_allotment_realigned']).'<br>' : '';
								echo (number_format($row['ps_allotment_realigned']) != 0) ? number_format($row['ps_allotment_realigned']).'<br>' : '';
								echo (number_format($row['mooe_allotment_realigned']) != 0) ? number_format($row['mooe_allotment_realigned']).'<br>' : '';
								echo (number_format($row['co_allotment_realigned']) != 0) ? number_format($row['co_allotment_realigned']) : '';

							endif; 

						?> 
					</td>
					<!--8-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" valign="top">
						<?php 
							if($row['parent_line_item_id'] != $line_item_id): 

								$transfer_from = 0;
								$transfer_from += $row['transfer_from'];

								echo ($transfer_from != 0) ? number_format($transfer_from).'<br>' : '';
								echo ($row['line_item_id'] == $row['recipient_line_item_id']) ? (number_format($row['transfer_from']) != 0) ? number_format($row['transfer_from']) : '' : '';
							
							endif;
						?> 
					</td>
					<!--9-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" valign="top">
						<?php 
							if($row['parent_line_item_id'] != $line_item_id):

								$transfer_to = 0;
								$transfer_to += $row['transfer_to'];

								echo (number_format($transfer_to != 0)) ? number_format($transfer_to).'<br>' : '';
								echo ($row['line_item_id'] == $row['source_line_item_id']) ? (number_format($row['transfer_to']) != 0) ? number_format($row['transfer_to']) : '' : '';

							endif;
						?>
					</td>
					<!--10-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" valign="top">
						<?php 
							if($row['parent_line_item_id'] != $line_item_id):

								$total_allo_and_realigned 	= ($row['total_allotment'] + $row['total_allotment_realigned']);
								$total_ps_allo 				= ($row['ps_allotment'] + $row['ps_allotment_realigned']);
								$total_mooe_allo 			= ($row['mooe_allotment'] + $row['mooe_allotment_realigned']);
								$total_co_allo 				= ($row['co_allotment'] + $row['co_allotment_realigned']);

								echo (number_format($total_allo_and_realigned) != 0) ? number_format($total_allo_and_realigned).'<br>' : '';
								echo (number_format($total_ps_allo) != 0) ? number_format($total_ps_allo).'<br>' : '';
								echo (number_format($total_mooe_allo) != 0) ? number_format($total_mooe_allo).'<br>' : '';
								echo (number_format($total_co_allo) != 0) ? number_format($total_co_allo).'<br>' : '';
							
							endif; 
						?> 
					</td>
					<!--11-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" valign="top">
						<?php 

							if($row['parent_line_item_id'] != $line_item_id):

								echo (number_format($row['obligated_amount_1']) != '0') ? number_format($row['obligated_amount_1']).'<br>' : '';
								echo (number_format($row['ps_ob_amount_1']) != '0') ? number_format($row['ps_ob_amount_1']).'<br>' : '';
								echo (number_format($row['mooe_ob_amount_1']) != '0') ? number_format($row['mooe_ob_amount_1']).'<br>' : '';
								echo (number_format($row['co_ob_amount_1']) != '0') ? number_format($row['co_ob_amount_1']).'<br>' : '';

							endif;

						?> 
						
					</td>
					<!--12-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" valign="top">
						<?php 

							if($row['parent_line_item_id'] != $line_item_id):

								echo (number_format($row['obligated_amount_2']) != '0') ? number_format($row['obligated_amount_2']).'<br>' : '';
								echo (number_format($row['ps_ob_amount_2']) != '0') ? number_format($row['ps_ob_amount_2']).'<br>' : '';
								echo (number_format($row['mooe_ob_amount_2']) != '0') ? number_format($row['mooe_ob_amount_2']).'<br>' : '';
								echo (number_format($row['co_ob_amount_2']) != '0') ? number_format($row['co_ob_amount_2']) : '';
							
							endif; 

						?> 
						
					</td>
					<!--13-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" valign="top">
						<?php 

							if($row['parent_line_item_id'] != $line_item_id):

								echo (number_format($row['obligated_amount_3']) != '0') ? number_format($row['obligated_amount_3']).'<br>' : '';
								echo (number_format($row['ps_ob_amount_3']) != '0') ? number_format($row['ps_ob_amount_3']).'<br>' : '';
								echo (number_format($row['mooe_ob_amount_3']) != '0') ? number_format($row['mooe_ob_amount_3']).'<br>' : '';
								echo (number_format($row['co_ob_amount_3']) != '0') ? number_format($row['co_ob_amount_3']).'<br>' : '';
							
							endif; 
						?> 
					</td>
					<!--14-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" valign="top">
						<?php 

							if($row['parent_line_item_id'] != $line_item_id):

								echo (number_format($row['obligated_amount_4']) != '0') ? number_format($row['obligated_amount_4']).'<br>' : '';
								echo (number_format($row['ps_ob_amount_4']) != '0') ? number_format($row['ps_ob_amount_4']).'<br>' : '';
								echo (number_format($row['mooe_ob_amount_4']) != '0') ? number_format($row['mooe_ob_amount_4']).'<br>' : '';
								echo (number_format($row['co_ob_amount_4']) != '0') ? number_format($row['co_ob_amount_4']).'<br>' : '';
							
							endif; 

						?> 
					</td>
					<!--15-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" valign="top">
						<?php 
							if($row['parent_line_item_id'] != $line_item_id): 

								$total_ob_ps 	= ($row['ps_ob_amount_1'] + $row['ps_ob_amount_2'] + $row['ps_ob_amount_3'] + $row['ps_ob_amount_4']);
								$total_ob_mooe 	= ($row['mooe_ob_amount_1'] + $row['mooe_ob_amount_2'] + $row['mooe_ob_amount_3'] + $row['mooe_ob_amount_4']);
								$total_ob_co 	= ($row['co_ob_amount_1'] + $row['co_ob_amount_2'] + $row['co_ob_amount_3'] + $row['co_ob_amount_4']);

								echo (number_format($total_ob_ps + $total_mooe + $total_ob_co) != 0) ? number_format($total_ob_ps + $total_ob_mooe + $total_ob_co).'<br>' : '';
								echo (number_format($total_ob_ps)) ? number_format($total_ob_ps).'<br>' : '';
								echo (number_format($total_ob_mooe)) ? number_format($total_ob_mooe).'<br>' : '';
								echo (number_format($total_ob_co)) ? number_format($total_ob_co) : '';

							endif; 
						?> 
					</td>
					<!--21-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" valign="top">
						<?php 
							if($row['parent_line_item_id'] != $line_item_id): 

								$total_ps_15 				= ($total_ps_appro + $total_ps_allo);
								$total_mooe_15				= ($total_mooe_appro + $total_mooe_allo);
								$total_co_15				= ($total_co_appro + $total_co_allo);


								echo number_format($total_ps_15 + $total_mooe_15 + $total_co_15).'<br>';
								echo number_format($total_ps_15).'<br>';
								echo number_format($total_mooe_15).'<br>';
								echo number_format($total_co_15);

							endif; 
						?> 
					</td>
					<!--22-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" valign="top">
						<?php 
							if($row['parent_line_item_id'] != $line_item_id): 

								echo number_format(($total_ps_allo - $total_ob_ps) + ($total_mooe_allo - $total_ob_mooe) + ($total_co_allo - $total_ob_co) ).'<br>';
								echo number_format($total_ps_allo - $total_ob_ps).'<br>';
								echo number_format($total_mooe_allo - $total_ob_mooe).'<br>';
								echo number_format($total_co_allo - $total_ob_co).'<br>';

							endif; 
						?> 
					</td>
					<!--23-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right" valign="top">
						<?php 
							if($row['parent_line_item_id'] != $line_item_id):

								echo (number_format( ($total_ob_co - $total_dv_co) + ($total_ob_mooe - $total_dv_mooe) + ($total_ob_ps - $total_dv_ps)) != 0) ? number_format( ($total_ob_co - $total_dv_co) + ($total_ob_mooe - $total_dv_mooe) + ($total_ob_ps - $total_dv_ps)).'<br>' : '';
								echo (number_format($total_ob_ps - $total_dv_ps) != 0) ? number_format($total_ob_ps - $total_dv_ps).'<br>' : '';
								echo (number_format($total_ob_mooe - $total_dv_mooe) != 0) ? number_format($total_ob_mooe - $total_dv_mooe).'<br>' : '';
								echo (number_format($total_ob_co - $total_dv_co) != 0) ? number_format($total_ob_co - $total_dv_co).'<br>' : '';

							endif;
						?> 
					</td>
					<!--24-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px;" class="border-right text-right" valign="top">
						<?php if($row['parent_line_item_id'] != $line_item_id): ?>
							
						<?php endif; ?> 
					</td>
				</tr>
				<?php endforeach;?>
			</table>
			
		</div>
	</body>
</html>