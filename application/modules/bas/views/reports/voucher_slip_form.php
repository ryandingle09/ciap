<!DOCTYPE html>
<html>
	<head>
		<title>DISBURSEMENT VOUCHER</title>
		<style type="text/css">
			.text-center{
				text-align: center;
				padding: 0px;
				margin: 0px;
			}
			.outline{
				border: 1px solid #ccc;
				margin: 30px;
				padding: 0px;
			}
			.small{
				font-size: 11px;
			}
			.no-padding{
				padding: 0px;
			} 
			.no-margin{
				margin: 0px;
			}
			.border-top{
				border-top: 1px solid #ccc;
			}
			.border-bottom{
				border-bottom: 1px solid #ccc;
			}
			.border-left{
				border-left: 1px solid #ccc;
			}
			.border-right{
				border-right: 1px solid #ccc;
			}
			.inline{
				display: inline-block;
			}
			.box{
				width: 25px;
				height: 25px;
				border: 1px solid #ccc;
			}
			table td {
				padding: 5px;
			}
			.border{
				border: 1px solid #ccc;
			}
					
		</style>
	</head>
	<body>
		<div style="margin: 30px;width: 100%;padding: 0px">
			<table width="100%">
				<tr>
					<td width="10%" class="text-center"><img src="<?php echo base_url();?>static/images/logo.png" style="width: 70px; height: 70px"></td>
					<td style="padding: 30px" colspan="2" width="80%" class="text-center">
						<p class="small no-margin no-padding">REPUBLIC OF THE PHILIPPINES</p>
						<p class="small no-margin no-padding">DEPARTMENT OF TRADE AND INDUSTRY</p>
						<p class="no-margin no-padding"><b>CONSTRUCTION INDUSTRY OF THE PHILIPPINES</b></p>
						<p class="small no-margin no-padding">5th Floor Executive Building Center 369 Sen. Gil Puyat Ave.,</p>
						<p class="small no-margin no-padding">cor. Makati City Philippines</p>
					</td>
					<td width="10%" class="text-center"><img src="<?php echo base_url();?>static/images/dti-logo.png" style="width: 70px; height: 70px"></td>
				</tr>
			</table>
			<?php foreach($form_data as $key => $row):?>
			<table width="100%" cellpadding="0" cellspacing="0">

				<tr>
					<td colspan="4" class="text-center border"><b>DISBURSEMENT VOUCHER</b></td>
				</tr>

				<tr>
					<td colspan="2" class="text-center border-left border-right" style="border-left: 1px solid #ccc;border-right: 1px solid #ccc"><b>MODE OF PAYMENT</b></td>
					<td colspan="2" class="border-right"><small>No.</small><br><?php echo $row['dv_no'];?></td>
				</tr>

				<tr style="border-bottom: 1px solid #ccc; border-right: 1px solid #ccc">
					<td colspan="2" class="text-center border-right border-left border-top" style="padding-left: 30px;border-left: 1px solid #ccc">
						<table border="0">
							<tr>
								<td style="border: 1px solid #ccc;width: 70px;padding-left: 25px">
									<?php echo ($row['mode_of_payment'] == '1') ? '<div style="padding: 0px;margin: 0px;"><img src="'.base_url().'static/font/admin-ui/png/checkmark21.png" style="height: 12px;"></div>' : '';?>
								</td>
								<td align="left" ><small> MDS Check</small></td>
								<td style="border: 1px solid #ccc;width: 70px;padding-left: 25px">
									<?php echo ($row['mode_of_payment'] == '2') ? '<div style="padding: 0px;margin: 0px;"><img src="'.base_url().'static/font/admin-ui/png/checkmark21.png" style="height: 12px;"></div>' : '';?>
								</td>
								<td align="left" ><small style="margin-top: 10px"> Commercial Check</small></td>
								<td style="border: 1px solid #ccc;width: 70px;padding-left: 25px">
									<?php echo ($row['mode_of_payment'] == '3') ? '<div style="padding: 0px;margin: 0px;"><img src="'.base_url().'static/font/admin-ui/png/checkmark21.png" style="height: 12px;"></div>' : '';?>
								</td>
								<td align="left" ><small> ADA</small></td>
							</tr>
						</table>
					</td>
					<td class="border-top border-right" colspan="2" width="20%"><small>Date:</small><br><?php echo $row['dv_date'];?></td>
				</tr>
		
				<tr>
					<td class="border-top border-left"><small>Payee/Office:</td>
					<td class="text-left border-top border-left" width="50%"><small><?php echo $row['payee_name'];?></small></td>
					<td colspan="2" style="padding: 0px; margin: 0px">
						<table width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td class="border-right border-left border-top"><small><small>TIN/Emplyee No.</small><br><?php echo $row['payee_tin'];?></td>
							</tr>
							<tr>
								<td class="border-top border-right border-left"><small>OS No.</small><br><?php echo $row['os_no'];?></td>
							</tr>
							<tr>
								<td class="border-top border-left border-right"><small>Date:</small><br><?php echo $row['os_date'];?></td>
							</tr>
						</table>
					</td>
				</tr>

				<tr>
					<td colspan="2" class="border-top border-left"><small>Address:</small></td>
					<td colspan="2" class="border-top border-left border-right border-bottom text-center"><small>Responsibility Center</small></td>
				</tr>

				<tr>
					<td style="border-left: 1px solid #ccc"></td>
					<td><small><?php echo $row['payee_address'];?></small></td>
					<td width="30%" class="border-right border-left" style="padding: 0;margin: 0">
						<table width="100%" cellspacing="0" cellpadding="0">
							<tr>
								<td class="text-left border-bottom">
									<small>Title:</small><br>
									<?php foreach($charges as $he => $ho):?>
										<?php foreach($account_codes as $a => $b):?>
											<?php echo ($ho['account_code'] == $b['account_code']) ? $b['account_name'] : '';?>
										<?php endforeach;?>
									<?php endforeach;?>	
								</td>
							</tr>
							<tr>
								<td class="text-left">
									<small>Code:</small><br>
									<?php foreach($charges as $hi => $hey):?>
									<?php echo $hey['account_code'].'<br>';?>
									<?php endforeach;?>	

								</td>
							</tr>
						</table>
					</td>
				</tr>
				
				<tr>
					<td colspan="2" class="border-left border-top text-center"><b>PARTICULARS<b></td>
					<td colspan="2" class="text-center border-top border-right border-left"><b>AMOUNT</b></td>
				</tr>

				<tr>
					<td colspan="2" class="text-right border-left border-top" height="200px"><?php echo $row['particulars'];?></td>
					<td colspan="2" class="text-right border-left border-top border-right"><b><?php echo number_format($row['disbursed_amount'], 2);?></b></td>
				</tr>

				<tr style="border: 1px solid #ccc">
					<td colspan="2" class="text-right border-left border-top"><b>Amount Due<b></td>
					<td colspan="2" class="text-right border-left border-top border-right"><b>P <?php echo number_format($row['amount_due'], 2);?></b></td>
				</tr>
				
		
			</table>

			<!-- <table width="100%" style="margin-top: 2px;" class="border-top border-right border-left">
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
			</table>
			<table width="100%" class="border-right border-left text-small">
				<tr>
					<td width="10%" class="border-right border-top border-bottom small">&nbsp;A</td>
					<td class="small">Certified</td>
					<td width="10%" class="border">&nbsp;</td>
					<td class="small">Suporting documents complete and proper</td>
					<td width="10%" class="border small">&nbsp; B</td>
					<td class="small">Aprroved for payment</td>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td width="10%">&nbsp;</td>
					<td class="small">&nbsp;</td>
					<td width="10%" class="border">&nbsp;</td>
					<td class="small">Cash available</td>
					<td width="10%">&nbsp;</td>
					<td class="small">&nbsp;</td>
					<td class="2">&nbsp;</td>
				</tr>
				<tr>
					<td width="10%">&nbsp;</td>
					<td class="small">&nbsp;</td>
					<td width="10%" class="border">&nbsp;</td>
					<td class="small">Subject to ADA(where applicable)</td>
					<td width="10%">&nbsp;</td>
					<td class="small">&nbsp;</td>
					<td class="2">&nbsp;</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				
				<tr>
					<td width="10%" class="small">&nbsp;Signature</td>
					<td>&nbsp;</td>
					<td width="10%" class="border-bottom" colspan="2">&nbsp;</td>
					<td width="10%" class="small">Signature</td>
					<td class="border-bottom" colspan="3">&nbsp;</td>
				</tr>
			</table> -->
			<!-- OLD BOTTOM -->
			<table width="100%" border="0" style="font-size: 10px;border-top: 1px solid #ccc;border-right: 1px solid #ccc;border-left: 1px solid #ccc;margin-top: 2px;">
				<tr>
					<td colspan="5">&nbsp;</td>
					<td colspan="2">&nbsp;</td>
				</tr>

				<tr>
					<td width="5%" style="border: 1px solid #ccc;padding-left: 5px" class="text-center"><b>A</b></td>
					<td width="10%" ><small>Certified</small></td>
					<td width="10%" style="border: 1px solid #ccc"></td>
					<td width="25%" class="text-left"><small style="font-size: 9px;padding-left: 5px">Supporting documents complete & proper</small></td>
					<td width="3%">&nbsp;</td>
					<td width="5%" style="border: 1px solid #ccc;padding-left: 5px" class="text-center">B</td>
					<td width="42%" class="text-left"><small>Approved for payment</small></td>
				</tr>
				
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td style="border: 1px solid #ccc;padding-left: 5px" ></td>
					<td class="text-left"><small style="font-size: 9px">Cash Available</small></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				
				<tr>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td style="border: 1px solid #ccc;padding-left: 5px" ></td>
					<td class="text-left"><small style="font-size: 9px;padding-left: 5px">Subject to ADA(where applicable)</small></td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
					<td>&nbsp;</td>
				</tr>
				
			</table>
			
			<table border="0" width="100%" style="font-size: 10px" cellpadding="0" cellspacing="0">	
				<tr>
					<td width="15%" class="border-left"><small>Signature</small></td>
					<td width="26%" class="border-bottom">&nbsp;</td>
					<td width="10%">&nbsp;</td>
					<td width="15%"><small>Signature</small></td>
					<td width="25%" class="border-bottom">&nbsp;</td>
					<td width="5%" class="border-right">&nbsp;</td>
				</tr>

				<tr>
					<td width="15%" class="border-left"><small>Printed Name</small></td>
					<td width="25%" class="border-bottom">&nbsp;</td>
					<td width="10%">&nbsp;</td>
					<td width="15%"><small>Printed Name</small></td>
					<td width="25%" class="border-bottom">&nbsp;</td>
					<td width="5%" class="border-right">&nbsp;</td>
				</tr>
				<tr>
					<td width="15%" class="border-left"><small>Position</small></td>
					<td width="25%" class="border-bottom">&nbsp;</td>
					<td width="10%">&nbsp;</td>
					<td width="15%"><small>Position</small></td>
					<td width="25%" class="border-bottom">&nbsp;</td>
					<td width="5%" class="border-right">&nbsp;</td>
				</tr>
				<tr>
					<td width="15%" class="border-left"><small>Date</small></td>
					<td width="25%" class="border-bottom">&nbsp;</td>
					<td width="10%">&nbsp;</td>
					<td width="15%"><small>Date</small></td>
					<td width="25%" class="border-bottom">&nbsp;</td>
					<td width="5%" class="border-right">&nbsp;</td>
				</tr>

				<tr>
					<td colspan="3" class="border-left">&nbsp;</td>
					<td colspan="3" class="border-right">&nbsp;</td>
				</tr>
			</table>
			<BR>

			<table width="100%" class="border" cellspacing="0" cellpadding="0" style="margin-top: -25px;">

				<tr>
					<td colspan="4" class="border-right">&nbsp;</td>
					<td colspan="3">&nbsp;</td>
				</tr>
				
				<tr>
					<td class="text-center border-right border-bottom border-top"><b>C</b></td>
					<td width="20%"><small>Recieved Payment</small></td>
					<td width="30%">Check/ADA No.</td>
					<td width="10%" class="text-left border-right">_____________</td>

					<td width="10%" class="border-right border-top border-bottom" style="text-align:center;padding-left: 5px">D</td>
					<td width="10%" class="text-left"><small>Journal Entry Voucher</small></td>
				</tr>

				<tr>
					<td><small>Signature</small></td>
					<td>_____________</td>
					<td width="10%"><small>Bank Name:</small></td>
					<td style="border-right: 1px solid #ccc">_____________</td>
					<td width="10%"><small></small></td>
					<td width="40%"></td>
				</tr>


				<tr>
					<td><small>Printed Name:</small></td>
					<td>_____________</td>
					<td width="10%"><small>OR no./other relevenat</small></td>
					<td class="border-right">_____________</td>
					<td width="10%"><small>No:</small></td>
					<td>_____________</td>
				</tr>


				<tr>
					<td><small>Date:</small></td>
					<td>_____________</td>
					<td width="10%"><small>document issued on</small></td>
					<td>_____________</td>
					<td style="border-left: 1px solid #ccc" width="10%"><small>Date:</small></td>
					<td>_____________</td>
				</tr>
				
				<tr>
					<td colspan="4">&nbsp;</td>
					<td colspan="3">&nbsp;</td>
				</tr>

			</table>
			<?php endforeach;?>
		</div>
		
	</body>
</html>