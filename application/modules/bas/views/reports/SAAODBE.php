<!DOCTYPE html>
<html>
	<head>
		<title>SAAODBE - REPORT</title>
		<style type="text/css">
			.auto{
				margin: 0px auto;
			}
			.text-center{
				text-align: center;
				padding: 0px;
				margin: 0px;
			}
			.text-left{
				text-align: left;
				padding: 0px;
				margin: 0px;
			}
			.text-right{
				text-align: right;
				padding: 0px;
				margin: 0px;
			}
			.outline{
				border: 1px solid #ccc;
				margin: 30px;
				padding: 0px;
			}
			.small{
				font-size: 11px;
			}
			.small2{
				font-size: 10px;
			}
			.small3{
				font-size: 9px;
			}
			.small4{
				font-size: 8px;
			}
			.small5{
				font-size: 6px;
			}
			.no-padding{
				padding: 0px;
			} 
			.no-margin{
				margin: 0px;
			}
			.border-top{
				border-top: 1px solid #000;
			}
			.border-bottom{
				border-bottom: 1px solid #000;
			}
			.border-left{
				border-left: 1px solid #cc000c;
			}
			.border-right{
				border-right: 1px solid #000;
			}
			.inline{
				display: inline-block;
			}
			table td {
				padding: 5px;
			}
			.border{
				border: 1px solid #000;
			}
					
		</style>
	</head>
	<body>
		<div style="margin-left: 25px">
			<div class="text-right">
				<b class="small3">
					FAR No. 1-A
				</b>
			</div>
			<div class="text-center">
				<b class="small3">
					STATEMENT OF APPROPRIATIONS, ALLOTMENTS, OBLIGATIONS, DISBURSMENTS AND BALANCES BY OBJECT OF EXPENDITURES*
				</b><br>
				<p class="small3">As of the Quarter ending <?php echo $to;?></p>
			</div>
		</div>
		
		<div style="margin-left: 25px">
			
			<table class="small3" width="100%" cellpadding="0" cellspacing="0">

				<tr>
					<td width="6%">
						<b>Department</b>
					</td>
					<td class="border-bottom text-center" width="20.8%"> <b><?php echo $department;?></b> </td>
					<td width="54.9%">&nbsp;</td>	
					<td class="border text-center" width="6%"><?php echo ($fund_sourse == 1) ? 'X' : '';?></td>
					<td>
						<div>Current Year Appropriations</div>
					</td>
				</tr>
			</table>


			<table class="small3" width="100%" cellpadding="0" cellspacing="0">

				<tr>
					<td width="5%">
						<b>Agency</b>
					</td>
					<td class="border-bottom text-center" width="30%"><b><?php echo $agency;?></b></td>
					<td width="46.7%">&nbsp;</td>	
					<td class="border-bottom border-left border-right text-center" width="6%"><?php echo ($fund_sourse == 2) ? 'X' : '';?></td>
					<td>
						<div>Supplemental Appropriations</div>
					</td>
				</tr>

			</table>

			<table class="small3" width="100%" cellpadding="0" cellspacing="0">

				<tr>
					<td width="18%">
						<b>Operating Unit</b>
					</td>
					<td class="border-bottom" width="20%"></td>
					<td width="43.7%">&nbsp;</td>	
					<td class="border-bottom border-left border-right text-center" width="6%"><?php echo ($fund_sourse == 3) ? 'X' : '';?></td>
					<td>
						<div>Continuing Appropriations</div>
					</td>
				</tr>

			</table>

			<table class="small3" width="100%" cellpadding="0" cellspacing="0">

				<tr>
					<td width="18%">
						<b>Organization Code (UACS)</b>
					</td>
					<td class="border-bottom text-left" width="20%"><b><?php echo $organization_code;?></b></td>
					<td width="45%">&nbsp;</td>	
					<td></td>
					<td></td>
				</tr>

			</table>

			<table class="small3" width="100%" cellpadding="0" cellspacing="0">

				<tr>
					<td width="18%">
						<b>Funding Source Code( as clustered):</b>
					</td>
					<td class="border-bottom" width="20%"></td>
					<td width="45%">&nbsp;</td>	
					<td></td>
					<td></td>
				</tr>

			</table>
			<br>

			<!-- START MAIN TABLE -->

			<table width="100%" class="border small4 text-center" cellpadding="0" cellspacing="0">

				<tr>
					<td rowspan="3" class="border-right border-bottom" width="17%">
						PARTICULARS					
					</td>
					<td rowspan="3" class="border-right border-bottom">
						UACS<br>CODE					
					</td>
					<td colspan="3" class="border-right border-bottom">
						Appropriations					
					</td>
					<td colspan="5" class="border-right border-bottom">
						Allotments					
					</td>
					<td colspan="5" class="border-right border-bottom">
						Current Year Obligations					
					</td>
					<td colspan="5" class="border-right border-bottom">
						Current Year Disbursements					
					</td>
					<td colspan="4" class=" border-bottom">
						Balances					
					</td>
				</tr>

				<tr>
					<td rowspan="2" class="border-right border-bottom">Authorized<br>Appropriations</td>
					<td rowspan="2" class="border-right border-bottom">Adjustments<br>Transfer (To)From<br>Realignment</td>
					<td rowspan="2" class="border-right border-bottom">Adusted<br>Appropriation</td>
					<td rowspan="2" class="border-right border-bottom">Allotments<br>Recieved</td>
					<td rowspan="2" class="border-right border-bottom">Adjustments<br>(withdrawal<br>Realignment)</td>
					<td rowspan="2" class="border-right border-bottom">Transfer<br>To</td>
					<td rowspan="2" class="border-right border-bottom">Transfer<br>From</td>
					<td rowspan="2" class="border-right border-bottom">Adjusted<br>Total<br>Allotments</td>
					<td rowspan="2" class="border-right border-bottom">1st Quarter<br>Ending<br>March 31</td>
					<td rowspan="2" class="border-right border-bottom">2nd Quarter<br>Ending<br>June 30</td>
					<td rowspan="2" class="border-right border-bottom">3rd Quarter<br>Ending<br>Sept 30</td>
					<td rowspan="2" class="border-right border-bottom">4th Quarter<br>Ending<br>March 31</td>
					<td rowspan="2" class="border-right border-bottom">Total</td>
					<td rowspan="2" class="border-right border-bottom">1st Quarter<br>Ending<br>March 31</td>
					<td rowspan="2" class="border-right border-bottom">2nd Quarter<br>Ending<br>June 30</td>
					<td rowspan="2" class="border-right border-bottom">3rd Quarter<br>Ending<br>Sept 30</td>
					<td rowspan="2" class="border-right border-bottom">4th Quarter<br>Ending<br>March 31</td>
					<td rowspan="2" class="border-right border-bottom">Total</td>
					<td rowspan="2" class="border-right border-bottom">Unreleased<br>Appropriation</td>
					<td rowspan="2" class="border-right border-bottom">Unobligated<br>Allotment</td>
					<td class="border-right border-bottom" colspan="2">Unpaid Obligations<br>(15-20) = (23+24)</td>
				</tr>

				<tr>
					<td class="border-right border-bottom">Due and<br>demandable</td>
					<td class="border-right border-bottom">Not Yet Due<br>Demandable</td>
				</tr>
				
				<tr>
					<td class="border-right border-bottom small5">1</td>
					<td class="border-right border-bottom small5">2</td>
					<td class="border-right border-bottom small5">3</td>
					<td class="border-right border-bottom small5">4</td>
					<td class="border-right border-bottom small5">5=(3+4)</td>
					<td class="border-right border-bottom small5">6</td>
					<td class="border-right border-bottom small5">7</td>
					<td class="border-right border-bottom small5">8</td>
					<td class="border-right border-bottom small5">9</td>
					<td class="border-right border-bottom small5">10=(6+(-)7-8+9)</td>
					<td class="border-right border-bottom small5">11</td>
					<td class="border-right border-bottom small5">12</td>
					<td class="border-right border-bottom small5">13</td>
					<td class="border-right border-bottom small5">14</td>
					<td class="border-right border-bottom small5">15=(11+12+13+14)</td>
					<td class="border-right border-bottom small5">16</td>
					<td class="border-right border-bottom small5">17</td>
					<td class="border-right border-bottom small5">18</td>
					<td class="border-right border-bottom small5">19</td>
					<td class="border-right border-bottom small5">20=(16+17+18+19)</td>
					<td class="border-right border-bottom small5">21=(5+10)</td>
					<td class="border-right border-bottom small5">22=(10-15)</td>
					<td class="border-right border-bottom small5">(15-20)<br>23</td>
					<td class="border-bottom small5">24</td>
				</tr>

				<?php 
				foreach($report as $key => $row):

					if($row['parent_account_code'] == ''){

						$total_row_3 = 0;
						$total_row_4 = 0;
						$total_row_5 = 0;
						$total_row_6 = 0;
						$total_row_7 = 0;
						$total_row_8 = 0;
						$total_row_9 = 0;
						$total_row_10 = 0;
						$total_row_11 = 0;
						$total_row_12 = 0;
						$total_row_13 = 0;
						$total_row_14 = 0;
						$total_row_15 = 0;
						$total_row_16 = 0;
						$total_row_17 = 0;
						$total_row_18 = 0;
						$total_row_19 = 0;
						$total_row_20 = 0;
						$total_row_21 = 0;
						$total_row_22 = 0;
						$total_row_23 = 0;
						$total_row_24 = 0;

						//trans var here
						$realigned_amount_trans 	= 0;
						$realigned_amount_trans_2 	= 0;
						$no5top 					= 0;
						$no10top 					= 0;
						$total_dvtop 				= 0;
						$total_obtop 				= 0;

						$realigned_amount_spec 		= 0;

						foreach($report as $keys => $rows):

							if(substr($rows['b_account_code'], 0, 3) == '501' &&  $row['account_code'] == 501)
							{
								$total_row_3 += $rows['appropriation_amount'];

								($rows['realigned_amount'] < 0) ? $realigned_amount_trans = $rows['realigned_amount'] : 0;
								$total_row_4 += $realigned_amount_trans;

								$total_row_5 = ($total_row_3 + $total_row_4);

								$total_row_6 += $rows['allotment_amount'];

								($rows['realigned_amount'] > 0) ? $realigned_amount_trans_2 = $rows['realigned_amount'] : 0;
								$total_row_7 = $realigned_amount_trans_2;


								$total_row_8 += $rows['transfer_to'];
								$total_row_9 += $rows['transfer_from'];

								$total_row_10 += ($rows['allotment_amount'] + $rows['realigned_amount'] - $rows['transfer_to'] + $rows['transfer_from']);

								$total_row_11 += $rows['obligated_amount_1'];
								$total_row_12 += $rows['obligated_amount_2'];
								$total_row_13 += $rows['obligated_amount_3'];
								$total_row_14 += $rows['obligated_amount_4'];
								$total_row_15 += ($rows['obligated_amount_1'] + $rows['obligated_amount_2'] + $rows['obligated_amount_3'] + $rows['obligated_amount_4']);
								
								$total_row_16 += $rows['disbursed_amount_1'];
								$total_row_17 += $rows['disbursed_amount_2'];
								$total_row_18 += $rows['disbursed_amount_3'];
								$total_row_19 += $rows['disbursed_amount_4'];
								$total_row_20 += ($rows['disbursed_amount_1'] + $rows['disbursed_amount_2'] + $rows['disbursed_amount_3'] + $rows['disbursed_amount_4']);
								
								$no5top 	+= ($rows['appropriation_amount'] +  $rows['transfer_from']);//5
								$no10top 	+= ($rows['allotment_amount'] + $rows['realigned_amount'] - $rows['transfer_to'] + $rows['transfer_from']);//10
								$total_row_21 = ($no5top + $no10top);

								$total_obtop  += ($rows['obligated_amount_1'] + $rows['obligated_amount_2'] + $rows['obligated_amount_3'] + $rows['obligated_amount_4']);
								$total_row_22 = ($no10top - $total_obtop);

								$total_dvtop  += ($rows['disbursed_amount_1'] + $rows['disbursed_amount_2'] + $rows['disbursed_amount_3'] + $rows['disbursed_amount_4']);
								$total_row_23 = ($total_obtop - $total_dvtop);

								$total_row_24 += 0;
							}
							if(substr($rows['b_account_code'], 0, 3) == '502' &&  $row['account_code'] == 502)
							{
								$total_row_3 += $rows['appropriation_amount'];

								($rows['realigned_amount'] < 0) ? $realigned_amount_trans = $rows['realigned_amount'] : 0;
								$total_row_4 += $realigned_amount_trans;

								$total_row_5 = ($total_row_3 + $total_row_4);

								$total_row_6 += $rows['allotment_amount'];

								($rows['realigned_amount'] > 0) ? $realigned_amount_trans_2 = $rows['realigned_amount'] : 0;
								$total_row_7 = $realigned_amount_trans_2;


								$total_row_8 += $rows['transfer_to'];
								$total_row_9 += $rows['transfer_from'];

								$total_row_10 += ($rows['allotment_amount'] + $rows['realigned_amount'] - $rows['transfer_to'] + $rows['transfer_from']);

								$total_row_11 += $rows['obligated_amount_1'];
								$total_row_12 += $rows['obligated_amount_2'];
								$total_row_13 += $rows['obligated_amount_3'];
								$total_row_14 += $rows['obligated_amount_4'];
								$total_row_15 += ($rows['obligated_amount_1'] + $rows['obligated_amount_2'] + $rows['obligated_amount_3'] + $rows['obligated_amount_4']);
								
								$total_row_16 += $rows['disbursed_amount_1'];
								$total_row_17 += $rows['disbursed_amount_2'];
								$total_row_18 += $rows['disbursed_amount_3'];
								$total_row_19 += $rows['disbursed_amount_4'];
								$total_row_20 += ($rows['disbursed_amount_1'] + $rows['disbursed_amount_2'] + $rows['disbursed_amount_3'] + $rows['disbursed_amount_4']);
								
								$no5top 	+= ($rows['appropriation_amount'] +  $rows['transfer_from']);//5
								$no10top 	+= ($rows['allotment_amount'] + $rows['realigned_amount'] - $rows['transfer_to'] + $rows['transfer_from']);//10
								$total_row_21 = ($no5top + $no10top);

								$total_obtop  += ($rows['obligated_amount_1'] + $rows['obligated_amount_2'] + $rows['obligated_amount_3'] + $rows['obligated_amount_4']);
								$total_row_22 = ($no10top - $total_obtop);

								$total_dvtop  += ($rows['disbursed_amount_1'] + $rows['disbursed_amount_2'] + $rows['disbursed_amount_3'] + $rows['disbursed_amount_4']);
								$total_row_23 = ($total_obtop - $total_dvtop);
								
								$total_row_24 += 0;
							}
							if(substr($rows['b_account_code'], 0, 3) == '503' &&  $row['account_code'] == 503)
							{
								$total_row_3 += $rows['appropriation_amount'];

								($rows['realigned_amount'] < 0) ? $realigned_amount_trans = $rows['realigned_amount'] : 0;
								$total_row_4 += $realigned_amount_trans;

								$total_row_5 = ($total_row_3 + $total_row_4);

								$total_row_6 += $rows['allotment_amount'];

								($rows['realigned_amount'] > 0) ? $realigned_amount_trans_2 = $rows['realigned_amount'] : 0;
								$total_row_7 = $realigned_amount_trans_2;


								$total_row_8 += $rows['transfer_to'];
								$total_row_9 += $rows['transfer_from'];

								$total_row_10 += ($rows['allotment_amount'] + $rows['realigned_amount'] - $rows['transfer_to'] + $rows['transfer_from']);

								$total_row_11 += $rows['obligated_amount_1'];
								$total_row_12 += $rows['obligated_amount_2'];
								$total_row_13 += $rows['obligated_amount_3'];
								$total_row_14 += $rows['obligated_amount_4'];
								$total_row_15 += ($rows['obligated_amount_1'] + $rows['obligated_amount_2'] + $rows['obligated_amount_3'] + $rows['obligated_amount_4']);
								
								$total_row_16 += $rows['disbursed_amount_1'];
								$total_row_17 += $rows['disbursed_amount_2'];
								$total_row_18 += $rows['disbursed_amount_3'];
								$total_row_19 += $rows['disbursed_amount_4'];
								$total_row_20 += ($rows['disbursed_amount_1'] + $rows['disbursed_amount_2'] + $rows['disbursed_amount_3'] + $rows['disbursed_amount_4']);
								
								$no5top 	+= ($rows['appropriation_amount'] +  $rows['transfer_from']);//5
								$no10top 	+= ($rows['allotment_amount'] + $rows['realigned_amount'] - $rows['transfer_to'] + $rows['transfer_from']);//10
								$total_row_21 = ($no5top + $no10top);

								$total_obtop  += ($rows['obligated_amount_1'] + $rows['obligated_amount_2'] + $rows['obligated_amount_3'] + $rows['obligated_amount_4']);
								$total_row_22 = ($no10top - $total_obtop);

								$total_dvtop  += ($rows['disbursed_amount_1'] + $rows['disbursed_amount_2'] + $rows['disbursed_amount_3'] + $rows['disbursed_amount_4']);
								$total_row_23 = ($total_obtop - $total_dvtop);
								
								$total_row_24 += 0;
							}

						endforeach;

					}

				?>
				<tr>	
					<!--1-->
					<td style="padding-bottom: 0px;padding-top: 5px;margin: 0px;<?php echo ($row['parent_account_code'] == '') ? 'font-weight: bold' : '';?>" class="border-right text-left" valign="bottom">

						<?php 

							echo ($row['parent_account_code'] != '') ? '&nbsp;&nbsp;&nbsp;&nbsp;' : '';
							echo $row['account_name']

						?>
							
					</td>	
					<!--2-->
					<td style="padding-bottom: 0px;padding-top: 0px;margin: 0px" class="border-right text-left" valign="bottom">

						<?php echo $row['b_account_code'];?>
							
					</td>	
					<!--3-->
					<td style="padding: 0px;padding-right: 5px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_3) != 0) ? number_format($total_row_3).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_3) != 0) ? number_format($total_row_3).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_3) != 0) ? number_format($total_row_3).'<br>' : '';

							echo (number_format($row['appropriation_amount']) != 0) ? number_format($row['appropriation_amount']) : '';

						?>
							
					</td>	
					<!--4-->
					<td style="padding: 0px;padding-right: 5px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_4) != 0) ? number_format($total_row_4).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_4) != 0) ? number_format($total_row_4).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_4) != 0) ? number_format($total_row_4).'<br>' : '';

							echo (number_format($row['realigned_amount']) != 0) ? ($row['realigned_amount'] < 0) ? number_format($row['realigned_amount']).'<br>' : '' : '';

						?>

					</td>	
					<!--5-->
					<td style="padding: 0px;padding-right: 5px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_5) != 0) ? number_format($total_row_5).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_5) != 0) ? number_format($total_row_5).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_5) != 0) ? number_format($total_row_5).'<br>' : '';

							($row['realigned_amount'] < 0) ? $realigned_amount_spec = $row['realigned_amount'] : 0; 

							echo (number_format($row['appropriation_amount']) != 0) ? number_format(($row['appropriation_amount'] + $realigned_amount_spec)).'<br>' : '';
						?>

					</td>	
					<!--6-->
					<td style="padding: 0px;padding-right: 5px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_6) != 0) ? number_format($total_row_6).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_6) != 0) ? number_format($total_row_6).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_6) != 0) ? number_format($total_row_6).'<br>' : '';

							echo (number_format($row['allotment_amount']) != 0) ? number_format($row['allotment_amount']).'<br>' : '';

						?>

					</td>	
					<!--7-->
					<td style="padding: 0px;padding-right: 5px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_7) != 0) ? number_format($total_row_7).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_7) != 0) ? number_format($total_row_7).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_7) != 0) ? number_format($total_row_7).'<br>' : '';
						
							echo (number_format($row['realigned_amount']) != 0) ? ($row['realigned_amount'] > 0) ? number_format($row['realigned_amount']).'<br>' : '' : '';

						?>

					</td>	
					<!--8-->
					<td style="padding: 0px;padding-right: 5px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_8) != 0) ? number_format($total_row_8).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_8) != 0) ? number_format($total_row_8).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_8) != 0) ? number_format($total_row_8).'<br>' : '';

							echo (number_format($row['transfer_to']) != 0) ? number_format($row['transfer_to']).'<br>' : '';

						?>

					</td>	
					<!--9-->
					<td style="padding: 0px;padding-right: 5px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_9) != 0) ? number_format($total_row_9).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_9) != 0) ? number_format($total_row_9).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_9) != 0) ? number_format($total_row_9).'<br>' : '';

							echo (number_format($row['transfer_from']) != 0) ? number_format($row['transfer_from']).'<br>' : '';

						?>

					</td>	
					<!--10-->
					<td style="padding: 0px;padding-right: 5px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_10) != 0) ? number_format($total_row_10).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_10) != 0) ? number_format($total_row_10).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_10) != 0) ? number_format($total_row_10).'<br>' : '';

							$no6 = ($row['allotment_amount'] + $row['realigned_amount'] - $row['transfer_to'] + $row['transfer_from']);
							echo (number_format($no6) != 0) ? number_format($no6).'<br>' : '';

						?>

					</td>	
					<!--11-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_11) != 0) ? number_format($total_row_11).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_11) != 0) ? number_format($total_row_11).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_11) != 0) ? number_format($total_row_11).'<br>' : '';

							echo (number_format($row['obligated_amount_1']) != '0') ? number_format($row['obligated_amount_1']) : '';

						?>
						
					</td>
					<!--12-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_12) != 0) ? number_format($total_row_12).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_12) != 0) ? number_format($total_row_12).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_12) != 0) ? number_format($total_row_12).'<br>' : '';

							echo (number_format($row['obligated_amount_2']) != '0') ? number_format($row['obligated_amount_2']) : '';

						?>
						
					</td>
					<!--13-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_13) != 0) ? number_format($total_row_13).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_13) != 0) ? number_format($total_row_13).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_13) != 0) ? number_format($total_row_13).'<br>' : '';

							echo (number_format($row['obligated_amount_3']) != '0') ? number_format($row['obligated_amount_3']) : '';

						?>

					</td>
					<!--14-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_14) != 0) ? number_format($total_row_14).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_14) != 0) ? number_format($total_row_14).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_14) != 0) ? number_format($total_row_14).'<br>' : '';

							echo (number_format($row['obligated_amount_4']) != '0') ? number_format($row['obligated_amount_4']) : '';

						?>

					</td>
					<!--15-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">
						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_15) != 0) ? number_format($total_row_15).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_15) != 0) ? number_format($total_row_15).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_15) != 0) ? number_format($total_row_15).'<br>' : '';

							$total_ob 	= ($row['obligated_amount_1'] + $row['obligated_amount_2'] + $row['obligated_amount_3'] + $row['obligated_amount_4']);
							
							echo (number_format($total_ob) != 0) ? number_format($total_ob).'<br>' : '';
						?> 
					</td>
					<!--16-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_16) != 0) ? number_format($total_row_16).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_16) != 0) ? number_format($total_row_16).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_16) != 0) ? number_format($total_row_16).'<br>' : '';

							echo (number_format($row['disbursed_amount_1']) != '0') ? number_format($row['disbursed_amount_1']) : '';

						?>

					</td>
					<!--17-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

						<?php

							if($row['account_code'] == 501)
								echo (number_format($total_row_17) != 0) ? number_format($total_row_17).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_17) != 0) ? number_format($total_row_17).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_17) != 0) ? number_format($total_row_17).'<br>' : '';

							echo (number_format($row['disbursed_amount_2']) != '0') ? number_format($row['disbursed_amount_2']) : '';

						?>

					</td>
					<!--18-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

						<?php

							if($row['account_code'] == 501)
								echo (number_format($total_row_18) != 0) ? number_format($total_row_18).'<br>' : ''; 
							if($row['account_code'] == 502)
								echo (number_format($total_row_18) != 0) ? number_format($total_row_18).'<br>' : ''; 
							if($row['account_code'] == 503)
								echo (number_format($total_row_18) != 0) ? number_format($total_row_18).'<br>' : ''; 

							echo (number_format($row['disbursed_amount_3']) != '0') ? number_format($row['disbursed_amount_3']) : '';

						?>

					</td>
					<!--19-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_19) != 0) ? number_format($total_row_19).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_19) != 0) ? number_format($total_row_19).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_19) != 0) ? number_format($total_row_19).'<br>' : '';

							echo (number_format($row['disbursed_amount_4']) != '0') ? number_format($row['disbursed_amount_4']) : '';

						?>

					</td>
					<!--20-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">
						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_20) != 0) ? number_format($total_row_20).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_20) != 0) ? number_format($total_row_20).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_20) != 0) ? number_format($total_row_20).'<br>' : '';

							$total_dv 	= ($row['disbursed_amount_1'] + $row['disbursed_amount_2'] + $row['disbursed_amount_3'] + $row['disbursed_amount_4']);
							echo (number_format($total_dv) != 0) ? number_format($total_dv) : '';

						?> 
					</td>
					<!--21-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">
						<?php

							if($row['account_code'] == 501)
								echo (number_format($total_row_21) != 0) ? number_format($total_row_21).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_21) != 0) ? number_format($total_row_21).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_21) != 0) ? number_format($total_row_21).'<br>' : '';

							//5
							$no5 = ($row['appropriation_amount'] +  $row['transfer_from']);
							//10
							$no10 = ($row['allotment_amount'] + $row['realigned_amount'] - $row['transfer_to'] + $row['transfer_from']);

							echo (number_format($no5 + $no10) != 0) ? number_format($no5 + $no10).'<br>' : '';

						?> 
					</td>
					<!--22-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">
						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_22) != 0) ? number_format($total_row_22).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_22) != 0) ? number_format($total_row_22).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_22) != 0) ? number_format($total_row_22).'<br>' : '';

							echo (number_format($no10 - $total_ob) != 0) ? number_format($no10 - $total_ob) : '';
						?> 
					</td>
					<!--23-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">
						<?php 

							if($row['account_code'] == 501)
								echo (number_format($total_row_23) != 0) ? number_format($total_row_23).'<br>' : '';
							if($row['account_code'] == 502)
								echo (number_format($total_row_23) != 0) ? number_format($total_row_23).'<br>' : '';
							if($row['account_code'] == 503)
								echo (number_format($total_row_23) != 0) ? number_format($total_row_23).'<br>' : '';

							echo (number_format($total_ob - $total_dv) != 0) ? number_format($total_ob - $total_dv) : '';
						
						?> 
					</td>
					<!--24-->
					<td style="padding: 0px;padding-right: 3px;margin: 0px;" class="border-right text-right <?php echo ($row['parent_account_code'] == '') ? 'border-bottom' : '';?>" valign="bottom">

					</td>
				</tr>
				<?php endforeach;?>
			</table>
			
		</div>
	</body>
</html>