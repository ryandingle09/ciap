<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="<?php echo base_url();?>bas/bas_checks" class="active">Checks</a></li>
  </ul>
  <div class="row m-b-n m-r-n">
	<div class="col s6 p-r-n">
	  <h5>Checks
		<span>Manage Checks.</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_add" id="add" name="add" onclick="modal_add_init()">Add New Check</button>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable">&nbsp;</div>

<div>

	<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="tbl_check">
		<thead>
			<tr>
				<th>DV NO</th>
				<th>DV DATE</th>
				<th>DISBURSEMENT AMOUNT</ht>
				<th>CHECK/ADA NO.</th>
				<th>CHECK/ADA DATE</th>
				<th>CHECK/ADA AMOUNT</ht>
				<th>STATUS</ht>
				<th class="center-align" width="10%">ACTION</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>

</div>

<script type="text/javascript">
 var deleteObj = new handleData({ 
 		controller : 'bas_checks', 
 		method : 'delete_check', 
 		module: '<?php echo PROJECT_BAS ?>' 
 });
</script>