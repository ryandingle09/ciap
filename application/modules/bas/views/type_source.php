<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="<?php echo base_url();?>bas/bas_type_source" class="active">Type of Source</a></li>
  </ul>
  <div class="row m-b-n m-r-n">
	<div class="col s6 p-r-n">
	  <h5>Types of Source
		<span>Manage Type of Sources.</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal" id="add" name="add" onclick="modal_init()">Add New Type of Source</button>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable">&nbsp;</div>

<div>

	<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="tbl_source">
		<thead>
			<tr>
				<th>Type Of Source</th>
				<th width="10%">STATUS</th>
				<th width="10%">ACTION</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>

</div>

<script type="text/javascript">
 var deleteObj = new handleData({ 
	 	controller : 'bas_type_source', 
	 	method : 'delete_source', 
	 	module: '<?php echo PROJECT_BAS ?>' 
	 });
</script>