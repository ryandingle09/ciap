<?php if(empty($line_items)): ?>

<tr style="border-bottom: 1px solid #ccc" class="rows" id="row_<?php echo $count;?>">
	<td width="40%">
		<div class="row p-t-sm">
			<div class="col s4">
				<input type="text" name="code_<?php echo $count;?>" id="code" class="code_<?php echo $count;?> all<?php echo $count;?> validate" placeholder="Code" required="" aria-required="true" autocomplete="off">
			</div>
			<div class="col s8">
				<input type="text" name="line_item_<?php echo $count;?>" class="line_item_<?php echo $count;?> all<?php echo $count;?> validate" placeholder="Line Item" required="" aria-required="true" autocomplete="off">
			</div>
		</div>
		<div class="row" style="padding-left: 12px;padding-top: 0px;margin-top: -50px">
			<div class="input-field" style="padding: 0px;margin: 0px;margin-left: -10px;">			    
				<input type="checkbox" <?php if(empty($parent)) echo 'checked="checked"';?> class="is_parent_level" name="is_parent_level_<?php echo $count;?>" id="is_parent_level_<?php echo $count;?>" data-count="<?php echo $count;?>" value="1" />
			    <label for="is_parent_level_<?php echo $count;?>">Parent Level</label>
		    </div>
			<p class="parent_seperator_<?php echo $count;?>" style="font-size: 18px;margin-top: 50px"><?php if(!empty($parent)) echo 'Parent is <b>'.$parent.'</b>';?>&nbsp;</p>
			<input type="hidden" name="parent_line_item_<?php echo $count;?>" id="parent_line_item_<?php echo $count;?>" value="">
		</div>
	</td>
	<td>
		<?php
			$reader 	= '';
			$required 	= 'required=""';
			$valid 		= 'validate';

			if($count == 1 || empty($parent))
			{
				$readonly 	= 'readonly="readonly"';
				$required  	= '';
				$valid 		= '';
			}
		?>
		<input type="text" class="all_<?php echo $count;?> <?php echo $valid;?>" <?php echo $required;?> aria-required="true" 
			autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
	    	<?php echo $readonly;?>
			name="ps_appropriation_amount_<?php echo $count;?>" placeholder="Enter PS Amount" style="text-align: right">
		<input type="text" class="all_<?php echo $count;?> <?php echo $valid;?>" <?php echo $required;?> aria-required="true" 
			autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
	    	<?php echo $readonly;?>
			name="mooe_appropriation_amount_<?php echo $count;?>" class="all_<?php echo $count;?>" placeholder="Enter MOOE Amount" style="text-align: right">
		<input type="text" class="all_<?php echo $count;?> <?php echo $valid;?>" <?php echo $required;?> aria-required="true" 
			autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
	    	<?php echo $readonly;?>
			name="coo_appropriation_amount_<?php echo $count;?>" placeholder="Enter CO Amount" style="text-align: right">
	</td>
	<td>
		<input type="text" class="all_<?php echo $count;?> <?php echo $valid;?>" <?php echo $required;?> aria-required="true" 
			autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
	    	<?php echo $readonly;?>
			name="ps_allotment_amount_<?php echo $count;?>" placeholder="Enter PS Amount" style="text-align: right">
		<input type="text" class="all_<?php echo $count;?> <?php echo $valid;?>" <?php echo $required;?> aria-required="true" 
			autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
	    	<?php echo $readonly;?>
			name="mooe_allotment_amount_<?php echo $count;?>" placeholder="Enter MOOE Amount" style="text-align: right">
		<input type="text" class="all_<?php echo $count;?> <?php echo $valid;?>" <?php echo $required;?> aria-required="true"
	    	<?php echo $readonly;?> autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
			name="coo_allotment_amount_<?php echo $count;?>" placeholder="Enter CO Amount" style="text-align: right">
	</td>
	<td width="15%" style="text-align: center;">
		<div class="table-actions" style="margin-bottom: 90px">
			<a style="cursor:pointer" id="add_sub_item" data-action="false" data-count="<?php echo $count;?>" class="add_sub_item_<?php echo $count;?> add_sub tooltipped add_sub_item_<?php echo $count;?>" data-tooltip='Add sub line item' data-position='bottom' data-delay='50'><i class="flaticon-add175"></i></a>
				&nbsp;
			<?php if($count != 1):?>
				<a style="cursor:pointer" id="delete_sub_item_<?php echo $count;?>" data-id="" data-action="" data-count="<?php echo $count;?>" class="del_sub tbl_delete tooltipped" data-tooltip='Delete' data-position='bottom' data-delay='50' ><i class="flaticon-cross95"></i></a>
			<?php endif;?>
		</div>
		<button type="button" id="save_one" data-id="" data-action="<?php echo sha1('insert_data_'.$count.'');?>" data-count="<?php echo $count;?>" class="btn waves-effect waves-light btn-success save_one_<?php echo $count;?>" value="Saving" type="button">Save</button>
	</td>
</tr>

<?php else:?>	
<?php $key= 0; foreach($line_items as $keys => $row): $key++ ?>
<tr style="border-bottom: 1px solid #ccc" class="rows" id="row_<?php echo $key;?>">
	<td width="40%">
		<div class="row p-t-sm">
			<div class="col s4">
				<input type="text" name="code_<?php echo $key;?>" class="code_<?php echo $key;?> all<?php echo $key;?> validate" value="<?php echo $row['line_item_code'];?>" validate" placeholder="Code" required="" aria-required="true" autocomplete="off">
			</div>
			<div class="col s8">
				<input type="text" name="line_item_<?php echo $key;?>" class="line_item_<?php echo $key;?> all<?php echo $key;?> validate" value="<?php echo $row['line_item_name'];?>" placeholder="Line Item" required="" aria-required="true" autocomplete="off">
			</div>
		</div>
		<div class="row" style="padding-left: 12px;padding-top: 0px;margin-top: -50px">
			<div class="input-field" style="padding: 0px;margin: 0px;margin-left: -10px;">			    
				<input type="checkbox" 
				<?php 
				if(empty($row['parent_line_item_id']) || floatval($row['ps_appropriation']) == 0 
				&& floatval($row['co_appropriation']) == 0 && floatval($row['mooe_appropriation']) == 0
				&& floatval($row['ps_allotment']) == 0 
				&& floatval($row['co_allotment']) == 0 && floatval($row['mooe_allotment']) == 0
				) 
				echo 'checked="checked"  onclick="return false"';
				?> 
				class="is_parent_level_false" name="is_parent_level_<?php echo $key;?>" 
				id="is_parent_level_<?php echo $key;?>" 
				data-count="<?php echo $key;?>" value="1" />
			    <label for="is_parent_level_<?php echo $key;?>">Parent Level</label>
		    </div>
			<p class="parent_seperator_<?php echo $count;?>" style="font-size: 18px;margin-top: 55px">
				<?php 
				foreach($line_items as $keyss => $row2):
				if($row2['line_item_id'] == $row['parent_line_item_id']) echo 'Parent is <b>'.$row2['line_item_code'].'</b>';
				endforeach;
				?>
				&nbsp;
			</p>
			<input type="hidden" name="parent_line_item_<?php echo $key;?>" id="parent_line_item_<?php echo $key;?>" value="<?php echo $row['parent_line_item_id'];?>">
		</div>
	</td>
	<td>
		<input type="text" class="all_<?php echo $key;?> validate" aria-required="true" 
			<?php if(floatval($row['ps_appropriation']) == 0) echo 'readonly="readonly"';else echo 'value="'.number_format(floatval($row['ps_appropriation'])).'"';?> autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
			name="ps_appropriation_amount_<?php echo $key;?>" placeholder="Enter PS Amount" style="text-align: right">
		<input type="text" class="all_<?php echo $key;?> validate" aria-required="true" 
			<?php if(floatval($row['mooe_appropriation']) == 0) echo 'readonly="readonly"';else echo 'value="'.number_format(floatval($row['mooe_appropriation'])).'"';?>
	    	autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
			name="mooe_appropriation_amount_<?php echo $key;?>" class="all_<?php echo $key;?>" placeholder="Enter MOOE Amount" style="text-align: right">
		<input type="text" class="all_<?php echo $key;?> validate" aria-required="true" 
			<?php if(floatval($row['co_appropriation']) == 0) echo 'readonly="readonly"';else echo 'value="'.number_format(floatval($row['co_appropriation'])).'"';?>
	    	autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
			name="coo_appropriation_amount_<?php echo $key;?>" placeholder="Enter CO Amount" style="text-align: right">
	</td>
	<td>
		<input type="text" class="all_<?php echo $key;?> validate" aria-required="true" 
			<?php if(floatval($row['ps_allotment']) == 0) echo 'readonly="readonly"';else echo 'value="'.number_format(floatval($row['ps_allotment'])).'"';?>
	    	autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
			name="ps_allotment_amount_<?php echo $key;?>" placeholder="Enter PS Amount" style="text-align: right">
		<input type="text" class="all_<?php echo $key;?> validate" aria-required="true" 
			<?php if(floatval($row['mooe_allotment']) == 0) echo 'readonly="readonly"';else echo 'value="'.number_format(floatval($row['mooe_allotment'])).'"';?>
	    	autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
			name="mooe_allotment_amount_<?php echo $key;?>" placeholder="Enter MOOE Amount" style="text-align: right">
		<input type="text" class="all_<?php echo $key;?> validate" aria-required="true" 
			<?php if(floatval($row['co_allotment']) == 0) echo 'readonly="readonly"';else echo 'value="'.number_format(floatval($row['co_allotment'])).'"';?>
	    	autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
			name="coo_allotment_amount_<?php echo $key;?>" placeholder="Enter CO Amount" style="text-align: right">
	</td>
	<td width="15%" style="text-align: center;">
		<div class="table-actions" style="margin-bottom: 90px">
			<a style="cursor:pointer" id="add_sub_item" data-action="true" data-count="<?php echo $key;?>"d data-id="<?php echo $row['line_item_id'];?>" class="add_sub tooltipped add_sub_item_<?php echo $key;?>" data-tooltip='Add sub line item' data-position='bottom' data-delay='50'><i class="flaticon-add175"></i></a>
				&nbsp;
			<a style="cursor:pointer" id="delete_sub_item_<?php echo $key;?>" data-id="<?php echo $row['line_item_id'];?>" data-count="<?php echo $key;?>" data-action="<?php echo sha1('delete_data_'.$key.'_'.$row['line_item_id'].'');?>" class="del_sub tbl_delete tooltipped" data-tooltip='Delete' data-position='bottom' data-delay='50' ><i class="flaticon-cross95"></i></a>
		</div>

		<button type="button" id="update_one" data-action="<?php echo sha1('update_data_'.$row['line_item_id'].'_'.$key.'');?>" data-count="<?php echo $key;?>" data-id="<?php echo $row['line_item_id'];?>" class="btn waves-effect waves-light btn-success update_one_<?php echo $key;?>" value="Saving" type="button">Update</button>
	</td>
</tr>

<?php endforeach;?>

<?php endif;?>