<?php if($view == 'account_code'):?>
<tr id="row_<?php echo $count;?>">
	<td>
		<select class="selectize validate" required="" aria-required="true"  
		name="account_code[]" id="account_code_1">
			<option value="">Select accout code</option>
			<?php foreach($account_codes as $key => $row):?>
			<option value="<?php echo $row['account_code'];?>"><?php echo $row['account_code'];?>  -  <?php echo $row['account_name'];?></option>
			<?php endforeach;?>
		</select>
	</td>
	<td width="25%">
		<input name="appropriation_amount[]" class="right-align validate" 
		required="" aria-required="true" type="text" autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
		placeholder="Enter appropriation amount" />
	</td>
	<td width="25%">
		<input name="allotment_amount[]" class="right-align validate" 
		required="" aria-required="true" type="text" autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
		placeholder="Enter allotment amount" />
	</td>
	<td>
		<div class="table-actions">
			<a style="cursor:pointer" data-count="<?php echo $count;?>" id="delete_row" data-identifier="" data-id="deny" class="delete"></a>
		</div>
	</td>
</tr>

<?php elseif($view == 'office'):?>
<tr id="row_<?php echo $count;?>">
	<td width="30%">
		<select class="selectize validate" name="office[]" id="office_code" required="" aria-required="true">
			<option value="">Select Office</option>
			<?php foreach($line_item_office as $key => $row2):?>
			<option value="<?php echo $row2['office_num'];?>"><?php echo $row2['office_name'];?></option>
			<?php endforeach;?>
		</select>
	</td>
	<td width="30%">
		<input class="right-align validate" name="appropriation_amount[]" required="" aria-required="true"
		type="text" autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);"
		placeholder="Enter amount" />
	</td>
	<td width="30%">
		<input class="right-align validate" name="allotment_amount[]" required="" aria-required="true"
		type="text" autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
		placeholder="Enter amount" />
	</td>
	<td>
		<div class="table-actions">
			<a style="cursor:pointer" data-count="<?php echo $count;?>" id="delete_row" data-identifier="" data-id="deny" class="delete"></a>
		</div>
	</td>
</tr>

<?php elseif($view == 'release'):?>

<tr id="row_<?php echo $count;?>">
	<td width="70%">
		<select class="selectize validate" required="" aria-required="true" 
		name="account_code[]" id="account_code">
			<option value="">Select accout code</option>
			<?php foreach($all_account_codes as $key => $row2):?>
			<option value="<?php echo $row2['account_code'];?>"><?php echo $row2['account_code'];?>  -  <?php echo $row2['account_name'];?></option>
			<?php endforeach;?>
		</select>
	</td>
	<td width="30%">
		<input class="right-align validate" aria-required="true" required="" placeholder="Enter allotment amount" name="allotment_amount[]" type="text" autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
		/>
	</td>
	<td class="center-align">
		<div class="table-actions">
			<a style="cursor:pointer" data-count="<?php echo $count;?>" data-id="deny" id="delete_row" data-identifier="" class="delete"></a>
		</div>
	</td>
</tr>

<?php endif; ?>