<?php foreach($data as $key => $row):?>
<tr style="border: 1px solid #ccc">
	<td style="border: 1px solid #ccc" width="30%">
		<?php foreach($data as $keys => $rows):?>
		<?php if($row['parent_line_item_id'] == $rows['line_item_id']):?>
			&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
		<?php endif;?>
		<?php endforeach;?>
		<b><?php echo $row['line_item_code'];?>.&nbsp;<?php echo $row['line_item_name'];?></b>
	</td>
	<td style="border: 1px solid #ccc" class="right-align"><right><?php if(floatval($row['ps_appropriation']) != 0) echo number_format($row['ps_appropriation']) ;?></right></td>
	<td style="border: 1px solid #ccc" class="right-align"><right><?php if(floatval($row['mooe_appropriation']) != 0) echo number_format($row['mooe_appropriation']);?></right></td>
	<td style="border: 1px solid #ccc" class="right-align"><right><?php if(floatval($row['co_appropriation']) != 0) echo number_format($row['co_appropriation']);?></right></td>
	<td style="border: 1px solid #ccc" class="right-align"><right><?php if(floatval($row['ps_allotment']) != 0) echo number_format($row['ps_allotment']);?></right></td>
	<td style="border: 1px solid #ccc" class="right-align"><right><?php if(floatval($row['mooe_allotment']) != 0) echo number_format($row['mooe_allotment']);?></right></td>
	<td style="border: 1px solid #ccc" class="right-align"><right><?php if(floatval($row['co_allotment']) != 0) echo number_format($row['co_allotment']);?></right></td>
	<td class="right-align">
		<?php if(/*$row['parent_line_item_id'] != NULL || !empty($row['parent_line_item_id']) ||*/floatval($row['ps_appropriation']) != 0 || floatval($row['mooe_appropriation']) != 0 || floatval($row['co_appropriation']) != 0): ?>
		<div class="table-actions">
			<a style="cursor:pointer;" 
			class="tooltipped md-trigger" 
			data-tooltip="Add account code" 
			data-position="bottom" data-delay="50" 
			data-modal="modal_account_code" 
			onclick='modal_account_code_init("<?php echo '?id='.$row['line_item_id'];?>")'>
			<i class="flaticon-code41"></i></a>
			&nbsp;

			<a style="cursor:pointer;" 
			class="tooltipped md-trigger" 
			data-tooltip="Add Office" 
			data-position="bottom" data-delay="50" 
			data-modal="modal_office" 
			onclick='modal_office_init("<?php echo '?id='.$row['line_item_id'];?>")'>
			<i class="flaticon-building71"></i></a>
			&nbsp;

			<a style="cursor:pointer;" 
			class="tooltipped md-trigger" 
			data-tooltip="Add Allotment Release" 
			data-position="bottom" data-delay="50" 
			data-modal="modal_allotment_releases" 
			onclick='modal_allotment_releases_init("<?php echo '?id='.$row['line_item_id'];?>")'>
			<i class="flaticon-hierarchy9"></i></a>
			&nbsp;
		</div>
		<?php endif; ?>
	</td>
</tr>
<?php endforeach;?>