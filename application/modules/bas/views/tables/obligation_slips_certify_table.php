<div class="row">
	<?php foreach($obligations as $key => $val):?>
	<input type="hidden" name="obligation_id" value="<?php echo $val['obligation_id'];?>" >
	<table width="100%">
		<tr>
			<td width="30%">
				OBLIGATION SLIP NO.<br>
				<p>(Upon Processing)</p>
			</td>
			<td colspan="2">
				OBLIGATION SLIP DATE<br>
				<p>(Upon Processing)</p>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				DATE OF REQUEST<br>
				<p class="requested_date">
				<?php
				$dateTime = new DateTime($val['requested_date']);
				echo $dateTime->format("d F Y");
				?>
				</p>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				PAYEE<br>
				<p class="payee"><?php echo $val['payee_name'];?></p>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				ADDRESS<br>
				<p class="payee_address"><?php echo $val['payee_address'];?></p>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				PARTICULARS<br>
				<p class="particulars"><?php echo $val['particulars'];?></p>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				FUND SOURCE<br>
				<p class="fund_source"><?php echo $val['source_type_name'];?></p>
			</td>
		</tr>
	</table>
	<?php endforeach; ?>

	<table class="bordered responsive-table" style="border-top: 1px solid #ddd">
		<thead class="cyan lighten-5">
			<tr>
				<th data-field="id">LINE ITEM</th>
				<th data-field="name">ACCOUNT CODE</th>
				<th data-field="price">REQUESTED AMOUNT</th>
				<th data-field="price">OBLIGATED AMOUNT</th>
			</tr>
		</thead>
		<tbody class="light-green lighten-5">
			<?php $total = 0;foreach($obligation_charges as $key => $val):?>
			<tr>
				<td>
					<input type="hidden" name="line_item_id[]" value="<?php echo $val['line_item_id'];?>">
					<?php echo $val['line_item_code'];?>
				</td>
				<td>
					<input type="hidden" name="account_code[]" value="<?php echo $val['account_code'];?>">
					<?php echo $val['account_name'];?>
					
				</td>
				<td>
					<?php echo number_format($val['requested_amount'], 2);?>
					
				</td>
				<td>
					<div class="col s12">
						<div class="input-field">
							<input 
							type="text" 
							class="validate" 
							required="" aria=required="true" 
							name="obligated_amount[]" 
							id="release_date" 
							style="text-align: right"
							min="0" autocomplete="off"
					    	data-parsley-validation-threshold="1" data-parsley-trigger="keyup" 
					    	data-parsley-type="number" 
					    	placeholder="Enter Amount" 
							value=""/>
						</div>
					</div>
				</td>
			</tr>
			<?php $total += $val['requested_amount'] ;endforeach; ?>
			<tr>
				<td></td>
				<td>TOTAL</td>
				<td><?php echo number_format(floatval($total), 2);?></td>
				<td><p class="show_type_total"></p></td>
			</tr>
		</tbody>
	</table>

	<table>
		<tr>
			<td colspan="3" width="30%">
				<div class="input-field">			    
					<input type="checkbox" class="manual_flag" name="manual_flag" id="manual_flag" value="1" />
				    <label for="manual_flag">Manual Encoding ( ? )</label>
		   		</div>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<div class="col s3">
					<div class="input-field">
						<?php
					  	$date = new DateTime($obligated_date);
						$newdate = $date->format('Y/m/d'); // 2012-07-31
					  	?>
					    <input type="text" class="datepicker validate" required="" name="obligated_date" id="obligated_date" value="<?php if(!empty($obligated_date)) echo $newdate;?>"/>
					    <label for="obligated_date">OBLIGATED DATE</label>
					</div>
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="3">
				<div class="input-field col s5">
					<input 
					type="text" 
					class="validate" 
					required="" aria=required="true" 
					name="obligated_no" id="obligation_no" placeholder="Enter Obligation No." 
					value=""/>
					<label for="obligation_no">OBLIGATION NO</label>
				</div>
			</td>
		</tr>
	</table>
</div>