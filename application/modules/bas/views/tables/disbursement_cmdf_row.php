<tr id="row_<?php echo $count;?>" class="row_<?php echo $count;?>">
	<td class="p-t-n">
		<!--<input type="hidden" name="old_account_code[]" id="old_account_code_<?php echo $count;?>" class="old_account_code_<?php echo $count;?>" value="">-->
		<div class="input-field">
			<select class="selectize validate" required="" aria-required="true" 
			name="account_code[]" id="account_code_<?php echo $count;?>" class="account_code_<?php echo $count;?>">
				<option value=""> Select Account code </option>
				<?php foreach($account_codes as $keys => $row2): ?>
				<option value="<?php echo $row2['account_code'];?>" ><?php echo $row2['account_code'].'&nbsp; - &nbsp;'.$row2['account_name'];?>
				</option>
				<?php endforeach;?>
			</select>
		</div>
	</td>
	<td class="p-t-n">
		<div class="input-field">
			<input type="text" 
			name="amount[]" 
			placeholder="Enter amount" 
			onkeyup="javascript:this.value=CommaCustom(this.value, <?php echo $count;?>);"
			autocomplete="off"
			aria-required="true" 
			required="" 
			class="validate right-align amount_<?php echo $count;?>" 
			id="amount_<?php echo $count;?>">
		</div>
	</td>
	<td>
		<a href='javascript:void(0)' class='tooltip md-trigger tooltipped ppp tax_link_<?php echo $count;?>' id="tax_link_<?php echo $count;?>" data-count="<?php echo $count;?>" data-tooltip='Tax amount' data-position='bottom' data-delay='50' data-modal='modal_tax' onclick="modal_tax_init('<?php echo $security;?>/tax/cmfd/')">[ 0 ]</a>

		<input type="hidden" name="tax_amount[]" value="apir" id="tax_amount_<?php echo $count;?>">
		<input type="hidden" name="base_tax_amount[]" value="" id="base_tax_amount_<?php echo $count;?>">
		<input type="hidden" name="ewt_rate[]" value="" id="ewt_rate_<?php echo $count;?>">
		<input type="hidden" name="ewt_amount[]" value="" id="ewt_amount_<?php echo $count;?>">
		<input type="hidden" name="vat_rate[]" value="" id="vat_rate_<?php echo $count;?>">
		<input type="hidden" name="vat_amount[]" value="" id="vat_amount_<?php echo $count;?>">
	</td>
	<td>
		<div class="table-actions">
			<a style="cursor:pointer" data-count="<?php echo $count;?>" id="delete_row" data-identifier="account_code" data-id="" class="delete"></a>
		</div>
	</td>
</tr>