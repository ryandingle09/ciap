<tr id="row_<?php echo $count;?>" class="row_<?php echo $count;?>">
	<td>
		<select aria-required="true" name="deduction_type[]"  id="deduction_type" required="" class="selectize if">
			<option value="">Deduction Type</option>
			<?php foreach ($deductions as $key => $value) { ?>
				<?php echo ($value['deduction_code'] != 'DTA') ? '<option value="'.$value['deduction_code'].'">'.$value['deduction_name'].'</option>' : ''?>
			<?php } ?>
		</select>
	</td>
	<td>
		<input type="text" 
		name="amount[]" 
		placeholder="Enter amount" 
		onkeyup="javascript:this.value=CommaCustom(this.value, <?php echo $count;?>);"
		autocomplete="off"
		aria-required="true" 
		required="" 
		class="validate right-align amount_<?php echo $count;?>" 
		id="amount_<?php echo $count;?>">
	</td>
	<td>
		<div class="table-actions">
			<a style="cursor:pointer" data-count="<?php echo $count;?>" id="delete_row" data-identifier="deduction" data-id="" class="delete"></a>
		</div>
	</td>
</tr>