<?php if($view == 'line_item'): ?>
<tr id="row_<?php echo $count;?>">
	<td class="p-l-sm">
		<div class="form-basic">

			<div class="input-field col s6">
				<select name="line_item[]" id="line_item_<?php echo $count;?>" class="selectize getCodesAndOffices validate" required="" data-target1="account_code_<?php echo $count;?>" data-target2="office_<?php echo $count;?>">
					<option value="" disabled selected>SELECT</option>
					<?php foreach($line_items as $key => $val): ?>
					<option value="<?php echo $val['line_item_id'];?>"><?php echo $val['line_item_name'];?></option>
					<?php endforeach;?>
				</select>
			</div>

		</div>
	</td>
	<td class="p-l-sm">
		<div class="form-basic">

			<div class="input-field col s6">
				<select name="account_code[]" id="account_code_<?php echo $count;?>" class="selectize validate" required="">
					<option value="" disabled selected>SELECT</option>
				</select>
			</div>

		</div>
	</td>
	<td class="p-l-sm">
		<div class="form-basic">

			<div class="input-field col s6">
				<select name="office[]" id="office_<?php echo $count;?>" class="selectize validate" required="">
					<option value="" disabled selected>SELECT</option>
				</select>
			</div>

		</div>
	</td>
	<td width="20%" class="p-l-sm">
		<div class="form-basic">
			<div class="input-field col s12">
				<input 
				type="text" 
				class="validate" 
				required="" aria=required="true" 
				name="requested_amount[]" 
				id="release_date" 
				style="text-align: right"
				autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);"
		    	placeholder="Enter Amount" 
				value=""/>
			</div>
		</div>
	</td>
	<td>
		<div class="table-actions">
			<a style="cursor:pointer" data-tooltip="Delete" data-identity="line_item" data-count="<?php echo $count;?>" id="delete_row" data-identifier="line_item" data-id="" class="delete delete_row"></a>
		</div>
	</td>
</tr>
<?php elseif($view == 'signatory'): ?>
<tr id="row_<?php echo $count;?>">
	<td class="p-l-sm">
		<div class="form-basic">
			<div class="input-field col s12">
				<select name="role[]" data-count="<?php echo $count;?>" id="role_<?php echo $count;?>" class="selectize validate role" required="">
					<option value="" disabled selected>Please Select</option>
					<option value="R">Requested By</option>
					<option value="C">Certified By</option>
				</select>
			</div>
		</div>
	</td>
	<td class="p-l-sm">
		<div class="form-basic">
			<div class="input-field col s12">
				<input 
				type="text" 
				class="validate name_<?php echo $count;?>" 
				required="" aria=required="true" 
				name="name[]" placeholder="Enter Name" 
				value=""/>
			</div>
		</div>
	</td>
	<td width="30%" class="p-l-sm">
		<div class="form-basic">
			<div class="input-field col s12">
				<input 
				type="text" 
				class="validate position_<?php echo $count;?>" 
				required="" aria=required="true" 
				name="position[]"  placeholder="Enter Position" 
				value=""/>
			</div>
		</div>
	</td>
	<td>
		<div class="table-actions">
			<a style="cursor:pointer" data-tooltip="Delete" data-identity="signatory" data-count="<?php echo $count;?>" id="delete_row" data-identifier="signatory" data-id="" class="delete delete_row"></a>
		</div>
	</td>
</tr>
<?php else: ?>
<?php show_404();?>
<?php endif;?>