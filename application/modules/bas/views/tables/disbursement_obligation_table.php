<?php foreach($obligation_info as $key => $val):?>
<table>
	<tr>
		<td colspan="2">
			<b>DATE OF REQUEST</b>
			<p class="date_of_request">
				<?php echo date('F j, Y', strtotime($val['requested_date']));?>
			</p>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<b>PAYEE</b>
			<p class="payee"><?php echo $val['payee_name'];?></p>
			<input type="hidden" name="v_trigger" name="v_trigger" value="<?php echo $val['vatable_flag'];?>">
			<input type="hidden" name="payee" name="payee" value="<?php echo $val['payee_id'];?>">
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<b>ADDRESS</b>
			<p class="address"><?php echo $val['address'];?></p>
			<input type="hidden" name="address" name="address" value="<?php echo $val['address'];?>">
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<b>PARTICULARS</b>
			<p class="particulars"><?php echo $val['particulars'];?></p>
			<input type="hidden" name="particulars" name="particulars" value="<?php echo $val['particulars'];?>">
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<b>FUND SOURCE</b>
			<p class="fund_source"><?php echo $val['source_type_name'];?></p>
			<input type="hidden" name="source_type_num" name="source_type_num" value="<?php echo $val['source_type_num'];?>">
		</td>
	</tr>

</table>

<hr>

<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="tbl_gaa_fund">
	<thead>
		<tr class="teal lighten-5">
			<th>LINE ITEM</th>
			<th>ACCOUNT CODE</th>
			<th>REQUESTED AMOUNT</ht>
			<th>OBLIGATED AMOUNT</th>
			<th>DISBURSED AMOUNT</th>
			<th>TAX AMOUNT</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$total_ob_amount = 0;
			$total_rq_amount = 0;
		?>
		<?php 
		$count = 0;
		foreach($obligation_charges as $kissme => $here):
		$count ++;
		?>
		<?php if(!empty($details2)):
		foreach($details2 as $keys => $row):
		?>
		<tr>
			<td>
				<?php echo $here['line_item_code']. ' - '.$here['line_item_name'];?>
				<input type="hidden" name="line_item[]" value="<?php echo $here['line_item_id'];?>">
			</td>
			<td>
				<?php echo $here['account_code']. ' - '.$here['account_name'];?>
				<input type="hidden" name="account_code[]" value="<?php echo $here['account_code'];?>">
				</td>
			<td>
				<?php echo number_format($here['requested_amount']);?>
				<input type="hidden" name="requested_amount[]" value="<?php echo $here['requested_amount'];?>">
			</td>
			<td>
				<?php echo number_format($here['obligated_amount']);?>
				<input type="hidden" name="obligated_amount[]" value="<?php echo $here['obligated_amount'];?>">
			</td>
			<td class="form-basic">
				<input 
				type="text" name="disbursed_amount[]" 
				value="<?php echo ($row['disbursed_amount'] != 0.00) ? number_format($row['disbursed_amount']) : '';?>" 
				placeholder="Amount" 
				onkeyup="javascript:this.value=CommaCustom(this.value, <?php echo $count;?>);"
				autocomplete="off"
				aria-required="true" 
				required="" 
				class="validate right-align">
			</td>
			<td>
				<a href='javascript:void(0)' 
				class='tooltip md-trigger tooltipped tax_link_<?php echo $count;?>' 
				data-tooltip='Tax amount' data-position='bottom' data-delay='50' data-modal='modal_tax' 
				onclick="modal_tax_init('<?php echo $security;?>/tax/<?php echo $val['obligation_id'];?>/<?php echo $count;?>')">[ <?php echo number_format($row['ewt_amount']+$row['vat_amount']);?> ]
				</a>
				<input type="hidden" name="tax_amount[]" value="<?php echo number_format($row['ewt_amount']+$row['vat_amount']);?>" id="tax_amount_<?php echo $count;?>">
				<input type="hidden" name="base_tax_amount[]" value="<?php echo number_format($row['base_tax_amount']);?>" id="base_tax_amount_<?php echo $count;?>">
				<input type="hidden" name="ewt_rate[]" value="<?php echo floatval($row['ewt_rate']);?>" id="ewt_rate_<?php echo $count;?>">
				<input type="hidden" name="ewt_amount[]" value="<?php echo number_format($row['ewt_amount']);?>" id="ewt_amount_<?php echo $count;?>">
				<input type="hidden" name="vat_rate[]" value="<?php echo floatval($row['vat_rate']);?>" id="vat_rate_<?php echo $count;?>">
				<input type="hidden" name="vat_amount[]" value="<?php echo number_format($row['vat_amount']);?>" id="vat_amount_<?php echo $count;?>">
			</td>
		</tr>
		<?php 
			$total_ob_amount += $here['obligated_amount'];
			$total_rq_amount += $here['requested_amount'];
		?>
		<?php endforeach;?>
		<?php else:?>
		<?php 
			$total_ob_amount += $here['obligated_amount'];
			$total_rq_amount += $here['requested_amount'];
		?>
		<tr>
			<td>
				<?php echo $here['line_item_code']. ' - '.$here['line_item_name'];?>
				<input type="hidden" name="line_item[]" value="<?php echo $here['line_item_id'];?>">
			</td>
			<td>
				<?php echo $here['account_code']. ' - '.$here['account_name'];?>
				<input type="hidden" name="account_code[]" value="<?php echo $here['account_code'];?>">
				</td>
			<td>
				<?php echo number_format($here['requested_amount']);?>
				<input type="hidden" name="requested_amount[]" value="<?php echo $here['requested_amount'];?>">
			</td>
			<td>
				<?php echo number_format($here['obligated_amount']);?>
				<input type="hidden" name="obligated_amount[]" value="<?php echo $here['obligated_amount'];?>">
			</td>
			<td class="form-basic">
				<input 
				type="text" name="disbursed_amount[]" 
				value="<?php echo ($row['disbursed_amount'] != 0.00) ? number_format($row['disbursed_amount']) : '';?>" 
				placeholder="Amount" 
				onkeyup="javascript:this.value=CommaCustom(this.value, <?php echo $count;?>);"
				autocomplete="off"
				aria-required="true" 
				required="" 
				class="validate right-align">
			</td>
			<td>
				<a href='javascript:void(0)' 
				class='tooltip md-trigger tooltipped tax_link_<?php echo $count;?>' 
				data-tooltip='Tax amount' data-position='bottom' data-delay='50' data-modal='modal_tax' 
				onclick="modal_tax_init('<?php echo $security;?>/tax/<?php echo $val['obligation_id'];?>/<?php echo $count;?>')">[ 0 ]
				</a>
				<input type="hidden" name="tax_amount[]" value="apir" id="tax_amount_<?php echo $count;?>">
				<input type="hidden" name="base_tax_amount[]" value="" id="base_tax_amount_<?php echo $count;?>">
				<input type="hidden" name="ewt_rate[]" value="" id="ewt_rate_<?php echo $count;?>">
				<input type="hidden" name="ewt_amount[]" value="" id="ewt_amount_<?php echo $count;?>">
				<input type="hidden" name="vat_rate[]" value="" id="vat_rate_<?php echo $count;?>">
				<input type="hidden" name="vat_amount[]" value="" id="vat_amount_<?php echo $count;?>">
			</td>
		</tr>	
		<?php endif;?>
		<?php endforeach;?>
	</tbody>
	<thead>
		<tr>
			<td>&nbsp</td>
			<td>&nbsp&nbspTOTAL</td>
			<td>&nbsp&nbsp<?php echo number_format($total_rq_amount);?></td>
			<td>&nbsp&nbsp<?php echo number_format($total_ob_amount);?></td>
			<td>&nbsp</td>
			<td>&nbsp</td>
		</tr>
	</thead>
</table>
<?php endforeach;?>