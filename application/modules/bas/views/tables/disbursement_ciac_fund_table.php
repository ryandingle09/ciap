<div class="3 ciac">
	<table width="100%">
		<tbody>
			<tr>
				<td width="35%">
					<div><b>CASE NO</b></div><br>
					<select aria-required="true" name="case" required="" id="case" class="selectize if dropdown case" data-identity="case_no" data-target="#proceeding" data-target2="#arbitrator" style="width: 100%" <?php echo (!empty($case)) ? 'disabled="disabled"' : '';?>>
						<option value="">Select case no</option>
						<?php foreach ($cases as $key => $value) { ?>
							<option value="<?php echo $value['case_id'];?>"><?php echo $value['case_no'];?></option>
						<?php } ?>
					</select>
				</td>
				<td width="10%">&nbsp;</td>
				<td width="30">
					<div><b>STAGE OF PROCEEDING</b></div><br>
					<input type="hidden" name="hidden_me" id="hidden_proceeding" value="<?php echo $proceeding_id;?>">
					<select aria-required="true" name="proceeding" required="" id="proceeding" class="selectize if proceeding" style="width: 60%">
						<option value="">Select stage of proceeding</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<div><b>ARBITRATOR</b></div><br>
					<input type="hidden" name="hidden_me" id="hidden_arbitrator" value="<?php echo $arbitrator;?>">
					<select aria-required="true" name="arbitrator" id="arbitrator" required="" data-target="arbitrator_fee" data-identity="arbitrator" class="selectize if arbitrator dropdown" style="width: 100%">
						<option value="">Select arbitrator</option>
					</select>
				</td>
			</tr>
			<tr>
				<td>
					<div><b>ARBITRATOR FEE</b></div><br>
					<input type="hidden" id="arbitrator_fee" name="arbitrator_fee" value="<?php echo (number_format($disbursed_amount) != 0) ? number_format($disbursed_amount) : '0.00';?>">
					<p class="show_fee">0.00</p>
				</td>
			</tr>
			<tr>
				<td colspan="3">
					<div><b>PARTICULARS</b></div>
					<div class="input-field">
						<textarea placeholder="Enter particulars" class="validate materialize-textarea if" required="" aria-required="true" name="particulars" id="particulars" <?php echo (!empty($particulars)) ? 'disabled="disabled"' : '';?>><?php echo $particulars;?></textarea>
					</div>
				</td>
			</tr>
		</tbody>
	</table>

	<?php if(sizeof($details3) == 0 || empty($details3)):?>
	<div align="right">
	<button type="button" class="btn waves-effect waves-light add_deduction" data-identity="ciac" id="add_row">Add deduction</button>
	</div>
	<?php endif;?>
	<hr>

	<table style="width: 100%" cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="tbl_ciac_fund">
		<thead class="teal lighten-5">
			<tr>
				<th>DEDUCTION</th>
				<th width="20%" class="right-align">AMOUNT</th>

				<?php if(sizeof($details3) == 0 || empty($details3)):?>
				<th width="5%">ACTION</th>
				<?php endif;?>
			</tr>
		</thead>
		<tbody class="isca_deductions">
				
		</tbody>
		<tbody class="rows">
			<?php 
			if(sizeof($details3) != 0):
			foreach($details3 as $key => $row):
			?>
			<tr id="row_0" class="row_<?php echo $key;?>">
				<td>
					<select aria-required="true" name="deduction_type[]" id="deduction_type" required="" class="selectize if">
						<option value="">Deduction Type</option>
						<?php foreach ($deductions as $keys => $value) { ?>
							<?php if($value['deduction_code'] != 'DTA'):?>
							<option <?php echo ($value['deduction_code'] == 'DTA') ? 'disabled' : '';?> <?php echo ($row['deduction'] == $value['deduction_code']) ? 'selected' : '';?> value="<?php echo $value['deduction_code'];?>"><?php echo $value['deduction_name'];?></option>
							<?php endif;?>
						<?php } ?>
					</select>
				</td>
				<td>
					<input type="text" 
					name="amount[]" 
					placeholder="Enter amount" 
					onkeyup="javascript:this.value=CommaCustom(this.value, <?php echo $key;?>);"
					autocomplete="off"
					aria-required="true" 
					required="" 
					value="<?php echo (number_format($row['deduction_amount']) != 0) ? number_format($row['deduction_amount']) : '';?>" 
					class="validate right-align amount_<?php echo $key;?>" 
					id="amount_<?php echo $key;?>">
				</td>
			</tr>
			<?php 
			endforeach;
			else:
			?>
			<tr id="row_0" class="row_0">
				<td>
					<select aria-required="true" name="deduction_type[]"  id="deduction_type" required="" class="selectize if">
						<option value="">Deduction Type</option>
						<?php foreach ($deductions as $key => $value) { ?>
							<?php echo ($value['deduction_code'] != 'DTA') ? '<option value="'.$value['deduction_code'].'">'.$value['deduction_name'].'</option>' : ''?>
						<?php } ?>
					</select>
				</td>
				<td>
					<input type="text" 
					name="amount[]" 
					placeholder="Enter amount" 
					onkeyup="javascript:this.value=CommaCustom(this.value, 0);"
					autocomplete="off"
					aria-required="true" 
					required="" 
					class="validate right-align amount_0" 
					id="amount_<?php echo $count;?>">
				</td>
				<td>
					<div class="table-actions">
						<a style="cursor:pointer" data-count="0" id="delete_row" data-identifier="deduction" data-id="" class="delete"></a>
					</div>
				</td>
			</tr>
			<?php endif;?>
		</tbody>
	</table>
	<input type="hidden" name="count" id="count" class="count" value="<?php echo (sizeof($details3) == 0) ? '0' : sizeof($details3) - 1 ;?>">

</div>	

<script type="text/javascript">
	$(function(){
		selectize_init();
		$('#case')[0].selectize.setValue(<?php echo $case;?>);
	});
</script>