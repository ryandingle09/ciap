<div class="2 cmdf form-basic">
	<table>
		<tr>
			<td>
				<b>PAYEE</b>
				<div class="input-field">
					<select aria-required="false" name="payee"  id="payee" class="selectize dropdown if" data-identity="payee" style="width: 35%" required="" aria-required="true" >
						<option value="">Select payee</option>
						<?php foreach($payees as $key => $val):?>
						<option value="<?php echo $val['payee_id'];?>"><?php echo $val['payee_name'];?></option>
						<?php endforeach;?>
					</select>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<b>ADDRESS</b>
				<div class="input-field">
					<textarea placeholder="Enter Address" class="validate materialize-textarea if" required="" aria-required="true" name="address" id="address"></textarea>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<b>PARTICULARS</b>
				<div class="input-field">
					<textarea placeholder="Enter particulars" class="validate materialize-textarea if" required="" aria-required="true" name="particulars" id="particulars">
					</textarea>
				</div>
			</td>
		</tr>
	</table>
	<?php if(sizeof($details2) == 0 || empty($details2)):?>
	<div align="right">
	<button class="btn waves-effect waves-light" type="button" class="add_row add_account_code" id="add_row">Add Account Code</button>
	</div>
	<hr>
	<?php endif;?>

	<table cellpadding="0" cellspacing="0" type="button" class="table table-default table-layout-auto" id="tbl_cmd_fund">
		<thead class="teal lighten-5">
			<tr>
				<th width="40%">ACCOUNT CODE</th>
				<th width="40%">AMOUNT</th>
				<th width="15%">TAX AMOUNT</ht>
				<?php if(sizeof($details2) == 0 || empty($details2)):?>
				<th width="5%">ACTION</ht>
				<?php endif;?>
			</tr>
		</thead>
		<tbody class="rows">
			<?php $count = 0;if(sizeof($details2) != 0):?>
			<?php foreach($details2 as $key => $row): $count++ ?>
			<tr id="row_<?php echo $key;?>" class="row_<?php echo $key;?>">
				<td class="p-t-n form-basic">
					<div class="input-field">
						<select class="selectize validate" required="" aria-required="true" 
						name="account_code[]" id="account_code_<?php echo $key;?>" class="account_code_<?php echo $key;?>">
							<option value=""> Select Account code </option>
							<?php foreach($account_codes as $keys => $row2): ?>
							<option <?php echo ($row['account_code'] == $row2['account_code']) ? 'selected="selected"' : '';?> value="<?php echo $row2['account_code'];?>" ><?php echo $row2['account_code'].'&nbsp; - &nbsp;'.$row2['account_name'];?>
							</option>
							<?php endforeach;?>
						</select>
					</div>
				</td>
				<td class="p-t-n">
					<div class="input-field">
						<input type="text" 
						name="amount[]" 
						placeholder="Enter amount" 
						onkeyup="javascript:this.value=CommaCustom(this.value, <?php echo $key;?>);"
						autocomplete="off"
						aria-required="true" 
						required=""  
						value="<?php echo number_format($row['base_tax_amount']);?>"
						class="validate right-align amount_<?php echo $key;?>" 
						id="amount_<?php echo $key;?>">
					</div>
				</td>
				<td>
					<a href='javascript:void(0)' class='tooltip md-trigger tooltipped ppp tax_link_<?php echo $key;?>' id="tax_link_<?php echo $key;?>" data-count="<?php echo $key;?>" data-tooltip='View tax amount' data-position='bottom' data-delay='50' data-modal='modal_tax' onclick="modal_tax_init('<?php echo $security;?>/tax/cmdf/')">[ <?php echo number_format($row['ewt_amount']+$row['vat_amount']);?> ]</a>

					<input type="hidden" name="tax_amount[]" value="<?php echo number_format($row['ewt_amount']+$row['vat_amount']);?>" id="tax_amount_<?php echo $key;?>">
					<input type="hidden" name="base_tax_amount[]" value="<?php echo number_format($row['base_tax_amount']);?>" id="base_tax_amount_<?php echo $key;?>">
					<input type="hidden" name="ewt_rate[]" value="<?php echo floatval($row['ewt_rate']);?>" id="ewt_rate_<?php echo $key;?>">
					<input type="hidden" name="ewt_amount[]" value="<?php echo number_format($row['ewt_amount']);?>" id="ewt_amount_<?php echo $key;?>">
					<input type="hidden" name="vat_rate[]" value="<?php echo floatval($row['vat_rate']);?>" id="vat_rate_<?php echo $key;?>">
					<input type="hidden" name="vat_amount[]" value="<?php echo number_format($row['vat_amount']);?>" id="vat_amount_<?php echo $key;?>">
				</td>
			</tr>
			<?php endforeach;?>
			<?php else:?>
			<tr id="row_0" class="row_0">
				<td class="p-t-n form-basic">
					<div class="input-field">
						<select class="selectize validate" required="" aria-required="true" 
						name="account_code[]" id="account_code_0" class="account_code_0">
							<option value=""> Select Account code </option>
							<?php foreach($account_codes as $keys => $row2): ?>
							<option value="<?php echo $row2['account_code'];?>" ><?php echo $row2['account_code'].'&nbsp; - &nbsp;'.$row2['account_name'];?>
							</option>
							<?php endforeach;?>
						</select>
					</div>
				</td>
				<td class="p-t-n">
					<div class="input-field">
						<input type="text" 
						name="amount[]" 
						placeholder="Enter amount" 
						onkeyup="javascript:this.value=CommaCustom(this.value, 0);"
						autocomplete="off"
						aria-required="true" 
						required="" 
						class="validate right-align amount_0" 
						id="amount_0">
					</div>
				</td>
				<td>
					<a href='javascript:void(0)' class='tooltip md-trigger tooltipped ppp tax_link_0' id="tax_link_0" data-count="0" data-tooltip='Tax amount' data-position='bottom' data-delay='50' data-modal='modal_tax' onclick="modal_tax_init('<?php echo $security;?>/tax/cmfd/')">[ 0 ]</a>

					<input type="hidden" name="tax_amount[]" value="apir" id="tax_amount_0">
					<input type="hidden" name="base_tax_amount[]" value="" id="base_tax_amount_0">
					<input type="hidden" name="ewt_rate[]" value="" id="ewt_rate_0">
					<input type="hidden" name="ewt_amount[]" value="" id="ewt_amount_0">
					<input type="hidden" name="vat_rate[]" value="" id="vat_rate_0">
					<input type="hidden" name="vat_amount[]" value="" id="vat_amount_0">
				</td>
				<td>
					<div class="table-actions">
						<a style="cursor:pointer" data-count="0" id="delete_row" data-identity="cmdf_account_code" data-id="" class="delete"></a>
					</div>
				</td>
			</tr>
			<?php endif;?>
		</tbody>
	</table>
	<input type="hidden" name="count" id="count" class="count" value="<?php echo (sizeof($details2) == 0) ? '0' : sizeof($details2) - 1 ;?>">
</div>

<script type="text/javascript">
	$(function($){
		selectize_init();
		$("#payee")[0].selectize.setValue($('#payee_id').val());
		$('#particulars').val($('#particulars_a').val());
	});
</script>