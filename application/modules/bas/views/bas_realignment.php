<div class="page-title m-b-lg">
	<ul id="breadcrumbs">
		<li><a href="<?php echo base_url() ?>/bas/bas_dashboard">Home</a></li>
		<li><a href="<?php echo base_url() ?>bas/bas_realignment" class="active">Realignment</a></li>
	</ul>

	<div class="row m-b-n m-r-n">
		<div class="col s6 p-r-n">
			<h5>Realignment
				<span>Manage Realignments.</span>
			</h5>
		</div>

		<div class="col s6 p-r-n right-align">
		  <div class="input-field inline p-l-md">
			<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_realignment" id="add_realignment" onclick="modal_init()">Add Realignment</button>
		  </div>
		</div>
		
	</div>

</div>

<div class="pre-datatable">&nbsp;</div>
<div>
	<table cellpadding="0" cellspacing="0" class="table table-default bordered table-layout-auto" id="realignment_table">
	  <thead>
		<tr>
		 	<th>Release Date</th>
			<th>Release Number</th>
			<th>Amount</th>
			<th>Status</th>
			<th>Source</th>
			<th>Recipient</th>
			<th class="center-align" width="10%">Action</th>
		</tr>
	  </thead>
  	</table>
</div>

<script type="text/javascript">
 var deleteObj = new handleData({ 
 		controller : 'bas_realignment', 
 		method : 'delete_realignment', 
 		module: '<?php echo PROJECT_BAS ?>' 
 });
</script>