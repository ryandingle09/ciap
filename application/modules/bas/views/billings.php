<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="<?php echo base_url();?>bas/bas_billings" class="active">Billings</a></li>
  </ul>
  <div class="row m-b-n m-r-n">
	<div class="col s6 p-r-n">
	  <h5>Billings
		<span>Manage Billings.</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_add" id="add" name="add" onclick="modal_add_init()">Add New Billings</button>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable">&nbsp;</div>

<div>

	<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="tbl_billings">
		<thead>
			<tr>
				<th>Bill No</th>
				<th>Bill Date</th>
				<th>Due Date</ht>
				<th>Obligation Slip No.</th>
				<th>ACTION</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>

</div>

<script type="text/javascript">
 var deleteObj = new handleData({ 
 		controller : 'bas_billings', 
 		method : 'delete_billing', 
 		module: '<?php echo PROJECT_BAS ?>' 
 });
</script>