<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="<?php echo base_url();?>bas/bas_dashboard" class="active">Dashboard</a></li>
  </ul>
  <div class="row m-b-n">
	<div class="col s6 p-r-n">
	  <h5>Dashboard
		<!--<span>Manage Reports.</span>-->
	  </h5>
	</div>
</div>
</div>

<div class="pre-datatable">&nbsp;</div>

<div>

	<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="tbl_dashboard">
		<thead>
			<tr>
				<th>Particulars</th>
				<th>Appropriations</th>
				<th>Allotments</ht>
				<th>Obligations</th>
				<th>Disbursements</th>
				<th>Utilization Rate<br>Allotments Vs Obligations</ht>
				<th>Unobligated<br>Allotment</ht>
				<th>Unpaid Obligations</ht>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>

</div>