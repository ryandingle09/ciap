<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="<?php echo base_url();?>bas/bas_payees" class="active">Payees</a></li>
  </ul>
  <div class="row m-b-n m-r-n">
	<div class="col s6 p-r-n">
	  <h5>Payees
		<span>Manage payees.</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal" id="add" name="add" onclick="modal_init()">Add New Payee</button>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable">&nbsp;</div>
<div>

	<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="tbl_payees">
		<thead>
			<tr>
				<th>Payee</th>
				<th>Address</th>
				<th>TIN</th>
				<th width="10%">STATUS</th>
				<th width="10%">ACTION</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>

</div>

<script type="text/javascript">
 var deleteObj = new handleData({
	 	controller : 'bas_payees', 
	 	method : 'delete_payees', 
	 	module: '<?php echo PROJECT_BAS ?>'
	 });
</script>