<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="<?php echo base_url();?>bas/bas_type_source" class="active">Chart of Accounts</a></li>
  </ul>
  <div class="row m-b-n m-r-n">
	<div class="col s6 p-r-n">
	  <h5>Chart of Accounts
		<span>Manage Type Chart of Accounts.</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal" id="add" name="add" onclick="modal_init()">Add New Chart of Account</button>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable">&nbsp;</div>

<div>

	<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="tbl_chart">
		<thead>
			<tr>
				<th>CODE</th>
				<th>ACCUNT TITLE</th>
				<th width="10%">STATUS</ht>
				<th width="10%">ACTION</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>

</div>

<script type="text/javascript">
 var deleteObj = new handleData({ 
 		controller : 'bas_chart_accounts', 
 		method : 'delete_chart', 
 		module: '<?php echo PROJECT_BAS ?>' 
 });
</script>