<div class="page-title m-b-lg">
  <ul id="breadcrumbs">
	<li><a href="#">Home</a></li>
	<li><a href="<?php echo base_url();?>bas/bas_disbursement_vouchers" class="active">Disbursement Voucher</a></li>
  </ul>
  <div class="row m-b-n m-r-n">
	<div class="col s6 p-r-n">
	  <h5>Disbursement Voucher
		<span>Manage Disbursement Vouchers.</span>
	  </h5>
	</div>
	<div class="col s6 p-r-n right-align">
	  <div class="input-field inline p-l-md">
		<button type="button" class="btn waves-effect waves-light md-trigger btn-success" data-modal="modal_default" id="add" name="add" onclick="modal_default_init()">Add Disbursement Voucher</button>
	  </div>
	</div>
  </div>
</div>

<div class="pre-datatable">&nbsp;</div>

<div>

	<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="tbl_disbursement">
		<thead>
			<tr>
				<th>FUND</ht>
				<th>DV NO.</th>
				<th>DV DATE</th>
				<th>DISBURSEMENT AMOUNT</ht>
				<th>STATUS</ht>
				<th width="15%" class="center-align">ACTION</th>
			</tr>
		</thead>
		<tbody>
		</tbody>
	</table>

</div>

<script type="text/javascript">
 var deleteObj = new handleData({ 
 		controller : 'bas_disbursement_vouchers', 
 		method : 'delete_disbursement', 
 		module: '<?php echo PROJECT_BAS ?>' 
 });
</script>