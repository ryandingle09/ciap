<div class="panel p-n m-n form-basic">
	<input type="hidden" name="uri" value="co">
	<div class="row">
		<table cellpadding="0" cellspacing="0" class="table table-default table-layout-auto" id="account_code_table">
			<thead class="cyan lighten-5">
				<tr>
					<td width="30%">ACCOUNT CODE</td>
					<td width="27%">APPROPRIATION</td>
					<td>ALLOTMENT</td>
					<td class="center-align" width="5%">Action</td>
				</tr>
			</thead>
			<tbody>
				<?php 
				if(!empty($line_item_codes)):
				foreach($line_item_codes as $key => $row): 
				?>
				<tr id="row_<?php echo $row['account_code'];?>">
					<td>
						<input type="hidden" name="old_account_code[]" id="old_account_code" value="<?php echo $row['account_code'];?>">
						<select class="selectize validate" required="" aria-required="true" 
						name="update_account_code[]" id="account_code">
							<option value="">Select account code</option>
							<?php foreach($account_codes as $key => $row2): ?>
							<option <?php if($row2['account_code'] == $row['account_code']) echo 'selected' ;?> value="<?php echo $row2['account_code'];?>"><?php echo $row['account_code'];?>  -  <?php echo $row2['account_name'];?></option>
							<?php endforeach;?>
						</select>
					</td>
					<td width="25%">
						<input name="update_appropriation_amount[]" class="right-align validate" 
						required="" aria-required="true" type="text" autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
						value="<?php echo number_format(floatval($row['appropriation_amount']));?>"
						placeholder="Enter appropriation amount" />
					</td>
					<td width="25%">
						<input name="update_allotment_amount[]" class="right-align validate" 
						required="" aria-required="true" type="text" autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
						data-parsley-type="number" value="<?php echo number_format(floatval($row['allotment_amount']));?>" 
						placeholder="Enter allotment amount" />
					</td>
					<td>
						<div class="table-actions">
							<a style="cursor:pointer" data-count="<?php echo $row['account_code'];?>" id="delete_row" data-identifier="account_code" data-id="<?php echo $row['account_code'];?>" class="delete"></a>
						</div>
					</td>
				</tr>
				<?php
				endforeach;
				else:
				?>
				<tr id="row_1">
					<td width="40%">
						<select class="selectize validate" required="" aria-required="true"
						name="account_code[]" id="account_code">
							<option value="">Select account code</option>
							<?php foreach($account_codes as $key => $row):?>
							<option value="<?php echo $row['account_code'];?>"><?php echo $row['account_code'];?>  -  <?php echo $row['account_name'];?></option>
							<?php endforeach;?>
						</select>
					</td>
					<td width="25%">
						<input name="appropriation_amount[]" class="right-align validate" 
						required="" aria-required="true" type="text" autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
						placeholder="Enter appropriation amount" />
					</td>
					<td width="25%">
						<input name="allotment_amount[]" class="right-align validate" 
						required="" aria-required="true" type="text" autocomplete="off" onkeyup="javascript:this.value=Comma(this.value);" 
						placeholder="Enter allotment amount" />
					</td>
					<td>
						<div class="table-actions">
							<a style="cursor:pointer" data-count="denied" id="delete_row" data-identifier="" class="delete"></a>
						</div>
					</td>
				</tr>
				<?php endif;?>
			</tbody>
		</table>
		<input type="hidden" name="count_all" id="count_all" value="1">
	</div>
</div>