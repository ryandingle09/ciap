<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MPIS_Model extends Base_Model
{
	const MODULE = PROJECT_MPIS;
	
	protected static $dsn = DB_MPIS;

	const tbl_bill_status_history					= 'bill_status_history';
	const tbl_bills									= 'bills';
	
	const tbl_board_meetings						= 'board_meetings';
	const tbl_board_resolutions						= 'board_resolutions';
	const tbl_building_construction_regions			= 'building_construction_regions';
	const tbl_building_constructions				= 'building_constructions';
	
	const tbl_case_status_history					= 'case_status_history';
	const tbl_cases									= 'cases';
	
	const tbl_construction_countries				= 'construction_countries';
	const tbl_construction_country_indicators		= 'construction_country_indicators';
	const tbl_construction_employment_groups		= 'construction_employment_groups';
	const tbl_construction_employments				= 'construction_employments';
	const tbl_construction_professions				= 'construction_professions';
	const tbl_financial_ratio_items					= 'financial_ratio_items';
	const tbl_financial_ratios						= 'financial_ratios';
	const tbl_indicators							= 'indicators';
	const tbl_issuances								= 'issuances';
	const tbl_laws									= 'laws';
	const tbl_market_value_prices					= 'market_value_prices';
	const tbl_market_values							= 'market_values';
	const tbl_meeting_agenda						= 'meeting_agenda';
	
	const tbl_param_bill_origins					= 'param_bill_origins';
	const tbl_param_bill_types						= 'param_bill_types';
	const tbl_param_boards							= 'param_boards';
	const tbl_param_building_construction_regions	= 'param_building_construction_regions';
	const tbl_param_building_construction_sources	= 'param_building_construction_sources';
	const tbl_param_building_construction_types		= 'param_building_construction_types';
	const tbl_param_company_types					= 'param_company_types';
	const tbl_param_construction_indicators			= 'param_construction_indicators';
	const tbl_param_countries						= 'param_countries';	
	const tbl_param_financial_ratio_items			= 'param_financial_ratio_items';
	const tbl_param_financial_ratios				= 'param_financial_ratios';
	const tbl_param_indicator_categories			= 'param_indicator_categories';
	const tbl_param_indicator_source_types			= 'param_indicator_source_types';
	const tbl_param_indicator_sources				= 'param_indicator_sources';
	const tbl_param_issuance_types					= 'param_issuance_types';
	const tbl_param_license_categories				= 'param_license_categories';
	const tbl_param_major_industry_groups			= 'param_major_industry_groups';
	const tbl_param_market_value_types				= 'param_market_value_types';	
	const tbl_param_meeting_types					= 'param_meeting_types';	
	const tbl_param_policy_categories				= 'param_policy_categories';
	const tbl_param_price_types						= 'param_price_types';	
	const tbl_param_professions						= 'param_professions';
	const tbl_param_subject_matters					= 'param_subject_matters';
	
	const tbl_policies								= 'policies';
	const tbl_policy_attachments					= 'policy_attachments';
	const tbl_policy_keywords						= 'policy_keywords';
	
	const tbl_resolution_status_history				= 'resolution_status_history';
	const tbl_standard_templates					= 'standard_templates';

}

/* End of file mpis_model.php */
/* Location: ./application/core/mpis_model.php */