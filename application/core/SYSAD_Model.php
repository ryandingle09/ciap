<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SYSAD_Model extends Base_Model
{
	protected static $dsn = DB_CORE;

	const tbl_cias_user	= 'cias_user';
	const tbl_user_roles	= 'cias_user_role';
	
	const tbl_param_resource	= 'cias_param_resource';
}