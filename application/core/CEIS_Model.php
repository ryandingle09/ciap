<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CEIS_Model extends Base_Model {

	protected static $dsn = DB_CEIS;	

	const tbl_countries 		 	 			= 'param_countries';
	const tbl_country_profile	 	 			= 'country_profiles';
	const tbl_geo_regions 	 	 				= 'param_geo_regions';
	const tbl_geo_region_countries 				= 'param_geo_region_countries';
	const tbl_positions 			 			= 'param_positions';
	const tbl_project_types 		 			= 'param_project_types';
	const tbl_contract_involvement 				= 'param_contract_involvement';
	const tbl_opportunity_sources	 			= 'param_opportunity_sources';
	const tbl_opportunity_recipients  			= 'opportunity_recipients';
	const tbl_checklist 						= 'param_checklist';
	const tbl_category							= 'param_category';
	const tbl_param_contract_types				= 'param_contract_types';
	const tbl_param_request_types				= 'param_request_types';
	const tbl_opportunities		 				= 'opportunities';
	const tbl_resolutions		 	 			= 'resolutions';

	const tbl_contractors		 	 			= 'contractor';
	const tbl_contractor_payments		 	 	= 'contractor_payments';
	const tbl_contractor_payment_collections	= 'contractor_payment_collections';

	const tbl_equipment		 	 				= 'contractor_equipment';
	const tbl_cron_request		 				= 'cron_request_flag';
	const tbl_contractor_work_volume			= 'contractor_work_volume';
	const tbl_contractor_projects				= 'contractor_projects';
	const tbl_contractor_finance				= 'contractor_statements';
	const tbl_contractor_staff 					= 'contractor_staff';
	const tbl_contractor_staff_licenses 		= 'contractor_staff_licenses';
	const tbl_contractor_categories 			= 'param_contractor_categories';
	const tbl_contractor_statement				= 'contractor_statements';
	const tbl_contractor_services				= 'contractor_services';
	const tbl_contractor_services_dtl			= 'contractor_services_dtl';
	const tbl_contractor_projects_dtl			= 'contractor_project_dtl';
	const tbl_contractor_contracts_schedule		= 'contractor_contracts_schedule';
	const tbl_contractor_contracts				= 'contractor_contracts';
	const tbl_contractor_staff_projects 		= 'contractor_staff_projects';
	const tbl_contractor_staff_experiences  	= 'contractor_staff_experiences';
	const tbl_contractor_registration  			= 'contractor_registration';
	const tbl_param_registration_status			= 'param_registration_status';
	const tbl_contractor_license_classifications= 'contractor_license_classifications';

	const tbl_requests		 	 				= 'requests';
	const tbl_request_staff_projects  			= 'request_staff_projects';
	const tbl_request_services					= 'request_services';
	const tbl_request_task_remarks				= 'request_task_remarks';
	const tbl_request_contractor				= 'request_contractor';
	const tbl_request_license_classifications 	= 'request_license_classifications';
	const tbl_request_contracts		    		= 'request_contracts';
	const tbl_request_projects		    		= 'request_projects';
	const tbl_request_staff		   	 			= 'request_staff';
	const tbl_request_staff_licenses    		= 'request_staff_licenses';
	const tbl_request_staff_experiences 		= 'request_staff_experiences';
	const tbl_request_statements 				= 'request_statements';
	const tbl_request_equipment 				= 'request_equipment';
	const tbl_request_checklist 				= 'request_checklist';
	const tbl_request_attachments 				= 'request_attachments';
	const tbl_request_aut_projects 				= 'request_aut_projects';
	const tbl_request_aut_services				= 'request_aut_services';
	const tbl_request_schedule 					= 'request_schedule';
	const tbl_request_payments					= 'request_payments';
	const tbl_request_payment_collections 		= 'request_payment_collections';
	const tbl_request_registration 				= 'request_registration';
	const tbl_request_evaluation				= 'request_evaluation';
	const tbl_request_tasks						= 'request_tasks';
	const tbl_request_task_attachments			= 'request_task_attachments';

	const tbl_param_request_status      		= 'param_request_status';
	const tbl_param_opportunity_status 			= 'param_opportunity_status';
	const tbl_param_contract_status 			= 'param_contract_status';
	const tbl_param_license_category 			= 'param_license_category';
	const tbl_param_license_classifications 	= 'param_license_classifications';
	const tbl_param_nationalities				= 'param_nationalities';
	const tbl_param_collection_natures		    = 'param_collection_natures';
	const tbl_param_request_tasks				= 'param_request_tasks';
	const tbl_param_request_task_status			= 'param_request_task_status';
	const tbl_param_role						= 'param_role';
	const tbl_param_document_types				= 'param_document_types';
	const tbl_param_attachment_types			= 'param_attachment_types';
	const tbl_param_equipment_type				= 'param_equipment_type';
	
	const tbl_progress_reports 					= 'progress_reports';
	const tbl_progress_report_info 				= 'progress_report_info';
	const tbl_progress_report_manpower 			= 'progress_report_manpower';
	const tbl_progress_report_remittances 		= 'progress_report_remittances';
	const tbl_users 							= 'users';
	const tbl_registration 						= 'registration';

	
	protected function set_datatable($main_query, $aColumns, $bColumns, $params, $query_params, $others=array())
	{
		try 
		{
			$query  		  = "SELECT  * FROM ( ".$main_query." ) a ";
			$sWhere 		  = $this->filtering($bColumns, $params, FALSE);
			$sOrder			  = $this->ordering($params,$aColumns);
			$sLimit 		  = $this->paging($params);
			$filter_str       = $sWhere['search_str'];
			$filter_params    = array_merge($query_params, $sWhere['search_params']);
			$filtered_length  = 0;
			$total_length     = 0;
			
			$total_length = $this->get_total_length($query, $query_params);
			
			$query .= ' '.$filter_str;
			$query .= ' '.$sOrder;
			$query .= ' '.$sLimit;
			
			$stmt   = $this->query($query, $filter_params);
			
			$filtered_length = $this->get_filtered_length();
				
			return  array(
					"sEcho" 			   => intval($params['sEcho']),
					"iTotalRecords" 	   => $filtered_length,
					"iTotalDisplayRecords" => $total_length,
					"aaData" 			   => $stmt
			);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}	
	
	public function get_filtered_length()
	{
		try 
		{
			$query = <<<EOS
				SELECT FOUND_ROWS() count
EOS;
			$stmt  = $this->query($query, NULL, FALSE);
			
			return $stmt['count'];
		}
		catch(PDOException $e)
		{
			throw $e;
		}
	}	
	
	
	public function get_total_length($query, $params)
	{	
		try
		{
			$sql   =<<<EOS
				SELECT COUNT(1) as total_rows FROM ( $query ) a
EOS;
			$stmt  = $this->query($sql, $params, FALSE);
			
			return $stmt['total_rows'];
		}
		catch(PDOException $e)
		{
			throw $e;
		}
	}
		
	
}