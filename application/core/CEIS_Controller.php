<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CEIS_Controller extends Base_Controller
{
	const MODULE = PROJECT_CEIS;

	protected $module_code 	  = self::MODULE;
	protected $complete_flags = array(COMPLETE_FLAG, INCOMPLETE_FLAG, NONE_FLAG);
	protected $approve_flags  = array(APPROVE_FLAG, DISAPPROVE_FLAG, NONE_FLAG);
	public $user_id       	  = '';
	public $user_roles        = array();
	
	public function __construct(){
		parent::__construct();
		
		$this->user_id 	  = $this->session->userdata('employee_no');
		$this->user_roles = $this->session->userdata('user_roles');
	}
}