<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PSMS_Model extends Base_Model
{
	const MODULE = PROJECT_PSMS;
	
	protected static $dsn = DB_PSMS;
	
	const tbl_users 		= 'users';

	const tbl_asset_accountabilities				= 'asset_accountabilities';
	const tbl_asset_categories						= 'asset_categories';
	
	const tbl_asset_classifications					= 'asset_classifications';
	const tbl_asset_locations						= 'asset_locations';
	const tbl_assets								= 'assets';
	const tbl_employees								= 'employees';
	const tbl_offices								= 'offices';

	const tbl_param_asset_accountability_status		= 'cases';	
	const tbl_param_asset_conditions				= 'param_asset_conditions';
	const tbl_param_asset_conditions_status			= 'param_assets_conditions_status';
	const tbl_param_asset_status					= 'param_asset_status';
	const tbl_param_sites							= 'param_sites';
	
	const tbl_supplies								= 'supplies';
	const tbl_supplies_categories					= 'supplies_categories';
	const tbl_supplies_items						= 'supplies_items';
	
	const tbl_cias_user								= 'cias.cias_user';
	const tbl_cias_user_role						= 'cias.cias_user_role';
}

/* End of file mpis_model.php */
/* Location: ./application/core/mpis_model.php */