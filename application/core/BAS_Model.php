<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BAS_Model extends Base_Model
{
	protected static $dsn = DB_BAS;

	const tbl_billings		 	 				= 'billings';
	const tbl_checks		 	 				= 'checks';
	const tbl_disbursements	 	 				= 'disbursements';
	const tbl_disbursement_charges 				= 'disbursement_charges';
	const tbl_disbursement_deductions 			= 'disbursement_deductions';
	const tbl_fund_sources 						= 'fund_sources';
	const tbl_line_items 			 			= 'line_items';
	const tbl_line_item_accounts 				= 'line_item_accounts';
	const tbl_line_item_offices					= 'line_item_offices';
	const tbl_line_item_realigns		 		= 'line_item_realigns';
	const tbl_line_item_realign_details			= 'line_item_realign_details';
	const tbl_line_item_releases	 	 		= 'line_item_releases';
	const tbl_line_item_release_details			= 'line_item_release_details';
	const tbl_obligations		 	 			= 'obligations';
	const tbl_obligation_charges  				= 'obligation_charges';
	const tbl_obligation_signatories			= 'obligation_signatories';
	const tbl_param_account_codes				= 'param_account_codes';
	const tbl_param_disbursement_funds			= 'param_disbursement_funds';
	const tbl_param_check_status				= 'param_check_status';
	const tbl_param_disbursement_status			= 'param_disbursement_status';
	const tbl_param_obligation_status			= 'param_obligation_status';
	const tbl_param_offices						= 'param_offices';
	const tbl_param_payees						= 'param_payees';
	const tbl_param_proceeding_stages			= 'param_proceeding_stages';
	const tbl_param_source_types				= 'param_source_types';
	const tbl_param_proceeding_stage_activities	= 'param_proceeding_stage_activities';
	const tbl_param_check_mode_payments			= 'param_check_mode_payments';
	const tbl_param_deduction_types				= 'param_deduction_types';
	const tbl_arbitrators						= 'arbitrators';
	const tbl_cases								= 'cases';
	const tbl_case_arbitrator_payments			= 'case_arbitrator_payments';
	const tbl_case_arbitrator_fee_shares		= 'case_arbitrator_fee_shares';
	const tbl_case_arbitrator_deductions		= 'case_arbitrator_deductions';
}

/* End of file bas_model.php */
/* Location: ./application/core/bas_model.php */