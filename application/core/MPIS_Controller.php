<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MPIS_Controller extends Base_Controller
{
	const MODULE = PROJECT_MPIS;

	protected $module_code = self::MODULE;
}

/* End of file bas_controller.php */
/* Location: ./application/core/bas_controller.php */