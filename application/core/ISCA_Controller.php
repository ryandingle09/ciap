<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ISCA_Controller extends Base_Controller
{
	const MODULE = PROJECT_ISCA;

	protected $module_code = self::MODULE;
}

/* End of file isca_controller.php */
/* Location: ./application/core/isca_controller.php */