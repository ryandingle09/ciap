<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PSMS_Controller extends Base_Controller
{
	const MODULE = PROJECT_PSMS;

	protected $module_code = self::MODULE;
}