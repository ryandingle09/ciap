<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ISCA_Model extends Base_Model
{
	protected static $dsn = DB_ISCA;

	const tbl_arbitrator				= "isca_arbitrator";
	const tbl_card_catalog				= "books";
	const tbl_meeting					= "meetings";
	const tbl_minutes					= "meeting_minutes";
	const tbl_meeting_attendees 		= "meeting_attendees";
	const tbl_seminar_material 			= "seminar_materials";
	const tbl_payment_types 			= "param_payment_type";
	const tbl_parties 					= "parties";
	const tbl_case						= "cases";
	const tbl_case_party 				= "case_parties";
	const tbl_case_arbitration_detail 	= "case_arbitration_details";
	const tbl_case_claims				= "case_claims";
	const tbl_case_op_hdr				= "case_op_hdr";
	const tbl_case_op_dtl				= "case_op_dtl";
	const tbl_case_type					= "param_case_types";
	const tbl_case_status				= "param_case_status";
	const tbl_appeal_status				= "param_appeal_status";
	const tbl_document_type				= "param_document_types";
	const tbl_case_activity				= "param_case_activity";
	const tbl_policy_category			= "param_policy_category";
	const tbl_attendees					= "param_meeting_attendees";
	const tbl_profession				= "param_professions";
	const tbl_mace_rule					= "param_mace_rule";
	const tbl_nature_case				= "param_nature_case";
	const tbl_book_author				= "param_book_authors";

}

/* End of file isca_model.php */
/* Location: ./application/core/isca_model.php */
