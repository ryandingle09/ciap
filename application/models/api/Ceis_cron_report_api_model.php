<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Models::applications
 * @author Asiagate Networks, Inc
 * @email support@asiagate.com
 *
 * 
 */

class Ceis_cron_report_api_model extends CEIS_Model {
	            
	public function __construct(){
		parent::__construct();
	}
	
	public function get_contractor_email_by_contract($contract_id)
	{
		try 
		{
			$query  = <<<EOS
				SELECT b.email
					FROM  %s a
						JOIN %s b ON a.contractor_id = b.contractor_id
						WHERE a.contract_id = ?
EOS;
			$query  = sprintf($query, self::tbl_contractor_contracts, self::tbl_contractors);
			
			$result =  $this->query($query, array($contract_id), FALSE);
			
			return $result['email'];
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	public function get_contract_details($contract_id)
	{
		try
		{
			$query  = <<<EOS
				SELECT a.project_title
					FROM  %s a
						WHERE a.contract_id = ?
EOS;
			$query  = sprintf($query, self::tbl_contractor_contracts);
				
			return  $this->query($query, array($contract_id), FALSE);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _is_locked()
	{
		try
		{
			$result = $this->select_one(array('*'), self::tbl_cron_request);
			RLog::info(var_export($result, TRUE));
			return ($result['locked'] == 'N') ? FALSE : TRUE;
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function lock_cron()
	{
		try 
		{
			if($this->_is_locked()) throw new Exception('Cron request is still locked.');
			
			Ceis_model::beginTransaction();

			$this->update_data(self::tbl_cron_request, array('locked' => 'Y', 'datetime' => date('Y-m-d H:i:s')), array());
			
			Ceis_model::commit();
		}
		catch(PDOException $e)
		{
			Ceis_model::rollback();
			throw $e;
		}
		catch(Exception $e)
		{
			Ceis_model::rollback();
			throw $e;
		}
	}
	
	public function unlock_cron()
	{
		try
		{
			Ceis_model::beginTransaction();
				
			$this->update_data(self::tbl_cron_request, array('locked' => 'N', 'datetime' => date('Y-m-d H:i:s')), array());
				
			Ceis_model::commit();
		}
		catch(PDOException $e)
		{
			Ceis_model::rollback();
			throw $e;
		}
		catch(Exception $e)
		{
			Ceis_model::rollback();
			throw $e;
		}
	}
	
	
	public function insert_report_data($data, $request_type_id)
	{
		try
		{
			$tables = $this->_tables_for_reports($request_type_id);
			
			foreach($tables as $key => $tbl_name):
				/*
				 * $table_name is from the api and with prefix 'ceis_'.
				 * $tbl_name is from the ceis database in intranet.
				 */
				$table_name  = 'ceis_'.$tbl_name;
			
			/* 	if(ISSET($data[$table_name][0])):
					
					
					$params	     = $data[$table_name][0];
					
					$this->insert_data($tbl_name, $params);
				endif; */
				
				if( ISSET($data[$table_name]) && ! EMPTY($data[$table_name]) ):
				
					$this->insert_data($tbl_name, $data[$table_name]);
				
				endif;
				  
			endforeach;
			
		}
		catch (PDOException $e){
			throw $e;
		}
		catch(Exception $e){
			throw $e;
		}
	}
	
	
	private function _tables_for_reports($request_type_id)
	{
		$tables   = array();
		$tables[] = self::tbl_progress_reports;
		$tables[] = self::tbl_progress_report_info;
		$tables[] = self::tbl_progress_report_manpower;
		$tables[] = self::tbl_progress_report_remittances;
		
		return $tables;
	}
	
	public function insert_request_tasks($request_id, $request_type_id)
	{
		try 
		{ 
			$query = <<<EOS
				INSERT INTO %s
					(request_id, sequence_no, task_name, role_code, generate_reference_flag, get_task_flag, get_board_decision, generate_order_of_payment, evaluation_flag, task_status_id)
				
				SELECT ?, sequence_no, task_name, role_code, generate_reference_flag, get_task_flag, get_board_decision, generate_order_of_payment, evaluation_flag, ? 
					FROM  %s
					WHERE request_type_id = ?
					ORDER BY sequence_no ASC
EOS;
			$query = sprintf($query, self::tbl_request_tasks, self::tbl_param_request_tasks);
			
			$this->query($query, array($request_id, REQUEST_TASK_NOT_YET_STARTED, $request_type_id), NULL, TRUE);
		}
		catch (PDOException $e){
			throw $e;
		}
		catch(Exception $e){
			throw $e;
		}
	}
}
