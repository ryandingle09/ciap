<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Models::applications
 * @author Asiagate Networks, Inc
 * @email support@asiagate.com
 *
 * 
 */

class Cs_official_receipt_api_model extends Base_Model {
	            
	protected static $dsn = '';
	protected $tbl_op  	  = '';
	
	public function __construct(){
		parent::__construct();
	}
	

	public function set_dsn($dsn)
	{
	
		switch(strtolower($dsn)):
			case PROJECT_CEIS :
					$db  = DB_CEIS;
					$tbl = 'request_payments';
				break;
			case PROJECT_BAS :
					$db = DB_BAS;
					$tbl = '';
				break;
			case PROJECT_MPIS :
					$db = DB_MPIS;
					$tbl = '';
				break;
			case PROJECT_PIS :
					$db = DB_PIS;
					$tbl = '';
				break;
			case PROJECT_ASIS :
					$db = DB_ASIS;
					$tbl = '';
				break;
			case PROJECT_ISCA :
					$db = DB_ISCA;
					$tbl = '';
				break;
			case PROJECT_PSMS :
					$db = DB_PSMS;
					$tbl = '';
				break;
		endswitch;
	
		static::$dsn  = $db;
		$this->tbl_op = $tbl;
	}

	
	public function update_op_or_details($params, $where)
	{
		try 
		{ 
			$this->update_data($this->tbl_op, $params, $where);
		}
		catch (PDOException $e){
			throw $e;
		}
		catch(Exception $e){
			throw $e;
		}
	}
}
