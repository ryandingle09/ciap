<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Models::applications
 * @author Asiagate Networks, Inc
 * @email support@asiagate.com
 *
 * 
 */

class Auth_api_model extends Base_Model {
	
	protected static $dsn = 'aaaaa';
	const tbl_users  = 'users';
	
	public function __construct(){
		parent::__construct();
	}
	
	public function set_dsn($dsn)
	{
		
		switch(strtolower($dsn)):
			case PROJECT_CEIS :
				$db = DB_CEIS;
			break;
			case PROJECT_BAS :
				$db = DB_BAS;
				break;
			case PROJECT_MPIS :
				$db = DB_MPIS;
				break;
			case PROJECT_PIS :
				$db = DB_PIS;
				break;
			case PROJECT_ASIS :
				$db = DB_ASIS;
				break;
			case PROJECT_ISCA :
				$db = DB_ISCA;
				break;
			case PROJECT_PSMS :
				$db = DB_PSMS;
				break;
		endswitch;
		
		static::$dsn = $db;
	}
	
	public function select_user($employee_no, $fields=array('*'))
	{
		try
		{
			return $this->select_one($fields, self::tbl_users, array('employee_no' => $employee_no));
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function insert_user($params)
	{
		try
		{
			$this->insert_data(self::tbl_users, $params);
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function update_user($employee_no, $token)
	{
		try
		{
			$this->update_data(self::tbl_users, array('token' => $token), array('employee_no' => $employee_no));
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	

	
	public function get_request_type($request_type_id)
	{
		try 
		{
			$result = $this->select_one(array('request_type_name'), self::tbl_param_request_types, array('request_type_id' => $request_type_id));
			
			return $result['request_type_name'];
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _is_locked()
	{
		try
		{
			$result = $this->select_one(array('*'), self::tbl_cron_request);
			RLog::info(var_export($result, TRUE));
			return ($result['locked'] == 'N') ? FALSE : TRUE;
		}
		catch(PDOException $e)
		{
			throw $e;
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	public function lock_cron()
	{
		try 
		{
			if($this->_is_locked()) throw new Exception('Cron request is still locked.');
			
			Ceis_model::beginTransaction();

			$this->update_data(self::tbl_cron_request, array('locked' => 'Y', 'datetime' => date('Y-m-d H:i:s')), array());
			
			Ceis_model::commit();
		}
		catch(PDOException $e)
		{
			Ceis_model::rollback();
			throw $e;
		}
		catch(Exception $e)
		{
			Ceis_model::rollback();
			throw $e;
		}
	}
	
	public function unlock_cron()
	{
		try
		{
			Ceis_model::beginTransaction();
				
			$this->update_data(self::tbl_cron_request, array('locked' => 'N', 'datetime' => date('Y-m-d H:i:s')), array());
				
			Ceis_model::commit();
		}
		catch(PDOException $e)
		{
			Ceis_model::rollback();
			throw $e;
		}
		catch(Exception $e)
		{
			Ceis_model::rollback();
			throw $e;
		}
	}
	
	
	public function insert_request_data($data, $request_type_id)
	{
		try
		{
			//APPEND "ceis_"
			$tbl_request  = 'ceis_';
			$tbl_request .= self::tbl_requests;
			
			//GET REQUEST DETAILS NEEDED FROM "requests" TABLE.
			$request_id		 = $data[$tbl_request][0]['request_id'];
			$request_type_id = $data[$tbl_request][0]['request_type_id'];
			
			$this->insert_data(self::tbl_requests, $data[$tbl_request][0]);
			
			$tables = $this->_tables_for_request_types($request_type_id);
			
			foreach($tables as $key => $tbl_name):
				/*
				 * $table_name is from the api and with prefix 'ceis_'.
				 * $tbl_name is from the ceis database in intranet.
				 */
				$table_name  = 'ceis_'.$tbl_name;
			
			/* 	if(ISSET($data[$table_name][0])):
					
					
					$params	     = $data[$table_name][0];
					
					$this->insert_data($tbl_name, $params);
				endif; */
				
				if( ISSET($data[$table_name]) && ! EMPTY($data[$table_name]) ):
				
					$this->insert_data($tbl_name, $data[$table_name]);
				
				endif;
				  
			endforeach;
			
		}
		catch (PDOException $e){
			throw $e;
		}
		catch(Exception $e){
			throw $e;
		}
	}
	
	
	private function _tables_for_request_types($request_type_id)
	{
		$tables = array();
	
		switch($request_type_id):
		case REQUEST_TYPE_NEW:
		case REQUEST_TYPE_REN :
			//Contractor Information
			$tables[] =  self::tbl_request_contractor;
			$tables[] =  self::tbl_request_license_classifications;
			//Contracts
			$tables[] =  self::tbl_request_contracts;
			$tables[] =  self::tbl_request_projects;
			$tables[] =  self::tbl_request_services;
			//Staffing
			$tables[] =  self::tbl_request_staff;
			$tables[] =  self::tbl_request_staff_licenses;
			$tables[] =  self::tbl_request_staff_projects;
			$tables[] =  self::tbl_request_staff_experiences;
			//Finance
			$tables[] =  self::tbl_request_statements;
			//Construction Equipment
			$tables[] =  self::tbl_request_equipment;
			//Checklist
			$tables[] =  self::tbl_request_checklist;
			$tables[] =  self::tbl_request_attachments;
			break;
		case REQUEST_TYPE_PC :
			//Contractor Information
			$tables[] =  self::tbl_request_contractor;
			//Project Info / Owner
			$tables[] =  self::tbl_request_aut_projects;
			//Manpower Schedule
			$tables[] =  self::tbl_request_schedule;
			//Checklist
			$tables[] =  self::tbl_request_checklist;
			$tables[] =  self::tbl_request_attachments;
			break;
		case REQUEST_TYPE_MSC :
			//Contractor Information
			$tables[] =  self::tbl_request_contractor;
			//Project Info / Foreign Principal
			$tables[] =  self::tbl_request_aut_projects;
			//Manpower Schedule
			$tables[] =  self::tbl_request_schedule;
			//Contracts
			$tables[] =  self::tbl_request_contracts;
			$tables[] =  self::tbl_request_projects;
			$tables[] =  self::tbl_request_services;
			//Checklist
			$tables[] =  self::tbl_request_checklist;
			$tables[] =  self::tbl_request_attachments;
			break;
		case REQUEST_TYPE_EQP :
			//Construction Equipment
			$tables[] =  self::tbl_request_equipment;
			//Checklist
			$tables[] =  self::tbl_request_checklist;
			$tables[] =  self::tbl_request_attachments;
			break;
		default:
			throw new Exception('No request type defined!', EXCEPTION_CUSTOM);
			endswitch;
	
			return $tables;
	}
	
	public function insert_request_tasks($request_id, $request_type_id)
	{
		try 
		{ 
			$query = <<<EOS
				INSERT INTO %s
					(request_id, sequence_no, task_name, role_code, generate_reference_flag, get_task_flag, get_board_decision, generate_order_of_payment, evaluation_flag, task_status_id)
				
				SELECT ?, sequence_no, task_name, role_code, generate_reference_flag, get_task_flag, get_board_decision, generate_order_of_payment, evaluation_flag, ? 
					FROM  %s
					WHERE request_type_id = ?
					ORDER BY sequence_no ASC
EOS;
			$query = sprintf($query, self::tbl_request_tasks, self::tbl_param_request_tasks);
			
			$this->query($query, array($request_id, REQUEST_TASK_NOT_YET_STARTED, $request_type_id), NULL, TRUE);
		}
		catch (PDOException $e){
			throw $e;
		}
		catch(Exception $e){
			throw $e;
		}
	}
}
