<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Notifications_model extends SYSAD_Model {
    
	var $notifications_table = "notifications";
	
	public function insert_notification($params)
	{
		try
		{
			$val = array(
					"notification" => $params["notification"],
					"notified_by" => $this->session->user_id,
					"notification_date" => date("Y-m-d H:i:s")
				);
			
			if(ISSET($params["notify_users"])){
				$notify_users = implode(",",$params["notify_users"]);
				$val["notify_users"] = $notify_users;
			}	
				
			if(ISSET($params["notify_orgs"])){
				$notify_orgs = implode(",",$params["notify_orgs"]);
				$val["notify_orgs"] = $notify_orgs;
			}	
				
			if(ISSET($params["notify_roles"])){
				$notify_roles = implode(",",$params["notify_roles"]);
				$val["notify_roles"] = $notify_roles;
			}
				
			$this->insert_data($this->notifications_table, $val);
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}
	}
		
}

/* End of file notifications_model.php */
/*/application/models/notifications_model.php*/
