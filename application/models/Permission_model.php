<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Models::cias_permission
 * @author Asiagate Networks, Inc
 * @email support@asiagate.com
 *
 * CIAS Permission 
 */

class Permission_model extends CIAS_Model {

	private $tbl_param_role = 'cias_param_role';
	
	public function __construct() {
		parent::__construct();
 
	}

	public function get_application_by_role($role_code)
	{

		$query = <<<EOS
			SELECT a.application_code, b.intranet_app_flag, b.url, b.app_group
			FROM   cias_param_role a
				JOIN cias_param_application b
					ON a.application_code = b.application_code
			WHERE  a.role_code = ?
EOS;
		return $this->query($query, array($role_code));
	}
	
	
	public function get_resource_by_resource_code($resource_code)
	{
		
		$param = array();
		for($ctr = 0; $ctr < count($resource_code); $ctr++){
			$param[] = "?";			
		}
		$param = implode(',', $param);
		
		$query = " 
			SELECT application_code, resource_code, resource_name , icon, display_child_flag
			FROM cias_param_resource 
			WHERE resource_code IN ( ".$param." ) 
			ORDER BY sort_no ASC
			";
			

		
		return $this->query($query, $resource_code);
		/*
		$stmt = $this->db->prepare($query);            
		$stmt->execute($resource_code);
            
		return $stmt->fetchAll(PDO::FETCH_ASSOC);			
		*/
	}
	
	public function get_resource_by_role($application_code, $role_code, $main_flag, $parent_menu=NULL)
	{
		$param = array();
		$param[]	= $application_code;
		$param[]	= $role_code;
		$param[]	= $main_flag;
				
		$param_str	= "";
		if($parent_menu != NULL)
		{
			$param[] 	= $parent_menu;
			$param_str	= " AND a.parent_resource_code = ? ";
		}
		$query = <<<EOS
			SELECT a.resource_code, a.resource_name, a.icon
			FROM cias_param_resource a, cias_permission b
			WHERE a.resource_code = b.resource_code
			AND a.application_code = ?
			AND b.role_code = ? 
			AND a.main_flag = ?
			$param_str
EOS;

	
		return $this->query($query, $param);		
/*
		$stmt = $this->db->prepare($query);            
		$stmt->execute($param);

		echo $query;
print_R($param);
die();
            
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
		*/
	}
	
	public function get_permission($resource_code) {
            
		$query = <<<EOS
			SELECT resource_code, role_code 	
			FROM cias_permission
			WHERE resource_code = ?
			 
EOS;
          
          	return $this->query($query, array($resource_code));	

          /*  
		$stmt = $this->db->prepare($query);            
		$stmt->execute(array($resource_code));
            
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
		*/
	}
	
	
	public function get_permission_action($resource_code, $action_code) {
            
		$query = <<<EOS
			SELECT resource_code, action_code, role_code 	
			FROM cias_permission_action
			WHERE resource_code = ?
			AND action_code = ?
			 
EOS;
          
         return $this->query($query, array($resource_code, $action_code));	
        /*  
		$stmt = $this->db->prepare($query);            
		$stmt->execute(array($resource_code, $action_code));
            
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
		*/
	}	
    
}



/* End of file cpis_evaluators.php */
/*/application/modules_cpis/cpis_evaluators/models/cpis_evaluators.php*/
