var General = function(){

	var ajax_request = function(options){
		/* Sets of predefined config for ajax request */
		var predefined = {
				dataType   : "json",
				method     : "POST",
				beforeSend : function(){
					//no ui block in core General.showLoading();
				},
				complete   : function(jqXHR, status){
					//no ui block in core  General.hideLoading();
				}
		}
		/* Merge the two configs */
	   var final_option = $.extend(predefined, options);
		
	   return $.ajax(final_option);
	};
	
	var show_loading = function(){
		$('#ajax-loading').modal({keyboard : false});
	};
	
	var hide_loading = function(){
		$('#ajax-loading').modal('hide');
	};
	
	var reset_btn = function($btn){
		if(typeof $btn == 'string'){
			$btn = $('#'+$btn);
		}else if(typeof $btn == 'object' && $btn instanceof jQuery){
			
		}else{
			console.log('wrong type passed for $btn!');
			return false;
		}
		
		setTimeout(function(){
			$btn.button('reset');
		}, 1000);
	};
	
	var load_datatable = function(id, options, reload){
		
		if(reload){
			$('#'+id).dataTable().fnReloadAjax();
		}else{
			/* Sets of predefined config for datatable */
			var predefined = {
				"sEcho"    		 : 1,
				"bProcessing"	 : true,
				"bDestroy"	     : true,
				"oLanguage"		 : {"sEmptyTable": "No records found"},
				"bServerSide"	 : true,
			    "sServerMethod"	 : "POST",
		        "sPaginationType": "full_numbers",
		    	"columnDefs"	 : [ { orderable: false, targets: -1 }],
				"fnDrawCallback" : function(){
					if( $( '.tooltipped' ).length !== 0 ) $('.tooltipped').tooltip({delay: 50});
					
					if( ModalEffects !== undefined && $( '.md-trigger' ).length !== 0 ) ModalEffects.re_init();
				},
			}
			/* Merge the two configs */
		   var final_option = $.extend(predefined, options);
			
			$('#'+id).dataTable(final_option);
		}
		
	};
	
	var alert_msg = function(id, type, msg){
		
		var $alert_failed  = $('#'+id).find('#alert_failed');
		var $alert_success = $('#'+id).find('#alert_success');
		var $failed_msg    = $('#'+id).find('#failed_msg');
		var $success_msg   = $('#'+id).find('#success_msg');
		
		switch(type)
		{
			case 0  :
				
				$alert_success.addClass('hide');
				
				if( $alert_failed.hasClass('hide') )
					$alert_failed.removeClass('hide');
				else
					$alert_failed.show();
				
				$failed_msg.html(msg);
				$hide_container = $alert_failed;
				
				break;
			case 1  :
				
				$alert_failed.addClass('hide');
				
				if( $alert_success.hasClass('hide') )
					$alert_success.removeClass('hide');
				else
					$alert_success.show();
				
				$success_msg.html(msg);
				$hide_container = $alert_success;
				
				break;
			default : 
				console.log('wrong type passed for alert msg');
				return false;
		}
		
		setTimeout(function(){
			$hide_container.hide('slow');
		}, 5000);
	};

	
	var scroll_up = function(alert_id){
		$('.scrollable').animate({scrollTop:$('#'+alert_id).offset().top - 20}, "slow");
	};
	
	var bind_parsley = function($form, validate_visible_only){
		if(typeof $form == 'string'){
			$form = $('#'+$form);
		}else if(typeof $form == 'object' && $form instanceof jQuery){
			
		}else{
			console.log('wrong type passed for form!');
			return false;
		}
		
		$form.parsley().destroy();
		
		if(validate_visible_only)
		{
			/*
				$parsley = $form.parsley('addListener', {
					onFieldValidate: function ( elem ) {
				        // if field is not visible, do not apply Parsley validation!
				        if ( !$( elem ).is( ':visible' ) ) {
				        	
				            return true;
				        }
	
				        return false;
				    }
				});
				
				
				
				$parsley = $form.parsley().on('field:validated',function(elem){
					console.log(this.$element);
				});
				
				
				return $parsley;
			 */
		}
			
		$parsley = $form.parsley();
		
		
		return $parsley;
	};
	
	var load_script = function(script_url){
		
		var options = {
			url      : script_url,
			async    : false,
		  	//cache    : true,
			dataType : 'script'
		};
				
		$.ajax(options);
	};
	
	/*
	 * Handles the jquery upload
	 * @param1 tbl_body string : id of the table body
	 * @param2 tbl_row  string : id of the table row
	 * @param3 elements object : object with the details of the elements to be cloned.
	 * Note Example value of @param3 { field_id : {required:true, type: 'select2'} }. type may be omitted.
	 */
	var add_row = function(tbl_body, tbl_row, elements){
		/* Initiliaze params
		 * $row  	: jquery object for table row to cloned.
		 * $body    : jquery object for table body which the new row will be appended.
		 * $cloned  : the cloned row. *notice i removed the style so that the hidden css will be remove to. 
		 */
		var $row    = $('#' + tbl_row);
		var $body   = $('#' + tbl_body);
		var $cloned = $row.clone().removeAttr('id class');
		
		/* Loop the elements inside the cloned row */
		$cloned.find(':input, a').each(function(){
			/* Gets the id of the current element in the loop */
			var id = $(this).attr('id');
			
			/* Assign a name for the element. *notice will just use the id of element and add [] */
			$(this).attr('name', id+'[]');
			
			/* Check if element_id is defined in the elements */
			if(elements.hasOwnProperty(id)){
			
				/* Get the requirement for this element */
				var requirements = elements[id];
				
				/* If element is required bind data-parsley-required */
				if(requirements.hasOwnProperty('required')){					
					$(this).attr('data-parsley-required', true);
				}
				
				/* If element has type property check what jquery plugin should be bind */
				if(requirements.hasOwnProperty('type')){
					switch(requirements.type)
					{
						case 'select2' :
							//$(this).removeClass('selectize');
							$(this).selectize();
						break;
						case 'datepicker' :
							$(this).datepicker();
						break;
						case 'delete' :
							$(this).on('click',function(){
								$(this).closest("tr").remove();
							});
						break;
					}
				}
			}
		});
		
		/* Append the cloned table row to the table body */
		$body.append($cloned);
	};
	
	/*
	 * Binds plugins like datepicker, select and etc..
	 * Note options will be default
	 * Not yet finished 
	 */
	var bind_plugins = function($form, $options){
		if(typeof $form == 'string'){
			$form = $('#'+$form);
		}else if(typeof $form == 'object' && $form instanceof jQuery){
			
		}else{
			console.log('wrong type passed for form!');
			return false;
		}
		/*
		Used in ciap portal
		$datepickers = $form.find('.datepicker');
		$selects	 = $form.find('.select2');
		
		if($selects.length > 0) $selects.select2();
		
		if($datepickers.length > 0) $datepickers.datepicker();
		*/
		
		$datepickers = $form.find('.datepicker');
		
		if($datepickers.length > 0){
			$datepicker_options = { timepicker:false, scrollInput: false, format:'m/d/Y', formatDate:'m/d/Y' };
			
			$datepickers.datetimepicker($datepicker_options);
		}else{
			
		}
	};
	
	var get_form_data = function($form){
		if(typeof $form == 'string'){
			$form = $('#'+$form);
		}else if(typeof $form == 'object' && $form instanceof jQuery){
			
		}else{
			console.log('wrong type passed for form!');
			return false;
		}
		
		$data  = $form.find('input[type=\'hidden\'], select:visible, textarea, input:visible').serialize();
		$data += '&';
		//Specifically made for select2 plugin
		$data += $form.find('select').filter(function(index, elem){
					return (  ! $(this).is(':visible') && $(this).parent().is(':visible') ) ? true : false; 
				 }).serialize();
			
		return $data;	
	};
	
	var load_upload = function(elem, options){
		/* initialize load function */
		function load_func(obj){
		  var filename = (elem.hasOwnProperty('input_id')) ? $('#'+elem.input_id).val() : elem.filename;
		  
		  $.ajax({
				cache	: true,
				url		: base_url + "upload/existing_files/",
				dataType: "json",
				method  : "POST",
				data	: { dir: elem.path, file: filename} ,
				success : function(data) 
				{	
					for(var i=0;i<data.length;i++){
						obj.createProgress(data[i]["name"],data[i]["path"],data[i]["size"]);
					}	
					
					if( ! final_options.showDelete){
						$(".ajax-file-upload-progress").hide();
						$(".ajax-file-upload").hide();
					}
				}
			});
		}
		/* initialize delete function */
		 function delete_func(data,pd){
				var filename  = data.toString();
				var id        = $('#id').val();
				var action    = $('#action').val();
				var salt  	  = $('#salt').val();
				var token  	  = $('#token').val();
				var arguments = {op : "delete", filename: data[0], dir:elem.path, action:action, id:id, salt:salt, token:token};
				
				for_delete.push(filename);
				 
				$.post(base_url + elem.delete_url,arguments,
					function(resp, textStatus, jqXHR)
					{ 
						var existing_files = JSON.parse($('#'+elem.input_id).val());
						var array_index    = $.inArray(filename, existing_files);
						
						existing_files.splice(array_index, 1);
						
						existing_files = ( existing_files.length == 0 ? '' : JSON.stringify(existing_files))
						
						$(".ajax-file-upload-error").fadeOut();	
						
						$('#'+elem.input_id).val(existing_files);
						
						$('#'+elem.delete_input_id).val(JSON.stringify(for_delete));
					}
				);
				
				pd.statusbar.hide();
			}
		/* initialize success function */
	   function success_func(files,data,xhr, pd){
		     var filename 		= data.toString();
			 var existing_files =  $('#'+elem.input_id).val();
			 
			 if(existing_files == '' || existing_files == '["36: 1"]'){
				 var array = [];
			 }else{
				 var array = JSON.parse(existing_files);
			 }
			 
			 array.push(filename);
			 
			 $('#'+elem.input_id).val(JSON.stringify(array));
		}
	   /* initialize error function */
	    function error_func(files, status, errMsg, pd)
		{
			pd.preview.hide();
		}
	   
	    
	    /* initialize array for files that are candidate for delete */
		var  for_delete = [];
		/* Define the options that are required */
		var required 	= {url : 'string', formData: 'object'};
		/* Define the element details that are required 
		 * id, salt, token, action : this are all required for delete purposes
		 * input_id   : id of input element where the filenames are placed 	   		    
		 * delete_url : path for the delete ajax 
		 * delete_input_id : id of input element where the filenames that are candidate for delete are placed 
		 * */
		/* predefined options for upload plugin */
		var predefined  = {
	   			url			    : $base_url + "upload/",
				fileName	    : "file",
				//acceptTypes	    : "*",	
				allowedTypes	: "*",
				acceptFiles	    : "*",	
				dragDrop	    : false,
				allowDuplicates : true,
				duplicateStrict : false,
		        showDone	    : false,
		        showProgress    : false,
		        showFileSize    : false,   
				returnType	    : "json",	
				showDelete	    : true,
				multiple 	    : false,
				showPreview    	: true,
				onSuccess    	: success_func,
				deleteCallback  : delete_func,
				onLoad    		: load_func,
				onError         : error_func
				
	   	  }; 

		/* Merge the options defined by the user. */
		var final_options = $.extend(predefined, options);
		
	    /* Check if required properties exist and datatype is correct */
		for(var name in required){
			if( ! predefined.hasOwnProperty(name)){
				console.log('property does not exist! '+name);
			}else{
				if( ! (typeof final_options[name] == required[name]))
					console.log('Wrong data type for '+ name);
			}
		}
		
	    
	    
	    return $('#'+elem.id).uploadFile(final_options);
	};
	
	var is_empty  = function(value){
		var empty = (value) ? 0 : 1;
		
		if( ! empty && typeof(value) == 'string'){
			var lowered		= value.toLowerCase();
			var invalid_str = ['null', 'undefined', 'nan']; 
			
			empty       	= ($.inArray( lowered, invalid_str ) < 0) ? 0 : 1;
		}
		
		return empty;
	};

	var disable_fields = function($form){
		if(typeof $form == 'string'){
			$form = $('#'+$form);
		}else if(typeof $form == 'object' && $form instanceof jQuery){
			
		}else{
			console.log('wrong type passed for form!');
			return false;
		}

		$readonly = $form.find('.readonly');
		
		$readonly.filter('.select2').select2('readonly', true);
		$readonly.not('.select2').prop('readonly', true);
	};
	
	var close_modal  = function(){
		var modal_id = '#' + $('div.md-show').attr('id');
		
		$(modal_id).removeClass('md-show');
	}
	
	/*
	 * return obj
	 */
	return {
		showLoading : function(){
			show_loading();
		},
		hideLoading : function(){
			hide_loading();
		},
		ajax : function(options){
			return ajax_request(options);
		},
		resetBtn : function($btn){
			reset_btn($btn);
		},
		loadDatatable: function(id, options){
			load_datatable(id, options, false);
		},
		reloadDatatable: function(id){
			load_datatable(id, {}, true);
		},
		alertMsg: function(id, type, msg){
			alert_msg(id, type, msg);
		},
		scrollUp : function(alert_id){
			scroll_up(alert_id);
		},
		bindParsley : function(form, visible_only){
			var validate_visible_only = (visible_only !== undefined) ? visible_only : true;

			return bind_parsley(form, validate_visible_only);
		},
		loadScript : function(url){
			load_script(url);
		},
		addRow : function(tbl_body, tbl_row, elements){
			add_row(tbl_body, tbl_row, elements);
		},
		bindPlugins : function($form, $params){
			$options = ($params !== undefined) ? $params: {};
			
			bind_plugins($form, $options);
		},
		getFormData : function($form){
			return get_form_data($form);
		},
		initUpload : function(elem, options){
			return load_upload(elem, options);
		},
		isEmpty : function(value){
			return is_empty(value);
		},
		disableFields : function($form){
			disable_fields($form);
		},
		closeModal : function()
		{
			close_modal();
		}
	};
}();