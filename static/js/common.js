var $base_url = $("#base_url").val();
	$loader = '&nbsp;&nbsp;<img src="'+ $base_url +'static/images/ajax-loader-bar-black.gif"/>';

function button_loader(id, active){
	var loading_bar = (active === 1) ? $loader : "",
		btn = $("#" + id),
		active_btn_label = btn.val(),
		default_btn_label = btn.html();
	
	if(active){
		btn.html(active_btn_label + loading_bar); //updating
		btn.attr('disabled','disabled');
		btn.val(default_btn_label); // update
	} else {
		btn.removeAttr('disabled');	
		btn.html(btn.val()); // update
		btn.val(default_btn_label.split("&nbsp;")[0]);
	}
}

function content_form(controller, module, id){
	var module = module || "";
		id = id || "";
		path = module + "/" + controller + "/" + id;
		
	window.location.href = $base_url + path;
}

function content_delete(alert_text, param_1, param_2){
	var param_2 = param_2 || "";
		
	$('#confirm_modal').confirmModal({
		topOffset : 0,
		onOkBut : function() {
			deleteObj.removeData({ param_1 : param_1, param_2 : param_2 });
		},
		onCancelBut : function() {},
		onLoad : function() {
			$('.confirmModal_content h4').html('Are you sure you want to delete this ' + alert_text + '?');	
			$('.confirmModal_content p').html('This action will permanently delete this record from the database and cannot be undone.');
		},
		onClose : function() {}
	});
}

function alert_msg(type, msg){
	Materialize.toast(msg, 5000, type);
}

function notification_msg(type, msg){
	$(".notify." + type + " p").html(msg);
	$(".notify." + type).notifyModal({
		duration : -1
	});
}

function table_export(id, type, exclude, escape){
	var escape = escape || 'false';
		ignore_col = exclude || "";
		arr = "";
		
	if(ignore_col != ""){
		var ignore_col = ignore_col.split(",");
			arr = ignore_col.map(Number);
	}
		
		
	$('#' + id).tableExport({
		type:type,
		escape:escape,
		ignoreColumn: arr
	});
}

function modal_init(data_id){
	var data_id = data_id || '',
		jscroll;
	
	jscroll 	= modalObj.checkIfScroll();
	
	if(jscroll){
	  modalObj.loadViewJscroll({ id : data_id });
	}else{
	  modalObj.loadView({ id : data_id });
	}
	
	return false;
}

function set_active_tab(module){
	var active_tab = window.location.hash.replace('#',''),
		controller = active_tab.replace('tab_','');
	
	load_index(active_tab, controller, module);
}

function load_index(id, controller, module){
	var path = module + "/" + controller;
	
	$(".tab-content").not("#"+id).html("");
	$("#"+id).load($base_url + path).fadeIn("slow").show();
	window.location.hash = id;
}

function search_wrapper(id, input, items, highlight){
	var highlight = highlight || "";
	var highlight = (highlight === "") ? true : false;
	$(id).lookingfor({
		input: $(input),
		items: items,
		highlight: highlight
	});
}

function sort(id, order_by, wrapper, action, input_id, item){
	var id = id.attr('id'),
		up_ico = 'flaticon-up151',
		down_ico = 'flaticon-down95';
	
	// RESET ACTIVE BUTTONS
	$('.sort-btn').not('#' + id).removeClass('active').find("i").switchClass(up_ico, down_ico, 1000, "easeInOutQuad" );
	
    $('#' + id).toggleClass('active');

    if(($('#' + id).hasClass('active'))){
		var order = 'ASC',
			class_from = down_ico,
			class_to = up_ico;
    } else {
		var order = 'DESC',
			class_from = up_ico;
			class_to = down_ico;
    }
    
	filter(order_by, order, wrapper, action, input_id, item);
	$('#' + id).find("i").switchClass(class_from, class_to, 1000, "easeInOutQuad" );
}

function filter(filter_1, filter_2, wrapper, action, input_id, item){
	
	var data = {filter_1: filter_1, filter_2: filter_2};

	$(wrapper).isLoading();
	
	$.post($base_url + action, data, function(result){
	  $(wrapper).isLoading("hide").html(result);
	  search_wrapper(wrapper, input_id, item, 1);
	},'json');
}

function load_datatable(table_id, path, scrollX){

	var scrollX = scrollX || "",
		modal = modal || false,
		order = (table_id == 'audit_log_table') ? 3 : 0;
		
	
	var table = {
		table_id : function(){ 
	
			$("#"+table_id).dataTable(
				{
					"bDestroy": true,
					"bProcessing": true,
					"bServerSide": true,
					//"bPaginate": false,
					"sPaginationType": "full_numbers",
					"scrollX": scrollX,
					"fixedColumns": {
						"leftColumns": 1,
						//"rightColumns": 1
					},
					"sAjaxSource": $base_url + path, 
					"order": [[ order, "asc" ]],
					"fnServerData": function ( sSource, aoData, fnCallback ) {
						$.ajax( {
							"dataType": 'json', 
							"type": "POST", 
							"url": sSource, 
							"data": aoData, 
							"success": function(result) {
								
								if(result.flag == 0)
								{
									notification_msg("ERROR", result.msg);		
								}
								
								fnCallback(result);
							}
						} );	
					},
					/* START: Doc's code */
					fnDrawCallback : function(){
						if( $( '.tooltipped' ).length !== 0 )
						{
							$('.tooltipped').tooltip({delay: 50});
						}
						if( ModalEffects !== undefined && $( '.md-trigger' ).length !== 0 )
						{
							ModalEffects.re_init();
						}
						
					},
					/* END: Doc's code */
					columnDefs: [
						{ orderable: false, targets: -1 }
					]
				}
			);
		}
	}

	table.table_id();
}

function load_datatable2(options){

	//PARSE DATATABLE OPTIONS
	if(typeof(options) == 'string')
		var options = $.parseJSON(options);

	console.log(options);
	//PARSE DATATABLE SERVER PARAMETERS
	if(typeof(options.fnServerParams) != 'undefined' && options.fnServerParams){	
		var serverdata = $.parseJSON(options.fnServerParams);
		options.fnServerParams = function ( aoData ) {
	    	aoData.push(serverdata);
	    }
	}

	var scrollX = scrollX || "",
		modal = modal || false,
		order = (options.table_id == 'audit_log_table') ? 3 : 0;

	var defaults = {
		"bDestroy"		: true,
		"bProcessing"	: true,
		"bServerSide"	: true,
		"bPaginate"		: true,
		"scrollX"		: scrollX,
		"pageLength"	: 10,
		"fixedColumns": {
			"leftColumns": 1,
		},
		"sAjaxSource": '',
		
		"order": [[ order, "asc" ]],
		"fnServerData": function ( sSource, aoData, fnCallback ) {
			$.ajax( {
				"dataType": 'json', 
				"type": "POST", 
				"url": sSource, 
				"data": aoData, 
				"success": function(result) {
					
					if(result.flag == 0)
					{
						notification_msg("ERROR", result.msg);		
					}
					
					fnCallback(result);
				}
			} );	
		},
		
	
		/* START: Doc's code */
		fnDrawCallback : function(){
			if( $( '.tooltipped' ).length !== 0 ){
				$('.tooltipped').tooltip({delay: 50});
			}
			if( ModalEffects !== undefined && $( '.md-trigger' ).length !== 0 ){
				ModalEffects.re_init();
			}
		},
		/* END: Doc's code */
		columnDefs: [
			{ orderable: false, targets: -1 }
		]
	}
	var config = $.extend( {}, defaults, options );
	
	// LOAD DATATABLE	
	$("#"+options.table_id).dataTable(config);
}

function generate_report(controller, extension, filter){
	var filter = filter || "",
		url = $base_url + controller + "/" + extension + "/" + filter;
	
	window.open(url, '_blank');
}

/* START: Doc's code */
function selectize_init()
{
	if(  $( 'select.selectize' ).length !== 0 )
	{
		$( 'select.selectize' ).each( function() {
			
			if( !$(this).hasClass( 'selectized' ) )
			{
				$(this).selectize();
			}
		} )
	}

	if( $( 'select.tagging' ).length !== 0 )
	{
		$( 'select.tagging' ).each( function() {

			if( !$(this).hasClass( 'selectized' ) )
			{
				$(this).selectize({
					createOnBlur: true,
				    create: true,
				    delimiter: ',',
				    persist: false
				});
			}

		} );
	}

	if( $( 'select.tagging-max' ).length  !== 0 )
	{
		$(  'select.tagging-max' ).each( function() {
			if( !$(this).hasClass( 'selectized' ) )
			{
				$(this).selectize({ maxItems: 3 });
			}
		} )
	}
}

function datepicker_init()
{
	$('.datepicker').datetimepicker({
		timepicker:false,
		scrollInput: false,
		format:'m/d/Y',
		formatDate:'m/d/Y'
	  });
		
	$('.timepicker').datetimepicker({
		datepicker:false,
		format:'h:i A'
	});
		
	$('.datepicker_start').datetimepicker({
		format:'m/d/Y',
		formatDate:'m/d/Y',
		scrollInput: false,
		onShow:function( ct ){
		  this.setOptions({
			maxDate:jQuery('.datepicker_end').val()?jQuery('.datepicker_end').val():false
		  })
		},
		timepicker:false
	});
		
	$('.datepicker_end').datetimepicker({
		format:'m/d/Y',
		formatDate:'m/d/Y',
		scrollInput: false,
		onShow:function( ct ){
			this.setOptions({
				minDate:jQuery('.datepicker_start').val()?jQuery('.datepicker_start').val():false
			})
		},
		timepicker:false
	});

	if($('.datepicker,.datepicker_start,.datepicker_end,.timepicker').length === 0)
	$('.datepicker,.datepicker_start,.datepicker_end,.timepicker').datetimepicker('destroy');
}
/* END: Doc's code */

/* START: Heily's code */
function collapsible_init(){
	$('.collapsible').collapsible({
		accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
	});
}
/* END: Heily's code */

/* usage onkeyup="javascript:this.value=Comma(this.value);" */
function Comma(Num) {

	//-- FOR BAS OBS

	if($('.controller').data('controller')){
		$.post($base_url+'bas/'+$('.controller').data('controller')+'/array_sum_test', $('#form').serialize(), function(amount){
			$(".show_type_total").html(amount);
		});
	}

    //MAIN PROCESS

    Num += '';
    Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
    Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
    x = Num.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1))
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    return x1 + x2;
}
/* end */


/* reload selectice options
 *
 * @param selectice_id: selectized select tag id
 * @param options: new selecticed options ex. [{id: id, name: name},..]
 */
function selectize_reload(selectize_id, options){
        var htmldata = '';
        var new_value_options   = '[';
            for (var key in options) {
                htmldata += '<option value="'+options[key].id+'">'+options[key].name+'</option>';

                var keyPlus = parseInt(key) + 1;
                if (keyPlus == options.length) {
                    new_value_options += '{text: "'+options[key].name+'", value: '+options[key].id+'}';
                } else {
                    new_value_options += '{text: "'+options[key].name+'", value: '+options[key].id+'},';
                }
            }
            new_value_options   += ']';

        //convert to json object
        new_value_options = eval('(' + new_value_options + ')');
        
        if (new_value_options[0] != undefined) {
            // re-fill html select option field 
            $("#"+selectize_id).html(htmldata);
            // re-fill/set the selectize values
            var selectize = $("#"+selectize_id)[0].selectize;

            selectize.clear();
            selectize.clearOptions();
            selectize.renderCache['option'] = {};
            selectize.renderCache['item'] = {};
            selectize.addOption(new_value_options);
            selectize.setValue(new_value_options[0].value);
        }
    }