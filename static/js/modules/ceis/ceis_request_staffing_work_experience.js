var CeisRequestStaffingWorkExperience = function(){

	var table_name = 'request_staffing_experience';

	var init = function(){
		var option = {
				sAjaxSource : $base_url + 'ceis/ceis_request_staffing/get_staff_work_exp_list/' +  LoadTab.getAdditionalInfo(),
		};
			
		General.loadDatatable(table_name, option);
		
	}

	return {
		init: function(){
			init();
		}
	};
	
}();
