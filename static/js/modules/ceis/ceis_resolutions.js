var CeisResolutions = function(){
	
	var init = function(){

		jQuery(document).off('click', '.delete-resolution');		
		jQuery(document).on('click', '.delete-resolution', function(event){

			event.preventDefault();			

			//gets value from module resolutions
			var resolution_no = $(this).data('resolution');

			if(confirm('Do you want to delete this record?'))
			{
				
		        var option = {
		          url  : $base_url + 'ceis/ceis_resolutions/delete_resolutions/',
		          data : {resolution_no: resolution_no},
		          success : function(response){

		            alert(response.msg);    

		            if(response.flag == 1)
		            {
			            var table_option = {				
							sAjaxSource	: $base_url + "ceis/ceis_resolutions/get_resolution_list",
						};

						General.loadDatatable('tbl_resolutions', table_option);	

						// if error occurred prevent close modal

		            }
		          }
		        };

		        General.ajax(option);
			}
		});

	};

	var modal = function(){
		//script for datepicker
	    $('.datepicker-input').datepicker();
	    $('.datepicker').css('z-index',9999);

	    //script for parsley validation
	    $('#resolutions_form').parsley();

	    //script for submitting 
	    $('#resolutions_form').submit(function(event) {

	      event.preventDefault();

	      if ( $(this).parsley().isValid() ) {
	        var data = $(this).serialize();

	        var option = {
	          url  : $base_url + "ceis/ceis_resolutions/process",          
	          data : data,
	          success : function(response){
	            alert(response.msg);  

	            // IF STATEMENTS (validation)
	            var table_option = {        
	              sAjaxSource : $base_url + "ceis/ceis_resolutions/get_resolution_list",
	            };

              	General.loadDatatable('tbl_resolutions', table_option);  

	            // CLOSE MODAL
				$.modalStack.closeActive(); 
	          }
	        };

	        General.ajax(option);
	        }
	      });
		};

	return {
		init : function(){
			init();	
		},	

		modal : function(){
			modal();
		},

		//load function inside the controller
		load : function(){
			var option = {				
				sAjaxSource	: $base_url + "ceis/ceis_resolutions/get_resolution_list",
			};

			//load datatable for table id is tbl_resolutions
			General.loadDatatable('tbl_resolutions', option);
		}
	};

}();
