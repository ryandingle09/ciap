var LoadTab = function()
{
	var id 			 = $("#security").val();
	var $cpage       = $('#cpage');
	var $crequest_id = $('#crid');
	var $cadd_info   = $('#add_info');
	
	var load_tab = function(tab, container)
	{	
		/*data     : { 
			page 	   : page_name, 
			request_id : RequestModule.getRequestId(),
			add_info   : RequestModule.getAdditionalInfo()
		},*/
		$.post($base_url + 'ceis/ceis_requests/load_tab', {page : tab, security : id, additional_info : LoadTab.getAdditionalInfo() }, function(response){			
			$(response.container).html(response.html);
			
			LoadTab.tagLabelActive();
			
			$("div.list-basic a ").removeClass('grey darken-1 white-text');
			$("#menu_" + tab).addClass('grey darken-1 white-text');
			
			$cpage.val(tab);
		}, 'json');
	};
	
	var get_request_id  = function(type){

		var request_id = $crequest_id.val();
		
		switch(type)
		{
			case 'query':
				return '&request_id=' + request_id;
			break;
			default     : return request_id;
		}
	};
	
	var tag_label_active = function(){
		
		$('form input').each(function(index){
			$this = $(this);
			
			if($this.val()) $this.siblings('label').addClass('active');
		});
	}
	
	var current_page = function(){
	
		return $cpage.val();
	}
	
	/* Gets the value of the hidden add_info
	 * Note : returns data in query string format 
	 * */
	var additional_info = function(type){
		var add_info = $cadd_info.val();
		switch(type)
		{
			case 'query':
				return '&add_info=' + add_info;
			break;
			default     : return add_info; 
		}

	};

	var set_additional_info = function(value){
		$cadd_info.val(value);
	};
	
	return {
		init : function(){
			page = current_page();
			
			load_tab(page, '.tab-content');
		},
		getPage : function(page){
			load_tab(page, '.tab-content');
		},
		init_status_tab : function(tab){			
			load_tab(tab, '#tab_status_content');
		},
		getRequestId : function(type){
			return get_request_id(type);
		},
		tagLabelActive : function(){
			tag_label_active();
		},
		setAdditionalInfo : function(data){
			set_additional_info(data);
		},
		getAdditionalInfo : function(){
			return additional_info();
		}
	};
	
}();
