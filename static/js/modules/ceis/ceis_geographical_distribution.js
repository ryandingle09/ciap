var CeisGeographicalDistribution = function(){
	
	var init = function(){

		jQuery(document).off('click', '.delete-geographical');		
		jQuery(document).on('click', '.delete-geographical', function(event){

			event.preventDefault();			

			//gets val
			var geo_region_id = $(this).data('geographical');

			if(confirm('Do you want to delete this record?'))
			{
				
		        var option = {
		          url  : $base_url + 'ceis/ceis_geographical_distribution/delete_geographical/',
		          data : {geo_region_id: geo_region_id},
		          success : function(response){

		            alert(response.msg);    

		            if(response.flag == 1)
		            {
			            var table_option = {				
							sAjaxSource	: $base_url + "ceis/ceis_geographical_distribution/get_geo_list",
						};

						General.loadDatatable('tbl_geographical_distribution', table_option);

						// if error occurred prevent close modal

			            // CLOSE MODAL
						$.modalStack.closeActive();	
		            }
		          }
		        };
		        General.ajax(option);
			}
		});
	};

	var modal = function(){

		//for JS_SELECT (dropdown with search)
	    $("#country_name").select2({
        width : '100%'
      });

      $('#geographical_form').parsley();
      $('#geographical_form').submit(function(event) {

      event.preventDefault();

      if ( $(this).parsley().isValid() ) {
        var data = $(this).serialize();

        var option = {
          url  : $base_url + "ceis/ceis_geographical_distribution/process",          
          data : data,
          success : function(response){
            alert(response.msg); 

             // IF STATEMENTS (validation)
            var table_option = {        
              sAjaxSource : $base_url + "ceis/ceis_geographical_distribution/get_geo_list",
            };

              General.loadDatatable('tbl_geographical_distribution', table_option);    
          }
        };

        General.ajax(option);
        }
      });
	};

	return {
		init : function(){
			init();	
		},

		modal : function(){
			modal();
		},

		//load function inside the controller
		load : function(){
			var option = {				
				sAjaxSource	: $base_url + "ceis/ceis_geographical_distribution/get_geo_list",
			};

			//load datatable for table id is tbl_geographical_distribution
			General.loadDatatable('tbl_geographical_distribution', option);
		}
	};

}();
