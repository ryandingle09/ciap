var CeisDirectory = function(){
	

  var initpdf = function(){
   // var data = $("#report_form").serialize();

    var option = {

      url  : $base_url + 'ceis/ceis_directory/generate_report/',
      success : function(response){

        if( response.success )
        {
          $("body").isLoading("hide");
          var modal = response.modal;
          classie.add( $( '#modal_directory_report' )[0], 'md-show' );
          
          modal_directory_report.loadViewJscroll({
            ajax_modal  : modal, 
            height      : modal_height['modal_directory_report']
          });
        
          $( '.md-close' ).on( 'click', function() {
            classie.remove( $( '#modal_directory_report' )[0], 'md-show' );
          }); 

        }
      }
    };

    General.ajax(option);    
  };

  var initexcel = function(){
   // var data = $("#report_form").serialize();

    var option = {

      url  : $base_url + 'ceis/ceis_directory/generate_report_excel/',
      success : function(response){

        if( response.success )
        {
          $("body").isLoading("hide");
          var modal = response.modal;
          classie.add( $( '#modal_directory_report' )[0], 'md-show' );
          
          modal_directory_report.loadViewJscroll({
            ajax_modal  : modal, 
            height      : modal_height['modal_directory_report']
          });
        
          $( '.md-close' ).on( 'click', function() {
            classie.remove( $( '#modal_directory_report' )[0], 'md-show' );
          }); 

        }
      }
    };

    General.ajax(option);    
  };


	return {

    initpdf : function(){
      $( "body" ).isLoading({
            text:       "<div class='loader'></div>", 
            position:   "inside"
      });
      initpdf();
    },

     initexcel : function(){
     /* $( "body" ).isLoading({
            text:       "<div class='loader'></div>", 
            position:   "inside"
      });*/
      initexcel();
    },

	};

}();