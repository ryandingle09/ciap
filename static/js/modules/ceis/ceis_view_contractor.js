var LoadTab = function()
{
	var load_tab = function(default_tab,contract_id,progress_report_id,contr_staff_id,contr_staff_exp_id,contr_staff_proj_id,serial_no)
	{
		// grey darken-1 white-text

		var security = $("#security").val();
		$("div.list-basic a ").removeClass('grey darken-1 white-text');
		$("#menu_" + default_tab).addClass('grey darken-1 white-text');
		$('.tooltipped').tooltip('remove');

		$.post($base_url + 'ceis/ceis_contractors/load_tab', {tab : default_tab, security : security, contract_id : contract_id, progress_report_id : progress_report_id, contr_staff_id : contr_staff_id , contr_staff_exp_id : contr_staff_exp_id, contr_staff_proj_id : contr_staff_proj_id, serial_no : serial_no}, function(html){
			
			$(".tab-content").html(html);

		});

	};

	return {
		
		init : function(default_tab,contract_id,progress_report_id,contr_staff_id,contr_staff_exp_id,contr_staff_proj_id,serial_no)
		{
			load_tab(default_tab,contract_id,progress_report_id,contr_staff_id,contr_staff_exp_id,contr_staff_proj_id,serial_no);
			
		}/*,

		rel : function()
		{
			var table_option = {				
				sAjaxSource	: $base_url + "ceis/ceis_contractors/get_contract_project_list",
			};

			General.loadDatatable('table_project', table_option);
		}*/
		
	};

}();
