var CeisProjectTypes = function(){
	
	var init = function(){

		jQuery(document).off('click', '.delete-projecttypes');		
		jQuery(document).on('click', '.delete-projecttypes', function(event){

			event.preventDefault();			

			//gets val
			var project_type_code = $(this).data('projecttypes');

			if(confirm('Do you want to delete this record?'))
			{
		        var option = {
		          url  : $base_url + 'ceis/ceis_project_types/delete_project_types/',
		          data : {project_type_code: project_type_code},
		          success : function(response){

		            alert(response.msg);    

		            if(response.flag == 1)
		            {
			            var table_option = {				
							sAjaxSource	: $base_url + "ceis/ceis_project_types/get_project_types_list",
						};

						General.loadDatatable('tbl_project_types', table_option);	

						// if error occurred prevent close modal

		            }
		          }
		        };

		        General.ajax(option);
			}
		});

	};

	var modal = function(){

	 //script for parsley validation
    $('#project_type_form').parsley();

    //script for submitting 
    $('#project_type_form').submit(function(event) {

      event.preventDefault();

      if ( $(this).parsley().isValid() ) {
        var data = $(this).serialize();

        var option = {
          url  : $base_url + "ceis/ceis_project_types/process",          
          data : data,
          success : function(response){
            alert(response.msg);  
            // IF STATEMENTS (validation)
            var table_option = {        
              sAjaxSource : $base_url + "ceis/ceis_project_types/get_project_types_list",
            };

              	General.loadDatatable('tbl_project_types', table_option); 

		        // CLOSE MODAL
				$.modalStack.closeActive();

          }
        };

        General.ajax(option);
        }
      });
	};

	return {
		init : function(){
			init();	
		},	

		modal : function(){
			modal();
		},
		//load function inside the controller
		load : function(){
			var option = {				
				sAjaxSource	: $base_url + "ceis/ceis_project_types/get_project_types_list",
			};

			//load datatable for table id is tbl_project_types
			General.loadDatatable('tbl_project_types', option);
		}
	};

}();
