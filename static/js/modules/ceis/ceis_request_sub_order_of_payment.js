var CeisRequestSubOrderOfPayment = function(){
	/*var $form 	 = $('#evaluation_form'); 
	var btn_name = 'save_evaluation';
	var $btn     = $('#' + btn_name);
	var $parsley = General.bindParsley($form);*/
	
	var init = function(){
		
		/*$btn.on('click',function(ev){
			
			if($parsley.validate()){
				button_loader(btn_name, 1);
				
				var data   = General.getFormData($form) + LoadTab.getRequestId('query');
				var option = {
					'data' 	   : data,
					'url'  	   : $base_url + 'ceis/ceis_request_evaluation/save_evaluation',
					'success'  : function(response){
						type = (response.flag) ? 'success' : 'error';
						
						notification_msg(type, response.msg);
					},
					'complete' : function(){
						button_loader(btn_name, 0);
					}
				};
				
				General.ajax(option);
			}
		});*/
	}
	
	var init_modal = function(){
		var $form 	  = $('#op_form');
		var $add_btn  = $('#add_nature_of_collection');
		var $save_btn = $('#save_op');
		var $del_btn  = $('#delete_existing_row');
		var save_name = 'save_op';
	
		LoadTab.tagLabelActive();
		
		General.bindPlugins($form);

		$add_btn.off('click');
		$add_btn.on('click', function(){

			var add_details = {
					collection   : { required : true, type: 'select2'},
					amount		 : { required : true},
					delete_row 	 : { required : true, type: 'delete'},
			};
		
			General.addRow('tbl_tbody_nature_of_collection', 'tbl_row_nature_of_collection', add_details);
		});
		
		$save_btn.off('click');
		$save_btn.on('click', function(){
			$parsley = General.bindParsley($form);
			
			if($parsley.validate()){
				button_loader(save_name, 1);
				
				var data   = General.getFormData($form) + LoadTab.getRequestId('query');
				var option = {
						'data' 	   : data,
						'url'  	   : $base_url + 'ceis/ceis_request_sub_order_of_payment/save_order_payment',
						'success'  : function(response){
							type = (response.flag) ? 'success' : 'error';
							
							General.closeModal();
							
							notification_msg(type, response.msg);
							
							if(response.url != '')
								load_datatable('table_order_of_payment', response.url);
						},
						'complete' : function(){
							button_loader(save_name, 0);
						}
				};

				General.ajax(option);
			}else{
				button_loader(save_name, 0);
			}
		});
		
		$del_btn.off('click');
		$del_btn.on('click', function(){
			$(this).closest("tr").remove();
		});
		
		/*
		.on('click', function(){
			var data = $(this).data('params');
			
			General.deleteModal('text', options);
		});
		*/
	};

	return {
		init: function(){
			//init();
		},
		initModal: function(){
			init_modal();
		}
	};
	
}();
