var CeisRequestSubRequestStatusTasks = function(){
	var table_name = 'table_tasks';
	
	var init = function()
	{
		var option = {
			sAjaxSource : $base_url + 'ceis/ceis_request_tasks/get_request_task_list/' +  LoadTab.getRequestId(),
			ordering    : 0,
			columnDefs  : [],
			paging	    : false
		};
		
		General.loadDatatable(table_name, option);
	};
	
	var init_modal = function()
	{
		var $form 	   = $('#task_form');
		var $btn  	   = $('#save_task');
		var save_name  = 'save_task';
		 
		$btn.off('click');
		$btn.on('click', function(){
			var $parsley = General.bindParsley($form);	
			
			if($parsley.validate()){
				button_loader(save_name, 1);
				
				var data   = General.getFormData($form) + LoadTab.getRequestId('query');
				var option = {
						'data' 	   : data,
						'url'  	   : $base_url + 'ceis/ceis_request_tasks/save_task',
						'success'  : function(response){
							type = (response.flag) ? 'success' : 'error';
							
							General.closeModal();
							
							notification_msg(type, response.msg);
							
							General.reloadDatatable(table_name);
						},
						'complete' : function(){
							button_loader(save_name, 0);
						}
				};

				General.ajax(option);
			}else{
				button_loader(save_name, 0);
			}
			
		});
	};
	
	var remarks_modal = function(table_url)
	{
		var edit_name = 'edit_remarks';
		var del_name  = 'del_remarks';
		var btn_name  = 'save_task_remarks';
		var tbl_name  = 'table_remarks';
		
		var $form     = $('#remarks_form');
		var $table 	  = $('#' + tbl_name);
		var $btn 	  = $('#' + btn_name);
		
		var option    = {sAjaxSource : $base_url + 'ceis/ceis_request_tasks/get_task_remarks_list/' +  table_url};
			
		General.loadDatatable(tbl_name, option);
			
		$table.off('click', '#edit_remarks');
		$table.on('click', '#edit_remarks', function(){
			var url = $(this).data('url');
			
			$('#task_remark_id').val(url);
			
			var options = {
					url     : $base_url + 'ceis/ceis_request_tasks/fetch_data/',
					data    : {type : 'remarks', security : url},
					success : function(response){
						$('#remarks').val(response.data.remarks);					
					} 
			};
			
			General.ajax(options);
		});
		
		$btn.off('click');
		$btn.on('click', function(){
			$parsley = General.bindParsley($form);
			
			if($parsley.validate()){
				button_loader(btn_name, 1);
				
				var data   = General.getFormData($form) + LoadTab.getRequestId('query');
				var option = {
						'data' 	   : data,
						'url'  	   : $base_url + 'ceis/ceis_request_tasks/save_task_remark',
						'success'  : function(response){
							type = (response.flag) ? 'success' : 'error';
							
							notification_msg(type, response.msg);
							
							General.reloadDatatable(tbl_name);
							
							$('#task_remark_id').val('');
							$('#remarks').val('');
						},
						'complete' : function(){
							button_loader(btn_name, 0);
						}
				};

				General.ajax(option);
			}else{
				button_loader(btn_name, 0);
			}	
		});
	}
	
	var attachments_modal = function(table_url, upload_path){
		var tbl_name  = 'table_attachments';
		var save_name = 'save_task_attachment';
		var $table    = $('#' + tbl_name);
		var $form     = $('#attachment_form');
		var $save	  = $('#' + save_name); 
		var $parsley  = General.bindParsley($form);
		var option    = {sAjaxSource : $base_url + 'ceis/ceis_request_tasks/get_task_attachment_list/' + table_url };
		/* used for upload */
		var elem 	  = {id: 'file_upload'};
		
		/* load datatable */
		General.loadDatatable(tbl_name, option);
		/* bind jquery plugins sample datepicker */
		General.bindPlugins($form);
		
		var option  = {
				autoSubmit      : false,
				returnType	    : "json",	
				formData        : {dir : upload_path},
                maxFileCount    : 1,
				showDelete	    : false,
				showPreview    	: false,
				showDownload    : true,
				onLoad    		: function(){},
				onError         : function(){},
				deleteCallback  : function(){
					$('#'+elem.id).data('existing_file','');
				},
				onSuccess       : function(files, data){ 
					var filename = data.toString();
					$('#'+elem.id).data('existing_file', filename);
					
					save_attachment_details(filename);
				},
				afterUploadAll  : function(uploadObj){
					uploadObj.fileCounter = 0;
                    uploadObj.selectedFiles = 0;
                    uploadObj.fCounter = 0; //failed uploads
                    uploadObj.sCounter = 0; //success uploads
                    uploadObj.tCounter = 0; //total uploads
				},
				onCancel 		 : function(){
					$('#'+elem.id).data('existing_file', '');
				},
				downloadCallback : function(files){
					var filename  = $('#'+elem.id).data('existing_file');
					location.href =  $base_url + 'ceis/ceis_request_tasks/download_attachment/' + LoadTab.getRequestId() + '/' + filename;
				},
				onSelect		: function(files){
                    $('#'+elem.id).data('existing_file', files[0].name);

                    return true; //to allow file submission.
                }
	   	  }; 
   	 
		$uploadObj = General.initUpload(elem, option);
		
		$save.off('click');
		$save.on('click',function(){
			
			if($parsley.validate())
			{
				button_loader(save_name, 1);
				
				var $filename = $('#'+elem.id).data('existing_file');
				
				if($filename)
				{
					if($('input[name="file"]').val())
					{
						$uploadObj.startUpload();
					}
					else
					{
						save_attachment_details($filename);
					}
				}
				else
				{
					  notification_msg('error', 'Please upload a file first!');		
	                  
	                  button_loader(save_name, 0);    
				}
			}	
			else
			{
				button_loader(save_name, 0);
			}

		});
		
		$table.off('click', '#edit_attachment');
		$table.on('click', '#edit_attachment', function(){
			var url = $(this).data('url');
			
			$('#task_attachment_id').val(url);
			
			var options = {
					url     : $base_url + 'ceis/ceis_request_tasks/fetch_data/',
					data    : {type : 'attachment', security : url},
					success : function(response){
						/* Clear upload widget */
						$('.ajax-file-upload-statusbar').remove();
						/* Set data */
						$('#description').val(response.data.description);
						$('#date_received').val(response.data.received_date);
						
						$select    = $('#document_type').selectize();
						$selectize = $select[0].selectize;
						$selectize.setValue(response.data.attachment_type_id);
						
						$('#'+elem.id).data('existing_file', response.data.file_details.filename);
						
						/* Create upload widget */
						$uploadObj.createProgress(response.data.file_details.display_filename, response.data.file_details.path);
					} 
			};
		
			General.ajax(options);
		});

		function save_attachment_details(filename)
		{
			var data   = General.getFormData($form);
		    	data   += '&filename='+filename;
		    	data   += LoadTab.getRequestId('query');
	    
	    	var options = {
                url      : $base_url + 'ceis/ceis_request_tasks/save_task_attachment', 
                data     : data,
                success  : function(response){
					type = (response.flag) ? 'success' : 'error';
					
					if(response.flag)
					{
						$('.ajax-file-upload-statusbar').remove();
						$('#task_attachment_id').val('');
						$('#description').val('');
						$('#date_received').val('');

						$select    = $('#document_type').selectize();
						$selectize = $select[0].selectize;
						$selectize.clear();
					
						$('#'+elem.id).data('existing_file','');
					}
					
					notification_msg(type, response.msg);
					
					General.reloadDatatable(tbl_name);
				},
				complete : function(){
					button_loader(save_name, 0);
				}
	    	};
	    
	    	General.ajax(options);
		}
		
	};
	
	return {
		init : function(){
			init();
		},
		initModal : function(){
			init_modal();
		},
		RemarksModal : function(table_url){
			remarks_modal(table_url);
		},
		AttachmentsModal : function(table_url, upload_path){
			attachments_modal(table_url, upload_path);
		}
	}
	
}();