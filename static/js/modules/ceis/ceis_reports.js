$(function(){

  // General.alertMsg("alert_requests", 1, "dfasdfa");

  var _report_type = {

    1 : function() {
      _pdf();
    },

    2 : function() {
      _excel();
    }
  }

  function _pdf() 
  {
    var data        = $("#report_form").serialize();

    var option = {

      url  : $base_url + 'ceis/ceis_reports/generate_report/',
      data : data,
      success : function(response){

        if( response.success )
        {
          $("body").isLoading("hide");
          var modal = response.modal;
          classie.add( $( '#modal_report' )[0], 'md-show' );

          modal_report.loadViewJscroll({
            ajax_modal  : modal, 
            height      : modal_height['modal_report']
          });
        
          $( '.md-close' ).on( 'click', function() {

            classie.remove( $( '#modal_report' )[0], 'md-show' );

          } );
          
        }
      }
    };

    General.ajax(option);
  }


  function _excel()
  {

    $("#report_form").attr( 'action', $base_url + 'ceis/ceis_reports/generate_report/' );
    $("#report_form").attr( 'method', 'post' );
    $('#report_form' ).submit();

    $('#report_form').removeAttr( 'action' );
    $('#report_form').removeAttr( 'method' );
    $("body").isLoading("hide");

  }

  $( '#generate_report' ).on( 'click', function() {
      $( "body" ).isLoading({
            text:       "<div class='loader'></div>", 
            position:   "inside"
      });

      if( $( '#report_type_pdf' ).is(':checked') )
      {
    	  
         _report_type[ 1 ]();
      }
      else if( $( '#report_type_excel' ).is(':checked') )
      {
    	 
        _report_type[ 2 ]();
      }
      
    });

    $('#report_list').on('change', function() {
      if ( this.value == '1')
      {

        $( '#report_type_pdf' ).prop('checked', true);
        $( '#report_type_excel' ).prop('checked', false);
        
        $("#quarter").show();
        $("#contractor").hide();
        $("#excel").hide();
      }
      else
      {
        $("#quarter").hide();
        $("#contractor").show();
        $("#excel").show();
      }
    });


  

});
