var CeisContractors = function(){
	
	var init = function(){

		jQuery(document).off('click', '.delete-position');		
		jQuery(document).on('click', '.delete-position', function(event){

			event.preventDefault();			

			//gets val
			// var position_id = $(this).data('position');

			if(confirm('Do you want to delete this record?'))
			{
		        var option = {
		          url  : $base_url + 'ceis/ceis_contractors/delete_position/',
		          data : {position_id: position_id},
		          success : function(response){

		            alert(response.msg);    

		          }
		        };

		        General.ajax(option);
			}
		});

	};

	return {
		init : function(){
			init();	
		},	

		//load function inside the controller
		load : function(){
			var option = {				
				sAjaxSource	: $base_url + "ceis/ceis_contractors/get_contractor_list",
			};

			//load datatable for table id is tbl_contractors
			General.loadDatatable('tbl_contractors', option);
		}
	};

}();
