var CeisRequestStaffingProject = function(){

	var table_name = 'request_staffing_project';

	var init = function(){
		var option = {
				sAjaxSource : $base_url + 'ceis/ceis_request_staffing/get_staff_project_list/' +  LoadTab.getAdditionalInfo(),
		};
			
		General.loadDatatable(table_name, option);
	}

	return {
		init: function(){
			init();
		}
	};
	
}();
