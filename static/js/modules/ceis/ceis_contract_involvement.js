var CeisContractInvolvement = function(){
	
	var modal = function(){
		console.log($base_url + "ceis/ceis_contract_involvement/process");
		
	  //validation
      $('#contract_involvement_form').parsley();
      
      //submit function
      $('#contract_involvement_form').submit(function(event) {

      event.preventDefault();

      if ( $(this).parsley().isValid() ) {
        var data = $(this).serialize();

        var option = {
          url  : $base_url + "ceis/ceis_contract_involvement/process",          
          data : data,
          success : function(response){
            alert(response.msg);   

            var table_option = {        
              sAjaxSource : $base_url + "ceis/ceis_contract_involvement/get_contract_involvement_list",
            };

              General.loadDatatable('tbl_contract_involvement', table_option); 

            // if error occurred prevent close modal

            // CLOSE MODAL
			$.modalStack.closeActive();
            }   
        };

        General.ajax(option);
        }
      });
	};

	var init = function(){

		jQuery(document).off('click', '.delete-contractinvolvement');		
		jQuery(document).on('click', '.delete-contractinvolvement', function(event){

			event.preventDefault();			

			//gets val
			var involvement_code = $(this).data('contractinvolvement');

			if(confirm('Do you want to delete this record?'))
			{
				
		        
		        var option = {
		          url  : $base_url + 'ceis/ceis_contract_involvement/delete_contract_involovement/',
		          data : {involvement_code: involvement_code},
		          success : function(response){

		            alert(response.msg);    

		            if(response.flag == 1)
		            {
			            var table_option = {				
							sAjaxSource	: $base_url + "ceis/ceis_contract_involvement/get_contract_involvement_list",
						};

						General.loadDatatable('tbl_contract_involvement', table_option);	
		            }
		          }
		        };

		        General.ajax(option);
			}
		});

	};


	return {
		init : function(){
			init();	
		},

		modal : function() {
			modal();
		},
		
		//load function inside the controller
		load : function(){
			var option = {				
				sAjaxSource	: $base_url + "ceis/ceis_contract_involvement/get_contract_involvement_list",
			};

			//load datatable for table id is tbl_positions
			General.loadDatatable('tbl_contract_involvement', option);
		}
	};

}();
