var CeisRequestSubEvaluation = function(){
	var $form 	 = $('#evaluation_form'); 
	var btn_name = 'save_evaluation';
	var $btn     = $('#' + btn_name);
	var $parsley = General.bindParsley($form);
	var $pdf     = $('#print_eval_form');
	
	var init = function(){
		
		$btn.on('click',function(ev){
			
			if($parsley.validate()){
				button_loader(btn_name, 1);
				
				var data   = General.getFormData($form) + LoadTab.getRequestId('query');
				var option = {
					'data' 	   : data,
					'url'  	   : $base_url + 'ceis/ceis_request_evaluation/save_evaluation',
					'success'  : function(response){
						type = (response.flag) ? 'success' : 'error';
						
						notification_msg(type, response.msg);
					},
					'complete' : function(){
						button_loader(btn_name, 0);
					}
				};
				
				General.ajax(option);
			}
		});
		
		$pdf.on('click', function(ev){
		    var option = {
		    		url  	: $base_url + 'ceis/ceis_request_evaluation/generate_report/',
		    		data    : LoadTab.getRequestId('query'),
		    		success : function(response){
					       if( response.status )
					        {
							     var modal = response.modal;
							     
							     classie.add( $( '#modal_evaluation_report' )[0], 'md-show' );
					
							     modal_evaluation_report.loadViewJscroll({
							            ajax_modal  : modal, 
							            height      : modal_height['modal_evaluation_report']
							     });
							        
							     $( '.md-close' ).on( 'click', function() {
							            classie.remove( $( '#modal_evaluation_report' )[0], 'md-show' );
							     });          
							 }
		    		}
		    };

		    General.ajax(option);    
		});
	}

	return {
		init: function(){
			init();
		}
	};
	
}();