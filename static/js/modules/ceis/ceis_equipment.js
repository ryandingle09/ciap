var CeisEquipment = function(){
	
	var init = function(){
	};

	return {
		init : function(){
			init();	
		},	

		//load function inside the controller
		load : function(){
			var option = {				
				sAjaxSource	: $base_url + "ceis/ceis_equipment/get_equipment_list",
			};

			//load datatable for table id is tbl_positions
			General.loadDatatable('tbl_equipment', option);

			// if error occurred prevent close modal

            // CLOSE MODAL
			$.modalStack.closeActive();
		}
	};

}();
