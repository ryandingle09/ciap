
	$(function(){	

		console.log($base_url);

		 $('#opportunity_form').parsley();
		  $('#opportunity_form').submit(function(e) {
		    e.preventDefault();
		    
			if ( $(this).parsley().isValid() ) {
			  var data = $(this).serialize();
			  
			  button_loader('save_opportunity', 1);
			  $.post($base_url + "ceis/ceis_opportunities/process/", data, function(result) {
				if(result.flag == 0){
				  notification_msg("error", result.msg);
				  button_loader('save_opportunity', 0);
				} else {
					notification_msg("success", result.msg);
				  
				 	modal_opportunity.closeModal();	

				 	button_loader("save_opportunity",0);
				  	var table_option = {        
		            	sAjaxSource : $base_url + "ceis/ceis_opportunities/get_opportunity_list",
		            };
				   	// LOAD DATATABLE
		          //  General.loadDatatable('opportunity_table', table_option); 

				  // CLOSE MODAL
				  
				  
					load_datatable('opportunity_table', 'ceis/ceis_opportunities/get_opportunity_list/');
				}
			  }, 'json');       
		    }
		  });

		// $('.input-field label').addClass('active');

		//hides and shows divs
     	$('.contractor_list').on('change', function() {
	      	if ( this.value == '0')
	      	{
	        	$(this).parent().next("div").show();
	      	}
	      	else
	      	{
	      		$(this).parent().next("div").hide();
	        	//$("#new_contractor").hide();
	      }
	    });

     	//add rows
	    var options = {
    		btn_id 	: 'add_row',
			tbl_id 	: 'contractor',
			elem_to_mod : [ 'input', 'select' ],
			each_elem_mod : function( obj )
			{
				obj.val('');
				if( obj.is( 'select' ) && obj.hasClass('contractor_list') )
				{
					/*obj.select2({ 
						width  : '100%'
	        		});
	        		
					obj.parent().find( 'div.select2-container ' )[0].remove();*/
				}
			},
			after_copy_row : function()
			{
				var table = $( '#contractor' ),
					row   = table.find( 'tbody tr' );

				
				row.find( '.contractor_list' ).on( 'change', function() {
					
					if ( this.value == '0')
      				{
      					$(this).parents('tr').find('#new_contractor').show();
      				}
      				else 
      				{
      					$(this).parents('tr').find('#new_contractor').hide();
      				}
				} );
			},
			scroll 	: true
	    };
	    add_rows( options );
	});

