var CeisRequestSubRegistration = function(){
	var $form 	 = $('#registration_form'); 
	var btn_name = 'save_registration';
	var $btn     = $('#' + btn_name);
	var $parsley = General.bindParsley($form);
	
	var init = function(){
		//General.bindPlugins($form);
		
		$btn.on('click',function(ev){
			ev.preventDefault();
		
			if($parsley.validate()){
				button_loader(btn_name, 1);
				
				var data   = General.getFormData($form) + LoadTab.getRequestId('query');
				var option = {
					'data' 	   : data,
					'url'  	   : $base_url + 'ceis/ceis_request_registration/save_registration',
					'success'  : function(response){
						type = (response.flag) ? 'success' : 'error';
						
						notification_msg(type, response.msg);
					},
					'complete' : function(){
						button_loader(btn_name, 0);
					}
				};
				
				General.ajax(option);
			}
			
		});
	}

	return {
		init: function(){
			init();
		}
	};
	
}();