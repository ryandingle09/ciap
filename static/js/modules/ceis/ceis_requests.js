var CeisRequests = function(){
	
	var init = function(){
    var option = {				
      sAjaxSource	: $base_url + "ceis/ceis_requests/get_workexperience_project_list/",
    };

    //load datatable for table id is tbl_positions
    General.loadDatatable('tbl_work_experience_projects', option);
	};


	var initremarks = function(){
    var option = {				
      sAjaxSource	: $base_url + "ceis/ceis_requests/get_workexperience_remarks_list/",
    };

    //load datatable for table id is tbl_positions
    General.loadDatatable('tbl_request_task_remarks', option);
	};

	var initservice = function(){
    var option = {				
      sAjaxSource	: $base_url + "ceis/ceis_requests/get_workexperience_services_list/",
    };

    //load datatable for table id is tbl_positions
    General.loadDatatable('tbl_work_experience_services', option);
	};

  var initpdf = function(){
   // var data = $("#report_form").serialize();



    var option = {

      url  : $base_url + 'ceis/ceis_requests/generate_report/',
      success : function(response){
    	  console.log(response);
        if( response.success )
        {
          var modal = response.modal;
          classie.add( $( '#modal_evaluation_report' )[0], 'md-show' );

          modal_evaluation_report.loadViewJscroll({
            ajax_modal  : modal, 
            height      : modal_height['modal_evaluation_report']
          });
        
          $( '.md-close' ).on( 'click', function() {
            classie.remove( $( '#modal_evaluation_report' )[0], 'md-show' );
          });          
        }
      }
    };

    General.ajax(option);    
  };




	return {

    initpdf : function(){
      initpdf();
    },

		init : function(){
			init();	
		},	

		//load function inside the controller
		load : function(){
			var option = {				
				sAjaxSource	: $base_url + "ceis/ceis_requests/get_request_list",
			};

			//load datatable for table id is tbl_resolutions
			General.loadDatatable('tbl_requests', option);

			// if error occurred prevent close modal

            // CLOSE MODAL
			$.modalStack.closeActive();
		},

		initremarks : function(){
			initremarks();
		},

		initservice : function(){
			initservice();
		}
	};

}();

//REPORTS
$(function(){

 



  

   /* 



$( '#generate_report' ).on( 'click', function() {
       

    });


   $('#report_list').on('change', function() {
      if ( this.value == '1')
      {

        $( '#report_type_pdf' ).prop('checked', true);
        $( '#report_type_excel' ).prop('checked', false);
        
        $("#quarter").show();
        $("#contractor").hide();
        $("#excel").hide();
      }
      else
      {
        $("#quarter").hide();
        $("#contractor").show();
        $("#excel").show();
      }
    });*/


  

});


