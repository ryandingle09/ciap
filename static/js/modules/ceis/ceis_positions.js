var CeisPositions = function(){
	
	var init = function(){

		jQuery(document).off('click', '.delete-position');		
		jQuery(document).on('click', '.delete-position', function(event){

			event.preventDefault();			

			//gets val
			var position_id = $(this).data('position');

			if(confirm('Do you want to delete this record?'))
			{
		        var option = {
		          url  : $base_url + 'ceis/ceis_positions/delete_position/',
		          data : {position_id: position_id},
		          success : function(response){

		            alert(response.msg);    

		            if(response.flag == 1)
		            {
			            var table_option = {				
							sAjaxSource	: $base_url + "ceis/ceis_positions/get_positions_list",
						};

						General.loadDatatable('tbl_positions', table_option);	
		            }
		          }
		        };

		        General.ajax(option);
			}
		});

	};

	var modal = function(){

	//script for parsley validation
    $('#position_form').parsley();

    //script for submitting 
    $('#position_form').submit(function(event) {

      event.preventDefault();

      if ( $(this).parsley().isValid() ) {
        var data = $(this).serialize();

        var option = {
          url  : $base_url + "ceis/ceis_positions/process",          
          data : data,
          success : function(response){
            alert(response.msg);   
           
	            var table_option = {        
	              sAjaxSource : $base_url + "ceis/ceis_positions/get_positions_list",
	            };

	            // LOAD DATATABLE
	            General.loadDatatable('tbl_positions', table_option); 
    		
            // if error occurred prevent close modal
        	
        	// CLOSE MODAL
			$.modalStack.closeActive();	

          }

        };

        General.ajax(option);

        }

      });
	}

	return {
		init : function(){
			init();	
		},	

		modal : function(){
			modal();
		},

		//load function inside the controller
		load : function(){
			var option = {				
				sAjaxSource	: $base_url + "ceis/ceis_positions/get_positions_list",
			};

			//load datatable for table id is tbl_positions
			General.loadDatatable('tbl_positions', option);
		}
	};

}();
