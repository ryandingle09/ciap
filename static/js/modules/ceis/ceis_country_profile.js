var CeisCountryProfile = function(){ 
	
	var init = function(){

		jQuery(document).off('click', '.delete-country-profile');		
		jQuery(document).on('click', '.delete-country-profile', function(event){

			event.preventDefault();			

			var country_code = $(this).data('country');

			if(confirm('Do you want to delete this record?'))
			{
				
		        var data = $(this).serialize();

		        var option = {
		          url  : $base_url + 'ceis/ceis_country_profile/delete_country_profile/',
		          data : {country_code: country_code},
		          success : function(response){

		            alert(response.msg);    

		            if(response.flag == 1)
		            {
			            var table_option = {				
							sAjaxSource	: $base_url + "ceis/ceis_country_profile/get_country_profile",
						};

						General.loadDatatable('tbl_country', table_option);	
						// if error occurred prevent close modal
		            }
		          }
		        };

		        General.ajax(option);
			}
		});

	};

	var modal = function(){
		//for JS_SELECT (dropdown with search)
      $("#country_name").select2({
        width : '50%'
      });

      $('#country_form').parsley();
      $('#country_form').submit(function(event) {

      event.preventDefault();

      if ( $("#country_form").parsley().isValid() ) {
	      uploadObj.startUpload();

	      	for (instance in CKEDITOR.instances) {
				CKEDITOR.instances[instance].updateElement();
			}

       	  var data = $("#country_form").serialize();
	      var option = {
          url  : $base_url + "ceis/ceis_country_profile/process",          
          data : data,
          success : function(response){

            alert(response.msg);   

            // IF STATEMENTS (validation)
            var table_option = {        
              sAjaxSource : $base_url + "ceis/ceis_country_profile/get_country_profile",
            };

              General.loadDatatable('tbl_country', table_option);
              
               // CLOSE MODAL
				$.modalStack.closeActive();

          }
        };
	  }

      });
      
      var photo_path = $('#photo_path').val();

      var uploadObj = $("#course_photo").uploadFile({
        url: $base_url + "upload/",
        fileName: "file",
        formData: {"dir": photo_path},
        multiple:false,
        dragDrop:false,
        maxFileCount:1,
        autoSubmit : false,
        onSuccess : function(files, data, xhr,pd){
        	var filename = data.toString();
        	// console.log('FILENAME : ' + filename);
        	if ( $("#country_form").parsley().isValid() ) {
		        var data = $("#country_form").serialize();

		        var option = {
		          url  : $base_url + "ceis/ceis_country_profile/process",          
		          data : data + filename,
		          success : function(response){
		          	alert(data);
		            alert(response.msg);   

		            // IF STATEMENTS (validation)
		            var table_option = {        
		              sAjaxSource : $base_url + "ceis/ceis_country_profile/get_country_profile",
		            };

		              General.loadDatatable('tbl_country', table_option);
		              
		               // CLOSE MODAL
						$.modalStack.closeActive();
		          }
		        };

		        General.ajax(option);
		    };
        }
      });
	};

	return {
		init : function(){
			init();	
			
		},	

		modal : function(){
			modal();
		},

		load : function(){
			var option = {				
				sAjaxSource	: $base_url + "ceis/ceis_country_profile/get_country_profile",
			};
			
			//load datatable for table id is tbl_resolutions
			General.loadDatatable('tbl_country', option);
		}
	};



}();
