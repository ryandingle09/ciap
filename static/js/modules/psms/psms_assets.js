$(document).ready(function(){

    //RELOAD CLASSSIFACTION SELECT OPTIONS
    $('#category').bind('change', function(){
        var category_id = $(this).val();
        var security = $('input[name="security"]').val();
        
        var param = {
            asset_category_id: category_id,
            security: security,
        }
        //GET ASSET CLASSIFICATION OF THE SELECTED CATEGORY
       $.ajax({
            type: "POST",
            url: $base_url+'psms/psms_asset_classification/get_classification_options',
            data: param,
            dataType: 'json',
            success: function(results){
                selectize_reload('classification', results.options);
            },
        });
    });


    //RELOAD SITE SELECT OPTIONS
    $('#site').bind('change', function(){
        var site_id = $(this).val();
        var security = $('input[name="security"]').val();
        
        var param = {
            site_id: site_id,
            security: security,
        }
        //GET ASSET CLASSIFICATION OF THE SELECTED CATEGORY
       $.ajax({
            type: "POST",
            url: $base_url+'psms/psms_asset_location/get_location_options',
            data: param,
            dataType: 'json',
            success: function(results){
                selectize_reload('location', results.options);
            },
        });
    });

});