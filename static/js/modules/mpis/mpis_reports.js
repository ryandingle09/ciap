$(function(){

	var mpis_report = $base_url + 'mpis/mpis_reports/generate_report/';

	_default_fields();
	
	function _pdf() 
	{
		var data = $("#report_form").serialize();
    

		var option = {
			url  : mpis_report,
			data : data,
			success : function(response){

				if( response.success )
				{
					var modal = response.modal;
					classie.add( $( '#modal_report' )[0], 'md-show' );

					modal_report.loadViewJscroll({
						ajax_modal  : modal, 
						height      : modal_height['modal_report']
					});
        
					$( '.md-close' ).on( 'click', function() {
						classie.remove( $( '#modal_report' )[0], 'md-show' );
					});          
				}
			}
		};

		General.ajax(option);
	}


	function _excel()
	{

		$("#report_form").attr( 'action', mpis_report );
		$("#report_form").attr( 'method', 'post' );
		$('#report_form' ).submit();

		$('#report_form').removeAttr( 'action' );
		$('#report_form').removeAttr( 'method' );

	}
	
	$("#report_type_pdf, #report_type_excel").on('click', function() {
		$("#percent_share_row").hide();
	});
	
	$("#report_type_chart").on('click', function() {
		
		var report = $("#report").val();
		
		$("#percent_share_row").hide();
		
    	switch(report)
    	{
    		case 'MPIS-REPORTS-R1':    		
			case 'MPIS-REPORTS-R2':
			case 'MPIS-REPORTS-R3':
				$("#percent_share_row").show();
			break;
    	}
    	
	});
	
	
	
	$( '#generate_report' ).on( 'click', function() {
		
		var report = $("#report").val();
		
				
		if(report == '')
		{
			notification_msg("error", "Select report");
			return; 
		}
		
    	switch(report)
    	{
    		case 'MPIS-REPORTS-R1':    		
    		case 'MPIS-REPORTS-R2':
    			var year_start		= $("#year_start").val();
    			var year_end		= $("#year_end").val();
    			var quarter			= $("#quarter").val();
    			var building_type	= $("#building_type").val();
    			var generate 		= $("#report_form input[name='generate']:checked").val();
    			
    			if(year_start == '')
    			{
    				notification_msg("error", "Select start year");
    				return; 
    			}
    			
    			if(year_end == '')
    			{
    				notification_msg("error", "Select end year");
    				return; 
    			}
    			
    			if(quarter == '')
    			{
    				notification_msg("error", "Select quarter");
    				return; 
    			}
    			
    			if(building_type == '')
    			{
    				notification_msg("error", "Select type of building");
    				return; 
    			}
    			
    			if(generate == 'Chart')
    			{
    				var percent = $("#percent_share").val();
    				
    				if(percent == '')
    				{
    					notification_msg("error", "Select percent share to");
        				return;	
    				}
    			}
    	    	
    		break;
    		
    		case 'MPIS-REPORTS-R3':    		    		
    			var year_start	= $("#year_start").val();
    			var year_end	= $("#year_end").val();
    			var quarter		= $("#quarter").val();
    			var generate 	= $("#report_form input[name='generate']:checked").val();
    			    			
    			if(year_start == '')
    			{
    				notification_msg("error", "Select start year");
    				return; 
    			}
    			
    			if(year_end == '')
    			{
    				notification_msg("error", "Select end year");
    				return; 
    			}
    			
    			if(quarter == '')
    			{
    				notification_msg("error", "Select quarter");
    				return; 
    			}
    			
    			if(generate == 'Chart')
    			{
    				var percent = $("#percent_share").val();
    				
    				if(percent == '')
    				{
    					notification_msg("error", "Select percent share to");
        				return;	
    				}
    			}
    			    			    	    
    		break;    		
    		
    		case 'MPIS-REPORTS-R4':    		    		
    			var year_start	= $("#year_start").val();
    			var year_end	= $("#year_end").val();
    			var business	= $("#business_type").val();
    			var category	= $("#category").val();
    			var company		= $("#company").val();
    			var ratio		= $("#ratio").val();
    			
    			if(year_start == '')
    			{
    				notification_msg("error", "Select start year");
    				return; 
    			}
    			
    			if(year_end == '')
    			{
    				notification_msg("error", "Select end year");
    				return; 
    			}
    			
    			if(business == '')
    			{
    				notification_msg("error", "Select type of business");
    				return; 
    			}
    			
    			if(category == '')
    			{
    				notification_msg("error", "Select category");
    				return; 
    			}
    			
    			if(company == '')
    			{
    				notification_msg("error", "Select company");
    				return; 
    			}
    			
    			if(ratio == '')
    			{
    				notification_msg("error", "Select ratio");
    				return; 
    			}
    		break;
    		
    		case 'MPIS-REPORTS-R5':
    			var year_start		= $("#year_start").val();
    			var year_end		= $("#year_end").val();
    			var quarter			= $("#quarter").val();
    			var building_type	= $("#building_type").val();
    			
    			if(year_start == '')
    			{
    				notification_msg("error", "Select start year");
    				return; 
    			}
    			
    			if(year_end == '')
    			{
    				notification_msg("error", "Select end year");
    				return; 
    			}
    			
    			if(quarter == '')
    			{
    				notification_msg("error", "Select quarter");
    				return; 
    			}
    			
    	    	
    		break;

    		case 'MPIS-REPORTS-R6':
    		case 'MPIS-REPORTS-R7':
    		case 'MPIS-REPORTS-R8':    			
    			var year_start		= $("#year_start").val();
    			var year_end		= $("#year_end").val();
    			var price			= $("#price").val();
    			
    			
    			if(year_start == '')
    			{
    				notification_msg("error", "Select start year");
    				return; 
    			}
    			
    			if(year_end == '')
    			{
    				notification_msg("error", "Select end year");
    				return; 
    			}
    			
    			if(price == '')
    			{
    				notification_msg("error", "Select price");
    				return; 
    			}
    			
    	    	
    		break;
    		
    		case 'MPIS-REPORTS-R9':    			
    			var date_start		= $("#date_start").val();
    			var date_end		= $("#date_end").val();
    			var category		= $("#indicator_category").val();
    			
    			
    			if(date_start == '')
    			{
    				notification_msg("error", "Select start date");
    				return; 
    			}
    			
    			if(date_end == '')
    			{
    				notification_msg("error", "Select end date");
    				return; 
    			}
    			
    			if(category == '')
    			{
    				notification_msg("error", "Select category");
    				return; 
    			}    	    	
    		break;    	
    		
    		case 'MPIS-REPORTS-R10':
    			var year_start	= $("#year_start").val();
    			var year_end	= $("#year_end").val();
    			var indicator 	= $("#indicator").val();
    			
    			if(year_start == '')
    			{
    				notification_msg("error", "Select start year");
    				return; 
    			}
    			
    			if(year_end == '')
    			{
    				notification_msg("error", "Select end year");
    				return; 
    			}
    			
    			if(indicator == '')
    			{
    				notification_msg("error", "Select indicator");
    				return; 
    			}
    		break;
    		
    		case 'MPIS-REPORTS-R11':
    			var year_start	= $("#year_start").val();
    			var year_end	= $("#year_end").val();
    			
    			if(year_start == '')
    			{
    				notification_msg("error", "Select start year");
    				return; 
    			}
    			
    			if(year_end == '')
    			{
    				notification_msg("error", "Select end year");
    				return; 
    			}

    		break;
    		
    		case 'MPIS-REPORTS-R12':
    			var value	= $("#value").val();
    			    			
    			if(value == '')
    			{
    				notification_msg("error", "Select value");
    				return; 
    			}
    		break;
    		    		
    		
    	}
    
		if( $( '#report_type_pdf' ).is(':checked') ) 	 
			_pdf();
		else if( $( '#report_type_excel' ).is(':checked') )
			_excel();
		else if( $( '#report_type_chart' ).is(':checked') )
			_pdf();		      
    });

    $('#report').on('change', function() {
    	
    	var report = $(this).val();
    	
    	_default_fields();
    	
    	switch(report)
    	{
    		case 'MPIS-REPORTS-R1':
    			$("#year_range_row").show();
    	    	$("#quarter_row").show();
    	    	$("#building_type_row").show();
    	    	
    	    	_get_building_types(1);
    	    	
    		break;
    		
    		case 'MPIS-REPORTS-R2':
    			$("#year_range_row").show();
    	    	$("#quarter_row").show();
    	    	$("#building_type_row").show();
    	    	
    	    	_get_building_types(2);
    	    	
    		break;
    		
    		case 'MPIS-REPORTS-R3':
    			$("#year_range_row").show();
    	    	$("#quarter_row").show();    	    	    	    	
    		break;
    		
    		case 'MPIS-REPORTS-R4':
	    		$("#year_range_row").show();        	
	        	$("#business_type_category_row").show();
	        	$("#business_type_row").show();
	        	$("#category_row").show();
	        	$("#company_ratio_row").show();
	        	$("#company_row").show();
	        	$("#ratio_row").show();
	        	
	        	$("#chart").hide();
	        	
	        	_get_business_types();
	        	_get_license_categories();
	        	_get_companies();
	        	_get_ratios();
        	break;
        	
    		case 'MPIS-REPORTS-R5':
    			$("#year_range_row").show();
    	    	$("#quarter_row").show();    	    	
    		break;
    		
    		case 'MPIS-REPORTS-R6':
    		case 'MPIS-REPORTS-R7':
    		case 'MPIS-REPORTS-R8':
    			$("#year_range_row").show();
    	    	$("#price_row").show();    	    	
    		break;
    		
    		case 'MPIS-REPORTS-R9':
    			$("#date_range_row").show();
    			$("#indicator_category_row").show();
    			$("#chart").hide();
    			
    			_get_indicator_categories();
    			
    			
    		break;
    		
    		case 'MPIS-REPORTS-R10':
    			$("#year_range_row").show();
    			$("#indicator_row").show();
    			
    			_get_indicators();
    		break;
    		
    		case 'MPIS-REPORTS-R11':
    			$("#year_range_row").show();
    		break;
    		
    		case 'MPIS-REPORTS-R12':
    			$("#year_range_row").show();
    			$("#value_row").show();
    			
    			$("#chart").hide();
    		break;
    		
    		
    		
    	}
    	
    });
    
    function _default_fields()
    {
    	$("#year_range_row").hide();
    	$("#quarter_row").hide();
    	$("#building_type_row").hide();
    	$("#business_type_category_row").hide();
    	$("#business_type_row").hide();
    	$("#category_row").hide();
    	$("#company_ratio_row").hide();
    	$("#company_row").hide();
    	$("#ratio_row").hide();
    	$("#price_row").hide();
    	
    	$("#date_range_row").hide();
    	$("#indicator_category_row").hide();
    	$("#indicator_row").hide();
    	$("#value_row").hide();
    	    	
    	$("#pdf").show();
    	$("#excel").show();
    	$("#chart").show();
    	
    	var generate = $("#report_form input[name='generate']:checked").val();
    	
    	
    	$("#percent_share_row").hide();
    	
    	if(generate == "Chart")
    	{
    		var report = $("#report").val();
    		
        	switch(report)
        	{
        		case 'MPIS-REPORTS-R1':    		
    			case 'MPIS-REPORTS-R2':
    			case 'MPIS-REPORTS-R3':
    				$("#percent_share_row").show();
    			break;
    			
    			default:
    				$("#percent_share_row").hide();
    			break;
        	}
    	}
    	

    }
    
    function _get_building_types(type)
    {    	
    	$.post($base_url + 'mpis/mpis_reports/get_building_types', {type: type}, function(result){
    		$("#building_type")[0].selectize.destroy();
    		$("#building_type").html(result);
    		$("#building_type").selectize();
    	});
    }
    
    function _get_business_types()
    {    	
    	$.post($base_url + 'mpis/mpis_reports/get_business_types', function(result){
    		$("#business_type")[0].selectize.destroy();
    		$("#business_type").html(result);
    		$("#business_type").selectize();
    	});
    }
    
    function _get_companies()
    {    	
    	$.post($base_url + 'mpis/mpis_reports/get_companies', function(result){
    		$("#company")[0].selectize.destroy();
    		$("#company").html(result);
    		$("#company").selectize();
    	});
    }
    
    function _get_license_categories()
    {    	
    	$.post($base_url + 'mpis/mpis_reports/get_license_categories', function(result){
    		$("#category")[0].selectize.destroy();
    		$("#category").html(result);
    		$("#category").selectize();
    	});
    }
    
    function _get_ratios()
    {    	
    	$.post($base_url + 'mpis/mpis_reports/get_ratios', function(result){
    		$("#ratio")[0].selectize.destroy();
    		$("#ratio").html(result);
    		$("#ratio").selectize();
    	});
    }
    
    function _get_indicator_categories()
    {    	
    	$.post($base_url + 'mpis/mpis_reports/get_indicator_categories', function(result){
    		$("#indicator_category")[0].selectize.destroy();
    		$("#indicator_category").html(result);
    		$("#indicator_category").selectize();
    	});
    }   
    
    function _get_indicators()
    {    	
    	$.post($base_url + 'mpis/mpis_reports/get_indicators', function(result){
    		$("#indicator")[0].selectize.destroy();
    		$("#indicator").html(result);
    		$("#indicator").selectize();
    	});
    } 
});
