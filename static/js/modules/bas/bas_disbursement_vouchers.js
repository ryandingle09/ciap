var elements = {
	//base					: $base_url,
	doc      						: $(document),
	formDefault						: '#form',
	dropdown						: '.dropdown',
	add_row							: '#add_row',
	delete_row						: '#delete_row',
	status							: '#button',
}

;(function(){

	var basDisbursementVoucher = {

		statusFunction : function(e){//specific for approved and disapproved
			return this.delegate(elements.status,'click',function(){
				var dis 	= $(this)
				var status 	= dis.data('status');

				$('input[name="status"]').val(status);
				$('#form').submit();
			});

		},

		addRowFunction : function(e){
			return this.delegate(elements.add_row,'click',function(e){
				e.preventDefault();
				e.stopImmediatePropagation();

				var dis 		= $(this)
				var id 			= dis.data('id');
				var identity 	= dis.data('identity');
				var count 		= parseInt($('#count').val());
				var new_count   = (count + 1);

				var data 		= {
					'id'		: id,
					'identity'	: identity,
					'count'		: count,
					'new_count'	: new_count
				};

				$.post($base_url+'bas/bas_disbursement_vouchers/get_row', data, function(table_row){

					if(identity == 'ciac')
					{
						$('.rows').append(table_row);
					}
					else
					{
						$('.rows').append(table_row);
						var new_tax_uri = 'modal_tax_init("'+$('input[name="security"]').val()+'/tax/cmdf/'+new_count+'/'+$('#payee').val()+'")';
					    $('.tax_link_'+new_count+'').attr('onclick', new_tax_uri);
					}

				    $('#count').val(new_count);
				});
			});

		},

		deleteRowFunction : function(e){
			return this.delegate(elements.delete_row,'click',function(e){
				e.preventDefault();
				e.stopImmediatePropagation();
				    
				var dis 		= $(this)
				var id 			= dis.data('id');
				var identity 	= dis.data('identity');
				var count 		= parseInt($('#count').val());
				var new_count   = (count - 1);

				var data 		= {
					'id'		: id,
					'identity'	: identity,
					'count'		: count,
					'new_count'	: new_count
				};

				if(id == '')
				{
					if(count != 0)
					{
						$('#row_'+count+'').remove();

						$('#count').val(new_count);
					}
				}
				else
				{
					$.post($base_url+'bas/bas_disbursement_vouchers/delete_row', data, function(status){

						$('#row_'+count+'').remove();

						$('#count').val(new_count);
					});
				}
			});

		},

		SubmitFormFunction : function(e){
			return this.delegate(elements.formDefault,'submit',function(e){
				e.preventDefault();
				e.stopImmediatePropagation();
				    
				if ( $(this).parsley().isValid() )
				{
					var data = $(this).serialize();

					//button_loader('save', 1);

					$.post($base_url+"bas/bas_disbursement_vouchers/process/", data, function(result){
						if(result.flag == 0)
						{
							notification_msg("ERROR", result.msg);
							//button_loader('save', 0);
						} 
						else 
						{
							notification_msg("SUCCESS", result.msg);
							//button_loader("save",0);
							$('.cancel_modal').trigger('click');

							load_datatable('tbl_disbursement', '/bas/bas_disbursement_vouchers/get_disbursement_list/');
						}
					}, 'json');       
				}
			});

		},

		dropdownFunction : function(){
			return this.delegate(elements.dropdown,'change', function(e){
				e.stopImmediatePropagation();
				var dis 				= $(this);
				var value 				= dis.val();
				var identity 			= dis.data('identity');
				var target 				= dis.data('target');
				var target2 			= dis.data('target2');
				var security			= $('input[name="security"]').val();
				var new_value_options   = '[';
				var data 				= {'value': value, 'identity':identity, 'target':target, 'security': security , 'details': $('#details').val()};

				switch(identity)
				{
					//---------------------------------------------------------------------------------
					case 'payee':
				        $.post($base_url+'bas/bas_disbursement_vouchers/fill_dropdowns',data,function(data){
				        	$.each(data, function(key, val){
				        		$('#address').val(val.payee_address);
				        		for(var a = 0;a <= parseInt($('#count').val()); a++)
				        		{
				        			var new_tax_uri = 'modal_tax_init("'+$('input[name="security"]').val()+'/tax/cmdf/'+a+'/'+val.payee_id+'")';
				        			$('.tax_link_'+a+'').attr('onclick', new_tax_uri);

				        			if($('input[name="status"]').val() != 'POSTED')
				        			{
					        			var selectize3 = $(dis)[0].selectize;
										selectize3.disable();
										$('.if').attr('disabled','disabled');

										$('#payee').val(val.payee_id);
									}
				        		}
				        	});
						},'json');
				    break;

					//---------------------------------------------------------------------------------
					case 'fund':

				        $.post($base_url+'bas/bas_disbursement_vouchers/load_tables',data,function(html){
							$('.table-contents').html(html);
							if($('input[name="status"]').val() != 'POSTED')
							{
								 var selectize = $(dis)[0].selectize;
								selectize.disable();
							}
						});
				    break;

					//---------------------------------------------------------------------------------
				    case 'obligation_slip':

					  	$.post($base_url+'bas/bas_disbursement_vouchers/fill_dropdowns/', data, function(result){

					  		$.each(result, function(key, val){

		                        var keyPlus = parseInt(key) + 1;

		                        if (keyPlus == result.length) {
		                            new_value_options += '{text: "'+val.bill_no+'", value: '+val.billing_id+'}';
		                        } else {
		                            new_value_options += '{text: "'+val.bill_no+'", value: '+val.billing_id+'}';
		                        }
		                    });

		                    new_value_options   += ']';
		                    new_value_options = eval('(' + new_value_options + ')');

		                    if (new_value_options[0] != undefined)
		                    {
		                        var selectize = $(target)[0].selectize;
		 
		                        selectize.clear();
		                        selectize.clearOptions();
		                        selectize.renderCache['option'] = {};
		                        selectize.renderCache['item'] = {};
		                        selectize.addOption(new_value_options);
		                        selectize.setValue(new_value_options[0].value);
		                        selectize.setValue($('#bill_no_2').val());

		                        if($('input[name="status"]').val() != 'POSTED')
								{
									var selectize2 = $(dis)[0].selectize;
									selectize2.disable();
									selectize.disable();

								}
		                    }
		                    else
		                    {
		                    	var selectize = $(target)[0].selectize;
		 
		                        selectize.clear();
		                        selectize.clearOptions();
		                        selectize.renderCache['option'] = {};
		                        selectize.renderCache['item'] = {};
		                    }
							
					  	},'json');

					  	
				        $.post($base_url+'bas/bas_disbursement_vouchers/load_tables',data,function(html){
							$('.table-contents-obligation').html(html);
						});

				    break;

					//---------------------------------------------------------------------------------
					case 'case_no':

						$.post($base_url+'bas/bas_disbursement_vouchers/fill_dropdowns/', data, function(result){

					  		$.each(result, function(key, val){

		                        var keyPlus = parseInt(key) + 1;

		                        if (keyPlus == result.length) {
		                            new_value_options += '{text: "'+val.proceeding_stage+'", value: '+val.proceeding_stage_id+'}';
		                        } else {
		                            new_value_options += '{text: "'+val.proceeding_stage+'", value: '+val.proceeding_stage_id+'}';
		                        }
		                    });

		                    new_value_options   += ']';
		                    new_value_options = eval('(' + new_value_options + ')');

		                    if (new_value_options[0] != undefined)
		                    {
		                        var selectize = $(target)[0].selectize;
		 
		                        selectize.clear();
		                        selectize.clearOptions();
		                        selectize.renderCache['option'] = {};
		                        selectize.renderCache['item'] = {};
		                        selectize.addOption(new_value_options);
		                        selectize.setValue(new_value_options[0].value);
		                        selectize.setValue($('#hidden_proceeding').val());

		                        if($('input[name="status"]').val() != 'POSTED')
								{
									var selectize2 = $(dis)[0].selectize;
									selectize2.disable();
									selectize.disable();

								}
		                    }
		                    else
		                    {
		                    	var selectize = $(target)[0].selectize;
		 
		                        selectize.clear();
		                        selectize.clearOptions();
		                        selectize.renderCache['option'] = {};
		                        selectize.renderCache['item'] = {};
		                    }
							
					  	},'json');

					  	var new_value_options2   = '[';

					  	//arbitrator dropdown----------------------------------------------------------------

					  	$.post($base_url+'bas/bas_disbursement_vouchers/fill_dropdowns/', {'value': value, 'identity':'arbitrator', 'target':target, 'security': security , 'details': $('#details').val()}, function(result){

					  		$.each(result, function(key, val){

		                        var keyPlus = parseInt(key) + 1;

		                        if (keyPlus == result.length) {
		                            new_value_options2 += '{text: "'+val.name+'", value: '+val.arbitrator_id+'}';
		                        } else {
		                            new_value_options2 += '{text: "'+val.name+'", value: '+val.arbitrator_id+'}';
		                        }

		                    });

		                    new_value_options2   += ']';
		                    new_value_options2 = eval('(' + new_value_options2 + ')');

		                    if (new_value_options2[0] != undefined)
		                    {
		                        var selectize = $(target2)[0].selectize;
		 
		                        selectize.clear();
		                        selectize.clearOptions();
		                        selectize.renderCache['option'] = {};
		                        selectize.renderCache['item'] = {};
		                        selectize.addOption(new_value_options2);
		                        selectize.setValue(new_value_options2[0].value);
		                        selectize.setValue($('#hidden_arbitrator').val());

		                        if($('input[name="status"]').val() != 'POSTED')
								{
									var selectize2 = $(dis)[0].selectize;
									selectize2.disable();
									selectize.disable();

								}
		                    }
		                    else
		                    {
		                    	var selectize = $(target)[0].selectize;
		 
		                        selectize.clear();
		                        selectize.clearOptions();
		                        selectize.renderCache['option'] = {};
		                        selectize.renderCache['item'] = {};
		                    }
							
					  	},'json');
				        
				    break;

				    case 'arbitrator':
			    		//for fee
				        $.post($base_url+'bas/bas_disbursement_vouchers/get_amount',data,function(amount){
							$('#arbitrator_fee').val(amount.total_amount);
							$('.show_fee').html(amount.total_amount);
						},'json');

				        //for deductions
						$.post($base_url+'bas/bas_disbursement_vouchers/get_arbitrator_from_isca_deduction',data,function(row){
							$('.isca_deductions').html(row);
						});

				    break;
				}
				
			});
		},

	}

	$.extend(elements.doc,basDisbursementVoucher);
	elements.doc.SubmitFormFunction();
	elements.doc.dropdownFunction();
	elements.doc.addRowFunction();
	elements.doc.deleteRowFunction();
	elements.doc.statusFunction();

}(jQuery,window,document));	


function CommaCustom(Num, count) {
	$(function($){
		$('#tax_amount_'+count+', #base_tax_amount_'+count+', #ewt_rate_'+count+', #ewt_amount_'+count+', #vat_rate_'+count+', #vat_amount_'+count+'').val('');
		$('#tax_link_'+count+'').html('[ 0 ]');
	});

    Num += '';
    Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
    Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
    x = Num.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1))
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    return x1 + x2;
}