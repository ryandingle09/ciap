var elements = {
	doc      						: $(document),
	formDefault						: '#form',
	dropdown						: '.dropdown',
}


;(function(){

	var basReports = {

		SubmitFormFunction : function(e){
			return this.delegate(elements.formDefault,'submit',function(e){
				e.preventDefault();
				e.stopImmediatePropagation();

				var dis 	= $(this)
				var data 	= $(this).serialize();

				$.post($base_url+'bas/bas_reports/validate',data, function(response){
					if(response != '1') notification_msg('ERROR', response);
					else
					{
						$('#form').attr('method', 'POST');
						$('#form').attr('action', $base_url+'bas/bas_reports/generate_data');
						$('#form').addClass('form');
						$('#form').removeAttr('id');
						$('.form').attr('id','realForm');
						$('#realForm').submit();
					}
				},'json');

			});
		},		

		dropdownFunction : function(e){
			return this.delegate(elements.dropdown,'change',function(e){
				e.stopImmediatePropagation();

				var dis 		= $(this)
				var value 		= $(this).val();
				var value2 		= $('#year').val();
				var identity 	= $(this).data('identity');
				var target		= $(this).data('target');
				var data 		= { 'identity' : identity, 'target': target, 'value': value, 'value2': value2};
                var new_value_options   = '[';

				if(identity == 'year')
				{
					$.post($base_url+'bas/bas_reports/fill_dropdowns/', data, function(result){
				  		
				  		$.each(result, function(key, val){
	 
	                        var keyPlus = parseInt(key) + 1;

	                        if (keyPlus == result.length) {
	                            new_value_options += '{text: "'+val.source_type_name+'", value: '+val.source_type_num+'}';
	                        } else {
	                            new_value_options += '{text: "'+val.source_type_name+'", value: '+val.source_type_num+'},';
	                        }
	                    });


	                    new_value_options   += ']';
	                    new_value_options = eval('(' + new_value_options + ')');

	                    if (new_value_options[0] != undefined)
	                    {
	                        var selectize = $('#'+target+'')[0].selectize;
	 
	                        selectize.clear();
	                        selectize.clearOptions();
	                        selectize.renderCache['option'] = {};
	                        selectize.renderCache['item'] = {};
	                        selectize.addOption(new_value_options);
	                        //selectize.setValue(new_value_options[0].value);
	                        selectize.setValue($('#'+target+'').data('selected'));
	                    }
	                    else
	                    {
	                        var selectize = $('#'+target+'')[0].selectize;
	 
	                        selectize.clear();
	                        selectize.clearOptions();
	                        selectize.renderCache['option'] = {};
	                        selectize.renderCache['item'] = {};
	                    }
						
				  	},'json');
				}
				else if(identity == 'fund_source')
				{
					$.post($base_url+'bas/bas_reports/fill_dropdowns/', data, function(result){
				  		
				  		$.each(result, function(key, val){
	 
	                        var keyPlus = parseInt(key) + 1;

	                        if (keyPlus == result.length) {
	                            new_value_options += '{text: "'+val.line_item_name+'", value: '+val.line_item_id+'}';
	                        } else {
	                            new_value_options += '{text: "'+val.line_item_name+'", value: '+val.line_item_id+'},';
	                        }
	                    });


	                    new_value_options   += ']';
	                    new_value_options = eval('(' + new_value_options + ')');

	                    if (new_value_options[0] != undefined)
	                    {
	                        var selectize = $('#'+target+'')[0].selectize;
	 
	                        selectize.clear();
	                        selectize.clearOptions();
	                        selectize.renderCache['option'] = {};
	                        selectize.renderCache['item'] = {};
	                        selectize.addOption(new_value_options);
	                        //selectize.setValue(new_value_options[0].value);
	                        selectize.setValue($('#'+target+'').data('selected'));
	                    }
	                    else
	                    {
	                        var selectize = $('#'+target+'')[0].selectize;
	 
	                        selectize.clear();
	                        selectize.clearOptions();
	                        selectize.renderCache['option'] = {};
	                        selectize.renderCache['item'] = {};
	                    }
						
				  	},'json');
				}
				else if(identity == 'report_type')
				{
					if(value == 'SMFO')
			 		{
			 			$('.5').hide(2);
			 			$('#line_item').removeAttr('required','readonly');
			 			$('#allotment_class').removeAttr('required','readonly');

			 			$('.4').show(2);
			 			$('#office').attr('required','required');
			 			$('#office').attr('readonly','readonly');

			 			$.post($base_url+'bas/bas_reports/fill_dropdowns/', data, function(result){
				  		
					  		$.each(result, function(key, val){
		 
		                        var keyPlus = parseInt(key) + 1;

		                        if (keyPlus == result.length) {
		                            new_value_options += '{text: "'+val.office_name+'", value: '+val.office_num+'}';
		                        } else {
		                            new_value_options += '{text: "'+val.office_name+'", value: '+val.office_num+'},';
		                        }
		                    });


		                    new_value_options   += ']';
		                    new_value_options = eval('(' + new_value_options + ')');

		                    if (new_value_options[0] != undefined)
		                    {
		                        var selectize = $('#'+target+'')[0].selectize;
		 
		                        selectize.clear();
		                        selectize.clearOptions();
		                        selectize.renderCache['option'] = {};
		                        selectize.renderCache['item'] = {};
		                        selectize.addOption(new_value_options);
		                        //selectize.setValue(new_value_options[0].value);
		                        selectize.setValue($('#'+target+'').data('selected'));
		                    }
		                    else
		                    {
		                        var selectize = $('#'+target+'')[0].selectize;
		 
		                        selectize.clear();
		                        selectize.clearOptions();
		                        selectize.renderCache['option'] = {};
		                        selectize.renderCache['item'] = {};
		                    }
							
					  	},'json');
			 		}
			 		else if($(this).val() == 'RAO')
			 		{
			 			$('.4').hide(2);
			 			$('#office').removeAttr('required','readonly');

			 			$('.5').show(2);
			 			$('#line_item').attr('required','');
			 			$('#line_item').attr('readonly','readonly');
			 			$('#allotment_class').attr('required' ,'');
			 			$('#allotment_class').attr('readonly', 'readonly');

			 			$('.excel').prop('checked', true);
			 			$('.pdf').attr('disabled','disabled');
			 		}
			 		else
			 		{
			 			$('.4, .5').hide();
			 			$('#office').removeAttr('required', 'readonly');
			 			$('#line_item').removeAttr('required', 'readonly');
			 			$('#allotment_class').removeAttr('required', 'readonly');

			 			$('.excel').prop('checked', false);
			 			$('.pdf').removeAttr('disabled');

			 			var selectize = $('#year')[0].selectize;
		                    selectize.setValue($('').data('selected'));
				 	}
				}
				else notification_msg('ERROR', 'Invalid Action detected.');
			});
		},		
	}

	$.extend(elements.doc,basReports);
	elements.doc.SubmitFormFunction();
	elements.doc.dropdownFunction();

}(jQuery,window,document));	