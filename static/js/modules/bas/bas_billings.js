var elements = {
	//base					: $base_url,
	doc      						: $(document),
	formDefault						: '#form',
}

;(function(){

	var BasBillings = {

		SubmitFormFunction : function(e){
			return this.delegate(elements.formDefault,'submit',function(e){
				e.preventDefault();
				//e.stopImmediatePropagation();
				//codes here
			});
		},		
	}

	$.extend(elements.doc,BasBillings);
	elements.doc.SubmitFormFunction();

}(jQuery,window,document));	