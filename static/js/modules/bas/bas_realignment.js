var elements = {
	//base					: $base_url,
	doc      						: $(document),
	formDefault						: '#form',
	save 							: '#save',
	getAccountcodes					: '.getAccountCodes',
	getFundSources					: '.getFundSources',
	getAccountCodesCurrentAmount	: '.getAccountCodesCurrentAmount',
	getLineItemsByFundSources 		: '.getLineItemsByFundSources',
	num_cheker 						: '.num',
}

;(function(){

	var BasRealignment = {

		numCheckerFunction : function(){
			return this.delegate(elements.num_cheker,'keypress',function(e){
				//auto comma
				Num = $(this).val();
				Num += '';
			    Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
			    Num = Num.replace(',', ''); Num = Num.replace(',', ''); Num = Num.replace(',', '');
			    x = Num.split('.');
			    x1 = x[0];
			    x2 = x.length > 1 ? '.' + x[1] : '';
			    var rgx = /(\d+)(\d{3})/;
			    while (rgx.test(x1))
			        x1 = x1.replace(rgx, '$1' + ',' + '$2');
			    $(this).val(x1 + x2);

			    //filter chars
			    var value=$(this).val().replace(/[^0-9.,]*/g, '');
                value=value.replace(/\.{2,}/g, '.');
                value=value.replace(/\.,/g, ',');
                value=value.replace(/\,\./g, ',');
                value=value.replace(/\,{2,}/g, ',');
                value=value.replace(/\.[0-9]+\./g, '.');
                $(this).val(value);
			});	
		},

		SubmitFormFunction : function(e){
			return this.delegate(elements.save,'click',function(e){
				e.preventDefault();
				e.stopImmediatePropagation();
				
				var dis = $(this)
				$('#form').attr('data-status',dis.data('status'));
				$('#status').val(dis.data('status'));
				if(dis.data('status') == '0')
				{
					$('#confirm_modal').confirmModal({
						topOffset : 0,
						onOkBut : function() {
							$('#form').submit();
						},
						onCancelBut : function() {},
						onLoad : function() {
							$('.confirmModal_content h4').html('Are you sure you want to post this Reslignment ?');	
							$('.confirmModal_content p').html('This action will permanently posted this record from the database and cannot be edited.');
						},
						onClose : function() {}
					});
				}else $('#form').submit();
			});
		},

		SubmitRealignmentFunction : function(e){
			return this.delegate(elements.formDefault,'submit',function(e){
				e.preventDefault();
				e.stopImmediatePropagation();
				
				var dis = $(this)
				var data = dis.serialize();

				$.post($base_url+'bas/bas_realignment/check_excess_amount/',data,function(result1){
					if(result1.status == '0')
					{
						if ( dis.parsley().isValid() ) {
						  	//button_loader('save', 1);
						  	$.post($base_url+'bas/bas_realignment/process/', data, function(result) {
							if(result.flag == 0)
							{
							  	notification_msg(result.error_class, result.msg);
							  	//button_loader('save', 0);
							}
							else
							{
							  	notification_msg(result.error_class, result.msg);
							  	//button_loader("save",0);
							  	modalObj.closeModal();

							  	load_datatable('realignment_table', 'bas/bas_realignment/get_realignment_list/');
							}
						  }, 'json');       
					    }
					}
					else
					{
						notification_msg("ERROR","Realigned Amount cannot exceed on the account code current amount.");
					}
				},'json');
			});
		},

		getLineItemsByFundSourcesFunction : function(e){
			return this.delegate(elements.getLineItemsByFundSources,'change',function(e){
				e.preventDefault();
				e.stopImmediatePropagation();
				
				var dis = $(this)
				var target1 = dis.data('target1');
				var target2 = dis.data('target2');
				var id = dis.val();
				var year = $('#source_year').val();
				var data = {'target1': target1,'target2': target2, 'year':year, 'id':id};
                var new_value_options   = '[';

			  	$.post($base_url+'bas/bas_realignment/get_line_items_by_fund_sources/', data, function(result){
			  		
			  		$.each(result, function(key, val){
 
                        var keyPlus = parseInt(key) + 1;

                        if (keyPlus == result.length){
                            new_value_options += '{text: "'+val.line_item_name+'", value: '+val.line_item_id+'}';
                        }else{
                            new_value_options += '{text: "'+val.line_item_name+'", value: '+val.line_item_id+'},';
                        }
                    });

                    new_value_options   += ']';
                    new_value_options = eval('(' + new_value_options + ')');

                    if (new_value_options[0] != undefined)
                    {
                        var selectize = $('#'+target1+'')[0].selectize;
                        var selectize2 = $('#'+target2+'')[0].selectize;
 
                        selectize.clear();
                        selectize.clearOptions();
                        selectize.renderCache['option'] = {};
                        selectize.renderCache['item'] = {};
                        selectize.addOption(new_value_options);
                        //selectize.setValue(new_value_options[0].value);
                        selectize.setValue($('#'+target1+'').data('selected'));

                        selectize2.clear();
                        selectize2.clearOptions();
                        selectize2.renderCache['option'] = {};
                        selectize2.renderCache['item'] = {};
                        selectize2.addOption(new_value_options);
                        //selectize2.setValue(new_value_options[0].value);
                        selectize2.setValue($('#'+target2+'').data('selected'));
                    }
                    else
                    {
                        var selectize = $('#'+target1+'')[0].selectize;
                        var selectize2 = $('#'+target2+'')[0].selectize;
 
                        selectize.clear();
                        selectize.clearOptions();
                        selectize.renderCache['option'] = {};
                        selectize.renderCache['item'] = {};

                        selectize2.clear();
                        selectize2.clearOptions();
                        selectize2.renderCache['option'] = {};
                        selectize2.renderCache['item'] = {};
                    }
					
			  	},'json');

			});
		},

		getFundSourcesFunction : function(e){
			return this.delegate(elements.getFundSources,'change',function(e){
				e.preventDefault();
				e.stopImmediatePropagation();
				
				var dis = $(this)
				var target = dis.data('target');
				var year = dis.val();
				var data = {'target': target, 'year':year};
                var new_value_options   = '[';

			  	$.post($base_url+'bas/bas_realignment/get_fund_sources_by_year/', data, function(result){
			  		
			  		$.each(result, function(key, val){
 
                        var keyPlus = parseInt(key) + 1;

                        if (keyPlus == result.length) {
                            new_value_options += '{text: "'+val.source_type_name+'", value: '+val.source_type_num+'}';
                        } else {
                            new_value_options += '{text: "'+val.source_type_name+'", value: '+val.source_type_num+'},';
                        }
                    });


                    new_value_options   += ']';
                    new_value_options = eval('(' + new_value_options + ')');

                    if (new_value_options[0] != undefined)
                    {
                        var selectize = $('#'+target+'')[0].selectize;
 
                        selectize.clear();
                        selectize.clearOptions();
                        selectize.renderCache['option'] = {};
                        selectize.renderCache['item'] = {};
                        selectize.addOption(new_value_options);
                        //selectize.setValue(new_value_options[0].value);
                        selectize.setValue($('#'+target+'').data('selected'));
                    }
                    else
                    {
                        var selectize = $('#'+target+'')[0].selectize;
 
                        selectize.clear();
                        selectize.clearOptions();
                        selectize.renderCache['option'] = {};
                        selectize.renderCache['item'] = {};
                    }
					
			  	},'json');

			});
		},

		getAccountCodeByLineItemFunction : function(e){
			return this.delegate(elements.getAccountcodes,'change',function(e){
				e.preventDefault();
				e.stopImmediatePropagation();
				
				var dis = $(this)
				var target = dis.data('target');
				var id = dis.val();
				var data = {'target': target, 'id':id};
                var new_value_options   = '[';

                $('#line_item_id_trigger').val(id);

			  	$.post($base_url+'bas/bas_realignment/get_account_codes_by_line_item/', data, function(result){
			  		
			  		$.each(result, function(key, val){
 
                        var keyPlus = parseInt(key) + 1;

                        if (keyPlus == result.length) {
                            new_value_options += '{text: "'+val.account_code+' - '+val.account_name+'", value: '+val.account_code+'}';
                        } else {
                            new_value_options += '{text: "'+val.account_code+' - '+val.account_name+'", value: '+val.account_code+'},';
                        }
                    });


                    new_value_options   += ']';
                    new_value_options = eval('(' + new_value_options + ')');

                    if (new_value_options[0] != undefined)
                    {
                        var selectize = $('#'+target+'')[0].selectize;
 
                        selectize.clear();
                        selectize.clearOptions();
                        selectize.renderCache['option'] = {};
                        selectize.renderCache['item'] = {};
                        selectize.addOption(new_value_options);
                        //selectize.setValue(new_value_options[0].value);
                        selectize.setValue($('#'+target+'').data('selected'));
                    }
                    else
                    {
                        var selectize = $('#'+target+'')[0].selectize;
 
                        selectize.clear();
                        selectize.clearOptions();
                        selectize.renderCache['option'] = {};
                        selectize.renderCache['item'] = {};
                    }
					
			  	},'json');

			});
		},

		getAccountCodeByCurrentAmountFunction : function(e){
			return this.delegate(elements.getAccountCodesCurrentAmount,'change',function(e){
				e.preventDefault();
				e.stopImmediatePropagation();

				var dis = $(this)
				var target 				= dis.data('target');
				var account_code 		= dis.val();
				var trigger1 			= $('#source_line_item_id').val();
				var trigger2 			= $('#recipient_line_item_id').val();

			  	var data2 				= {'target': 'target_amount_2', 'line_item_id': trigger2, 'account_code': $('#recipient_account_code').val()};
				var data1 				= {'target': 'target_amount_1', 'line_item_id': trigger1, 'account_code': $('#source_account_code').val()};

			  	$.post($base_url+'bas/bas_realignment/get_account_codes_by_amount/', data1, function(result){
			  		$('#target_amount_1').html(result);
			  		$('.target_amount_1').val(result);
			  	});
			  	$.post($base_url+'bas/bas_realignment/get_account_codes_by_amount/', data2, function(result){
			  		$('#target_amount_2').html(result);
			  		$('.target_amount_2').val(result);
			  	});

			});
		},
		
	}

	$.extend(elements.doc,BasRealignment);
	elements.doc.SubmitRealignmentFunction();
	elements.doc.getAccountCodeByLineItemFunction();
	elements.doc.getAccountCodeByCurrentAmountFunction();
	elements.doc.getFundSourcesFunction();
	elements.doc.getLineItemsByFundSourcesFunction();
	elements.doc.SubmitFormFunction();
	elements.doc.numCheckerFunction();

}(jQuery,window,document));	