$(function(){
	var $base_url = $("#base_url").val();
		$home_page = $("#home_page").val();
		$login = $("#login_form");
		$logout = $("#logout");		

	$login.submit(function(event){
		var data = $("#login_form").serialize();
		event.preventDefault();
	
		button_loader('submit_login', 1);
		$.post($base_url + "auth/sign_in/", data, function(result) {
			if(result.flag == 0){			
				Materialize.toast(result.msg, 3000, '', function(){
					button_loader('submit_login', 0);
				});
			} else {
				window.location = $base_url + $home_page;
			}			  						
		}, 'json');	
	});
	
	$logout.on("click", function(){
		$.post($base_url + "auth/sign_out/", function(result) {
			
			window.location = result.url + 'auth/sign_out/';
			/*if(result.flag == 0){
				Materialize.toast(result.msg, 3000);
			}
			else
			{
				console.log(result.url);
				console.log('MERON');
				console.log('ciap_intranet_session' + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;');
				//window.location = $base_url;
				//document.cookie = 'ciap_intranet_session' + '=asdf;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
				window.location = result.url
			}	*/
		}, 'json');
	});
	
});