	$(function() {
		var counter_id = 1;
		var $base_url = $("#base_url").val();

		$('ul.tabs').tabs();
		$('#content_slip').css("display", "none");

		$('#form_order_payment').parsley();

		//first form validate submit
		$("#btnContinue").click(function() {

			$('#form_order_payment').submit(function(e) {
	  
				e.preventDefault();

		    	console.log("submitted");

				if ( $(this).parsley().isValid() ) {
				
					var data = $(this).serialize();
					
			  		var title_mid 			= ' vs. ';
			  			total_claimant 		= 0;
			  			total_respondent 	= 0;

					button_loader('btnContinue', 1);

					$.post($base_url+"isca/isca_payments_order/validate_parties/", data, function(result) {

						button_loader('btnContinue', 0);
						
						if(result.flag == 1) {
						
							// check parties
							$.post($base_url+"isca/isca_payments_order/check_parities", data, function(result_party) {

								if (result_party.flag == 1) {

									var claimants 		= '';
									var respondents 	= '';

									result_party.data.forEach(function(entry) {

										if(entry.party_type == '1') {

											if(entry.status_flag == "A") {

												//new party
												claimants 	+= "<span class='new_entry'>"+entry.party_name+"</span>"+', ';

											} else if (entry.status_flag == "D") {

												//deleted party
												claimants 	+= "<span class='deleted_entry'>"+entry.party_name+"</span>"+', ';

											} else {

												//orignal party
												claimants 	+= entry.party_name+', ';
											}

											total_claimant	+= 1;
										}

										if(entry.party_type == '2') {

											if(entry.status_flag == "A") {

												//new party
												respondents 		+= "<span class='new_entry'>"+entry.party_name+"</span>"+', ';
												total_respondent	+= 1;

											} else if (entry.status_flag == "D") {

												//deleted party
												respondents += "<span class='deleted_entry'>"+entry.party_name+"</span>"+', ';

											} else {

												//orignal party
												respondents 		+= entry.party_name+', ';
												total_respondent	+= 1;
											}
										}
									});
									
									// empty data means creating new 
									if(result_party.data.length == 0) {

										var claimants = '';

									    $('.claimant_name').find('option').each(function() {
									    	
									    	claimants	+= $(this).text()+', ';
									    	total_claimant+=1;
										});

										$('.respondent_name').find('option').each(function() {
						    	
									    	respondents	+= $(this).text()+', ';
									    	total_respondent+=1;
										});	

									}

									claimants 		= claimants.replace(/(^\s*,)|(,\s*$)/g, '');
									var len 		= (claimants.match(/,/g) ) ? claimants.match(/,/g).length : 0;
									respondents 	= respondents.replace(/(^\s*,)|(,\s*$)/g, '');
									
									if(len > 0) {

										title_mid 	= " etal. ";
									}

									$("#total_claimant").text("Total Claimant(s): "+total_claimant);
									$("#total_respondent").text("Total Respondet(s): "+total_respondent);
									$('#short_title span').html(claimants + title_mid + respondents);
									$("#case_title").val(claimants + title_mid + respondents);

						    		$('ul.tabs').tabs('select_tab', 'contracts');

						    		//rest total counts
								    total_claimant = 0;
								    total_respondent = 0;

								} else {

									notification_msg(result_party.class, result_party.msg);									
								}

							}, 'json');


						    //rest total counts
						    total_claimant = 0;
						    total_respondent = 0;

						} else {

							notification_msg(result.class, result.msg);
							
						}
							
			  		}, 'json');       
		    	}
			});
			
		});
		
		//2nd form validate submit
		$("#btnContinueContract").click(function() {

			$('#form_order_payment_contract').submit(function(e) {
	  
				e.preventDefault();
		    
				if ( $(this).parsley().isValid() ) {
				
					var data = $(this).serialize();
			  		
					button_loader('btnContinueContract', 1);

					$.post($base_url+"isca/isca_payments_order/validate_contract/", data, function(result) {

						button_loader('btnContinueContract', 0);
						
						if(result.flag == 1) {

						    $('ul.tabs').tabs('select_tab', 'order_of_payments');

						} else {

							notification_msg(result.class, result.msg);
						}
							
			  		}, 'json');       
		    	}
			});
			
		});

		//submit form process
		$("#btnContinueSubmit").click(function () {

			$('#form_order_payment_process').submit(function(e) {
	  
				e.preventDefault();
		    
				if ( $(this).parsley().isValid() ) {
					
					var parties_data = $("#form_order_payment").serializeArray();
					var contract_data = $("#form_order_payment_contract").serializeArray();
					var payment_data = $("#form_order_payment_process").serializeArray();

					var data = parties_data.concat(contract_data, payment_data);

					button_loader('btnContinueSubmit', 1);

					$.post($base_url+"isca/isca_payments_order/process/", data, function(result) {

						button_loader('btnContinueSubmit', 0);
						
						if (result.flag == 1) {

							notification_msg(result.class, result.msg);
							location.href=$base_url+"isca/isca_payments_order";

						} else {

							notification_msg(result.class, result.msg);
						}
							
			  		}, 'json');       
		    	}
			});

		});

		/*back*/
		$("#btnBack").click(function() {
			$('ul.tabs').tabs('select_tab', 'parties');
		});
		$("#btnBackOP").click(function() {
			$('ul.tabs').tabs('select_tab', 'contracts');
		});

		// HIDE MEDIATION CONTENT BY DEFAULT 
		/*$("#mediation_amount_table").css("display", "none");*/

		//writable select
		selectize_render("claimant_table");
    	selectize_render("respondent_table");
		
		//normal select
		$("#contracts select").selectize({create:false});
		$("#type_payment").selectize({create:false});

		$("#add_claimant").click(function() {
			var fields = "<tr>"+
							"<td>"+
								"<select class='claimant_name' id='claimant_name_"+counter_id+"'name='claimant_name[]'>"+
									"<option value='' selected> Select Claimant </option>"+
								"</select>"+
							"</td>"+
							"<td>"+
								"<select class='claimant_type' id='claimant_type_"+counter_id+"' name='claimant_type[]'>"+
									"<option value='C' selected>Claimant</option>"+
									"<option value='CC' >Cross Claimant</option>"+
									"<option value='TPC' >Type-party Claimant</option>"+
								"</select>"+
							"</td>"+
							"<td>"+ 
								"<div class='input-field'>"+
						 			"<input type='text' class='claimant_address' name = 'claimant_address[]'>"+
						 		"</div>"+
					 		"</td>"+

					 		"<td>"+ 
								"<div class='input-field'>"+
						 			"<input type='text' class='claimant_contact' name='claimant_contact[]'>"+
						 		"</div>"+
					 		"</td>"+

							"<td>"+ 
								"<div class='table-actions'>"+
									"<a href='javascript:;' class='delete delete_claimant tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50' ></a>"+
								"</div>"+
							"</td>"+
						"</tr>";

			$(".tbody").find('tr:last').before(fields);

			create_selectize_render("claimant_name_");

			selectize_render("claimant_table");	
		});

		$("#add_respondent").click(function() {
			var fields = "<tr>"+
							"<td>"+
								"<select class='respondent_name' id='respondent_name_"+counter_id+"' name ='respondent_name[]'>"+
								"<option value='' selected> Select Respondent </option>"+
								"</select>"+
							"</td>"+

							"<td>"+
								"<select class='respondent_type' id='respondent_type_"+counter_id+"' name='respondent_type[]'>"+
									"<option value='C' selected>Respondent</option>"+
									"<option value='TPR' >Type-party Respondent</option>"+
								"</select>"+
							"</td>"+

							"<td>"+ 
								"<div class='input-field'>"+
						 			"<input type='text' class='respondent_address' name='respondent_address[]'>"+
						 		"</div>"+
					 		"</td>"+

					 		"<td>"+ 
								"<div class='input-field'>"+
						 			"<input type='text' class='respondent_contact' name='respondent_contact[]'>"+
						 		"</div>"+
					 		"</td>"+

				 			"<td>"+ 
				 				"<div class='table-actions'>"+
				 					"<a href='javascript:;' class='delete delete_respondent tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>"+
				 				"</div>"+
				 			"</td>"+

						"</tr>";

			$(".tbody_respondent").find('tr:last').before(fields);
			create_selectize_render("respondent_name_");
			selectize_render("respondent_table");
		});

		$("#arbitration_add_amount").click(function(){
			var fields = "<tr>"+

						"<td>"+ 
							"<div class='input-field'>"+
					 			"<input type='text' name='arbitration_contract_description[]'>"+
					 		"</div>"+
				 		"</td>"+

						"<td>"+
							"<select class='type_claim[]' name='type_claim[]'>"+
								"<option value='M' selected> Monetary </option>"+
								"<option value='N' > Non-Monetary with Monetary Cruz </option>"+
								"<option value='P' > Purely Non-Monetary </option>"+
							"</select>"+
						"</td>"+

						"<td>"+ 
							"<div class='input-field'>"+
					 			"<input type='text' name='arbitration_contract_amount[]'>"+
					 		"</div>"+
				 		"</td>"+

			 			"<td>"+ 
			 				"<div class='table-actions'>"+
			 					"<a href='javascript:;' class='delete delete_amount tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>"+
			 				"</div>"+
			 			"</td>"+
					"</tr>";

			$("#arbitration_amount_table").find('tr:last').before(fields);

			$("#contracts select").selectize({create:true});
		});

		$("#mediation_add_amount").click(function(){
			var fields = "<tr>"+

						"<td>"+ 
							"<div class='input-field'>"+
					 			"<input type='text' name='mediation_contract_description[]'>"+
					 		"</div>"+
				 		"</td>"+

						"<td>"+ 
							"<div class='input-field'>"+
					 			"<input type='text' name='mediation_contract_amount[]'>"+
					 		"</div>"+
				 		"</td>"+

			 			"<td>"+ 
			 				"<div class='table-actions'>"+
			 					"<a href='javascript:;' class='delete delete_amount tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>"+
			 				"</div>"+
			 			"</td>"+
					"</tr>";

			$("#mediation_amount_table").find('tr:last').before(fields);

			$("#contracts select").selectize({create:true});
		});

		// delete claimant tr
		$("#claimant_table").on('click' , '.delete_claimant', (function() {

			var tr_count = $('#claimant_table>tbody>tr').length;

			/*trap for deleting all table row*/
			if( tr_count > 2) {
				$(this).closest('tr').remove();
			} else {

				alert("Content cannot be empty.");
			}
		}));

		// delete claimant tr
		$("#respondent_table").on('click' , '.delete_respondent', (function() {
			
			var tr_count = $('#respondent_table>tbody>tr').length;

			/*trap for deleting all table row*/
			if( tr_count > 2) {
				$(this).closest('tr').remove();
			} else {

				alert("Content cannot be empty.");
			}
		}));

		// delete amount tr arbitration tbl
		$("#arbitration_amount_table").on('click' , '.delete_amount', (function() {
			
			var tr_count = $('#arbitration_amount_table>tbody>tr').length;

			/*trap for deleting all table row*/
			if( tr_count > 2) {
				$(this).closest('tr').remove();
			} else {

				alert("Content cannot be empty.");
			}
		}));

		// delete amount tr mediation tbl
		$("#mediation_amount_table").on('click' , '.delete_amount', (function() {
			
			var tr_count = $('#mediation_amount_table>tbody>tr').length;

			/*trap for deleting all table row*/
			if( tr_count > 2) {
				$(this).closest('tr').remove();
			} else {

				alert("Content cannot be empty.");
			}
		}));

		$("#generate_slip").click(function() {

			$("#content_slip").css("display","block");
		});

		/*SELECT TRIGGER FOR CHANGING DISPLAYED DATA*/
		$("#mode_of_settlement").change(function() {

			var mode_selected = $(this).val();

			if (mode_selected == 'A') {
				/* 
				* MODE SELECTED = ARBITRATION 
				* DISPLAY FIELDS NEED FOR ARBITRATION
				*/

				$("#mode_of_arbitration").css("display", "block");
				$("#number_of_disputants").css("display", "block");
				$("#mediation_amount_table").css("display", "none");
				$("#arbitration_amount_table").css("display", "block");
			} else {
				/* 
				* MODE SELECTED = MEDIATION 
				* DISPLAY FIELDS NEED FOR MEDIATION
				*/

				$("#mode_of_arbitration").css("display", "none");
				$("#number_of_disputants").css("display", "none");
				$("#mediation_amount_table").css("display", "block");
				$("#arbitration_amount_table").css("display", "none");
			}
		});

		$("#claimant_table").on("change", ".claimant_name", function() {
			var data = [];
				data = { id : $(this).val() };
				

			var address = $(this).closest("tr").find(".claimant_address");
				contact = $(this).closest("tr").find(".claimant_contact");

			$.post($base_url + "isca/isca_payments_order/party_data", data, function(result) {
				
				address.val(result.party_address);
				contact.val(result.party_contact_no);

			}, 'json');
		});

		$("#respondent_table").on("change",".respondent_name", function() {

			var data = [];
				data = { id : $(this).val() };

			var address = $(this).closest("tr").find(".respondent_address");
				contact = $(this).closest("tr").find(".respondent_contact");

			$.post($base_url+"isca/isca_payments_order/party_data", data, function(result) {
				
				address.val(result.party_address);
				contact.val(result.party_contact_no);

			}, 'json');
		});

		/*selectize render*/
		function selectize_render (tbl) {

			$("#"+tbl+" select").selectize({

				create:function (input, callback1) {

					var  data = [];

					data =  {value:input};

	                $.post($base_url+"isca/isca_payments_order/new_claimant", data, function(result) {

		                if(result.flag == 1) {

		                	callback1({value:result.id, text:input});

		                } else {

		                	notification_msg("<?php echo ERROR ?>", result.msg);
		                }

	            	}, 'json');
	        	}

			});
		}

		/*new selectize render data*/
		function create_selectize_render(select_name) {

			$.get($base_url+"isca/isca_payments_order/get_parties", function(data) {

	    		var respondent_select = $("#"+select_name+counter_id)[0].selectize;

		   		respondent_select.clear();
				respondent_select.clearOptions();

				respondent_select.load(function(callback) {

					callback(data.parties);
					counter_id+=1;
				});

        	}, 'json');
		}
	})
	