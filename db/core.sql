-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 16, 2016 at 02:55 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `core_module`
--

-- --------------------------------------------------------

--
-- Table structure for table `actions`
--

CREATE TABLE IF NOT EXISTS `actions` (
  `action_id` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `action_type` enum('MODULE','TASK') NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `actions`
--

INSERT INTO `actions` (`action_id`, `name`, `action_type`) VALUES
(1, 'SAVE', 'MODULE'),
(2, 'ADD', 'MODULE'),
(3, 'EDIT', 'MODULE'),
(4, 'DELETE', 'MODULE'),
(5, 'VIEW', 'MODULE'),
(6, 'PRINT', 'MODULE'),
(7, 'LOCK', 'MODULE'),
(8, 'UPLOAD', 'MODULE');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `activity_id` int(11) unsigned NOT NULL,
  `activity` text NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `apps`
--

CREATE TABLE IF NOT EXISTS `apps` (
  `app_id` int(11) unsigned NOT NULL,
  `app_name` varchar(255) NOT NULL,
  `icon` varchar(100) DEFAULT NULL,
  `link` varchar(255) NOT NULL COMMENT 'This is the url of the application (ex: http://www.google.com)',
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `actions`
--

INSERT INTO `apps` (`app_id`, `app_name`, `link`, `created_by`, `created_date`) VALUES
(1, 'Aisagate Core', 'localhost/ani_core/', 1, '2016-03-11 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `app_roles`
--

CREATE TABLE IF NOT EXISTS `app_roles` (
  `app_id` int(11) unsigned NOT NULL,
  `role_code` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `audit_trail`
--

CREATE TABLE IF NOT EXISTS `audit_trail` (
  `audit_trail_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `module_id` int(11) unsigned NOT NULL,
  `activity` text NOT NULL,
  `ip_address` varchar(255) NOT NULL,
  `user_agent` varchar(120) NOT NULL,
  `activity_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `audit_trail_detail`
--

CREATE TABLE IF NOT EXISTS `audit_trail_detail` (
  `audit_trail_detail_id` int(11) unsigned NOT NULL,
  `audit_trail_id` int(11) unsigned NOT NULL,
  `trail_schema` varchar(25) NOT NULL,
  `action` varchar(10) NOT NULL,
  `field` varchar(100) NOT NULL,
  `prev_detail` text,
  `curr_detail` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `file`
--

CREATE TABLE IF NOT EXISTS `file` (
  `file_id` int(11) unsigned NOT NULL,
  `cy` year(4) NOT NULL,
  `module_id` int(11) unsigned DEFAULT NULL,
  `file_name` varchar(255) NOT NULL,
  `display_name` varchar(250) DEFAULT NULL,
  `description` text,
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `file_versions`
--

CREATE TABLE IF NOT EXISTS `file_versions` (
  `file_version_id` int(11) unsigned NOT NULL,
  `file_id` int(11) unsigned NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `display_name` varchar(250) DEFAULT NULL,
  `description` text,
  `minor_revision_flag` tinyint(1) NOT NULL DEFAULT '0',
  `version` float(3,1) DEFAULT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `locations`
--

CREATE TABLE IF NOT EXISTS `locations` (
  `location_code` char(12) NOT NULL,
  `name` varchar(255) NOT NULL,
  `location_parent` char(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `module_id` int(11) unsigned NOT NULL,
  `app_id` int(11) unsigned NOT NULL,
  `system_code` varchar(25) NOT NULL,
  `parent_module_id` int(11) unsigned DEFAULT NULL,
  `module_name` varchar(100) NOT NULL,
  `module_location` int(11) unsigned NOT NULL,
  `sort_order` float(2,1) unsigned NOT NULL,
  `enabled_flag` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`module_id`, `app_id`, `system_code`, `parent_module_id`, `module_name`, `module_location`, `sort_order`, `enabled_flag`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(1, 1, 'SYSAD', NULL, 'Dashboard', 1, 1.0, 1, 1, '2016-03-11 00:00:00', NULL, NULL),
(2, 1, 'SYSAD', NULL, 'Settings', 8, 1.0, 1, 1, '2016-03-11 00:00:00', NULL, NULL),
(3, 1, 'SYSAD', NULL, 'User Management', 1, 2.0, 1, 1, '2016-03-11 00:00:00', NULL, NULL),
(4, 1, 'SYSAD', 3, 'Users', 1, 2.1, 1, 1, '2016-03-11 00:00:00', NULL, '2016-03-15 09:58:14'),
(5, 1, 'SYSAD', 3, 'Role', 1, 2.2, 1, 1, '2016-03-11 00:00:00', NULL, '2016-03-15 09:57:16'),
(6, 1, 'SYSAD', 3, 'Permissions', 1, 2.3, 1, 1, '2016-03-11 00:00:00', NULL, '2016-03-15 09:57:27'),
(7, 1, 'SYSAD', 3, 'Organizations', 1, 2.4, 1, 1, '2016-03-11 00:00:00', NULL, '2016-03-15 09:57:32'),
(8, 1, 'SYSAD', NULL, 'Files', 1, 3.0, 1, 1, '2016-03-11 00:00:00', NULL, NULL),
(9, 1, 'SYSAD', NULL, 'Workflow', 1, 4.0, 1, 1, '2016-03-11 00:00:00', NULL, NULL),
(10, 1, 'SYSAD', NULL, 'Audit Trail', 1, 5.0, 1, 1, '2016-03-11 00:00:00', NULL, NULL),
(11, 1, 'SYSAD', 2, 'Site Settings', 7, 1.1, 1, 1, '2016-03-11 00:00:00', NULL, '2016-03-15 09:58:26'),
(12, 1, 'SYSAD', 2, 'Authentication Settings', 7, 1.2, 1, 1, '2016-03-11 00:00:00', NULL, '2016-03-15 09:58:32');

-- --------------------------------------------------------

--
-- Table structure for table `module_actions`
--

CREATE TABLE IF NOT EXISTS `module_actions` (
  `module_action_id` int(11) unsigned NOT NULL,
  `module_id` int(11) unsigned NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `action_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module_actions`
--

INSERT INTO `module_actions` (`module_action_id`, `module_id`, `created_by`, `created_date`, `modified_by`, `modified_date`, `action_id`) VALUES
(1, 1, 1, '2016-03-14 10:52:50', NULL, NULL, 5),
(2, 2, 1, '2016-03-14 10:52:50', NULL, NULL, 5),
(3, 3, 1, '2016-03-14 10:52:50', NULL, NULL, 5),
(4, 4, 1, '2016-03-14 10:52:50', NULL, NULL, 5),
(5, 5, 1, '2016-03-14 10:52:50', NULL, NULL, 5),
(6, 6, 1, '2016-03-14 10:52:50', NULL, NULL, 5),
(7, 7, 1, '2016-03-14 10:52:50', NULL, NULL, 5),
(8, 8, 1, '2016-03-14 10:52:50', NULL, NULL, 5),
(9, 9, 1, '2016-03-14 10:52:50', NULL, NULL, 5),
(10, 10, 1, '2016-03-14 10:52:50', NULL, NULL, 5),
(11, 11, 1, '2016-03-14 10:52:50', NULL, NULL, 5),
(12, 12, 1, '2016-03-14 10:52:50', NULL, NULL, 5),
(16, 4, 1, '2016-03-14 11:02:44', NULL, NULL, 1),
(17, 4, 1, '2016-03-14 11:02:44', NULL, NULL, 2),
(18, 4, 1, '2016-03-14 11:02:44', NULL, NULL, 3),
(19, 4, 1, '2016-03-14 11:02:44', NULL, NULL, 4),
(20, 5, 1, '2016-03-14 11:02:44', NULL, NULL, 1),
(21, 5, 1, '2016-03-14 11:02:44', NULL, NULL, 2),
(22, 5, 1, '2016-03-14 11:02:44', NULL, NULL, 3),
(23, 5, 1, '2016-03-14 11:02:44', NULL, NULL, 4),
(24, 6, 1, '2016-03-14 11:02:44', NULL, NULL, 1),
(25, 7, 1, '2016-03-14 11:02:44', NULL, NULL, 1),
(26, 7, 1, '2016-03-14 11:02:44', NULL, NULL, 2),
(27, 7, 1, '2016-03-14 11:02:44', NULL, NULL, 3),
(28, 7, 1, '2016-03-14 11:02:44', NULL, NULL, 4),
(29, 8, 1, '2016-03-14 11:02:44', NULL, NULL, 1),
(30, 8, 1, '2016-03-14 11:02:44', NULL, NULL, 2),
(31, 8, 1, '2016-03-14 11:02:44', NULL, NULL, 3),
(32, 8, 1, '2016-03-14 11:02:44', NULL, NULL, 4),
(33, 9, 1, '2016-03-14 11:02:44', NULL, NULL, 1),
(34, 9, 1, '2016-03-14 11:02:44', NULL, NULL, 2),
(35, 9, 1, '2016-03-14 11:02:44', NULL, NULL, 3),
(36, 9, 1, '2016-03-14 11:02:44', NULL, NULL, 4),
(37, 11, 1, '2016-03-14 11:02:44', NULL, NULL, 1),
(38, 11, 1, '2016-03-14 11:02:44', NULL, NULL, 8),
(39, 12, 1, '2016-03-14 11:02:44', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `module_action_roles`
--

CREATE TABLE IF NOT EXISTS `module_action_roles` (
  `module_action_id` int(11) unsigned NOT NULL,
  `role_code` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `module_action_roles`
--

INSERT INTO `module_action_roles` (`module_action_id`, `role_code`) VALUES
(1, 'ADMINISTRATOR'),
(2, 'ADMINISTRATOR'),
(3, 'ADMINISTRATOR'),
(4, 'ADMINISTRATOR'),
(5, 'ADMINISTRATOR'),
(6, 'ADMINISTRATOR'),
(7, 'ADMINISTRATOR'),
(8, 'ADMINISTRATOR'),
(9, 'ADMINISTRATOR'),
(10, 'ADMINISTRATOR'),
(11, 'ADMINISTRATOR'),
(12, 'ADMINISTRATOR'),
(16, 'ADMINISTRATOR'),
(17, 'ADMINISTRATOR'),
(25, 'ADMINISTRATOR'),
(26, 'ADMINISTRATOR'),
(29, 'ADMINISTRATOR'),
(30, 'ADMINISTRATOR'),
(33, 'ADMINISTRATOR'),
(34, 'ADMINISTRATOR');

-- --------------------------------------------------------

--
-- Table structure for table `module_scopes`
--

CREATE TABLE IF NOT EXISTS `module_scopes` (
  `module_id` int(11) unsigned NOT NULL COMMENT 'Refers to module',
  `scope_id` int(10) unsigned NOT NULL COMMENT 'Refers to type of scope'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains of applicable scopes per module';

--
-- Dumping data for table `module_scopes`
--

INSERT INTO `module_scopes` (`module_id`, `scope_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(1, 2),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(9, 2),
(10, 2),
(11, 2),
(12, 2),
(1, 3),
(2, 3),
(3, 3),
(4, 3),
(5, 3),
(6, 3),
(7, 3),
(8, 3),
(9, 3),
(10, 3),
(11, 3),
(12, 3);

-- --------------------------------------------------------

--
-- Table structure for table `module_scope_roles`
--

CREATE TABLE IF NOT EXISTS `module_scope_roles` (
  `module_id` int(11) unsigned NOT NULL COMMENT 'Refers to module',
  `role_code` varchar(25) NOT NULL COMMENT 'Refers to role',
  `scope_id` int(10) unsigned NOT NULL COMMENT 'Refers to scope'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains of all modules and scope assigned to role';

--
-- Dumping data for table `module_scope_roles`
--

INSERT INTO `module_scope_roles` (`module_id`, `role_code`, `scope_id`) VALUES
(1, 'ADMINISTRATOR', 2),
(2, 'ADMINISTRATOR', 2),
(3, 'ADMINISTRATOR', 2),
(4, 'ADMINISTRATOR', 2),
(5, 'ADMINISTRATOR', 2),
(6, 'ADMINISTRATOR', 2),
(7, 'ADMINISTRATOR', 2),
(8, 'ADMINISTRATOR', 2),
(9, 'ADMINISTRATOR', 2),
(10, 'ADMINISTRATOR', 2),
(11, 'ADMINISTRATOR', 2),
(12, 'ADMINISTRATOR', 2);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `notification_id` int(11) unsigned NOT NULL,
  `notification` text NOT NULL,
  `notify_users` text,
  `notify_orgs` text,
  `notify_roles` text,
  `notified_by` int(11) unsigned NOT NULL,
  `notification_date` datetime NOT NULL,
  `read_date` datetime DEFAULT NULL,
  `module_id` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE IF NOT EXISTS `organizations` (
  `org_code` char(14) NOT NULL,
  `org_parent` char(14) DEFAULT NULL,
  `location_code` char(12) NOT NULL,
  `short_name` varchar(50) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `website` varchar(100) DEFAULT NULL COMMENT 'This field contains multiple values. Use | character to seperate the values',
  `email` varchar(100) DEFAULT NULL,
  `phone` varchar(100) DEFAULT NULL COMMENT 'This field contains multiple values. Use | character to seperate the values',
  `fax` varchar(100) DEFAULT NULL COMMENT 'This field contains multiple values. Use | character to seperate the values',
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`org_code`, `org_parent`, `location_code`, `short_name`, `name`, `website`, `email`, `phone`, `fax`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
('1', NULL, '04-34', 'ANI', 'Asiagate Networks, Inc.', 'www.asiagate.com', 'inquiries@asiagate.com', '7524971', '7524971', 1, '2015-05-11 00:00:00', 1, '2016-03-15 07:59:50');

-- --------------------------------------------------------

--
-- Table structure for table `param_genders`
--

CREATE TABLE IF NOT EXISTS `param_genders` (
  `gender_code` char(1) NOT NULL COMMENT 'Standard code for this table is single character (ex: M , F)',
  `gender` varchar(10) NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `param_genders`
--

INSERT INTO `param_genders` (`gender_code`, `gender`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
('F', 'Female', 1, '2015-05-11 00:00:00', NULL, NULL),
('M', 'Male', 1, '2015-05-11 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `param_municities`
--

CREATE TABLE IF NOT EXISTS `param_municities` (
  `municity_code` varchar(50) NOT NULL COMMENT 'Primary key',
  `province_code` varchar(50) NOT NULL COMMENT 'Refers to province',
  `municity_name` varchar(100) NOT NULL COMMENT 'Name of city/municipality'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains the list of Philippine municipalities and cities';

-- --------------------------------------------------------

--
-- Table structure for table `param_offices`
--

CREATE TABLE IF NOT EXISTS `param_offices` (
  `office_id` int(11) unsigned NOT NULL COMMENT 'Primary key',
  `parent_office_id` int(11) unsigned DEFAULT NULL COMMENT 'Refers to parent/mother office',
  `hlurb_region_code` varchar(10) NOT NULL COMMENT 'Refers to HLURB Regional Office',
  `name` varchar(100) NOT NULL COMMENT 'Name of the office',
  `short_name` varchar(100) DEFAULT NULL COMMENT 'Short name of the office',
  `active_flag` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Tagging if the office is active',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to the last user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `param_provinces`
--

CREATE TABLE IF NOT EXISTS `param_provinces` (
  `province_code` varchar(50) NOT NULL COMMENT 'Primary key',
  `region_code` varchar(50) NOT NULL COMMENT 'Refers to region',
  `province_name` varchar(100) NOT NULL COMMENT 'Name of province'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains the list of Philippine provinces';

-- --------------------------------------------------------

--
-- Table structure for table `param_regions`
--

CREATE TABLE IF NOT EXISTS `param_regions` (
  `region_code` varchar(50) NOT NULL COMMENT 'Primary key',
  `hlurb_region_code` varchar(10) DEFAULT NULL COMMENT 'Refers to HLURB Central Office/Regional Office',
  `region_name` varchar(100) NOT NULL COMMENT 'Name of region',
  `region_short_name` varchar(100) NOT NULL COMMENT 'Short name of region',
  `sort_order` tinyint(2) unsigned NOT NULL DEFAULT '1' COMMENT 'Ordering of record'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains the list of Philippine regions';

-- --------------------------------------------------------

--
-- Table structure for table `param_scopes`
--

CREATE TABLE IF NOT EXISTS `param_scopes` (
  `scope_id` int(10) unsigned NOT NULL COMMENT 'Primary key of scope',
  `scope` varchar(100) NOT NULL COMMENT 'Name of scope; System-wide; Regional-wide; Own-office'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Contains of all types of scope';

--
-- Dumping data for table `param_scopes`
--

INSERT INTO `param_scopes` (`scope_id`, `scope`) VALUES
(1, 'System Wide'),
(2, 'Region Wide'),
(3, 'Own Agency');

-- --------------------------------------------------------

--
-- Table structure for table `param_status`
--

CREATE TABLE IF NOT EXISTS `param_status` (
  `status_id` int(11) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `param_status`
--

INSERT INTO `param_status` (`status_id`, `status`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(1, 'Active', 1, '2015-05-12 00:00:00', NULL, NULL),
(2, 'Pending', 1, '2015-05-27 00:00:00', NULL, NULL),
(3, 'Inactive', 1, '2015-05-28 00:00:00', NULL, NULL),
(4, 'Approved', 1, '2015-06-24 19:33:00', NULL, '2015-07-09 06:50:25'),
(5, 'Disapproved', 1, '2015-06-24 19:33:00', NULL, NULL),
(6, 'Deleted', 1, '2015-07-03 16:30:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `param_user_status`
--

CREATE TABLE IF NOT EXISTS `param_user_status` (
  `status_id` int(11) unsigned NOT NULL COMMENT 'Primary key',
  `status` varchar(50) NOT NULL COMMENT 'Status of user',
  `created_by` int(11) unsigned NOT NULL COMMENT 'Refers to user who created or encoded the record',
  `created_date` datetime NOT NULL COMMENT 'Refers to date when the record was encoded or created by the user',
  `modified_by` int(11) unsigned DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Refers to date when the record was edited by the user'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains the list of status of user';

-- --------------------------------------------------------

--
-- Table structure for table `process`
--

CREATE TABLE IF NOT EXISTS `process` (
  `process_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `num_stages` tinyint(2) unsigned NOT NULL,
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `process_actions`
--

CREATE TABLE IF NOT EXISTS `process_actions` (
  `process_action_id` int(11) unsigned NOT NULL,
  `process_step_id` int(11) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `proceeding_step` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `process_stages`
--

CREATE TABLE IF NOT EXISTS `process_stages` (
  `process_stage_id` int(11) unsigned NOT NULL,
  `process_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `process_stage_roles`
--

CREATE TABLE IF NOT EXISTS `process_stage_roles` (
  `process_stage_id` int(11) unsigned NOT NULL,
  `role_code` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `process_steps`
--

CREATE TABLE IF NOT EXISTS `process_steps` (
  `process_step_id` int(11) unsigned NOT NULL,
  `process_stage_id` int(11) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `role_code` varchar(25) NOT NULL,
  `role_name` varchar(100) NOT NULL,
  `built_in_flag` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `created_by` int(11) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`role_code`, `role_name`, `built_in_flag`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
('ADMINISTRATOR', 'Administrator', 1, 1, '2016-03-14 00:00:00', NULL, NULL),
('AUTHENTICATED_USER', 'Authenticated User', 1, 1, '2016-03-14 00:00:00', NULL, NULL),
('SUPER_ADMIN', 'Super Admin', 1, 1, '2016-03-14 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `process_stage_id` int(11) unsigned NOT NULL COMMENT 'Primary key',
  `name` varchar(255) NOT NULL COMMENT 'Name of service',
  `classification` int(11) unsigned NOT NULL COMMENT 'Classification of service',
  `task_count` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Total no. of tasks',
  `case_type_id` int(11) unsigned DEFAULT NULL COMMENT 'Tagging if the service is REM, HOA, Inhibit. THis field is applicable to Case and Appeal classification',
  `for_conciliation_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Tagging if the request is for conciliation. This field is applicable only to  REM/HOA monitoring classification',
  `limit_flag` tinyint(1) NOT NULL DEFAULT '0',
  `appendable_flag` tinyint(1) NOT NULL DEFAULT '0',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created or encoded the record',
  `created_date` datetime NOT NULL COMMENT 'Refers to date when the record was encoded or created by the user',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Refers to date when the record was edited by the user'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service_tabs`
--

CREATE TABLE IF NOT EXISTS `service_tabs` (
  `service_id` int(11) unsigned NOT NULL,
  `tab_num` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `service_tasks`
--

CREATE TABLE IF NOT EXISTS `service_tasks` (
  `process_step_id` int(11) unsigned NOT NULL COMMENT 'Primary key of service task',
  `process_stage_id` int(11) unsigned NOT NULL COMMENT 'Refers to service',
  `sys_sequence_no` int(11) NOT NULL COMMENT 'System generated',
  `sequence` int(11) unsigned NOT NULL COMMENT 'User input; sequence of task',
  `name` varchar(255) NOT NULL COMMENT 'Name of task',
  `role_code` varchar(25) NOT NULL COMMENT 'Refers to assigned role',
  `turn_around_time` decimal(6,2) unsigned NOT NULL COMMENT 'Turnaround time to process the task',
  `allow_get_task_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Tagging if the task is allow to get directly by user',
  `final_task_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Tagigng if the task needs to validate all required fields of the service request',
  `gen_docket_num_flag` tinyint(1) NOT NULL DEFAULT '1',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created or encoded the record',
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Refers to date when the record was edited by the user'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains of all tasks per task';

-- --------------------------------------------------------

--
-- Table structure for table `service_task_actions`
--

CREATE TABLE IF NOT EXISTS `service_task_actions` (
  `process_action_id` int(10) unsigned NOT NULL COMMENT 'Primary key of task action',
  `process_step_id` int(11) unsigned NOT NULL COMMENT 'Refers to service task',
  `action_id` int(11) NOT NULL COMMENT 'Refers to action',
  `proceeding_step` int(11) unsigned DEFAULT NULL COMMENT 'Refers to next task once the task was tagged as completed',
  `created_by` int(11) unsigned NOT NULL COMMENT 'Refers to user who created or encoded the record',
  `created_date` datetime NOT NULL COMMENT 'Refers to date when the record was encoded or created by the user',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT 'Refers to date when the record was edited by the user'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains of all actions of service''s task';

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE IF NOT EXISTS `session` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL DEFAULT '',
  `last_activity` bigint(20) unsigned NOT NULL DEFAULT '0',
  `user_data` longtext
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table stores the session per user';

-- --------------------------------------------------------

--
-- Table structure for table `site_settings`
--

CREATE TABLE IF NOT EXISTS `site_settings` (
  `setting_location` varchar(100) NOT NULL,
  `setting_type` varchar(45) NOT NULL,
  `setting_name` varchar(100) NOT NULL,
  `setting_value` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `site_settings`
--

INSERT INTO `site_settings` (`setting_location`, `setting_type`, `setting_name`, `setting_value`) VALUES
('AUTHENTICATION', 'ACCOUNT', 'account_creator', 'VISITOR'),
('SITE_APPEARANCE', 'GENERAL', 'system_description', ''),
('SITE_APPEARANCE', 'GENERAL', 'system_email', 'info@asiagate.com'),
('SITE_APPEARANCE', 'GENERAL', 'system_favicon', 'favicon_145319.ico'),
('SITE_APPEARANCE', 'GENERAL', 'system_logo', 'logowhite_145247.png'),
('SITE_APPEARANCE', 'GENERAL', 'system_tagline', ''),
('SITE_APPEARANCE', 'GENERAL', 'system_title', 'eBudget'),
('SITE_APPEARANCE', 'LAYOUT', 'header', 'default'),
('SITE_APPEARANCE', 'LAYOUT', 'sidebar_menu', 'slide-nav'),
('AUTHENTICATION', 'LOGIN', 'login_attempts', '8'),
('AUTHENTICATION', 'LOGIN', 'login_via', 'USERNAME_EMAIL'),
('AUTHENTICATION', 'PASSWORD_CONSTRAINTS', 'constraint_digit', '1'),
('AUTHENTICATION', 'PASSWORD_CONSTRAINTS', 'constraint_history', '2'),
('AUTHENTICATION', 'PASSWORD_CONSTRAINTS', 'constraint_length', '10'),
('AUTHENTICATION', 'PASSWORD_CONSTRAINTS', 'constraint_uppercase', '3'),
('AUTHENTICATION', 'PASSWORD_EXPIRY', 'password_duration', ''),
('AUTHENTICATION', 'PASSWORD_EXPIRY', 'password_expiry', '0'),
('AUTHENTICATION', 'PASSWORD_EXPIRY', 'password_reminder', ''),
('SITE_APPEARANCE', 'THEME', 'skins', 'default');

-- --------------------------------------------------------

--
-- Table structure for table `systems`
--

CREATE TABLE IF NOT EXISTS `systems` (
  `system_code` varchar(25) NOT NULL,
  `system_name` varchar(100) NOT NULL,
  `on_off_flag` tinyint(1) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `systems`
--

INSERT INTO `systems` (`system_code`, `system_name`, `on_off_flag`) VALUES
('SYSAD', 'System Administration', 1);

-- --------------------------------------------------------

--
-- Table structure for table `system_roles`
--

CREATE TABLE IF NOT EXISTS `system_roles` (
  `system_code` varchar(25) NOT NULL,
  `role_code` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_roles`
--

INSERT INTO `system_roles` (`system_code`, `role_code`) VALUES
('SYSAD', 'SUPER_ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `sys_param`
--

CREATE TABLE IF NOT EXISTS `sys_param` (
  `sys_param_id` int(11) unsigned NOT NULL,
  `sys_param_type` varchar(45) NOT NULL,
  `sys_param_name` varchar(100) NOT NULL,
  `sys_param_value` varchar(45) NOT NULL,
  `built_in_flag` tinyint(1) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sys_param`
--

INSERT INTO `sys_param` (`sys_param_id`, `sys_param_type`, `sys_param_name`, `sys_param_value`, `built_in_flag`) VALUES
(1, 'MODULE_LOCATION', 'Left', 'LEFT', 1),
(2, 'SMTP', 'PROTOCOL', 'smtp', 1),
(3, 'SMTP', 'SMTP_HOST', 'ssl://smtp.gmail.com', 1),
(4, 'SMTP', 'SMTP_PORT', '465', 1),
(5, 'SMTP', 'SMTP_USER', 'kebwaitforit@gmail.com', 1),
(6, 'SMTP', 'SMTP_PASS', 'technofreeze', 1),
(7, 'MODULE_LOCATION', 'On Page', 'ON_PAGE', 1),
(8, 'MODULE_LOCATION', 'Top Bar', 'TOP_BAR', 1);

-- --------------------------------------------------------

--
-- Table structure for table `to_dos`
--

CREATE TABLE IF NOT EXISTS `to_dos` (
  `to_do_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `username` varchar(100) DEFAULT NULL,
  `role_code` varchar(25) DEFAULT NULL,
  `to_do` varchar(255) NOT NULL,
  `to_do_date` datetime NOT NULL,
  `read_date` datetime DEFAULT NULL,
  `completed_date` datetime DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) unsigned NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `location_code` char(12) NOT NULL,
  `org_code` char(14) NOT NULL,
  `password` varchar(128) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `raw_password` text CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `salt` varchar(128) NOT NULL,
  `reset_salt` varchar(128) DEFAULT NULL,
  `logged_in_flag` tinyint(1) NOT NULL DEFAULT '0',
  `fname` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `lname` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `mname` varchar(100) DEFAULT NULL,
  `nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `gender` char(1) NOT NULL,
  `email` varchar(100) NOT NULL,
  `job_title` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `contact_no` varchar(50) DEFAULT NULL,
  `mobile_no` varchar(50) DEFAULT NULL,
  `mail_flag` tinyint(1) NOT NULL DEFAULT '0',
  `status_id` int(11) unsigned NOT NULL,
  `reason` text,
  `created_by` int(11) unsigned DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `username`, `location_code`, `org_code`, `password`, `raw_password`, `salt`, `reset_salt`, `logged_in_flag`, `fname`, `lname`, `mname`, `nickname`, `gender`, `email`, `job_title`, `photo`, `contact_no`, `mobile_no`, `mail_flag`, `status_id`, `reason`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(0, 'anonymous', '', '', 'anonymous', '', 'anonymous', NULL, 0, 'anonymous', 'anonymous', 'anonymous', NULL, 'M', 'anonymous', NULL, NULL, NULL, NULL, 0, 1, NULL, NULL, '2015-05-27 00:00:00', NULL, NULL),
(1, 'administrator', '04-34', '1', '9ed97a430da568d13e25493d70cc801c452f2b767448ee492a6b3d685381b1fabf62c93f6efd61feed567aa2e300c146cc08767fb8f4ce21e82414faeb056350', 'f4d267bbee3399816600ebaa9d57e98d2f608218ef2fd6a8aec8ffcd68a74902dde1ff6fa861d702fc402dd0989deed2123393f39ceef97c2f6c853fc3eba7fbwit0DYoU.HLeE0B.ptAW.g26d4ybwshvZXM9HsUTEsc-', '56ed5b6087ce3efa61fc745692dd6536a7d49159eeb12712adea0723656aa6e5d6ba13540779e542f0a7fae7341941937717570722f30c109a6e0d744b71a413', '', 0, 'Juan', 'dela Cruz', 'A', 'Juan', 'M', 'juandelacruz@gmail.com', 'System Administrator', 'avatar002_181106.jpg', '785-3948', '09158792347', 0, 1, NULL, NULL, '2015-05-11 00:00:00', 1, '2016-03-14 10:11:35');

-- --------------------------------------------------------

--
-- Table structure for table `user_offices`
--

CREATE TABLE IF NOT EXISTS `user_offices` (
  `office_id` int(11) unsigned NOT NULL COMMENT 'Refers to office',
  `user_id` int(11) unsigned NOT NULL COMMENT 'Refers to user',
  `main_office_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Tagging if the office is the main office of the user'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contains the office/unit/division of user';

-- --------------------------------------------------------

--
-- Table structure for table `user_roles`
--

CREATE TABLE IF NOT EXISTS `user_roles` (
  `user_id` int(11) unsigned NOT NULL,
  `role_code` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_roles`
--

INSERT INTO `user_roles` (`user_id`, `role_code`) VALUES
(1, 'SUPER_ADMIN');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`action_id`);

--
-- Indexes for table `activities`
--
ALTER TABLE `activities`
  ADD PRIMARY KEY (`activity_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `apps`
--
ALTER TABLE `apps`
  ADD PRIMARY KEY (`app_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `app_roles`
--
ALTER TABLE `app_roles`
  ADD PRIMARY KEY (`app_id`,`role_code`),
  ADD KEY `role_code` (`role_code`);

--
-- Indexes for table `audit_trail`
--
ALTER TABLE `audit_trail`
  ADD PRIMARY KEY (`audit_trail_id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `audit_trail_detail`
--
ALTER TABLE `audit_trail_detail`
  ADD PRIMARY KEY (`audit_trail_detail_id`),
  ADD KEY `audit_trail_id` (`audit_trail_id`);

--
-- Indexes for table `file`
--
ALTER TABLE `file`
  ADD PRIMARY KEY (`file_id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `file_versions`
--
ALTER TABLE `file_versions`
  ADD PRIMARY KEY (`file_version_id`),
  ADD KEY `file_id` (`file_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`location_code`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`module_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`),
  ADD KEY `app_id` (`app_id`),
  ADD KEY `parent_module_id` (`parent_module_id`),
  ADD KEY `module_location` (`module_location`),
  ADD KEY `system_code` (`system_code`);

--
-- Indexes for table `module_actions`
--
ALTER TABLE `module_actions`
  ADD PRIMARY KEY (`module_action_id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`),
  ADD KEY `action_id` (`action_id`);

--
-- Indexes for table `module_action_roles`
--
ALTER TABLE `module_action_roles`
  ADD PRIMARY KEY (`module_action_id`,`role_code`),
  ADD KEY `role_code` (`role_code`);

--
-- Indexes for table `module_scopes`
--
ALTER TABLE `module_scopes`
  ADD KEY `fk_module_scopes_param_scope1` (`scope_id`),
  ADD KEY `fk_module_scopes_modules1` (`module_id`);

--
-- Indexes for table `module_scope_roles`
--
ALTER TABLE `module_scope_roles`
  ADD PRIMARY KEY (`module_id`,`role_code`,`scope_id`),
  ADD KEY `fk_module_scope_roles_modules1` (`module_id`),
  ADD KEY `fk_module_scope_roles_roles1` (`role_code`),
  ADD KEY `fk_module_scope_roles_param_scope1` (`scope_id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notification_id`),
  ADD KEY `notified_by` (`notified_by`),
  ADD KEY `module_id` (`module_id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`org_code`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `param_genders`
--
ALTER TABLE `param_genders`
  ADD PRIMARY KEY (`gender_code`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `param_municities`
--
ALTER TABLE `param_municities`
  ADD PRIMARY KEY (`municity_code`),
  ADD KEY `fk_par_prov_prov_code_idx` (`province_code`);

--
-- Indexes for table `param_offices`
--
ALTER TABLE `param_offices`
  ADD PRIMARY KEY (`office_id`),
  ADD KEY `fk_param_offices_param_hlurb_regions1` (`hlurb_region_code`),
  ADD KEY `fk_param_offices_param_offices1` (`parent_office_id`);

--
-- Indexes for table `param_provinces`
--
ALTER TABLE `param_provinces`
  ADD PRIMARY KEY (`province_code`),
  ADD KEY `fk_par_prov_reg_code_idx` (`region_code`);

--
-- Indexes for table `param_regions`
--
ALTER TABLE `param_regions`
  ADD PRIMARY KEY (`region_code`),
  ADD KEY `fk_par_reg_h_reg_code_idx` (`hlurb_region_code`);

--
-- Indexes for table `param_scopes`
--
ALTER TABLE `param_scopes`
  ADD PRIMARY KEY (`scope_id`);

--
-- Indexes for table `param_status`
--
ALTER TABLE `param_status`
  ADD PRIMARY KEY (`status_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `param_user_status`
--
ALTER TABLE `param_user_status`
  ADD PRIMARY KEY (`status_id`);

--
-- Indexes for table `process`
--
ALTER TABLE `process`
  ADD PRIMARY KEY (`process_id`);

--
-- Indexes for table `process_actions`
--
ALTER TABLE `process_actions`
  ADD PRIMARY KEY (`process_action_id`),
  ADD KEY `process_step_id` (`process_step_id`),
  ADD KEY `proceeding_step` (`proceeding_step`);

--
-- Indexes for table `process_stages`
--
ALTER TABLE `process_stages`
  ADD PRIMARY KEY (`process_stage_id`),
  ADD KEY `process_id` (`process_id`);

--
-- Indexes for table `process_stage_roles`
--
ALTER TABLE `process_stage_roles`
  ADD PRIMARY KEY (`process_stage_id`,`role_code`),
  ADD KEY `role_code` (`role_code`);

--
-- Indexes for table `process_steps`
--
ALTER TABLE `process_steps`
  ADD PRIMARY KEY (`process_step_id`),
  ADD KEY `process_stage_id` (`process_stage_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`role_code`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`process_stage_id`),
  ADD KEY `fk_services_sys_param1_idx` (`classification`);

--
-- Indexes for table `service_tabs`
--
ALTER TABLE `service_tabs`
  ADD PRIMARY KEY (`service_id`,`tab_num`);

--
-- Indexes for table `service_tasks`
--
ALTER TABLE `service_tasks`
  ADD PRIMARY KEY (`process_step_id`),
  ADD KEY `process_stage_id` (`process_stage_id`),
  ADD KEY `fk_process_steps_roles` (`role_code`);

--
-- Indexes for table `service_task_actions`
--
ALTER TABLE `service_task_actions`
  ADD PRIMARY KEY (`process_action_id`),
  ADD KEY `fk_task_action_id` (`action_id`),
  ADD KEY `fk_service_task_actions_service_tasks1_idx` (`process_step_id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`session_id`);

--
-- Indexes for table `site_settings`
--
ALTER TABLE `site_settings`
  ADD PRIMARY KEY (`setting_type`,`setting_name`);

--
-- Indexes for table `systems`
--
ALTER TABLE `systems`
  ADD PRIMARY KEY (`system_code`);

--
-- Indexes for table `system_roles`
--
ALTER TABLE `system_roles`
  ADD PRIMARY KEY (`system_code`,`role_code`),
  ADD KEY `role_code` (`role_code`);

--
-- Indexes for table `sys_param`
--
ALTER TABLE `sys_param`
  ADD PRIMARY KEY (`sys_param_id`);

--
-- Indexes for table `to_dos`
--
ALTER TABLE `to_dos`
  ADD PRIMARY KEY (`to_do_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `gender` (`gender`),
  ADD KEY `status_id` (`status_id`);

--
-- Indexes for table `user_offices`
--
ALTER TABLE `user_offices`
  ADD PRIMARY KEY (`office_id`,`user_id`),
  ADD KEY `fk_user_offices_param_offices1_idx` (`office_id`),
  ADD KEY `fk_user_offices_users1_idx` (`user_id`);

--
-- Indexes for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD PRIMARY KEY (`user_id`,`role_code`),
  ADD KEY `role_code` (`role_code`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `actions`
--
ALTER TABLE `actions`
  MODIFY `action_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `activities`
--
ALTER TABLE `activities`
  MODIFY `activity_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `apps`
--
ALTER TABLE `apps`
  MODIFY `app_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `audit_trail`
--
ALTER TABLE `audit_trail`
  MODIFY `audit_trail_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `audit_trail_detail`
--
ALTER TABLE `audit_trail_detail`
  MODIFY `audit_trail_detail_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `file`
--
ALTER TABLE `file`
  MODIFY `file_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `file_versions`
--
ALTER TABLE `file_versions`
  MODIFY `file_version_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `module_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `module_actions`
--
ALTER TABLE `module_actions`
  MODIFY `module_action_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `notification_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `param_offices`
--
ALTER TABLE `param_offices`
  MODIFY `office_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key';
--
-- AUTO_INCREMENT for table `param_scopes`
--
ALTER TABLE `param_scopes`
  MODIFY `scope_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key of scope',AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `param_status`
--
ALTER TABLE `param_status`
  MODIFY `status_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `process`
--
ALTER TABLE `process`
  MODIFY `process_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `process_actions`
--
ALTER TABLE `process_actions`
  MODIFY `process_action_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `process_stages`
--
ALTER TABLE `process_stages`
  MODIFY `process_stage_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `process_steps`
--
ALTER TABLE `process_steps`
  MODIFY `process_step_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `process_stage_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key';
--
-- AUTO_INCREMENT for table `service_tasks`
--
ALTER TABLE `service_tasks`
  MODIFY `process_step_id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key of service task';
--
-- AUTO_INCREMENT for table `service_task_actions`
--
ALTER TABLE `service_task_actions`
  MODIFY `process_action_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key of task action';
--
-- AUTO_INCREMENT for table `sys_param`
--
ALTER TABLE `sys_param`
  MODIFY `sys_param_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `to_dos`
--
ALTER TABLE `to_dos`
  MODIFY `to_do_id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `activities`
--
ALTER TABLE `activities`
  ADD CONSTRAINT `activities_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `activities_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `apps`
--
ALTER TABLE `apps`
  ADD CONSTRAINT `apps_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `apps_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `app_roles`
--
ALTER TABLE `app_roles`
  ADD CONSTRAINT `app_roles_ibfk_2` FOREIGN KEY (`role_code`) REFERENCES `roles` (`role_code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `app_roles_ibfk_3` FOREIGN KEY (`app_id`) REFERENCES `apps` (`app_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `audit_trail`
--
ALTER TABLE `audit_trail`
  ADD CONSTRAINT `audit_trail_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `audit_trail_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `audit_trail_detail`
--
ALTER TABLE `audit_trail_detail`
  ADD CONSTRAINT `audit_trail_detail_ibfk_1` FOREIGN KEY (`audit_trail_id`) REFERENCES `audit_trail` (`audit_trail_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `file`
--
ALTER TABLE `file`
  ADD CONSTRAINT `file_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `file_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `file_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `file_versions`
--
ALTER TABLE `file_versions`
  ADD CONSTRAINT `file_version_ibfk_1` FOREIGN KEY (`file_id`) REFERENCES `file` (`file_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `file_version_ibfk_2` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `file_version_ibfk_3` FOREIGN KEY (`modified_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `modules`
--
ALTER TABLE `modules`
  ADD CONSTRAINT `modules_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `modules_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `modules_ibfk_3` FOREIGN KEY (`app_id`) REFERENCES `apps` (`app_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `modules_ibfk_4` FOREIGN KEY (`parent_module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `modules_ibfk_5` FOREIGN KEY (`module_location`) REFERENCES `sys_param` (`sys_param_id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `modules_ibfk_6` FOREIGN KEY (`system_code`) REFERENCES `systems` (`system_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `module_actions`
--
ALTER TABLE `module_actions`
  ADD CONSTRAINT `module_actions_ibfk_4` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `module_actions_ibfk_5` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `module_actions_ibfk_6` FOREIGN KEY (`modified_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `module_actions_ibfk_7` FOREIGN KEY (`action_id`) REFERENCES `actions` (`action_id`);

--
-- Constraints for table `module_action_roles`
--
ALTER TABLE `module_action_roles`
  ADD CONSTRAINT `module_action_roles_ibfk_2` FOREIGN KEY (`role_code`) REFERENCES `roles` (`role_code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `module_action_roles_ibfk_3` FOREIGN KEY (`module_action_id`) REFERENCES `module_actions` (`module_action_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `module_scopes`
--
ALTER TABLE `module_scopes`
  ADD CONSTRAINT `fk_module_scopes_modules1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_module_scopes_param_scope1` FOREIGN KEY (`scope_id`) REFERENCES `param_scopes` (`scope_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `module_scope_roles`
--
ALTER TABLE `module_scope_roles`
  ADD CONSTRAINT `fk_module_scope_roles_modules1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_module_scope_roles_param_scope1` FOREIGN KEY (`scope_id`) REFERENCES `param_scopes` (`scope_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_module_scope_roles_roles1` FOREIGN KEY (`role_code`) REFERENCES `roles` (`role_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`notified_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `notifications_ibfk_2` FOREIGN KEY (`module_id`) REFERENCES `modules` (`module_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `organizations_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `param_genders`
--
ALTER TABLE `param_genders`
  ADD CONSTRAINT `param_genders_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `param_genders_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `param_municities`
--
ALTER TABLE `param_municities`
  ADD CONSTRAINT `fk_par_prov_prov_code` FOREIGN KEY (`province_code`) REFERENCES `param_provinces` (`province_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `param_offices`
--
ALTER TABLE `param_offices`
  ADD CONSTRAINT `fk_param_offices_param_offices1` FOREIGN KEY (`parent_office_id`) REFERENCES `param_offices` (`office_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `param_provinces`
--
ALTER TABLE `param_provinces`
  ADD CONSTRAINT `fk_par_prov_reg_code` FOREIGN KEY (`region_code`) REFERENCES `param_regions` (`region_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `param_status`
--
ALTER TABLE `param_status`
  ADD CONSTRAINT `param_status_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `param_status_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `process_actions`
--
ALTER TABLE `process_actions`
  ADD CONSTRAINT `process_actions_ibfk_1` FOREIGN KEY (`process_step_id`) REFERENCES `process_steps` (`process_step_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `process_actions_ibfk_2` FOREIGN KEY (`proceeding_step`) REFERENCES `process_steps` (`process_step_id`);

--
-- Constraints for table `process_stages`
--
ALTER TABLE `process_stages`
  ADD CONSTRAINT `process_stages_ibfk_1` FOREIGN KEY (`process_id`) REFERENCES `process` (`process_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `process_stage_roles`
--
ALTER TABLE `process_stage_roles`
  ADD CONSTRAINT `process_stage_roles_ibfk_1` FOREIGN KEY (`process_stage_id`) REFERENCES `process_stages` (`process_stage_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `process_stage_roles_ibfk_2` FOREIGN KEY (`role_code`) REFERENCES `roles` (`role_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `process_steps`
--
ALTER TABLE `process_steps`
  ADD CONSTRAINT `process_steps_ibfk_1` FOREIGN KEY (`process_stage_id`) REFERENCES `process_stages` (`process_stage_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
  ADD CONSTRAINT `roles_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `roles_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `fk_services_sys_param1` FOREIGN KEY (`classification`) REFERENCES `sys_param` (`sys_param_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_services_sys_param2` FOREIGN KEY (`classification`) REFERENCES `sys_param` (`sys_param_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `service_tabs`
--
ALTER TABLE `service_tabs`
  ADD CONSTRAINT `fk_service_tab_service_id` FOREIGN KEY (`service_id`) REFERENCES `services` (`process_stage_id`);

--
-- Constraints for table `service_tasks`
--
ALTER TABLE `service_tasks`
  ADD CONSTRAINT `fk_process_steps_roles` FOREIGN KEY (`role_code`) REFERENCES `roles` (`role_code`),
  ADD CONSTRAINT `service_tasks_ibfk_1` FOREIGN KEY (`process_stage_id`) REFERENCES `services` (`process_stage_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `service_task_actions`
--
ALTER TABLE `service_task_actions`
  ADD CONSTRAINT `fk_service_task_actions_service_tasks1` FOREIGN KEY (`process_step_id`) REFERENCES `service_tasks` (`process_step_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_task_action_id` FOREIGN KEY (`action_id`) REFERENCES `actions` (`action_id`);

--
-- Constraints for table `system_roles`
--
ALTER TABLE `system_roles`
  ADD CONSTRAINT `system_roles_ibfk_1` FOREIGN KEY (`system_code`) REFERENCES `systems` (`system_code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `system_roles_ibfk_2` FOREIGN KEY (`role_code`) REFERENCES `roles` (`role_code`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `to_dos`
--
ALTER TABLE `to_dos`
  ADD CONSTRAINT `to_dos_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`gender`) REFERENCES `param_genders` (`gender_code`) ON UPDATE CASCADE,
  ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`status_id`) REFERENCES `param_status` (`status_id`) ON UPDATE CASCADE;

--
-- Constraints for table `user_offices`
--
ALTER TABLE `user_offices`
  ADD CONSTRAINT `fk_user_offices_param_offices1` FOREIGN KEY (`office_id`) REFERENCES `param_offices` (`office_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_user_offices_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `user_roles`
--
ALTER TABLE `user_roles`
  ADD CONSTRAINT `user_roles_ibfk_2` FOREIGN KEY (`role_code`) REFERENCES `roles` (`role_code`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_roles_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE;



-- For User account history
-- Joela G. Tan April 01, 2016 03:55 PM
CREATE TABLE `user_history` (
  `user_id` mediumint(8) unsigned NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `location_code` char(12) NOT NULL,
  `org_code` char(14) NOT NULL,
  `password` varchar(128) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `raw_password` text CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `salt` varchar(128) NOT NULL,
  `reset_salt` varchar(128) DEFAULT NULL,
  `logged_in_flag` tinyint(1) NOT NULL DEFAULT '0',
  `fname` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `lname` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `mname` varchar(100) DEFAULT NULL,
  `nickname` varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `gender` char(1) NOT NULL,
  `email` varchar(100) NOT NULL,
  `job_title` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `contact_no` varchar(50) DEFAULT NULL,
  `mobile_no` varchar(50) DEFAULT NULL,
  `mail_flag` tinyint(1) NOT NULL DEFAULT '0',
  `status_id` tinyint(2) unsigned NOT NULL,
  `reason` text,
  `created_by` int(11) unsigned DEFAULT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;


DELIMITER $$
CREATE
	TRIGGER `user_backup` BEFORE UPDATE
	ON `users` 
	FOR EACH ROW BEGIN
    	
		INSERT INTO user_history select * from users where user_id = NEW.user_id;
		
    END$$
DELIMITER ;


INSERT INTO `sys_param` (`sys_param_type`, `sys_param_name`, `sys_param_value`, `built_in_flag`) VALUES ('SMTP', 'SMTP_REPLY_NAME', 'LGU 360 Administrator', '1');
INSERT INTO `sys_param` (`sys_param_type`, `sys_param_name`, `sys_param_value`, `built_in_flag`) VALUES ('SMTP', 'SMTP_REPLY_EMAIL', 'kebwaitforit@gmail.com', '1');


ALTER TABLE `core`.`users` 
ADD COLUMN `attempts` TINYINT(2) NULL DEFAULT 0 COMMENT '' AFTER `reason`;

DROP TRIGGER IF EXISTS `user_backup`;
DELIMITER $$
CREATE
	TRIGGER `user_backup` BEFORE UPDATE
	ON `users` 
	FOR EACH ROW BEGIN
    	IF old.attempts = new.attempts && old.status_id = new.status_id THEN
			INSERT INTO user_history select * from users where user_id = NEW.user_id;
		END IF;
    END$$
DELIMITER ;


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
