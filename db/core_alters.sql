--
-- Table structure for table `process`
--

DROP TABLE IF EXISTS `process`;
DROP TABLE IF EXISTS `process_actions`;
DROP TABLE IF EXISTS `process_stages`;
DROP TABLE IF EXISTS `process_stage_roles`;
DROP TABLE IF EXISTS `process_steps`;
DROP TABLE IF EXISTS `notifications`;

CREATE TABLE IF NOT EXISTS `process` (
  `process_id` mediumint(8) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `num_stages` tinyint(2) unsigned NOT NULL,
  `created_by` mediumint(8) unsigned NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` mediumint(8) unsigned DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;


--
-- Table structure for table `process_actions`
--

CREATE TABLE IF NOT EXISTS `process_actions` (
  `process_action_id` mediumint(8) unsigned NOT NULL,
  `process_id` mediumint(8) unsigned NOT NULL,
  `process_step_id` mediumint(8) unsigned NOT NULL,
  `name` varchar(50) NOT NULL,
  `proceeding_step` mediumint(8) unsigned DEFAULT NULL,
  `message` text NOT NULL,
  `is_return` tinyint(1) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `process_actions`
--
DELIMITER $$
CREATE TRIGGER `generate_action_id` BEFORE INSERT ON `process_actions`
 FOR EACH ROW BEGIN	
SET NEW.process_action_id = (SELECT IFNULL(MAX(process_action_id), 0) + 1 FROM process_actions WHERE process_id = NEW.process_id);
END
$$
DELIMITER ;


--
-- Table structure for table `process_stages`
--

CREATE TABLE IF NOT EXISTS `process_stages` (
  `process_stage_id` mediumint(8) unsigned NOT NULL,
  `process_id` mediumint(8) unsigned NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Triggers `process_stages`
--
DELIMITER $$
CREATE TRIGGER `generate_stage_id` BEFORE INSERT ON `process_stages`
 FOR EACH ROW BEGIN	
SET NEW.process_stage_id = (SELECT IFNULL(MAX(process_stage_id), 0) + 1 FROM process_stages WHERE process_id = NEW.process_id);
END
$$
DELIMITER ;

--
-- Table structure for table `process_stage_roles`
--

CREATE TABLE IF NOT EXISTS `process_stage_roles` (
  `process_id` mediumint(8) unsigned NOT NULL,
  `process_stage_id` mediumint(8) unsigned NOT NULL,
  `role_code` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `process_steps`
--

CREATE TABLE IF NOT EXISTS `process_steps` (
  `process_step_id` mediumint(8) unsigned NOT NULL,
  `process_id` mediumint(8) unsigned NOT NULL,
  `process_stage_id` mediumint(8) unsigned NOT NULL,
  `name` varchar(255) NOT NULL,
  `status_id` tinyint(2) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Triggers `process_steps`
--
DELIMITER $$
CREATE TRIGGER `generate_step_id` BEFORE INSERT ON `process_steps`
 FOR EACH ROW BEGIN	
SET NEW.process_step_id = (SELECT IFNULL(MAX(process_step_id), 0) + 1 FROM process_steps WHERE process_id = NEW.process_id);
END
$$
DELIMITER ;


--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `notification_id` int(11) unsigned NOT NULL,
  `notification` text NOT NULL,
  `notify_users` text,
  `notify_orgs` text,
  `notify_roles` text,
  `notified_by` mediumint(8) unsigned NOT NULL,
  `notification_date` datetime NOT NULL,
  `read_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1;

--
-- Indexes for table `process`
--
ALTER TABLE `process`
  ADD PRIMARY KEY (`process_id`),
  ADD KEY `created_by` (`created_by`),
  ADD KEY `modified_by` (`modified_by`);
  
ALTER TABLE `process`
  MODIFY `process_id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;  

--
-- Indexes for table `process_actions`
--
ALTER TABLE `process_actions`
  ADD PRIMARY KEY (`process_action_id`,`process_id`,`process_step_id`),
  ADD KEY `process_step_id` (`process_step_id`),
  ADD KEY `proceeding_step` (`proceeding_step`),
  ADD KEY `process_id` (`process_id`);

--
-- Indexes for table `process_stages`
--
ALTER TABLE `process_stages`
  ADD PRIMARY KEY (`process_stage_id`,`process_id`),
  ADD KEY `process_id` (`process_id`);

--
-- Indexes for table `process_stage_roles`
--
ALTER TABLE `process_stage_roles`
  ADD PRIMARY KEY (`process_id`,`process_stage_id`,`role_code`),
  ADD KEY `role_code` (`role_code`),
  ADD KEY `process_stage_id` (`process_stage_id`);

--
-- Indexes for table `process_steps`
--
ALTER TABLE `process_steps`
  ADD PRIMARY KEY (`process_step_id`,`process_id`,`process_stage_id`),
  ADD KEY `process_stage_id` (`process_stage_id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `process_id` (`process_id`);
  
--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`notification_id`),
  ADD KEY `notified_by` (`notified_by`);

ALTER TABLE `notifications`
  MODIFY `notification_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;  

--
-- Constraints for table `process`
--
ALTER TABLE `process`
  ADD CONSTRAINT `process_ibfk_1` FOREIGN KEY (`created_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `process_ibfk_2` FOREIGN KEY (`modified_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
  
--
-- Constraints for table `process_actions`
--
ALTER TABLE `process_actions`
  ADD CONSTRAINT `process_actions_ibfk_1` FOREIGN KEY (`process_id`) REFERENCES `process` (`process_id`) ON DELETE CASCADE ON UPDATE CASCADE;  
  
--
-- Constraints for table `process_stages`
--
ALTER TABLE `process_stages`
  ADD CONSTRAINT `process_stages_ibfk_1` FOREIGN KEY (`process_id`) REFERENCES `process` (`process_id`) ON DELETE CASCADE ON UPDATE CASCADE;
  
--
-- Constraints for table `process_stage_roles`
--
ALTER TABLE `process_stage_roles`
  ADD CONSTRAINT `process_stage_roles_ibfk_1` FOREIGN KEY (`role_code`) REFERENCES `roles` (`role_code`) ON UPDATE CASCADE,
  ADD CONSTRAINT `process_stage_roles_ibfk_2` FOREIGN KEY (`process_id`) REFERENCES `process` (`process_id`) ON DELETE CASCADE ON UPDATE CASCADE;
  
--
-- Constraints for table `process_steps`
--
ALTER TABLE `process_steps`
  ADD CONSTRAINT `process_steps_ibfk_1` FOREIGN KEY (`status_id`) REFERENCES `param_status` (`status_id`),
  ADD CONSTRAINT `process_steps_ibfk_2` FOREIGN KEY (`process_id`) REFERENCES `process` (`process_id`) ON DELETE CASCADE ON UPDATE CASCADE;
  
--
-- Constraints for table `notifications`
--
ALTER TABLE `notifications`
  ADD CONSTRAINT `notifications_ibfk_1` FOREIGN KEY (`notified_by`) REFERENCES `users` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
  