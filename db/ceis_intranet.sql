-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 26, 2016 at 09:18 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ceis_intranet`
--

-- --------------------------------------------------------

--
-- Table structure for table `contractor`
--

CREATE TABLE IF NOT EXISTS `contractor` (
  `contractor_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `contractor_name` varchar(255) NOT NULL COMMENT 'Complete name of the company ',
  `contractor_address` text NOT NULL COMMENT 'Present address or location of the company''s main office',
  `sec_reg_no` varchar(100) NOT NULL COMMENT 'SEC Registration Number',
  `sec_reg_date` date NOT NULL COMMENT 'Date of the Certificate of Registration',
  `contractor_category_id` int(10) unsigned NOT NULL COMMENT 'Refers to category of contractor',
  `license_no` varchar(100) DEFAULT NULL COMMENT 'License no. issued by the PCAB',
  `license_date` date DEFAULT NULL COMMENT 'Date of issue of PCAB license',
  `bir_clearance_date` date NOT NULL COMMENT 'Date when the BIR clearance was issued',
  `customs_clearance_date` date NOT NULL COMMENT 'Date when the Customs clearance was issued',
  `court_clearance_date` date NOT NULL COMMENT 'Date when the Court clearance was issued',
  `tel_no` varchar(255) DEFAULT NULL COMMENT 'Telephone number of contractor',
  `fax_no` varchar(100) DEFAULT NULL COMMENT 'Fax no of contractor',
  `email` varchar(255) DEFAULT NULL COMMENT 'Email of contractor',
  `website` varchar(255) DEFAULT NULL COMMENT 'Website of contractor',
  `rep_first_name` varchar(100) NOT NULL COMMENT 'First name of the authorized representative of the company',
  `rep_mid_name` varchar(100) DEFAULT NULL COMMENT 'Middle name of the authorized representative of the company',
  `rep_last_name` varchar(100) NOT NULL COMMENT 'Last  name of the authorized representative of the company',
  `rep_address` text NOT NULL COMMENT 'Address of the authorized representative of the company',
  `rep_contact_no` varchar(100) NOT NULL COMMENT 'Contact no. of the authorized representative of the company',
  `rep_email` varchar(100) NOT NULL COMMENT 'Email of the authorized representative of the company',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Stores all registered contractors/consultants';

--
-- Dumping data for table `contractor`
--

INSERT INTO `contractor` (`contractor_id`, `contractor_name`, `contractor_address`, `sec_reg_no`, `sec_reg_date`, `contractor_category_id`, `license_no`, `license_date`, `bir_clearance_date`, `customs_clearance_date`, `court_clearance_date`, `tel_no`, `fax_no`, `email`, `website`, `rep_first_name`, `rep_mid_name`, `rep_last_name`, `rep_address`, `rep_contact_no`, `rep_email`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(1, 'christian gil aquino', 'alele address', '11', '2016-12-12', 1, NULL, NULL, '0000-00-00', '0000-00-00', '0000-00-00', NULL, NULL, NULL, NULL, '', NULL, '', '', '', '', 0, '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contractor_contracts`
--

CREATE TABLE IF NOT EXISTS `contractor_contracts` (
  `contract_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifer',
  `contractor_id` int(10) unsigned NOT NULL COMMENT 'Refers to contractor',
  `contract_type` enum('P','M') NOT NULL DEFAULT 'M' COMMENT 'Type of Contracts; P - Project; M - Manpower Service',
  `base` enum('L','O') NOT NULL DEFAULT 'O' COMMENT 'L - Local; O - Overseas',
  `with_authorization_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Tagging if the contract was filed for POCB authorization',
  `project_title` varchar(255) NOT NULL COMMENT 'Title of project',
  `project_location` varchar(255) DEFAULT NULL COMMENT 'Location of project',
  `province` varchar(255) DEFAULT NULL COMMENT 'Province where the project was located',
  `country_code` varchar(10) NOT NULL COMMENT 'Refers to country where project was located',
  `contract_status_id` int(10) unsigned NOT NULL COMMENT 'Refers to status of contract',
  `project_cost` decimal(25,2) DEFAULT NULL COMMENT 'overall project construction contract cost',
  `contract_cost` decimal(25,2) DEFAULT NULL COMMENT 'Amount of contract handled by Contractor, it could be less than the project cost for sub-contractor',
  `owner` varchar(255) DEFAULT NULL COMMENT 'Name of owner',
  `contract_duration` varchar(100) DEFAULT NULL COMMENT 'Duration of contract',
  `started_date` date DEFAULT NULL COMMENT 'Official start date of the contract',
  `completed_date` date DEFAULT NULL COMMENT 'Official completion date of the contract',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores all contracts made by a contractor';

-- --------------------------------------------------------

--
-- Table structure for table `contractor_contracts_schedule`
--

CREATE TABLE IF NOT EXISTS `contractor_contracts_schedule` (
  `contract_id` int(10) unsigned NOT NULL COMMENT 'Refers to contract',
  `category` enum('D','I') NOT NULL DEFAULT 'D' COMMENT 'Category of schedule; D - Direct (Execution); I - Indirect (Administrative)',
  `skills_description` varchar(255) DEFAULT NULL COMMENT 'Desription of skills',
  `salary_rate` decimal(25,2) DEFAULT NULL COMMENT 'Salary rate of skills',
  `number_required` int(4) DEFAULT NULL COMMENT 'Number required of skills',
  `employment_contract` varchar(100) DEFAULT NULL COMMENT 'Employment contract ',
  `monthly_breakdown` varchar(100) DEFAULT NULL COMMENT 'Monthly breakdown',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the manpower schedule of manpower service contract';

-- --------------------------------------------------------

--
-- Table structure for table `contractor_equipment`
--

CREATE TABLE IF NOT EXISTS `contractor_equipment` (
  `serial_no` varchar(100) NOT NULL COMMENT 'Equipment code  number as taken from the company''s equipment inventory report',
  `registrant_name` varchar(255) NOT NULL COMMENT 'Name of registrant',
  `registrant_address` text NOT NULL COMMENT 'Address of registrant',
  `equipment_type` varchar(255) NOT NULL COMMENT 'Type of equipment',
  `capacity` varchar(255) DEFAULT NULL COMMENT 'Description should give the equipment''s common name or its main functional use. Size/capacity shuld describe further the equipment by giving its functional horsepower, rated capacity, etc.',
  `brand` text COMMENT 'Describes the equipment''s brand/model/series number as specified by the equipment''s manufacturer',
  `model` varchar(255) DEFAULT NULL COMMENT 'Model of the equipment',
  `acquired_date` date DEFAULT NULL COMMENT 'Date when the equipment was acquired by the contractor',
  `acquisition_cost` decimal(25,2) DEFAULT NULL COMMENT 'Book value/Acquisition cost',
  `present_condition` text COMMENT 'Present condition of equipment',
  `location` varchar(255) DEFAULT NULL COMMENT 'Equipment''s present location',
  `motor_no` varchar(100) DEFAULT NULL COMMENT 'Motor no; Applicable if the equipment is for registration',
  `color` varchar(100) DEFAULT NULL COMMENT 'Color of the equipment; Applicable if the equipment is for registration',
  `year` int(4) DEFAULT NULL COMMENT 'Year of equipment; Applicable if the equipment is for registration',
  `flywheel` varchar(100) DEFAULT NULL COMMENT 'Flywheel; Applicable if the equipment is for registration',
  `acquired_from` varchar(100) DEFAULT NULL COMMENT 'Acquired from; Applicable if the equipment is for registration',
  `ownership_type` varchar(45) DEFAULT NULL COMMENT 'Type of ownership; Applicable if the equipment is for registration',
  `active_flag` tinyint(1) NOT NULL COMMENT 'Tagging if the equipment is active',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the construction equipment currently owned by contractor';

--
-- Dumping data for table `contractor_equipment`
--

INSERT INTO `contractor_equipment` (`serial_no`, `registrant_name`, `registrant_address`, `equipment_type`, `capacity`, `brand`, `model`, `acquired_date`, `acquisition_cost`, `present_condition`, `location`, `motor_no`, `color`, `year`, `flywheel`, `acquired_from`, `ownership_type`, `active_flag`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
('123', 'christian', 'alele address', 'alele tyle', '45', 'brand x', 'victoria secret mowdel', '2016-12-12', '500.00', 'pre loved', 'location set', '567', 'blue', 1999, 'yes', 'rodel', 'ownership type alele', 1, 0, '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contractor_projects`
--

CREATE TABLE IF NOT EXISTS `contractor_projects` (
  `contract_id` int(10) unsigned NOT NULL COMMENT 'Refers to contract',
  `project_type_code` varchar(10) NOT NULL COMMENT 'Refers to type of project',
  `involvement_code` varchar(1) NOT NULL,
  `scheduled_percent` decimal(5,2) DEFAULT NULL COMMENT 'Schedule Percentage of accomplishment',
  `actual_percent` decimal(5,2) DEFAULT NULL COMMENT 'Actual Percentage of accomplishment',
  `remarks` text COMMENT 'Remarks about the project contract'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the project contract details';

-- --------------------------------------------------------

--
-- Table structure for table `contractor_project_dtl`
--

CREATE TABLE IF NOT EXISTS `contractor_project_dtl` (
  `contract_id` int(10) unsigned NOT NULL COMMENT 'Refers to contract',
  `project_description` text COMMENT 'General Description of the project',
  `scope_work` text COMMENT 'General scope of services',
  `est_min_manpower_required` int(5) DEFAULT NULL COMMENT 'Estimated minumum manpower required',
  `est_max_manpower_required` int(5) DEFAULT NULL COMMENT 'Estimated maximum manpower required',
  `outward_amount` decimal(25,2) DEFAULT NULL COMMENT 'Outward amunt of Foreign remittances',
  `inward_amount` decimal(25,2) DEFAULT NULL COMMENT 'Inward amunt of Foreign remittances',
  `procument_type` enum('B','N') NOT NULL DEFAULT 'B' COMMENT 'Construction Procurement; B- Biddingl; N - Negotiation',
  `contract_involvement` enum('P','S','O') NOT NULL DEFAULT 'P' COMMENT 'Contract involvement; P - Prime contractor; S - SUb-contractor; O - Others',
  `prime_contractor_name` varchar(255) DEFAULT NULL COMMENT 'Indicate Name of prime contractor if the contract involvement is sub-contractor',
  `prime_contractor_nationality` varchar(3) DEFAULT NULL COMMENT 'Naitonality of of prime contractor',
  `other_contract_involvement` varchar(255) NOT NULL COMMENT 'Specify the contract involvement ',
  `bidding_date` date DEFAULT NULL COMMENT 'Date of bidding/negotiation',
  `bidding_place` varchar(255) DEFAULT NULL COMMENT 'Place of bidding/negotiation',
  `contract_type_id` int(10) unsigned NOT NULL COMMENT 'Refers to contract type',
  `other_filipino_companies` varchar(255) DEFAULT NULL COMMENT 'Other filipino companies bidding/negotiations for the project',
  `prime_fin_assistance_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Tagging if the financial assistance/guarantee to be secured from Prime contractor',
  `other_fin_assistance` varchar(255) DEFAULT NULL COMMENT 'Specify the financial assistance/guarantee to be secured from',
  `financed_by` varchar(255) DEFAULT NULL COMMENT 'Projects to be financed by',
  `bid_bond` decimal(25,2) DEFAULT NULL COMMENT 'Bid bond',
  `performance_bond` decimal(25,2) DEFAULT NULL COMMENT 'Performance bond',
  `payment_guarantee` decimal(25,2) DEFAULT NULL COMMENT 'Payment guarantee',
  `retentions` varchar(100) DEFAULT NULL COMMENT 'Retentions',
  `progress_billing` varchar(100) DEFAULT NULL COMMENT 'Progress billings',
  `others` varchar(100) DEFAULT NULL COMMENT 'Others financial terms and conditions',
  `owner` varchar(255) DEFAULT NULL COMMENT 'Name of owner',
  `owner_address` text COMMENT 'Address of owner',
  `owner_background` text COMMENT 'Background of owner'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the project contract details';

-- --------------------------------------------------------

--
-- Table structure for table `contractor_services`
--

CREATE TABLE IF NOT EXISTS `contractor_services` (
  `contract_id` int(10) unsigned NOT NULL,
  `foreign_principal_name` varchar(255) NOT NULL COMMENT 'Name of Foreign principal',
  `nationality_code` varchar(3) NOT NULL COMMENT 'Refers to nationality of Foreign principal',
  `required_manpower` int(5) DEFAULT NULL COMMENT 'No. of required manpower',
  `mobilized_manpower` int(5) DEFAULT NULL COMMENT 'No. of mobilized manpower',
  `onsite_manpower` int(5) DEFAULT NULL COMMENT 'No. of onsite manpower',
  `fees` decimal(25,2) DEFAULT NULL COMMENT 'Service fee and others',
  `workers_salaries` decimal(25,2) DEFAULT NULL COMMENT 'Worker''s salaries',
  `remarks` text COMMENT 'Remarks about the contract'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the manpower service contract details';

-- --------------------------------------------------------

--
-- Table structure for table `contractor_services_dtl`
--

CREATE TABLE IF NOT EXISTS `contractor_services_dtl` (
  `contract_id` int(10) unsigned NOT NULL COMMENT 'Refers to contract',
  `project_description` text COMMENT 'General Description of the project',
  `service_scope` text COMMENT 'General scope of services',
  `est_manpower_required` int(5) DEFAULT NULL COMMENT 'Estimated manpower required',
  `bidding_date` date DEFAULT NULL COMMENT 'Date of bidding/negotiation',
  `bidding_place` varchar(255) DEFAULT NULL COMMENT 'Place of bidding/negotiation',
  `financed_by` varchar(255) DEFAULT NULL COMMENT 'Project to be financed by',
  `guarantees_required` varchar(255) DEFAULT NULL COMMENT 'Guarantess required',
  `foreign_principal_address` text COMMENT 'Address of foreign principal\n'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the manpower service contract details';

-- --------------------------------------------------------

--
-- Table structure for table `contractor_staff`
--

CREATE TABLE IF NOT EXISTS `contractor_staff` (
  `contr_staff_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `contractor_id` int(10) unsigned NOT NULL COMMENT 'Refers to contractor',
  `staff_type` enum('D','P','T','W') NOT NULL DEFAULT 'T' COMMENT 'Type of staff; D - Directors; P - Principal Officers; T - Key Technical Staff; W - Workers',
  `first_name` varchar(100) NOT NULL COMMENT 'First name of staff',
  `middle_name` varchar(100) NOT NULL COMMENT 'Middle name of staff',
  `last_name` varchar(100) NOT NULL COMMENT 'Last name of staff',
  `address` text NOT NULL COMMENT 'Address of staff',
  `birth_date` date NOT NULL COMMENT 'Date of birth of staff',
  `birth_place` text COMMENT 'Place of birth of staff',
  `educational_attainment` varchar(255) NOT NULL COMMENT 'Highest educational attainment of staff',
  `school_attended` varchar(255) NOT NULL COMMENT 'Name of School/University attended',
  `year_graduated` int(4) NOT NULL COMMENT 'Year graduated',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores of all key technical and management personnel of the contractor';

-- --------------------------------------------------------

--
-- Table structure for table `contractor_staff_experiences`
--

CREATE TABLE IF NOT EXISTS `contractor_staff_experiences` (
  `contr_staff_exp_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `contr_staff_id` int(10) unsigned NOT NULL COMMENT 'Refers to staff',
  `present_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Tagging if the record  is the present work experience of staff',
  `start_date` date NOT NULL COMMENT 'Date when the staff started working on the company',
  `end_date` date DEFAULT NULL COMMENT 'Date when the staff ended working on the company',
  `company_name` varchar(255) NOT NULL COMMENT 'Name of company',
  `company_address` text NOT NULL COMMENT 'Address of the company',
  `job_description` text COMMENT 'Job Description',
  `stockholder_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Tagging if the staff is a stockholder',
  `paid_in_amount` decimal(25,2) DEFAULT NULL COMMENT 'Amount Paid-in ',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modifed_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores all work experiences of staff';

-- --------------------------------------------------------

--
-- Table structure for table `contractor_staff_licenses`
--

CREATE TABLE IF NOT EXISTS `contractor_staff_licenses` (
  `license_no` varchar(100) NOT NULL COMMENT 'PRC License no.; Primary key, unique identifier',
  `contr_staff_id` int(10) unsigned NOT NULL COMMENT 'Refers to staff',
  `profession` varchar(255) NOT NULL COMMENT 'Profession ',
  `issued_date` date NOT NULL COMMENT 'Date when the license was issued',
  `expiry_date` date NOT NULL COMMENT 'Expiration date of license'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the professional licenses of staff';

-- --------------------------------------------------------

--
-- Table structure for table `contractor_staff_projects`
--

CREATE TABLE IF NOT EXISTS `contractor_staff_projects` (
  `contr_staff_proj_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `contr_staff_id` int(10) unsigned NOT NULL COMMENT 'Refers to staff',
  `project_title` varchar(255) NOT NULL COMMENT 'Title of the project',
  `project_location` text COMMENT 'Location of the project',
  `owner` varchar(255) DEFAULT NULL COMMENT 'Owner of the project',
  `positions` varchar(255) DEFAULT NULL COMMENT 'Positions of staff in the project',
  `project_type_code` varchar(10) NOT NULL COMMENT 'Refers to type of project',
  `start_date` date DEFAULT NULL COMMENT 'Number of construction years the personnel has experience',
  `end_date` date NOT NULL,
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores all projects handled by staff';

-- --------------------------------------------------------

--
-- Table structure for table `contractor_statements`
--

CREATE TABLE IF NOT EXISTS `contractor_statements` (
  `contractor_id` int(10) unsigned NOT NULL COMMENT 'Refers to contractor',
  `statement_date` date NOT NULL COMMENT 'Date of statement',
  `stmt_type` enum('A','I') NOT NULL DEFAULT 'A' COMMENT 'Type of statement; A - Audited; I - Interim',
  `total_current_assets` decimal(25,2) DEFAULT '0.00' COMMENT 'Total current assets',
  `total_non_current_assets` decimal(25,2) DEFAULT '0.00',
  `total_current_liabilities` decimal(25,2) DEFAULT '0.00' COMMENT 'Total current liabilities',
  `long_term_liabilities` decimal(25,2) DEFAULT '0.00' COMMENT 'Long term liabilities',
  `networth` decimal(25,2) DEFAULT '0.00' COMMENT 'Net worth',
  `gross_sales` decimal(25,2) DEFAULT '0.00' COMMENT 'Gross sales/revenues',
  `net_profit` decimal(25,2) DEFAULT '0.00' COMMENT 'Net profit after tax',
  `common_shares` decimal(25,2) DEFAULT '0.00' COMMENT 'No. of common shares outstanding',
  `par_value` int(5) DEFAULT NULL COMMENT 'Par value per common share',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the audited financial statement of the contractor';

-- --------------------------------------------------------

--
-- Table structure for table `contractor_work_volume`
--

CREATE TABLE IF NOT EXISTS `contractor_work_volume` (
  `year` int(4) NOT NULL COMMENT 'Year of work volume',
  `contractor_id` int(10) unsigned NOT NULL,
  `amount` decimal(25,2) NOT NULL DEFAULT '0.00' COMMENT 'Amount of work volume',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the annual work volume of operations of the company';

-- --------------------------------------------------------

--
-- Table structure for table `country_profiles`
--

CREATE TABLE IF NOT EXISTS `country_profiles` (
  `country_code` varchar(10) NOT NULL COMMENT 'Refers to country',
  `highlights` text NOT NULL COMMENT 'Highlights about the country',
  `file_name` varchar(255) DEFAULT NULL COMMENT 'Name of file',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `country_profiles`
--

INSERT INTO `country_profiles` (`country_code`, `highlights`, `file_name`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
('COL', 'lo po hu ij as rrf sasa', '', 0, '2016-05-23 03:28:43', NULL, NULL),
('HND', 'hu ud da sa ed rfdfdf fdf dfakj  jkjkasd ds', '["PROJECT_NAME_access_103013.txt (1)"]', 0, '2016-05-23 03:30:14', NULL, NULL),
('KWT', 'ku sagaj sjahsjahjsa sjahsja sjahjshajshjas sa', '"PROJECT_NAME_access_103645.txt (1)"', 0, '2016-05-23 03:36:46', NULL, NULL),
('PHL', 'sahsjahsjas pi noy sa ko aos sas', '"25: Error. Failed to create folder."]', 0, '2016-05-23 03:45:33', NULL, NULL),
('TCD', 'hjdhjd sdhjsdhjs dsjhdsjdh sjhdjshdjshjdhs dsd sds d', '["PROJECT_NAME_access_113211.txt (1)"]', 0, '2016-05-23 04:32:11', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `opportunities`
--

CREATE TABLE IF NOT EXISTS `opportunities` (
  `opportunity_id` int(10) unsigned NOT NULL,
  `reference_no` varchar(100) NOT NULL,
  `received_date` date DEFAULT NULL,
  `opp_source_id` int(10) unsigned NOT NULL,
  `source_agency` varchar(255) DEFAULT NULL,
  `subject` text,
  `opp_status_id` int(10) unsigned NOT NULL COMMENT 'Refers to status of received opportunity',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB AUTO_INCREMENT=179 DEFAULT CHARSET=utf8 COMMENT='Stores all opportunities received from other agencies';

--
-- Dumping data for table `opportunities`
--

INSERT INTO `opportunities` (`opportunity_id`, `reference_no`, `received_date`, `opp_source_id`, `source_agency`, `subject`, `opp_status_id`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(176, '2016-001', '2016-05-12', 124, 'craving your body all  _ the night', 'subject matter', 124, 0, '2016-05-24 04:03:17', NULL, NULL),
(177, '2016-002', '2016-05-12', 124, 'craving your body all  _ the night', 'subject matter', 124, 0, '2016-05-24 04:03:53', NULL, NULL),
(178, '2016-003', '2016-05-12', 124, 'craving your body all  _ the night', 'subject matter', 124, 0, '2016-05-24 04:04:09', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `opportunity_recipients`
--

CREATE TABLE IF NOT EXISTS `opportunity_recipients` (
  `recipient_id` int(11) NOT NULL,
  `opportunity_id` int(10) unsigned NOT NULL,
  `contractor_name` varchar(255) NOT NULL COMMENT 'Name of contractor',
  `email` varchar(100) NOT NULL COMMENT 'Email of contractor',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date when the record was created'
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=utf8 COMMENT='Stores the list of recipients ';

--
-- Dumping data for table `opportunity_recipients`
--

INSERT INTO `opportunity_recipients` (`recipient_id`, `opportunity_id`, `contractor_name`, `email`, `created_by`, `created_date`) VALUES
(24, 112, 'dss', 'dad@dsf.dfdfd', 0, '2016-05-19 01:29:32'),
(25, 113, 'dss', 'dad@dsf.dfdfd', 0, '2016-05-19 01:29:51'),
(28, 126, 'not in not in not list', 'sasas@dsadad.dsdsds', 0, '2016-05-19 01:49:45'),
(29, 127, '1', 'sasas@dsadad.dsdsds', 0, '2016-05-19 01:49:54'),
(30, 129, '1', 'christian@alele.com', 0, '2016-05-19 01:56:39'),
(85, 176, 'google', 'cgaquino@asiagate.com', 0, '2016-05-23 21:03:17'),
(86, 176, 'google2', 'christiangilaquino@gmail.com', 0, '2016-05-23 21:03:17'),
(87, 176, 'google3', 'aquinochristiangil@gmail.com', 0, '2016-05-23 21:03:17'),
(88, 176, 'google4', 'cgaquino@asiagate.com', 0, '2016-05-23 21:03:17'),
(89, 176, 'google5', 'cgaquino@asiagate.com', 0, '2016-05-23 21:03:17'),
(90, 177, 'google', 'cgaquino@asiagate.com', 0, '2016-05-23 21:03:53'),
(91, 177, 'google2', 'christiangilaquino@gmail.com', 0, '2016-05-23 21:03:53'),
(92, 177, 'google3', 'aquinochristiangil@gmail.com', 0, '2016-05-23 21:03:53'),
(93, 177, 'google4', 'cgaquino@asiagate.com', 0, '2016-05-23 21:03:53'),
(94, 177, 'google5', 'cgaquino@asiagate.com', 0, '2016-05-23 21:03:53'),
(95, 178, 'google', 'cgaquino@asiagate.com', 0, '2016-05-23 21:04:09'),
(96, 178, 'google2', 'christiangilaquino@gmail.com', 0, '2016-05-23 21:04:09'),
(97, 178, 'google3', 'aquinochristiangil@gmail.com', 0, '2016-05-23 21:04:09'),
(98, 178, 'google4', 'cgaquino@asiagate.com', 0, '2016-05-23 21:04:09'),
(99, 178, 'google5', 'cgaquino@asiagate.com', 0, '2016-05-23 21:04:09');

-- --------------------------------------------------------

--
-- Table structure for table `param_attachment_types`
--

CREATE TABLE IF NOT EXISTS `param_attachment_types` (
  `attachment_type_id` int(11) NOT NULL,
  `attachment_type` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `param_checklist`
--

CREATE TABLE IF NOT EXISTS `param_checklist` (
  `checklist_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `request_type_id` int(10) unsigned NOT NULL COMMENT 'Refers type of request',
  `checklist` text NOT NULL COMMENT 'Checklist description',
  `with_attachment_flag` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Tagging if the checklist needs an attachment',
  `required_flag` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Tagging if the checklist is required',
  `sort_order` tinyint(2) NOT NULL DEFAULT '1' COMMENT 'Sorting of checklist',
  `for_submission_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Tagging if the checklist is for submission required documents upon releasing of registration'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the requirements or checklist per type of request';

-- --------------------------------------------------------

--
-- Table structure for table `param_collection_natures`
--

CREATE TABLE IF NOT EXISTS `param_collection_natures` (
  `coll_nature_code` varchar(100) NOT NULL COMMENT 'Primary key; Unique identifier',
  `collection_nature` varchar(255) NOT NULL COMMENT 'Nature of collection'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `param_contractor_categories`
--

CREATE TABLE IF NOT EXISTS `param_contractor_categories` (
  `contractor_category_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `contractor_category_name` varchar(100) NOT NULL COMMENT 'Categories: General Construction Contractor, Specialty Contractor, Specialized Consultancy'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='List of categories of contractor';

--
-- Dumping data for table `param_contractor_categories`
--

INSERT INTO `param_contractor_categories` (`contractor_category_id`, `contractor_category_name`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Table structure for table `param_contract_involvement`
--

CREATE TABLE IF NOT EXISTS `param_contract_involvement` (
  `involvement_code` varchar(1) NOT NULL COMMENT 'Primary key; Unique Identifier',
  `involvement` varchar(50) NOT NULL COMMENT 'Main Contractor, Sub-contractor, Joint venture',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modifed'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of contact involvement';

--
-- Dumping data for table `param_contract_involvement`
--

INSERT INTO `param_contract_involvement` (`involvement_code`, `involvement`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
('a', 'kokok', 0, '2016-05-04 17:28:57', NULL, '2016-05-05 00:41:47'),
('b', 'fdsfas', 0, '2016-05-04 18:31:03', NULL, NULL),
('s', 'dfsfdsf', 0, '2016-05-04 18:32:09', NULL, NULL),
('t', 'try again', 0, '2016-05-04 11:40:38', NULL, '2016-05-05 01:38:26'),
('u', 'uiuia', 0, '2016-05-03 14:25:36', NULL, '2016-05-10 01:21:32');

-- --------------------------------------------------------

--
-- Table structure for table `param_contract_status`
--

CREATE TABLE IF NOT EXISTS `param_contract_status` (
  `contract_status_id` int(10) unsigned NOT NULL COMMENT 'Primary  key; Unique identifier',
  `contract_status_name` varchar(20) NOT NULL COMMENT 'Onhand, Ongoing, Completed, Rescinded; For Authorization;'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='List of statuses of a contract';

--
-- Dumping data for table `param_contract_status`
--

INSERT INTO `param_contract_status` (`contract_status_id`, `contract_status_name`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Table structure for table `param_contract_types`
--

CREATE TABLE IF NOT EXISTS `param_contract_types` (
  `contract_type_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `contract_type_name` varchar(100) NOT NULL COMMENT 'Name of contract type'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of all contract types used in project authorization';

-- --------------------------------------------------------

--
-- Table structure for table `param_countries`
--

CREATE TABLE IF NOT EXISTS `param_countries` (
  `country_code` varchar(10) NOT NULL COMMENT 'Primary key; Unique identifier',
  `country_name` varchar(255) NOT NULL COMMENT 'Name of country',
  `currency_code` varchar(10) NOT NULL COMMENT 'Curreny code of country'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of all countries';

--
-- Dumping data for table `param_countries`
--

INSERT INTO `param_countries` (`country_code`, `country_name`, `currency_code`) VALUES
('ABW', 'Aruba', 'AWG'),
('AFG', 'Afghanistan', 'AFN'),
('AGO', 'Angola', 'AOA'),
('AIA', 'Anguilla', 'XCD'),
('ALB', 'Albania', 'ALL'),
('AND', 'Andorra', 'EUR'),
('ARE', 'United Arab Emirates', 'AED'),
('ARG', 'Argentina', 'ARS'),
('ARM', 'Armenia', 'AMD'),
('ASM', 'American Samoa', 'USD'),
('ATA', 'Antarctica', 'XCD'),
('ATG', 'Antigua and Barbuda', 'XCD'),
('AUS', 'Australia', 'AUD'),
('AUT', 'Austria', 'EUR'),
('AZE', 'Azerbaijan', 'AZN'),
('BDI', 'Burundi', 'BIF'),
('BEL', 'Belgium', 'EUR'),
('BEN', 'Benin', 'XOF'),
('BFA', 'Burkina Faso', 'XOF'),
('BGD', 'Bangladesh', 'BDT'),
('BGR', 'Bulgaria', 'BGN'),
('BHR', 'Bahrain', 'BHD'),
('BHS', 'Bahamas', 'BSD'),
('BIH', 'Bosnia-Herzegovina', 'BAM'),
('BLR', 'Belarus', 'BYR'),
('BLZ', 'Belize', 'BZD'),
('BMU', 'Bermuda', 'BMD'),
('BOL', 'Bolivia', 'BOB'),
('BRA', 'Brazil', 'BRL'),
('BRB', 'Barbados', 'BBD'),
('BRN', 'Brunei Darussalam', 'BND'),
('BTN', 'Bhutan', 'BTN'),
('BVT', 'Bouvet Island', 'NOK'),
('BWA', 'Botswana', 'BWP'),
('CAF', 'Central African Republic', 'XAF'),
('CAN', 'Canada', 'CAD'),
('CCK', 'Cocos (Keeling) Islands', 'AUD'),
('CHE', 'Switzerland', 'CHF'),
('CHL', 'Chile', 'CLP'),
('CHN', 'China', 'CNY'),
('CMR', 'Cameroon', 'XAF'),
('COD', 'Congo, Dem. Republic', 'CDF'),
('COG', 'Congo', 'XAF'),
('COK', 'Cook Islands', 'NZD'),
('COL', 'Colombia', 'COP'),
('COM', 'Comoros', 'KMF'),
('CPV', 'Cape Verde', 'CVE'),
('CRI', 'Costa Rica', 'CRC'),
('CUB', 'Cuba', 'CUP'),
('CXR', 'Christmas Island', 'AUD'),
('CYM', 'Cayman Islands', 'KYD'),
('CYP', 'Cyprus', 'EUR'),
('CZE', 'Czech Rep.', 'CZK'),
('DEU', 'Germany', 'EUR'),
('DJI', 'Djibouti', 'DJF'),
('DMA', 'Dominica', 'XCD'),
('DNK', 'Denmark', 'DKK'),
('DOM', 'Dominican Republic', 'DOP'),
('DZA', 'Algeria', 'DZD'),
('ECU', 'Ecuador', 'ECS'),
('EGY', 'Egypt', 'EGP'),
('ERI', 'Eritrea', 'ERN'),
('ESH', 'Western Sahara', 'MAD'),
('ESP', 'Spain', 'EUR'),
('EST', 'Estonia', 'EUR'),
('ETH', 'Ethiopia', 'ETB'),
('FIN', 'Finland', 'EUR'),
('FJI', 'Fiji', 'FJD'),
('FLK', 'Falkland Islands (Malvinas)', 'FKP'),
('FRA', 'France', 'EUR'),
('FRO', 'Faroe Islands', 'DKK'),
('FSM', 'Micronesia', 'USD'),
('GAB', 'Gabon', 'XAF'),
('GBR', 'U.K.', 'GBP'),
('GEO', 'Georgia', 'GEL'),
('GGY', 'Guernsey', 'GGP'),
('GHA', 'Ghana', 'GHS'),
('GIB', 'Gibraltar', 'GIP'),
('GIN', 'Guinea', 'GNF'),
('GLP', 'Guadeloupe (French)', 'EUR'),
('GMB', 'Gambia', 'GMD'),
('GNB', 'Guinea Bissau', 'GWP'),
('GNQ', 'Equatorial Guinea', 'XAF'),
('GRC', 'Greece', 'EUR'),
('GRD', 'Grenada', 'XCD'),
('GRL', 'Greenland', 'DKK'),
('GTM', 'Guatemala', 'QTQ'),
('GUF', 'French Guiana', 'EUR'),
('GUM', 'Guam (USA)', 'USD'),
('GUY', 'Guyana', 'GYD'),
('HKG', 'Hong Kong', 'HKD'),
('HMD', 'Heard Island and McDonald Islands', 'AUD'),
('HND', 'Honduras', 'HNL'),
('HRV', 'Croatia', 'HRK'),
('HTI', 'Haiti', 'HTG'),
('HUN', 'Hungary', 'HUF'),
('IDN', 'Indonesia', 'IDR'),
('IMN', 'Isle of Man', 'GBP'),
('IND', 'India', 'INR'),
('IOT', 'British Indian Ocean Territory', 'USD'),
('IRL', 'Ireland', 'EUR'),
('IRN', 'Iran', 'IRR'),
('IRQ', 'Iraq', 'IQD'),
('ISL', 'Iceland', 'ISK'),
('ISR', 'Israel', 'ILS'),
('ITA', 'Italy', 'EUR'),
('JAM', 'Jamaica', 'JMD'),
('JEY', 'Jersey', 'GBP'),
('JOR', 'Jordan', 'JOD'),
('JPN', 'Japan', 'JPY'),
('KAZ', 'Kazakhstan', 'KZT'),
('KEN', 'Kenya', 'KES'),
('KGZ', 'Kyrgyzstan', 'KGS'),
('KHM', 'Cambodia', 'KHR'),
('KIR', 'Kiribati', 'AUD'),
('KNA', 'Saint Kitts & Nevis Anguilla', 'XCD'),
('KOR', 'Korea-South', 'KRW'),
('KWT', 'Kuwait', 'KWD'),
('LAO', 'Laos', 'LAK'),
('LBN', 'Lebanon', 'LBP'),
('LBR', 'Liberia', 'LRD'),
('LBY', 'Libya', 'LYD'),
('LCA', 'Saint Lucia', 'XCD'),
('LIE', 'Liechtenstein', 'CHF'),
('LKA', 'Sri Lanka', 'LKR'),
('LSO', 'Lesotho', 'LSL'),
('LTU', 'Lithuania', 'LTL'),
('LUX', 'Luxembourg', 'EUR'),
('LVA', 'Latvia', 'LVL'),
('MAC', 'Macau', 'MOP'),
('MAR', 'Morocco', 'MAD'),
('MCO', 'Monaco', 'EUR'),
('MDA', 'Moldova', 'MDL'),
('MDG', 'Madagascar', 'MGF'),
('MDV', 'Maldives', 'MVR'),
('MEX', 'Mexico', 'MXN'),
('MHL', 'Marshall Islands', 'USD'),
('MKD', 'Macedonia', 'MKD'),
('MLI', 'Mali', 'XOF'),
('MLT', 'Malta', 'EUR'),
('MMR ', 'Myanmar', 'MMK'),
('MNE', 'Montenegro', 'EUR'),
('MNG', 'Mongolia', 'MNT'),
('MNP', 'Northern Mariana Islands', 'USD'),
('MOZ', 'Mozambique', 'MZN'),
('MRT', 'Mauritania', 'MRO'),
('MSR', 'Montserrat', 'XCD'),
('MTQ', 'Martinique (French)', 'EUR'),
('MUS', 'Mauritius', 'MUR'),
('MWI', 'Malawi', 'MWK'),
('MYS', 'Malaysia', 'MYR'),
('MYT', 'Mayotte', 'EUR'),
('NAM', 'Namibia', 'NAD'),
('NCL', 'New Caledonia (French)', 'XPF'),
('NER', 'Niger', 'XOF'),
('NFK', 'Norfolk Island', 'AUD'),
('NGA', 'Nigeria', 'NGN'),
('NIC', 'Nicaragua', 'NIO'),
('NIU', 'Niue', 'NZD'),
('NLD', 'Netherlands', 'EUR'),
('NOR', 'Norway', 'NOK'),
('NPL', 'Nepal', 'NPR'),
('NRU', 'Nauru', 'AUD'),
('NZL', 'New Zealand', 'NZD'),
('OMN', 'Oman', 'OMR'),
('PAK', 'Pakistan', 'PKR'),
('PAN', 'Panama', 'PAB'),
('PCN', 'Pitcairn Island', 'NZD'),
('PER', 'Peru', 'PEN'),
('PHL', 'Philippines', 'PHP'),
('PLW', 'Palau', 'USD'),
('PNG', 'Papua New Guinea', 'PGK'),
('POL', 'Poland', 'PLN'),
('PRI', 'Puerto Rico', 'USD'),
('PRK', 'Korea-North', 'KPW'),
('PRT', 'Portugal', 'EUR'),
('PRY', 'Paraguay', 'PYG'),
('PYF', 'French Southern Territories', 'EUR'),
('QAT', 'Qatar', 'QAR'),
('REU', 'Reunion (French)', 'EUR'),
('ROU', 'Romania', 'RON'),
('RUS', 'Russia', 'RUB'),
('RWA', 'Rwanda', 'RWF'),
('SAU', 'Saudi Arabia', 'SAR'),
('SDN', 'Sudan', 'SDG'),
('SEN', 'Senegal', 'XOF'),
('SGP', 'Singapore', 'SGD'),
('SGS', 'South Georgia & South Sandwich Islands', 'GBP'),
('SHN', 'Saint Helena', 'SHP'),
('SJM', 'Svalbard and Jan Mayen Islands', 'NOK'),
('SLB', 'Solomon Islands', 'SBD'),
('SLE', 'Sierra Leone', 'SLL'),
('SLV', 'El Salvador', 'SVC'),
('SMR', 'San Marino', 'EUR'),
('SOM', 'Somalia', 'SOS'),
('SPM', 'Saint Pierre and Miquelon', 'EUR'),
('SRB', 'Serbia', 'RSD'),
('SSD', 'South Sudan', 'SSP'),
('STP', 'Sao Tome and Principe', 'STD'),
('SUR', 'Suriname', 'SRD'),
('SVK', 'Slovakia', 'EUR'),
('SVN', 'Slovenia', 'EUR'),
('SWE', 'Sweden', 'SEK'),
('SWZ', 'Swaziland', 'SZL'),
('SYC', 'Seychelles', 'SCR'),
('SYR', 'Syria', 'SYP'),
('TCA', 'Turks and Caicos Islands', 'USD'),
('TCD', 'Chad', 'XAF'),
('TGO', 'Togo', 'XOF'),
('THA', 'Thailand', 'THB'),
('TJK', 'Tajikistan', 'TJS'),
('TKL', 'Tokelau', 'NZD'),
('TKM', 'Turkmenistan', 'TMT'),
('TON', 'Tonga', 'TOP'),
('TTO', 'Trinidad and Tobago', 'TTD'),
('TUN', 'Tunisia', 'TND'),
('TUR', 'Turkey', 'TRY'),
('TUV', 'Tuvalu', 'AUD'),
('TWN', 'Taiwan', 'TWD'),
('TZA', 'Tanzania', 'TZS'),
('UGA', 'Uganda', 'UGX'),
('UKR', 'Ukraine', 'UAH'),
('UMI', 'USA Minor Outlying Islands', 'USD'),
('URY', 'Uruguay', 'UYU'),
('USA', 'USA', 'USD'),
('UZB', 'Uzbekistan', 'UZS'),
('VCT', 'Saint Vincent & Grenadines', 'XCD'),
('VEN', 'Venezuela', 'VEF'),
('VGB', 'Virgin Islands (British)', 'USD'),
('VIR', 'Virgin Islands (USA)', 'USD'),
('VNM', 'Vietnam', 'VND'),
('VUT', 'Vanuatu', 'VUV'),
('WLF', 'Wallis and Futuna Islands', 'XPF'),
('WSM', 'Samoa', 'WST'),
('YEM', 'Yemen', 'YER'),
('ZAF', 'South Africa', 'ZAR'),
('ZMB', 'Zambia', 'ZMW'),
('ZWE', 'Zimbabwe', 'ZWD');

-- --------------------------------------------------------

--
-- Table structure for table `param_document_types`
--

CREATE TABLE IF NOT EXISTS `param_document_types` (
  `doc_type_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `doc_type_name` varchar(100) NOT NULL COMMENT 'Name of document type'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of all type of documents submitted in application for registration of equipment';

-- --------------------------------------------------------

--
-- Table structure for table `param_geo_regions`
--

CREATE TABLE IF NOT EXISTS `param_geo_regions` (
  `geo_region_id` int(11) NOT NULL,
  `region_name` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8 COMMENT='Geographical distribution of countries';

--
-- Dumping data for table `param_geo_regions`
--

INSERT INTO `param_geo_regions` (`geo_region_id`, `region_name`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(75, 'Middle East', 0, '2016-05-18 16:32:47', NULL, NULL),
(76, 'Africa', 0, '2016-05-18 16:33:29', NULL, NULL),
(77, 'N./S./C. America', 0, '2016-05-18 16:34:03', NULL, NULL),
(78, 'Asia', 0, '2016-05-18 16:36:10', NULL, NULL),
(79, 'Oceania-Pacific', 0, '2016-05-18 16:36:40', NULL, NULL),
(80, 'Russia', 0, '2016-05-18 16:36:51', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `param_geo_region_countries`
--

CREATE TABLE IF NOT EXISTS `param_geo_region_countries` (
  `geo_region_id` int(11) NOT NULL COMMENT 'Refers to geographical region',
  `country_code` varchar(10) NOT NULL COMMENT 'Refers to country'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Indicates the region of countries';

--
-- Dumping data for table `param_geo_region_countries`
--

INSERT INTO `param_geo_region_countries` (`geo_region_id`, `country_code`) VALUES
(75, 'ARE'),
(75, 'BHR'),
(75, 'DJI'),
(75, 'IRN'),
(75, 'IRQ'),
(75, 'KWT'),
(75, 'LBY'),
(75, 'OMN'),
(75, 'QAT'),
(75, 'SAU'),
(75, 'SYR'),
(75, 'TUR'),
(76, 'AGO'),
(76, 'DZA'),
(76, 'EGY'),
(76, 'KEN'),
(76, 'MLI'),
(76, 'MOZ'),
(76, 'NGA'),
(77, 'ATG'),
(77, 'BRA'),
(77, 'VEN'),
(78, 'AFG'),
(78, 'AZE'),
(78, 'BGD'),
(78, 'BRN'),
(78, 'CHN'),
(78, 'HKG'),
(78, 'IDN'),
(78, 'JPN'),
(78, 'KAZ'),
(78, 'KOR'),
(78, 'LKA'),
(78, 'MDV'),
(78, 'MNG'),
(78, 'MYS'),
(78, 'NPL'),
(78, 'PNG'),
(78, 'PRK'),
(78, 'SGP'),
(78, 'THA'),
(78, 'TWN'),
(79, 'FJI'),
(79, 'FSM'),
(79, 'GUM'),
(79, 'NCL'),
(79, 'PLW'),
(79, 'TUV'),
(80, 'RUS');

-- --------------------------------------------------------

--
-- Table structure for table `param_license_classifications`
--

CREATE TABLE IF NOT EXISTS `param_license_classifications` (
  `license_classification_code` varchar(15) NOT NULL COMMENT 'Primay key; Unique identifier; ',
  `license_classification_name` varchar(100) NOT NULL COMMENT 'Classification of license',
  `specialty_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Tagging if the classification is a specialty classification',
  `sort_order` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Sorting of classifications'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of classifications of license';

-- --------------------------------------------------------

--
-- Table structure for table `param_nationalities`
--

CREATE TABLE IF NOT EXISTS `param_nationalities` (
  `nationality_code` varchar(3) NOT NULL COMMENT 'Primary key; Unique identifier',
  `country_code` varchar(10) NOT NULL,
  `nationality_name` varchar(255) NOT NULL COMMENT 'Description/Name of nationality'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of all nationalities';

-- --------------------------------------------------------

--
-- Table structure for table `param_opportunity_sources`
--

CREATE TABLE IF NOT EXISTS `param_opportunity_sources` (
  `opp_source_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `source` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8 COMMENT='List of all sources of opportunities';

--
-- Dumping data for table `param_opportunity_sources`
--

INSERT INTO `param_opportunity_sources` (`opp_source_id`, `source`) VALUES
(123, 'source one'),
(124, 'source two'),
(125, 'source three'),
(126, 'source four'),
(127, 'source five');

-- --------------------------------------------------------

--
-- Table structure for table `param_opportunity_status`
--

CREATE TABLE IF NOT EXISTS `param_opportunity_status` (
  `opp_status_id` int(10) unsigned NOT NULL,
  `opp_status` varchar(50) NOT NULL COMMENT 'Status; Referral Sent'
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8 COMMENT='List of status of received opportunities';

--
-- Dumping data for table `param_opportunity_status`
--

INSERT INTO `param_opportunity_status` (`opp_status_id`, `opp_status`) VALUES
(123, ''),
(124, '');

-- --------------------------------------------------------

--
-- Table structure for table `param_positions`
--

CREATE TABLE IF NOT EXISTS `param_positions` (
  `position_id` int(10) unsigned NOT NULL COMMENT 'Primary key; unique identifier',
  `position_name` varchar(100) NOT NULL COMMENT 'Name of position/skill',
  `created_by` int(11) DEFAULT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime DEFAULT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `param_positions`
--

INSERT INTO `param_positions` (`position_id`, `position_name`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(50, 'popo', NULL, '2016-05-19 11:07:39', NULL, '2016-05-26 02:52:10'),
(52, 'sdadasd asdasd ad ad', NULL, '2016-05-19 11:10:01', NULL, NULL),
(57, 'new stuff eklabu', NULL, '2016-05-23 09:04:38', NULL, NULL),
(70, 'christian position', NULL, '2016-05-24 03:50:09', NULL, NULL),
(73, 'lelepong', NULL, '2016-05-25 14:20:19', NULL, NULL),
(74, 'iuiuiuiuiu', NULL, '2016-05-25 14:20:48', NULL, NULL),
(75, 'nineteen', NULL, '2016-05-25 14:21:24', NULL, NULL),
(76, 'pang lima', NULL, '2016-05-25 14:40:09', NULL, NULL),
(77, 'om eh', NULL, '2016-05-25 15:14:51', NULL, NULL),
(78, 'paps', NULL, '2016-05-25 15:41:15', NULL, NULL),
(81, 'sasas', NULL, '2016-05-25 17:05:11', NULL, NULL),
(84, 'craving your body', NULL, '2016-05-25 17:06:55', NULL, '2016-05-26 02:54:26'),
(88, 'name', NULL, '2016-05-25 17:23:06', NULL, '2016-05-25 11:51:01'),
(89, 'christian', NULL, '2016-05-25 17:23:16', NULL, '2016-05-25 11:51:11'),
(90, 'nuhnuh', NULL, '2016-05-25 17:30:59', NULL, NULL),
(91, 'cocjin', NULL, '2016-05-25 17:32:04', NULL, '2016-05-25 11:51:29'),
(92, 'gil', NULL, '2016-05-25 17:32:38', NULL, '2016-05-25 11:51:20'),
(93, 'opop', NULL, '2016-05-25 17:35:24', NULL, NULL),
(96, 'take it slow alele', NULL, '2016-05-25 17:37:21', NULL, '2016-05-25 11:39:39'),
(98, 'mayabang si kokin', NULL, '2016-05-25 18:37:12', NULL, '2016-05-25 11:37:31');

-- --------------------------------------------------------

--
-- Table structure for table `param_project_types`
--

CREATE TABLE IF NOT EXISTS `param_project_types` (
  `project_type_code` varchar(10) NOT NULL COMMENT 'Primary key; Unique identifier',
  `project_type_name` varchar(255) NOT NULL COMMENT 'Type of project',
  `classification` enum('H','V','O') NOT NULL DEFAULT 'H' COMMENT 'Classification of project type; H - Horizontal; V - Vertical; O - Others',
  `created_by` int(11) DEFAULT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime DEFAULT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='List of types of project';

--
-- Dumping data for table `param_project_types`
--

INSERT INTO `param_project_types` (`project_type_code`, `project_type_name`, `classification`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
('alele', 'pongs', 'V', NULL, '2016-05-03 14:47:47', NULL, '2016-05-12 05:33:17'),
('alelepo', 'alele', 'O', NULL, '2016-05-10 11:42:49', NULL, NULL),
('dede', 'more', 'O', NULL, '2016-05-05 09:12:36', NULL, '2016-05-05 03:01:49'),
('other', 'other', 'H', NULL, '2016-05-05 09:10:44', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `param_registration_status`
--

CREATE TABLE IF NOT EXISTS `param_registration_status` (
  `registration_status_id` int(10) unsigned NOT NULL COMMENT 'Primary key, Unique identifier',
  `registration_status` varchar(45) NOT NULL COMMENT 'Status of Registration; Active, Delisted, Renewal Ongoing'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `param_request_status`
--

CREATE TABLE IF NOT EXISTS `param_request_status` (
  `request_status_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `request_status` varchar(100) NOT NULL COMMENT 'Status of request'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='List of statuses of request';

--
-- Dumping data for table `param_request_status`
--

INSERT INTO `param_request_status` (`request_status_id`, `request_status`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Table structure for table `param_request_tasks`
--

CREATE TABLE IF NOT EXISTS `param_request_tasks` (
  `request_task_id` int(11) NOT NULL,
  `request_type_id` int(10) unsigned NOT NULL,
  `sequence_no` int(11) NOT NULL,
  `task_name` varchar(255) NOT NULL,
  `role_code` varchar(100) NOT NULL,
  `generate_reference_flag` tinyint(1) NOT NULL DEFAULT '0',
  `get_task_flag` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `param_request_tasks`
--

INSERT INTO `param_request_tasks` (`request_task_id`, `request_type_id`, `sequence_no`, `task_name`, `role_code`, `generate_reference_flag`, `get_task_flag`) VALUES
(1, 1, 12, '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `param_request_task_status`
--

CREATE TABLE IF NOT EXISTS `param_request_task_status` (
  `task_status_id` int(10) unsigned NOT NULL,
  `task_status` varchar(100) NOT NULL COMMENT 'Not yet started; Ongoing; Done; For Compliance; Return to Previous task; Skipped'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `param_request_types`
--

CREATE TABLE IF NOT EXISTS `param_request_types` (
  `request_type_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `request_type_name` varchar(255) NOT NULL COMMENT 'Type of request; Application of Registrationl; Application for Renewal of Registration; Authority to undertake/bid project contracts; Authority to undertake/bid manpower service contracts'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='List of all type of requests';

--
-- Dumping data for table `param_request_types`
--

INSERT INTO `param_request_types` (`request_type_id`, `request_type_name`) VALUES
(1, '');

-- --------------------------------------------------------

--
-- Table structure for table `progress_reports`
--

CREATE TABLE IF NOT EXISTS `progress_reports` (
  `progress_report_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifer',
  `contract_id` int(10) unsigned NOT NULL COMMENT 'Refers to contract',
  `year` int(4) unsigned NOT NULL COMMENT 'Reporting year',
  `quarter` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT 'Reporting quarter',
  `submitted_flag` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Tagging if the report was submitted',
  `started_date` date DEFAULT NULL COMMENT 'Official start date of the contract; ',
  `orig_completion_date` date DEFAULT NULL COMMENT 'Originail completion date',
  `rev_completion_date` date DEFAULT NULL COMMENT 'Revised completion date',
  `official_completion_date` date DEFAULT NULL COMMENT 'Official completion date',
  `contract_status_id` int(10) unsigned NOT NULL COMMENT 'Refers to status of contract',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to yser who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores all progress reports of contract';

-- --------------------------------------------------------

--
-- Table structure for table `progress_report_info`
--

CREATE TABLE IF NOT EXISTS `progress_report_info` (
  `progress_report_id` int(10) unsigned NOT NULL COMMENT 'Refers to progress report',
  `type` enum('P','S') NOT NULL DEFAULT 'P' COMMENT 'P  - Professional; S - Skilled/Semi-skilled worker',
  `req_manpower` int(5) DEFAULT NULL COMMENT 'Total manpower requirements',
  `mob_manpower` int(5) DEFAULT NULL COMMENT 'Total manpower mobilized',
  `onsite_manpower` int(5) DEFAULT NULL COMMENT 'Total manpower onsite',
  `payroll_amount` decimal(25,2) DEFAULT NULL COMMENT 'Amount of payroll',
  `service_fee` decimal(25,2) DEFAULT NULL COMMENT 'Contractor service fee',
  `other_fee` decimal(25,2) DEFAULT NULL COMMENT 'Other fees',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the mapower requirements and financial report for professional and skilled/semi-skilled workers';

-- --------------------------------------------------------

--
-- Table structure for table `progress_report_manpower`
--

CREATE TABLE IF NOT EXISTS `progress_report_manpower` (
  `progress_report_id` int(10) unsigned NOT NULL COMMENT 'Refers to progress report',
  `contr_staff_id` int(10) unsigned NOT NULL COMMENT 'Refers to active key tehcnical staff of contractor',
  `position_id` int(10) unsigned NOT NULL,
  `salary_in_usd` decimal(25,2) NOT NULL COMMENT 'Salary of staff in USD',
  `start_duration` date DEFAULT NULL COMMENT 'Start date of contract duration',
  `end_duration` date DEFAULT NULL COMMENT 'End date of contract duration',
  `mobilized_date` date DEFAULT NULL COMMENT 'Departure date from local (Philippines)',
  `mobilized_remarks` text COMMENT 'Remarks about the departure from local',
  `demobilized_date` date DEFAULT NULL COMMENT 'Arrival date from local',
  `demobilized_remarks` text COMMENT 'Remarks about the departure from overseas',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the manpower mobilization';

-- --------------------------------------------------------

--
-- Table structure for table `progress_report_remittances`
--

CREATE TABLE IF NOT EXISTS `progress_report_remittances` (
  `report_remitt_id` int(11) NOT NULL COMMENT 'Primary key; Unique identifer',
  `progress_report_id` int(10) unsigned NOT NULL COMMENT 'Refers to progress report',
  `remitt_date` date NOT NULL COMMENT 'Date of remittance',
  `remitt_amount` decimal(25,2) NOT NULL COMMENT 'Amount remitted in dollars',
  `country` varchar(255) NOT NULL COMMENT 'Country of origin',
  `receiving_bank` varchar(255) NOT NULL COMMENT 'Receiving bank',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the submitted remittances per report';

-- --------------------------------------------------------

--
-- Table structure for table `registration`
--

CREATE TABLE IF NOT EXISTS `registration` (
  `registration_no` varchar(20) NOT NULL COMMENT 'Registration number issued by POCB; Primary key; Unique identifier',
  `contractor_id` int(10) unsigned NOT NULL COMMENT 'Refers to contractor',
  `registration_date` date NOT NULL COMMENT 'Date when the registration was issued',
  `expiry_date` date NOT NULL COMMENT 'Expiration date of the registration',
  `last_renewal_date` date DEFAULT NULL COMMENT 'Last renewal date',
  `registration_status_id` int(10) unsigned NOT NULL COMMENT 'Refers to status of registration'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the registration detail of the contractor';

-- --------------------------------------------------------

--
-- Table structure for table `requests`
--

CREATE TABLE IF NOT EXISTS `requests` (
  `request_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `requested_date` date NOT NULL COMMENT 'Date of application or request',
  `request_type_id` int(10) unsigned NOT NULL COMMENT 'Refers to type of request',
  `reference_no` varchar(100) DEFAULT NULL COMMENT 'Reference number of application; This will be assigned by POCB',
  `reference_name` varchar(255) NOT NULL COMMENT 'Name of request; It''s either contractor name of project name based on the selected type of request',
  `request_status_id` int(10) unsigned NOT NULL COMMENT 'Refers to status of request',
  `pocb_registration_no` varchar(20) DEFAULT NULL COMMENT 'Refers to registration number issued by POCB; ',
  `equip_serial_no` varchar(100) DEFAULT NULL,
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Stores all requests of user';

--
-- Dumping data for table `requests`
--

INSERT INTO `requests` (`request_id`, `requested_date`, `request_type_id`, `reference_no`, `reference_name`, `request_status_id`, `pocb_registration_no`, `equip_serial_no`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(1, '2012-12-12', 1, '1', 'alele name', 1, '1', '1', 0, '2016-12-12 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `request_attachments`
--

CREATE TABLE IF NOT EXISTS `request_attachments` (
  `attachment_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `request_id` int(10) unsigned NOT NULL COMMENT 'Refers to request',
  `checklist_id` int(10) unsigned NOT NULL COMMENT 'Refers to checklist',
  `caption` varchar(255) DEFAULT NULL COMMENT 'Caption about the file attachment',
  `file_name` varchar(255) NOT NULL COMMENT 'Name of file',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores all attached documents per checklist';

-- --------------------------------------------------------

--
-- Table structure for table `request_aut_projects`
--

CREATE TABLE IF NOT EXISTS `request_aut_projects` (
  `rqst_aut_proj_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `request_id` int(10) unsigned NOT NULL COMMENT 'Refers to request',
  `project_title` varchar(255) DEFAULT NULL COMMENT 'Title of the project',
  `project_location` varchar(255) DEFAULT NULL COMMENT 'Location of the project',
  `province` varchar(255) DEFAULT NULL COMMENT 'Province where the project was located',
  `country_code` varchar(10) NOT NULL COMMENT 'Refers to country where the project was located',
  `project_description` text COMMENT 'General Description of the project',
  `scope_work` text COMMENT 'General scope of services',
  `contract_price` decimal(25,2) DEFAULT NULL COMMENT 'Estimated contract price',
  `contract_duration` varchar(255) DEFAULT NULL COMMENT 'Contract duration',
  `est_min_manpower_required` int(5) DEFAULT NULL COMMENT 'Estimated minumum manpower required',
  `est_max_manpower_required` int(5) DEFAULT NULL COMMENT 'Estimated maximum manpower required',
  `outward_amount` decimal(25,2) DEFAULT NULL COMMENT 'Outward amunt of Foreign remittances',
  `inward_amount` decimal(25,2) DEFAULT NULL COMMENT 'Inward amunt of Foreign remittances',
  `procument_type` enum('B','N') NOT NULL DEFAULT 'B' COMMENT 'Construction Procurement; B- Biddingl; N - Negotiation',
  `contract_involvement` enum('P','S','O') NOT NULL DEFAULT 'P' COMMENT 'Contract involvement; P - Prime contractor; S - SUb-contractor; O - Others',
  `prime_contractor_name` varchar(255) DEFAULT NULL COMMENT 'Indicate Name of prime contractor if the contract involvement is sub-contractor',
  `prime_contractor_nationality` varchar(3) DEFAULT NULL COMMENT 'Naitonality of of prime contractor',
  `other_contract_involvement` varchar(255) NOT NULL COMMENT 'Specify the contract involvement ',
  `bidding_date` date DEFAULT NULL COMMENT 'Date of bidding/negotiation',
  `bidding_place` varchar(255) DEFAULT NULL COMMENT 'Place of bidding/negotiation',
  `contract_type_id` int(10) unsigned NOT NULL COMMENT 'Refers to contract type',
  `other_filipino_companies` varchar(255) DEFAULT NULL COMMENT 'Other filipino companies bidding/negotiations for the project',
  `prime_fin_assistance_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Tagging if the financial assistance/guarantee to be secured from Prime contractor',
  `other_fin_assistance` varchar(255) DEFAULT NULL COMMENT 'Specify the financial assistance/guarantee to be secured from',
  `financed_by` varchar(255) DEFAULT NULL COMMENT 'Projects to be financed by',
  `bid_bond` decimal(25,2) DEFAULT NULL COMMENT 'Bid bond',
  `performance_bond` decimal(25,2) DEFAULT NULL COMMENT 'Performance bond',
  `payment_guarantee` decimal(25,2) DEFAULT NULL COMMENT 'Payment guarantee',
  `retentions` varchar(100) DEFAULT NULL COMMENT 'Retentions',
  `progress_billing` varchar(100) DEFAULT NULL COMMENT 'Progress billings',
  `others` varchar(100) DEFAULT NULL COMMENT 'Others financial terms and conditions',
  `owner` varchar(255) DEFAULT NULL COMMENT 'Name of owner',
  `owner_address` text COMMENT 'Address of owner',
  `owner_background` text COMMENT 'Background of owner'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the project contract details';

-- --------------------------------------------------------

--
-- Table structure for table `request_aut_services`
--

CREATE TABLE IF NOT EXISTS `request_aut_services` (
  `rqst_aut_svc_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `request_id` int(10) unsigned NOT NULL COMMENT 'Refers to request',
  `project_title` varchar(255) DEFAULT NULL COMMENT 'Title of the project',
  `project_location` varchar(255) DEFAULT NULL COMMENT 'Location of the project',
  `province` varchar(255) DEFAULT NULL COMMENT 'Province where the project was located',
  `country_code` varchar(10) NOT NULL COMMENT 'Refers to country where the project was located',
  `project_description` text COMMENT 'General Description of the project',
  `service_scope` text COMMENT 'General scope of services',
  `contract_price` decimal(25,2) DEFAULT NULL COMMENT 'Estimated contract price',
  `contract_duration` varchar(255) DEFAULT NULL COMMENT 'Contract duration',
  `est_manpower_required` int(5) DEFAULT NULL COMMENT 'Estimated manpower required',
  `bidding_date` date DEFAULT NULL COMMENT 'Date of bidding/negotiation',
  `bidding_place` varchar(255) DEFAULT NULL COMMENT 'Place of bidding/negotiation',
  `financed_by` varchar(255) DEFAULT NULL COMMENT 'Project to be financed by',
  `guarantees_required` varchar(255) DEFAULT NULL COMMENT 'Guarantess required',
  `foreign_principal_name` varchar(255) DEFAULT NULL COMMENT 'Name of foreign principal name',
  `foreign_principal_address` text COMMENT 'Address of foreign principal\n',
  `nationality_code` varchar(3) NOT NULL COMMENT 'Refers to nationality of foreign principal'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the manpower service contract details';

-- --------------------------------------------------------

--
-- Table structure for table `request_checklist`
--

CREATE TABLE IF NOT EXISTS `request_checklist` (
  `request_id` int(10) unsigned NOT NULL COMMENT 'Refers to request',
  `checklist_id` int(10) unsigned NOT NULL COMMENT 'Refers to checklist',
  `complied_flag` enum('Y','N') NOT NULL DEFAULT 'N' COMMENT 'Tagging if the checklist was complied by the contractor',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the checklist per request';

-- --------------------------------------------------------

--
-- Table structure for table `request_contractor`
--

CREATE TABLE IF NOT EXISTS `request_contractor` (
  `request_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `contractor_name` varchar(255) NOT NULL COMMENT 'Name of company',
  `contractor_address` text NOT NULL COMMENT 'Address of the company',
  `sec_reg_no` varchar(100) NOT NULL COMMENT 'SEC Registration No,',
  `sec_reg_date` date NOT NULL COMMENT 'Date of the Certificate of Registration',
  `contractor_category_id` int(10) unsigned NOT NULL COMMENT 'Refers to category of contractor',
  `license_no` varchar(100) DEFAULT NULL COMMENT 'Contractor''s License No issued by the PCAB',
  `license_date` date DEFAULT NULL COMMENT 'Date of issue of PCAB license',
  `bir_clearance_date` date NOT NULL COMMENT 'Date when the BIR clearance was issued',
  `customs_clearance_date` date NOT NULL COMMENT 'Date when customs clearance was issued',
  `court_clearance_date` date NOT NULL COMMENT 'Date when the court clearance was issued',
  `tel_no` varchar(255) DEFAULT NULL COMMENT 'Telephone number of contractor',
  `fax_no` varchar(100) DEFAULT NULL COMMENT 'fax no of contractor',
  `email` varchar(255) DEFAULT NULL COMMENT 'email of contractor',
  `website` varchar(255) DEFAULT NULL COMMENT 'Website of contractor',
  `rep_first_name` varchar(100) NOT NULL COMMENT 'First name of authorized representative',
  `rep_mid_name` varchar(100) DEFAULT NULL COMMENT 'Middle name of authorized representative',
  `rep_last_name` varchar(100) NOT NULL COMMENT 'Last name of authorized representative',
  `rep_address` text NOT NULL COMMENT 'Address of authorized representative',
  `rep_contact_no` varchar(100) NOT NULL COMMENT 'Contact number of authorized representative',
  `rep_email` varchar(100) NOT NULL COMMENT 'Email of authorized representative',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the contractor detail when filing the request\n';

-- --------------------------------------------------------

--
-- Table structure for table `request_contracts`
--

CREATE TABLE IF NOT EXISTS `request_contracts` (
  `rqst_contract_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifer',
  `request_id` int(10) unsigned NOT NULL COMMENT 'Refers to request',
  `contract_type` enum('P','M') NOT NULL DEFAULT 'M' COMMENT 'Type of Contracts; P - Project; M - Manpower Service',
  `base` enum('L','O') NOT NULL DEFAULT 'O' COMMENT 'L - Local; O - Overseas',
  `with_authorization_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Tagging if the contract was filed for POCB authorization',
  `project_title` varchar(255) NOT NULL COMMENT 'Title of project',
  `project_location` varchar(255) DEFAULT NULL COMMENT 'Location of project',
  `province` varchar(255) DEFAULT NULL COMMENT 'Province where the project was located',
  `country_code` varchar(10) NOT NULL COMMENT 'Refers to country where project was located',
  `contract_status_id` int(10) unsigned NOT NULL COMMENT 'Refers to status of contract',
  `project_cost` decimal(25,2) DEFAULT NULL COMMENT 'overall project construction contract cost',
  `contract_cost` decimal(25,2) DEFAULT NULL COMMENT 'Amount of contract handled by Contractor, it could be less than the project cost for sub-contractor',
  `owner` varchar(255) DEFAULT NULL COMMENT 'Name of owner',
  `contract_duration` varchar(100) DEFAULT NULL COMMENT 'Duration of contract',
  `started_date` date DEFAULT NULL COMMENT 'Official start date of the contract',
  `completed_date` date DEFAULT NULL COMMENT 'Official completion date of the contract',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores all contracts made by a contractor';

-- --------------------------------------------------------

--
-- Table structure for table `request_documents`
--

CREATE TABLE IF NOT EXISTS `request_documents` (
  `rqst_doc_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `request_id` int(10) unsigned NOT NULL COMMENT 'Refers to request',
  `doc_type_id` int(10) unsigned NOT NULL COMMENT 'Refers to type of document',
  `file_name` varchar(255) NOT NULL COMMENT 'Name of file',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `request_equipment`
--

CREATE TABLE IF NOT EXISTS `request_equipment` (
  `rqst_equip_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `serial_no` varchar(100) NOT NULL COMMENT 'Equipment code  number as taken from the company''s equipment inventory report',
  `request_id` int(10) unsigned NOT NULL COMMENT 'Refers to request',
  `equipment_type` varchar(255) NOT NULL COMMENT 'Type of equipment',
  `capacity` varchar(255) DEFAULT NULL COMMENT 'Description should give the equipment''s common name or its main functional use. Size/capacity shuld describe further the equipment by giving its functional horsepower, rated capacity, etc.',
  `brand` text COMMENT 'Describes the equipment''s brand/model/series number as specified by the equipment''s manufacturer',
  `model` varchar(255) DEFAULT NULL COMMENT 'Model of the equipment',
  `acquired_date` date DEFAULT NULL COMMENT 'Date when the equipment was acquired by the contractor',
  `acquisition_cost` decimal(25,2) DEFAULT NULL COMMENT 'Book value/Acquisition cost',
  `present_condition` text COMMENT 'Present condition of equipment',
  `location` varchar(255) DEFAULT NULL COMMENT 'Equipment''s present location',
  `for_registration_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Tagging if the equipment is for registration',
  `motor_no` varchar(100) DEFAULT NULL COMMENT 'Motor no; Applicable if the equipment is for registration',
  `color` varchar(100) DEFAULT NULL COMMENT 'Color of the equipment; Applicable if the equipment is for registration',
  `year` int(4) DEFAULT NULL COMMENT 'Year of equipment; Applicable if the equipment is for registration',
  `flywheel` varchar(100) DEFAULT NULL COMMENT 'Flywheel; Applicable if the equipment is for registration',
  `acquired_from` varchar(100) DEFAULT NULL COMMENT 'Acquired from; Applicable if the equipment is for registration',
  `ownership_type` varchar(45) DEFAULT NULL COMMENT 'Type of ownership; Applicable if the equipment is for registration',
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the construction equipment currently owned by contractor';

-- --------------------------------------------------------

--
-- Table structure for table `request_evaluation`
--

CREATE TABLE IF NOT EXISTS `request_evaluation` (
  `request_id` int(10) unsigned NOT NULL,
  `legal_requirement` enum('C','I','N') NOT NULL DEFAULT 'N' COMMENT 'C - Completed; I - Incompleted; N - Not yet defined',
  `total_actual_constr_year` int(3) DEFAULT NULL,
  `total_completed_contracts` int(3) DEFAULT NULL,
  `total_rescinded_contracts` int(3) DEFAULT NULL,
  `work_experience` enum('C','I','N') NOT NULL DEFAULT 'N' COMMENT 'C - Completed; I - Incompleted; N - Not yet defined',
  `total_staff` int(5) DEFAULT NULL,
  `staff_reqmt` enum('C','I','N') NOT NULL DEFAULT 'N' COMMENT 'C - Completed; I - Incompleted; N - Not yet defined',
  `committee_recommend` enum('A','D','N') NOT NULL DEFAULT 'N' COMMENT 'A - Approved; D - Disapproved; N - Not yet defined'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `request_license_classifications`
--

CREATE TABLE IF NOT EXISTS `request_license_classifications` (
  `request_id` int(10) unsigned NOT NULL COMMENT 'Refers to request',
  `license_classification_code` varchar(15) NOT NULL COMMENT 'Refers to license classification'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the license classifications per request';

-- --------------------------------------------------------

--
-- Table structure for table `request_payments`
--

CREATE TABLE IF NOT EXISTS `request_payments` (
  `rqst_payment_id` int(11) NOT NULL COMMENT 'Primary key; unique identifier',
  `request_id` int(10) unsigned NOT NULL COMMENT 'Refesr to request',
  `op_date` date NOT NULL COMMENT 'Date of Order of Payment',
  `op_amount` decimal(25,2) NOT NULL DEFAULT '0.00' COMMENT 'Amount indicated on the order of payment',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores all payments made by the contractor';

-- --------------------------------------------------------

--
-- Table structure for table `request_payment_collections`
--

CREATE TABLE IF NOT EXISTS `request_payment_collections` (
  `rqst_payment_id` int(11) NOT NULL COMMENT 'Refers to order of payment',
  `coll_nature_code` varchar(100) NOT NULL COMMENT 'Refers to nature of collection',
  `amount` varchar(45) NOT NULL COMMENT 'Amount '
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores all nature of collections per order of payment';

-- --------------------------------------------------------

--
-- Table structure for table `request_projects`
--

CREATE TABLE IF NOT EXISTS `request_projects` (
  `rqst_contract_id` int(10) unsigned NOT NULL COMMENT 'Refers to contract',
  `project_type_code` varchar(10) NOT NULL COMMENT 'Refers to type of project',
  `involvement_code` varchar(1) NOT NULL COMMENT 'Refers to contact involvement',
  `scheduled_percent` decimal(5,2) DEFAULT NULL COMMENT 'scheduled percentage of accomplishment ',
  `actual_percent` decimal(5,2) DEFAULT NULL COMMENT 'Actual percentage of accomplishment ',
  `remarks` text COMMENT 'Remarks about the project'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the project contracts';

-- --------------------------------------------------------

--
-- Table structure for table `request_registration`
--

CREATE TABLE IF NOT EXISTS `request_registration` (
  `request_id` int(10) unsigned NOT NULL,
  `registration_no` varchar(20) NOT NULL COMMENT 'Registration number issued by POCB; Primary key; Unique identifier',
  `registration_date` date NOT NULL COMMENT 'Date when the registration was issued',
  `expiry_date` date NOT NULL COMMENT 'Expiration date of the registration',
  `last_renewal_date` date DEFAULT NULL COMMENT 'Last renewal date'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the registration detail of the contractor';

-- --------------------------------------------------------

--
-- Table structure for table `request_schedule`
--

CREATE TABLE IF NOT EXISTS `request_schedule` (
  `rqst_sched_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `request_id` int(10) unsigned NOT NULL COMMENT 'Refers to request',
  `category` enum('D','I') NOT NULL DEFAULT 'D' COMMENT 'Category of schedule; D - Direct (Execution); I - Indirect (Administrative)',
  `skills_description` varchar(255) DEFAULT NULL COMMENT 'Desription of skills',
  `salary_rate` decimal(25,2) DEFAULT NULL COMMENT 'Salary rate of skills',
  `number_required` int(4) DEFAULT NULL COMMENT 'Number required of skills',
  `employment_contract` varchar(100) DEFAULT NULL COMMENT 'Employment contract ',
  `monthly_breakdown` varchar(100) DEFAULT NULL COMMENT 'Monthly breakdown',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the manpower schedule of manpower service contract';

-- --------------------------------------------------------

--
-- Table structure for table `request_services`
--

CREATE TABLE IF NOT EXISTS `request_services` (
  `rqst_contract_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `foreign_principal_name` varchar(255) NOT NULL COMMENT 'Name of foreign principal',
  `nationality_code` varchar(3) NOT NULL COMMENT 'Refers to nationality of foreign principal',
  `required_manpower` int(5) DEFAULT NULL COMMENT 'Required manpower',
  `mobilized_manpower` int(5) DEFAULT NULL COMMENT 'Mobilized manpower',
  `onsite_manpower` int(5) DEFAULT NULL COMMENT 'Onsite manpower',
  `fees` decimal(25,2) DEFAULT NULL COMMENT 'Service fee and others',
  `workers_salaries` decimal(25,2) DEFAULT NULL COMMENT 'Workers salaries',
  `remarks` text COMMENT 'Remarks'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the manpower service contract';

-- --------------------------------------------------------

--
-- Table structure for table `request_staff`
--

CREATE TABLE IF NOT EXISTS `request_staff` (
  `rqst_staff_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `request_id` int(10) unsigned NOT NULL COMMENT 'Refers to request',
  `staff_type` enum('D','P','T','W') NOT NULL DEFAULT 'T' COMMENT 'D - Directors; P - Principal Officers; T - Key Technical Staff, W - Workers',
  `first_name` varchar(100) NOT NULL COMMENT 'First name of staff',
  `middle_name` varchar(100) DEFAULT NULL COMMENT 'Middle name of staff',
  `last_name` varchar(100) NOT NULL COMMENT 'Last name of staff',
  `address` text NOT NULL COMMENT 'Address of staff',
  `birth_date` date NOT NULL COMMENT 'Date of birth',
  `birth_place` text COMMENT 'Place of birth',
  `educational_attainment` varchar(255) NOT NULL COMMENT 'Highest educational attainment',
  `school_attended` varchar(255) NOT NULL COMMENT 'Schooll/University attended',
  `year_graduated` int(4) NOT NULL COMMENT 'Year graduated',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Stores the key management/technical personnel employed by the contractor';

--
-- Dumping data for table `request_staff`
--

INSERT INTO `request_staff` (`rqst_staff_id`, `request_id`, `staff_type`, `first_name`, `middle_name`, `last_name`, `address`, `birth_date`, `birth_place`, `educational_attainment`, `school_attended`, `year_graduated`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(1, 1, 'D', 'alele name', 'alele middle', 'alele last', 'alele address', '2016-12-12', NULL, '', '', 0, 0, '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `request_staff_experiences`
--

CREATE TABLE IF NOT EXISTS `request_staff_experiences` (
  `rqst_staff_exp_id` int(10) unsigned NOT NULL COMMENT 'Primary key; Unique identifier',
  `rqst_staff_id` int(10) unsigned NOT NULL COMMENT 'Refers to staff',
  `present_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Tagging if the work is the current work',
  `start_date` date NOT NULL COMMENT 'Start date working on the company',
  `end_date` date DEFAULT NULL COMMENT 'End date working on the company',
  `company_name` varchar(255) NOT NULL COMMENT 'Name of company',
  `company_address` text NOT NULL COMMENT 'Address of the company',
  `job_description` text COMMENT 'Job description on the company',
  `stockholder_flag` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Tagging if the staff is a stockholder',
  `paid_in_amount` decimal(25,2) NOT NULL DEFAULT '0.00' COMMENT 'Paid-in amount',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modifed_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the work experiences of staff';

-- --------------------------------------------------------

--
-- Table structure for table `request_staff_licenses`
--

CREATE TABLE IF NOT EXISTS `request_staff_licenses` (
  `license_no` varchar(100) NOT NULL COMMENT 'PRC License number; Unique identifier',
  `rqst_staff_id` int(10) unsigned NOT NULL COMMENT 'Refers to staff',
  `profession` varchar(255) NOT NULL COMMENT 'Profession taken',
  `issued_date` date NOT NULL COMMENT 'Date when the license was issued',
  `expiry_date` date NOT NULL COMMENT 'Expiration date of license'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the professional licenses of staff';

-- --------------------------------------------------------

--
-- Table structure for table `request_staff_projects`
--

CREATE TABLE IF NOT EXISTS `request_staff_projects` (
  `rqst_staff_proj_id` int(10) unsigned NOT NULL COMMENT 'Primary key; unique identifier',
  `rqst_staff_id` int(10) unsigned NOT NULL COMMENT 'Refers to staff',
  `project_title` varchar(255) NOT NULL COMMENT 'Title of project',
  `project_location` text COMMENT 'Location of the project',
  `owner` varchar(255) DEFAULT NULL COMMENT 'Owner of the project',
  `positions` varchar(255) NOT NULL COMMENT 'Positions of the staff in project',
  `project_type_code` varchar(10) NOT NULL COMMENT 'Refers to type of project',
  `start_date` date DEFAULT NULL COMMENT 'Start date of project involvement',
  `end_date` date DEFAULT NULL COMMENT 'End date of project involvement',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='Stores the major pojects handled by staff';

--
-- Dumping data for table `request_staff_projects`
--

INSERT INTO `request_staff_projects` (`rqst_staff_proj_id`, `rqst_staff_id`, `project_title`, `project_location`, `owner`, `positions`, `project_type_code`, `start_date`, `end_date`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
(6, 1, 'alele title', 'alele location', 'alele', 'alele', 'alele', '1202-12-12', '2016-12-12', 0, '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `request_statements`
--

CREATE TABLE IF NOT EXISTS `request_statements` (
  `request_id` int(10) unsigned NOT NULL COMMENT 'Refers to request',
  `statement_date` date NOT NULL COMMENT 'Date of Statement',
  `stmt_type` enum('A','I') NOT NULL DEFAULT 'A' COMMENT 'Type of statement; A - Audited; I - Interim',
  `total_current_assets` decimal(25,2) NOT NULL DEFAULT '0.00' COMMENT 'Total current assets',
  `total_non_current_assets` decimal(25,2) NOT NULL DEFAULT '0.00',
  `total_current_liabilities` decimal(25,2) NOT NULL DEFAULT '0.00' COMMENT 'Total current liaibilities',
  `long_term_liabilities` decimal(25,2) NOT NULL DEFAULT '0.00' COMMENT 'Long term liabilities',
  `networth` decimal(25,2) NOT NULL DEFAULT '0.00' COMMENT 'Net worth',
  `gross_sales` decimal(25,2) NOT NULL DEFAULT '0.00' COMMENT 'Gross sales/revenues',
  `net_profit` decimal(25,2) NOT NULL DEFAULT '0.00' COMMENT 'Net profit after tax',
  `common_shares` decimal(25,2) NOT NULL DEFAULT '0.00' COMMENT 'No. of common shares outstanding',
  `par_value` int(5) NOT NULL DEFAULT '0' COMMENT 'Par value per common share',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores the audited financial statements of contractor';

-- --------------------------------------------------------

--
-- Table structure for table `request_tasks`
--

CREATE TABLE IF NOT EXISTS `request_tasks` (
  `request_task_id` int(11) NOT NULL,
  `sequence_no` int(11) NOT NULL,
  `request_id` int(10) unsigned NOT NULL,
  `task_name` varchar(255) NOT NULL,
  `role_code` varchar(100) NOT NULL,
  `generate_reference_flag` tinyint(1) NOT NULL DEFAULT '0',
  `get_task_flag` tinyint(1) NOT NULL DEFAULT '0',
  `assigned_to` int(11) DEFAULT NULL,
  `start_date` datetime DEFAULT NULL,
  `end_date` datetime DEFAULT NULL,
  `task_status_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `request_task_attachments`
--

CREATE TABLE IF NOT EXISTS `request_task_attachments` (
  `task_attachment_id` int(10) unsigned NOT NULL,
  `request_task_id` int(11) NOT NULL,
  `request_id` int(10) unsigned NOT NULL,
  `received_date` date NOT NULL,
  `attachment_type_id` int(11) NOT NULL,
  `description` text NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` varchar(45) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `request_task_remarks`
--

CREATE TABLE IF NOT EXISTS `request_task_remarks` (
  `task_remark_id` int(10) unsigned NOT NULL,
  `request_task_id` int(11) NOT NULL,
  `request_id` int(10) unsigned NOT NULL,
  `remarks` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_date` datetime NOT NULL,
  `modified_by` int(11) DEFAULT NULL,
  `modified_date` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `resolutions`
--

CREATE TABLE IF NOT EXISTS `resolutions` (
  `resolution_no` varchar(100) NOT NULL COMMENT 'Resolution No.; Primary key; Unique identifier',
  `resoulution_date` date NOT NULL COMMENT 'Date of Board Resolution',
  `title` varchar(255) NOT NULL COMMENT 'Title of resolution',
  `first_round` date NOT NULL COMMENT 'Date of first round of meeting',
  `last_round` date NOT NULL COMMENT 'Date of last round of meeting',
  `offers` text NOT NULL COMMENT 'Offers ',
  `commitments` text NOT NULL COMMENT 'Commitments',
  `created_by` int(11) NOT NULL COMMENT 'Refers to user who created the record',
  `created_date` datetime NOT NULL COMMENT 'Date when the record was created',
  `modified_by` int(11) DEFAULT NULL COMMENT 'Refers to user who modified the record',
  `modified_date` timestamp NULL DEFAULT NULL COMMENT 'Date when the record was modified'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Stores all board resolutions pertaining to international negotiations/agreements';

--
-- Dumping data for table `resolutions`
--

INSERT INTO `resolutions` (`resolution_no`, `resoulution_date`, `title`, `first_round`, `last_round`, `offers`, `commitments`, `created_by`, `created_date`, `modified_by`, `modified_date`) VALUES
('09', '2016-05-18', 'zero nine ten', '2016-05-16', '2016-05-18', 'zero nine ten', 'zero nine ten', 0, '0000-00-00 00:00:00', NULL, '2016-05-10 05:08:26'),
('12', '2016-05-25', 'sxsasas', '2016-05-18', '2016-05-27', 'r', 'f', 0, '0000-00-00 00:00:00', NULL, NULL),
('123121', '1900-11-27', '27', '1900-12-27', '1900-12-27', '27', '27', 0, '0000-00-00 00:00:00', NULL, '2016-05-05 09:21:15'),
('13444546456', '2016-05-21', 'lolo', '2016-05-20', '2016-05-25', 'lolo', 'lolo', 0, '0000-00-00 00:00:00', NULL, NULL),
('31232', '2016-05-11', '32ew', '2016-05-23', '2016-06-02', 'ewed', 'dcdwed', 0, '0000-00-00 00:00:00', NULL, NULL),
('435345', '2016-05-02', 'aelele pong pa more alele', '2016-05-25', '2016-05-28', 'alulu', 'alulu', 0, '0000-00-00 00:00:00', NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contractor`
--
ALTER TABLE `contractor`
  ADD PRIMARY KEY (`contractor_id`),
  ADD UNIQUE KEY `company_name_UNIQUE` (`contractor_name`),
  ADD KEY `fk_request_contractor_param_license_categories1_idx` (`contractor_category_id`),
  ADD KEY `fk_request_contractor_requests1_idx` (`contractor_id`);

--
-- Indexes for table `contractor_contracts`
--
ALTER TABLE `contractor_contracts`
  ADD PRIMARY KEY (`contract_id`),
  ADD KEY `fk_contractor_contracts_param_countries1_idx` (`country_code`),
  ADD KEY `fk_contractor_contracts_param_contract_status1_idx` (`contract_status_id`),
  ADD KEY `fk_contractor_contracts_contractor1_idx` (`contractor_id`);

--
-- Indexes for table `contractor_contracts_schedule`
--
ALTER TABLE `contractor_contracts_schedule`
  ADD PRIMARY KEY (`contract_id`),
  ADD KEY `fk_contractor_contracts_schedule_contractor_contracts1_idx` (`contract_id`);

--
-- Indexes for table `contractor_equipment`
--
ALTER TABLE `contractor_equipment`
  ADD PRIMARY KEY (`serial_no`);

--
-- Indexes for table `contractor_projects`
--
ALTER TABLE `contractor_projects`
  ADD PRIMARY KEY (`contract_id`),
  ADD KEY `fk_request_projects_param_project_types1_idx` (`project_type_code`),
  ADD KEY `fk_contractor_projects_contractor_contracts1_idx` (`contract_id`),
  ADD KEY `fk_contractor_projects_param_contract_involvement1_idx` (`involvement_code`);

--
-- Indexes for table `contractor_project_dtl`
--
ALTER TABLE `contractor_project_dtl`
  ADD PRIMARY KEY (`contract_id`),
  ADD KEY `fk_request_aut_projects_param_nationalities1_idx` (`prime_contractor_nationality`),
  ADD KEY `fk_request_aut_projects_param_contract_types1_idx` (`contract_type_id`),
  ADD KEY `fk_request_aut_projects_copy1_contractor_contracts1_idx` (`contract_id`);

--
-- Indexes for table `contractor_services`
--
ALTER TABLE `contractor_services`
  ADD PRIMARY KEY (`contract_id`),
  ADD KEY `fk_request_services_param_nationalities1_idx` (`nationality_code`),
  ADD KEY `fk_contractor_services_contractor_contracts1_idx` (`contract_id`);

--
-- Indexes for table `contractor_services_dtl`
--
ALTER TABLE `contractor_services_dtl`
  ADD PRIMARY KEY (`contract_id`),
  ADD KEY `fk_contractor_services_dtl_contractor_contracts1_idx` (`contract_id`);

--
-- Indexes for table `contractor_staff`
--
ALTER TABLE `contractor_staff`
  ADD PRIMARY KEY (`contr_staff_id`),
  ADD KEY `fk_contractor_staff_contractor1_idx` (`contractor_id`);

--
-- Indexes for table `contractor_staff_experiences`
--
ALTER TABLE `contractor_staff_experiences`
  ADD PRIMARY KEY (`contr_staff_exp_id`),
  ADD KEY `fk_contractor_staff_experiences_contractor_staff1_idx` (`contr_staff_id`);

--
-- Indexes for table `contractor_staff_licenses`
--
ALTER TABLE `contractor_staff_licenses`
  ADD PRIMARY KEY (`license_no`),
  ADD KEY `fk_contractor_staff_licenses_contractor_staff1_idx` (`contr_staff_id`);

--
-- Indexes for table `contractor_staff_projects`
--
ALTER TABLE `contractor_staff_projects`
  ADD PRIMARY KEY (`contr_staff_proj_id`),
  ADD KEY `fk_request_staff_projects_param_project_types1_idx` (`project_type_code`),
  ADD KEY `fk_contractor_staff_projects_contractor_staff1_idx` (`contr_staff_id`);

--
-- Indexes for table `contractor_statements`
--
ALTER TABLE `contractor_statements`
  ADD PRIMARY KEY (`contractor_id`,`statement_date`),
  ADD KEY `fk_contractor_statements_contractor1_idx` (`contractor_id`);

--
-- Indexes for table `contractor_work_volume`
--
ALTER TABLE `contractor_work_volume`
  ADD PRIMARY KEY (`year`,`contractor_id`),
  ADD KEY `fk_request_work_volume_copy1_contractor1_idx` (`contractor_id`);

--
-- Indexes for table `country_profiles`
--
ALTER TABLE `country_profiles`
  ADD PRIMARY KEY (`country_code`),
  ADD KEY `fk_country_profiles_param_countries1_idx` (`country_code`);

--
-- Indexes for table `opportunities`
--
ALTER TABLE `opportunities`
  ADD PRIMARY KEY (`opportunity_id`),
  ADD KEY `fk_opportunities_param_opportunity_sources1_idx` (`opp_source_id`),
  ADD KEY `fk_opportunities_param_opportunity_status1_idx` (`opp_status_id`);

--
-- Indexes for table `opportunity_recipients`
--
ALTER TABLE `opportunity_recipients`
  ADD PRIMARY KEY (`recipient_id`),
  ADD KEY `fk_opportunity_recipients_opportunities1_idx` (`opportunity_id`);

--
-- Indexes for table `param_attachment_types`
--
ALTER TABLE `param_attachment_types`
  ADD PRIMARY KEY (`attachment_type_id`);

--
-- Indexes for table `param_checklist`
--
ALTER TABLE `param_checklist`
  ADD PRIMARY KEY (`checklist_id`),
  ADD KEY `fk_param_request_checklist_param_request_types1_idx` (`request_type_id`);

--
-- Indexes for table `param_collection_natures`
--
ALTER TABLE `param_collection_natures`
  ADD PRIMARY KEY (`coll_nature_code`);

--
-- Indexes for table `param_contractor_categories`
--
ALTER TABLE `param_contractor_categories`
  ADD PRIMARY KEY (`contractor_category_id`);

--
-- Indexes for table `param_contract_involvement`
--
ALTER TABLE `param_contract_involvement`
  ADD PRIMARY KEY (`involvement_code`);

--
-- Indexes for table `param_contract_status`
--
ALTER TABLE `param_contract_status`
  ADD PRIMARY KEY (`contract_status_id`),
  ADD UNIQUE KEY `contract_status_name_UNIQUE` (`contract_status_name`);

--
-- Indexes for table `param_contract_types`
--
ALTER TABLE `param_contract_types`
  ADD PRIMARY KEY (`contract_type_id`);

--
-- Indexes for table `param_countries`
--
ALTER TABLE `param_countries`
  ADD PRIMARY KEY (`country_code`);

--
-- Indexes for table `param_document_types`
--
ALTER TABLE `param_document_types`
  ADD PRIMARY KEY (`doc_type_id`);

--
-- Indexes for table `param_geo_regions`
--
ALTER TABLE `param_geo_regions`
  ADD PRIMARY KEY (`geo_region_id`);

--
-- Indexes for table `param_geo_region_countries`
--
ALTER TABLE `param_geo_region_countries`
  ADD KEY `fk_param_geo_region_countries_param_countries1_idx` (`country_code`),
  ADD KEY `fk_param_geo_region_countries_param_geo_regions1_idx` (`geo_region_id`);

--
-- Indexes for table `param_license_classifications`
--
ALTER TABLE `param_license_classifications`
  ADD PRIMARY KEY (`license_classification_code`);

--
-- Indexes for table `param_nationalities`
--
ALTER TABLE `param_nationalities`
  ADD PRIMARY KEY (`nationality_code`),
  ADD KEY `fk_param_nationalities_param_countries1_idx` (`country_code`);

--
-- Indexes for table `param_opportunity_sources`
--
ALTER TABLE `param_opportunity_sources`
  ADD PRIMARY KEY (`opp_source_id`);

--
-- Indexes for table `param_opportunity_status`
--
ALTER TABLE `param_opportunity_status`
  ADD PRIMARY KEY (`opp_status_id`);

--
-- Indexes for table `param_positions`
--
ALTER TABLE `param_positions`
  ADD PRIMARY KEY (`position_id`),
  ADD UNIQUE KEY `position_name_UNIQUE` (`position_name`);

--
-- Indexes for table `param_project_types`
--
ALTER TABLE `param_project_types`
  ADD PRIMARY KEY (`project_type_code`);

--
-- Indexes for table `param_registration_status`
--
ALTER TABLE `param_registration_status`
  ADD PRIMARY KEY (`registration_status_id`);

--
-- Indexes for table `param_request_status`
--
ALTER TABLE `param_request_status`
  ADD PRIMARY KEY (`request_status_id`);

--
-- Indexes for table `param_request_tasks`
--
ALTER TABLE `param_request_tasks`
  ADD PRIMARY KEY (`request_task_id`),
  ADD KEY `fk_param_request_tasks_param_request_types1_idx` (`request_type_id`);

--
-- Indexes for table `param_request_task_status`
--
ALTER TABLE `param_request_task_status`
  ADD PRIMARY KEY (`task_status_id`);

--
-- Indexes for table `param_request_types`
--
ALTER TABLE `param_request_types`
  ADD PRIMARY KEY (`request_type_id`);

--
-- Indexes for table `progress_reports`
--
ALTER TABLE `progress_reports`
  ADD PRIMARY KEY (`progress_report_id`),
  ADD UNIQUE KEY `contract_id_UNIQUE` (`contract_id`),
  ADD UNIQUE KEY `year_UNIQUE` (`year`),
  ADD UNIQUE KEY `quarter_UNIQUE` (`quarter`),
  ADD KEY `fk_progress_reports_contractor_contracts1_idx` (`contract_id`),
  ADD KEY `fk_progress_reports_param_contract_status1_idx` (`contract_status_id`);

--
-- Indexes for table `progress_report_info`
--
ALTER TABLE `progress_report_info`
  ADD PRIMARY KEY (`progress_report_id`);

--
-- Indexes for table `progress_report_manpower`
--
ALTER TABLE `progress_report_manpower`
  ADD PRIMARY KEY (`progress_report_id`,`contr_staff_id`),
  ADD KEY `fk_progress_report_manpower_progress_reports1_idx` (`progress_report_id`),
  ADD KEY `fk_progress_report_manpower_contractor_staff1_idx` (`contr_staff_id`),
  ADD KEY `fk_progress_report_manpower_param_positions1_idx` (`position_id`);

--
-- Indexes for table `progress_report_remittances`
--
ALTER TABLE `progress_report_remittances`
  ADD PRIMARY KEY (`report_remitt_id`),
  ADD KEY `fk_progress_report_remittances_progress_reports1_idx` (`progress_report_id`);

--
-- Indexes for table `registration`
--
ALTER TABLE `registration`
  ADD PRIMARY KEY (`registration_no`),
  ADD KEY `fk_registration_param_registration_status1_idx` (`registration_status_id`),
  ADD KEY `fk_registration_contractor1_idx` (`contractor_id`);

--
-- Indexes for table `requests`
--
ALTER TABLE `requests`
  ADD PRIMARY KEY (`request_id`),
  ADD KEY `fk_requests_param_request_status_idx` (`request_status_id`),
  ADD KEY `fk_requests_param_request_types1_idx` (`request_type_id`);

--
-- Indexes for table `request_attachments`
--
ALTER TABLE `request_attachments`
  ADD PRIMARY KEY (`attachment_id`),
  ADD KEY `fk_request_attachments_request_checklist1_idx` (`request_id`,`checklist_id`);

--
-- Indexes for table `request_aut_projects`
--
ALTER TABLE `request_aut_projects`
  ADD PRIMARY KEY (`rqst_aut_proj_id`),
  ADD KEY `fk_request_aut_projects_requests1_idx` (`request_id`),
  ADD KEY `fk_request_aut_projects_param_countries1_idx` (`country_code`),
  ADD KEY `fk_request_aut_projects_param_nationalities1_idx` (`prime_contractor_nationality`),
  ADD KEY `fk_request_aut_projects_param_contract_types1_idx` (`contract_type_id`);

--
-- Indexes for table `request_aut_services`
--
ALTER TABLE `request_aut_services`
  ADD PRIMARY KEY (`rqst_aut_svc_id`),
  ADD KEY `fk_request_aut_projects_requests1_idx` (`request_id`),
  ADD KEY `fk_request_aut_projects_param_countries1_idx` (`country_code`),
  ADD KEY `fk_request_aut_services_param_nationalities1_idx` (`nationality_code`);

--
-- Indexes for table `request_checklist`
--
ALTER TABLE `request_checklist`
  ADD PRIMARY KEY (`request_id`,`checklist_id`),
  ADD KEY `fk_request_checklist_requests1_idx` (`request_id`),
  ADD KEY `fk_request_checklist_param_checklist1_idx` (`checklist_id`);

--
-- Indexes for table `request_contractor`
--
ALTER TABLE `request_contractor`
  ADD PRIMARY KEY (`request_id`),
  ADD KEY `fk_request_contractor_param_license_categories1_idx` (`contractor_category_id`),
  ADD KEY `fk_request_contractor_requests1_idx` (`request_id`);

--
-- Indexes for table `request_contracts`
--
ALTER TABLE `request_contracts`
  ADD PRIMARY KEY (`rqst_contract_id`),
  ADD KEY `fk_contractor_contracts_param_countries1_idx` (`country_code`),
  ADD KEY `fk_contractor_contracts_param_contract_status1_idx` (`contract_status_id`),
  ADD KEY `fk_request_contracts_requests1_idx` (`request_id`);

--
-- Indexes for table `request_documents`
--
ALTER TABLE `request_documents`
  ADD PRIMARY KEY (`rqst_doc_id`),
  ADD KEY `fk_request_documents_param_document_types1_idx` (`doc_type_id`),
  ADD KEY `fk_request_documents_requests1_idx` (`request_id`);

--
-- Indexes for table `request_equipment`
--
ALTER TABLE `request_equipment`
  ADD PRIMARY KEY (`rqst_equip_id`),
  ADD KEY `fk_request_equipment_requests1_idx` (`request_id`);

--
-- Indexes for table `request_evaluation`
--
ALTER TABLE `request_evaluation`
  ADD PRIMARY KEY (`request_id`),
  ADD KEY `fk_request_evaluation_requests1_idx` (`request_id`);

--
-- Indexes for table `request_license_classifications`
--
ALTER TABLE `request_license_classifications`
  ADD PRIMARY KEY (`request_id`,`license_classification_code`),
  ADD KEY `fk_request_license_classifications_requests1_idx` (`request_id`),
  ADD KEY `fk_request_license_classifications_param_license_classifica_idx` (`license_classification_code`);

--
-- Indexes for table `request_payments`
--
ALTER TABLE `request_payments`
  ADD PRIMARY KEY (`rqst_payment_id`),
  ADD KEY `fk_request_payments_requests1_idx` (`request_id`);

--
-- Indexes for table `request_payment_collections`
--
ALTER TABLE `request_payment_collections`
  ADD PRIMARY KEY (`rqst_payment_id`),
  ADD KEY `fk_request_payment_collections_request_payments1_idx` (`rqst_payment_id`),
  ADD KEY `fk_request_payment_collections_param_collection_natures1_idx` (`coll_nature_code`);

--
-- Indexes for table `request_projects`
--
ALTER TABLE `request_projects`
  ADD PRIMARY KEY (`rqst_contract_id`),
  ADD KEY `fk_request_projects_param_project_types1_idx` (`project_type_code`),
  ADD KEY `fk_request_projects_request_contracts1_idx` (`rqst_contract_id`),
  ADD KEY `fk_request_projects_param_contact_involvement1_idx` (`involvement_code`);

--
-- Indexes for table `request_registration`
--
ALTER TABLE `request_registration`
  ADD PRIMARY KEY (`request_id`),
  ADD KEY `fk_request_registration_requests1_idx` (`request_id`);

--
-- Indexes for table `request_schedule`
--
ALTER TABLE `request_schedule`
  ADD PRIMARY KEY (`rqst_sched_id`),
  ADD KEY `fk_request_schedule_requests1_idx` (`request_id`);

--
-- Indexes for table `request_services`
--
ALTER TABLE `request_services`
  ADD PRIMARY KEY (`rqst_contract_id`),
  ADD KEY `fk_request_services_param_nationalities1_idx` (`nationality_code`),
  ADD KEY `fk_request_services_request_contracts1_idx` (`rqst_contract_id`);

--
-- Indexes for table `request_staff`
--
ALTER TABLE `request_staff`
  ADD PRIMARY KEY (`rqst_staff_id`),
  ADD KEY `fk_request_staff_requests1_idx` (`request_id`);

--
-- Indexes for table `request_staff_experiences`
--
ALTER TABLE `request_staff_experiences`
  ADD PRIMARY KEY (`rqst_staff_exp_id`),
  ADD KEY `fk_request_staff_experiences_request_staff1_idx` (`rqst_staff_id`);

--
-- Indexes for table `request_staff_licenses`
--
ALTER TABLE `request_staff_licenses`
  ADD PRIMARY KEY (`license_no`),
  ADD KEY `fk_request_staff_licenses_request_staff1_idx` (`rqst_staff_id`);

--
-- Indexes for table `request_staff_projects`
--
ALTER TABLE `request_staff_projects`
  ADD PRIMARY KEY (`rqst_staff_proj_id`),
  ADD KEY `fk_request_staff_projects_request_staff1_idx` (`rqst_staff_id`),
  ADD KEY `fk_request_staff_projects_param_project_types1_idx` (`project_type_code`);

--
-- Indexes for table `request_statements`
--
ALTER TABLE `request_statements`
  ADD PRIMARY KEY (`request_id`,`statement_date`),
  ADD KEY `fk_request_statements_requests1_idx` (`request_id`);

--
-- Indexes for table `request_tasks`
--
ALTER TABLE `request_tasks`
  ADD PRIMARY KEY (`request_task_id`,`request_id`),
  ADD KEY `fk_param_request_tasks_copy1_requests1_idx` (`request_id`),
  ADD KEY `fk_request_tasks_param_request_task_status1_idx` (`task_status_id`);

--
-- Indexes for table `request_task_attachments`
--
ALTER TABLE `request_task_attachments`
  ADD PRIMARY KEY (`task_attachment_id`),
  ADD KEY `fk_request_task_attachments_request_tasks1_idx` (`request_task_id`,`request_id`),
  ADD KEY `fk_request_task_attachments_param_attachment_types1_idx` (`attachment_type_id`);

--
-- Indexes for table `request_task_remarks`
--
ALTER TABLE `request_task_remarks`
  ADD PRIMARY KEY (`task_remark_id`),
  ADD KEY `fk_request_task_remarks_request_tasks1_idx` (`request_task_id`,`request_id`);

--
-- Indexes for table `resolutions`
--
ALTER TABLE `resolutions`
  ADD PRIMARY KEY (`resolution_no`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contractor`
--
ALTER TABLE `contractor`
  MODIFY `contractor_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `contractor_contracts`
--
ALTER TABLE `contractor_contracts`
  MODIFY `contract_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifer';
--
-- AUTO_INCREMENT for table `contractor_staff`
--
ALTER TABLE `contractor_staff`
  MODIFY `contr_staff_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier';
--
-- AUTO_INCREMENT for table `contractor_staff_experiences`
--
ALTER TABLE `contractor_staff_experiences`
  MODIFY `contr_staff_exp_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier';
--
-- AUTO_INCREMENT for table `contractor_staff_projects`
--
ALTER TABLE `contractor_staff_projects`
  MODIFY `contr_staff_proj_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier';
--
-- AUTO_INCREMENT for table `opportunities`
--
ALTER TABLE `opportunities`
  MODIFY `opportunity_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=179;
--
-- AUTO_INCREMENT for table `opportunity_recipients`
--
ALTER TABLE `opportunity_recipients`
  MODIFY `recipient_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `param_checklist`
--
ALTER TABLE `param_checklist`
  MODIFY `checklist_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier';
--
-- AUTO_INCREMENT for table `param_contractor_categories`
--
ALTER TABLE `param_contractor_categories`
  MODIFY `contractor_category_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `param_contract_status`
--
ALTER TABLE `param_contract_status`
  MODIFY `contract_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary  key; Unique identifier',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `param_contract_types`
--
ALTER TABLE `param_contract_types`
  MODIFY `contract_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier';
--
-- AUTO_INCREMENT for table `param_document_types`
--
ALTER TABLE `param_document_types`
  MODIFY `doc_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier';
--
-- AUTO_INCREMENT for table `param_geo_regions`
--
ALTER TABLE `param_geo_regions`
  MODIFY `geo_region_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=81;
--
-- AUTO_INCREMENT for table `param_opportunity_sources`
--
ALTER TABLE `param_opportunity_sources`
  MODIFY `opp_source_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier',AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT for table `param_opportunity_status`
--
ALTER TABLE `param_opportunity_status`
  MODIFY `opp_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=125;
--
-- AUTO_INCREMENT for table `param_positions`
--
ALTER TABLE `param_positions`
  MODIFY `position_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; unique identifier',AUTO_INCREMENT=99;
--
-- AUTO_INCREMENT for table `param_registration_status`
--
ALTER TABLE `param_registration_status`
  MODIFY `registration_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key, Unique identifier';
--
-- AUTO_INCREMENT for table `param_request_status`
--
ALTER TABLE `param_request_status`
  MODIFY `request_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `param_request_tasks`
--
ALTER TABLE `param_request_tasks`
  MODIFY `request_task_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `param_request_task_status`
--
ALTER TABLE `param_request_task_status`
  MODIFY `task_status_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `param_request_types`
--
ALTER TABLE `param_request_types`
  MODIFY `request_type_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `progress_reports`
--
ALTER TABLE `progress_reports`
  MODIFY `progress_report_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifer';
--
-- AUTO_INCREMENT for table `progress_report_remittances`
--
ALTER TABLE `progress_report_remittances`
  MODIFY `report_remitt_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifer';
--
-- AUTO_INCREMENT for table `requests`
--
ALTER TABLE `requests`
  MODIFY `request_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `request_attachments`
--
ALTER TABLE `request_attachments`
  MODIFY `attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier';
--
-- AUTO_INCREMENT for table `request_aut_projects`
--
ALTER TABLE `request_aut_projects`
  MODIFY `rqst_aut_proj_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier';
--
-- AUTO_INCREMENT for table `request_aut_services`
--
ALTER TABLE `request_aut_services`
  MODIFY `rqst_aut_svc_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier';
--
-- AUTO_INCREMENT for table `request_contracts`
--
ALTER TABLE `request_contracts`
  MODIFY `rqst_contract_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifer';
--
-- AUTO_INCREMENT for table `request_documents`
--
ALTER TABLE `request_documents`
  MODIFY `rqst_doc_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier';
--
-- AUTO_INCREMENT for table `request_equipment`
--
ALTER TABLE `request_equipment`
  MODIFY `rqst_equip_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier';
--
-- AUTO_INCREMENT for table `request_payments`
--
ALTER TABLE `request_payments`
  MODIFY `rqst_payment_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key; unique identifier';
--
-- AUTO_INCREMENT for table `request_schedule`
--
ALTER TABLE `request_schedule`
  MODIFY `rqst_sched_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier';
--
-- AUTO_INCREMENT for table `request_staff`
--
ALTER TABLE `request_staff`
  MODIFY `rqst_staff_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `request_staff_experiences`
--
ALTER TABLE `request_staff_experiences`
  MODIFY `rqst_staff_exp_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; Unique identifier';
--
-- AUTO_INCREMENT for table `request_staff_projects`
--
ALTER TABLE `request_staff_projects`
  MODIFY `rqst_staff_proj_id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary key; unique identifier',AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `request_tasks`
--
ALTER TABLE `request_tasks`
  MODIFY `request_task_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `request_task_attachments`
--
ALTER TABLE `request_task_attachments`
  MODIFY `task_attachment_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `request_task_remarks`
--
ALTER TABLE `request_task_remarks`
  MODIFY `task_remark_id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `contractor`
--
ALTER TABLE `contractor`
  ADD CONSTRAINT `fk_request_contractor_param_contractor_categories10` FOREIGN KEY (`contractor_category_id`) REFERENCES `param_contractor_categories` (`contractor_category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_contractor_requests10` FOREIGN KEY (`contractor_id`) REFERENCES `requests` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contractor_contracts`
--
ALTER TABLE `contractor_contracts`
  ADD CONSTRAINT `fk_contractor_contracts_contractor1` FOREIGN KEY (`contractor_id`) REFERENCES `contractor` (`contractor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contractor_contracts_param_contract_status1` FOREIGN KEY (`contract_status_id`) REFERENCES `param_contract_status` (`contract_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contractor_contracts_param_countries1` FOREIGN KEY (`country_code`) REFERENCES `param_countries` (`country_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contractor_contracts_schedule`
--
ALTER TABLE `contractor_contracts_schedule`
  ADD CONSTRAINT `fk_contractor_contracts_schedule_contractor_contracts1` FOREIGN KEY (`contract_id`) REFERENCES `contractor_contracts` (`contract_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contractor_projects`
--
ALTER TABLE `contractor_projects`
  ADD CONSTRAINT `fk_contractor_projects_contractor_contracts1` FOREIGN KEY (`contract_id`) REFERENCES `contractor_contracts` (`contract_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contractor_projects_param_contract_involvement1` FOREIGN KEY (`involvement_code`) REFERENCES `param_contract_involvement` (`involvement_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_projects_param_project_types10` FOREIGN KEY (`project_type_code`) REFERENCES `param_project_types` (`project_type_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contractor_project_dtl`
--
ALTER TABLE `contractor_project_dtl`
  ADD CONSTRAINT `fk_request_aut_projects_copy1_contractor_contracts1` FOREIGN KEY (`contract_id`) REFERENCES `contractor_contracts` (`contract_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_aut_projects_param_contract_types10` FOREIGN KEY (`contract_type_id`) REFERENCES `param_contract_types` (`contract_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_aut_projects_param_nationalities10` FOREIGN KEY (`prime_contractor_nationality`) REFERENCES `param_nationalities` (`nationality_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contractor_services`
--
ALTER TABLE `contractor_services`
  ADD CONSTRAINT `fk_contractor_services_contractor_contracts1` FOREIGN KEY (`contract_id`) REFERENCES `contractor_contracts` (`contract_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_services_param_nationalities10` FOREIGN KEY (`nationality_code`) REFERENCES `param_nationalities` (`nationality_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contractor_services_dtl`
--
ALTER TABLE `contractor_services_dtl`
  ADD CONSTRAINT `fk_contractor_services_dtl_contractor_contracts1` FOREIGN KEY (`contract_id`) REFERENCES `contractor_contracts` (`contract_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contractor_staff`
--
ALTER TABLE `contractor_staff`
  ADD CONSTRAINT `fk_contractor_staff_contractor1` FOREIGN KEY (`contractor_id`) REFERENCES `contractor` (`contractor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contractor_staff_experiences`
--
ALTER TABLE `contractor_staff_experiences`
  ADD CONSTRAINT `fk_contractor_staff_experiences_contractor_staff1` FOREIGN KEY (`contr_staff_id`) REFERENCES `contractor_staff` (`contr_staff_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contractor_staff_licenses`
--
ALTER TABLE `contractor_staff_licenses`
  ADD CONSTRAINT `fk_contractor_staff_licenses_contractor_staff1` FOREIGN KEY (`contr_staff_id`) REFERENCES `contractor_staff` (`contr_staff_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contractor_staff_projects`
--
ALTER TABLE `contractor_staff_projects`
  ADD CONSTRAINT `fk_contractor_staff_projects_contractor_staff1` FOREIGN KEY (`contr_staff_id`) REFERENCES `contractor_staff` (`contr_staff_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_staff_projects_param_project_types10` FOREIGN KEY (`project_type_code`) REFERENCES `param_project_types` (`project_type_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contractor_statements`
--
ALTER TABLE `contractor_statements`
  ADD CONSTRAINT `fk_contractor_statements_contractor1` FOREIGN KEY (`contractor_id`) REFERENCES `contractor` (`contractor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `contractor_work_volume`
--
ALTER TABLE `contractor_work_volume`
  ADD CONSTRAINT `fk_request_work_volume_copy1_contractor1` FOREIGN KEY (`contractor_id`) REFERENCES `contractor` (`contractor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `country_profiles`
--
ALTER TABLE `country_profiles`
  ADD CONSTRAINT `fk_country_profiles_param_countries1` FOREIGN KEY (`country_code`) REFERENCES `param_countries` (`country_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `opportunities`
--
ALTER TABLE `opportunities`
  ADD CONSTRAINT `fk_opportunities_param_opportunity_sources1` FOREIGN KEY (`opp_source_id`) REFERENCES `param_opportunity_sources` (`opp_source_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_opportunities_param_opportunity_status1` FOREIGN KEY (`opp_status_id`) REFERENCES `param_opportunity_status` (`opp_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `opportunity_recipients`
--
ALTER TABLE `opportunity_recipients`
  ADD CONSTRAINT `fk_opportunity_recipients_opportunities1` FOREIGN KEY (`opportunity_id`) REFERENCES `opportunities` (`opportunity_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `param_checklist`
--
ALTER TABLE `param_checklist`
  ADD CONSTRAINT `fk_param_request_checklist_param_request_types1` FOREIGN KEY (`request_type_id`) REFERENCES `param_request_types` (`request_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `param_geo_region_countries`
--
ALTER TABLE `param_geo_region_countries`
  ADD CONSTRAINT `fk_param_geo_region_countries_param_countries1` FOREIGN KEY (`country_code`) REFERENCES `param_countries` (`country_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_param_geo_region_countries_param_geo_regions1` FOREIGN KEY (`geo_region_id`) REFERENCES `param_geo_regions` (`geo_region_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `param_nationalities`
--
ALTER TABLE `param_nationalities`
  ADD CONSTRAINT `fk_param_nationalities_param_countries1` FOREIGN KEY (`country_code`) REFERENCES `param_countries` (`country_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `param_request_tasks`
--
ALTER TABLE `param_request_tasks`
  ADD CONSTRAINT `fk_param_request_tasks_param_request_types1` FOREIGN KEY (`request_type_id`) REFERENCES `param_request_types` (`request_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `progress_reports`
--
ALTER TABLE `progress_reports`
  ADD CONSTRAINT `fk_progress_reports_contractor_contracts1` FOREIGN KEY (`contract_id`) REFERENCES `contractor_contracts` (`contract_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_progress_reports_param_contract_status1` FOREIGN KEY (`contract_status_id`) REFERENCES `param_contract_status` (`contract_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `progress_report_info`
--
ALTER TABLE `progress_report_info`
  ADD CONSTRAINT `fk_progress_report_info_progress_reports1` FOREIGN KEY (`progress_report_id`) REFERENCES `progress_reports` (`progress_report_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `progress_report_manpower`
--
ALTER TABLE `progress_report_manpower`
  ADD CONSTRAINT `fk_progress_report_manpower_contractor_staff1` FOREIGN KEY (`contr_staff_id`) REFERENCES `contractor_staff` (`contr_staff_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_progress_report_manpower_param_positions1` FOREIGN KEY (`position_id`) REFERENCES `param_positions` (`position_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_progress_report_manpower_progress_reports1` FOREIGN KEY (`progress_report_id`) REFERENCES `progress_reports` (`progress_report_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `progress_report_remittances`
--
ALTER TABLE `progress_report_remittances`
  ADD CONSTRAINT `fk_progress_report_remittances_progress_reports1` FOREIGN KEY (`progress_report_id`) REFERENCES `progress_reports` (`progress_report_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `registration`
--
ALTER TABLE `registration`
  ADD CONSTRAINT `fk_registration_contractor1` FOREIGN KEY (`contractor_id`) REFERENCES `contractor` (`contractor_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_registration_param_registration_status1` FOREIGN KEY (`registration_status_id`) REFERENCES `param_registration_status` (`registration_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `requests`
--
ALTER TABLE `requests`
  ADD CONSTRAINT `fk_requests_param_request_status` FOREIGN KEY (`request_status_id`) REFERENCES `param_request_status` (`request_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_requests_param_request_types1` FOREIGN KEY (`request_type_id`) REFERENCES `param_request_types` (`request_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_attachments`
--
ALTER TABLE `request_attachments`
  ADD CONSTRAINT `fk_request_attachments_request_checklist1` FOREIGN KEY (`request_id`, `checklist_id`) REFERENCES `request_checklist` (`request_id`, `checklist_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_aut_projects`
--
ALTER TABLE `request_aut_projects`
  ADD CONSTRAINT `fk_request_aut_projects_param_contract_types1` FOREIGN KEY (`contract_type_id`) REFERENCES `param_contract_types` (`contract_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_aut_projects_param_countries10` FOREIGN KEY (`country_code`) REFERENCES `param_countries` (`country_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_aut_projects_param_nationalities1` FOREIGN KEY (`prime_contractor_nationality`) REFERENCES `param_nationalities` (`nationality_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_aut_projects_requests10` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_aut_services`
--
ALTER TABLE `request_aut_services`
  ADD CONSTRAINT `fk_request_aut_projects_param_countries1` FOREIGN KEY (`country_code`) REFERENCES `param_countries` (`country_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_aut_projects_requests1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_aut_services_param_nationalities1` FOREIGN KEY (`nationality_code`) REFERENCES `param_nationalities` (`nationality_code`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_checklist`
--
ALTER TABLE `request_checklist`
  ADD CONSTRAINT `fk_request_checklist_param_checklist1` FOREIGN KEY (`checklist_id`) REFERENCES `param_checklist` (`checklist_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_checklist_requests1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_contractor`
--
ALTER TABLE `request_contractor`
  ADD CONSTRAINT `fk_request_contractor_param_contractor_categories1` FOREIGN KEY (`contractor_category_id`) REFERENCES `param_contractor_categories` (`contractor_category_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_contractor_requests1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_contracts`
--
ALTER TABLE `request_contracts`
  ADD CONSTRAINT `fk_contractor_contracts_param_contract_status10` FOREIGN KEY (`contract_status_id`) REFERENCES `param_contract_status` (`contract_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_contractor_contracts_param_countries10` FOREIGN KEY (`country_code`) REFERENCES `param_countries` (`country_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_contracts_requests1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_documents`
--
ALTER TABLE `request_documents`
  ADD CONSTRAINT `fk_request_documents_param_document_types1` FOREIGN KEY (`doc_type_id`) REFERENCES `param_document_types` (`doc_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_documents_requests1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_equipment`
--
ALTER TABLE `request_equipment`
  ADD CONSTRAINT `fk_request_equipment_requests1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_evaluation`
--
ALTER TABLE `request_evaluation`
  ADD CONSTRAINT `fk_request_evaluation_requests1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_license_classifications`
--
ALTER TABLE `request_license_classifications`
  ADD CONSTRAINT `fk_request_license_classifications_param_license_classificati1` FOREIGN KEY (`license_classification_code`) REFERENCES `param_license_classifications` (`license_classification_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_license_classifications_requests1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_payments`
--
ALTER TABLE `request_payments`
  ADD CONSTRAINT `fk_request_payments_requests1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_payment_collections`
--
ALTER TABLE `request_payment_collections`
  ADD CONSTRAINT `fk_request_payment_collections_param_collection_natures1` FOREIGN KEY (`coll_nature_code`) REFERENCES `param_collection_natures` (`coll_nature_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_payment_collections_request_payments1` FOREIGN KEY (`rqst_payment_id`) REFERENCES `request_payments` (`rqst_payment_id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `request_projects`
--
ALTER TABLE `request_projects`
  ADD CONSTRAINT `fk_request_projects_param_contact_involvement1` FOREIGN KEY (`involvement_code`) REFERENCES `param_contract_involvement` (`involvement_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_projects_param_project_types1` FOREIGN KEY (`project_type_code`) REFERENCES `param_project_types` (`project_type_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_projects_request_contracts1` FOREIGN KEY (`rqst_contract_id`) REFERENCES `request_contracts` (`rqst_contract_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_registration`
--
ALTER TABLE `request_registration`
  ADD CONSTRAINT `fk_request_registration_requests1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_schedule`
--
ALTER TABLE `request_schedule`
  ADD CONSTRAINT `fk_request_schedule_requests1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_services`
--
ALTER TABLE `request_services`
  ADD CONSTRAINT `fk_request_services_param_nationalities1` FOREIGN KEY (`nationality_code`) REFERENCES `param_nationalities` (`nationality_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_services_request_contracts1` FOREIGN KEY (`rqst_contract_id`) REFERENCES `request_contracts` (`rqst_contract_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_staff`
--
ALTER TABLE `request_staff`
  ADD CONSTRAINT `fk_request_staff_requests1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_staff_experiences`
--
ALTER TABLE `request_staff_experiences`
  ADD CONSTRAINT `fk_request_staff_experiences_request_staff1` FOREIGN KEY (`rqst_staff_id`) REFERENCES `request_staff` (`rqst_staff_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_staff_licenses`
--
ALTER TABLE `request_staff_licenses`
  ADD CONSTRAINT `fk_request_staff_licenses_request_staff1` FOREIGN KEY (`rqst_staff_id`) REFERENCES `request_staff` (`rqst_staff_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_staff_projects`
--
ALTER TABLE `request_staff_projects`
  ADD CONSTRAINT `fk_request_staff_projects_param_project_types1` FOREIGN KEY (`project_type_code`) REFERENCES `param_project_types` (`project_type_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_staff_projects_request_staff1` FOREIGN KEY (`rqst_staff_id`) REFERENCES `request_staff` (`rqst_staff_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_statements`
--
ALTER TABLE `request_statements`
  ADD CONSTRAINT `fk_request_statements_requests1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_tasks`
--
ALTER TABLE `request_tasks`
  ADD CONSTRAINT `fk_param_request_tasks_copy1_requests1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_tasks_param_request_task_status1` FOREIGN KEY (`task_status_id`) REFERENCES `param_request_task_status` (`task_status_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_task_attachments`
--
ALTER TABLE `request_task_attachments`
  ADD CONSTRAINT `fk_request_task_attachments_param_attachment_types1` FOREIGN KEY (`attachment_type_id`) REFERENCES `param_attachment_types` (`attachment_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_request_task_attachments_request_tasks1` FOREIGN KEY (`request_task_id`, `request_id`) REFERENCES `request_tasks` (`request_task_id`, `request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `request_task_remarks`
--
ALTER TABLE `request_task_remarks`
  ADD CONSTRAINT `fk_request_task_remarks_request_tasks1` FOREIGN KEY (`request_task_id`, `request_id`) REFERENCES `request_tasks` (`request_task_id`, `request_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
