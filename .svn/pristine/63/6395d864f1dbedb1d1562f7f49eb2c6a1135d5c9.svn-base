<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Equipment_model extends CEIS_Model {
	
	public function __construct()
	{
		parent::__construct();
	}		
	
	public function get_equipment_list($aColumns, $bColumns, $params)
	{
		try
		{	
			
			$cColumns = array(
						"a.serial_no",
						"a.registrant_name", 
						"a.registrant_address", 
						"a.equipment_type", 
						"a.capacity", 
						"a.brand",
						"a.model",
						"a.acquired_date",
						"a.acquisition_cost",
						"a.present_condition",
						"a.location",
						"a.motor_no",
						"a.color",
						"a.year",
						"a.flywheel",
						"a.acquired_from",
						"a.ownership_type",
						"a.active_flag",
						"a.created_by",
						"a.created_date",
						"a.modified_by",
						"a.modified_date");
			
			$fields = str_replace(" , ", " ", implode(", ", $aColumns));

			$sWhere = $this->filtering($cColumns, $params, FALSE);
			$sOrder = $this->ordering($bColumns, $params);
			$sLimit = $this->paging($params);
			
			$filter_str = $sWhere["search_str"];
			$filter_params = $sWhere["search_params"];
		
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $fields 
				FROM  %s a
				$filter_str
				GROUP BY a.serial_no
	        	$sOrder
	        	$sLimit
EOS;

			
			$query	= sprintf($query, CEIS_Model::tbl_equipment);			
			$stmt	= $this->query($query, $filter_params);
		
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}	
	
	public function get_specific_equipment($where){

		try
		{
			$fields = array("*");
			
			
			return $this->select_one($fields, CEIS_Model::tbl_equipment, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function filtered_length($aColumns, $bColumns, $params)
	{
		try
		{
			$this->get_equipment_list($aColumns, $bColumns, $params);
	
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(serial_no) cnt");
				
			return $this->select_one($fields, CEIS_Model::tbl_equipment);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			
			throw $e;			
		}	
	}
	

	public function get_equipment_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("serial_no" => $id);
				
			return $this->select_one($fields, CEIS_Model::tbl_equipment, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
	}
}
/* End of file Equipment_model.php */
/* Location: ./application/modules/ceis/models/Equipment_model.php */