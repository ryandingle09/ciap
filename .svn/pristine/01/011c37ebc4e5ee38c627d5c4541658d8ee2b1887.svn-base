<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bas_chart_accounts_model extends BAS_Model {

	//fixed code define
	var $find_code_ps   	= PARAM_ACCOUNT_CODE_PS;//501
	var $find_code_mooe   	= PARAM_ACCOUNT_CODE_MOOE;//502
	var $find_code_co   	= PARAM_ACCOUNT_CODE_CO;//503

	//fix status
	var $voided   			= STATUS_VOIDED;
	var $excess   			= STATUS_EXCESS;
	var $overriden  		= STATUS_OVERRIDEN;
	var $posted   			= STATUS_POSTED;
	var $submitted   		= STATUS_SUBMITTED;
	var $approved   		= STATUS_APPROVED;
	var $disapproved   		= STATUS_DISAPPROVED;
	var $active   			= STATUS_ACTIVE;
	var $not_active   		= STATUS_NOT_ACTIVE;

	public function __construct()
	{
		parent::__construct();
	}

	public function get_parent_acount_list()
	{
		try
		{
			$fields = array("account_code", "account_name");
			return $this->select_all($fields, BAS_Model::tbl_param_account_codes);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function get_specific_chart($where, $multi=FALSE){

		try
		{
			$fields 	= array("*");
			$function	= ($multi) ? "select_all" : "select_one";
			
			return $this->{$function}($fields, BAS_Model::tbl_param_account_codes, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
				
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);

			throw $e;
		}
	}

	public function get_chart_details($id)
	{
		try
		{
			$fields = array("account_code","account_name","parent_account_code","active_flag");
			$where = array('account_code' => $id);
			return $this->select_one($fields, BAS_Model::tbl_param_account_codes, $where);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	
	
	
	public function get_chart_list($fields, $selects, $params)
	{
		try
		{

			$c_columns = array("account_code","account_name","parent_account_code","IF(active_flag = 1, '$this->active', '$this->not_active')");
			$tbl_fields = str_replace(" , ", " ", implode(", ", $fields));
			
			$where = $this->filtering($c_columns, $params, FALSE);
			$order = $this->ordering($selects, $params);
			$limit = $this->paging($params);
			
			$filter_str = $where["search_str"];
			$filter_params = $where["search_params"];
			
			$query = <<<EOS
				SELECT SQL_CALC_FOUND_ROWS $tbl_fields 
				FROM %s
				$filter_str
	        	$order
	        	$limit
EOS;
			$table = sprintf($query, BAS_Model::tbl_param_account_codes);
			return $this->query($table, $filter_params);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}	
	
	
	public function filtered_length($fields, $selects, $params)
	{
		try
		{
			$this->get_chart_list($fields, $selects, $params);
			$query = <<<EOS
				SELECT FOUND_ROWS() cnt
EOS;
	
			$stmt = $this->query($query, NULL, FALSE);
			return $stmt;
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}
	
	
	public function total_length()
	{
		try
		{
			$fields = array("COUNT(account_code) cnt");
			return $this->select_one($fields, BAS_Model::tbl_param_account_codes);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}	
	}

	public function check_exist_account_codes($code)
	{
		try
		{
			$count = $this->select_all(['COUNT(account_code) as count'], BAS_Model::tbl_param_account_codes, ['account_code'=> $code]);
			return $count[0]['count'];
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}

	public function insert_chart($params)
	{
		try
		{
			$data = array(
				'account_code' 			=> filter_var($params['code'], FILTER_SANITIZE_STRING),
				'account_name' 			=> filter_var($params['title'], FILTER_SANITIZE_STRING),
				'created_date' 			=> date("Y-m-d H:i:s"),
				'created_by' 			=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : 1,
				'active_flag' 			=> filter_var($params['status'], FILTER_SANITIZE_NUMBER_INT),
			);

			IF(!EMPTY($params['parent_code'])) 
				$data['parent_account_code'] = filter_var($params['parent_code'], FILTER_SANITIZE_STRING);
			
			return $this->insert_data(BAS_Model::tbl_param_account_codes, $data);
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}
	
	public function update_chart($params)
	{
		try
		{
			$data = array(
				'account_code' 			=> filter_var($params['code'], FILTER_SANITIZE_STRING),
				'account_name' 			=> filter_var($params['title'], FILTER_SANITIZE_STRING),
				'modified_date' 		=> date("Y-m-d H:i:s"),
				'modified_by' 			=> ($this->session->userdata('user_id')) ? $this->session->userdata('user_id') : 1,				
				'active_flag' 			=> filter_var($params['status'], FILTER_SANITIZE_NUMBER_INT),
			);

			if(!EMPTY($params['parent_code'])) 
				$data['parent_account_code'] = filter_var($params['parent_code'], FILTER_SANITIZE_STRING);

			$where = array(
				'account_code' => $params['old_code']
			);

			$this->update_data(BAS_Model::tbl_param_account_codes, $data, $where);
			
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;			
		}
	}
	
	public function delete_chart($id)
	{
		try
		{
			$this->delete_data(BAS_Model::tbl_param_account_codes, array('account_code' => $id));				
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}

	public function get_chart_count($id)
	{
		try
		{	
			$fields = array("COUNT(*) cnt");
			$where	= array("account_code" => $id);
			return $this->select_one($fields, BAS_Model::tbl_param_account_codes, $where);
	
		}
		catch(PDOException $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
		catch(Exception $e)
		{
			$this->rlog_error($e);
			throw $e;
		}
	}
	
	
}