<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mpis_code_libraries_policy_categories extends MPIS_Controller {
	
	private $module = MODULE_MPIS_CODE_LIBRARIES_POLICY_CATEGORIES;
	
	public $permission_add		= FALSE;
	public $permission_edit		= FALSE;	
	public $permission_save		= FALSE;
	public $permission_view		= FALSE;
	public $permission_delete	= FALSE;
	
	public function __construct()
	{
		parent::__construct();
		
		$this->load->model('policy_categories_model', 'policy_categories');
		
		$this->permission			= $this->check_permission($this->module);
		$this->permission_add		= $this->check_permission($this->module, ACTION_ADD);
		$this->permission_edit		= $this->check_permission($this->module, ACTION_EDIT);
		$this->permission_save		= $this->check_permission($this->module, ACTION_SAVE);
		$this->permission_view		= $this->check_permission($this->module, ACTION_VIEW);
		$this->permission_delete	= $this->check_permission($this->module, ACTION_DELETE);

	}
	
	public function index()
	{	
		try{
			$data = $resources = $modal = array();
			
			
			
			if($this->permission)
			{
				$modal = array(
					'modal_policy_categories' => array(
						'module'		=> PROJECT_MPIS,
						'controller'	=> __CLASS__,
						'method'		=> 'modal_policy_categories',
						'title'			=> 'Policy categories',					
						'multiple'		=> TRUE
					),
					
				);
				
				$resources['load_css'] 		= array(CSS_DATATABLE);
				$resources['load_js'] 		= array(JS_DATATABLE);
				$resources['load_modal'] 	= $modal;
				$resources['datatable']		= array();
				$resources['datatable'][] 	= array(
					'table_id' 	=> 'policy_categories_table', 
					'path' 		=> PROJECT_MPIS.'/mpis_code_libraries_policy_categories/get_policy_categories_list'				
				);
				
				
				// STORE THE PERMISSION
				$data['permission_add']	= $this->permission_add; 	
				
				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash(0);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();
				$token 			= in_salt($encoded_id, $salt);
				$data['url']	= $encoded_id."/".$salt."/".$token;
				
				$view_page	= "policy_categories";
			}
			else
			{
				$view_page	= "errors/html/error_permission";
			}

			$this->template->load($view_page, $data, $resources);
		}
		catch(PDOException $e){
			echo $e->getMessage();
		}
		catch(Exception $e){
			echo $e->getMessage();
		}
	}
	
	
	public function get_policy_categories_list()
	{
		try{

			$params = get_params(); // DO NOT USE: $_POST $this->input->post
			
			$output = array(
				"sEcho" 				=> intval($params['sEcho']),
				"iTotalRecords" 		=> 0,
				"iTotalDisplayRecords" 	=> 0,
				"aaData" 				=> array()
			);
			
			if(!$this->permission)
				throw new Exception($this->lang->line('err_unauthorized_view'));
		
			// FIELDS TO BE SELECTED FROM TABLE
			$select_fields = array(
				"A.policy_category_id",
				"A.policy_category_name",
				"A.policy_category_desc",
				"A.built_in_flag",
				"B.policy_total"
			);

			// APPEARS ON TABLE
			$where_fields = array(
				"A.policy_category_name",
				"A.policy_category_desc"
			);
		
			$results 		= $this->policy_categories->get_policy_categories_list($select_fields, $where_fields, $params);
			$total	 		= $this->policy_categories->total_length(); // TOTAL COUNT OF RECORDS
			$filtered_total = $this->policy_categories->filtered_length($select_fields, $where_fields, $params); // TOTAL COUNT OF RECORDS PER PAGE
		
			foreach ($results as $data):

				// PRIMARY KEY
				$primary_key	= $data["policy_category_id"];

				// CONSTRUCT SECURITY VARIABLES
				$hash_id 		= $this->hash($primary_key);
				$encoded_id 	= base64_url_encode($hash_id);
				$salt 			= gen_salt();			
				$token 			= in_salt($encoded_id, $salt);			
				$url 			= $encoded_id."/".$salt."/".$token;
				
				$delete_action 	= 'content_delete("policy category", "'.$url.'")';

				// START: CONSTRUCT ACTION ICONS
				$action = "<div class='table-actions'>";
				
					if($this->permission_edit) :
						$action .= "<a href='javascript:;' class='md-trigger edit tooltipped' data-tooltip='Edit' data-position='bottom' data-delay='50' data-modal='modal_policy_categories' onclick=\"modal_policy_categories_init('".$url."')\"></a>";
					endif;
					
					if($this->permission_delete AND EMPTY($data['policy_total']) AND EMPTY($data['built_in_flag'])) : 	  
						$action .= "<a href='javascript:;' onclick='" . $delete_action. "' class='delete tooltipped' data-tooltip='Delete' data-position='bottom' data-delay='50'></a>";
					endif;
				
				$action .= '</div>';
				// END: CONSTRUCT ACTION ICONS				

				$row 	= array();
				$row[] 	= $data['policy_category_name'];
				$row[] 	= $data['policy_category_desc'];				
				$row[] 	= $action;
					
				$output['aaData'][] = $row;
				
			endforeach;
			
			$output["iTotalRecords"] 		= $total["cnt"];
			$output["iTotalDisplayRecords"] = $filtered_total["cnt"];
		}
		catch(PDOException $e){
			$this->rlog_error($e);
		}
		catch(Exception $e){
			$this->rlog_error($e);
		}
		
		echo json_encode( $output );
	}
	
	public function modal_policy_categories($encoded_id, $salt, $token)
	{
		try{
			$msg	= "";
			$info	= array();
			$data 	= array();
			
			// CHECK THE SECURITY VARIABLES
			check_salt($encoded_id, $salt, $token);
			
			$hash_id = base64_url_decode($encoded_id);
			
			$data['permission_save']	= $this->permission_add;
			
			if($this->hash(0) != $hash_id){
				
				$key 			= $this->get_hash_key('policy_category_id');
				$where			= array();
				$where[$key]	= $hash_id;
				
				$info = $this->policy_categories->get_specific_policy_category($where);

				if(EMPTY($info))
					throw new Exception($this->lang->line('err_invalid_request'));
				
				$data['permission_save']	= $this->permission_edit;
			}
			
			$resources					= array();
			$resources['load_css'] 		= array(CSS_LABELAUTY);
			$resources['load_js'] 		= array(JS_LABELAUTY);
			
			
			$data['info']	= $info; // POLICY CATEGORY DETAILS
		
			// REGENERATE SECURITY VARIABLES
			$salt 				= gen_salt();
			$token				= in_salt($encoded_id, $salt);
			$data['security']	= $encoded_id . '/' . $salt . '/' . $token;
			
			$this->load->view('modals/policy_categories',$data);
			$this->load_resources->get_resource($resources);			
		}
		catch(PDOException $e){
			$msg = $e->getMessage();
		}
		catch(Exception $e){
			$msg = $e->getMessage();
		}
		

	}
	
	public function process()
	{
		try
		{
			$flag 			= 0;
			$params 		= get_params();
			
						
			// SERVER VALIDATION
			$this->_validate($params);

	
			// GET THE ORIGINAL VALUE OF PRIMARY KEY
			$key 			= $this->get_hash_key('policy_category_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];

			$info 				= $this->policy_categories->get_specific_policy_category($where);
			$policy_category_id	= $info['policy_category_id'];
			

			MPIS_Model::beginTransaction();

			$activity_title	= $params['policy_category_name'];
			
			// IF EMPTY MEANS INSERT/SAVE RECORD
			IF(EMPTY($policy_category_id)){
				
				if($this->permission_add === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_add'));
				
				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_INSERT;			
				$audit_table[]	= MPIS_Model::tbl_param_policy_categories;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array();
				
				$policy_category_id = $this->policy_categories->insert_policy_category($params); // SAVES THE INDICATOR RECORD
				
				$current_info 		= $this->policy_categories->get_specific_policy_category(array('policy_category_id' => $policy_category_id));
				
				$curr_detail[]		= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				

				$activity	= "%s has been added in policy categories.";
				$activity 	= sprintf($activity, $activity_title);

				$msg  = $this->lang->line('data_saved');
			}

			else{

				if($this->permission_edit === FALSE)
					throw new Exception($this->lang->line('err_unauthorized_edit'));
				
				$params['policy_category_id']	= $policy_category_id;

				// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL  
				$audit_action[]	= AUDIT_UPDATE;			
				$audit_table[]	= MPIS_Model::tbl_param_policy_categories;
				$audit_schema[]	= DB_MPIS;
				$prev_detail[]	= array($info);
				
				$this->policy_categories->update_policy_category($params); // UPDATE THE INDICATOR RECORD
				
				$current_info 	= $this->policy_categories->get_specific_policy_category(array('policy_category_id' => $policy_category_id));
				
				$curr_detail[]	= array($current_info);
				// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL  

				$activity	= "%s has been updated in policy categories.";
				$activity 	= sprintf($activity, $activity_title);

				$msg  = $this->lang->line('data_updated');
			}


			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity, 
				$this->module, 
				$prev_detail, 
				$curr_detail, 
				$audit_action, 
				$audit_table, 
				$audit_schema
			);
			
			MPIS_Model::commit();

			$flag = 1;
			
		}
		catch(PDOException $e)
		{
			MPIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
			
		}
		catch(Exception $e)
		{
			MPIS_Model::rollback();
			
			$msg = $this->rlog_error($e, TRUE);
		}
		
		$info = array(
			"flag" => $flag,
			"msg" => $msg
		);
	
		echo json_encode($info);
	
	}
	
	/*
	 * START: SERVER VALIDATION 
	 */
	
	private function _validate(&$params)
	{
		try
		{

			$this->_validate_security($params);
			
			//SPECIFY HERE INPUTS FROM USER
			$fields = array();
			$fields["policy_category_name"]		= 'Policy Category';

			$this->check_required_fields($params, $fields);
	
			return $this->_validate_input($params);
			
		}
		catch(Exception $e)
		{
			throw $e;
		}
	
	}

	private function _validate_security(&$params)
	{
		try
		{
			if(EMPTY($params['security']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}


			$security = explode('/', $params['security']);

			$params['encoded_id']	= $security[0];
			$params['salt']			= $security[1];
			$params['token']		= $security[2];

			check_salt($params['encoded_id'], $params['salt'], $params['token']);

			$params['hash_id']		= base64_url_decode($params['encoded_id']);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}
	
	private function _validate_input($params)
	{
		try
		{
			
			$validation['policy_category_name'] = array(
				'data_type' => 'string',
				'name'		=> 'Policy Category',
				'min_len'	=> 5,
				'max_len'	=> 255
			);
			
			IF(!EMPTY($params['policy_category_desc']))
			{
				$validation['policy_category_desc'] = array(
					'data_type' => 'string',
					'name'		=> 'Description',
					'min_len'	=> 5,
					'max_len'	=> 60000
				);
			}
	
			return $this->validate_inputs($params, $validation);
		}
		catch(Exception $e)
		{
			throw $e;
		}
	}

	/*
	 * END: SERVER VALIDATION
	 */
	
	
	public function delete_policy_category() 
	{
		try 
		{

			$flag				= 0;
			$msg				= "Error";
						
			if($this->permission_delete === FALSE)
				throw new Exception($this->lang->line('err_unauthorized_delete'));
			
			$params 			= get_params();
			$params['security']	= $params['param_1'];

			$this->_validate_security($params);

			$key 			= $this->get_hash_key('policy_category_id');
			$where			= array();
			$where[$key]	= $params['hash_id'];
			
			$info 			= $this->policy_categories->get_specific_policy_category($where);
			
			
			$title			= $info['policy_category_name'];

			if(EMPTY($info['policy_category_id']))
			{
				throw new Exception($this->lang->line('err_invalid_request'));
			}
			
			MPIS_Model::beginTransaction();
			
			// START: THESE VARIABLES ARE USED IN AUDIT TRAIL
			$audit_action[]	= AUDIT_DELETE;
			$audit_table[]	= MPIS_Model::tbl_param_policy_categories;
			$audit_schema[]	= DB_MPIS;			
			$prev_detail[]	= array($info);
			// END: START: THESE VARIABLES ARE USED IN AUDIT TRAIL

			$this->policy_categories->delete_policy_category($info['policy_category_id']);

			// SET AS AN ARRAY
			$curr_detail[]	= array();
			
			// START: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			$activity	= "%s has been deleted.";
			$activity	= sprintf($activity, $title);
			// END: THESE VARIABLES ARE USED FOR AUDIT TRAIL
			
			// SAVE THE AUDIT TRAIL
			$this->audit_trail->log_audit_trail(
				$activity,
				$this->module,
				$prev_detail,
				$curr_detail,
				$audit_action,
				$audit_table,
				$audit_schema
			);
			
			MPIS_Model::commit();

			$flag	= 1;
			$msg = $this->lang->line('data_deleted');
		}		
		catch(PDOException $e)
		{		
			MPIS_Model::rollback();

			$msg	= $e->getMessage(); 	
		}
		catch(Exception $e)
		{	

			MPIS_Model::rollback();
						
			$msg	= $e->getMessage(); 	
		}		
		
		$info = array(
			"flag"	=> $flag,
			"msg"	=> $msg
		);

		echo json_encode($info);
	}
}

/* End of file Ceis_positions.php */
/* Location: ./application/modules/ceis/controllers/Ceis_positions.php */